<?php 
	 function SetAccessRole($action=""){
		// ceck referer from host or not
		if(
		static::$referer == true and
		str_replace('/','',str_replace('panelbackend','',str_replace('index.php','',$_SERVER['HTTP_REFERER'])))
		<>
		str_replace('/','',str_replace('panelbackend','',str_replace('index.php','',base_url())))
		)
		{

			$this->Error404();
			exit();
		}

		if(in_array($this->page_ctrl, $this->page_escape))
			return true;

		// set private area
		if($this->private)
		{
			// ceck login
			if(!$_SESSION[SESSION_APP]['login']){
				if($this->post[date('Ymd')]){
					$token = $this->post[date('Ymd')];
					$token = base64_decode($token);
					list($menu, $group_id) = explode(md5(date('Ymdhi')), $token);
					$menu = base64_decode($menu);
					$_SESSION[SESSION_APP]['menu'] = json_decode($menu, true);
					$group_id = base64_decode($group_id);
					$_SESSION[SESSION_APP]['group_id'] = json_decode($group_id, true);
				}else{
					$_SESSION[SESSION_APP]['curr_page'] = uri_string();
					redirect('panelbackend/login','client');
				}
			}

			/*if($this->sso['auth_page'] && $_SESSION[SESSION_APP]['user_id']!=1){

				$username = $_SESSION[SESSION_APP]['username'];
				$credential = $_SESSION[SESSION_APP]['credential'];
            	$respon = $this->auth->autoAuthenticate($username,$credential);

            	if(!($respon->RESPONSE == "1" or $respon->RESPONSE == "PAGE")){

					unset($_SESSION[SESSION_APP]);

					$_SESSION[SESSION_APP]['curr_page'] = uri_string();

					redirect('panelbackend/login','client');
            	}
			}*/
		}

		if($_SESSION[SESSION_APP]['user_id']==1){
			$this->is_super_admin = true;
		}else{
			$this->is_super_admin = false;
		}

		if($this->page_ctrl=='panelbackend/page' or $this->page_ctrl=='panelbackend/pageone'){
			$this->access_role = $this->auth->GetAccessRole('panelbackend/page');
		}else{
			$this->access_role = $this->auth->GetAccessRole($this->page_ctrl);
		}

		$this->access_role_custom[$this->page_ctrl] = $this->access_role;
		// $this->access_role[$this->mode] = 1;


	/*	if($this->page_ctrl=='panelbackend/home' && !empty($_SESSION[SESSION_APP]['user_id']))
			return true;*/

		if($this->page_ctrl=='panelbackend/home' && $this->mode=='ug')
			return;
		
		if(!$this->access_role[$this->mode]){
			$str = '';

			if(ENVIRONMENT=='development')
				$str = "akses : ".print_r($this->access_role,true);

			$this->Error403($str);
			exit();
		}
	} 
?>