<?php 

function pre(){
	echo "<pre>";
}

function _______before(){
	$ci =& get_instance();
	return $ci;
}

function debug_query(){
	pre();
	$ci = _______before();
	echo $ci->db->last_query();
	exit;
}

function debug_post(){
	pre();
	$ci = _______before();
	print_r($ci->input->post());
	exit;
}

function debug_return( $return ){
	pre();
	print_r($return);
	exit;
}


/*
 * @param = date('Y-m-d') can null
 *
 */
function indodate($date = null){

		if ($date != null) {
			$tanggal = $date;
		} else {
			$tanggal = date('Y-m-d');
		}

		$bulan = array (
			1 =>   'Januari',
			2 =>'Februari',
			3 =>'Maret',
			4 =>'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember'
		);
		$pecahkan = explode('-', $tanggal); 
		return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
