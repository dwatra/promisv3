<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['base_url'] = (is_https() ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
$config['index_page'] = '';
$config['uri_protocol']	= 'REQUEST_URI';
$config['url_suffix'] = '';
$config['language']	= 'indonesia';
$config['charset'] = 'UTF-8';
$config['enable_hooks'] = FALSE;
$config['subclass_prefix'] = '_';
$config['composer_autoload'] = 'vendor/autoload.php';
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';
$config['allow_get_array'] = TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd';
$config['log_threshold'] = 0;
$config['log_path'] = '';
$config['log_file_extension'] = '';
$config['log_file_permissions'] = 0644;
$config['log_date_format'] = 'Y-m-d H:i:s';
$config['error_views_path'] = '';
$config['cache_path'] = '';
$config['cache_query_string'] = FALSE;
$config['encryption_key'] = '';
$config['sess_driver'] = 'files';
$config['sess_cookie_name'] = 'ci_session';
$config['sess_expiration'] = 72000;
$config['sess_save_path'] = NULL;
$config['sess_match_ip'] = FALSE;
$config['sess_time_to_update'] = 300;
$config['sess_regenerate_destroy'] = FALSE;
$config['cookie_prefix']	= '';
$config['cookie_domain']	= '';
$config['cookie_path']		= '/';
$config['cookie_secure']	= FALSE;
$config['cookie_httponly'] 	= FALSE;
$config['standardize_newlines'] = FALSE;
$config['global_xss_filtering'] = FALSE;
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;
$config['csrf_regenerate'] = TRUE;
$config['csrf_exclude_uris'] = array();
$config['compress_output'] = FALSE;
$config['time_reference'] = 'local';
$config['rewrite_short_tags'] = FALSE;
$config['proxy_ips'] = '';

$config['title'] = "PROMIS";
$config['copyright'] = 'Copyright &copy; PJBS SCM '.date('Y');
$config['date_format'] = "DD-MM-YYYY";
$config['timestamp_format'] = "dd-mm-yyyy hh24:mi:ss";

$config['company_name'] = "PT PJB SERVICES";
$config['company_address'] = "Jl. Raya Juanda No. 17 Sidoarjo, Jawa Timur - Indonesia 61253";
$config['company_telp'] = "(031) 8548391, (031) 8557909";
$config['company_email'] = "info@pjbservices.com";
$config['company_fax'] = "(031) 8548360";
$config['company_jabatan_scm'] = "MANAJER PENGADAAN & SUPPLY CHAIN";
$config['company_pejabat_scm'] = "WISNU TRI MULYANTO";

$config['file_upload_config']['upload_path']          = './uploads/';
$config['file_upload_config']['allowed_types']        = 'gif|jpg|jpeg|bmp|png|pdf|doc|docx|mpp';
$config['file_upload_config']['max_size']             = 100000; //kb

$config['sso']['auth_page'] = true;
$config['url_java'] = "http://localhost:8080/javabridge/java/Java.inc";

$config['url_hpe'] = "http://scm.pjbservices.com/panelbackend/ws/";
$config['auth_hpe'] = array("username"=>"ngademin","password"=>"46f7ccf5b4ce37384ec5ac4e524b8f7e5ef28c0a");