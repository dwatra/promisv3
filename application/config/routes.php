<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';
$route['panelbackend'] = 'panelbackend/home';
//$route['panelbackend/(:any)'] = 'panelbackend/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



/* ROUTES */
$route['master_tool']                    = 'new/tool/index';
$route['laporan_manpower']               = 'new/man/report';
$route['dashboard_manpower']             = 'new/man/dashboard';
$route['master_manpower']                = 'new/man';
$route['transaction_site']               = 'new/tool_site/index';
$route['transaction_site_detail/(:num)'] = 'new/tool_site/detail/$1';


