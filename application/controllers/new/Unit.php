<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	private $table = 'NEW_TL_UNIT';
	
	public function index(){


		$this->data['content'] = $this->load->view('unit/index', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data);
	}

	function load_data(){
		$select = "*";
		$tbl    = $this->table;		
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length') 
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(ID),LOWER(ID_DISTRIK),LOWER(KD_DISTRIK),LOWER(NAMA_DISTRIK),LOWER(ALAMAT),LOWER(KETERANGAN)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		$where['data'][] = [
			'column' => 'STATUS_DELETE',
			'param'   => 0
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, null , null , null , $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , NULL , null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null);

		// echo $this->db->last_query();
		// exit;

		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {

					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="edit_unit('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="javascript:void(0)" onclick="delete_data('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					$st .= '</ul>';
					

					$response['data'][] = array(
						$item->id,
						$item->id_distrik,
						$item->kd_distrik,
						$item->nama_distrik,
						$item->alamat,
						$item->keterangan,
						$st
						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		echo json_encode($response);
	}

	public function process($unit_id = null){
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$data =[
				'ID_DISTRIK'   => $_POST['id_distrik'],
				'KD_DISTRIK'   => $_POST['kd_distrik'],
				'NAMA_DISTRIK' => $_POST['nm_distrik'],
				'ALAMAT'       => $_POST['alamat_distrik'],
				'KETERANGAN'   => $_POST['ket'],
			];
			if ($unit_id != null ) {
				$status = 200;
				$s =  $this->query->update_data($this->table , ['ID'=> $unit_id],  $data);
			} else {
				$status = 201;
				$s =  $this->query->save_data($this->table , $data);
			}
			if ($s) {
				$resp =[
					'status' => $status,
					'msg' => 'Success input / update data'
				];
			} else {
				$resp =[
					'status' => 400,
					'msg' => 'Something error'
				];
			}
			echo json_encode($resp);
		} else {
			exit('no method allowed');
		}
	}

	function load_detail($id){
		$where = [
			'ID' => $id
		];
		$data['status'] = 200;
		$data['data'] = $this->query->get_data_simple('NEW_TL_UNIT' , $where)->row();
		echo json_encode($data);
	}

	function delete_data($id){


		$where =[
			'STATUS_DELETE' => 0,
			'ID' => $id,
		];

		$update = $this->query->update_data($this->table , $where , ['STATUS_DELETE' => 1]);

		if ($update) {
			$resp = [
				'status' => 200,
				'msg' => 'success delete data'
			];
		}

		else {

			$resp = [
				'status' => 400,
				'msg' => 'faIL delete data'
			];	
		}

		echo json_encode($resp);

	}
}
