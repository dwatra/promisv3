<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tdb extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel", "proyek");
		$this->load->model("New_tl_toolModel", "tool");
		$this->load->model("New_tl_tool_detailModel", "tool_detail");
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	private $table = 'NEW_TL_TDB';

	public function index()
	{


		$this->data['content'] = $this->load->view('tdb/index', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data);
	}

	function load_data()
	{
		$select = "*";
		$tbl    = $this->table;
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(ID),LOWER(NAMA_UNIT),LOWER(TIPE_UNIT),LOWER(KAPASITAS),LOWER(TAHUN_OPERASI),LOWER(ENGINE_MANUFACTURE),LOWER(BOILER_MANUFACTURE),LOWER(TURBINE_MANUFACTURE),LOWER(FILE_0),LOWER(FILE_1)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$where['data'][] = [
			'column' => 'STATUS_DELETE',
			'param'   => 0
		];
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, null, null, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null, NULL, null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null, null, null);

		// echo $this->db->last_query();
		// exit;

		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {

					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="edit_unit(' . $item->id . ')" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="javascript:void(0)" onclick="delete_data(' . $item->id . ')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					$st .= '</ul>';

					$response['data'][] = array(
						$item->id,
						$item->nama_unit,
						$item->tipe_unit,
						$item->kapasitas,
						$item->tahun_operasi,
						$item->engine_manufacture,
						$item->boiler_manufacture,
						$item->turbine_manufacture,
						'<a href="' . base_url() . 'uploads/tdb/' . $item->file_0 . '" download >Download file 1</a>',
						'<a href="' . base_url() . 'uploads/tdb/' . $item->file_1 . '" download >Download file 2</a>',
						$st
						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}


		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		echo json_encode($response);
	}

	public function process($tdb_id = null)
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			// print_r($_POST);
			// print_r($_FILES);
			// exit;

			$config['upload_path']   = 'uploads/tdb';
			$config['allowed_types'] = '*';
			$config['encrypt_name']  = true;
			$this->load->library('upload', $config);
			$data = [
				'NAMA_UNIT'   => $_POST['nama_unit'],
				'TIPE_UNIT' => $_POST['tipe_unit'],
				'KAPASITAS'       => $_POST['kapasitas'],
				'TAHUN_OPERASI'   => $_POST['tahun_operasi'],
				'ENGINE_MANUFACTURE'   => $_POST['engine_manufacture'],
				'BOILER_MANUFACTURE'   => $_POST['boiler_manufacture'],
				'TURBINE_MANUFACTURE'   => $_POST['turbine_manufacture'],

			];
			if ($tdb_id != null) {
				$get = $this->query->get_data_simple($this->table, ['ID' => $tdb_id])->row();
				
				if (isset($_FILES['files']['name'])) {

					if (!empty($_FILES['files']['name'][0])) {

						$_FILES['file']['name'] = $_FILES['files']['name'][0];
						$_FILES['file']['type'] = $_FILES['files']['type'][0];
						$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][0];
						$_FILES['file']['error'] = $_FILES['files']['error'][0];
						$_FILES['file']['size'] = $_FILES['files']['size'][0];

						if ($this->upload->do_upload('file')) {
							
							if ($get) {
								$old_path = './uploads/tdb/' . $get->file_0;
								if (file_exists($old_path)) {
									unlink($old_path);
								}
							}
							$uploadData = $this->upload->data();
							$data['FILE_0'] = $uploadData['file_name'];
						}
					}
					if (!empty($_FILES['files']['name'][1])) {

						$_FILES['file']['name'] = $_FILES['files']['name'][1];
						$_FILES['file']['type'] = $_FILES['files']['type'][1];
						$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][1];
						$_FILES['file']['error'] = $_FILES['files']['error'][1];
						$_FILES['file']['size'] = $_FILES['files']['size'][1];

						if ($this->upload->do_upload('file')) {
							
							if ($get) {
								$old_path = './uploads/tdb/' . $get->file_1;
								if (file_exists($old_path)) {
									unlink($old_path);
								}
							}
							$uploadData = $this->upload->data();
							$data['FILE_1'] = $uploadData['file_name'];
						}
					}
					
				}
				$status = 200;

				$s =  $this->query->update_data($this->table, ['ID' => $tdb_id],  $data);
			} else {

				if (isset($_FILES['files']['name'])) {

					$count = count($_FILES['files']['name']);
					for ($i = 0; $i < $count; $i++) {
						if (!empty($_FILES['files']['name'][$i])) {

							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							if ($this->upload->do_upload('file')) {
								$uploadData = $this->upload->data();
								$data['FILE_' . $i] = $uploadData['file_name'];
							}
						}
					}
				}
				$status = 201;
				$s =  $this->query->save_data($this->table, $data);
			}
			if ($s) {
				$resp = [
					'status' => $status,
					'msg' => 'Success input / update data'
				];
			} else {
				$resp = [
					'status' => 400,
					'msg' => 'Something error'
				];
			}
			echo json_encode($resp);
		} else {
			exit('no method allowed');
		}
	}

	function load_detail($id)
	{
		$where = [
			'ID' => $id
		];
		$data['status'] = 200;
		$data['data'] = $this->query->get_data_simple('NEW_TL_TDB', $where)->row();
		echo json_encode($data);
	}

	function delete_data($id)
	{


		$where = [
			'STATUS_DELETE' => 0,
			'ID' => $id,
		];

		$update = $this->query->update_data($this->table, $where, ['STATUS_DELETE' => 1]);

		if ($update) {
			$resp = [
				'status' => 200,
				'msg' => 'success delete data'
			];
		} else {

			$resp = [
				'status' => 400,
				'msg' => 'faIL delete data'
			];
		}

		echo json_encode($resp);
	}
}
