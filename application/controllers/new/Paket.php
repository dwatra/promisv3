
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require BASEPATH.'vendor\autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
class Paket extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->helper('intive');
		$this->load->library('form_validation');
		$this->load->model('Api', 'api');
	}

	public function index()
	{
		$data['page_title'] = 'PAKET | LIST ITEM PAKET';
		$data['content'] = $this->load->view('tool/paket', NULL, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function paket_detail($id_paket){
		$data['page_title'] = 'Paket | Edit Paket Detail';
		$data['detail']  = $this->query->get_data_simple('NEW_TL_PAKET' , ['ID' => $id_paket])->row();
		$paket	 = $this->query->get_data_simple('NEW_TL_PAKET' , ['ID !=' => $id_paket]);
		$data['paket'] = $paket ? $paket->result() : [];
		$data['content'] = $this->load->view('tool/paket_detail', $data, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function load_data_paket_detai($paket_id , $type){
		$select = "A.* , B.MERK , B.JENIS , B.KATEGORI";
		$tbl    = "NEW_TL_PAKET_DETAIL A";
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(A.ID),LOWER(A.TOOL_NAME),LOWER(A.TOOL_SPEC),LOWER(B.JENIS),LOWER(B.KATEGORI),LOWER(A.QTY)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		$join['data'][] = [
			'table' => 'NEW_TL_TOOL B',
			'join'  => 'A.TOOL_ID=B.ID' ,
			'type'  => 'INNER',
		];

		$where['data'][0]= [
			'column' => 'A.PAKET_ID',
			'param' => $paket_id
		];

		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null , NULL , null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , NULL , null);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {

					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="edit2('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="javascript:void(0)" onclick="delete2('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					$st .= '</ul></div>';

					if ($cant_update == '1') {
						$st = '';
					}

					// debug_query();

					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/tool/detail/'.$item->tool_id).'">'.$item->tool_name.'</a>',
						$item->tool_spec,
						$item->jenis,
						$item->kategori,
						$item->qty,
						$item->pic_name,
						$st,

						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		echo json_encode($response);


	}

	function copy_paket(){
		$table_detail = 'NEW_TL_PAKET_DETAIL';
		$to_paket_id   = $_POST['to_paket_id'];
		$from_paket_id = $_POST['from_paket_id'];

		$where_paket_from = [
				'PAKET_ID' => $from_paket_id
		];
		$where_paket_to = [
				'PAKET_ID' => $to_paket_id
		];
		/* DELETE THE ITEM IN PAKET /to */
		$this->query->delete_data($table_detail , $where_paket_to);

		$q = $this->query->get_data_simple($table_detail , $where_paket_from);
		$num_inserted= 0;
		if ($q) {
			foreach ($q->result() as $data) {
				$copyied_data = [
					'PAKET_ID'       => $_POST['to_paket_id'],
					'QTY'            => $data->qty,
					'TOOL_NAME'      => $data->tool_name,
					'TOOL_SPEC'      => $data->tool_spec,
					'TOOL_ID'        => $data->tool_id,
					'USER_ID'        => (int)$_SESSION['SESSION_APP_EBUDGETING']['user_id'],
					'TOOL_JENIS'     => $data->tool_jenis,
					'TOOL_KATEGORI'  => $data->tool_kategori,
				];
				$saved = $this->query->save_data($table_detail,$copyied_data);
				if ($saved) {
					$num_inserted++;
				}
			}
			$resp = [
				'status'       => 200,
				'msg'          => 'Succes copying paket item copyied : ' . $num_inserted,
				'row_affected' => $num_inserted
			];

		} else {
			$resp = [
				'status' => 400,
				'msg'  => 'Gagal copy paket karena : paket tersebut tidak memiliki item ',
				'row_affected' => 0
			];
		}

		echo json_encode($resp);


	}



	function delete($id_paket){
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			# code...
			$this->query->delete_data('NEW_TL_PAKET' , ['ID' => $id_paket]);
			$this->query->delete_data('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $id_paket]);
			$resp = ['status' => 200, 'msg' => 'success delete'];
			echo json_encode($resp);
		}
	}

	public function load_data(){

		$select = "*";
		$tbl = "NEW_TL_PAKET";
		$join['data'][] = [
			'table' => 'RAB_PROYEK',
			'join' => 'NEW_TL_PAKET.PR_ID=RAB_PROYEK.ID_PROYEK',
			'type' => 'LEFT',
		];
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PAKET.ID),LOWER(NEW_TL_PAKET.NAME),LOWER(RAB_PROYEK.NAMA_PROYEK),LOWER(NEW_TL_PAKET.KETERANGAN)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);

		$index_order = $this->input->get('order[0][column]');

		$order['data'][] = [
			'column' => $this->input->get('columns['.$this->input->get('order[0][column]').'][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null , null , $orderx);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null , null , $orderx);
		$response['data'] = [];
		if ($query) {
			$no = $this->input->get('start') ;
			// ? $this->input->get('length') + 1 : 0;
			foreach ($query->result() as $item) {
				if ($item->id > 0) {

					if ($item->pr_id > 0) {

						$li = '<li><a href="'.base_url('/new/tool/paket/'.$item->pr_id).'" class="waves-effect " ><span class="glyphicon glyphicon-search"></span> Detail</a></li>';

					} else {
						$li = '<li><a href="'.base_url('/new/paket/paket_detail/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Detail</a></li>';
						$li .= '<li><a onclick="delete_data_this('.$item->id.')" href="javascript:void(0)" class="waves-effect" ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					}

					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/paket/paket_detail/'.$item->id).'" class="waves-effect " >'.$item->name.'</a> </li>',
						$item->nama_proyek ? $item->nama_proyek : '-',
						$item->keterangan,
						'<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">

								'.$li.'


							</ul>
						</div>',
					);
					$no++;
				}
			}
		}
		$response['q'] = $this->db->last_query();
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		echo json_encode($response);
	}

	public function new(){
		if ($this->input->post()) {
			$this->form_validation->set_rules([
							['field' => 'name', 'label' => 'Name', 	'rules' => 'required|min_length[1]|max_length[50]|trim'],
							['field' => 'project', 'label' => 'project', 'rules' => 'required'],
							// ['field' => 'tool',	'label' => 'tool', 'rules' => 'required'],
				 		]);
	 		if ($this->form_validation->run() == FALSE) {
	 			$data['content'] = $this->load->view('tool/new_paket', ['error' => $this->form_validation->error_array(), 'projects' => $this->api->fetchapi([], 'GET', 'proyek')->data], TRUE);
	 			$this->load->view('layouts/dashboard', $data, FALSE);
	 		} else {
	 			//upload file
	 			$config['upload_path'] = './assets/upload/tool/excel/';
	 			$config['allowed_types'] = 'xls|xlsx';
	 			$config['file_name'] = 'fileupload-'.date('dmYHis');
	 			$config['overwrite'] = true;
	 			$config['max_size'] = 1024;
	 			if (empty($_FILES['tool']['name']) and $this->input->post('project') != 0) redirect(base_url('/new/tool/paket/'.$this->input->post('project')));
	 			$this->load->library('upload', $config);
	 			$this->upload->do_upload('tool');
	 			$uploaded = $this->upload->data();
	 			//insert to paket
	 			$paket = $this->query->save_data('NEW_TL_PAKET', ['NAME' => $this->input->post('name'), 'PR_ID' => $this->input->post('project')]);
	 			$paket = $this->query->get_data('*', 'NEW_TL_PAKET', NULL, NULL, 'ID DESC')->row()->id;
	 			//read data
	 			$reader  = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	 			$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
	 			$spreadsheet = $reader->load($uploaded['full_path']);
	 			$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
	 			foreach ($sheetData as $data) {
	 				if ($data['A'] and $data['C'] and $paket) {
	 					$save = ['TOOL_NAME' => $data['A'], 'QTY' => $data['C'], 'TOOL_SPEC' => $data['B'], 'PAKET_ID' => $paket];
		 				if ($this->query->save_data('NEW_TL_PAKET_DETAIL', $save)) {

		 				}else{
		 					$content['projects'] = $this->api->fetchapi([], 'GET', 'proyek')->data;
		 					$data['content'] = $this->load->view('tool/new_paket', $content, TRUE);
		 					$this->load->view('layouts/dashboard', $data, FALSE);
		 					// echo 'error insert';
		 					return;
		 				}
	 				}

	 			}
	 			if ($paket) {
	 				redirect(base_url('/new/paket'));
	 			}else{
	 				$data['content'] = $this->load->view('tool/new_paket', ['error' => $resp->message], TRUE);
	 				$this->load->view('layouts/dashboard', $data, FALSE);
	 			}
	 		}
		}else{

			$content['tools'] = $this->api->fetchapi([], 'GET', 'tool/list')->data;
			$data['content'] = $this->load->view('tool/new_paket', $content, TRUE);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	function add_new_item_in_paket_new(){

		$res = $this->api->fetchapi([], 'GET', 'tool/list')->data;

		foreach ($res as $x) {
			echo "<option value='".$x->ID.'xXx'.$x->NAME.'xXx'.$x->SPEK."'>";
				echo $x->NAME;
				echo ($x->SPEK != '') ? ' - '.$x->SPEK : '';
			echo "</option>";
		}
	}


	function add_paket($id= null){

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$data_paket = [
				'NAME'       => $_POST['name'],
				'KETERANGAN' => $_POST['keterangan'],
			];

			if ($id==null) {
				$data_paket['PR_ID'] = $_POST['project'];
				$insert = $this->query->save_data('NEW_TL_PAKET' , $data_paket);
				/* this is will be risk - cannot get id so i decided to insert first then get the max id.
				 * it will error if u insert in two users do the same time., and insert_batch cannot used bcause oracle too.
				 * so i must insert inside for each
				 * PLEASE SEE THE COMMENT CODE.
				 */
				$query  = $this->query->get_query('SELECT MAX(ID) as IDX FROM NEW_TL_PAKET')->row();
				$paket_id    = $query ? $query->idx : 1;
				$affected_rows = 0;
				if (isset($_POST['tool'])) {
					$data = [];
					// $sql = 'INSERT ALL ';
					for ($i=0; $i < count($_POST['tool']) ; $i++) {
						$tool_desc = $_POST['tool'][$i];
						$tool	 = explode('xXx', $tool_desc);
						$data = [
							'TOOL_NAME' => $tool[1],
							'TOOL_SPEC' => $tool[2],
							'QTY'       => 1,
							'TOOL_ID'   => (int)$tool[0],
							'PAKET_ID'  => (int)$paket_id,
							'USER_ID'   => (int)$_SESSION['SESSION_APP_EBUDGETING']['user_id'],
						];
						$this->query->save_data('NEW_TL_PAKET_DETAIL' , $data);
						if ($this->db->affected_rows() > 0) {
							$affected_rows++;
						}
					}
				}
				redirect( base_url() .'new/paket/');
			} else {
				$where = ['ID' => $id ];
				$insert = $this->query->update_data('NEW_TL_PAKET' , $where , $data_paket);
				if ($insert) {

					$resp = [
						'status' => 200 ,
						'msg' => 'Success update the paket'
					];


					echo json_encode($resp);
				}




			}


			// echo $this->db->last_query();
			// echo $affected_rows;

		}

	}


	function update_paket(){

		$paket_id = $_POST['paket_id'];
		$nama_paket = $_POST['nama_paket'];
		$data =[
			'NAME' => $nama_paket,
		];
		$this->query->update_data('NEW_TL_PAKET' , ['ID' => $paket_id] , $data);

		$resp  = [
			'status' => 201,
			'msg' => 'success update data'
		];

		echo json_encode($resp);



	}

	function realisasi($type){

		$count_error = 0;
		$error = [];

		if ($type == 'add') {
			$new_tl_paket =[
				'NAME'       => $_POST['paket_name'],
				'PR_ID'      => $_POST['proyek_id'],
				'KETERANGAN' => $_POST['keterangan'],
			];
			$s_paket = $this->query->save_data('NEW_TL_PAKET' , $new_tl_paket);
			if (!$s_paket) {
				$error['total'] = $count_error+1;
				$error['0']['msg'] = 'save to paket is not saved';
			}
			$get = $this->query->get_query('SELECT MAX(ID) AS max from NEW_TL_PAKET WHERE PR_ID = '.$_POST['proyek_id'])->row();
			$paket_id = $get->max;

			$get_data_proyek = $this->query->get_data_simple('RAB_PROYEK' , ['ID_PROYEK' => $_POST['proyek_id'] ]);
			$proyek_detail = $get_data_proyek->row();
			if ($proyek_detail->tgl_realisasi_mulai != null && $proyek_detail->tgl_rencana_mulai == null) {
				 $start_date = $proyek_detail->tgl_realisasi_mulai;
			} else if($proyek_detail->tgl_realisasi_mulai != null && $proyek_detail->tgl_rencana_mulai != null){
				 $start_date = $proyek_detail->tgl_realisasi_mulai;
			} else if($proyek_detail->tgl_realisasi_mulai == null && $proyek_detail->tgl_rencana_mulai != null){
				 $start_date = $proyek_detail->tgl_rencana_mulai;
			} else {
				 $start_date = date('d-m-Y');
			}

			$insert_data = [
				'START_DATE' => $start_date,
				'STATUS'     => 'menunggu persetujuan',
				'PR_ID'      => $_POST['proyek_id'],
				'CREATED_AT' => date('d-m-Y'),
				'ADMIN_ID'   => $_SESSION['SESSION_APP_EBUDGETING']['user_id'],
				'PAKET_ID'   => $paket_id
			];
			if (!$this->query->save_data('NEW_TL_PEMINJAMAN', $insert_data)) {
				$error['total'] = $count_error+1;
				$error['1']['msg'] = 'save to peminjaman is not saved';
			}
			$update_paket_detail = [
				'PAKET_ID' => $paket_id
			];
			$where =[
				'PAKET_ID' => 0,
				'USER_ID' => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
			];
			// update paket_detail
			$u_paket_detail = $this->query->update_data('NEW_TL_PAKET_DETAIL' , $where ,$update_paket_detail );
			if (!$u_paket_detail) {
				$error['total'] = $count_error+1;
				$error['1']['msg'] = 'update to paket detail is not saved';
			}
			if (count($error) > 0) {
				$json_msg = [
					'status' => 400,
					'msg'  => 'Something error ',
					'error' => $error
				];
			} else {
				$json_msg = [
					'status' => 200,
					'msg' => 'Update Berhasil , Realisasi telah di kirim'
				];
			}
		} else if($type == 'update') {
			$wh = [
				'PR_ID'  => $_POST['proyek_id'],
			];
			$update =[
				'STATUS'     => 'menunggu persetujuan',
				'ADMIN_ID'   =>  $_SESSION['SESSION_APP_EBUDGETING']['user_id'],
			];
			if ($this->query->update_data('NEW_TL_PEMINJAMAN' , $wh , $update)) {
				$json_msg = [
					'status' => 200,
					'msg'  => 'Something error ',
				];
			}
		}

		echo json_encode($json_msg);
		// echo "Terjadi Kesalahan";
	}




	function additem($paket_id){

		$data = $this->query->get_data_simple('NEW_TL_TOOL', ['ID' => $this->input->post('tool')]);

		$wh_special = [
			'USER_ID' => $_SESSION['SESSION_APP_EBUDGETING']['user_id'],
			'TOOL_ID' => $this->input->post('tool'),
			'PAKET_ID' => $paket_id
		];
		$check = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL' , $wh_special);
		$nid   = $this->input->post('nid');
		$nid   = explode('xXx',$nid);

		if ($check->num_rows() > 0) {
			$qty = $check->row();
			$id  = $qty->id;
			$qty = $qty->qty + $this->input->post('jml');

			$update_data =[
				'NID_PIC'  => $nid[0],
				'PIC_NAME' => $nid[1],
				'TOOL_JENIS'     => $this->input->post('jenis'),
				'TOOL_KATEGORI'  => $this->input->post('kat'),
			];

			if ($this->input->post('idx') != '0') {
				$update_data['QTY'] = $this->input->post('jml');

			} else {
				$update_data['QTY'] = $qty;
			}
			$wherex = [
				'ID' => $id
			];

			if ($this->query->update_data('NEW_TL_PAKET_DETAIL' , $wherex , $update_data)) {
				$resp = [
					'status' => 200,
					'msg' => 'berhasil update'
				];
			}

		} else {
			$id = $this->input->post('idx');
			$sql      = 'SELECT COUNT(*) AS JML FROM NEW_TL_TOOL_DETAIL WHERE KONDISI = 1 AND TOOL_ID ='.$this->input->post('tool');
			$tersedia = $this->query->get_query($sql);
			$jml      = $tersedia ? $tersedia->row()->jml : 0;

			if ($this->input->post('jml') > $jml) {
				$gacukup = 'rolling proyek';
			} else if($this->input->post('jml') < $jml){
				$gacukup = '';
			} else {
				$gacukup = '';
			}

			if ($data) {
				$insert = [
					'TOOL_NAME'      => $data->row()->name,
					'TOOL_SPEC'      => $data->row()->spek,
					'QTY'            => $this->input->post('jml'),
					'TOOL_ID'        => $this->input->post('tool'),
					'PAKET_ID'       => $paket_id,
					'USER_ID'        => $_SESSION['SESSION_APP_EBUDGETING']['user_id'],
					'IF_TIDAK_CUKUP' => $gacukup,
					'NID_PIC'        => $nid[0],
					'PIC_NAME'       => $nid[1],
					'TOOL_JENIS'     => $this->input->post('jenis'),
					'TOOL_KATEGORI'  => $this->input->post('kat'),
	 			];

	 			if ($id != 0) {
	 				$wherex = ['ID' => $id];
	 				if ($this->query->update_data('NEW_TL_PAKET_DETAIL',$wherex, $insert)) {
		 				$resp = [
							'status' => 200,
							'msg'    => 'Berhasil add'
		 				];
		 			}else{
		 				$resp = [
							'status' => 400,
							'msg'    => 'Bad Request'
						];
		 			}
	 			} else {
	 				if ($this->query->save_data('NEW_TL_PAKET_DETAIL', $insert)) {
		 				$resp = [
		 					'status' => 200,
		 					'msg' => 'Berhasil add'
		 				];
					}else{
		 				$resp = [
		 					'status' => 400,
		 					'msg' => 'Bad Request'
		 				];
		 			}
		 		}
	 		}
 		}
	 	echo json_encode($resp);

	}
	function edititem($id){
		// $this->form_validation->set_rules([
		// 	['field' => 'name', 'label' => 'Name', 	'rules' => 'required|min_length[1]|max_length[50]|trim'],
		// 	['field' => 'jml', 'label' => 'Jumlah', 'rules' => 'required'],
		// 	['field' => 'spek',	'label' => 'Spesifikasi', 'rules' => 'required'],
 	// 	]);
 		if ($this->form_validation->run() == FALSE) {
 			redirect($_SERVER['HTTP_REFERER']);
 		} else {
 			$update = [
 				'TOOL_NAME' => $this->input->post('name'),
 				'TOOL_SPEC' => $this->input->post('spek'),
 				'QTY'		=> $this->input->post('jml'),
 			];
 		}
		if ($this->query->update_data('NEW_TL_PAKET_DETAIL',['ID' => $id], $update)) {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function deleteitem($id){
		if ($this->query->delete_data('NEW_TL_PAKET_DETAIL',['ID' => $id])) {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function detail($id)
	{
		$content['paket']  = $this->query->get_data_simple('NEW_TL_PAKET', ['ID' => $id])->row();
		$content['detail'] = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $id])->result();
		$data['content']   = $this->load->view('tool/paket_detail', $content, TRUE);


		$this->load->view('layouts/dashboard', $data, FALSE);
	}







	function load_data_detail(){

		$select = "*";
		$tbl = "NEW_TL_PAKET";

		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PAKET.ID),LOWER(NEW_TL_PAKET.NAME),LOWER(RAB_PROYEK.NAMA_PROYEK),LOWER(NEW_TL_PAKET.TYPE),LOWER(NEW_TL_PAKET.KETERANGAN)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);

		$index_order = $this->input->get('order[0][column]');

		$order['data'][] = [
			'column' => $this->input->get('columns['.$this->input->get('order[0][column]').'][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null , null , $orderx);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null , null , $orderx);
		$response['data'] = [];
		if ($query) {
			$no = $this->input->get('start') ;
			// ? $this->input->get('length') + 1 : 0;
			foreach ($query->result() as $item) {
				if ($item->id > 0) {
					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/paket/detail/'.$item->id).'" class="waves-effect " >'.$item->name.'</a> </li>',
						$item->nama_proyek ? $item->nama_proyek : '-',
						'',
						$item->keterangan,
						'<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="'.base_url('/new/paket/edit/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="'.base_url('/new/paket/detail/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-pencil"></span> Detail</a> </li>
								<li><a href="'.base_url('/new/paket/delete/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>

							</ul>
						</div>',
					);
					$no++;
				}
			}
		}
		$response['q'] = $this->db->last_query();
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		// echo $this->db->last_query();
		// exit;
		echo json_encode($response);

	}

	// public function delete($id){
	// 	$this->data = $this->api->fetchapi(['id' => $id], 'DELETE', 'paket');
	// 	if ($this->data->status == 200) {
	// 		redirect(base_url('/new/paket'));
	// 	}else{
	// 		echo $this->data->message;
	// 	}
	// }

}

/* End of file Paket.php */
/* Location: ./application/controllers/new/Paket.php */
