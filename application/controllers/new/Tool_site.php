<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool_Site extends CI_Controller {
	private $data;

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->helper('intive');
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}
	//Master Tool

	public function index(){
		$this->load->model('M_tool', 'mtool');

		$order_by = ' B.STATUS DESC';
		
		$select  = 'A.ID_PROYEK , A.NAMA_PROYEK , A.TGL_RENCANA_MULAI,  A.TGL_RENCANA_SELESAI ,';
		$select .= 'B.* ';
		$from    = 'RAB_PROYEK A ';
		$join['data'][] = [
			'table' => 'NEW_TL_PEMINJAMAN B',
			'join'  => 'A.ID_PROYEK=B.PR_ID',
			'type'  => 'LEFT',
		];
		$where['data'][] = [
			'column' => 'to_char(A.TGL_RENCANA_MULAI ,\'DD-MM-YYYY\' ) >',
			'param' => '01-01-'.date('Y')
		];
		$where['data'][] = [
			'column' => 'B.STATUS',
			'param' => 'disetujui'
		];

		$proyeks = $this->query->get_data_complex($select,$from,  NULL, NULL, NULL, $join, $where, null , null , $order_by);


		$content['proyek']   = $proyeks ? $proyeks->result(): [];
	

		$data['content'] = $this->load->view('tool/transaction_site', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function detail($proyek_id){
		$this->load->model('M_tool', 'mtool');

		$storage = $this->query->get_data_simple('NEW_TL_STORAGE_ITEM' , ['PROYEK_ID' => $proyek_id]);
		$content['data_tool_storage'] = ($storage->num_rows() > 0) ? $storage->result() : [];
		$nota = $this->query->get_data_simple('NEW_TL_TRANSACTION_SITE' , ['PROYEK_ID' => $proyek_id]);
		$content['data_nota'] = ($nota->num_rows() > 0) ? $nota->result() : [];

		$data['content'] = $this->load->view('tool/transaction_site_detail', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

}

/* End of file Tool.php */
/* Location: ./application/controllers/new/Tool.php */
