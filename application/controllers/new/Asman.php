<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asman extends CI_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->library('form_validation');
		$this->load->helper('intive');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	function index(){
		exit('no direct script access allowed ');
	}

	function atur_paket($id){

		$this->load->model('M_tool', 'mtool');
		if ($id == null) {
			exit('no direct script acccess allowed');
		}
		$content['trx']          = $this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['PR_ID' => $id]) ? $this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['PR_ID' => $id])->row() : [];
		$content['kp2']          = $this->query->get_data_simple('MT_PEGAWAI', ['UNIT' => "KP2"])->result();
		$content['id_proyek']    = $id;
		$content['project']      = $this->proyek->GetByPk($id);
		$content['projects']     = $this->query->get_data_simple('RAB_PROYEK', null)->result();
		$data['page_title'] = 'Tool | Asman Atur Paket ';
		if ($content['trx']) {
			$content['paket'] = $this->query->get_data_simple('NEW_TL_PAKET' , ['ID !=' => $content['trx']->paket_id ])->result();
			$data['content']  = $this->load->view('tool/asman_rab_paket/rab_paket', $content, TRUE);
		} else {
			$data['content'] = $this->load->view('tool/asman_rab_paket/rab_paket_unavailable', $content, TRUE);
		}
		$this->load->view('layouts/dashboard', $data, FALSE);
	}


	public function accept($trx_id){
		$trx          = $this->query->get_data_simple('NEW_TL_PEMINJAMAN' , ['ID' => $trx_id])->row();
		$paket        = $this->query->get_data_simple('NEW_TL_PAKET' , ['ID' => (int)$trx->paket_id ])->row();
		$paket_detail = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $trx->paket_id ])->result();

		foreach ($paket_detail as $data_detail) {
			$quantity = $data_detail->qty;
			$items    = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL', ['TOOL_ID'=> $data_detail->tool_id , 'KONDISI' => 1]);
			$tersedia = ($items) ? $items->num_rows() : 0;

			if ($quantity <= $tersedia && $tersedia > 0 /* cukup */ ) {
				$row_inserted = 0;
				$sql = 'SELECT * FROM NEW_TL_TOOL_DETAIL WHERE TOOL_ID ='.$data_detail->tool_id.' AND KONDISI = 1 FETCH FIRST '.$quantity.' ROWS ONLY';
				$result = $this->query->get_query($sql);
				foreach ($result->result() as $item) {
					$insert = [
						'TOOL_ID'         => (int)$data_detail->tool_id,
						'TOOL_NAME'       => $data_detail->tool_name,
						'TOOL_SPEC'       => $data_detail->tool_spec,
						'TOOL_QTY'        => 1,
						'PAKET_DETAIL_ID' => (int)$data_detail->id,
						'TOOL_INVENTARIS' => $item->no_inventarisasi,
						'TOOL_DETAIL_ID'  => (int)$item->id
					];
					$s_paket_detail_item = $this->query->save_data('NEW_TL_PAKET_DETAIL_ITEM' , $insert);
					if ($s_paket_detail_item) {
						$update_item = [
							'KONDISI' => 3,
						];
						$this->query->update_data('NEW_TL_TOOL_DETAIL', ['ID' => $item->id],$update_item);
						$row_inserted++;
					}
				}
			} else { /* TIDAK CUKUP */
				$update_item = [
					'KONDISI' => 3,
				];
				$where_item = [
					'TOOL_ID' => $data_detail->tool_id,
					'KONDISI' => 1
				];
				// update the current available item into not avavilble bcause all item is needed
				$this->query->update_data('NEW_TL_TOOL_DETAIL',$where_item,$update_item);

				$items_exist = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL' , ['TOOL_ID'=>$data_detail->tool_id] );
				if (! $items_exist) {
					$jumlah = 1;
					# code...
					foreach ($items_exist as $x) {
						$insert = [
							'TOOL_ID'         => (int)$data_detail->tool_id,
							'TOOL_NAME'       => $data_detail->tool_name,
							'TOOL_SPEC'       => $data_detail->tool_spec,
							'TOOL_QTY'        => 1,
							'PAKET_DETAIL_ID' => (int)$data_detail->id,
							'TOOL_INVENTARIS' => $x->no_inventarisasi,
							'TOOL_DETAIL_ID'  => (int)$x->id
						];
						$s_paket_detail_item = $this->query->save_data('NEW_TL_PAKET_DETAIL_ITEM' , $insert);
						$jumlah++;
					}

				} else {
					$jumlah = 0;
				}
				// hitung yang di butuhkan dikurangi jumlah tersedia
				$belum_di_input = $quantity - $jumlah;


				for ($i=1; $i < $belum_di_input ; $i++) {
					$insert = [
						'TOOL_ID'         => (int)$data_detail->tool_id,
						'TOOL_NAME'       => $data_detail->tool_name,
						'TOOL_SPEC'       => $data_detail->tool_spec,
						'TOOL_QTY'        => 1,
						'PAKET_DETAIL_ID' => (int)$data_detail->id,
						'TOOL_INVENTARIS' => 'Belum ada',
						'TOOL_DETAIL_ID'  => 0,
					];
					$s_paket_detail_item = $this->query->save_data('NEW_TL_PAKET_DETAIL_ITEM' , $insert);

					if ($s_paket_detail_item) {
						$row_inserted++;
					}
				}
			}
		}
		$update_approve = [
			'APPROVED_BY'    => $_SESSION[SESSION_APP]['name'],
			'APPROVED_BY_ID' => $_SESSION[SESSION_APP]['user_id'],
			'STATUS'         => 'disetujui',
			'PIC_GENERAL'    => $_POST['picgnr'],
			'PIC_SPECIFIC'   => $_POST['picspe'],
			'PIC_SPECIAL'    => $_POST['picspc'],
		];
		$this->query->update_data('NEW_TL_PEMINJAMAN' , ['ID' => $trx_id ] , $update_approve );
		// $respx = $this->x_hitung_utilisasi($paket->id , $trx->pr_id);
		$resp = [
			'status'       => 200,
			'msg'          => 'Berhasil disetujui , Print Surat jalan ?',
			'total_insert' => $row_inserted,
			'utilitas'	   => $respx
		];

		$resp['android'] = $this->_generate_token_android($trx->id  , $trx->pr_id , $trx->paket_id );

		echo json_encode($resp);
	}

	function _generate_token_android($transaction_id , $proyek_id , $paket_id){
		$string    = $transaction_id.$proyek_id.$paket_id;
		$user_pass = md5($string);
		$user_name = strtoupper(substr($user_pass , 0 , 5));
		$data_to_new_user = [
			'PROYEK_ID'     => $proyek_id ,
			'PEMINJAMAN_ID' => $transaction_id ,
			'PAKET_ID'      => $paket_id,
			'USER_NAME'     => $user_name ,
			'USER_PASSWORD' => $user_pass
		];

		$save_ = $this->query->save_data('NEW_TL_USER' , $data_to_new_user);

	

		if ($save_) {
			$return  = 'Android user has been generated';
		} else {
			$return  = 'fail to generate android user';
		}

		return $return;
	}

	function x_hitung_utilisasi($paket_id , $pr_id){

		$pr         = $this->query->get_data_simple('RAB_PROYEK' , ['ID_PROYEK' => $pr_id ])->row();

		$date1 = date_create($pr->tgl_rencana_mulai);
		$date2 = date_create($pr->tgl_rencana_selesai);
		$diff  = date_diff($date1,$date2);

		$tgl_mulai  = date("d-m-Y", strtotime($start_date));
		$tgl_selesai= date("d-m-Y", strtotime($end_date));


		$total_days = $diff->days;

		$select = 'A.* , C.KATEGORI';
		$table  = 'NEW_TL_PAKET_DETAIL_ITEM A';

		$join['data'][] = [
			'table' => 'NEW_TL_PAKET_DETAIL B',
			'join'  => 'B.ID=A.PAKET_DETAIL_ID',
			'type'  => 'RIGHT',
 		];

 		$join['data'][] = [
			'table' => 'NEW_TL_TOOL C',
			'join'  => 'A.TOOL_ID=C.ID',
			'type'  => 'INNER',
 		];

		$where['data'][] = [
			'column' => 'B.PAKET_ID',
			'param'  => $paket_id
		];

		$query_items = $this->query->get_data_complex($select , $table, null , null , null , $join , $where);


		if ($query_items) {
			$tgl_inputx = date('d-m-Y', strtotime($pr->tgl_rencana_mulai));
			$tgl_inputy = date('d-m-Y' ,strtotime($pr->tgl_rencana_selesai));

			$item_effected   =0;
			$history_created =0;
			$item_no_id      =0;
			$total_items     = $query_items->num_rows();
			foreach ($query_items->result() as $value) {
				switch ($value->kategori) {
					case 'mekanik':
						$hours = 5;
						break;
					case 'instrument':
						$hours = 5;
						break;
					case 'listrik':
						$hours = 6;
						break;
					case 'predictive':
						$hours = 4;
						break;
					default:
						$hours = 0;
				}

				if ($hours != 0) {
					$utilitas = $total_days * $hours ;
				} else {
					$utilitas = 0;
				}

				if ($value->tool_detail_id > 0) {
					$q = ' UPDATE NEW_TL_TOOL_DETAIL SET UTILITAS = UTILITAS+'.$utilitas.' WHERE ID = '.$value->tool_detail_id;
					$run = $this->query->get_query($q);
					if ($run) {
						$item_effected++;
					}
					$insert_history = [
						'TOOL_ID'        => $value->tool_id,
						'TOOL_DETAIL_ID' => $value->tool_detail_id,
						'PROYEK_ID'      => $pr_id,
						'START_DATE'     => $tgl_inputx,
						'END_DATE'       => $tgl_inputy,
						'TOOL_NAME'      => $value->tool_name,
						'TOOL_SPEC'      => $value->tool_spec,
						'TOOL_INV'       => $value->tool_inventaris,
						'UTILITAS' 		 => $utilitas
					];

					$ins_his = $this->query->save_data('NEW_TL_TOOL_HISTORY' , $insert_history );
					if ($ins_his) {
						$history_created++;
					}
				} else {
					$item_no_id ++;
				}
			}
		}
		$response = [
			'total_items'     => 'Total items : '.$total_items,
			'item_effected'   => 'Item Utilitas Updated : '.$item_effected,
			'history_created' => 'History Created : '.$history_created,
			'item_no_effect'  => 'item utilitas cannot update because no item : '.$item_no_id,
 		];
 		return $response;
	}

	public function reject($id){
		$update = [
			'KET_DITOLAK' => $this->input->post('text'),
			'STATUS' => 'ditolak'
		];
		$this->query->update_data('NEW_TL_PEMINJAMAN' , ['ID' => $id ] , $update);
		$resp  = [
			'status' => 200,
			'msg' => 'success menolak'
		];
		echo json_encode($resp);
	}

	public function generate_surat_jalan($id){
		$sql = 'select * from
				new_tl_peminjaman a , new_tl_paket b
				where a.paket_id = b.id and a.pr_id = '.$id;
		$trx = $this->query->get_query($sql)->row();
		$data['paket_id']  = $trx->paket_id;
	  $data['transaksi'] = $trx;
		$data['pegawai']   = $this->query->get_data_simple('MT_PEGAWAI' , ['NID' => $trx->pic_general])->row();

		$select = 'A.* , B.JENIS , B.KATEGORI , B.SPEK , B.SATUAN';
		$table  = 'NEW_TL_PAKET_DETAIL A';
		$where['data'][] = [
			'column' => 'A.PAKET_ID' ,
			'param'  => $trx->paket_id
		];

		$where_a['data'][] = [
			'column' => 'A.PAKET_ID' ,
			'param'  => $trx->paket_id
		];

		$where_a['data'][] = [
			'column' => 'B.JENIS !=' ,
			'param'  => 'general'
		];

		$join['data'][] = [
			'table' => 'NEW_TL_TOOL B' ,
			'join' =>  'A.TOOL_ID=B.ID' ,
			'type' => 'INNER'
		];

		$paket_detail = $this->query->get_data_complex( $select , $table ,null,null,null,  $join, $where_a);
		$data['paket_detail'] = $paket_detail ? $paket_detail->result() : [];

		$where_b['data'][] = [
			'column' => 'A.PAKET_ID' ,
			'param'  => $trx->paket_id
		];

		$where_b['data'][] = [
			'column' => 'B.JENIS' ,
			'param'  => 'general'
		];
		$item_general = $this->query->get_data_complex( $select , $table ,null,null,null,  $join, $where_b);
		$data['item_general'] = $item_general ? $item_general->result() : [];


		// $data['pic_special_specific'] = $paket_detail;
		//
		$q                            = 'SELECT distinct PIC_NAME from NEW_TL_PAKET_DETAIL where paket_id = '.$trx->paket_id;
		//
		$person           = $this->query->get_query($q);
		// debug_query();
		$data['person'] = $person ? $person->result() : [];
		//
		// $data['pic_general']          = $item_general;

		// debug_return($data);

		// $this->load->view('pdf/pdf_surat_jalan', $data, FALSE);
		$this->load->library('pdf');
	  $this->pdf->setPaper('A4', 'potrait');
	  $this->pdf->filename = "surat_jalan.pdf";
	  $this->pdf->load_view('pdf/pdf_surat_jalan', $data);

	}

}
?>
