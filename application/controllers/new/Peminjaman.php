<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
		$this->load->model('M_tool', 'mtool');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	public function index(){
		
		$content['cart'] = $this->mtool->getCart();
		$data['content'] = $this->load->view('tool/peminjaman', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
		
	}


	public function savein(){
		$this->form_validation->set_rules([
			['field' => 'jml', 'label' => 'Jumlah', 'rules' => 'required'],
			['field' => 'tool',	'label' => 'Tool', 'rules' => 'required'],
			['field' => 'spek',	'label' => 'Spesifikasi', 'rules' => 'required'],
 		]);
 		if ($this->form_validation->run() == FALSE) {
 			redirect($_SERVER['HTTP_REFERER']);
 		} else {
 			$insert = [
 				'NAME' => $this->input->post('tool'),
 				'QTY' => $this->input->post('jml'),
 				'SPEK' => $this->input->post('spek')
 			];
 			// print_r($this->input->post());
 			if ($this->query->save_data('NEW_TL_CART_IE2', $insert)) {
 				redirect($_SERVER['HTTP_REFERER']);
 			}
 			
 		}
	}

	public function create(){
		//create peminjaman
		$insert_data = [
			'VENDOR'     => $this->input->post('vendor'),
			'START_DATE' => $this->input->post('start'),
			'END_DATE'   => $this->input->post('end'),
			'DIRECTION'  => 'keluar'
		];
		if ($this->query->save_data('NEW_TL_PEMINJAMAN_IE', $insert_data)) {
			$trx = $this->query->get_data('*','NEW_TL_PEMINJAMAN_IE',null,null,'ID DESC')->row();
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
		$req_items = $this->query->get_data_simple('NEW_TL_CART_IE', []);
		foreach ($req_items as $req_item) {
			for ($i = 0; $i < $req_item->qty; $i++) {
				$item = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL', ['TOOL_ID' => $req_item->tool_id])->row();
				$insert_data = ['PEMINJAMAN_ID' => $trx->id, 'TOOL_ID' => $item->id];
				if ($this->query->update_data('NEW_TL_TOOL_DETAIL' ,['ID' => $item->id], ['KONDISI' => 3]) and $this->query->save_data('NEW_TL_PEMINJAMAN_DETAIL' , $insert_data)) {
					$items[] = ['tool_name' => $req_item->tool_name, 'tool' => $item->id];
				}	
			}
		}
		$this->query->delete_data('NEW_TL_CART_IE', ['ID >' => 0]);
		redirect(base_url('new/tools/transaksi'));


	}

}

/* End of file Peminjaman.php */
/* Location: ./application/controllers/new/Peminjaman.php */