<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;

class Man extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	public function index()
	{
		$data['content'] = $this->load->view('man/master', NULL, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	public function load_data(){
		$select = "MT_PEGAWAI.NID,MT_PEGAWAI.NAMA,MT_PEGAWAI.JABATAN,MT_PEGAWAI.UNIT,MT_UNIT.TABLE_DESC as LOKASI, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI";
		$tbl = "MT_PEGAWAI";
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);
		$where_like['data'][] = [
			'column' => 'MT_PEGAWAI.NID,MT_PEGAWAI.NAMA,MT_PEGAWAI.JABATAN,MT_UNIT.TABLE_DESC, MT_JABATAN_PROYEK.NAMA, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA',
			'param'	 => $this->input->get('search[value]')
		];
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		$join['data'][] = [
			'table' => 'MT_UNIT',
			'join' => 'MT_PEGAWAI.UNIT=TRIM(MT_UNIT.TABLE_CODE)',
			'type' => 'left',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_MAN_JABATAN',
			'join' => 'MT_PEGAWAI.NID=TRIM(NEW_BIND_MAN_JABATAN.NID)',
			'type' => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'MT_JABATAN_PROYEK',
			'join' => 'NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK=MT_JABATAN_PROYEK.ID_JABATAN_PROYEK',
			'type' => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_KOMPETENSI',
			'join' => 'MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)',
			'type' => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'MT_SUMBER_PEGAWAI',
			'join' => 'MT_PEGAWAI.ID_SUMBER_PEGAWAI=MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI',
			'type' => 'LEFT',
		];
		// $where['data'] = [[
		// 	'column' => 'UNIT =',
		// 	'param' => 'KP2'
		// ]];
		$where2['data'] = [[
			'column' => 'KDJABATAN !=',
			'param' => 'TERM'
		],[
			'column' => 'KDJABATAN is null',
		]];
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, $where, $where2);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, $where2);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, $where2);
		$response['data'] = [];
		if ($query) {
			foreach ($query->result() as $item) {
				$response['data'][] = [
					$item->nid,
					'<a href="'.base_url('/new/man/detail/'.$item->nid).'">'.$item->nama.'</a>',
					$item->jabatan,
					$item->sumber_pegawai,
					$item->jabatan_proyek,
					// $item->kompetensi,
					$item->lokasi,
					'<div class="dropdown" style="display:inline">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
							<span class="glyphicon glyphicon-option-vertical"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
							<li><a href="'.base_url('/new/man/edit/'.str_replace('/', '~', $item->nid)).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
							<li><a href="'.base_url('/new/man/delete/'.str_replace('/', '~', $item->nid)).'" class="waves-effect " style="pointer-events: none;cursor: default;" ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>
						</ul>
					</div>'
				];
			}
		}
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	public function add(){
		if ($this->input->post()) {
			// print_r($this->input->post());
			$rules = [
				['field' => 'nama', 'label' => 'Nama','rules' => 'trim|required'],
				['field' => 'jabatan', 'label' => 'Jabatan','rules' => 'trim|required'],
				// ['field' => 'kdjabtan', 'label' => 'Kode Jabatan','rules' => 'trim|required'],
				['field' => 'jabatan_proyek', 'label' => 'Jabatan Proyek','rules' => 'required'],
				['field' => 'sumber_pegawai', 'label' => 'Sumber Pegawai','rules' => 'required'],
			];
			if ($this->input->post('sumber_pegawai') == '3') {
				$rules[] = ['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]|is_unique[MT_PEGAWAI.NID]'];
				$rules[] = ['field' => 'unit', 'label' => 'Unit','rules' => 'trim|required'];
			}else{
				$rules[] = ['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]'];
				$rules[] = ['field' => 'unit', 'label' => 'Unit','rules' => 'trim'];
			}
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$content['data'] = $this->input->post();
				$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
				$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
				$content['sumbers'] = $this->query->get_query("select * from MT_SUMBER_PEGAWAI where id_sumber_pegawai not in(4)")->result();
				$data['content'] = $this->load->view('man/new', $content, true);
				$data['error'] = $this->form_validation->error_array();
				$this->load->view('layouts/dashboard', $data, FALSE);
			} else {
				$insert_data = [
					'NAMA' => strtoupper($this->input->post('nama')),
					'NID'  => strtoupper($this->input->post('nid')),
					'UNIT' => strtoupper($this->input->post('unit')),
					'JABATAN' => strtoupper($this->input->post('jabatan')),
					// 'KDJABATAN' => strtoupper($this->input->post('kdjabtan'))
					'ID_SUMBER_PEGAWAI' => strtoupper($this->input->post('sumber_pegawai'))
				];
				if ($this->query->save_data('MT_PEGAWAI', $insert_data)) {
					$insert_data_bind = [
						'NID' => strtoupper($this->input->post('nid')),
						'ID_JABATAN_PROYEK'  => $this->input->post('jabatan_proyek'),
					];
					if ($this->query->save_data('NEW_BIND_MAN_JABATAN', $insert_data_bind)) {
						// $insert_data_bind_kompetensi = [
						// 	'NID' => strtoupper($this->input->post('nid')),
						// 	'KOMPETENSI'  => strtoupper($this->input->post('kompetensi')),
						// ];
						// if ($this->query->save_data('NEW_BIND_KOMPETENSI', $insert_data_bind_kompetensi)) {
							$_SESSION['success'] = 'Data berhasil disimpan';
							redirect(base_url().'new/man');
						// }
					}
				}else{
					$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
					$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
					$content['sumbers'] = $this->query->get_query("select * from MT_SUMBER_PEGAWAI where id_sumber_pegawai not in(4)")->result();
					$data['content'] = $this->load->view('man/new', $content, true);
					$data['error'] = "Terjadi Kesalahan, hubungi admin jika masalah berlanjut";
					$this->load->view('layouts/dashboard', $data, FALSE);
				} 
			}
		}else{
			$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
			$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
			$content['sumbers'] = $this->query->get_query("select * from MT_SUMBER_PEGAWAI where id_sumber_pegawai not in(4)")->result();
			$data['content'] = $this->load->view('man/new', $content, true);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}
	public function edit($id){
		$content['pegawai'] = $this->query->get_data_simple('MT_PEGAWAI', ['NID' => str_replace("~", "/", $id)])->row();
		$content['data'] = $this->query->get_data_simple('NEW_BIND_MAN_JABATAN', ['NID' => str_replace("~", "/", $id)])->row();
		$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
		if ($this->input->post()) {
			$rules = [
				// ['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]|is_unique[MT_PEGAWAI.NID]'],
				['field' => 'nama', 'label' => 'Nama','rules' => 'trim|required'],
				['field' => 'jabatan', 'label' => 'Jabatan','rules' => 'trim|required'],
				// ['field' => 'unit', 'label' => 'Unit','rules' => 'trim|required'],
				// ['field' => 'kdjabtan', 'label' => 'Kode Jabatan','rules' => 'trim|required'],
				['field' => 'jabatan_proyek', 'label' => 'Jabatan Proyek','rules' => 'required'],
				['field' => 'sumber_pegawai', 'label' => 'Sumber Pegawai','rules' => 'required'],
			];
			if ($id == $this->input->post('nid')) {
				$rules[] = ['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]'];
			}else{
				$rules[] = ['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]|is_unique[MT_PEGAWAI.NID]'];
			}
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
				$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
				$content['sumbers'] = $this->query->get_query("select * from MT_SUMBER_PEGAWAI where id_sumber_pegawai not in(4)")->result();
				$data['content'] = $this->load->view('man/edit_man', $content, true);
				$data['error'] = $this->form_validation->error_array();
				$this->load->view('layouts/dashboard', $data, FALSE);
			} else {
				$insert_data = [
					'NAMA' => strtoupper($this->input->post('nama')),
					'NID'  => strtoupper($this->input->post('nid')),
					// 'UNIT' => strtoupper($this->input->post('unit')),
					'JABATAN' => strtoupper($this->input->post('jabatan')),
					// 'KDJABATAN' => strtoupper($this->input->post('kdjabtan'))
					'ID_SUMBER_PEGAWAI' => strtoupper($this->input->post('sumber_pegawai'))
				];
				if ($this->db->update('MT_PEGAWAI', $insert_data, ['NID' => $id])) {
					$cek_data = $this->query->get_data_simple('NEW_BIND_MAN_JABATAN', ['NID' => str_replace("~", "/", $id)])->row();
					if(count($cek_data) > 0){
						$insert_data_bind = [
							'ID_JABATAN_PROYEK'  => $this->input->post('jabatan_proyek'),
						];
						$this->db->update('NEW_BIND_MAN_JABATAN', $insert_data_bind, ['NID' => $id]);
					}else{
						$insert_data_bind = [
							'NID' => $id,
							'ID_JABATAN_PROYEK'  => $this->input->post('jabatan_proyek'),
						];
						$this->query->save_data('NEW_BIND_MAN_JABATAN', $insert_data_bind);
					}

					// $cek_data_kompetensi = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
					// if(count($cek_data_kompetensi) > 0){
					// 	$insert_data_bind_kompetensi = [
					// 		'KOMPETENSI'  => strtoupper($this->input->post('kompetensi')),
					// 	];
					// 	$this->db->update('NEW_BIND_KOMPETENSI', $insert_data_bind_kompetensi, ['NID' => $id]);
					// }else{
					// 	$insert_data_bind_kompetensi = [
					// 		'NID' => $id,
					// 		'KOMPETENSI'  => strtoupper($this->input->post('kompetensi')),
					// 	];
					// 	$this->query->save_data('NEW_BIND_KOMPETENSI', $insert_data_bind_kompetensi);
					// }
					$_SESSION['success'] = 'Data berhasil disimpan';
					redirect(base_url().'new/man');
				}else{
					$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
					$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
					$content['sumbers'] = $this->query->get_query("select * from MT_SUMBER_PEGAWAI where id_sumber_pegawai not in(4)")->result();
					$data['content'] = $this->load->view('man/edit_man', $content, true);
					$data['error'] = '';
					$this->load->view('layouts/dashboard', $data, FALSE);
				} 
			}
		}else{
			$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
			$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
			$content['sumbers'] = $this->query->get_query("select * from MT_SUMBER_PEGAWAI where id_sumber_pegawai not in(4)")->result();
			$data['content'] = $this->load->view('man/edit_man', $content, true);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	function delete($id){
		$this->query->delete_data("MT_PEGAWAI", ['NID' => $id]);
		$this->query->delete_data("NEW_BIND_MAN_JABATAN", ['NID' => $id]);
		$this->query->delete_data("NEW_BIND_KOMPETENSI", ['NID' => $id]);
		$_SESSION['success'] = 'Data berhasil dihapus';
		redirect(base_url().'new/man');
	}

	public function detail($id){
		$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
			from MT_PEGAWAI
			left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
			left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
			left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
			left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
			left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
			where MT_PEGAWAI.NID = '".$id."'")->row();
		$content['history'] = $this->query->get_query("select DISTINCT RAB_PROYEK.NAMA_PROYEK, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD Mon YYYY') TGL_MULAI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI
			from NEW_BIND_MAN_JABATAN 
			join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
			join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
			join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
			join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
			join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
			where NEW_BIND_MAN_JABATAN.id = '".$content['data']->bind_jabatan_id."'")->result();
		$this->data['content'] = $this->load->view('man/detail_assign', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data, FALSE);
	}

	public function management(){
		$data['content'] = $this->load->view('man/bind', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function load_data_bind(){
		//preparation
		$distinct = 1;
		$select = "RAB_PROYEK.ID_PROYEK, NAMA_PROYEK, SUM(RAB_MANPOWER.JUMLAH) QTY, TGL_RENCANA_MULAI, TGL_RENCANA_SELESAI, MT_STATUS_PROYEK.NAMA STATUS";
		$tbl = "RAB_PROYEK";
		$join['data'][] = [
			'table' => 'MT_STATUS_PROYEK',
			'join' => 'RAB_PROYEK.ID_STATUS_PROYEK = MT_STATUS_PROYEK.ID_STATUS_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_PEKERJAAN',
			'join' => 'RAB_PROYEK.ID_PROYEK = RAB_PEKERJAAN.ID_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_RAB',
			'join' => 'RAB_PEKERJAAN.ID_PEKERJAAN= RAB_RAB.ID_PEKERJAAN',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_MANPOWER',
			'join' => 'RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB',
			'type' => 'INNER',
		];
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NAMA_PROYEK), LOWER(TGL_RENCANA_MULAI), LOWER(TGL_RENCANA_SELESAI), LOWER(MT_STATUS_PROYEK.NAMA)',
			'param'	 => $this->input->get('search[value]')
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		$group = "RAB_PROYEK.ID_PROYEK, NAMA_PROYEK, TGL_RENCANA_MULAI, TGL_RENCANA_SELESAI, MT_STATUS_PROYEK.NAMA";

		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null, $group, null, $distinct);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null, $group, null, $distinct);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null, $group, null, $distinct);
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') : 0;
			foreach ($resp as $item) {
				if ($item->id_proyek > 0) {
					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/panelbackend/rab_pekerjaan/index/'.-$item->id_proyek).'">'.$item->nama_proyek.'</a>',
						$item->qty,
						$item->tgl_rencana_mulai,
						$item->tgl_rencana_selesai,
						$item->status,
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		
		if($query_total){
			$response['recordsTotal'] = $query_total->num_rows();
		}else{
			$response['recordsTotal'] = 0;
		}
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}
	
	public function manage(){
		$data['content'] = $this->load->view('man/manage', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function load_data_manage(){
		//preparation
		$select = "MT_PEGAWAI.*, MT_UNIT.*, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_MAN_JABATAN.ID AS BIND_ID";
		$tbl = "MT_PEGAWAI";
		$join['data'][] = [
			'table' => 'MT_UNIT',
			'join' => 'MT_PEGAWAI.UNIT=TRIM(MT_UNIT.TABLE_CODE)',
			'type' => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_MAN_JABATAN',
			'join' => 'MT_PEGAWAI.NID=NEW_BIND_MAN_JABATAN.NID',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'MT_JABATAN_PROYEK',
			'join' => 'NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK=MT_JABATAN_PROYEK.ID_JABATAN_PROYEK',
			'type' => 'INNER',
		];
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);
		// $where['data'][] = array(
		// 	'column' => 'NEW_BIND_MAN_JABATAN.ID',
		// 	'param'	 => 'IS NOT NULL'
		// );
		$where['data'] = [[
			'column' => 'KDJABATAN !=',
			'param' => 'TERM'
		]
		// ,[
		// 	'column' => 'UNIT =',
		// 	'param' => 'KP2'
		// ]
		];

		$where_like['data'][] = array(
			'column' => 'MT_PEGAWAI.NAMA,MT_PEGAWAI.NID,MT_UNIT.TABLE_DESC,MT_PEGAWAI.JABATAN,MT_JABATAN_PROYEK.NAMA',
			'param'	 => $this->input->get('search[value]')
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		// $query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null);
		// $query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null);
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->bind_id > 0) {
					$proyek_now = $this->query->get_query("select DISTINCT RAB_PROYEK.NAMA_PROYEK, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD Mon YYYY') TGL_MULAI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI
						from NEW_BIND_MAN_JABATAN 
						join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
						join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
						join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
						join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
						join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
						where SYSDATE BETWEEN RAB_PROYEK.TGL_REALISASI_MULAI AND RAB_PROYEK.TGL_REALISASI_SELESAI
						and NEW_BIND_MANPOWER.NID = '".$item->nid."'")->row();
					if($proyek_now){
						$status_proyek = $proyek_now->nama_proyek;
					}else{
						$status_proyek = 'AVAILABLE';
					}
					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/man/detail_assign/'.$item->bind_id).'">'.$item->nama.'</a>',
						$item->nid,
						$item->table_desc,
						$status_proyek,
						$item->jabatan,
						$item->jabatan_proyek,
						/*'<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="'.base_url('/new/man/edit_assign/'.$item->bind_id).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="'.base_url('/new/man/delete_assign/'.$item->bind_id).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>
							</ul>
						</div>'*/
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		
		if($query_total){
			$response['recordsTotal'] = $query_total->num_rows();
		}else{
			$response['recordsTotal'] = 0;
		}
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	public function assign(){
		if ($this->input->post()) {
			// print_r($this->input->post());
			$this->form_validation->set_rules([
				['field' => 'nama', 'label' => 'Nama','rules' => 'trim|required|max_length[10]'],
				['field' => 'jabatan_proyek', 'label' => 'Jabatan Proyek','rules' => 'required'],
			]);
			if ($this->form_validation->run() == FALSE) {
				$content['namas'] = $this->query->get_data_simple('MT_PEGAWAI', NULL)->result();
				$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
				$data['content'] = $this->load->view('man/assign', $content, true);
				$data['error'] = $this->form_validation->error_array();
				$this->load->view('layouts/dashboard', $data, FALSE);
			} else {
				$insert_data = [
					'NID' => strtoupper($this->input->post('nama')),
					'ID_JABATAN_PROYEK'  => $this->input->post('jabatan_proyek'),
				];
				if ($this->query->save_data('NEW_BIND_MAN_JABATAN', $insert_data)) {
					$_SESSION['success'] = 'Data berhasil disimpan';
					redirect(base_url().'new/man/manage');
				}else{
					$content['namas'] = $this->query->get_data_simple('MT_PEGAWAI', NULL)->result();
					$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
					$data['content'] = $this->load->view('man/assign', $content, true);
					$data['error'] = "Terjadi Kesalahan, hubungi admin jika masalah berlanjut";
					$this->load->view('layouts/dashboard', $data, FALSE);
				} 
			}
		}else{
			$content['namas'] = $this->query->get_data_simple('MT_PEGAWAI', NULL)->result();
			$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
			$data['content'] = $this->load->view('man/assign', $content, true);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	public function edit_assign($id){
		$content['data'] = $this->query->get_query("select ID_JABATAN_PROYEK, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN 
			from NEW_BIND_MAN_JABATAN 
			join MT_PEGAWAI on NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
			left join MT_UNIT on MT_PEGAWAI.UNIT=TRIM(MT_UNIT.TABLE_CODE) 
			where NEW_BIND_MAN_JABATAN.id = '".$id."'")->row();
		if ($this->input->post()) {
			$rules = [
				['field' => 'jabatan_proyek', 'label' => 'Jabatan Proyek','rules' => 'required'],
			];
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
				$data['content'] = $this->load->view('man/edit_assign', $content, true);
				$data['error'] = $this->form_validation->error_array();
				$this->load->view('layouts/dashboard', $data, FALSE);
			} else {
				$insert_data = [
					'ID_JABATAN_PROYEK'  => $this->input->post('jabatan_proyek'),
				];
				if ($this->db->update('NEW_BIND_MAN_JABATAN', $insert_data, ['ID' => $id])) {
					$_SESSION['success'] = 'Data berhasil diubah';
					redirect(base_url().'new/man/manage');
				}else{
					$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
					$data['content'] = $this->load->view('man/edit_assign', $content, true);
					$data['error'] = '';
					$this->load->view('layouts/dashboard', $data, FALSE);
				} 
			}
		}else{
			$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
			$data['content'] = $this->load->view('man/edit_assign', $content, true);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	function delete_assign($id){
		$this->query->delete_data("NEW_BIND_MAN_JABATAN", ['ID' => $id]);
		$_SESSION['success'] = 'Data berhasil dihapus';
		redirect(base_url().'new/man/manage');
	}

	public function detail_assign($id){
		$content['data'] = $this->query->get_query("select NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK
			from NEW_BIND_MAN_JABATAN 
			join MT_PEGAWAI on NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
			left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
			join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
			where NEW_BIND_MAN_JABATAN.id = '".$id."'")->row();
		$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $content['data']->nid)])->row();
		$content['history'] = $this->query->get_query("select DISTINCT RAB_PROYEK.NAMA_PROYEK, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD Mon YYYY') TGL_MULAI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI
			from NEW_BIND_MAN_JABATAN 
			join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
			join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
			join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
			join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
			join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
			where NEW_BIND_MAN_JABATAN.id = '".$id."'")->result();
		$this->data['content'] = $this->load->view('man/detail_assign', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data, FALSE);
	}

	public function getJson() {
        $nid = $this->input->get('nid', TRUE);
        $nama = $this->query->get_query("select * from MT_PEGAWAI join MT_UNIT on MT_PEGAWAI.UNIT=TRIM(MT_UNIT.TABLE_CODE) 
        	where (kdjabatan != 'TERM' or kdjabatan is null)
        	-- and unit = 'KP2'
        	and nid = '".$nid."'")->result();
        $response = [
            'success' => TRUE,
            'data' => $nama
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

	public function getJsonNid() {
        $nid = $this->input->get('nid', TRUE);
        $unit = strtoupper($this->input->get('unit', TRUE));
        $where = '';
        if($unit != ''){
        	$where = ' and unit = \''.$unit.'\'';
        }
        $nama = $this->query->get_query("select * from MT_PEGAWAI join MT_UNIT on MT_PEGAWAI.UNIT=TRIM(MT_UNIT.TABLE_CODE) 
        	where (kdjabatan != 'TERM' or kdjabatan is null)
        	-- and unit = 'KP2'
        	".$where."
        	and nid like '%".$nid."%' ")->result();
        $response = [
            'success' => TRUE,
            'data' => $nama
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

	public function getJsonName() {
        $name = strtoupper($this->input->get('name', TRUE));
        $unit = strtoupper($this->input->get('unit', TRUE));
        $where = '';
        if($unit != ''){
        	$where = ' and unit = \''.$unit.'\'';
        }
        $nama = $this->query->get_query("select NID, NAMA from MT_PEGAWAI 
        	where (kdjabatan != 'TERM' or kdjabatan is null)
        	-- and unit = 'KP2'
        	".$where."
        	and NAMA like '%".$name."%'")->result();
        $response = [
            'success' => TRUE,
            'data' => $nama
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

	public function getJsonRabManPower() {
        $id_jabatan_proyek = $this->input->get('id_jabatan_proyek', TRUE);
        $where = '';
        if($id_jabatan_proyek != ''){
        	$where = "AND NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = '".$id_jabatan_proyek."'";
        }
        $nama = $this->conn->GetArray("select MT_PEGAWAI.nid, MT_PEGAWAI.nama 
				from NEW_BIND_MAN_JABATAN 
				join MT_PEGAWAI on NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
				where (kdjabatan != 'TERM' or kdjabatan is null)
        		-- and unit = 'KP2' 
        		".$where."
				order by nid");
        $response = [
            'success' => TRUE,
            'data' => $nama
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
	
	public function report(){
    	for ($i=1; $i <= 12 ; $i++) {
    		$manpower = $this->query->get_query("
    			SELECT NVL(SUM(RAB_MANPOWER.JUMLAH),0) qty, NVL(SUM(BIND.QTY_BIND),0) qty_real
				FROM RAB_MANPOWER
				join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
				join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
				join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
				left join (
					SELECT COUNT(NEW_BIND_MANPOWER.ID) QTY_BIND, NEW_BIND_MANPOWER.ID_MAN_POWER
					FROM NEW_BIND_MANPOWER
					GROUP BY NEW_BIND_MANPOWER.ID_MAN_POWER
				) bind on RAB_MANPOWER.ID_MANPOWER = bind.ID_MAN_POWER
				where $i BETWEEN TO_CHAR(RAB_PROYEK.TGL_RENCANA_MULAI, 'mm') and TO_CHAR(RAB_PROYEK.TGL_RENCANA_SELESAI, 'mm')
    		")->row();
    		$kebutuhan[] = $manpower->qty;
    		$realisasi[] = $manpower->qty_real;
    	}
    	$dalam_proyek = $this->query->get_query("
			select 
			count(NEW_BIND_MANPOWER.NID) x
				from NEW_BIND_MAN_JABATAN 
				join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
				join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
				join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
				join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
				join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
				where SYSDATE BETWEEN RAB_PROYEK.TGL_REALISASI_MULAI AND RAB_PROYEK.TGL_REALISASI_SELESAI
		")->row();
    	$available = $this->query->get_query("
			select count(id) x
			from NEW_BIND_MANPOWER
			where id not in(
			select 
			NEW_BIND_MANPOWER.ID
				from NEW_BIND_MAN_JABATAN 
				join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
				join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
				join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
				join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
				join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
				where SYSDATE BETWEEN RAB_PROYEK.TGL_REALISASI_MULAI AND RAB_PROYEK.TGL_REALISASI_SELESAI
				)
		")->row();
    	$content['dataChart'] = [];
        $content['dataChart']['kebutuhan'] = $kebutuhan;
        $content['dataChart']['realisasi'] = $realisasi;
        $content['dataChart']['available'] = $available->x;
        $content['dataChart']['dalam_proyek'] = $dalam_proyek->x;
		$data['content'] = $this->load->view('man/report', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

    public function excel($periode=0){
    	$bulan = array (
    		'01'=>'Januari',
			'02'=>'Februari',
			'03'=>'Maret',
			'04'=>'April',
			'05'=>'Mei',
			'06'=>'Juni',
			'07'=>'Juli',
			'08'=>'Agustus',
			'09'=>'September',
			'10'=>'Oktober',
			'11'=>'November',
			'12'=>'Desember'
		);
		$holiday = json_decode(file_get_contents("https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json"),true);
        $periode_next = str_pad($periode+1, 2, '0', STR_PAD_LEFT);
        if($periode=='12'){
        	$periode_next = '01';
        }
        // $month = date('m');
        $month = $periode;
        $year = date('Y');
        $month_name = $bulan[$month];
        $count_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        // $next_month = date($periode_next,strtotime('first day of +1 month'));
        $next_month = $periode_next;
        $next_year = date('Y', strtotime('+1 year'));
        $next_month_name = $bulan[$next_month];
        $count_days_next = cal_days_in_month(CAL_GREGORIAN, $next_month, $next_month == 1 ? $next_year : $year);
    	$workbook = IOFactory::load('assets/excel/monitoring_manual_karyawan.xlsx');
        $cols = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK');
        $style=array(
            'borders' => array(
                'top' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'left' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'right' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            )
        );
        $styleCenter = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
        $styleYellow=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFffff00',
		        ],
		    ],
        );
        $styleBlue=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FF00b0f0',
		        ],
		    ],
        );
        $styleRed=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFff0000',
		        ],
		    ],
        );
        $styleOrange=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFffd966',
		        ],
		    ],
        );
        $spreadsheet = $workbook->getSheetByName('Man Power');
        $spreadsheet->setCellValue('O9', $year);
        $spreadsheet->setCellValue('T9', $next_month == '01' ? $next_year : $year);
        $spreadsheet->setCellValue('O10', $month_name);
        $spreadsheet->setCellValue('T10', $next_month_name);
        $tgl = 1;
        for($i=27;$i<27+$count_days;$i++){
        	if($i==27){
        		$range1 = $cols[$i].'11';
        		$spreadsheet->setCellValue($range1, $month_name." ".$year);
        	}
    		$spreadsheet->getStyle($cols[$i].'11')->applyFromArray($style);
    		$spreadsheet->getStyle($cols[$i].'11')->applyFromArray($styleYellow);
        	$range2 = $cols[$i].'11';
        	$spreadsheet->setCellValue($cols[$i].'12', $tgl++);
        	$spreadsheet->getStyle($cols[$i].'12')->applyFromArray($style);
        	$spreadsheet->getStyle($cols[$i].'12')->applyFromArray($styleYellow);
            $day = date('D', strtotime(($tgl-1).'-'.$month.'-'.$year));
            $cekDay = date('Ymd', strtotime(($tgl-1).'-'.$month.'-'.$year));
            if($day == 'Sat' || $day == 'Sun' || isset($holiday[$cekDay])){
            	$spreadsheet->getStyle($cols[$i].'12')->applyFromArray($styleRed);
            }
        }
        $spreadsheet->mergeCells("$range1:$range2");
        $spreadsheet->getStyle("$range1:$range2")->applyFromArray($styleCenter);
        $tgl = 1;
        for($i=27+$count_days;$i<27+$count_days+$count_days_next;$i++){
        	if($i==27+$count_days){
        		$range1 = $cols[$i].'11';
        		$tahun = $next_month == '01' ? $next_year : $year;
        		$spreadsheet->setCellValue($range1, $next_month_name." ".$tahun);
        	}
    		$spreadsheet->getStyle($cols[$i].'11')->applyFromArray($style);
    		$spreadsheet->getStyle($cols[$i].'11')->applyFromArray($styleYellow);
        	$range2 = $cols[$i].'11';
        	$spreadsheet->setCellValue($cols[$i].'12', $tgl++);
        	$spreadsheet->getStyle($cols[$i].'12')->applyFromArray($style);
        	$spreadsheet->getStyle($cols[$i].'12')->applyFromArray($styleYellow);
            $day = date('D', strtotime(($tgl-1).'-'.$next_month.'-'.$tahun));
            $cekDay = date('Ymd', strtotime(($tgl-1).'-'.$next_month.'-'.$tahun));
            if($day == 'Sat' || $day == 'Sun' || isset($holiday[$cekDay])){
            	$spreadsheet->getStyle($cols[$i].'12')->applyFromArray($styleRed);
            }
        }
        $tahun = $next_month == '01' ? $next_year : $year;
        $spreadsheet->mergeCells("$range1:$range2");
        $spreadsheet->getStyle("$range1:$range2")->applyFromArray($styleCenter);
        $no = 1;
        $numrow = 13;
        $datas = $this->query->get_query("
        			select MT_PEGAWAI.nid, MT_PEGAWAI.nama, MT_PEGAWAI.jabatan, MT_JABATAN_PROYEK.NAMA jabatan_proyek
        				, NEW_BIND_KOMPETENSI.INTI, NEW_BIND_KOMPETENSI.TAMBAHAN, NEW_BIND_KOMPETENSI.TAHUN_MASUK, NEW_BIND_KOMPETENSI.GRADE, NEW_BIND_KOMPETENSI.SKILL, NEW_BIND_KOMPETENSI.KOMPETENSI
					from NEW_BIND_MAN_JABATAN 
					join MT_PEGAWAI on NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
					join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
					left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID = NEW_BIND_KOMPETENSI.NID
					order by nama
				")->result();
        foreach($datas as $data){
            $spreadsheet->setCellValue('A'.$numrow, $no++);
            $spreadsheet->setCellValue('B'.$numrow, $data->nama);
            $spreadsheet->setCellValue('C'.$numrow, $data->nid);
            $spreadsheet->setCellValue('F'.$numrow, $data->jabatan);
            $spreadsheet->setCellValue('G'.$numrow, $data->jabatan_proyek);
            $spreadsheet->setCellValue('H'.$numrow, $data->inti);
            $spreadsheet->setCellValue('I'.$numrow, $data->tambahan);
            $spreadsheet->setCellValue('J'.$numrow, $data->tahun_masuk);
            $spreadsheet->setCellValue('K'.$numrow, $data->grade);
            $spreadsheet->setCellValue('L'.$numrow, $data->skill);
            $spreadsheet->setCellValue('M'.$numrow, $data->kompetensi);
            $proyek = $this->query->get_query("
        			select NEW_BIND_MAN_JABATAN.NID, MT_PEGAWAI.nama ,RAB_PROYEK.NAMA_PROYEK, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD Mon YYYY') TGL_MULAI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI, RAB_PROYEK.TGL_REALISASI_SELESAI-RAB_PROYEK.TGL_REALISASI_MULAI DURASI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD-MM-YYYY') TGL_MULAI_DATE, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI_DATE
					from NEW_BIND_MAN_JABATAN 
					join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
					join MT_PEGAWAI on NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
					join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
					join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
					join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
					join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
					where '".$year."-".$month."' between TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'YYYY-MM') and TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'YYYY-MM')
					--where TO_DATE('".$year."-".$month."-01', 'YYYY-MM-DD') between RAB_PROYEK.TGL_REALISASI_MULAI and RAB_PROYEK.TGL_REALISASI_SELESAI
					--where RAB_PROYEK.TGL_REALISASI_MULAI between TO_DATE('".$year."-".$month."-01', 'YYYY-MM-DD') and TO_DATE('".$year."-".$month."-".$count_days."', 'YYYY-MM-DD')
					and NEW_BIND_MAN_JABATAN.NID = '".$data->nid."'
				")->row();
            $spreadsheet->setCellValue('O'.$numrow, $proyek->nama_proyek);
            $spreadsheet->setCellValue('P'.$numrow, $proyek->tgl_mulai);
            $spreadsheet->setCellValue('Q'.$numrow, $proyek->tgl_selesai);
            // $spreadsheet->setCellValue('R'.$numrow, $proyek->durasi);
            // $spreadsheet->setCellValue('S'.$numrow, ((int)$proyek->durasi*8));
            if($proyek->nama_proyek != ''){
            	$spreadsheet->getStyle('O'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('P'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('Q'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('R'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('S'.$numrow)->applyFromArray($styleOrange);
            }
            $proyek_next = $this->query->get_query("
        			select NEW_BIND_MAN_JABATAN.NID, MT_PEGAWAI.nama ,RAB_PROYEK.NAMA_PROYEK, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD Mon YYYY') TGL_MULAI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI, RAB_PROYEK.TGL_REALISASI_SELESAI-RAB_PROYEK.TGL_REALISASI_MULAI DURASI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD-MM-YYYY') TGL_MULAI_DATE, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI_DATE
					from NEW_BIND_MAN_JABATAN 
					join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
					join MT_PEGAWAI on NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
					join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
					join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
					join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
					join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
					where '".$tahun."-".$next_month."' between TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'YYYY-MM') and TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'YYYY-MM')
					--where TO_DATE('".$tahun."-".$next_month."-01', 'YYYY-MM-DD') between RAB_PROYEK.TGL_REALISASI_MULAI and RAB_PROYEK.TGL_REALISASI_SELESAI
					--where RAB_PROYEK.TGL_REALISASI_MULAI between TO_DATE('".$tahun."-".$next_month."-01', 'YYYY-MM-DD') and TO_DATE('".$tahun."-".$next_month."-".$count_days_next."', 'YYYY-MM-DD')
					and NEW_BIND_MAN_JABATAN.NID = '".$data->nid."'
				")->row();
            $spreadsheet->setCellValue('T'.$numrow, $proyek_next->nama_proyek);
            $spreadsheet->setCellValue('U'.$numrow, $proyek_next->tgl_mulai);
            $spreadsheet->setCellValue('V'.$numrow, $proyek_next->tgl_selesai);
            // $spreadsheet->setCellValue('W'.$numrow, $proyek_next->durasi);
            // $spreadsheet->setCellValue('X'.$numrow, ((int)$proyek_next->durasi*8));
            if($proyek_next->nama_proyek != ''){
            	$spreadsheet->getStyle('T'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('U'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('V'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('W'.$numrow)->applyFromArray($styleOrange);
            	$spreadsheet->getStyle('X'.$numrow)->applyFromArray($styleOrange);
            }
            // $spreadsheet->setCellValue('N'.$numrow, ((int)$proyek->durasi*8)+((int)$proyek_next->durasi*8));
            $spreadsheet->setCellValue('AA'.$numrow, '0');
            for($i=0;$i<27+$count_days+$count_days_next;$i++){
            	if($cols[$i] != 'Z'){
                	$spreadsheet->getStyle($cols[$i].$numrow)->applyFromArray($style);
            	}
            }
            $tgl = 1;
            $biru = 0;
            $cekProyekDayStart = date('Ymd', strtotime($proyek->tgl_mulai_date));
	        $cekProyekDayEnd = date('Ymd', strtotime($proyek->tgl_selesai_date));
            for($i=27;$i<27+$count_days;$i++){
	            $cekDay = date('Ymd', strtotime(($tgl).'-'.$month.'-'.$year));
        		$day = date('D', strtotime(($tgl).'-'.$month.'-'.$year));
            	if($day == 'Sat' || $day == 'Sun' || isset($holiday[$cekDay])){
	            	$spreadsheet->getStyle($cols[$i].$numrow)->applyFromArray($styleRed);
	            }
	            if(($cekDay >= $cekProyekDayStart) && ($cekDay <= $cekProyekDayEnd)){
	            	$spreadsheet->getStyle($cols[$i].$numrow)->applyFromArray($styleBlue);
	            	$biru++;
	            }
	            $tgl++;
        	}
        	$biru1 = ((int)$biru*8);
            $spreadsheet->setCellValue('R'.$numrow, $biru);
            $spreadsheet->setCellValue('S'.$numrow, ((int)$biru*8));
            $tgl = 1;
            $biru = 0;
            $tahun = $next_month == '01' ? $next_year : $year;
            $cekNextProyekDayStart = date('Ymd', strtotime($proyek_next->tgl_mulai_date));
	        $cekNextProyekDayEnd = date('Ymd', strtotime($proyek_next->tgl_selesai_date));
        	for($i=27+$count_days;$i<27+$count_days+$count_days_next;$i++){
        		$cek_biru = 0;
	            $cekDay = date('Ymd', strtotime(($tgl).'-'.$next_month.'-'.$tahun));
        		$day = date('D', strtotime(($tgl).'-'.$next_month.'-'.$tahun));
            	if($day == 'Sat' || $day == 'Sun' || isset($holiday[$cekDay])){
	            	$spreadsheet->getStyle($cols[$i].$numrow)->applyFromArray($styleRed);
	            }
	            if(($cekDay >= $cekProyekDayStart) && ($cekDay <= $cekProyekDayEnd)){
	            	$spreadsheet->getStyle($cols[$i].$numrow)->applyFromArray($styleBlue);
	            	$biru++;
	            	$cek_biru++;
	            }
	            if(($cekDay >= $cekNextProyekDayStart) && ($cekDay <= $cekNextProyekDayEnd)){
	            	$spreadsheet->getStyle($cols[$i].$numrow)->applyFromArray($styleBlue);
	            	if($cek_biru == 0){
	            		$biru++;
	            	}
	            }
	            $tgl++;
        	}
        	$biru2 = ((int)$biru*8);
            $spreadsheet->setCellValue('W'.$numrow, $biru);
            $spreadsheet->setCellValue('X'.$numrow, ((int)$biru*8));
            $spreadsheet->setCellValue('N'.$numrow, $biru1+$biru2);
            $numrow++;
        }
        $writer = IOFactory::createWriter($workbook, 'Xlsx');
        header('Content-Disposition: attachment; filename="Report Man Power.xlsx"');
        $writer->save('php://output');
    }

	function load_data_persebaran($tgl='', $tgl_selesai=''){
		//preparation
		$distinct = 1;
		$select = "RAB_PROYEK.ID_PROYEK, NAMA_PROYEK, SUM(RAB_MANPOWER.JUMLAH) QTY, TGL_RENCANA_MULAI, TGL_RENCANA_SELESAI, MT_STATUS_PROYEK.NAMA STATUS";
		$tbl = "RAB_PROYEK";
		$join['data'][] = [
			'table' => 'MT_STATUS_PROYEK',
			'join' => 'RAB_PROYEK.ID_STATUS_PROYEK = MT_STATUS_PROYEK.ID_STATUS_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_PEKERJAAN',
			'join' => 'RAB_PROYEK.ID_PROYEK = RAB_PEKERJAAN.ID_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_RAB',
			'join' => 'RAB_PEKERJAAN.ID_PEKERJAAN= RAB_RAB.ID_PEKERJAAN',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_MANPOWER',
			'join' => 'RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_MANPOWER',
			'join' => 'NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER',
			'type' => 'LEFT',
		];
		$where = null;
		if ($tgl != '') {
			$where['data'][] = [
				'column' => 'TO_CHAR(TGL_RENCANA_MULAI, \'DD-MM-YYYY\') >= ',
				'param'  => $tgl
			];
		}
		if ($tgl_selesai != '') {
			$where['data'][] = [
				'column' => 'TO_CHAR(TGL_RENCANA_SELESAI, \'DD-MM-YYYY\') <= ',
				'param'  => $tgl_selesai
			];
		}
		$having = "SUM(RAB_MANPOWER.JUMLAH) = count(NEW_BIND_MANPOWER.NID)";
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NAMA_PROYEK), LOWER(TGL_RENCANA_MULAI), LOWER(TGL_RENCANA_SELESAI), LOWER(MT_STATUS_PROYEK.NAMA)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		$group = "RAB_PROYEK.ID_PROYEK, NAMA_PROYEK, TGL_RENCANA_MULAI, TGL_RENCANA_SELESAI, MT_STATUS_PROYEK.NAMA";

		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null, $group, null, $distinct, $having);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null, $group, null, $distinct, $having);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null, $group, null, $distinct, $having);
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') : 0;
			foreach ($resp as $item) {
				if ($item->id_proyek > 0) {
					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/man/proyek/'.$item->id_proyek).'">'.$item->nama_proyek.'</a>',
						$item->qty,
						$item->tgl_rencana_mulai,
						$item->tgl_rencana_selesai,
						$item->status,
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		
		if($query_total){
			$response['recordsTotal'] = $query_total->num_rows();
		}else{
			$response['recordsTotal'] = 0;
		}
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

    public function excelPersebaran($filter=''){
    	$workbook = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $cols = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK');
        $style=array(
            'borders' => array(
                'top' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'left' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'right' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            )
        );
        $styleCenter = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
        $styleYellow=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFffff00',
		        ],
		    ],
        );
        $styleBlue=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FF00b0f0',
		        ],
		    ],
        );
        $styleRed=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFff0000',
		        ],
		    ],
        );
        $styleOrange=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFffd966',
		        ],
		    ],
        );
		$spreadsheet = $workbook->setActiveSheetIndex(0);
        $spreadsheet->setCellValue('A1', 'No');
        $spreadsheet->setCellValue('B1', 'Nama Proyek');
        $spreadsheet->setCellValue('C1', 'Jumlah Man Power');
        $spreadsheet->setCellValue('D1', 'Tgl Mulai');
        $spreadsheet->setCellValue('E1', 'Tgl Selesai');
        $spreadsheet->getStyle("A1:E1")->applyFromArray($style);
        $spreadsheet->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getColumnDimension('E')->setAutoSize(true);
        $no = 1;
        $numrow = 2;
        $datas = $this->query->get_query("SELECT DISTINCT RAB_PROYEK.ID_PROYEK,
					NAMA_PROYEK,
					SUM( RAB_MANPOWER.JUMLAH ) QTY,
					TGL_RENCANA_MULAI,
					TGL_RENCANA_SELESAI,
					MT_STATUS_PROYEK.NAMA STATUS 
				FROM
					RAB_PROYEK
					INNER JOIN MT_STATUS_PROYEK ON RAB_PROYEK.ID_STATUS_PROYEK = MT_STATUS_PROYEK.ID_STATUS_PROYEK
					INNER JOIN RAB_PEKERJAAN ON RAB_PROYEK.ID_PROYEK = RAB_PEKERJAAN.ID_PROYEK
					INNER JOIN RAB_RAB ON RAB_PEKERJAAN.ID_PEKERJAAN = RAB_RAB.ID_PEKERJAAN
					INNER JOIN RAB_MANPOWER ON RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
					LEFT JOIN NEW_BIND_MANPOWER ON NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER 
				GROUP BY
					RAB_PROYEK.ID_PROYEK,
					NAMA_PROYEK,
					TGL_RENCANA_MULAI,
					TGL_RENCANA_SELESAI,
					MT_STATUS_PROYEK.NAMA 
				HAVING
					SUM( RAB_MANPOWER.JUMLAH ) = count( NEW_BIND_MANPOWER.NID )")->result();
        foreach($datas as $data){
        	$spreadsheet->setCellValue('A'.$numrow, $no++);
            $spreadsheet->setCellValue('B'.$numrow, $data->nama_proyek);
            $spreadsheet->setCellValue('C'.$numrow, $data->qty);
            $spreadsheet->setCellValue('D'.$numrow, $data->tgl_rencana_mulai);
            $spreadsheet->setCellValue('E'.$numrow, $data->tgl_rencana_selesai);

            $spreadsheet->getStyle('A'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('B'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('C'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('D'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('E'.$numrow)->applyFromArray($style);
            $numrow++;
        }

        $writer = IOFactory::createWriter($workbook, 'Xlsx');
        header('Content-Disposition: attachment; filename="Laporan Persebaran Man Power.xlsx"');
        $writer->save('php://output');
    }

	public function proyek($id){
		$content['proyek'] = $this->query->get_query("select *
			from RAB_PROYEK
			where id_proyek = '".$id."'")->row();
		$content['detail'] = $this->query->get_query("select DISTINCT MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, MT_JABATAN_PROYEK.NAMA JABATAN
			from NEW_BIND_MAN_JABATAN 
			join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
			join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
			join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
			join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
			join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
			join MT_PEGAWAI on NEW_BIND_MANPOWER.NID = MT_PEGAWAI.NID
			join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
			where RAB_PROYEK.id_proyek = '".$id."'")->result();
		$this->data['content'] = $this->load->view('man/detail_proyek', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data, FALSE);
	}

	function load_data_jamkerja(){
		//preparation
		$distinct = null;
		$select = "MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, MT_PEGAWAI.JABATAN, MT_JABATAN_PROYEK.NAMA JABATAN_PROYEK, NVL(sum(RAB_PROYEK.TGL_REALISASI_SELESAI-RAB_PROYEK.TGL_REALISASI_MULAI),0)*8 DURASI";
		$tbl = "NEW_BIND_MAN_JABATAN";
		$join['data'][] = [
			'table' => 'MT_PEGAWAI',
			'join' => 'NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'MT_JABATAN_PROYEK',
			'join' => 'NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_MANPOWER',
			'join' => 'NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_MANPOWER',
			'join' => 'NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_RAB',
			'join' => 'RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_PEKERJAAN',
			'join' => 'RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_PROYEK',
			'join' => 'RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK',
			'type' => 'INNER',
		];
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(MT_PEGAWAI.NID), LOWER(MT_PEGAWAI.NAMA), LOWER(MT_PEGAWAI.JABATAN), LOWER(MT_JABATAN_PROYEK.NAMA)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = 'DURASI desc';
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		$group = "MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, MT_PEGAWAI.JABATAN, MT_JABATAN_PROYEK.NAMA";

		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null, $group, null, $distinct, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null, $group, null, $distinct, null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null, $group, null, $distinct, null, $index_order);
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') : 0;
			foreach ($resp as $item) {
				if ($item->nid > 0) {
					$response['data'][] = array(
						$no+1,
						$item->nid,
						$item->nama,
						$item->jabatan,
						$item->jabatan_proyek,
						$item->durasi.' Jam'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		
		if($query_total){
			$response['recordsTotal'] = $query_total->num_rows();
		}else{
			$response['recordsTotal'] = 0;
		}
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

    public function excelJamKerja($filter=''){
    	$workbook = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $cols = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK');
        $style=array(
            'borders' => array(
                'top' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'left' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'right' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            )
        );
        $styleCenter = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
        $styleYellow=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFffff00',
		        ],
		    ],
        );
        $styleBlue=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FF00b0f0',
		        ],
		    ],
        );
        $styleRed=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFff0000',
		        ],
		    ],
        );
        $styleOrange=array(
            'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
		        'rotation' => 90,
		        'color' => [
		            'argb' => 'FFffd966',
		        ],
		    ],
        );
		$spreadsheet = $workbook->setActiveSheetIndex(0);
        $spreadsheet->setCellValue('A1', 'No');
        $spreadsheet->setCellValue('B1', 'Nama');
        $spreadsheet->setCellValue('C1', 'NID');
        $spreadsheet->setCellValue('D1', 'Bidang');
        $spreadsheet->setCellValue('E1', 'Jabatan Proyek');
        $spreadsheet->setCellValue('F1', 'Total Utilitas');
        $spreadsheet->getStyle("A1:F1")->applyFromArray($style);
        $spreadsheet->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getColumnDimension('F')->setAutoSize(true);
        $no = 1;
        $numrow = 2;
        $datas = $this->query->get_query("SELECT MT_PEGAWAI.NID,
				MT_PEGAWAI.NAMA,
				MT_PEGAWAI.JABATAN,
				MT_JABATAN_PROYEK.NAMA JABATAN_PROYEK,
				NVL( sum( RAB_PROYEK.TGL_REALISASI_SELESAI - RAB_PROYEK.TGL_REALISASI_MULAI ), 0 ) * 8 DURASI 
			FROM
				NEW_BIND_MAN_JABATAN
				INNER JOIN MT_PEGAWAI ON NEW_BIND_MAN_JABATAN.NID = MT_PEGAWAI.NID
				INNER JOIN MT_JABATAN_PROYEK ON NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
				INNER JOIN NEW_BIND_MANPOWER ON NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
				INNER JOIN RAB_MANPOWER ON NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
				INNER JOIN RAB_RAB ON RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
				INNER JOIN RAB_PEKERJAAN ON RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
				INNER JOIN RAB_PROYEK ON RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK 
			GROUP BY
				MT_PEGAWAI.NID,
				MT_PEGAWAI.NAMA,
				MT_PEGAWAI.JABATAN,
				MT_JABATAN_PROYEK.NAMA
				ORDER BY DURASI DESC")->result();
        foreach($datas as $data){
        	$spreadsheet->setCellValue('A'.$numrow, $no++);
            $spreadsheet->setCellValue('B'.$numrow, $data->nama);
            $spreadsheet->setCellValue('C'.$numrow, $data->nid);
            $spreadsheet->setCellValue('D'.$numrow, $data->jabatan);
            $spreadsheet->setCellValue('E'.$numrow, $data->jabatan_proyek);
            $spreadsheet->setCellValue('F'.$numrow, $data->durasi);

            $spreadsheet->getStyle('A'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('B'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('C'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('D'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('E'.$numrow)->applyFromArray($style);
            $spreadsheet->getStyle('F'.$numrow)->applyFromArray($style);
            $numrow++;
        }

        $writer = IOFactory::createWriter($workbook, 'Xlsx');
        header('Content-Disposition: attachment; filename="Laporan Jam Kerja Man Power.xlsx"');
        $writer->save('php://output');
    }

    function dashboard(){
    	$data['content'] = $this->load->view('man/dashboard', $content, TRUE);
    	$this->load->view('layouts/dashboard', $data, FALSE);
    }

    function hak_akses(){
    	$data['content'] = $this->load->view('man/hak_akses', $content, TRUE);
    	$this->load->view('layouts/dashboard', $data, FALSE);
    }

	function load_data_proyek(){
		//preparation
		$distinct = 1;
		$select = "RAB_PROYEK.ID_PROYEK, NAMA_PROYEK, SUM(RAB_MANPOWER.JUMLAH) QTY, TGL_RENCANA_MULAI, TGL_RENCANA_SELESAI, MT_STATUS_PROYEK.NAMA STATUS";
		$tbl = "RAB_PROYEK";
		$join['data'][] = [
			'table' => 'MT_STATUS_PROYEK',
			'join' => 'RAB_PROYEK.ID_STATUS_PROYEK = MT_STATUS_PROYEK.ID_STATUS_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_PEKERJAAN',
			'join' => 'RAB_PROYEK.ID_PROYEK = RAB_PEKERJAAN.ID_PROYEK',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_RAB',
			'join' => 'RAB_PEKERJAAN.ID_PEKERJAAN= RAB_RAB.ID_PEKERJAAN',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'RAB_MANPOWER',
			'join' => 'RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB',
			'type' => 'INNER',
		];
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NAMA_PROYEK), LOWER(TGL_RENCANA_MULAI), LOWER(TGL_RENCANA_SELESAI), LOWER(MT_STATUS_PROYEK.NAMA)',
			'param'	 => $this->input->get('search[value]')
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		$group = "RAB_PROYEK.ID_PROYEK, NAMA_PROYEK, TGL_RENCANA_MULAI, TGL_RENCANA_SELESAI, MT_STATUS_PROYEK.NAMA";

		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null, $group, null, $distinct);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null, $group, null, $distinct);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null, $group, null, $distinct);
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') : 0;
			foreach ($resp as $item) {
				if ($item->id_proyek > 0) {
					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/man/hak_akses_detail/'.$item->id_proyek).'">'.$item->nama_proyek.'</a>',
						$item->qty,
						$item->tgl_rencana_mulai,
						$item->tgl_rencana_selesai,
						$item->status,
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		
		if($query_total){
			$response['recordsTotal'] = $query_total->num_rows();
		}else{
			$response['recordsTotal'] = 0;
		}
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	public function hak_akses_detail($id){
		if ($this->input->post()) {
			$cb_data = $this->input->post('cb_data');
			$data = $this->query->get_query("select DISTINCT MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, MT_JABATAN_PROYEK.NAMA JABATAN, NEW_BIND_MANPOWER.ID, NEW_BIND_MANPOWER.AKSES_TOOLS CHECKED
				from NEW_BIND_MAN_JABATAN 
				join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
				join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
				join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
				join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
				join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
				join MT_PEGAWAI on NEW_BIND_MANPOWER.NID = MT_PEGAWAI.NID
				join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
				where RAB_PROYEK.id_proyek = '".$id."'")->result();
			foreach ($data as $man) {
				if($cb_data[$man->id]){
					$this->db->update('NEW_BIND_MANPOWER', ['AKSES_TOOLS' => 1], 'ID = '. $man->id);
				}else{
					$this->db->update('NEW_BIND_MANPOWER', ['AKSES_TOOLS' => 0], 'ID = '. $man->id);
				}
			}
			$_SESSION['success'] = 'Data berhasil disimpan';
			redirect(base_url().'new/man/hak_akses');
		}else{
			$content['proyek'] = $this->query->get_query("select *
				from RAB_PROYEK
				where id_proyek = '".$id."'")->row();
			$content['detail'] = $this->query->get_query("select DISTINCT MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, MT_JABATAN_PROYEK.NAMA JABATAN, NEW_BIND_MANPOWER.ID, NEW_BIND_MANPOWER.AKSES_TOOLS CHECKED
				from NEW_BIND_MAN_JABATAN 
				join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
				join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
				join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
				join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
				join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
				join MT_PEGAWAI on NEW_BIND_MANPOWER.NID = MT_PEGAWAI.NID
				join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
				where RAB_PROYEK.id_proyek = '".$id."'")->result();
			$this->data['content'] = $this->load->view('man/detail_hak_akses', $content, TRUE);
			$this->load->view('layouts/dashboard', $this->data, FALSE);
		}
	}
}

/* End of file Man.php */
/* Location: ./application/controllers/new/Man.php */