<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
		$this->load->model('M_tool', 'mtool');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	function edit($id){
		$content['data'] = $this->query->get_data_simple('NEW_TL_PEMINJAMAN_IE', ['ID' => $id])->row();
		$data['content'] = $this->load->view('tool/transaksi/edit_meminjamkan', $content, TRUE);
		if ($this->input->post()) {
			// print_r($this->input->post());exit;
			$kondisi_required = true;
			$rules = [
				['field' => 'end_date', 'label' => 'Sampai Dengan','rules' => 'trim|required'],
			];
			$this->form_validation->set_rules($rules);
			foreach ($this->input->post('kondisi') as $key => $value) {
				if($value == ''){
					$kondisi_required = false;
				}
			}
			if ($this->form_validation->run() == FALSE || $kondisi_required == false) {
				$data['error'] = 'Tgl kembali & Kondisi tool harus diisi semua.';
				$this->load->view('layouts/dashboard', $data, FALSE);
			}else{
				$update_data = [
					'END_DATE' => $this->input->post('end_date'),
					'STATUS_PEMINJAMAN' => 'selesai',
				];
				if ($this->db->update('NEW_TL_PEMINJAMAN_IE', $update_data, ['ID' => $id])) {
					$cek_rusak = 0;
					foreach ($this->input->post('kondisi') as $key => $value) {
						if($value != 'Baik'){
							$cek_rusak++;
						}
						if(isset($this->input->post('cb_data')[$key])){
							$kembali = 1;
						}else{
							$kembali = 0;
						}
						$update_data_detail = [
							'TOOL_KEMBALI' => $kembali,
							'TOOL_KONDISI' => $value,
						];
						if($content['data']->direction == 'masuk'){
							$this->db->update('NEW_TL_PEMINJAMAN_TOOL_MASUK', $update_data_detail, ['ID' => $key]);
						}else{
							$this->db->update('NEW_TL_PEMINJAMAN_TOOL_KELUAR', $update_data_detail, ['ID' => $key]);
						}
					}
					$_SESSION['success'] = 'Data berhasil disimpan';
					// if($cek_rusak > 0){
					// 	redirect(base_url().'new/pengembalian/berita_acara/'.$id);
					// }else{
						// redirect(base_url().'new/tools/transaksi');
						redirect(base_url().'new/pengembalian/berita_acara_kembali/'.$id);
					// }
				}else{
					$data['error'] = '';
					$this->load->view('layouts/dashboard', $data, FALSE);
				}
			}
		}else{
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	function detail($id){
		$content['data'] = $this->query->get_data_simple('NEW_TL_PEMINJAMAN_IE', ['ID' => $id])->row();
		if($content['data']->direction == 'masuk'){
			$content['count_item'] = $this->query->get_query("select count(-1) jumlah
				from NEW_TL_PEMINJAMAN_TOOL_MASUK
				where TOOL_KEMBALI = 1 and PEMINJAMAN_ID = '".$content['data']->id."'")->row();
			$content['count'] = $this->query->get_query("select count(-1) jumlah
				from NEW_TL_PEMINJAMAN_TOOL_MASUK
				where TOOL_KONDISI != 'Baik' and PEMINJAMAN_ID = '".$content['data']->id."'")->row();
		}else{
			$content['count_item'] = $this->query->get_query("select count(-1) jumlah
				from NEW_TL_PEMINJAMAN_TOOL_KELUAR
				where TOOL_KEMBALI = 1 and PEMINJAMAN_ID = '".$content['data']->id."'")->row();
			$content['count'] = $this->query->get_query("select count(-1) jumlah
				from NEW_TL_PEMINJAMAN_TOOL_KELUAR
				where TOOL_KONDISI != 'Baik' and PEMINJAMAN_ID = '".$content['data']->id."'")->row();
		}
		$data['content'] = $this->load->view('tool/transaksi/detail_meminjamkan', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function berita_acara($id){
		$content['data'] = $this->query->get_query("select NEW_TL_PEMINJAMAN_IE.*, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'DD-MM-YYYY') tgl_ba, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'DD') tgl, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'Month') bulan, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'YYYY') tahun
			from NEW_TL_PEMINJAMAN_IE
			where ID = '".$id."'")->row();

		if($content['data']->direction == 'masuk'){
			$content['detail'] = $this->query->get_query("select *
				from NEW_TL_PEMINJAMAN_TOOL_MASUK
				where TOOL_KONDISI != 'Baik' and PEMINJAMAN_ID = '".$content['data']->id."'")->result();
		}else{
			$content['detail'] = $this->query->get_query("select *
				from NEW_TL_PEMINJAMAN_TOOL_KELUAR
				where TOOL_KONDISI != 'Baik' and PEMINJAMAN_ID = '".$content['data']->id."'")->result();
		}
		// $content['data'] = $this->query->get_data_simple('NEW_TL_PEMINJAMAN_IE', ['ID' => $id])->row();
		// $content['detail'] = $this->query->get_data_simple('NEW_TL_PEMINJAMAN_TOOL_KELUAR', ['PEMINJAMAN_ID' => $id])->result();
		$data['content'] = $this->load->view('tool/berita_acara_kerusakan', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function berita_acara_kembali($id){
		$content['data'] = $this->query->get_query("select NEW_TL_PEMINJAMAN_IE.*, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'DD-MM-YYYY') tgl_ba, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'DD') tgl, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'Month') bulan, TO_CHAR(NEW_TL_PEMINJAMAN_IE.END_DATE, 'YYYY') tahun
			from NEW_TL_PEMINJAMAN_IE
			where ID = '".$id."'")->row();
		if($content['data']->direction == 'masuk'){
			$content['detail'] = $this->query->get_query("select *
				from NEW_TL_PEMINJAMAN_TOOL_MASUK
				where TOOL_KEMBALI = 1 and PEMINJAMAN_ID = '".$content['data']->id."'")->result();
			$content['count'] = $this->query->get_query("select count(-1) jumlah
				from NEW_TL_PEMINJAMAN_TOOL_MASUK
				where TOOL_KONDISI != 'Baik' and PEMINJAMAN_ID = '".$content['data']->id."'")->row();
		}else{
			$content['detail'] = $this->query->get_query("select *
				from NEW_TL_PEMINJAMAN_TOOL_KELUAR
				where TOOL_KEMBALI = 1 and PEMINJAMAN_ID = '".$content['data']->id."'")->result();
			$content['count'] = $this->query->get_query("select count(-1) jumlah
				from NEW_TL_PEMINJAMAN_TOOL_KELUAR
				where TOOL_KONDISI != 'Baik' and PEMINJAMAN_ID = '".$content['data']->id."'")->row();
		}
		$data['content'] = $this->load->view('tool/berita_acara', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	// function pengembalian_transaksi($trxid){
	// 	$table  = 'NEW_TL_PEMINJAMAN A';
	// 	$join[] = 'NEW_TL_PAKET B';
	// 	$join[] = 'A.PAKET_ID = B.ID';
	// 	$join[] = 'INNER';
	// 	$where  = ['A.PR_ID' => $trxid];
	// 	$content['trx'] = $this->query->get_data_simple($table, $where , $join)->row();

	// 	// self::debug();

	// 	$data['content'] = $this->load->view('tool/transaksi/pengembalian_transaksi', $content, TRUE);
	// 	$this->load->view('layouts/dashboard', $data, FALSE);

	// }
}
