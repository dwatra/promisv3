<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('M_query', 'query');
		$this->load->model('AuthModel', 'auth');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
	}

	function calculate_utilitas_internal_external_keluar(){

		/*
			selecting the peminjmaan ie ->  limit 10 
		*/
		$tool_effected = 0;
		$peminjaman_effected = 0;

		$ids_tool_updated = [];
		$ids_transaction_updated = [];

		$tbl        = 'NEW_TL_PEMINJAMAN_IE';
		$tbl_detail = 'NEW_TL_PEMINJAMAN_TOOL_KELUAR';
		$limit      = ['start' => 0 , 'finish' => 10];
		$where['data'][] = [
			'column' =>  'START_DATE <=' ,
			'param'  =>  date('d-m-Y'),
		];
		$where['data'][] = [
			'column' =>  'END_DATE >=' ,
			'param'  =>  date('d-m-Y'),
		];
		$where['data'][] = [
			'column' =>  'STATUS_PEMINJAMAN' ,
			'param'  =>  'belum_mulai',
		];
		$where['data'][] = [
			'column' =>  'DIRECTION' ,
			'param'  =>  'keluar',
		];

		$list_ie 	 = 	 $this->query->get_data_complex('*' , $tbl ,$limit , null , null , null , $where );

		if ($list_ie) {
			foreach ($list_ie->result() as $x) {
				$meminjmankan_id = $x->id;
				$start_date      = strtotime($x->start_date);
				$end_date        = strtotime($x->end_date);
				$total_days      = ($end_date - $start_date)/60/60/24;

				$where2['data'][] = [
					'column' => 'PEMINJAMAN_ID',
					'param'  => $meminjmankan_id
				];

				$list_tool = $this->query->get_data_complex('*', $tbl_detail,null , null , null , null , $where2);

				if ($list_tool) {
					foreach ($list_tool->result() as $tools) {
						$tool_id = $tools->tool_id;

						switch ($tools->tool_jenis) {
							case 'mekanik':
								$hours = 5;
								break;

							case 'instrument':
								$hours = 5;
								break;

							case 'listrik':
								$hours = 6;
								break;

							case 'predictive':
								$hours = 4;
								break;

							default:
								$hours = 0;
								break;
						}

						if ($hours != 0) {
							$utilitas = $total_days * $hours ;
						} else {
							$utilitas = 0;
						}
						$q = ' UPDATE NEW_TL_TOOL_DETAIL SET KONDISI = 3 ,UTILITAS = UTILITAS+'.$utilitas.' WHERE ID = '.$tool_id;
						$this->query->get_query($q);

						if ($this->db->affected_rows() > 0) {
							$tool_effected++;
							$ids_tool_updated[] = $tool_id;
						}

					}
				}
				$q2 = " UPDATE NEW_TL_PEMINJAMAN_IE SET STATUS_PEMINJAMAN = 'progres' WHERE ID =  ".$meminjmankan_id;
				$this->query->get_query($q2);

				if ($this->db->affected_rows() > 0) {
					$peminjaman_effected++;
					$ids_transaction_updated[] = $meminjmankan_id;
				}

			}
		}

		echo 'Total tool updated : '.$tool_effected . ' || Total Peminjaman updated : '.$peminjaman_effected.' <br>';
		echo 'ID Transaction updated :<br>';
		print_r($ids_transaction_updated);
		echo '<br>ID Tool updated :<br>';
		print_r($ids_tool_updated);
		// $this->__update_the_utility($ids_transaction_updated , $ids_tool_updated);
		exit;
	}

	// function __update_the_utility($ids_transaction_updated){

	// 	for ($i=0; $i < count($ids_transaction_updated) ; $i++) {

	// 		$id_peminjaman = $ids_transaction_updated[$i];

	// 		$result = $this->query->get_data_simple('NEW_TL_PEMINJAMAN_IE' , ['ID' => $id_peminjaman]);

	// 		if ($result) {
	// 			foreach ($result->result() as $x) {






	// 			}
	// 		}



	// 	}




	// }

	function current_ci_ver(){
		echo CI_VERSION;
	}


}
