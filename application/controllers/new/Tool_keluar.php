<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool_Keluar extends CI_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->model("M_tool","mtool");

		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	function index(){

		$data['content'] = $this->load->view('tool/transaksi/add_meminjamkan', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function load_data_meminjamnkan_detail($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_KELUAR";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_INVENTARIS),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_JENIS)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		$where['data'][] = [
			'column' => 'USER_ID' ,
			'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		];


		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$response['data'][] = array(
						$item->id,
						$item->tool_name ,
						$item->tool_spec,
						$item->tool_inventaris,
						$item->tool_jenis,
						$st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function load_detail_tool_keluar($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_KELUAR";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_INVENTARIS),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_JENIS)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		// $where['data'][] = [
		// 	'column' => 'USER_ID' ,
		// 	'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		// ];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		];


		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 1;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$check = '<input type="checkbox" name="cb_data['.$item->id.']" >';
					$lov = '<select name="kondisi['.$item->id.']" id="kondisi_'.$item->id.'" class="form-control w-100">
								<option value="">- Pilih -</option>
								<option value="Baik">Baik</option>
								<option value="Rusak">Rusak</option>
								<option value="Hilang">Hilang</option>
							</select>';
					$btn_save = '<button type="submit" class="btn-save btn btn-sm btn-success" onclick="save_rusak('.$item->id.')"> Save</button>';
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$response['data'][] = array(
						$check,
						// $no,
						$item->tool_name .' - '. $item->tool_inventaris ,
						$item->tool_spec,
						$item->tool_jenis,
						$lov,
						$btn_save
						// $st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $recordsTotal;
		// $response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function load_detail_tool_keluar_detail($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_KELUAR";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_INVENTARIS),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_JENIS),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_KONDISI)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		// $where['data'][] = [
		// 	'column' => 'USER_ID' ,
		// 	'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		// ];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		];


		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 1;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$checked = $item->tool_kembali == 1 ? 'checked' : '';
					$check = '<input type="checkbox" name="cb_data['.$item->id.']" '.$checked.' disabled>';
					$lov = '<select name="kondisi['.$item->id.']" id="kondisi_'.$item->id.'" class="form-control w-100">
								<option value="">- Pilih -</option>
								<option value="Baik">Baik</option>
								<option value="Rusak">Rusak</option>
								<option value="Hilang">Hilang</option>
							</select>';
					$btn_save = '<button type="submit" class="btn-save btn btn-sm btn-success" onclick="save_rusak('.$item->id.')"> Save</button>';
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$response['data'][] = array(
						$check,
						// $no,
						$item->tool_name .' - '. $item->tool_inventaris ,
						$item->tool_spec,
						$item->tool_jenis,
						$item->tool_kondisi,
						$btn_save
						// $st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $recordsTotal;
		// $response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function load_detail_tool_keluar_detail_rusak($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_KELUAR";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_INVENTARIS),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_JENIS),LOWER(NEW_TL_PEMINJAMAN_TOOL_KELUAR.TOOL_KONDISI)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		$where['data'] = [
		// [
		// 	'column' => 'USER_ID' ,
		// 	'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		// ],
		[
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		],[
			'column' => 'TOOL_KONDISI !=',
			'param' => 'Baik'
		]];

		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 1;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$checked = $item->tool_kembali == 1 ? 'checked' : '';
					$check = '<input type="checkbox" name="cb_data['.$item->id.']" '.$checked.' disabled>';
					$lov = '<select name="kondisi['.$item->id.']" id="kondisi_'.$item->id.'" class="form-control w-100">
								<option value="">- Pilih -</option>
								<option value="Baik">Baik</option>
								<option value="Rusak">Rusak</option>
								<option value="Hilang">Hilang</option>
							</select>';
					$btn_save = '<button type="submit" class="btn-save btn btn-sm btn-success" onclick="save_rusak('.$item->id.')"> Save</button>';
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$response['data'][] = array(
						$item->tool_name .' - '. $item->tool_inventaris ,
						$item->tool_spec,
						$item->tool_jenis,
						// $st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $recordsTotal;
		// $response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function save(){

		$tool_id =  $_POST['tool'];
		$get_detail      = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL' , ['ID' => $tool_id ])->row();
		$get_tool_detail = $this->query->get_data_simple('NEW_TL_TOOL' , ['ID' => $get_detail->tool_id ])->row();

		$insert = [
			'TOOL_ID'       => $tool_id,
			'TOOL_DETAIL_ID'=> $get_tool_detail->id,
			'TOOL_INVENTARIS'=> $get_detail->no_inventarisasi,
			'TOOL_NAME'     => $get_tool_detail->name,
			'TOOL_JENIS'    => $_POST['kategori'],
			'TOOL_SPEC'    	=> $get_tool_detail->spek,
			'PEMINJAMAN_ID' => 0,
			'USER_ID'       => $_SESSION['SESSION_APP_EBUDGETING']['user_id'] ,
		];
		if ($this->query->save_data('NEW_TL_PEMINJAMAN_TOOL_KELUAR', $insert)) {

			$json_resp = [
				'status' => 200,
				'msg' => 'success input data'
			];

		} else {
			$json_resp = [
				'status' => 400,
				'msg' => $this->db->error()
			];

		}

		echo json_encode($json_resp);
	}

	function create(){

		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			exit('no direct script access allowed');
		}

		$where= [
			'NAME' => $_POST['vendor_name']
		];

		$res = $this->query->get_data_simple('NEW_TL_VENDORS' , $where);


		if (count($res->result()) > 0) {

			$vendor_id = $res->row()->id;

		} else {
			$vendor_id = sprintf('%08d', date('dn').rand(0,999));

			$data_vendors = [
				'NAME' => $_POST['vendor_name'],
				'ID'   => $vendor_id,
			];

			$this->query->save_data('NEW_TL_VENDORS' , $data_vendors);
		}

		$data_to_meminjamkan = [
			'VENDOR'             => $_POST['vendor_name'],
			'START_DATE'         => $_POST['start'],
			// 'END_DATE'        => $_POST['end'],
			'DIRECTION'          => 'keluar',
			'VENDOR_ID'          => $vendor_id,
			'STATUS_PEMINJAMAN'  => 'belum_mulai',
			'PIC_VENDOR'         => $_POST['vendor_pic'],
			'PIC_VENDOR_JABATAN' => $_POST['vendor_pic_level'],
			'NO_KTP'             => $_POST['vendor_ktp'],
			'DIVISI'             => $_POST['vendor_div'],
			'KEPERLUAN'          => $_POST['keperluan'],
		];
		$config['upload_path']          = './uploads/peminjaman_ie_file/';
		$config['allowed_types']        = '*';
		$config['encrypt_name'] 		= true;

		$this->load->library('upload',$config);
		if ($this->upload->do_upload('berkas')) {
				$upload_data = $this->upload->data();
				$data_to_peminjaman['LAMPIRAN'] = $upload_data['file_name'];
		}
		$this->query->save_data('NEW_TL_PEMINJAMAN_IE' , $data_to_meminjamkan);

		$last_id = $this->query->get_query("SELECT MAX(ID) as idx FROM NEW_TL_PEMINJAMAN_IE WHERE DIRECTION = 'keluar' ")->row();
		$meminjamkan_id = $last_id->idx;

		$update_the_peminjaman = [
			'PEMINJAMAN_ID' => $meminjamkan_id,
		];

		$where = [
			'PEMINJAMAN_ID' => 0,
			'USER_ID' => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		];

		$this->query->update_data('NEW_TL_PEMINJAMAN_TOOL_KELUAR' , $where , $update_the_peminjaman);

		redirect( base_url('new/tools/transaksi') );

	}



	function delete($id){

		$where = [
			'ID' => $id
		];


		if ($this->query->delete_data('NEW_TL_PEMINJAMAN_TOOL_KELUAR' , $where)) {
			$resp  =[
				'status' => 200,
				'msg'    =>'success delete this data'
			];
		}

		echo json_encode($resp);


	}


}
