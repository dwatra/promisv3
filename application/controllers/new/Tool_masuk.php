<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool_Masuk extends CI_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->library('form_validation');
		$this->load->helper('intive');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	function index(){

		$content['cart']       = $this->query->get_data_simple('NEW_TL_CART_IE2', [])->result();
		$content['proyek']     = $this->api->fetchapi([],'GET','rab')->data;
		// $proyek = $this->query->get_data_simple('rab', []);
		// $content['proyek']     = ->result();
		$this->data['content'] = $this->load->view('tool/transaksi/add_peminjaman', $content, TRUE);

		$this->load->view('layouts/dashboard', $this->data, FALSE);
	}

	function progress_peminjaman(){
		ini_set('display_errors' , 0);
		$where= [
			'NAME' => $_POST['vendor_name']
		];
		$res = $this->query->get_data_simple('NEW_TL_VENDORS' , $where);

		if ($res) {
			$vendor_id = $res->row()->id;

		} else {
			$vendor_id = sprintf('%08d', date('dn').rand(0,999));
			$data_vendors = [
				'NAME' => $_POST['vendor_name'],
				'ID'   => $vendor_id,
			];
			$this->query->save_data('NEW_TL_VENDORS' , $data_vendors);
		}

		$pr = $this->query->get_query('SELECT * FROM RAB_PROYEK where ID_PROYEK = '.$_POST['proyek_id'])->row();
		$pr_name = $pr->nama_proyek;

		$data_to_peminjaman = [
			'VENDOR'             => $_POST['vendor_name'],
			'START_DATE'         => $_POST['start'],
			'END_DATE'           => null,
			'DIRECTION'          => 'masuk',
			'PR_ID'              => $_POST['proyek_id'],
			'PR_NAME'            => $pr_name,
			'VENDOR_ID'          => $vendor_id,
			'STATUS_PEMINJAMAN'  => 'belum_mulai',
			'PIC_VENDOR'         => $_POST['vendor_pic'],
			'PIC_VENDOR_JABATAN' => $_POST['vendor_pic_level'],
			'NO_KTP'             => $_POST['vendor_ktp'],
			'DIVISI'             => $_POST['vendor_div'],
			'KEPERLUAN'          => $_POST['keperluan'],
		];

		$config['upload_path']   = './uploads/peminjaman_ie_file/';
		$config['allowed_types'] = '*';
		$config['encrypt_name']  = true;

		$this->load->library('upload',$config);
		if ($this->upload->do_upload('berkas')) {
				$upload_data = $this->upload->data();
				$data_to_peminjaman['LAMPIRAN'] = $upload_data['file_name'];
				// $file = $upload_data['file_name'];
		}
		$s_data = $this->query->save_data('NEW_TL_PEMINJAMAN_IE' , $data_to_peminjaman);



		$peminjaman =  $this->query->get_query('SELECT max(ID) AS idx FROM NEW_TL_PEMINJAMAN_IE ')->row();
		$peminjaman_id = $peminjaman->idx;

		$update_the_peminjaman = [
			'PEMINJAMAN_ID' => $peminjaman_id,
		];

		$where = [
			'PEMINJAMAN_ID' => 0,
			'USER_ID' => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		];

		$this->query->update_data('NEW_TL_PEMINJAMAN_TOOL_MASUK' , $where , $update_the_peminjaman);

		redirect( base_url('new/tools/transaksi') );
	}


	//  =========================================== Ajaxx ==========================================
	function progress_add_item(){

		$config['upload_path']   = './uploads/item_peminjaman/';
		$config['allowed_types'] = '*';
		$config['encrypt_name']  = true;

		$data_save_temp = [
			'PEMINJAMAN_ID' => 0,
			'TOOL_NAME'     => $_POST['tool'],
			'TOOL_SPEC'     => $_POST['spek'],
			'QTY'           => $_POST['jml'] ,
			'USER_ID'       => $_SESSION['SESSION_APP_EBUDGETING']['user_id'] ,
		];

		$this->load->library('upload',$config);
		if ($this->upload->do_upload('category_image')) {
				$upload_data = $this->upload->data();
				$data_save_temp['ITEM_FILES'] = $upload_data['file_name'];
		}









		$this->query->save_data('NEW_TL_PEMINJAMAN_TOOL_MASUK' , $data_save_temp);

		$resp = [
			'status' => 200 ,
			'msg' => 'success save data'
		];

		echo json_encode($resp);
	}


	function delete_item_peminjaman(){
		$id = $_POST['id'];
		$where = ['ID' => $id ];
		$rows = $this->query->get_data_simple('NEW_TL_PEMINJAMAN_TOOL_MASUK' , $where)->row();
		$path= './uploads/item_peminjaman/'.$rows->item_files;
		if (file_exists($path)) {
			 unlink($path);
		}

		$this->query->delete_data("NEW_TL_PEMINJAMAN_TOOL_MASUK", $where);

		$resp = [
			'status' => 200 ,
			'msg' => 'success DELETE data'
		];

		echo json_encode($resp);

	}

	function load_temporary_data_peminjaman($id){
		//preparation
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_MASUK";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_NAME),
			LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.QTY),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.ITEM_FILES)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		$where['data'][] = [
			'column' => 'USER_ID' ,
			'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		];

		$supported_image = array(
	    'gif',
	    'jpg',
	    'jpeg',
	    'png'
		);

		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];



		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {
					$st = '<div class="dropdown" style="display:inline">
								<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
									<span class="glyphicon glyphicon-option-vertical"></span>
								</a>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
									<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
									</ul>
								</div>';


					$ext = strtolower(pathinfo($item->item_files, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image)) {
					    $files = '<img style="height:120px;width:120px;" src="'.base_url().'uploads/item_peminjaman/'.$item->item_files.'">';
					} else {
					    $files = '<a href="'.base_url().'uploads/item_peminjaman/'.$item->item_files.'" download> Download File </a>';
					}


					$response['data'][] = array(
						$item->id,
						$item->tool_name ,
						$item->tool_spec,
						$item->qty,
						$files,
						$st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function load_detail_tool_masuk($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_MASUK";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.QTY)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		// $where['data'][] = [
		// 	'column' => 'USER_ID' ,
		// 	'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		// ];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		];


		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 1;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$check = '<input type="checkbox" name="cb_data['.$item->id.']" >';
					$lov = '<select name="kondisi['.$item->id.']" id="kondisi_'.$item->id.'" class="form-control w-100">
								<option value="">- Pilih -</option>
								<option value="Baik">Baik</option>
								<option value="Rusak">Rusak</option>
								<option value="Hilang">Hilang</option>
							</select>';
					$btn_save = '<button type="submit" class="btn-save btn btn-sm btn-success" onclick="save_rusak('.$item->id.')"> Save</button>';
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$ext = strtolower(pathinfo($item->item_files, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image)) {
					    $files = '<img style="height:120px;width:120px;" src="'.base_url().'uploads/item_peminjaman/'.$item->item_files.'">';
					} else {
					    $files = '<a href="'.base_url().'uploads/item_peminjaman/'.$item->item_files.'" download> Download File </a>';
					}

					$response['data'][] = array(
						$check,
						// $no,
						$item->tool_name,
						$item->tool_spec,
						$item->qty,
						$files,
						$lov,
						$btn_save
						// $st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $recordsTotal;
		// $response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function load_detail_tool_masuk_detail($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_MASUK";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.QTY),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_KONDISI)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		// $where['data'][] = [
		// 	'column' => 'USER_ID' ,
		// 	'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		// ];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		];


		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 1;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$checked = $item->tool_kembali == 1 ? 'checked' : '';
					$check = '<input type="checkbox" name="cb_data['.$item->id.']" '.$checked.' disabled>';
					$lov = '<select name="kondisi['.$item->id.']" id="kondisi_'.$item->id.'" class="form-control w-100">
								<option value="">- Pilih -</option>
								<option value="Baik">Baik</option>
								<option value="Rusak">Rusak</option>
								<option value="Hilang">Hilang</option>
							</select>';
					$btn_save = '<button type="submit" class="btn-save btn btn-sm btn-success" onclick="save_rusak('.$item->id.')"> Save</button>';
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$ext = strtolower(pathinfo($item->item_files, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image)) {
					    $files = '<img style="height:120px;width:120px;" src="'.base_url().'uploads/item_peminjaman/'.$item->item_files.'">';
					} else {
					    $files = '<a href="'.base_url().'uploads/item_peminjaman/'.$item->item_files.'" download> Download File </a>';
					}

					$response['data'][] = array(
						$check,
						// $no,
						$item->tool_name,
						$item->tool_spec,
						$item->qty,
						$files,
						$item->tool_kondisi,
						$btn_save
						// $st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $recordsTotal;
		// $response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	function load_detail_tool_masuk_detail_rusak($id){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL_MASUK";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL_MASUK.TOOL_KONDISI)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		$where['data'] = [
		// [
		// 	'column' => 'USER_ID' ,
		// 	'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		// ],
		[
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => $id
		],[
			'column' => 'TOOL_KONDISI !=',
			'param' => 'Baik'
		]];

		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query;
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 1;
			foreach ($resp->result() as $item) {
				if ($item->id > 0) {
					$checked = $item->tool_kembali == 1 ? 'checked' : '';
					$check = '<input type="checkbox" name="cb_data['.$item->id.']" '.$checked.' disabled>';
					$lov = '<select name="kondisi['.$item->id.']" id="kondisi_'.$item->id.'" class="form-control w-100">
								<option value="">- Pilih -</option>
								<option value="Baik">Baik</option>
								<option value="Rusak">Rusak</option>
								<option value="Hilang">Hilang</option>
							</select>';
					$btn_save = '<button type="submit" class="btn-save btn btn-sm btn-success" onclick="save_rusak('.$item->id.')"> Save</button>';
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$response['data'][] = array(
						$item->tool_name,
						$item->tool_spec,
						$item->qty,
						// $st
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $recordsTotal;
		// $response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}




}
