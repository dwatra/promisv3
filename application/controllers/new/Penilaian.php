<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;

class Penilaian extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	public function index()
	{
		$data['content'] = $this->load->view('man/penilaian', NULL, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	public function load_data(){
		$select = "MT_PEGAWAI.NID,MT_PEGAWAI.NAMA,MT_PEGAWAI.JABATAN,MT_PEGAWAI.UNIT,MT_UNIT.TABLE_DESC as LOKASI, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.INTI, NEW_BIND_KOMPETENSI.TAMBAHAN, NEW_BIND_KOMPETENSI.TAHUN_MASUK, NEW_BIND_KOMPETENSI.GRADE, NEW_BIND_KOMPETENSI.SKILL, NEW_BIND_KOMPETENSI.KOMPETENSI";
		$tbl = "MT_PEGAWAI";
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);
		$where_like['data'][] = [
			'column' => 'MT_PEGAWAI.NID,MT_PEGAWAI.NAMA,MT_PEGAWAI.JABATAN,MT_UNIT.TABLE_DESC, MT_JABATAN_PROYEK.NAMA, NEW_BIND_KOMPETENSI.INTI, NEW_BIND_KOMPETENSI.TAMBAHAN, NEW_BIND_KOMPETENSI.TAHUN_MASUK, NEW_BIND_KOMPETENSI.GRADE, NEW_BIND_KOMPETENSI.SKILL, NEW_BIND_KOMPETENSI.KOMPETENSI',
			'param'	 => $this->input->get('search[value]')
		];
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		$join['data'][] = [
			'table' => 'MT_UNIT',
			'join' => 'MT_PEGAWAI.UNIT=MT_UNIT.TABLE_CODE',
			'type' => 'left',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_MAN_JABATAN',
			'join' => 'MT_PEGAWAI.NID=TRIM(NEW_BIND_MAN_JABATAN.NID)',
			'type' => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'MT_JABATAN_PROYEK',
			'join' => 'NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK=MT_JABATAN_PROYEK.ID_JABATAN_PROYEK',
			'type' => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'NEW_BIND_KOMPETENSI',
			'join' => 'MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)',
			'type' => 'LEFT',
		];
		$where['data'] = [[
			'column' => 'KDJABATAN !=',
			'param' => 'TERM'
		],[
			'column' => 'UNIT =',
			'param' => 'KP2'
		]];
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null);
		$response['data'] = [];
		if ($query) {
			foreach ($query->result() as $item) {
				$response['data'][] = [
					$item->nid,
					'<a href="'.base_url('/new/penilaian/edit/'.$item->nid).'">'.$item->nama.'</a>',
					$item->jabatan,
					$item->jabatan_proyek,
					$item->inti,
					$item->tambahan,
					$item->tahun_masuk,
					$item->grade,
					$item->skill,
					$item->kompetensi,
					/*'<div class="dropdown" style="display:inline">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
							<span class="glyphicon glyphicon-option-vertical"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
							<li><a href="'.base_url('/new/penilaian/edit/'.str_replace('/', '~', $item->nid)).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
						</ul>
					</div>'*/
				];
			}
		}
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}

	// public function add(){
	// 	if ($this->input->post()) {
	// 		// print_r($this->input->post());
	// 		$this->form_validation->set_rules([
	// 			['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]|is_unique[MT_PEGAWAI.NID]'],
	// 			['field' => 'nama', 'label' => 'Nama','rules' => 'trim|required'],
	// 			['field' => 'jabatan', 'label' => 'Jabatan','rules' => 'trim|required'],
	// 			['field' => 'unit', 'label' => 'Unit','rules' => 'trim|required'],
	// 			['field' => 'kdjabtan', 'label' => 'Kode Jabatan','rules' => 'trim|required'],
	// 			['field' => 'jabatan_proyek', 'label' => 'Jabatan Proyek','rules' => 'required'],
	// 		]);
	// 		if ($this->form_validation->run() == FALSE) {
	// 			$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
	// 			$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
	// 			$data['content'] = $this->load->view('man/new', $content, true);
	// 			$data['error'] = $this->form_validation->error_array();
	// 			$this->load->view('layouts/dashboard', $data, FALSE);
	// 		} else {
	// 			$insert_data = [
	// 				'NAMA' => strtoupper($this->input->post('nama')),
	// 				'NID'  => strtoupper($this->input->post('nid')),
	// 				'UNIT' => strtoupper($this->input->post('unit')),
	// 				'JABATAN' => strtoupper($this->input->post('jabatan')),
	// 				'KDJABATAN' => strtoupper($this->input->post('kdjabtan'))
	// 			];
	// 			if ($this->query->save_data('MT_PEGAWAI', $insert_data)) {
	// 				$insert_data_bind = [
	// 					'NID' => strtoupper($this->input->post('nid')),
	// 					'ID_JABATAN_PROYEK'  => $this->input->post('jabatan_proyek'),
	// 				];
	// 				if ($this->query->save_data('NEW_BIND_MAN_JABATAN', $insert_data_bind)) {
	// 					$insert_data_bind_kompetensi = [
	// 						'NID' => strtoupper($this->input->post('nid')),
	// 						'KOMPETENSI'  => strtoupper($this->input->post('kompetensi')),
	// 					];
	// 					if ($this->query->save_data('NEW_BIND_KOMPETENSI', $insert_data_bind_kompetensi)) {
	// 						$_SESSION['success'] = 'Data berhasil disimpan';
	// 						redirect(base_url().'new/man');
	// 					}
	// 				}
	// 			}else{
	// 				$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
	// 				$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
	// 				$data['content'] = $this->load->view('man/new', $content, true);
	// 				$data['error'] = "Terjadi Kesalahan, hubungi admin jika masalah berlanjut";
	// 				$this->load->view('layouts/dashboard', $data, FALSE);
	// 			} 
	// 		}
	// 	}else{
	// 		$content['units'] = $this->query->get_data_simple('MT_UNIT', ['TABLE_CODE !=' => '***'])->result();
	// 		$content['jabatans'] = $this->query->get_data_simple('MT_JABATAN_PROYEK', NULL)->result();
	// 		$data['content'] = $this->load->view('man/new', $content, true);
	// 		$this->load->view('layouts/dashboard', $data, FALSE);
	// 	}
	// }

	public function edit($id){
		$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
			from MT_PEGAWAI
			left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
			left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
			left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
			left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
			left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
			where MT_PEGAWAI.NID = '".$id."'")->row();
		$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
		if ($this->input->post()) {
			// $rules = [
			// 	['field' => 'inti', 'label' => 'Inti','rules' => 'trim|required'],
			// ];
			if ($id == $this->input->post('nid')) {
				$rules[] = ['field' => 'nid', 'label' => 'Nomer ID','rules' => 'trim|required|max_length[10]'];
			}
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
					from MT_PEGAWAI
					left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
					left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
					left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
					left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
					left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
					where MT_PEGAWAI.NID = '".$id."'")->row();
				$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
				$data['content'] = $this->load->view('man/edit_penilaian', $content, true);
				$data['error'] = $this->form_validation->error_array();
				$this->load->view('layouts/dashboard', $data, FALSE);
			} else {
				$cek_data_kompetensi = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
				if(count($cek_data_kompetensi) > 0){
					$insert_data_bind_kompetensi = [
						'INTI'  => strtoupper($this->input->post('inti')),
						'TAMBAHAN'  => strtoupper($this->input->post('tambahan')),
						'TAHUN_MASUK'  => strtoupper($this->input->post('tahun_masuk')),
						'GRADE'  => strtoupper($this->input->post('grade')),
						'SKILL'  => strtoupper($this->input->post('skill')),
						'KOMPETENSI'  => strtoupper($this->input->post('kompetensi')),
					];
					if ($this->db->update('NEW_BIND_KOMPETENSI', $insert_data_bind_kompetensi, ['NID' => $id])){
						$_SESSION['success'] = 'Data berhasil disimpan';
						redirect(base_url().'new/penilaian');
					}else{
						$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
							from MT_PEGAWAI
							left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
							left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
							left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
							left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
							left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
							where MT_PEGAWAI.NID = '".$id."'")->row();
						$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
						$data['content'] = $this->load->view('man/edit_penilaian', $content, true);
						$data['error'] = 'Data gagal disimpan';
						$this->load->view('layouts/dashboard', $data, FALSE);
					} 
				}else{
					$insert_data_bind_kompetensi = [
						'NID' => $id,
						'INTI'  => strtoupper($this->input->post('inti')),
						'TAMBAHAN'  => strtoupper($this->input->post('tambahan')),
						'TAHUN_MASUK'  => strtoupper($this->input->post('tahun_masuk')),
						'GRADE'  => strtoupper($this->input->post('grade')),
						'SKILL'  => strtoupper($this->input->post('skill')),
						'KOMPETENSI'  => strtoupper($this->input->post('kompetensi')),
					];
					if($this->query->save_data('NEW_BIND_KOMPETENSI', $insert_data_bind_kompetensi)){
						$_SESSION['success'] = 'Data berhasil disimpan';
						redirect(base_url().'new/penilaian');
					}else{
						$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
							from MT_PEGAWAI
							left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
							left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
							left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
							left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
							left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
							where MT_PEGAWAI.NID = '".$id."'")->row();
						$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
						$data['content'] = $this->load->view('man/edit_penilaian', $content, true);
						$data['error'] = 'Data gagal disimpan';
						$this->load->view('layouts/dashboard', $data, FALSE);
					} 
				}
			}
		}else{
			$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
				from MT_PEGAWAI
				left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
				left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
				left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
				left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
				left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
				where MT_PEGAWAI.NID = '".$id."'")->row();
			$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
			$data['content'] = $this->load->view('man/edit_penilaian', $content, true);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	// function delete($id){
	// 	$this->query->delete_data("MT_PEGAWAI", ['NID' => $id]);
	// 	$this->query->delete_data("NEW_BIND_MAN_JABATAN", ['NID' => $id]);
	// 	$this->query->delete_data("NEW_BIND_KOMPETENSI", ['NID' => $id]);
	// 	$_SESSION['success'] = 'Data berhasil dihapus';
	// 	redirect(base_url().'new/man');
	// }

	public function detail($id){
		$content['data'] = $this->query->get_query("select NVL(NEW_BIND_MAN_JABATAN.ID,0) AS BIND_JABATAN_ID, MT_PEGAWAI.NID, MT_PEGAWAI.NAMA, TABLE_DESC as UNIT, MT_PEGAWAI.JABATAN, MT_PEGAWAI.KDJABATAN, MT_JABATAN_PROYEK.NAMA AS JABATAN_PROYEK, NEW_BIND_KOMPETENSI.KOMPETENSI, MT_SUMBER_PEGAWAI.NAMA AS SUMBER_PEGAWAI
			from MT_PEGAWAI
			left join NEW_BIND_MAN_JABATAN on MT_PEGAWAI.NID = NEW_BIND_MAN_JABATAN.NID
			left join MT_UNIT on MT_PEGAWAI.UNIT = TRIM(MT_UNIT.TABLE_CODE)
			left join MT_JABATAN_PROYEK on NEW_BIND_MAN_JABATAN.ID_JABATAN_PROYEK = MT_JABATAN_PROYEK.ID_JABATAN_PROYEK
			left join NEW_BIND_KOMPETENSI on MT_PEGAWAI.NID=TRIM(NEW_BIND_KOMPETENSI.NID)
			left join MT_SUMBER_PEGAWAI on MT_PEGAWAI.ID_SUMBER_PEGAWAI = MT_SUMBER_PEGAWAI.ID_SUMBER_PEGAWAI
			where MT_PEGAWAI.NID = '".$id."'")->row();
		$content['data_kompetensi'] = $this->query->get_data_simple('NEW_BIND_KOMPETENSI', ['NID' => str_replace("~", "/", $id)])->row();
		// $content['history'] = $this->query->get_query("select DISTINCT RAB_PROYEK.NAMA_PROYEK, TO_CHAR(RAB_PROYEK.TGL_REALISASI_MULAI, 'DD Mon YYYY') TGL_MULAI, TO_CHAR(RAB_PROYEK.TGL_REALISASI_SELESAI, 'DD Mon YYYY') TGL_SELESAI
		// 	from NEW_BIND_MAN_JABATAN 
		// 	join NEW_BIND_MANPOWER on NEW_BIND_MAN_JABATAN.NID = NEW_BIND_MANPOWER.NID
		// 	join RAB_MANPOWER on NEW_BIND_MANPOWER.ID_MAN_POWER = RAB_MANPOWER.ID_MANPOWER
		// 	join RAB_RAB on RAB_MANPOWER.ID_RAB = RAB_RAB.ID_RAB
		// 	join RAB_PEKERJAAN on RAB_RAB.ID_PEKERJAAN = RAB_PEKERJAAN.ID_PEKERJAAN
		// 	join RAB_PROYEK on RAB_PEKERJAAN.ID_PROYEK = RAB_PROYEK.ID_PROYEK
		// 	where NEW_BIND_MANPOWER.nid = '".$id."'")->result();
		$this->data['content'] = $this->load->view('man/detail_penilaian', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data, FALSE);
	}
}

/* End of file Man.php */
/* Location: ./application/controllers/new/Man.php */