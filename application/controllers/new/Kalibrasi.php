<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalibrasi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('M_query', 'query');
		$this->load->model('AuthModel', 'auth');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
	}

	public function index(){
		$data['content'] = $this->load->view('tool/kalibrasi', NULL, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}
	public function load_data(){
		//preparation
		$select = "NEW_TL_JWL_KALIBRASI.ID,NEW_TL_JWL_KALIBRASI.START_DATE,NEW_TL_JWL_KALIBRASI.END_DATE,NEW_TL_TOOL_DETAIL.NO_INVENTARISASI,NEW_TL_TOOL.NAME";
		$tbl = "NEW_TL_JWL_KALIBRASI";
		$join['data'][] = [
			'table' => 'NEW_TL_TOOL_DETAIL',
			'join' => 'NEW_TL_JWL_KALIBRASI.TOOL_ID=NEW_TL_TOOL_DETAIL.ID',
			'type' => 'INNER',
		];
		$join['data'][] = [
			'table' => 'NEW_TL_TOOL',
			'join' => 'NEW_TL_TOOL_DETAIL.TOOL_ID=NEW_TL_TOOL.ID',
			'type' => 'LEFT',
		];
		$limit = array(
			'start'  => $this->input->get('start') ? $this->input->get('start') : 0,
			'finish' => $this->input->get('length') ? $this->input->get('length') : 10
		);

		$where_like['data'][] = array(
			'column' => 'NEW_TL_JWL_KALIBRASI.START_DATE,NEW_TL_JWL_KALIBRASI.END_DATE,LOWER(NEW_TL_TOOL_DETAIL.NO_INVENTARISASI),LOWER(NEW_TL_TOOL.NAME)',
			'param'	 => $this->input->get('search[value]')
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//process
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, $where);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, $where);
		// print_r($resp);
		// return;
		$response['data'] = [];
		if ($query) {
			$no = $this->input->get('start') ? $this->input->get('start') : 0;
			foreach ($query->result() as $item) {
				if ($item) {
					$style ='<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="'.base_url('/new/kalibrasi/edit/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="'.base_url('/new/kalibrasi/delete/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-ok"></span> Selesai</a> </li>';
					$st .= '</ul>
						</div>';
					;
					$response['data'][] = array(
						$no+1,
						$item->no_inventarisasi."  -  ".strtoupper($item->name),
						date_format(date_create($item->START_DATE), 'd M Y')."-".date_format(date_create($item->end_date), 'd M Y'),
						'',
						$st
					);
					$no++;
				}
			}
			
		}
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}
	public function add(){
		if ($this->input->post()) {
			$this->form_validation->set_rules([
				['field' => 'invt', 'label' => 'No Inventarisasi', 	'rules' => 'required'],
				['field' => 'start', 'label' => 'Tanggal mulai', 	'rules' => 'required'],
				['field' => 'end', 'label' => 'Tanggal selesai', 	'rules' => 'required'],
	 		]);
	 		if ($this->form_validation->run() == FALSE) { 
	 			$data['content'] = $this->load->view('tool/new_jadwal', ['error' => $this->form_validation->error_array()], TRUE);
	 			$this->load->view('layouts/dashboard', $data, FALSE);
	 		} else {
	 			$data_tool = $this->query->get_data('ID','NEW_TL_TOOL_DETAIL', ['NO_INVENTARISASI' => $this->input->post('invt')])->row();
	 			$insert_data = [
	 				'TOOL_ID' => $data_tool->id,
	 				'START_DATE' => $this->input->post('start'),
	 				'END_DATE' => $this->input->post('end'),
	 				'KETERANGAN' => $this->input->post('ket')
	 			];
	 			
	 			if ($this->query->save_data('NEW_TL_JWL_KALIBRASI', $insert_data)) {
	 				// $this->session->set_flashdata('success', 'Tool Berhasil disimpan');
	 				redirect(base_url('/new/kalibrasi'));
	 			}else{
	 				$data['content'] = $this->load->view('tool/new_jadwal', ['error' => 'gagal'], TRUE);
	 				$this->load->view('layouts/dashboard', $data, FALSE);
	 			}
	 		}
		}else{
			$data['content'] = $this->load->view('tool/new_jadwal', NULL, TRUE);
			$this->load->view('layouts/dashboard', $data, FALSE);
		}
	}

	function edit($id){
		$content['jwl'] = $this->query->get_data('*','NEW_TL_JWL_KALIBRASI', ['ID' => $id], ['table' => 'NEW_TL_TOOL', 'join' => 'NEW_TL_TOOL.ID=NEW_TL_JWL_KALIBRASI.TOOL_ID', 'type' => 'left'])->row();
		print_r($content);
		die();
		$data['content'] = $this->load->view('tool/edit_kalibrasi', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function delete($id){

	}
}

/* End of file Kalibrasi.php */
/* Location: ./application/controllers/new/Kalibrasi.php */