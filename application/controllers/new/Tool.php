<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool extends CI_Controller {
	private $data;

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->helper('intive');
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}
	//Master Tool

	public function index(){


		$data['title'] = 'Master Tool | List Data Tools';
		$data['content'] = $this->load->view('tool/master_tool_new', $content, TRUE);
		$this->load->view('layouts/dashboard', $data);
	}

	function dashboard(){
		$this->load->model('M_tool', 'mtool');

		$order_by = ' B.STATUS DESC';

		if($_SESSION[SESSION_APP]['group_id'] == 101) {
			$where['data'][] = [
				'column' => 'B.STATUS !=',
				'param'  => NULL
			];
			$content['url'] 		 = 'asman/atur_paket/';
			$data['page_title']  = 'Tool | Dashboard Asman Tools ';
		} else {
			$content['url'] 		 = 'tool/siapkan_paket/';
			$data['page_title']  = 'Tool | Dashboard Admin Tools ';
		}
		$select  = 'A.ID_PROYEK , A.NAMA_PROYEK , A.TGL_RENCANA_MULAI,  A.TGL_RENCANA_SELESAI ,';
		$select .= 'B.* ';
		$from    = 'RAB_PROYEK A ';
		$join['data'][] = [
			'table' => 'NEW_TL_PEMINJAMAN B',
			'join'  => 'A.ID_PROYEK=B.PR_ID',
			'type'  => 'LEFT',
		];
		$where['data'][] = [
			'column' => 'to_char(A.TGL_RENCANA_MULAI ,\'DD-MM-YYYY\' ) >',
			'param' => '01-01-'.date('Y')
		];
		$proyeks = $this->query->get_data_complex($select,$from,  NULL, NULL, NULL, $join, $where, null , null , $order_by);
		
		$content['proyek']   = $proyeks ? $proyeks->result(): [];
		$content['notif']    = $this->getNotif();
		$content['gap_tool'] = $this->mtool->getSummedPlan();

		$data['content'] = $this->load->view('tool/dashboard', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	public function update_detail(){
		$item_detail = $this->query->get_data_simple('NEW_TL_TOOL' , ['ID' => $this->input->post('tool_id')])->row();
		$row_inserted = 0;
		if ($this->input->post('no_inventaris') == '0') {
			// General Tools
			$quantity=  $this->input->post('quantity');
			if ( $quantity > 0) {
				for ($i=1; $i <= $quantity ; $i++) {
						if ($item_detail->kategori == 'general'  ) {
							  $code = rand(0,9).'/GNR/'.rand(0,99);
						} else if( $item_detail->kategori == 'consumable') {
								$code = rand(0,9).'/CON/'.rand(0,99);
						}
						$data_detail = [
							'NO_INVENTARISASI' => $code,
							'KONDISI'          => 1,
							'UTILITAS'         => 0,
							'SISA_UTILITAS'    => 0,
							'TOOL_ID'          => $this->input->post('tool_id'),
						];
						$s_data = $this->query->save_data('NEW_TL_TOOL_DETAIL' , $data_detail);
						if ($s_data) {
							$row_inserted++;
						}
				}
			}
		} else if ($this->input->post('quantity') == '0') {
			$data_detail = [
				'NO_INVENTARISASI' => $this->input->post('no_inventaris'),
				'KONDISI'          => 1,
				'UTILITAS'         => 0,
				'SISA_UTILITAS'    => 0,
				'TOOL_ID'          => $this->input->post('tool_id'),
			];
			$s_data = $this->query->save_data('NEW_TL_TOOL_DETAIL' , $data_detail);
			if ($s_data) {
				$row_inserted++;
			}
		}
		echo 'success input , row inserted: '.$row_inserted;
		redirect(base_url().'new/tool/detail/'.$this->input->post('tool_id'));

	}

	function insert_files(){
		$config['upload_path']          = './uploads/spec_tool/';
		$config['allowed_types']        = '*';
		// $config['max_size']             = 500;
		// $config['max_width']            = 2048;
		// $config['max_height']           = 1000;
		$config['encrypt_name'] 		= true;
		$tool_id = $this->input->post('tool_id');
		$this->load->library('upload',$config);

		$jumlah_berkas = count($_FILES['files']['name']);
		for($i = 0; $i < $jumlah_berkas;$i++)
		{
				if(!empty($_FILES['files']['name'][$i])){

				$_FILES['file']['name'] = $_FILES['files']['name'][$i];
				$_FILES['file']['type'] = $_FILES['files']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES['files']['error'][$i];
				$_FILES['file']['size'] = $_FILES['files']['size'][$i];

				if($this->upload->do_upload('file')){

					$uploadData = $this->upload->data();
					$data['FILE_PATH'] = $uploadData['file_name'];
					$data['TOOL_ID'] = $tool_id;

					$this->db->insert('NEW_TL_TOOL_FILES',$data);


				}
			}
		}

		redirect(base_url().'new/tool/detail/'.$tool_id);
	}




	function siapkan_paket($id){
		$this->load->model('M_tool', 'mtool');
		$data['page_title']      = 'Tool | Siapkan Paket Tool buat Proyek ';
		$content['kp2']          = $this->query->get_data_simple('MT_PEGAWAI', ['UNIT' => "KP2"])->result();
		$content['id_proyek']    = $id;
		$content['project']      = $this->proyek->GetByPk($id);
		$content['projects']     = $this->query->get_data_simple('RAB_PROYEK', null)->result();
		$content['trx']          = $this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['PR_ID' => $id]) ? $this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['PR_ID' => $id])->row(): [];
		$content['paket_detail'] = $this->mtool->getDetail($id);
		$content['paket']        = $this->query->get_data_simple('NEW_TL_PAKET')->result();


		$data['content'] = $this->load->view('tool/prepare_paket', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function load_data_paket_detail($paket_id , $cant_update){

		$select = "A.* , B.MERK , B.JENIS , B.KATEGORI";
		$tbl    = "NEW_TL_PAKET_DETAIL A";
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(A.ID),LOWER(A.TOOL_NAME),LOWER(A.TOOL_SPEC),LOWER(B.JENIS),LOWER(B.KATEGORI),LOWER(A.QTY)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		$join['data'][] = [
			'table' => 'NEW_TL_TOOL B',
			'join'  => 'A.TOOL_ID=B.ID' ,
			'type'  => 'INNER',
		];
		$where['data'][0]= [
			'column' => 'A.PAKET_ID',
			'param' => $paket_id
		];
		if ($paket_id == 0 ) {
			$where['data'][] = [
				'column' => 'A.USER_ID',
				'param' => $_SESSION['SESSION_APP_EBUDGETING']['user_id']	,
			];
		}
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null , NULL , null);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , NULL , null);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {

					$sql      = 'SELECT COUNT(*) AS JML FROM NEW_TL_TOOL_DETAIL WHERE KONDISI = 1 AND TOOL_ID ='.$item->tool_id;
					$tersedia = $this->query->get_query($sql);

					$tersedia = ($tersedia) ? $tersedia->row() : [];


					if ($item->qty < $tersedia->jml ) {
						$status = 'Item Tersedia';
						$act = '';
					} else if($item->qty > $tersedia->jml) {
						$status = 'Item Tidak Cukup';

						if ($item->if_tidak_cukup == 'rolling proyek') {
							$act = '<select onchange="update_tidak_cukup('.$item->id.' , this.value)" class="form-control">
									<option selected value="rolling proyek"> Rolling Proyek </option>
									<option value="beli baru"> Beli Baru </option>
								</select>';
						} else {
							$act = '<select onchange="update_tidak_cukup('.$item->id.' , this.value)" class="form-control">
									<option  value="rolling proyek"> Rolling Proyek </option>
									<option selected value="beli baru"> Beli Baru </option>
								</select>';
						}

					} else {
						$status = 'Item Cukup';
						$act = '';

					}
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="edit2('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="javascript:void(0)" onclick="delete2('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					$st .= '</ul></div>';

					if ($cant_update == '1') {
						$st = '';
					}

					// debug_query();

					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/tool/detail/'.$item->tool_id).'">'.$item->tool_name.'</a>',
						$item->tool_spec,
						$item->jenis,
						$item->kategori,
						$item->qty,
						$tersedia->jml,
						$status ,
						$item->pic_name,
						$st,

						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		echo json_encode($response);
	}







	function paket($id){


	}



	public function getNotif(){
		$date = date('d-m-Y');
		// $kalibrasi = $this->query->get_data_simple('NEW_TL_JWL_KALIBRASI', ['START_DATE >' => date('d-m-Y', strtotime("-7 day")), 'START_DATE < ' => $date]);
		$join['data'][0]['table'] = 'NEW_TL_TOOL_DETAIL';
		$join['data'][0]['join'] = "NEW_TL_JWL_KALIBRASI.TOOL_ID=NEW_TL_TOOL_DETAIL.ID";
		$join['data'][0]['type'] = "INNER";
		$join['data'][1]['table'] = "NEW_TL_TOOL";
		$join['data'][1]['join'] = "NEW_TL_TOOL_DETAIL.TOOL_ID=NEW_TL_TOOL.ID";
		$join['data'][1]['type'] = "INNER";


		$where['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) >',
			'param' => date('d-m-Y', strtotime("-7 day"))
		];

		$where['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) <',
			'param' => $date
		];

		$where['data'][] = [
			'column' => 'KATEGORI',
			'param' => 'listrik'
		];

		$where_instrument['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) >',
			'param' => date('d-m-Y', strtotime("-7 day"))
		];

		$where_instrument['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) <',
			'param' => $date
		];

		$where_instrument['data'][] = [
			'column' => 'KATEGORI',
			'param' => 'instrument'
		];

		$where_mekanik['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) >',
			'param' => date('d-m-Y', strtotime("-7 day"))
		];

		$where_mekanik['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) <',
			'param' => $date
		];

		$where_mekanik['data'][] = [
			'column' => 'KATEGORI',
			'param' => 'mekanik'
		];

		$where_predictive['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) >',
			'param' => date('d-m-Y', strtotime("-7 day"))
		];


		$where_predictive['data'][] = [
			'column' => 'to_char(START_DATE ,\'DD-MM-YYYY\' ) <',
			'param' => $date
		];

		$where_predictive['data'][] = [
			'column' => 'KATEGORI',
			'param' => 'predictive'
		];


		$kalibrasi_listrik = $this->query->get_data_complex('NEW_TL_TOOL.NAME,NEW_TL_TOOL_DETAIL.NO_INVENTARISASI,NEW_TL_TOOL_DETAIL.ID,NEW_TL_JWL_KALIBRASI.START_DATE', 'NEW_TL_JWL_KALIBRASI',  NULL, NULL, NULL, $join, $where, null);
		$kalibrasi_instrument = $this->query->get_data_complex('NEW_TL_TOOL.NAME,NEW_TL_TOOL_DETAIL.NO_INVENTARISASI,NEW_TL_TOOL_DETAIL.ID,NEW_TL_JWL_KALIBRASI.START_DATE', 'NEW_TL_JWL_KALIBRASI',  NULL, NULL, NULL, $join, $where_instrument, null);
		$kalibrasi_mekanik = $this->query->get_data_complex('NEW_TL_TOOL.NAME,NEW_TL_TOOL_DETAIL.NO_INVENTARISASI,NEW_TL_TOOL_DETAIL.ID,NEW_TL_JWL_KALIBRASI.START_DATE', 'NEW_TL_JWL_KALIBRASI',  NULL, NULL, NULL, $join, $where_mekanik, null);
		$kalibrasi_predictive = $this->query->get_data_complex('NEW_TL_TOOL.NAME,NEW_TL_TOOL_DETAIL.NO_INVENTARISASI,NEW_TL_TOOL_DETAIL.ID,NEW_TL_JWL_KALIBRASI.START_DATE', 'NEW_TL_JWL_KALIBRASI',  NULL, NULL, NULL, $join, $where_predictive, null);


		$kalibrasi_listrik    = ($kalibrasi_listrik) ? $kalibrasi_listrik->result() : false;
		$kalibrasi_instrument = ($kalibrasi_instrument) ? $kalibrasi_instrument->result() : false;
		$kalibrasi_mekanik    = ($kalibrasi_mekanik) ? $kalibrasi_mekanik->result() : false;
		$kalibrasi_predictive = ($kalibrasi_predictive) ? $kalibrasi_predictive->result() : false;

		$proyek = $this->query->get_data_simple('RAB_PROYEK', ['TGL_RENCANA_MULAI >' => date('d-m-Y', strtotime("-7 day")), 'TGL_RENCANA_MULAI <' => $date]);

		return  [
			'kalibrasi_listrik'    => $kalibrasi_listrik,
			'kalibrasi_instrument' => $kalibrasi_instrument,
			'kalibrasi_mekanik'    => $kalibrasi_mekanik,
			'kalibrasi_predictive' => $kalibrasi_predictive,
			// 'proyek' => $proyek->result(),
		];


	}


	public function prepare(){
		if ($this->input->post()) {
			print_r($this->api->fetchapi($this->input->post(), 'POST', 'proyek'));
		}else{
			$content['users'] = $this->api->fetchapi([], 'GET', 'auth')->data;
			$content['pakets'] = $this->api->fetchapi([], 'GET', 'paket')->data;
			$content['projects'] = $this->api->fetchapi([], 'GET', 'proyek')->data;
			$this->data['content'] = $this->load->view('tool/hak_tool', $content, TRUE);
			$this->load->view('layouts/dashboard', $this->data);
		}
	}

	public function new(){
		if ($this->input->post()) {
			// echo '<pre>'; print_r($this->input->post()); echo "</pre>";
			$this->form_validation->set_rules([
				// ['field' => 'invt', 'label' => 'no inventarisasi', 'rules' => 'max_length[100]|trim'],
				['field' => 'jenis', 'label' => 'Jenis', 'rules' => 'required|min_length[1]|max_length[50]|trim|in_list[general,specific,special]'],
				['field' => 'kategori',	'label' => 'kategori', 'rules' => 'required|min_length[1]|max_length[50]|trim|in_list[listrik,mekanik,instrument,predictive,consumable]'],
				['field' => 'name', 'label' => 'Name', 	'rules' => 'required|min_length[1]|max_length[50]|trim'],
				['field' => 'merk', 'label' => 'Merk / Brand', 'rules' => 'required|min_length[1]|max_length[50]|trim'],
				['field' => 'spesifikasi', 'label' => 'spesifikasi', 'rules' => 'required|min_length[1]|max_length[100]|trim'],
				['field' => 'filename', 'label' => 'gambar', 'rules' => 'max_length[50]|trim'],
				['field' => 'tgl', 'label' => 'tanggal perolehan', 'rules' => 'max_length[50]|trim'],
				// ['field' => 'qty', 'label' => 'Qty', 'rules' => 'max_length[50]|trim'],
				['field' => 'harga', 'label' => 'harga', 'rules' => 'max_length[50]|trim'],
				// ['field' => 'kondisi', 'label' => 'kondisi', 'rules' => 'required|min_length[1]|max_length[50]|trim'],
				['field' => 'lokasi', 'label' => 'lokasi', 	'rules' => 'max_length[100]|trim'],
				['field' => 'tempat', 'label' => 'tempat penyimpanan', 'rules' => 'max_length[100]|trim'],
				['field' => 'ket', 	'label' => 'keterangan', 'rules' => 'trim'],
	 		]);
	 		if ($this->form_validation->run() == FALSE) {
	 			$this->data['content'] = $this->load->view('tool/new', ['error' => $this->form_validation->error_array()], TRUE);
	 			$this->load->view('layouts/dashboard', $this->data, FALSE);
	 		} else {

	 			// print_r($this->input->post());
	 			// exit;
	 			// $resp = $this->api->fetchapi($this->input->post(), 'POST', 'tool');
	 			// echo '<pre>'; print_r($resp); echo "</pre>";
	 			// if ($resp->status == 200) {
	 			// 	// $this->session->set_flashdata('success', 'Tool Berhasil disimpan');
	 			// 	redirect(base_url('/new/tool'));
	 			// }else{
	 			// 	$this->data['content'] = $this->load->view('tool/new', ['error' => $resp->message], TRUE);
	 			// 	$this->load->view('layouts/dashboard', $this->data, FALSE);
	 			// }
	 			$post = $this->input->post();
	 			$this->store($post);
	 		}
		}else{
			$this->data['content'] = $this->load->view('tool/new', [], TRUE);
			$this->load->view('layouts/dashboard', $this->data, FALSE);
		}
	}
	public function store($post){//print_r($post);exit;
		// $this->tool->conn->StartTrans();
		$record = array();
		$record['jenis']       = $post['jenis'];
		$record['kategori']    = $post['kategori'];
		$record['name']        = $post['name'];
		$record['merk']        = $post['merk'];
		$record['spek']        = $post['spesifikasi'];
		$record['harga']       = $post['harga'];
		$record['qty']         = $post['qty'];
		$record['lokasi']      = $post['lokasi'];
		$record['penyimpanan'] = $post['tempat'];
		$record['keterangan']  = $post['ket'];
		$record['created_at']  = date('d-m-Y');
		$record['satuan']      = $post['satuan'];
		$result = $this->tool->insert($record);
		if($result){
			if($result['data']['kategori'] == 'mekanik'){
				$kategori = '1';
			}else if($result['data']['kategori'] == 'listrik'){
				$kategori = '2';
			}else if($result['data']['kategori'] == 'instrument'){
				$kategori = '3';
			}else if($result['data']['kategori'] == 'predictive'){
				$kategori = '4';
			}else{
				$kategori = '5';
			}

			if($result['data']['jenis'] == 'general' ){
				$jenis = 'GNR';
			}else if($result['data']['jenis'] == 'consumable'){
				$jenis = 'CON';
			} else {
				$result['data']['qty'] = 1;
			}

			$sql = 'select no_inventarisasi as x from new_tl_tool_detail where tool_id = (
					select max(new_tl_tool.id)
					from new_tl_tool
					inner join new_tl_tool_detail on new_tl_tool.id = new_tl_tool_detail.tool_id
					where jenis = \''.$post['jenis'].'\'
					and kategori = \''.$post['kategori'].'\'
					and name = \''.$post['name'].'\'
					and merk = \''.$post['merk'].'\'
					and spek = \''.$post['spesifikasi'].'\'
					) order by id desc';



			$cek_digit = $this->tool->GetRow($sql);
			if($cek_digit['x'] == ''){
				$cek_digit2 = $this->tool_detail->GetRow('select no_inventarisasi as x from new_tl_tool_detail where no_inventarisasi like \'%'.$no_inv.'%\'
					order by id desc');
				if($cek_digit2['x'] == ''){
					$digit = 1;
				}else{
					$cek_digit = explode('/', $cek_digit2['x']);
					$cek_digit = $cek_digit[2];
					$cek_digit = explode('.', $cek_digit);
					$digit = $cek_digit[0]+1;
				}
			}else{
				$cek_digit = explode('/', $cek_digit['x']);
				$cek_digit = $cek_digit[2];
				$cek_digit = explode('.', $cek_digit);
				$digit = $cek_digit[0];
			}

			$record_detail = array();
			$record_detail['tool_id']       = $result['data']['id'];
			// $record_detail['tgl_perolehan'] = $post['tgl'];
			$record_detail['kondisi']       = 1;

			for($i=0;$i<(int)$result['data']['qty'];$i++){
				$last_digit = $this->tool->GetRow('select count(new_tl_tool.id)+1 as x
						from new_tl_tool
						inner join new_tl_tool_detail on
						new_tl_tool.id = new_tl_tool_detail.tool_id
						where jenis  = \''.$post['jenis'].'\'
						and kategori = \''.$post['kategori'].'\'
						and name     = \''.$post['name'].'\'
						and merk     = \''.$post['merk'].'\'
						and spek     = \''.$post['spesifikasi'].'\'');
				$last_digit = $last_digit['x'];
				if($result['data']['jenis'] == 'general'){
					$record_detail['no_inventarisasi'] = $kategori.'/'.$jenis.'/'.$digit.'.'.$last_digit;
				} else if($result['data']['jenis'] == 'consumable'  ){
					$record_detail['no_inventarisasi'] = $kategori.'/'.$jenis.'/'.$digit.'.'.$last_digit;
				}else{
					$record_detail['no_inventarisasi'] = $post['invt'];
				}
				$result1 = $this->tool_detail->insert($record_detail);
			}
		}
		// $this->tool->conn->CompleteTrans();
		if($result){
			redirect(base_url('/new/tool'));
		}else{
			$this->data['content'] = $this->load->view('tool/new', ['error' => $resp->message], TRUE);
			$this->load->view('layouts/dashboard', $this->data, FALSE);
		}
	}

	function load_data( $jenis , $kat ){


		//preparation
		// $select = "NEW_TL_TOOL_DETAIL.ID, NEW_TL_TOOL_DETAIL.TOOL_ID , NEW_TL_TOOL.NAME, NEW_TL_TOOL.MERK, NEW_TL_TOOL.SPEK, NEW_TL_TOOL.JENIS, NEW_TL_TOOL.KATEGORI, TO_CHAR(NEW_TL_TOOL_DETAIL.TGL_PEROLEHAN, 'DD Mon YYYY') TGL_PEROLEHAN, NEW_TL_TOOL_DETAIL.NO_INVENTARISASI, NEW_TL_TOOL_DETAIL.KONDISI, NEW_TL_TOOL.KETERANGAN, NEW_TL_TOOL_DETAIL.UTILITAS";


		$select = "A.ID AS IDX , A.NAME , A.JENIS , A.KATEGORI, A.SPEK , A.MERK ,";
		$select .= " B.NO_INVENTARISASI, TO_CHAR(B.TGL_PEROLEHAN , 'DD mon YYYY') AS TGL_PEROLEHAN , B.NO_INVENTARISASI , B.KONDISI , A.KETERANGAN , B.UTILITAS";


		$tbl = "NEW_TL_TOOL A ";
		$join['data'][] = [
			'table' => 'NEW_TL_TOOL_DETAIL B',
			'join' => 'A.ID=B.TOOL_ID',
			'type' => 'LEFT',
		];
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(A.ID),LOWER(A.NAME),LOWER(A.MERK),LOWER(A.SPEK),LOWER(B.NO_INVENTARISASI),LOWER(A.JENIS),LOWER(A.KATEGORI),LOWER(B.TGL_PEROLEHAN),LOWER(B.KONDISI),LOWER(B.UTILITAS)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		// $order_by = 'created_at desc'
		// $order_by = null;

		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];


		// $where = null;

		if ($kat != '0') {
			$where['data'][] = [
				'column' => 'A.JENIS',
				'param'  => $kat
			];
		}

		if ($jenis != '0') {
			$where['data'][] = [
				'column' => 'A.KATEGORI',
				'param'  => $jenis
			];
		}

		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null , NULL , null , $order_by);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , NULL , null , $order_by);

		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->idx > 0) {
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="'.base_url('/new/tool/edit/'.$item->idx).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="'.base_url('/new/tool/delete/'.$item->idx).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					if ($item->jenis != 'general') {
						$st .= '<li><a href="'.base_url('/new/kalibrasi/add?invt='.$item->no_inventarisasi).'" class="waves-effect " ><span class="glyphicon glyphicon-add"></span> Jadwal kalibrasi</a> </li>';
					}
					$st .= '</ul>
						</div>';

					if ($item->kondisi == 1) {
						$kondisi  = 'Available';
					} else if($item->kondisi == 2) {
						$kondisi  = 'Rusak';
					} else if($item->kondisi == 3) {
						$kondisi  = 'Terpakai';
					} else {
						$kondisi = 'Unknown';
					}


					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/tool/detail/'.$item->idx).'">'.$item->name.'</a>',
						$item->merk,
						$item->spek,
						$item->no_inventarisasi,
						$item->jenis,
						$item->kategori,
						$item->tgl_perolehan,
						$kondisi,
						$item->utilitas ? $item->utilitas." jam" : "0 jam",
						$st
						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}


		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		// echo $this->db->last_query();
		// exit;

		echo json_encode($response);
	}

	function update_keterangan_paket_detail(){

		$data = [
			'IF_TIDAK_CUKUP' => $_POST['val']
		];
		$where = [
			'ID' => $_POST['idx']
		];

		$res = $this->query->update_data('NEW_TL_PAKET_DETAIL' , $where , $data);
		if ($res) {
			$resp = [
				'status' => 201,
				'msg' => 'Data updated!'
			];
		} else {
			$resp = [
				'status' => 400,
				'msg' => 'Data not updated!'
			];
		}

		echo json_encode($resp);

	}



	function delete_detail_item_paket($id){

		$this->query->delete_data('NEW_TL_PAKET_DETAIL' , ['ID' => $id]);

		$resp = [
			'status' => 200,
			'msg' => 'success delete'
		];

		echo json_encode($resp);

	}

	function load_detail_item_paket($id){
		ini_set('display_errors', 0);
		$join['data'][] = [
			'table' => 'NEW_TL_TOOL B',
			'join' => 'A.TOOL_ID=B.ID',
			'type' => 'INNER',
		];

		$where['data'][] = [
			'column' => 'A.ID',
			'param' => $id
		];

		$data = $this->query->get_data_complex('A.* , B.JENIS , B.KATEGORI','NEW_TL_PAKET_DETAIL A' ,NULL , NULL , NULL , $join , $where )->row();
		// echo $this->db->last_query();

		echo json_encode($data);

	}



	function load_data_new( $jenis , $kat ){
		$select = "ID , NAME , JENIS , KATEGORI, SPEK , MERK ,";
		$tbl    = "NEW_TL_TOOL";
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(ID),LOWER(NAME),LOWER(MERK),LOWER(SPEK),LOWER(JENIS),LOWER(KATEGORI)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		$where = null;
		if ($kat != '0') {
			$where['data'][] = [
				'column' => 'JENIS',
				'param'  => $kat
			];
		}
		if ($jenis != '0') {
			$where['data'][] = [
				'column' => 'KATEGORI',
				'param'  => $jenis
			];
		}
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, null, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, NULL , NULL , null , $order_by);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, NULL , NULL , null , $order_by);

		// echo $this->db->last_query();
		// exit;
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);

		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {

					// select a.id , a.name , b.no_inventarisasi , sum(b.utilitas) from new_tl_tool a ,
					// new_tl_tool_detail b where b.tool_id = a.id group by a.id , a.name , b.no_inventarisasi
					$data_row      = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL' , ['TOOL_ID' => $item->id]);

					$get_utilitas  = 'SELECT a.ID , a.NAME , SUM(b.UTILITAS) as utilitas from NEW_TL_TOOL a , NEW_TL_TOOL_DETAIL b WHERE b.TOOL_ID = a.ID and b.tool_id = '.$item->id.' GROUP BY a.ID,A.NAME';
					$ut            = $this->query->get_query($get_utilitas)->row()->utilitas;

					($ut != null) ? $utilitas = $ut : $utilitas = 0;

					$no_inv        = '- Belum ada Detail Item -';
					$tgl_perolehan = '- Belum ada Detail Item -';
					$kondisi       = '- Belum ada Detail Item -';


					if ($data_row) {
						$no_inv         = $data_row->row()->no_inventarisasi;
						$tgl_perolehan  = $data_row->row()->tgl_perolehan;
						$kondisi        = $data_row->row()->kondisi;
					}



					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="'.base_url('/new/tool/edit/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="'.base_url('/new/tool/delete/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>';
					if ($item->jenis != 'general') {
						$st .= '<li><a href="'.base_url('/new/kalibrasi/add?invt='.$no_inv).'" class="waves-effect " ><span class="glyphicon glyphicon-add"></span> Jadwal kalibrasi</a> </li>';
					}
					$st .= '</ul>
						</div>';

					if ($kondisi == 1) {
						$kondisi_t  = 'Available';
					} else if($kondisi == 2) {
						$kondisi_t  = 'Rusak';
					} else if($kondisi == 3) {
						$kondisi_t  = 'Terpakai';
					} else {
						$kondisi_t = 'Unknown';
					}
					if ($utilitas      == null) {
						$utilitas          = 0;
					}
					if ($no_inv        == '') {
						$no_inv            = '- Belum ada Detail Item -';
					}
					if ($tgl_perolehan == '') {
						$tgl_perolehan     = '- Belum ada Detail Item -';
					}
					if ($kondisi       == '') {
						$kondisi           = '- Belum ada Detail Item -';
					}

					$response['data'][] = array(
						$no+1,
						'<a href="'.base_url('/new/tool/detail/'.$item->id).'">'.$item->name.'</a>',
						$item->merk,
						$item->spek,
						ucwords($item->jenis),
						ucwords($item->kategori),
						$no_inv,
						$tgl_perolehan,
						$kondisi_t,
						$utilitas . ' Jam' ,
						$st
						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}


		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		// echo $this->db->last_query();
		// exit;

		echo json_encode($response);
	}


	function delete($id){
		$result = $this->tool_detail->delete('id = '. $id);
		if ($result) {
			redirect(base_url('/new/tool'));
		}else{
			echo $this->data->message;
		}
	}

	public function edit($id){
		// echo '<pre>'; print_r($this->api->fetchapi([], 'GET', 'tool?id='.$id)); echo "</pre>"; return;
		// $content['tool'] = $this->api->fetchapi([], 'GET', 'tool?id='.$id)->data;
		$sql = "select * from new_tl_tool
			inner join new_tl_tool_detail on new_tl_tool.id = new_tl_tool_detail.tool_id
			where new_tl_tool_detail.id = ".$id;
		$resp = $this->tool->execute($sql)->result();
		$content['tool'] = $resp[0];
		if ($this->input->post()) {

			$this->form_validation->set_rules([
				['field' => 'invt', 'label' => 'no inventarisasi', 'rules' => 'max_length[100]|trim'],
				['field' => 'jenis', 'label' => 'Jenis', 'rules' => 'required|min_length[1]|max_length[50]|trim|in_list[general,specific,special]'],
				['field' => 'kategori',	'label' => 'kategori', 'rules' => 'required|min_length[1]|max_length[50]|trim|in_list[listrik,mekanik,instrument,predictive,consumable]'],
				['field' => 'name', 'label' => 'Name', 	'rules' => 'required|min_length[1]|max_length[50]|trim'],
				['field' => 'merk', 'label' => 'Merk / Brand', 'rules' => 'required|min_length[1]|max_length[50]|trim'],
				['field' => 'spesifikasi', 'label' => 'spesifikasi', 'rules' => 'required|min_length[1]|max_length[100]|trim'],
				['field' => 'filename', 'label' => 'gambar', 'rules' => 'max_length[50]|trim'],
				['field' => 'tgl', 'label' => 'tanggal perolehan', 'rules' => 'max_length[50]|trim'],
				['field' => 'qty', 'label' => 'Qty', 'rules' => 'max_length[50]|trim'],
				['field' => 'harga', 'label' => 'harga', 'rules' => 'max_length[50]|trim'],
				['field' => 'kondisi', 'label' => 'kondisi', 'rules' => 'required|min_length[1]|max_length[50]|trim'],
				['field' => 'lokasi', 'label' => 'lokasi', 	'rules' => 'max_length[100]|trim'],
				['field' => 'tempat', 'label' => 'tempat penyimpanan', 'rules' => 'max_length[100]|trim'],
				['field' => 'ket', 	'label' => 'keterangan', 'rules' => 'trim'],
	 		]);
	 		if ($this->form_validation->run() == FALSE) {
	 			$this->data['content'] = $this->load->view('tool/update', ['error' => $this->form_validation->error_array()], TRUE);
	 			$this->load->view('layouts/dashboard', $this->data, FALSE);
	 		} else {
	 			// $update_data = $this->input->post();
	 			// $update_data['id'] = $id;
	 			// $resp = $this->api->fetchapi($update_data, 'PUT', 'tool');
	 			// echo '<pre>'; print_r($resp); echo "</pre>";
	 			// if ($resp->status == 200) {
	 			// 	// $this->session->set_flashdata('success', 'Tool Berhasil disimpan');
	 			// 	redirect(base_url('/new/tool'));
	 			// }else{
	 			// 	echo '<pre>'; print_r($resp); echo "</pre>"; return;
	 			// 	$content['error'] = $resp;
	 			// 	$this->data['content'] = $this->load->view('tool/update', $content, TRUE);
	 			// 	// $this->load->view('layouts/dashboard', $this->data, FALSE);
	 			// }
	 			$post = $this->input->post();
	 			$this->update($post);
	 		}
		}else{
			$this->data['content'] = $this->load->view('tool/update', $content, TRUE);
			$this->load->view('layouts/dashboard', $this->data, FALSE);
		}
	}

	public function autoName(){
		$like['data'][] = [
			'column' => 'LOWER(NAME)',
			'param'  => $this->input->get('term') ? strtolower(str_replace("+", " ", $this->input->get('term'))) : ''
		];
		$q = $this->query->get_data_complex('ID,NAME', 'NEW_TL_TOOL', NULL, $like)->result();
		$data = [];
		foreach ($q as $item) {
			if ($this->input->get('is_with_id')) {
				$data[] = [$item->id,$item->name];
			}else{
				$data[] = $item->name;
			}
		}
		echo json_encode($data);
		// echo json_encode($this->data);
	}
	public function update($post){
		// $this->tool->conn->StartTrans();
		$record = array();
		$record['jenis']       = $post['jenis'];
		$record['kategori']    = $post['kategori'];
		$record['name']        = $post['name'];
		$record['merk']        = $post['merk'];
		$record['spek']        = $post['spesifikasi'];
		$record['harga']       = $post['harga'];
		$record['qty']         = $post['qty'];
		$record['lokasi']      = $post['lokasi'];
		$record['penyimpanan'] = $post['tempat'];
		$record['keterangan']  = $post['ket'];
		// $record['created_at'] = date('d-m-Y');
		$result = $this->tool->update($record, 'id = '. $post['tool_id']);

		$record_detail = array();
		$record_detail['tool_id'] = $result['data']['id'];
		// $record_detail['tgl_perolehan'] = $post['tgl'];
		$record_detail['kondisi'] = $post['kondisi'];
		if($post['jenis'] == 'special'){
			$record_detail['no_inventarisasi'] = $post['invt'];
		}
		$result1 = $this->tool_detail->update($record_detail, 'id = '. $post['id']);

		// $this->tool->conn->CompleteTrans();
		if($result){
			redirect(base_url('/new/tool'));
		}else{
			$this->data['content'] = $this->load->view('tool/update', ['error' => $resp->message], TRUE);
			$this->load->view('layouts/dashboard', $this->data, FALSE);
		}
	}




    public function upload_file(){

        error_reporting(0);
        $this->load->library('PHPExcel');
        $this->load->library('upload');




        $fileName = $_FILES['fileimport']['name'];
        $config['file_name'] = $fileName;
        $config['upload_path'] = 'uploads/excel';
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 100000;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('fileimport')) {
            $this->output->set_content_type('application/json')
                ->set_status_header(406)->set_output(json_encode($this->upload->error_msg));
            return;
        }else{

            $filename = $this->upload->data();



            $inputFileName = "uploads/excel/".$config['file_name'];
            // chmod($inputFileName, 0777);
            // baca file excel
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();



            $rows = $sheet->rangeToArray("A2:L{$highestRow}", NULL, false, FALSE);

        }

        $data = [];
        if ($rows) {
            foreach ($rows as $key => $row) {
                $data[] = [
                    'name' => $row[0],
                    'spek' => $row[1],
                    'qty' => ( (int)$row[2] == null ) ? 0 : (int)$row[2] ,
                    'merk' => $row[3],
                    'kategori' => $row[4],
                    'jenis' => $row[5]
                ];
            }
        }

        foreach ($data as $tools) {
        	// print_r($tools);exit;
        	$record = array();
			$record['jenis']    = $tools['jenis'];
			$record['kategori'] = $tools['kategori'];
			$record['name']     = $tools['name'];
			$record['merk']     = $tools['merk'];
			$record['spek']     = $tools['spek'];
			$record['qty']      = $tools['qty'];
			$record['created_at'] = date('d-m-Y');
			$result = $this->tool->insert($record);
			if($result){
				if($result['data']['kategori'] == 'mekanik'){
					$kategori = '1';
				}else if($result['data']['kategori'] == 'listrik'){
					$kategori = '2';
				}else if($result['data']['kategori'] == 'instrument'){
					$kategori = '3';
				}else if($result['data']['kategori'] == 'predictive'){
					$kategori = '4';
				}else{
					$kategori = '5';
				}

				if($result['data']['jenis'] == 'general'){
					$jenis = 'GNR';
				}else{
					$jenis = 'SPF';
				}

				$sql = 'select no_inventarisasi as x from new_tl_tool_detail where tool_id = (
						select max(new_tl_tool.id)
						from new_tl_tool
						inner join new_tl_tool_detail on new_tl_tool.id = new_tl_tool_detail.tool_id
						where jenis = \''.$tools['jenis'].'\'
						and kategori = \''.$tools['kategori'].'\'
						and name = \''.$tools['name'].'\'
						and merk = \''.$tools['merk'].'\'
						and spek = \''.$tools['spek'].'\'
					) order by id desc';
				$cek_digit = $this->tool->GetRow($sql);
				if($cek_digit['x'] == ''){
					$no_inv = $kategori.'/'.$jenis.'/';
					$cek_digit2 = $this->tool_detail->GetRow('select no_inventarisasi as x from new_tl_tool_detail where no_inventarisasi like \'%'.$no_inv.'%\'
						order by id desc');
					if($cek_digit2['x'] == ''){
						$digit = 1;
					}else{
						$cek_digit = explode('/', $cek_digit2['x']);
						$cek_digit = $cek_digit[2];
						$cek_digit = explode('.', $cek_digit);
						$digit = $cek_digit[0]+1;
					}
				}else{
					$cek_digit = explode('/', $cek_digit['x']);
					$cek_digit = $cek_digit[2];
					$cek_digit = explode('.', $cek_digit);
					$digit = $cek_digit[0];
				}

				$record_detail = array();
				$record_detail['tool_id'] = $result['data']['id'];
				// $record_detail['tgl_perolehan'] = date('d-m-Y');
				$record_detail['kondisi'] = '1';
				$record_detail['utilitas'] = 0;
				for($i=0;$i<(int)$result['data']['qty'];$i++){
					$last_digit = $this->tool->GetRow('select count(new_tl_tool.id)+1 as x
							from new_tl_tool
							inner join new_tl_tool_detail on new_tl_tool.id = new_tl_tool_detail.tool_id
							where jenis = \''.$tools['jenis'].'\'
							and kategori = \''.$tools['kategori'].'\'
							and name = \''.$tools['name'].'\'
							and merk = \''.$tools['merk'].'\'
							and spek = \''.$tools['spek'].'\'');
					$last_digit = $last_digit['x'];
					if($result['data']['jenis'] != 'special'){
						$record_detail['no_inventarisasi'] = $kategori.'/'.$jenis.'/'.$digit.'.'.($i+1);

					}//echo $record_detail['no_inventarisasi'];
					$result1 = $this->tool_detail->insert($record_detail);
				}
			} //end if result
        } //end for

        if($result){
	        $response = array(
                'success' => true,
            );
    	}else{
	        $response = array(
                'success' => false,
            );
    	}
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

	public function detail($id){
		$join = [
			'table' => 'RAB_PROYEK B',
			'join'  => 'B.ID_PROYEK=A.PROYEK_ID',
			'type'  => 'INNER'
		];

		// debug_return($tool_detail->result());
		$tool_detail   = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL' , ['TOOL_ID' => $id]);
		// debug_query();
		$tools         = $this->query->get_data_simple('NEW_TL_TOOL' , ['ID' => $id]);
		$tools_history = $this->query->get_data('A.* , B.NAMA_PROYEK' , 'NEW_TL_TOOL_HISTORY A' , ['A.TOOL_ID' => $id] , $join);
		$tfiles 			 = $this->query->get_data_simple('NEW_TL_TOOL_FILES' , ['TOOL_ID' => $id]);

		$content['tool_file']   = $tfiles ? $tfiles->result() : [];
		$content['tool']         = $tools ? $tools->row() : [];
		$content['tool_history'] = $tools_history ? $tools_history->result() : [];
		$content['tool_detail']  = $tool_detail ? $tool_detail->result() : [];
		$content['tool_first']   = $tool_detail ? $tool_detail->row() : [];

		$this->data['content'] = $this->load->view('tool/detail', $content, TRUE);
		$this->load->view('layouts/dashboard', $this->data, FALSE);
	}

	function paket2($id){

		// this unused function
		$this->load->model('M_tool', 'mtool');
		$content['kp2']          = $this->query->get_data_simple('MT_PEGAWAI', ['UNIT' => "KP2"])->result();
		$content['id_proyek']    = $id;
		$content['project']      = $this->proyek->GetByPk($id);
		$content['projects']     = $this->query->get_data_simple('RAB_PROYEK', null)->result();

		$content['trx']          = $this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['PR_ID' => $id]) ?
		$this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['PR_ID' => $id])->row(): [];

		$content['paket_detail'] = $this->mtool->getDetail($id);
		$content['paket']        = $this->query->get_data_simple('NEW_TL_PAKET', ['PR_ID' => 0], null)->result();

		$data['content'] = $this->load->view('tool/rab_paket', $content, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);


	}




	function generate_paket_detail(){
		$paket_id = $_POST['paket_id'];
		$affected_rows = 0;

		$resp = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $paket_id]);

		$resp = $resp ? $resp->result() : [];

		if ( !empty($resp) ) {
			$delete_first = $this->query->delete_data('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $_POST['current_paket_id'] , 'USER_ID' => $_SESSION['SESSION_APP_EBUDGETING']['user_id'] ] );
			foreach ($resp as $key) {
				$sql      = 'SELECT COUNT(*) AS JML FROM NEW_TL_TOOL_DETAIL WHERE KONDISI = 1 AND TOOL_ID ='.$key->tool_id;
				$tersedia = $this->query->get_query($sql);
				$jml      = $tersedia ? $tersedia->row()->jml : 0;

				if ($key->qty > $jml) {
					$gacukup = 'rolling proyek';
				} else if($key->qty < $jml){
					$gacukup = '';
				} else {
					$gacukup = '';
				}

				$insert = [
					'TOOL_NAME' => $key->tool_name,
					'TOOL_SPEC' => $key->tool_spec,
					'QTY'       => $key->qty,
					'TOOL_ID'   => $key->tool_id,
					'PAKET_ID'  => $_POST['current_paket_id'],
					'USER_ID'   => $_SESSION['SESSION_APP_EBUDGETING']['user_id'],
					'IF_TIDAK_CUKUP' => $gacukup,
					'TOOL_JENIS' => $key->tool_jenis,
					'TOOL_KATEGORI' => $key->tool_kategori,
					'PIC_NAME' =>$key->pic_name,
	 			];

	 			$saved = $this->query->save_data('NEW_TL_PAKET_DETAIL' , $insert);
	 			if ($saved) {
	 				$affected_rows++;
	 			}
			}

			if ($affected_rows > 0) {
				$resp = [
					'status' => 200,
					'msg' => 'success copy the template , row inserted : '. $affected_rows

				];
			}

		} else {
			$resp = [
				'status' => 400,
				'msg' => 'fail copy the template , something error atau paket tersebut kosong'
			];


		}



		echo json_encode($resp);


		// if ($row->num_rows() > 0) {

		// 	// paket that will be overwrittend

		// 	$current_paket_id = $row->row()->id;
		// 	$delete = $this->query->delete_data('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $current_paket_id]);


		// 	//delete firsts

		// } else {
		// 	$paket = $this->query->get_data_simple('NEW_TL_PAKET' , ['ID' => $paket_id])->row();

		// 	$input_new_paket = [
		// 		'NAME' => strtoupper($paket->name).' '.$proyek->nama_proyek,
		// 		'PR_ID' => $pr_id ,
		// 	];

		// 	$this->query->save_data('NEW_TL_PAKET' , $input_new_paket);
		// 	// paket that will be overwrittend
		// 	$current_paket_id = $this->query->get_data_simple( 'NEW_TL_PAKET', [ 'PR_ID' => $pr_id ])->row()->id;

		// }
		// $paket_detail    = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $paket_id]);

		// if ($paket_detail) {

		// 	foreach($paket_detail->result() as $item){
		// 		$insert_data = [
		// 			'PAKET_ID'  => $current_paket_id,
		// 			'TOOL_NAME' => $item->tool_name,
		// 			'TOOL_SPEC' => $item->tool_spec,
		// 			'TOOL_ID' 	=> $item->tool_id,
		// 			'QTY'       => $item->qty,
		// 		];

		// 		$this->query->save_data('NEW_TL_PAKET_DETAIL' , $insert_data);
		// 	}

		// 	$json_encode = [
		// 		'status' => 200,
		// 		'msg'    => 'OK'
		// 	];

		// 	echo json_encode($json_encode);
		// }
	}


	public function copy_paket($old){
		$new 	  = $this->input->post('id');
		$data_old = $this->query->get_data_simple('NEW_TL_PAKET', ['PR_ID' => $old])->row();
		$data_new = $this->query->get_data_simple('NEW_TL_PAKET', ['PR_ID' => $new]);
		$ex		  = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $data_old->id])->result();
		$id 	  = 0;
		if ($data_new) {
			$this->query->delete_data('NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $data_old->id]);
			$id = $data_old->id;
		}else{
			$nama_proyek = strtoupper($data_old->name.' EDISI '.$this->proyek->GetByPk($id)['nama_proyek']);
			$this->query->save_data('NEW_TL_PAKET', ['NAME' => $name, 'PR_ID' => $new]);
			$id = $this->query->get_data('*', 'NEW_TL_PAKET', ['PR_ID' => $new], NULL, 'ID DESC')->row()->id;
		}
		if ($id != 0) {
			foreach ($ex as $item) {
				$insert_data = [
					'PAKET_ID' => $id,
					'TOOL_NAME' => $item->tool_name,
					'TOOL_SPEC' => $item->tool_spec,
					'QTY' => $item->qty,
				];
				$this->query->save_data('NEW_TL_PAKET_DETAIL', $insert_data);
			}
			redirect(base_url('new/tool/paket/').$new);
		}
		echo "Terjadi Kesalahan";
	}

	function dulinan(){
		$this->load->model('M_tool', 'mtool');
		$this->mtool->getSummedPlan("06-2020");
	}

	function add_io(){
		if ($this->input->post()) {
			// echo '<pre>'; print_r($this->input->post()); echo "</pre>";
			$this->form_validation->set_rules([
				['field' => 'vendor', 	'label' => 'vendor', 'rules' => 'required'],
				['field' => 'start',	'label' => 'Tgl Mulai', 'rules' => 'required'],
				['field' => 'end',	'label' => 'Tgl Selesai', 'rules' => 'required'],
	 		]);
	 		if ($this->form_validation->run() == FALSE) {
	 			$this->data['content'] = $this->load->view('tool/new_io', ['error' => $this->form_validation->error_array()], TRUE);
	 			$this->load->view('layouts/dashboard', $this->data, FALSE);
	 		} else {
	 			//insert trx
	 			$insert = [
	 				'VENDOR' => strtoupper($this->input->post('vendor')),
	 				'START_DATE' => $this->input->post('start'),
	 				'END_DATE' => $this->input->post('end'),
	 				'DIRECTION' => 'masuk'
	 			];
	 			$this->query->save_data('NEW_TL_PEMINJAMAN_IE', $insert);
	 			$trx = $this->query->get_data('*', 'NEW_TL_PEMINJAMAN_IE', ['VENDOR' => $insert['VENDOR']], NULL, 'ID DESC')->row()->id;
	 			//insert item

	 			$items = $this->query->get_data_simple('NEW_TL_CART_IE2', [])->result();
	 			foreach ($items as $i) {
	 				$insert_item = [
	 					'PEMINJAMAN_ID' => $trx,
	 					'NAME' => strtoupper($i->name),
	 					'SPEK' => $i->spek,
	 					'QTY' => $i->qty
	 				];
	 				$this->query->save_data('NEW_TL_IE', $insert_item);


	 			}
	 			$this->query->delete_data('NEW_TL_CART_IE', ['ID >' => 0]);
	 			redirect(base_url('new/tools/transaksi'));
	 		}
 		}else{
 			$content['cart'] = $this->query->get_data_simple('NEW_TL_CART_IE2', [])->result();
 			$content['proyek'] = $this->api->fetchapi([],'GET','proyek')->data;


 			// print_r($content['proyek']	);
 			// exit;
			$this->data['content'] = $this->load->view('tool/new_io', $content, TRUE);
			$this->load->view('layouts/dashboard', $this->data, FALSE);
		}
	}




	function load_io(){
		//preparation
		$select = "*";
		$tbl = "NEW_TL_PEMINJAMAN_IE";
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_IE.VENDOR),LOWER(NEW_TL_PEMINJAMAN_IE.PR_NAME),LOWER(NEW_TL_PEMINJAMAN_IE.VENDOR),LOWER(NEW_TL_PEMINJAMAN_IE.START_DATE),LOWER(NEW_TL_PEMINJAMAN_IE.END_DATE)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');

		$order_by = 'created_at desc';

		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];

		$join = null;
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, $join, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, null, null , null , null , $order_by);
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, null, null , null , null , $order_by);
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		// print_r($resp);
		// exit();
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="'.base_url('/new/tool/edit/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
								<li><a href="'.base_url('/new/tool/delete/'.$item->id).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Kembalikan</a> </li>
								</ul>
							</div>';

					if ($item->direction == 'masuk') {
						$badge = '&nbsp;<span style="color:green">(Masuk)</span>';
						if($item->status_peminjaman == 'selesai'){
							$nama = '<a href="'.base_url("new/pengembalian/detail/".$item->id).'" target="_blank">'.$item->vendor.'</a>'.$badge;
						}else{
							$nama = '<a href="'.base_url("new/pengembalian/edit/".$item->id).'" target="_blank">'.$item->vendor.'</a>'.$badge;
						}
					} else if($item->direction == 'keluar') {
						$badge = '&nbsp;<span style="color:red">(Keluar)</span>' ;
						if($item->status_peminjaman == 'selesai'){
							$nama = '<a href="'.base_url("new/pengembalian/detail/".$item->id).'" target="_blank">'.$item->vendor.'</a>'.$badge;
						}else{
							$nama = '<a href="'.base_url("new/pengembalian/edit/".$item->id).'" target="_blank">'.$item->vendor.'</a>'.$badge;
						}
					}




					$response['data'][] = array(
						$no+1,
						$nama ,
						$item->pr_name,
						$item->pic_vendor,
						$item->no_ktp,
						$item->start_date,
						$item->end_date,
						// $st
						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = $query_total->num_rows();
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		// echo  $this->db->last_query();

		// exit;

		echo json_encode($response);
	}
	function get_tool_by_kategori(){

		$where = [];
		if ($_GET['jenis'] != '') {
			$where['JENIS'] = $_GET['jenis'];
		}

		if ($_GET['kategori'] != '') {
			$where['KATEGORI'] = $_GET['kategori'];
		}

		$res = $this->query->get_data_simple('NEW_TL_TOOL' ,  $where );
		if ($res) {
			$data = $res->result();
		} else {
			$data = [];
		}


		echo json_encode($data);
	}

	function get_tool_by_kategori_transaksi_ie(){
		$join['table'] = 'NEW_TL_TOOL B';
		$join['join']  = 'A.TOOL_ID=B.ID';
		$join['type']  = 'INNER';

		$group = null;
		$datax = $this->query->get_data('A.NO_INVENTARISASI,B.NAME,B.SPEK,A.ID', 'NEW_TL_TOOL_DETAIL A',
			[
				'B.KATEGORI' => strtolower($this->input->get('kategori')),
				'A.KONDISI' => 1
			], $join , null , null , $group);
		// echo $this->db->last_query();



		// exit;

		// $res = $this->query->get_data_simple('NEW_TL_TOOL' , ['KATEGORI' => $_GET['kategori'] ]);
		if ($datax) {
			$data = $datax->result();
		} else {
			$data = [];
		}


		echo json_encode($data);
	}

	function check_stock(){

			$tool_id = $_GET['tool_id'];

			$stock   = $this->query->get_query('SELECT COUNT(*) AS JML FROM NEW_TL_TOOL_DETAIL WHERE TOOL_ID = '.$tool_id.' AND KONDISI = 1')->row();

			$resp = [
				'status' => 200,
				'msg'    => 'success bisa cek stok ',
				'stock'  => $stock->jml
			];

			echo json_encode($resp);
	}

	function get_old_jwl(){
		$data_tool = $this->query->get_data('ID','NEW_TL_TOOL_DETAIL', ['NO_INVENTARISASI' => $this->input->get('invt')])->row();
		$data = $this->query->get_data_simple('NEW_TL_JWL_KALIBRASI', ['TOOL_ID' => $data_tool->id]);
		echo json_encode($data->result());
	}

	function report(){

	}




}

/* End of file Tool.php */
/* Location: ./application/controllers/new/Tool.php */
