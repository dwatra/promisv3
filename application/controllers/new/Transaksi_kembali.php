<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_kembali extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->library('form_validation');
		$this->load->model('M_tool', 'mtool');
		$this->load->helper('intive');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}


	function debug(){
		echo "<pre>";
		echo $this->db->last_query();

	}

	function index($id = null){

	}

	function pengembalian_transaksi($trxid){
			$table  = 'NEW_TL_PEMINJAMAN A';
			$join['data'][0]['table']   = 'NEW_TL_PAKET B';
			$join['data'][0]['join']    = 'A.PAKET_ID = B.ID';
			$join['data'][0]['type']    = 'INNER';
			$join['data'][1]['table']   = 'RAB_PROYEK C';
			$join['data'][1]['join']    = 'A.PR_ID = C.ID_PROYEK';
			$join['data'][1]['type']    = 'INNER';
			$where['data'][0]['column'] = 'A.PR_ID';
			$where['data'][0]['param']  =  $trxid;

			// $where['data'][0]['column'] = 'B.IS_KEMBALI';
			// $where['data'][0]['param']  = '0';

			$content['trx']          = $this->query->get_data_complex('A.* ,B.* , C.NAMA_PROYEK AS PROYEK , ',$table, NULL,NULL,NULL,$join,$where)->row();
			// debug_query();
			$content['pic_general']  = $this->query->get_data_simple('MT_PEGAWAI' , ['NID' => $content['trx']->pic_general ]);
			$content['pic_specific'] = $this->query->get_data_simple('MT_PEGAWAI' , ['NID' => $content['trx']->pic_specific ]);
			$content['pic_special']  = $this->query->get_data_simple('MT_PEGAWAI' , ['NID' => $content['trx']->pic_special ]);

			// jika disetujui dan belum kembali == proyek udah lagi berjalan dan menunggu pengembalian
			if ($content['trx']->status  == 'disetujui' && $content['trx']->is_kembali == 0) {
				$content['status'] = '<span class="label label-warning">PROYEK BELUM KEMBALI</span>';
				$data['content'] = $this->load->view('tool/transaksi/pengembalian_transaksi_only_view', $content, TRUE);

			// proyek belum di setujui dan belum kembali == proyek belum berjalan
			} else if($content['trx']->status  != 'disetujui' && $content['trx']->is_kembali == 0) {
				$content['status'] = '<span class="label label-danger">PROYEK BELUM DISETUJUI </span>';
				$data['content'] = $this->load->view('tool/transaksi/pengembalian_transaksi_only_view', $content, TRUE);

			// proyek disetujui dan sudah kembali == waktunya pengecekan list
		  } else if($content['trx']->status  == 'disetujui' && $content['trx']->is_kembali == 1){

				$content['status'] = '<span class="label label-success">PROYEK SUDAH KEMBALI </span>';
				$data['content'] = $this->load->view('tool/transaksi/pengembalian_transaksi', $content, TRUE);
			} else {

				exit('imposible if proyek not yet statrted but its already back');
			}



			// print_r($content['trx']);
			// $data['content'] = $this->load->view('tool/transaksi/pengembalian_transaksi', $content, TRUE);


			$this->load->view('layouts/dashboard', $data, FALSE);
	}

		function load_barang_hilang($trxid , $process){
			$select  = " A.* , B.KATEGORI , B.JENIS , B.MERK ";
			$tbl     = "NEW_TL_PAKET_DETAIL_ITEM A";

			$join['data'][] = [
				'table' => 'NEW_TL_PAKET_DETAIL C',
				'join' =>  'C.ID=A.PAKET_DETAIL_ID',
				'type' =>  'RIGHT',
			];

			$join['data'][] = [
				'table' => 'NEW_TL_PAKET D',
				'join' =>  'D.ID=C.PAKET_ID',
				'type' =>  'LEFT',
			];

			$join['data'][] = [
				'table' => 'NEW_TL_TOOL B',
				'join' =>  'B.ID=A.TOOL_ID',
				'type' =>  'RIGHT',
			];

			$where['data'][] = [
				'column' => 'D.PR_ID' ,
				'param'  => $trxid ,
			];

			$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , null , null );

			if ($query) {
				foreach ($query->result() as $keys) {

					if ($keys->tool_inventaris != 'Beli' && $keys->tool_inventaris != 'Rolling Proyek' && $keys->tool_inventaris != 'Belum ada' && $keys->tool_inventaris != '' && $keys->tool_inventaris != null && $keys->tool_kembali == 0  ) {

						echo "<tr>";
							echo "<td>".$keys->tool_name."</td>";
							echo "<td>".$keys->tool_inventaris."</td>";
							echo "<td>".$keys->tool_spec."</td>";
							echo "<td>".$keys->merk."</td>";
							echo "<td> 1 </td>";
							echo "<td>".$keys->keterangan_kembali."</td>";
							if ($process == 'process') {
								echo "<td> <input maxlength='20' type='text' value='".$keys->penyebab."' class='itungx form-control' onchange='save_penyebab(".$keys->id." , this.value)'> </td>";
								# code...
							} else {
								echo "<td>".$keys->penyebab."</td>";
							}
						echo "</tr>";


					} else {
						echo "<tr>";
							echo "<td>".$keys->tool_name."</td>";
							echo "<td>".$keys->tool_inventaris."</td>";
							echo "<td>".$keys->tool_spec."</td>";
							echo "<td>".$keys->tool_merk."</td>";
							echo "<td colspan='3'> Tool Aman </td>";
						echo "</tr>";
					}

				}
			}

		}

		function load_data_detail($trxid , $process , $jenis = null){
			// echo $process;
			// exit;
			$select  = " A.* , B.KATEGORI , B.JENIS , C.PIC_NAME";
			$tbl     = "NEW_TL_PAKET_DETAIL_ITEM A";
			$limit   = array(
				'start'  => $this->input->get('start'),
				'finish' => $this->input->get('length')
			);
			$where_like['data'][] = array(
				'column' => 'LOWER(A.ID),LOWER(A.TOOL_NAME),LOWER(A.TOOL_SPEC),LOWER(B.JENIS),LOWER(B.KATEGORI),LOWER(A.TOOL_INVENTARIS),LOWER(A.TOOL_KEMBALI),LOWER(A.KETERANGAN_KEMBALI)',
				'param'	 => strtolower($this->input->get('search[value]'))
			);
			$index_order = $this->input->get('order[0][column]');
			$join['data'][] = [
				'table' => 'NEW_TL_PAKET_DETAIL C',
				'join' =>  'C.ID=A.PAKET_DETAIL_ID',
				'type' =>  'RIGHT',
			];
			$join['data'][] = [
				'table' => 'NEW_TL_PAKET D',
				'join' =>  'D.ID=C.PAKET_ID',
				'type' =>  'LEFT',
			];
			$join['data'][] = [
				'table' => 'NEW_TL_TOOL B',
				'join' =>  'B.ID=A.TOOL_ID',
				'type' =>  'RIGHT',
			];
			$where['data'][] = [
				'column' => 'D.PR_ID' ,
				'param'  => $trxid ,
			];
			if ($jenis == null) {
				$where['data'][] = [
					'column' => 'B.KATEGORI !=' ,
					'param'  => 'specific' ,
				];
			} else {
				$where['data'][] = [
					'column' => 'B.KATEGORI' ,
					'param'  => 'specific' ,
				];
			}


			$order['data'][] = [
				'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
				'type'	 => $this->input->get('order[0][dir]')
			];
			//datatable
			$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, $join, $where);
			$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null , null , null );
			$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , null , null );
			$recordsTotal = 0;
			$response['data'] = array();
			$resp = $query ? $query->result() : [];
			if ($resp <> false) {
				$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
				foreach ($resp as $item) {
					if ($item->id > 0) {
						// $st = '<div class="dropdown" style="display:inline">
						// 		<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
						// 			<span class="glyphicon glyphicon-option-vertical"></span>
						// 		</a>
						// 		<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
						// 			<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
						// 			</ul>
						// 		</div>';

						// if ($process == 'process') {
						// 	# code...
						// 	if ($item->tool_kembali == 0) {
						//
						// 	$select = '<input id="check_'.$item->id.'" type="checkbox" onclick="must_check('.$item->id.',this.id)" name="">';
						// 		# code...
						// 	} else {
						//
						// 	$select = '<input id="check_'.$item->id.'" type="checkbox" checked onclick="must_check('.$item->id.',this.id)" name="">';
						//
						// 	}
						// } else if($process == 'view') {
						// }
						if ($item->tool_kembali == 0) {
							$select = 'Belum Kembali';
						} else {

							$select = 'Sudah Kembali';
						}



						if ($item->tool_inventaris == 'Beli') {
							$trg = '<select onchange="update_inv('.$item->id.',this.value)" class="form-control">
							<option>Belum ada</option>
							<option>Rolling Proyek</option>
							<option selected>Beli</option>';
							$invent = 0;
						} else if($item->tool_inventaris == 'Rolling Proyek'){

							$trg = '<select onchange="update_inv('.$item->id.',this.value)" class="form-control">
							<option>Belum ada</option>
							<option selected>Rolling Proyek</option>
							<option>Beli</option>
						</select>';
						$invent = 0;

						}
						 else if($item->tool_inventaris == 'Belum ada') {
						$trg = '<select onchange="update_inv('.$item->id.',this.value)" class="form-control">
							<option selected>Belum ada</option>
							<option>Rolling Proyek</option>
							<option>Beli</option>
						</select>';
						$invent = 0;
						} else {
							$trg = $item->tool_inventaris;
							$invent = 1;
						}

						// if ($process == 'process') {
						// 	$ket = '<input readonly type="text" class="form-control" onchange="update_ket('.$item->id.' , this.value)" placeholder="Rusak / Hilang " value="'.$item->keterangan_kembali.'">';
						// 	# code...
						// } else {

						// }
						$ket = '<label>'.$item->keterangan_kembali.'</label>';

						if ($invent == 1) {
							// code...
							if ($item->kategori == 'specific') {

								$trg = $item->tool_inventaris;
							} else {
								$trg = 'tidak ada';
							}
						}





						$response['data'][$no] = array(
							$item->id,
							$item->tool_name ,
							$item->tool_spec,
							$item->jenis,
							$item->kategori,
							$select,
							$trg,
							$ket,
							// $item->keterangan_kembali,
							// $st,
						);

						if ($jenis != null) {
							$response['data'][$no][8] = $item->pic_name;
						}


						$no++;
						$recordsTotal++;
					}
				}
			}

			$response['recordsTotal'] =$query_total ? $query_total->num_rows() : 0;
			$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
			echo json_encode($response);
		}

		function trx_cek($type , $trx_id ){

			$get = $this->query->get_data_simple('NEW_TL_PAKET' , ['PR_ID' => $trx_id])->row();
			$got2 = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $get->id])->result();

			foreach ($got2 as $val) {

				$paket_detail_id = $val->id ;
				$where =[
					'PAKET_DETAIL_ID' => $paket_detail_id
				];

				if ($type == 'check') {
					$update = [
						'TOOL_KEMBALI' => 1
					];
					# code...
				} else {

					$update = [
						'TOOL_KEMBALI' => 0
					];
				}

				$this->query->update_data('NEW_TL_PAKET_DETAIL_ITEM' , $where , $update );

			}

			$resp = [
				'status' => 200,
				'msg' => 'success'
			];

			echo json_encode($resp);
		}

		function update_detail($id ){
			$where =[
				'ID' => $id
			];

			$update = [
				'TOOL_INVENTARIS' => $_POST['val']
			];

			$this->query->update_data('NEW_TL_PAKET_DETAIL_ITEM' , $where , $update );

			$resp = [
				'status' => 200,
				'msg' => 'success'
			];

			echo json_encode($resp);

		}

		function update_detail_ket($id){
			$where =[
				'ID' => $id
			];

			$update = [
				'KETERANGAN_KEMBALI' => $_POST['val']
			];



			$this->query->update_data('NEW_TL_PAKET_DETAIL_ITEM' , $where , $update );

			// self::debug();
			$resp = [
				'status' => 200,
				'msg' => 'success'
			];

			echo json_encode($resp);
		}

		function update_stat_check($id){
			$where =[
				'ID' => $id
			];

			if ($_POST['val'] == 'cek') {
				# code...
				$update = [
					'TOOL_KEMBALI' => 1
				];
			} else {
				$update = [
					'TOOL_KEMBALI' => 0
				];

			}

			$this->query->update_data('NEW_TL_PAKET_DETAIL_ITEM' , $where , $update );

			// self::debug();
			$resp = [
				'status' => 200,
				'msg' => 'success'
			];

			echo json_encode($resp);
		}

		function update_penyebab($id){
			$where =[
				'ID' => $id
			];

			$update = [
				'PENYEBAB' => $_POST['val']
			];

			$this->query->update_data('NEW_TL_PAKET_DETAIL_ITEM' , $where , $update );

			// self::debug();
			$resp = [
				'status' => 200,
				'msg' => 'success'
			];

			echo json_encode($resp);
		}

		function simpan_ba(){

			$data_update = [
				'KRONOLOGIS' => $_POST['txt_kro'],
				'USULAN' => $_POST['txt_usu'],
				'IS_KEMBALI' => 1
			];

			$where =[
				'ID' =>$_POST['paket_id'],
				'PR_ID' => $_POST['pr_id'],
			];

			$this->query->update_data('NEW_TL_PAKET' , $where , $data_update);

			$resp =[
				'status' => 200,
				'msg' => 'success'
			];

			echo json_encode($resp);


		}


		function berita_acara($trx_id){

			$select  = " A.* , B.KATEGORI , B.JENIS , B.MERK ";
			$tbl     = "NEW_TL_PAKET_DETAIL_ITEM A";

			$join['data'][] = [
				'table' => 'NEW_TL_PAKET_DETAIL C',
				'join' =>  'C.ID=A.PAKET_DETAIL_ID',
				'type' =>  'RIGHT',
			];

			$join['data'][] = [
				'table' => 'NEW_TL_PAKET D',
				'join' =>  'D.ID=C.PAKET_ID',
				'type' =>  'LEFT',
			];

			$join['data'][] = [
				'table' => 'NEW_TL_TOOL B',
				'join' =>  'B.ID=A.TOOL_ID',
				'type' =>  'RIGHT',
			];

			$where['data'][] = [
				'column' => 'D.PR_ID' ,
				'param'  => $trx_id ,
			];

			$where['data'][] = [
				'column' => 'A.TOOL_KEMBALI >' ,
				'param'  =>  0,
			];


			$query      = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , null , null );

			$paket      = $this->query->get_data_simple('NEW_TL_PAKET' , ['PR_ID'      => $trx_id] );
			$peminjaman = $this->query->get_data_simple('NEW_TL_PEMINJAMAN' , ['PR_ID' => $trx_id] );
			$pegawai_general    = $this->query->get_data_simple('MT_PEGAWAI' , ['NID' => $peminjaman->row()->pic_general ] );
			$pegawai_special    = $this->query->get_data_simple('MT_PEGAWAI' , ['NID' => $peminjaman->row()->pic_specific ] );
			$proyek     = $this->query->get_data_simple('RAB_PROYEK' , ['ID_PROYEK'    => $trx_id ] );


			$data['item']            = ($query) ? $query->result():                  [];
			$data['paket']           = ($paket) ? $paket->row():                     [];
			$data['peminjaman']      = ($peminjaman) ? $peminjaman->row():           [];
			$data['pegawai_general'] = ($pegawai_general) ? $pegawai_general->row(): [];
			$data['pegawai_special'] = ($pegawai_special) ? $pegawai_special->row(): [];
			$data['proyek']          = ($proyek) ? $proyek->row():                   [];


			  // $this->load->library('pdf');
		    // $this->pdf->setPaper('A4', 'potrait');
		    // $this->pdf->filename = "BERITA_ACARA.pdf";
			  // $this->pdf->load_view('tool/transaksi/berita_acara_pengembalian', $data, FALSE);
				$this->load->view('tool/transaksi/berita_acara_pengembalian' ,$data);
		    // $this->pdf->load_view('pdf/pdf_surat_jalan', $data);
		}

		function test_ba(){
			$this->load->view('tool/transaksi/berita_acara_pengembalian');
		}

		function tgl_indo($tanggal){
			$bulan = array (
				1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
			$pecahkan = explode('-', $tanggal);

			// variabel pecahkan 0 = tanggal
			// variabel pecahkan 1 = bulan
			// variabel pecahkan 2 = tahun

			return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
		}

}
