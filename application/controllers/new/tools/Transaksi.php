<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('Api', 'api');
		$this->load->model('M_query', 'query');
		$this->load->library('form_validation');
		$this->load->model("Rab_proyekModel","proyek");
	}

	public function index()
	{
		$join['table'] = 'RAB_PROYEK';
		$join['join'] = 'RAB_PROYEK.ID_PROYEK=NEW_TL_PEMINJAMAN.PR_ID';
		$join['type'] = 'INNER';
		$content['trx'] = $this->query->get_data('*', 'NEW_TL_PEMINJAMAN', [], $join)->result();
		// echo "<pre>";
		// print_r($content['trx']);
		// exit;
		$data['content'] = $this->load->view('tool/master_transaksi', $content, TRUE);
		$this->load->view('layouts/dashboard', $data);
	}



	function progress_add_item(){

		$data_save_temp = [
			'PEMINJAMAN_ID' => 0,
			'TOOL_NAME'     => $_POST['tool'],
			'TOOL_SPEC'     => $_POST['spek'],
			'QTY'           => $_POST['jml'],
			'USER_ID'       => $_SESSION['SESSION_APP_EBUDGETING']['user_id'] ,
		];

		$this->query->save_data('NEW_TL_PEMINJAMAN_TOOL' , $data_save_temp);

		$resp = [
			'status' => 200 ,
			'msg' => 'success save data'
		];

		echo json_encode($resp);
	}


	function load_temporary_data_meminjamnkan(){
		$select  = "*";
		$tbl     = "NEW_TL_PEMINJAMAN_TOOL";
		$limit   = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);

		$where_like['data'][] = array(
			'column' => 'LOWER(NEW_TL_PEMINJAMAN_TOOL.ID),LOWER(NEW_TL_PEMINJAMAN_TOOL.TOOL_NAME),LOWER(NEW_TL_PEMINJAMAN_TOOL.TOOL_SPEC),LOWER(NEW_TL_PEMINJAMAN_TOOL.QTY)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);

		$index_order = $this->input->get('order[0][column]');

		$where['data'][] = [
			'column' => 'USER_ID' ,
			'param'  => $_SESSION['SESSION_APP_EBUDGETING']['user_id']
		];

		$where['data'][] = [
			'column' => 'PEMINJAMAN_ID' ,
			'param'  => 0
		];


		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		//datatable
		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, null, null, $where);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, null, $where, null , null , null );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, null, $where, null , null , null );
		// $resp = $this->api->fetchapi([],'GET',  'tool?'.$_SERVER['QUERY_STRING']);
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];
		// print_r($resp);
		// exit();
		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id > 0) {
					$st = '<div class="dropdown" style="display:inline">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
								<span class="glyphicon glyphicon-option-vertical"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
								<li><a href="javascript:void(0)" onclick="delete_this('.$item->id.')" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Hapus</a> </li>
								</ul>
							</div>';

					$response['data'][] = array(
						$item->id,
						$item->tool_name ,
						$item->tool_spec,
						$item->qty,
						$st
						// '<a href="'.base_url('/new/tool/edit/'.$item->ID).'"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;'.
						// '<a href="'.base_url('/new/tool/delete/'.$item->ID).'"><span class="text-danger glyphicon glyphicon-trash"></span></a>'
					);
					$no++;
					$recordsTotal++;
				}
			}
		}

		$response['recordsTotal'] = count($query_total->result());
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;
		echo json_encode($response);
	}



	public function load_vendors(){
		if ( isset($_GET['searchTerm']) ) {
			$add = '?searchTerm='.$_GET['searchTerm'];
		} else {
			$add = '';
		}

		$response = [];

		$result = $this->api->fetchapi([] , 'GET' , 'vendors'.$add)->data;

		// print_r($result);
		// exit;

		if($result){
			foreach ($result as $key ) {
				$response[] = [
					'id' =>  $key->NAME,
					'text' =>  $key->NAME,
				];

			}
		}
		echo json_encode($response);
	}

	public function load_data()
	{
		$data = $this->api->fetchapi([], 'GET', 'transaksi?'.$_SERVER['QUERY_STING']);
		$response['data'] = [];
		if ($data->data != false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($data->data as $trx) {
				$response['data'][] = [
					$no+1,
					$trx->NAMA,
					$trx->START_DATE.' - '.$trx->END_DATE,
					strtoupper($trx->PIC),
					'<p class="pfile"><a target="_BLANK" href="http://localhost:8080/pjbs-api/assets/upload/tool/transaksi/doc/'.$trx->DOC.'">'.$trx->DOC.'</a></p>',
					$trx->NAME,
					'<div class="dropdown" style="display:inline">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
							<span class="glyphicon glyphicon-option-vertical"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
							<li><a href="'.base_url('/new/tool/edit/'.$trx->MAIN_ID).'" class="waves-effect " ><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
							<li><a href="'.base_url('/new/tool/delete/'.$trx->MAIN_ID).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>
						</ul>
					</div>'
				];
				$no++;
			}
		}
		$response['recordsTotal']    = $data->recordsTotal;
		$response['recordsFiltered'] = $data->recordsFiltered;
		echo json_encode($response);
	}

	public function load_data_surat()
	{
		$data = $this->api->fetchapi([], 'GET', 'transaksi/file?'.$_SERVER['QUERY_STING']);
		$response['data'] = [];
		if ($data->data != false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($data->data as $trx) {
				$response['data'][] = [
					$no+1,
					$trx->NAMA,
					$trx->START_DATE.' - '.$trx->END_DATE,
					$trx->NAME,
					'<p class="pfile"><a target="_BLANK" href="http://localhost:8080/pjbs-api/assets/upload/tool/transaksi/doc/'.$trx->DOC.'">'.$trx->DOC.'</a></p>',
					'-	',
					'<div class="dropdown" style="display:inline">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#1f91f3;display:inline-block;">
							<span class="glyphicon glyphicon-option-vertical"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu$no" style="min-width: 10px; margin-top:-20px">
							<li><a href="'.base_url('/new/transaksi/delete/file'.$item->ID).'" class="waves-effect " ><span class="glyphicon glyphicon-remove"></span> Delete</a> </li>
						</ul>
					</div>'
				];
				$no++;
			}
		}
		$response['recordsTotal'] = $data->recordsTotal;
		$response['recordsFiltered'] = $data->recordsFiltered;
		echo json_encode($response);
	}
	public function realisasi($id){
		$insert_data = [
			'STATUS' => 'menunggu persetujuan',
			'PR_ID' => $id,
			'CREATED_AT' => date('d-m-Y'),
		];
		if ($this->query->save_data('NEW_TL_PEMINJAMAN', $insert_data)) {
			redirect($_SERVER['HTTP_REFERER']);
		}
		echo "Terjadi Kesalahan";
	}
	public function reject($id){
		if ($this->query->delete_data('NEW_TL_PEMINJAMAN' , ['ID' => $id])) {
			redirect($_SERVER['HTTP_REFERER']);
		}
		echo "Terjadi Kesalahan";
	}
	public function accept($trx){
		$trx = $this->query->get_data_simple('NEW_TL_PEMINJAMAN', ['ID' => $trx])->row();
		$req_paket = $this->query->get_data_simple('NEW_TL_PAKET', ['PR_ID' => $trx->pr_id])->row();
		$req_items = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $req_paket->id])->result();
		$items = [];
		foreach ($req_items as $req_item) {
			$tool = $this->query->get_data_simple('NEW_TL_TOOL', ['LOWER(NAME)' => strtolower($req_item->tool_name), 'LOWER(SPEK)' => strtolower($req_item->tool_spec)])->row();
			for ($i = 0; $i < $req_item->qty; $i++) {
				$item = $this->query->get_data_simple('NEW_TL_TOOL_DETAIL', ['TOOL_ID' => $tool->id])->row();
				$insert_data = ['PEMINJAMAN_ID' => $trx->id, 'TOOL_ID' => $item->id];
				if ($this->query->update_data('NEW_TL_TOOL_DETAIL' ,['ID' => $item->id], ['KONDISI' => 3]) and $this->query->save_data('NEW_TL_PEMINJAMAN_DETAIL' , $insert_data)) {
					$items[] = ['tool_name' => $req_item->tool_name, 'tool' => $item->id];
				}
			}
		}
		$update_data = ['STATUS' => 'disetujui'];
		if ($this->query->update_data('NEW_TL_PEMINJAMAN', ['ID' => $trx->id], $update_data)) {
			// redirect(base_url('new/tool/paket/').$trx->pr_id);
			$this->generate_surat_jalan($trx->pr_id);
		}else{
			echo "Terjadi Kesalahan";
		}


	}

	public function generate_surat_jalan($id){
		$picgnr = $this->query->get_data_simple('MT_PEGAWAI', ['NID' => $this->input->post('picgnr')]);
		$picspe = $this->query->get_data_simple('MT_PEGAWAI', ['NID' => $this->input->post('picspe')]);
		$picspc = $this->query->get_data_simple('MT_PEGAWAI', ['NID' => $this->input->post('picspc')]);

		$trx = $this->query->get_data_simple('NEW_TL_PAKET', ['PR_ID' => $id])->row();

		$join['table'] = 'NEW_TL_TOOL';
		$join['join'] = '(LOWER(NEW_TL_TOOL.NAME)=LOWER(NEW_TL_PAKET_DETAIL.TOOL_NAME ) AND LOWER(NEW_TL_TOOL.SPEK)=LOWER(NEW_TL_PAKET_DETAIL.TOOL_SPEC))';
		$join['type'] = 'INNER';
		$data['proyek'] = $this->proyek->GetByPk($id);
		//general
		$data['general']['items'] = $this->query->get_data('*', 'NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $trx->id, 'NEW_TL_TOOL.JENIS' => 'general'], $join);
		$data['general']['pic'] = $picgnr->row()->nama;
		//specific
		$data['specific']['items'] = $this->query->get_data('*', 'NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $trx->id, 'NEW_TL_TOOL.JENIS' => 'specific'], $join);
		$data['specific']['pic'] = $picspe->row()->nama;
		//special
		$data['special']['items'] = $this->query->get_data('*', 'NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $trx->id, 'NEW_TL_TOOL.JENIS' => 'special'], $join);
		$data['special']['pic'] = $picspc->row()->nama;
		// echo json_encode($this->query->get_data('*', 'NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $trx->id, 'NEW_TL_TOOL.JENIS' => 'specific'], $join)->result());
		$this->load->view('pdf/pdf_surat_jalan', $data, FALSE);
		$this->load->library('pdf');
	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = "surat_jalan.pdf";
	    $this->pdf->load_view('pdf/pdf_surat_jalan', $data);
	}


}
/* End of file Transaksi.php */
/* Location: ./application/controllers/new/tool/Transaksi.php */
