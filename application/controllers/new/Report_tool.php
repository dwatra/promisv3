<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_tool extends CI_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('AuthModel', 'auth');
		$this->load->model('M_query', 'query');
		$this->load->model('Api', 'api');
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("New_tl_toolModel","tool");
		$this->load->model("New_tl_tool_detailModel","tool_detail");
		$this->load->model("M_tool","mtool");
		$this->load->helper('intive');
		$this->load->library('form_validation');
		if (!isset($_SESSION[SESSION_APP])) {
			redirect(base_url());
		}
	}

	function index(){
    // exit('hello');
    $data['page_title'] = 'Tool | Laporan Tool';

		$data['content'] = $this->load->view('tool/report/index_report_tool', null, TRUE);
		$this->load->view('layouts/dashboard', $data, FALSE);
	}

	function load_data($date_start =  null , $date_end = null ){
		$select = "A.ID_PROYEK , A.NAMA_PROYEK , A.TGL_RENCANA_MULAI , A.TGL_RENCANA_SELESAI , C.NAME , C.IS_KEMBALI , C.ID AS PAKET_ID , B.STATUS  ";
		$tbl    = "RAB_PROYEK A";
		$limit = array(
			'start'  => $this->input->get('start'),
			'finish' => $this->input->get('length')
		);
		$where_like['data'][] = array(
			'column' => 'LOWER(A.ID_PROYEK),LOWER(A.NAMA_PROYEK),LOWER(A.TGL_RENCANA_MULAI),LOWER(A.TGL_RENCANA_SELESAI),LOWER(C.NAME),LOWER(B.STATUS)',
			'param'	 => strtolower($this->input->get('search[value]'))
		);
		$index_order = $this->input->get('order[0][column]');
		$order['data'][] = [
			'column' => $this->input->get('columns[' . $this->input->get('order[0][column]') . '][name]'),
			'type'	 => $this->input->get('order[0][dir]')
		];
		$join['data'][] = [
			'table' => 'NEW_TL_PEMINJAMAN B',
			'join'  => 'A.ID_PROYEK=B.PR_ID' ,
			'type'  => 'LEFT',
		];
		$join['data'][] = [
			'table' => 'NEW_TL_PAKET C',
			'join'  => 'A.ID_PROYEK=C.PR_ID' ,
			'type'  => 'LEFT',
		];
		// $join['data'][] = [
		// 	'table' => 'NEW_TL_PAKET_DETAIL D',
		// 	'join'  => 'C.ID=D.PAKET_ID' ,
		// 	'type'  => 'INNER',
		// ];

		if ($date_start == null) {
			 $date_start = date('d-m-Y');
		}
		if ($date_end == null) {
			 $date_end   = "31-12-".date('Y');
		}
		$where['data'][] = [
			'column' => 'to_char(A.TGL_RENCANA_MULAI ,\'DD-MM-YYYY\' ) >=',
			'param' => $date_start
		];
		$where['data'][] = [
			'column' => 'to_char(A.TGL_RENCANA_MULAI ,\'DD-MM-YYYY\' ) <=',
			'param' => $date_end
		];
		// $orderby = ' C.STATUS DESC ';

		$query_total  = $this->query->get_data_complex($select, $tbl, NULL, NULL, NULL, $join, $where, null);
		$query_filter = $this->query->get_data_complex($select, $tbl, NULL, $where_like, $order, $join, $where, null , NULL );
		$query        = $this->query->get_data_complex($select, $tbl, $limit, $where_like, $order, $join, $where, null , NULL );
		$recordsTotal = 0;
		$response['data'] = array();
		$resp = $query ? $query->result() : [];

		if ($resp <> false) {
			$no = $this->input->get('start') ? $this->input->get('start') + 1 : 0;
			foreach ($resp as $item) {
				if ($item->id_proyek > 0) {

					if ($item->status != null) {
							$tool = $this->_get_count_tool( $item->paket_id );
							if ($item->is_kembali == 0) {
									$tool_kembali  = 0;
									$tool_dipinjam = $tool['tool_dipinjam'];
							} else {
									$tool_kembali  = $tool['tool_kembali'];
									$tool_dipinjam = $tool['tool_dipinjam'];
							}
					} else {
							$tool_kembali  = 0;
							$tool_dipinjam = 0;
					}

					// $btn = '<button class="btn btn-primary"> <i class="glyphicon glyphicon-search"></i> </button>';


					$response['data'][] = array(
						$item->id_proyek,
						$item->nama_proyek,
						$item->tgl_rencana_mulai,
						$item->tgl_rencana_selesai,
						$item->name,
						$item->status,
						$tool_dipinjam,
						$tool_kembali,
						// $btn,
					);
					$no++;
					$recordsTotal++;
				}
			}
		}
		$response['recordsTotal'] = $query_total ? $query_total->num_rows() : 0;
		$response['recordsFiltered'] = $query_filter ? $query_filter->num_rows() : 0;

		echo json_encode($response);
	}

	private function  _get_count_tool( $paket_id ){
		$where['data'][] = ['column' => 'PAKET_ID' , 'param' => $paket_id ];
		$get           = $this->query->get_data_complex('sum(qty) as count_peminjaman', 'NEW_TL_PAKET_DETAIL' , null , null, null, null, $where)->row();
		$set           = $this->query->get_data_complex('*', 'NEW_TL_PAKET_DETAIL' , null , null, null, null, $where)->result();
		$total_kembali = 0;
		foreach ($set as $items) {
			$q              = 'SELECT count(*) as tool_kembali FROM NEW_TL_PAKET_DETAIL_ITEM WHERE PAKET_DETAIL_ID = '.$items->id.' AND TOOL_KEMBALI = 1 ';
			$count          = $this->query->get_query($q)->row();
			$total_kembali += $count;
		}
		$return = [
			'tool_dipinjam'=> $get->count_peminjaman,
			'tool_kembali' => $total_kembali
		];
		return $return;
	}

}
