<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Mt_item_detail extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/mt_item_detaillist";
		$this->viewdetail = "panelbackend/mt_item_detaildetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah Nilai Item';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit Nilai Item';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail Nilai Item';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Nilai Item';
		}

		$this->load->model("Mt_item_detailModel","model");
		$this->load->model("Mt_itemModel","mtitem");

		
		$this->load->model("Mt_unitModel","mtunit");
		$this->data['mtunitarr'] = $this->mtunit->GetCombo();
		$this->load->model("Mt_itemModel","mtitemmodel");

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'select2'
		);
		$this->data['width'] = "900px";
	}

	protected function Header(){
		return array(
			array(
				'name'=>'kode', 
				'label'=>'Kode', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'id_unit', 
				'label'=>'Unit', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['mtunitarr'],
			),
			array(
				'name'=>'nilai', 
				'label'=>'Nilai', 
				'width'=>"auto",
				'type'=>"number",
			),
			array(
				'name'=>'url', 
				'label'=>'URL', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'created_date', 
				'label'=>'Tgl Input', 
				'width'=>"auto",
				'type'=>"date",
			),
		);
	}

	protected function Record($id=null){
		return array(
			'kode'=>$this->post['kode'],
			'id_unit'=>$this->post['id_unit'],
			'nilai'=>$this->post['nilai'],
			'url'=>$this->post['url'],
		);
	}

	protected function Rules(){
		return array(
			"kode"=>array(
				'field'=>'kode', 
				'label'=>'Kode', 
				'rules'=>"max_length[20]",
			),
			"id_unit"=>array(
				'field'=>'id_unit', 
				'label'=>'Unit', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['mtunitarr']))."]|max_length[18]",
			),
			"nilai"=>array(
				'field'=>'nilai', 
				'label'=>'Nilai', 
				'rules'=>"required|integer",
			),
			"url"=>array(
				'field'=>'url', 
				'label'=>'URL', 
				'rules'=>"max_length[1000]",
			),
		);
	}

	public function Index($id_item=null, $page=0){
		$this->data['header']=$this->Header();

		$this->_beforeDetail($id_item);

		$this->_setFilter("id_item = ".$this->conn->escape($id_item));

		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_item"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'uri_segment'=>1,
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;

		$this->View($this->viewlist);
	}

	public function Edit($id_item=null, $id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_item, $id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_item'] = $id_item;

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_item/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id_item, $id);

		$this->View($this->viewdetail);
	}

	public function Add($id_item=null){
		$this->Edit($id_item);
	}

	public function Detail($id_item=null, $id=null){

		$this->_beforeDetail($id_item, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id_item, $id);

		$this->View($this->viewdetail);
	}

	public function Delete($id_item=null, $id=null){

        $this->model->conn->StartTrans();

        $this->_beforeDetail($id_item, $id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");
			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_item");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_item/$id");
		}

	}

	protected function _beforeDetail($id_item=null, $id=null){
		$this->data['rowheader1'] = $this->mtitemmodel->GetByPk($id_item);
		
		$this->data['add_param'] .= $id_item;

		$this->data['layout_header'] = "<a class='btn btn-success btn-sm' href='".site_url('panelbackend/mt_item')."'>List Item</a><br/><h5>".($this->data['rowheader1']['keterangan_spec']?$this->data['rowheader1']['keterangan_spec']:$this->data['rowheader1']['nama'])."</h5>";
	}

}