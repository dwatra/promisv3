<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Sp3 extends _adminController{
	public $limit = 5;
	public $limit_arr = array('5','10','30','50','100');

	public function __construct(){
		parent::__construct();
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->plugin_arr = array(
			'select2','upload','datepicker'
		);

		$this->data['no_header'] = false;
	}

	protected function init(){
		parent::init();
	}

	function Index($page=0){
		$this->data['page_title'] = 'Monitoring RAB';
		$this->View("panelbackend/sp3list");
	}

	function Edit($id=null){
		$this->data['width'] = "1200px";
		$this->data['edited'] = true;
		$this->data['page_title'] = 'SP3';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/sp3detail");
	}

	function Detail($id=null){
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'SP3';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/sp3detail");
	}
}
