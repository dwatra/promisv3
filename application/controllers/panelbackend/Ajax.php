<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Ajax extends _adminController{

	public function __construct(){
		parent::__construct();
	}

	public function set_toggle(){
		$_SESSION[SESSION_APP]['toggle'] = ($this->get['collapse']?0:1);
	}

	public function listpegawai($unit=null, $subdit=null){
		$data = array("results"=>array());

		$q = $this->conn->escape_str(strtolower($_GET['q']));
		$unit = $this->conn->escape(trim(urldecode($unit)));
		$subdit = $this->conn->escape(trim(urldecode($subdit)));

		if($this->is_pengadaan)
			$unit = $_SESSION[SESSION_APP]['unit'];

		if($q){
			$sql = "select nid as id, nama as text 
				from mt_pegawai 
				where 1=1 ";
			/*if($unit!=="''")
				$sql .= " and unit=$unit ";

			if($subdit!=="''")
				$sql .= " and subdit=$subdit ";*/

			$sql .= " and  lower(nama) like '%$q%' and rownum <= 10";

			$data['results'] = $this->conn->GetArray($sql);
		}

		echo json_encode($data);
	}

	public function listitem($unit=null){
		$data = array("results"=>array());

		$q = $this->conn->escape_str(strtolower(str_replace(' ','%',$_GET['q'])));
		$unit = $this->conn->escape(trim(urldecode($unit)));

		if($this->is_pengadaan)
			$unit = $_SESSION[SESSION_APP]['unit'];

		if($q){
			$sql = "select i.id_item as id, i.nama as text, id.nilai, id.url
				from mt_item i join mt_item_detail id on i.id_item = id.id_item
				where 1=1 ";

			if($unit!=="''")
				$sql .= " and id.id_unit=$unit ";

			$sql .= " and  lower(i.nama) like '%$q%' and rownum <= 10";

			$data['results'] = $this->conn->GetArray($sql);
		}

		echo json_encode($data);
	}

	public function listitemspec($unit=null){
		$data = array("results"=>array());

		$q = $this->conn->escape_str(strtolower(str_replace(' ','%',$_GET['q'])));
		$unit = $this->conn->escape(trim(urldecode($unit)));

		if($this->is_pengadaan)
			$unit = $_SESSION[SESSION_APP]['unit'];

		if($q){
			$sql = "select a.* from (select i.id_item as id, nvl(i.keterangan_spec, i.nama) as text, id.nilai, i.id_spec_item, 0 as is_new
				from mt_item i left join mt_item_detail id on i.id_item = id.id_item
				where 1=1 ";

			if($unit!=="''")
				$sql .= " and id.id_unit=$unit ";

			$sql .= " and lower(nvl(i.keterangan_spec,i.nama)) like '%$q%' and rownum <= 10
			union 
			select id_spec_item as id, nama as text, null as nilai, id_spec_item, 1 as is_new
			from mt_spec_item 
			where lower(nama) like '%$q%' and rownum <= 10 ) a
			order by id_spec_item, id desc";

			$data['results'] = $this->conn->GetArray($sql);
		}

		echo json_encode($data);
	}

	public function listspec($unit=null){
		$data = array("results"=>array());

		$q = $this->conn->escape_str(strtolower(str_replace(' ','%',$_GET['q'])));
		$unit = $this->conn->escape(trim(urldecode($unit)));

		if($this->is_pengadaan)
			$unit = $_SESSION[SESSION_APP]['unit'];

		if($q){
			$sql = "select id_spec_item as id, nama as text
			from mt_spec_item 
			where lower(nama) like '%$q%' and rownum <= 10
			order by id desc";

			$data['results'] = $this->conn->GetArray($sql);
		}

		echo json_encode($data);
	}
}