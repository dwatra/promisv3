<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Mt_pos_anggaran extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/mt_pos_anggaranlist";
		$this->viewdetail = "panelbackend/mt_pos_anggarandetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->data['width'] = "800px";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah POS Anggaran';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit POS Anggaran';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail POS Anggaran';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar POS Anggaran';
		}

		$this->load->model("Mt_pos_anggaranModel","model");
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			''
		);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
		);
	}

	protected function Record($id=null){
		return array(
			'nama'=>$this->post['nama'],
		);
	}

	protected function Rules(){
		return array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"max_length[200]",
			),
		);
	}

}