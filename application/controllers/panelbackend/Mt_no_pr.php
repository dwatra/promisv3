<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Mt_no_pr extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/mt_no_prlist";
		$this->viewdetail = "panelbackend/mt_no_prdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah NO PR';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit NO PR';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail NO PR';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar NO PR';
		}

		$this->load->model("Mt_no_prModel","model");
		$this->load->model("Mt_jenis_no_prModel","mtjenisnopr");
		$this->data['mtjenisnoprarr'] = $this->mtjenisnopr->GetCombo();

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			''
		);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'id_jenis_no_pr', 
				'label'=>'Jenis NO PR', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['mtjenisnoprarr'],
			),
			array(
				'name'=>'tahun', 
				'label'=>'Tahun', 
				'width'=>"auto",
				'type'=>"number",
			),
			array(
				'name'=>'no', 
				'label'=>'NO', 
				'width'=>"auto",
				'type'=>"number",
			),
		);
	}

	protected function Record($id=null){
		return array(
			'id_jenis_no_pr'=>$this->post['id_jenis_no_pr'],
			'tahun'=>$this->post['tahun'],
			'no'=>$this->post['no'],
		);
	}

	protected function Rules(){
		return array(
			"id_jenis_no_pr"=>array(
				'field'=>'id_jenis_no_pr', 
				'label'=>'Jenis NO PR', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['mtjenisnoprarr']))."]",
			),
			"tahun"=>array(
				'field'=>'tahun', 
				'label'=>'Tahun', 
				'rules'=>"integer",
			),
			"no"=>array(
				'field'=>'no', 
				'label'=>'NO', 
				'rules'=>"integer",
			),
		);
	}

}