<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Wbs extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/wbs_planlist";
		$this->viewdetail = "panelbackend/wbs_plandetail";
		$this->viewcetak = "panelbackend/wbs_cetak";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		if ($this->mode == 'add') {
			// $this->data['width'] = "1200px";
			$this->data['page_title'] = 'Tambah Rencana';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			// $this->data['width'] = "1200px";
			$this->data['page_title'] = 'Edit Rencana';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			// $this->data['width'] = "1200px";
			$this->data['page_title'] = 'Detail Rencana';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Rencana';
		}

		// $this->data['no_header'] = true;

		$this->load->model("Wbs_planModel","model");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'datepicker','select2','upload'
		);

		$this->access_role = array('add'=>1, 'edit'=>1, 'delete'=>1, 'detail'=>1, 'list'=>1, 'reset'=>1, 'save'=>1, 'batal'=>1);
	}

	function Index($id_pekerjaan=0, $page=0){
		$this->_beforeDetail($id_pekerjaan);

		$this->data['header']=$this->Header();

		//$this->_setFilter("id_plan = ".$this->conn->escape($id_plan));
		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_pekerjaan"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;

		$this->View($this->viewlist);
	}

	function readMPP($file) {
		require_once($this->config->item("url_java"));

		echo $dest = FCPATH."uploads/".time().".mpp";
		move_uploaded_file ($file , $dest);
		chmod($dest, 0777);

		$project = new java('javaapplication1.JavaApplication1');
		$ret = java_values($project->importMPP($dest));

		$arr = json_decode($ret, true);
		ksort($arr);
		unlink($dest);

		return $arr;

	}


	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama Rencana', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			// array(
			// 	'name'=>'no_prk', 
			// 	'label'=>'No. PRK', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'id_tipe_pekerjaan', 
			// 	'label'=>'Tipe Pekerjaan', 
			// 	'width'=>"auto",
			// 	'type'=>"list",
			// 	'value'=>$this->data['mttipepekerjaanarr'],
			// ),
			// array(
			// 	'name'=>'no_pekerjaan', 
			// 	'label'=>'No. SP3', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'tgl_pekerjaan', 
			// 	'label'=>'Tgl. SP3', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'no_kontrak', 
			// 	'label'=>'No. Kontrak', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'tgl_kontrak', 
			// 	'label'=>'Tgl. Kontrak', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'nilai_hpp', 
			// 	'label'=>'Nilai HPP', 
			// 	'width'=>"auto",
			// 	'type'=>"number",
			// ),
			// array(
			// 	'name'=>'tgl_mulai_rab', 
			// 	'label'=>'Pengerjaan RAB', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'durasi', 
			// 	'label'=>'Durasi', 
			// 	'width'=>"auto",
			// 	'type'=>"polos",
			// ),
		);
	}

	protected function Record($id=null){
		$return = array(
			'nama'=>$this->post['nama'],
		);

		// $pic = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_pic'])));

		// $return['nama_pic'] = $pic['nama'];
		// $return['jabatan_pic'] = $pic['jabatan'];

		return $return;
	}

	protected function Rules(){
		return array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama Rencana', 
				'rules'=>"required",
			),
		);
	}

	public function Add($id_pekerjaan=0){
		$this->Edit($id_pekerjaan);
	}

	public function Edit($id_pekerjaan=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_pekerjaan,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_pekerjaan'] = $id_pekerjaan;

			$this->_isValid($record,true);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_pekerjaan=null, $id=null){
		$this->_beforeDetail($id_pekerjaan, $id);
		unset($this->access_role['add']);
		unset($this->access_role['delete']);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Delete($id_pekerjaan=null, $id=null){
        $this->model->conn->StartTrans();

        $this->_beforeDetail($id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_pekerjaan");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");
		}

	}

	protected function _beforeDetail($id_pekerjaan=null, &$id=null){
		$id = $this->conn->GetOne("select max(id_plan) from wbs_plan where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$this->data['row'] = $this->model->GetByPk($id);

		$this->data['id_pekerjaan'] = $id_pekerjaan;

		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_pekerjaan;

		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['id_rab'] = $id_rab = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);

		if (!$this->data['rowheader2']) $this->NoData('Tidak ada master RAB');

		# TODO: sementara
		$this->access_role = array('add'=>1, 'edit'=>1, 'delete'=>1, 'detail'=>1, 'list'=>1, 'reset'=>1, 'save'=>1, 'batal'=>1);
	}

	protected function _afterDetail($id){
		$id = (int) $id;

		$id_plan = $this->data['id_plan'];

		$cek = $this->conn->GetOne("select 1 from wbs_plan where id_plan = ".$this->conn->escape($id_plan));

		if ($id) {
			$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai, max(selesai)-min(mulai) as total_hari from wbs_plan_detail where id_plan=$id ";
			$times = $this->conn->GetRow($sql);
			$mulai = $times['mulai'];
			$selesai = $times['selesai'];
			$total_hari = $times['total_hari'];

			$sql = "select t.*, extract(day from t.selesai-t.mulai) as hari, to_char(t.mulai, 'dd-mm-yyyy hh24:mi') as mulai_str, to_char(t.selesai, 'dd-mm-yyyy hh24:mi') as selesai_str, to_char(t.last_update_realisasi, 'dd-mm-yyyy hh24:mi') as last_update_realisasi_str,
			t.mulai-t2.rencanaharipertama as rencanahari1, t.selesai-t2.rencanaharipertama as rencanahari2,
			t.last_update_realisasi-t3.realisasiharipertama as realisasihari
			from wbs_plan_detail t, (select min(mulai) as rencanaharipertama from wbs_plan_detail where id_plan=$id) t2,
			(select min(last_update_realisasi) as realisasiharipertama from wbs_plan_detail where id_plan=$id) t3 
			where t.id_plan=$id order by urutan";
			$details = $this->conn->GetArray($sql);
			$parents = array();
			foreach ($details as &$detail) {
				# menghitung tingkat
				if (!$detail['urutan_parent']) {
					$detail['tingkat'] = $parents[$detail['urutan']] = 0;
				}
				else {
					$detail['tingkat'] = $parents[$detail['urutan']] = $parents[$detail['urutan_parent']] + 1;
				}
			}


			$this->data['details'] = $details;

		}
	}

	protected function _afterInsert($id=null){

		############################

		$ret = true;

		if ($_FILES) {
			$file = $_FILES['file1']['tmp_name'];
			// $file = "/media/solikul/Data/a.mpp";
			$details = $this->readMPP($file);

			foreach ($details as $key => $value) {
				$id_detail = (int) $key;
				$urutan_parent = (int) $value[0]['parent'][0];
				$nama = $value[0]['name'][0];
				$start = date('d-m-Y H:i:s', strtotime($value[0]['start'][0]));
				$finish = date('d-m-Y H:i:s', strtotime($value[0]['finish'][0]));
				$is_leaf = $value[0]['isleaf'][0];
				$record = array(
					'id_plan'=>(int) $id,
					'urutan'=>$id_detail,
					'nama'=>$nama,
					'mulai'=>$start,
					'selesai'=>$finish,
					'rencana'=>0,
					'realisasi'=>0,
					'urutan_parent'=>$urutan_parent,
					'is_leaf'=>$is_leaf,
				);
		        $sql = $this->conn->InsertSQL('wbs_plan_detail', $record);
		        if($sql){
				    $ret = $this->conn->Execute($sql);
				}
			}

			$this->_hitungDetail($id);
			$this->_hitungRencanaParent($id);
		}

		###########################
		

		// if($ret)
		// 	$ret = $this->newRab($id);

		return $ret;
	}

	protected function _afterUpdate($id){
		if ($_FILES) {
			$id = (int) $id;
			$file = $_FILES['file1']['tmp_name'];
			// $file = "/media/solikul/Data/a.mpp";
			$details = $this->readMPP($file);

			foreach ($details as $key => $value) {
				$id_detail = (int) $key;
				$urutan_parent = (int) $value[0]['parent'][0];
				$nama = $value[0]['name'][0];
				$start = date('d-m-Y H:i:s', strtotime($value[0]['start'][0]));
				$finish = date('d-m-Y H:i:s', strtotime($value[0]['finish'][0]));
				$is_leaf = $value[0]['isleaf'][0];
				$record = array(
					'id_plan'=> $id,
					'urutan'=>$id_detail,
					'nama'=>$nama,
					'mulai'=>$start,
					'selesai'=>$finish,
					'urutan_parent'=>$urutan_parent,
					'is_leaf'=>$is_leaf,
				);
		        $sql = $this->conn->UpdateSQL('wbs_plan_detail', $record, "id_plan=$id and urutan=$id_detail");
		        if($sql){
				    $ret = $this->conn->Execute($sql);
				}
			}

			$this->_hitungDetail($id);
			$this->_hitungRencanaParent($id);
		}

		$ret = true;
		
		// if($ret)
		// 	$ret = $this->_delsertFiles($id);
		
		// if($ret)
		// 	$ret = $this->_delsertTTD($id);
		
		return $ret;
	}
/*
	private function _delsertFiles($id_pekerjaan = null){
		$ret = true;

		if(!empty($this->post['sp3'])){
			foreach($this->post['sp3']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}

		if(!empty($this->post['kontrak'])){
			foreach($this->post['kontrak']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}

		if(!empty($this->post['file'])){
			foreach($this->post['file']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}
		return $ret;
	}

	private function _delsertTTD($id_pekerjaan = null){
		$ret = $this->conn->Execute("delete from rab_pekerjaan_ttd where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$MainSpecarr = array();

		if(!empty($this->post['ttd'])){
			foreach ($this->post['ttd'] as $key => $v) {
				if(!$v['nid'])
					continue;

				if(!$ret)
					break;

				$record = array();
				$record['id_pekerjaan'] = $id_pekerjaan;
				$record['nid'] = $v['nid'];
				$row_pegawai = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape($v['nid']));
				$record['nama'] = $row_pegawai['nama'];
				$record['jabatan'] = $row_pegawai['jabatan'];

				$ret = $this->conn->goInsert('rab_pekerjaan_ttd', $record);
			}
		}

		return $ret;
	}
*/	
	function _hitungDetail($id) {
		$this->conn->Execute("update wbs_plan_detail set rencana = null where id_plan=$id");

		$sql = "select min(mulai) as mulai, max(selesai) as selesai from wbs_plan_detail where id_plan=$id ";
		$times = $this->conn->GetRow($sql);
		$mulai = $times['mulai'];
		$selesai = $times['selesai'];
		$total_jam = (strtotime($selesai) - strtotime($mulai))/3600;

		$tgl_plans = array();
		$mulai_int = strtotime($mulai);
		for ($i=0;$i<$total_jam;$i++) {
			# menghitung bobot rencana
			$tanggal = strtotime("+$i hours", $mulai_int);
			$tgl_plans[date('Y-m-d H', $tanggal)] = array();
		}

		$tgl_plans1 = array();

		foreach ($tgl_plans as $key => $value) {
			$sql = "select urutan from wbs_plan_detail where id_plan=$id and (to_char(mulai, 'yyyy-mm-dd HH24') <= '$key' and to_char(selesai, 'yyyy-mm-dd HH24') >= '$key')  and is_leaf='1' ";
			$rows = $this->conn->GetArray($sql);
			foreach ($rows as $row) {
				$tgl_plans1[$key][] = $row['urutan'];
			}
		}

		$bobot_perjam = 100/count($tgl_plans1);

		$bobotjams = array();
		foreach($tgl_plans1 as $key=>$r){
			$bobotjams[$key] = $bobot_perjam/count($r);
		}

		$bobots = array();
		foreach($tgl_plans1 as $key=>$r){
			foreach($r as $k=>$v){
				$bobots[$v]+=$bobotjams[$key];
			}
		}

		foreach ($bobots as $urutan => $bobot_rencana) {
			$sql = "update wbs_plan_detail set rencana=$bobot_rencana where id_plan=$id and urutan=$urutan";
			$ret = $this->conn->Execute($sql);
		}

	}

	function _hitungRencanaParent($id_plan, $id_parent=false) {
		if ($id_parent === false) 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and is_leaf=1 order by urutan";
		else 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and urutan_parent=$id_parent order by urutan";

		$rows = $this->conn->GetArray($sql);
		$list_rencana = array();
		$list_parent = array();
		foreach ($rows as $row) {
			if ($row['urutan_parent'] == 0) return false;

			$list_rencana[$row['urutan_parent']] += (float)$row['rencana'];
			$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan={$row['urutan_parent']}";
			$new_urutan_parent = $this->conn->GetOne($sql);
			if (!in_array($new_urutan_parent, $list_parent)) {
				$list_parent[] = $new_urutan_parent;
			}
		}

		foreach ($list_rencana as $urutan_parent=>$rencana) {
			$sql = "update wbs_plan_detail set rencana=$rencana where id_plan=$id_plan and urutan=$urutan_parent and is_leaf=0";
			$this->conn->Execute($sql);
		}

		foreach ($list_parent as $urutan_parent) {
			$this->_hitungRencanaParent($id_plan, $urutan_parent);
		}

	}

	function _hitungRealisasiParent($id_plan, $id_parent=false) {
		if ($id_parent === false) 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and is_leaf=1 order by urutan";
		else 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and urutan_parent=$id_parent order by urutan";

		$rows = $this->conn->GetArray($sql);
		$list_realisasi = array();
		$list_parent = array();
		foreach ($rows as $row) {
			if ($row['urutan_parent'] == 0) return false;

			$list_realisasi[$row['urutan_parent']] += $row['realisasi'];
			$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan={$row['urutan_parent']}";
			$new_urutan_parent = $this->conn->GetOne($sql);
			if (!in_array($new_urutan_parent, $list_parent)) {
				$list_parent[] = $new_urutan_parent;
			}
		}

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		foreach ($list_realisasi as $urutan_parent=>$realisasi) {
			$sql = "update wbs_plan_detail set realisasi=$realisasi, progress=($realisasi/rencana*100), last_update_realisasi=sysdate, modified_by_desc = '$user_name', modified_by = $user_id where id_plan=$id_plan and urutan=$urutan_parent and is_leaf=0";
			$this->conn->Execute($sql);
		}

		foreach ($list_parent as $urutan_parent) {
			$this->_hitungRealisasiParent($id_plan, $urutan_parent);
		}

	}

	function kurva($id_pekerjaan, $id, $inline=false, $no_layout=false) {
		$this->_beforeDetail($id_pekerjaan);

		$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai, max(selesai)-min(mulai) as total_hari,
		to_char(min(last_update_realisasi), 'yyyy-mm-dd') as realisasi_mulai, to_char(max(last_update_realisasi), 'yyyy-mm-dd') as realisasi_akhir,
		max(last_update_realisasi)-min(last_update_realisasi) as total_hari_realisasi
		from wbs_plan_detail where id_plan=$id ";
		$times = $this->conn->GetRow($sql);
		$mulai = strtotime($times['mulai'])-(3600*24);

		if($mulai>strtotime($times['realisasi_mulai']))
			$mulai = strtotime($times['realisasi_mulai']);

		$selesai = $times['selesai'];

		// if($this->data['rowheader1']['sysdate1'])
		
		if($selesai<strtotime($times['realisasi_akhir']))
			$selesai = strtotime($times['realisasi_akhir']);

		$total_hari = (($selesai-$mulai)/(3600*24));

		$sql = "select t.*, extract(day from t.selesai-t.mulai) as hari from wbs_plan_detail t where t.id_plan=$id order by urutan";
		$details = $this->conn->GetArray($sql);
		$parents = array();
		foreach ($details as &$detail) {
			# menghitung tingkat
			if (!$detail['urutan_parent']) {
				$detail['tingkat'] = $parents[$detail['urutan']] = 0;
			}
			else {
				$detail['tingkat'] = $parents[$detail['urutan']] = $parents[$detail['urutan_parent']] + 1;
			}
		}

		$plan_per_day = array();
		$realisasi_per_day = array();

		$tgl_plans = array();
		$mulai_int = ($mulai);
		for ($i=0;$i<=$total_hari;$i++) {
			$tanggal = strtotime("+$i day", $mulai_int);
			$tgl_plans[date('Y-m-d', $tanggal)] = array();
		}

		$is_realisasi = true;
		foreach ($tgl_plans as $key => $value) {
			$sql = "select sum(rencana) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and to_char(selesai, 'yyyy-mm-dd') <= '$key'  and is_leaf='1')";
			$plan_per_day[$key] = (float)$this->conn->GetOne($sql);
		}
		ksort($plan_per_day);

		foreach ($tgl_plans as $key => $value) {
			$sql = "select sum(nvl(realisasi,0)) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and to_char(last_update_realisasi, 'yyyy-mm-dd') <= '$key'  and is_leaf='1')";
			$realisasi_per_day[$key] = (float)$this->conn->GetOne($sql);
		}
		ksort($realisasi_per_day);

		$times['total_hari'] = $total_hari;
		$this->data['times'] = $times;
		$this->data['plan_per_day'] = $plan_per_day;
		$this->data['realisasi_per_day'] = $realisasi_per_day;
		$this->data['modified_by_desc'] = $this->conn->GetOne("select modified_by_desc 
			from wbs_plan_detail where id_plan = $id and modified_by_desc is not null and last_update_realisasi is not null order by last_update_realisasi desc");

		if (!$inline) {
			$this->View('panelbackend/wbs_kurva');
		}

		if ($no_layout){
			$this->data['no_layout'] = $no_layout;
			echo $this->PartialView('panelbackend/wbs_kurva', true);
		}
	}

	function kurva1($id_pekerjaan, $id, $inline=false, $no_layout=false) {
		$this->_beforeDetail($id_pekerjaan);

		$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai, max(selesai)-min(mulai) as total_hari,
		to_char(min(last_update_realisasi), 'yyyy-mm-dd') as realisasi_mulai, to_char(max(last_update_realisasi), 'yyyy-mm-dd') as realisasi_akhir,
		max(last_update_realisasi)-min(last_update_realisasi) as total_hari_realisasi
		from wbs_plan_detail where id_plan=$id ";
		$times = $this->conn->GetRow($sql);
		$mulai = $times['mulai'];
		$selesai = $times['selesai'];
		$total_hari = $times['total_hari']+1;

		$realisasi_mulai = $times['realisasi_mulai'];
		$realisasi_akhir = $times['realisasi_akhir'];
		$total_hari_realisasi = (int)$times['total_hari_realisasi']+1;

		$sql = "select t.*, extract(day from t.selesai-t.mulai) as hari from wbs_plan_detail t where t.id_plan=$id order by urutan";
		$details = $this->conn->GetArray($sql);
		$parents = array();
		foreach ($details as &$detail) {
			# menghitung tingkat
			if (!$detail['urutan_parent']) {
				$detail['tingkat'] = $parents[$detail['urutan']] = 0;
			}
			else {
				$detail['tingkat'] = $parents[$detail['urutan']] = $parents[$detail['urutan_parent']] + 1;
			}
		}

		$plan_per_day = array();
		$realisasi_per_day = array();

		$tgl_plans = array();
		$mulai_int = strtotime($mulai);
		for ($i=0;$i<=$total_hari;$i++) {
			$tanggal = strtotime("+$i day", $mulai_int);
			$tgl_plans[date('Y-m-d', $tanggal)] = array();
		}

		$is_realisasi = true;
		foreach ($tgl_plans as $key => $value) {
			$sql = "select sum(rencana) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and to_char(selesai, 'yyyy-mm-dd') <= '$key'  and is_leaf='1')";
			$plan_per_day[$key] = $this->conn->GetOne($sql);
		}
		ksort($plan_per_day);

		$tgl_realisasi = array();
		$realisasi_mulai_int = strtotime($realisasi_mulai);
		for ($i=0;$i<=$total_hari_realisasi;$i++) {
			$tanggal = strtotime("+$i day", $realisasi_mulai_int);
			$tgl_realisasi[date('Y-m-d', $tanggal)] = array();
		}

		foreach ($tgl_realisasi as $key => $value) {
			$sql = "select sum(nvl(realisasi,0)) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and (to_char(last_update_realisasi, 'yyyy-mm-dd') <= '$key')  and is_leaf='1')";
			if ($this->conn->GetOne($sql)) {
				$sql = "select sum(nvl(realisasi,0)) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and to_char(last_update_realisasi, 'yyyy-mm-dd') <= '$key'  and is_leaf='1')";
				$realisasi_per_day[$key] = $this->conn->GetOne($sql);
			}
		}
		ksort($realisasi_per_day);

		if ($times['total_hari_realisasi'] > $times['total_hari'])
			$times['total_hari'] = $times['total_hari_realisasi'];

		$this->data['times'] = $times;
		$this->data['plan_per_day'] = $plan_per_day;
		$this->data['realisasi_per_day'] = $realisasi_per_day;
		if (!$inline) {
			$this->View('panelbackend/wbs_kurva');
		}

		if ($no_layout){
			$this->data['no_layout'] = $no_layout;
			echo $this->PartialView('panelbackend/wbs_kurva', true);
		}
	}

	function setdatetimemanual() {
		$id = (int) $_POST['id'];
		$urutan = (int) $_POST['id_detail'];

		if (!strtotime($_POST['val'])) die('-1');

		$datetime =  date('d-m-Y H:i:s', strtotime($_POST['val']));

		// die($datetime);

		$record = array(
			'last_update_realisasi'=>$datetime,
		);
        $sql = $this->conn->UpdateSQL('wbs_plan_detail', $record, "id_plan=$id and urutan=$urutan");
        if($sql){
		    $ret = $this->conn->Execute($sql);
		}

		// $sql = "update wbs_plan_detail set last_update_realisasi=sysdate where id_plan=$id and urutan=$urutan";
		// $ret = $this->conn->query($sql);
		if ($ret) {
			$sql = "select to_char(last_update_realisasi, 'dd-mm-yyyy hh24:mi') as last_update_realisasi_str from wbs_plan_detail where id_plan=$id and urutan=$urutan";
			$row = $this->conn->GetRow($sql);
			$update_terakhir = Eng2Ind($row['last_update_realisasi_str']);

			$ret = array('update_terakhir'=>$update_terakhir);
			echo json_encode($ret);
			$this->_updateProgressPekerjaan($id, $urutan);

		}
		else {
			echo '-1';
		}

		// TODO log
		die();
	}

	function updaterealisasi() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);
		$val = (int) $_POST['val'];

		if ($val > 100) $val = 100;

		$sql = "select max(urutan) from wbs_plan_detail where id_plan=$id";
		$urutan_max = $this->conn->GetOne($sql);

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		$sql = "update wbs_plan_detail set progress=$val, realisasi=rencana*$val/100, last_update_realisasi=sysdate, modified_by = '$user_id', modified_by_desc = '$user_name' where id_plan=$id and urutan=$urutan";

		if ($urutan == 2 || $urutan == $urutan_max) {
			$sql = "update wbs_plan_detail set progress=$val, realisasi=rencana*$val/100, modified_by = '$user_id', modified_by_desc = '$user_name' where id_plan=$id and urutan=$urutan";
		}


		$ret = $this->conn->query($sql);
		if ($ret) {
			$sql = "select urutan_parent, progress, realisasi, to_char(last_update_realisasi, 'dd-mm-yyyy hh24:mi') as last_update_realisasi_str from wbs_plan_detail where id_plan=$id and urutan=$urutan";
			$row = $this->conn->GetRow($sql);
			$progress = $row['progress'];
			$realisasi = number_format($row['realisasi'], 2);
			$urutan_parent = $row['urutan_parent'];
			$update_terakhir = Eng2Ind($row['last_update_realisasi_str']);

			$this->_hitungRealisasiParent($id, $urutan_parent);

			$progress_pr = setProgressBar($progress);

			$ret = array('progress'=>$progress, 'progress_pr'=>$progress_pr, 'realisasi'=>$realisasi, 'update_terakhir'=>$update_terakhir);
			echo json_encode($ret);

			// $this->conn->debug = 1;
			$this->_updateProgressPekerjaan($id, $urutan);

		}
		else {
			echo '-1';
		}

		// TODO log
		die();
	}

	function getparentrealisasi() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);

		$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id and urutan=$urutan";
		$urutan_parent = $this->conn->GetOne($sql);

		$list_realisasi = $this->_getParentRealisasi($id, $urutan_parent);
		$str = '';
		foreach ($list_realisasi as $key => $value) {
			$str .= $key . '.' . $value . '__';
		}

		$ret = array('list_realisasi'=>$str);
		echo json_encode($list_realisasi);
	}


	function _getParentRealisasi($id_plan, $urutan_parent) {
		$list_realisasi = array();

		$sql = "select realisasi, urutan, urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan=$urutan_parent";
		$row = $this->conn->GetRow($sql);
		if ($row['urutan_parent'] == 0) return array('rparent'.$row['urutan']=>@number_format($row['realisasi'],2));

		$list_realisasi["rparent{$row['urutan']}"] = @number_format($row['realisasi'],2);
		$arr = $this->_getParentRealisasi($id_plan, $row['urutan_parent']);
		$list_realisasi = array_merge($list_realisasi, $arr);

		return $list_realisasi;
	}

	function getparentprogress() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);

		$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id and urutan=$urutan";
		$urutan_parent = $this->conn->GetOne($sql);

		$list_progress = $this->_getParentProgress($id, $urutan_parent);
		$str = '';
		foreach ($list_progress as $key => $value) {
			$str .= $key . '.' . $value . '__';
		}

		$ret = array('list_progress'=>$str);
		echo json_encode($list_progress);
	}

	function _getParentProgress($id_plan, $urutan_parent) {
		$list_progress = array();

		$sql = "select progress, urutan, urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan=$urutan_parent";
		$row = $this->conn->GetRow($sql);
		if ($row['urutan_parent'] == 0) return array('pr'.$row['urutan']=>setProgressBar($row['progress']));

		$list_progress["pr{$row['urutan']}"] = setProgressBar($row['progress']);
		$arr = $this->_getParentProgress($id_plan, $row['urutan_parent']);
		$list_progress = array_merge($list_progress, $arr);

		return $list_progress;
	}

	function getparentlastupdate() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);

		$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id and urutan=$urutan";
		$urutan_parent = $this->conn->GetOne($sql);

		$list_update = $this->_getParentLastUpdate($id, $urutan_parent);
		$str = '';
		foreach ($list_update as $key => $value) {
			$str .= $key . '.' . $value . '__';
		}

		$ret = array('list_update'=>$str);
		echo json_encode($list_update);
	}

	function _getParentLastUpdate($id_plan, $urutan_parent) {
		$list_update = array();

		$sql = "select to_char(last_update_realisasi, 'dd-mm-yyyy hh24:mi') as last_update_realisasi_str, urutan, urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan=$urutan_parent";
		$row = $this->conn->GetRow($sql);
		if ($row['urutan_parent'] == 0) return array('update_terakhir'.$row['urutan']=>Eng2Ind($row['last_update_realisasi_str']));

		$list_update["update_terakhir{$row['urutan']}"] = Eng2Ind($row['last_update_realisasi_str']);
		$arr = $this->_getParentLastUpdate($id_plan, $row['urutan_parent']);
		$list_update = array_merge($list_update, $arr);

		return $list_update;
	}

	function cetak($id_pekerjaan=null, $id=null) {
		$this->_beforeDetail($id_pekerjaan);
		$this->_afterDetail($id);
		$this->kurva($id_pekerjaan, $id, true);

		$details = $this->data['details'];
		$i=0;
		foreach ($details as $detail) {
			if ($detail['tingkat'] > 2 || $detail['tingkat'] == 0) continue;
			$i++;
		}
		$this->data['height_kurva'] = $i*27;
		$this->data['num_item_show'] = $i;

		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout3";
		$this->data['width'] = "900px";
		$this->data['no_header'] = true;
		$this->data['excel'] = false;
		/*$this->data['image'] = "uploads/wbs".$id_pekerjaan.'.jpg';

		system("wkhtmltoimage --crop-w 500 \"".site_url("panelbackend/wbs/kurva/$id_pekerjaan/$id/1/1")."\" ".$this->data['image'] );*/

		$this->View($this->viewcetak);
		// unlink($this->data['image']);
	}




	function updateketerangan() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('k', '', $_POST['id_detail']);
		$ket = $_POST['ket'];

		$record = array(
			'keterangan'=>$ket,
		);
        $sql = $this->conn->UpdateSQL('wbs_plan_detail', $record, "id_plan=$id and urutan=$urutan");
        if($sql){
		    $ret = $this->conn->Execute($sql);
		}

		if ($ret) {
			echo '1';
		}
		else {
			echo '-1';
		}

		// TODO log
		die();
	}


}
