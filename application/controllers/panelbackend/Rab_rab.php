<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_rab extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_rablist";
		$this->viewdetail = "panelbackend/rab_rabdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah RAB';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit RAB';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail RAB';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar RAB';
		}

		$this->data['no_menu'] = true;

		$this->load->model("Rab_rabModel","model");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			''
		);
	}

	public function Index($id_pekerjaan=0, $id_rab=null){

		$this->_beforeDetail($id_pekerjaan);

		if($id_rab){
			redirect("panelbackend/rab_jasa_material/index/".$id_rab);
		}elseif($this->data['last_versi']){
			redirect("panelbackend/rab_jasa_material/index/".$this->data['last_versi']);
		}else{
			$this->newRab($id_pekerjaan);
			redirect("panelbackend/rab_jasa_material/index/".$id);
		}

	}

	protected function _beforeDetail($id_pekerjaan=null, $id=null){
		$this->data['id_pekerjaan'] = $id_pekerjaan;
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_pekerjaan;
		$this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
	}

	protected function Record($id=null){
		return array(
			'id_pekerjaan'=>$this->post['id_pekerjaan'],
			'versi'=>$this->post['versi'],
			'is_final'=>(int)$this->post['is_final'],
		);
	}

	protected function Rules(){
		return array(
			"id_pekerjaan"=>array(
				'field'=>'id_pekerjaan', 
				'label'=>'Pekerjaan', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['rabpekerjaanarr']))."]",
			),
			"versi"=>array(
				'field'=>'versi', 
				'label'=>'Versi', 
				'rules'=>"required|max_length[20]",
			),
			"is_final"=>array(
				'field'=>'is_final', 
				'label'=>'IS Final', 
				'rules'=>"max_length[1]",
			),
		);
	}

}