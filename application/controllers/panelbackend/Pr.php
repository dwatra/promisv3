<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Pr extends _adminController{
	public $limit = 5;
	public $limit_arr = array('5','10','30','50','100');

	public function __construct(){
		parent::__construct();
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->plugin_arr = array(
			'select2','upload','datepicker'
		);
	}

	protected function init(){
		parent::init();
	}

	function Index($page=0){
		$this->data['page_title'] = 'Monitoring pr';
		$this->View("panelbackend/prlist");
	}

	function Add(){
		$this->data['edited'] = 1;
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'TAMBAH PR';
		$this->View("panelbackend/prdetail");
	}

	function Detail($id=null){
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'DETAIL PR';
		$this->View("panelbackend/prdetail");
	}

	function Edit($id=null){
		$this->data['edited'] = 1;
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'UBAH PR';
		$this->View("panelbackend/prdetail");
	}
}
