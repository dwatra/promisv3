<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_rab_niaga extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_rab_niagalist";
		$this->viewdetail = "panelbackend/rab_rab_niagadetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";
		$this->data['width'] = "1200px";

		if ($this->mode == 'add') {
			$this->viewdetail = "panelbackend/rab_rab_niagaedit";
			$this->data['page_title'] = 'Tambah RAB RAB Detail';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->viewdetail = "panelbackend/rab_rab_niagaedit";
			$this->data['page_title'] = 'Edit RAB RAB Detail';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->viewdetail = "panelbackend/rab_rab_niagadetail";
			$this->data['page_title'] = 'Detail RAB RAB Detail';
			$this->data['edited'] = false;	
		}else{

			$this->data['no_menu'] = true;
			$this->data['page_title'] = 'Daftar RAB RAB Detail';
		}

		$this->load->model("Rab_rab_detailModel","model");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_rabModel","rabrab");
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("Rab_rab_detail_filesModel","modelfile");

		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->data['mtposanggaranarr'] = $this->mtposanggaran->GetCombo();
		$this->data['mtposanggaranarr'][''] = '-kosong-';

		$this->load->model("Mt_jabatan_proyekModel","mtjabatanproyek");
		$this->data['jabatanarr'] = $this->mtjabatanproyek->GetCombo();
		unset($this->data['jabatanarr']['']);

		
		$this->load->model("Mt_itemModel","mtitem");
		$this->data['mtitemarr'] = $this->mtitem->GetCombo();
		$this->data['jasamaterialarr'] = array('1'=>'Jasa','2'=>'Material');
		$this->data['sumbersatuanarr'] = array('0'=>'Tanpa Satuan','1'=>'Manual','2'=>'Mandays');
		$this->data['sumbernilaiarr'] = array('1'=>'Hitung di sub','2'=>'Master Harga','3'=>'Skope Pekerjaan','5'=>'Kontigensi','4'=>'Manual');
		$this->data['jenismandaysarr'] = array('1'=>'Max','2'=>'Total');

		$this->load->model("Mt_sumber_pegawaiModel","mtsumberpegawai");
		$this->data['mtsumberpegawaiarr'] = $this->mtsumberpegawai->GetCombo();

		$this->data['configfile'] = $this->config->item('file_upload_config');
		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'upload'
		);
	}

	private function setAjukan($id_rab=null){
		$this->rabrab->Update(array('status'=>1,'ket_rendal'=>$this->post['keterangan']),"id_rab = ".$this->conn->escape($id_rab));

		redirect(current_url());
	}

	private function setAjukanManager($id_rab=null){
		$this->rabrab->Update(array('status'=>1,'asman_ok'=>1,'ket_asman'=>$this->post['keterangan']),"id_rab = ".$this->conn->escape($id_rab));

		redirect(current_url());
	}

	private function setSetujui($id_rab=null){
		$this->conn->StartTrans();

		$ret = $this->rabrab->Update(array('status'=>2,'manager_ok'=>1,'is_final'=>1,'ket_manager'=>$this->post['keterangan']),"id_rab = ".$this->conn->escape($id_rab));


		if($ret['success']){
			$id_rab_old = $id_rab;

			$record = array(
				'id_pekerjaan'=>$this->data['id_pekerjaan'],
				'versi'=>1,
				'jenis'=>2,
				'is_final'=>0,
			);
			$return = $this->model->Insert($record);
			$r = $id_rab = $id = $return['data'][$this->pk];

			if($id_rab){
				$rows = $this->conn->GetArray("select $id_rab as id_rab, id_item, uraian, nilai_satuan, kode_biaya, id_pos_anggaran, keterangan, vol, satuan, sumber_satuan, is_ppn, id_rab_detail_parent, jasa_material, sumber_nilai, jenis_mandays, id_sumber_pegawai, day, id_rab_detail as id_rab_detail_old, no_prk
				from rab_rab_detail where id_rab = ".$this->conn->escape($id_rab_old));

				foreach($rows as $rc){
					if(!$r)
						break;

					$r = $this->conn->goInsert("rab_rab_detail",$rc);
				}
			}

			if($id_rab && $r){
				$r = $this->conn->execute("update rab_rab_detail a 
				set id_rab_detail_parent = (select id_rab_detail from rab_rab_detail b 
				where b.id_rab = ".$this->conn->escape($id_rab)." and a.id_rab_detail_parent = b.id_rab_detail_old) 
				where id_rab = ".$this->conn->escape($id_rab));
			}

			if($r){
				$return['success'] = true;
			}else{
				$return['success'] = false;
			}
		}

		if($ret['success']){
			SetFlash("suc_msg","Berhasil");
			$this->conn->trans_commit();
		}
		else{
			SetFlash("err_msg","Gagal");
			$this->conn->trans_rolback();
		}

		redirect(current_url());
	}

	private function setKembalikan($id_rab=null){
		$this->rabrab->Update(array('status'=>3,'manager_ok'=>0,'asman_ok'=>0,'ket_asman'=>$this->post['keterangan']),"id_rab = ".$this->conn->escape($id_rab));

		redirect(current_url());
	}

	private function setRevisi($id_rab=null){
		$this->_revisi($id_rab, null, $this->post['keterangan']);

		redirect(current_url());
	}

	public function Index($id_rab=0, $id_rab_detail=null){

		if($this->post['act']=='set_ajukan')
			$this->setAjukan($id_rab);

		if($this->post['act']=='set_kembalikan')
			$this->setKembalikan($id_rab);

		if($this->post['act']=='set_ajukan_manager')
			$this->setAjukanManager($id_rab);

		if($this->post['act']=='set_revisi')
			$this->setRevisi($id_rab);

		if($this->post['act']=='set_pekerjaan_baru')
			$this->jadikanRAB($id_rab);

		$this->_beforeDetail($id_rab);

		if($this->post['act']=='set_setujui')
			$this->setSetujui($id_rab);

		$this->data['row']['id_rab_detail'] = $id_rab_detail;
		$this->data['realisasi'] = $this->rabrab->realisasi($id_rab);
		$this->_afterDetail($id_rab);

		$this->View($this->viewlist);
	}

	public function Add($id_rab=0, $id_rab_detail_parent=null){
		$this->data['id_rab_detail_parent'] = $id_rab_detail_parent;
		$this->Edit($id_rab);
	}

	public function Edit($id_rab=0,$id=null,$id_rab_detail_parent=null){

		if($id_rab_detail_parent)
			$this->data['id_rab_detail_parent'] = $id_rab_detail_parent;

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_rab,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if($this->data['row']['id_rab_detail_parent'])
			$this->data['id_rab_detail_parent'] = $this->data['row']['id_rab_detail_parent'];

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$this->post);
			$this->data['row'] = array_merge($this->data['row'],$record);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_rab'] = $id_rab;
			if($this->data['id_rab_detail_parent'])
				$record['id_rab_detail_parent'] = $this->data['id_rab_detail_parent'];

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {
					
					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->data['row'] = $return;

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				if($this->data['id_rab_detail_parent']){
					redirect("$this->page_ctrl/detail/$id_rab/".$this->data['id_rab_detail_parent']);
				}else{
					redirect("$this->page_ctrl/detail/$id_rab/".$id);
				}

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_rab=null, $id=null){

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		if($this->data['row']['sumber_nilai']<>1 and $this->data['row']['sumber_nilai']<>3){
			$this->data['width'] = "900px";
		}

		$this->View($this->viewdetail);
	}

	public function Delete($id_rab=null, $id=null, $id_rab_detail_parent=null){

        $this->model->conn->StartTrans();

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);

			if($id_rab_detail_parent)
				redirect("$this->page_ctrl/detail/$id_rab/$id_rab_detail_parent");
			else
				redirect("$this->page_ctrl/index/$id_rab");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab/$id");
		}

	}

	protected function _afterDetail($id=null){
		if($this->data['row']['id_rab_detail_parent'])
			$this->data['id_rab_detail_parent'] = $this->data['row']['id_rab_detail_parent'];

		$this->data['breadcrumb'] = $this->model->GetComboParent($this->data['id_rab_detail_parent']);

		if($this->data['row']['id_item'])
			$this->viewdetail = "panelbackend/rab_rab_niagaedit";

		if($this->viewdetail<>"panelbackend/rab_rab_niagaedit"){
			if(!$this->data['row']['id_rab_detail']){
				$max_level = 3;
				$rows = $this->conn->GetArray("select * from rab_rab_detail where id_rab = ".$this->conn->escape($this->data['id_rab'])." and id_item is null order by kode_biaya, id_rab_detail");
			}else{
				$max_level = 2;
				$id_rab_detailarr = $this->model->GetChild($this->data['row']['id_rab_detail']);
				if(!count($id_rab_detailarr))
					return array();

				$rows = $this->conn->GetArray("select * from rab_rab_detail where id_rab = ".$this->conn->escape($this->data['id_rab'])." and id_rab_detail in (".implode(",", $id_rab_detailarr).") and id_rab_detail <> ".$this->conn->escape($this->data['row']['id_rab_detail'])." order by kode_biaya, id_rab_detail");
			}

			$this->data['rows'] = array();
			$i = 0;
			$this->GenerateTree($rows, "id_rab_detail_parent", "id_rab_detail", "uraian", $this->data['rows'], $this->data['row']['id_rab_detail'], $i, 0, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $max_level);

			$this->data['is_satuan'] = false;
			$this->data['is_kode_biaya'] = false;
			foreach($this->data['rows'] as $r){
				if($r['vol'] or $r['satuan'])
					$this->data['is_satuan'] = true;

				if($r['kode_biaya'])
					$this->data['is_kode_biaya'] = true;
			}
		}



		if(!$this->data['row']['files']['id'] && $id){
			$rows = $this->conn->GetArray("select id_rab_detail_files as id, client_name as name
				from rab_rab_detail_files
				where id_rab_detail = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['files']['id'][] = $r['id'];
				$this->data['row']['files']['name'][] = $r['name'];
			}
		}

		if($this->data['row']['sumber_satuan']==2){
			if(empty($this->data['row']['id_jabatan_proyek'])){
				$this->data['row']['id_jabatan_proyek'] = $this->conn->GetList("select id_jabatan_proyek as key, id_jabatan_proyek as val 
					from rab_rab_detail_jabatan_proyek 
					where id_rab_detail = ".$this->conn->escape($id));
			}
		}

		if($this->data['row']['sumber_nilai']==3){
			$this->data['rowsjasa_material'] = $this->conn->GetArray("select 
			nvl(vol,1)*nvl(harga_satuan,0) as total,
			a.*
			from rab_jasa_material a
			where id_rab = ".$this->conn->escape($this->data['id_rab'])."
			and kode_biaya = ".$this->conn->escape($this->data['row']['kode_biaya'])."
			and id_pos_anggaran = ".$this->conn->escape($this->data['row']['id_pos_anggaran'])."
			and jasa_material = ".$this->conn->escape($this->data['row']['jasa_material']));
		}

		if($this->data['row']['sumber_nilai']!=1){
			$rows = $this->conn->GetArray("select * from rab_realisasi where id_rab_detail = ".$this->conn->escape($this->data['row']['id_rab_detail']));

			if($this->data['row']['sumber_nilai']==3){
				$this->data['rowsrealisasi'] = array();
				foreach($rows as $r){
					$this->data['rowsrealisasi'][$r['id_jasa_material']][] = $r;
				}
			}else{
				$this->data['rowsrealisasi'] = $rows;
			}

			$keystr = $this->conn->GetKeysStr($rows, 'id_realisasi');

			if($keystr){
				$rows = $this->conn->GetArray("select id_realisasi, id_realisasi_files as id, client_name as name
					from rab_realisasi_files
					where id_realisasi in ($keystr)");

				foreach($rows as $r){
					$this->data['filerealisasi'][$r['id_realisasi']][] = $r;
				}
			}
		}
	}

	function GenerateTree(&$row, $colparent, $colid, $collabel, &$return=array(), $valparent=null, &$i=0, $level=0, $spacea = "&nbsp;➥&nbsp;", $max_level=100){

		$level++;
		foreach ($row as $key => $value) {
			# code...
			if(trim($value[$colparent])==trim($valparent)){
			
				$space = '';
				for($k=1; $k<$level; $k++){
					$space .= $spacea;
				}

				$value[$collabel] = $space.$value[$collabel];
				$value['level'] = $level;

				if($level<=1 or (!$value['kode_biaya'] && $level<=2))
					$value[$collabel] = "<b>".$value[$collabel]."</b>";


				if($level<=$max_level){
					$return[$i]=$value;
				}

				$i++;

				$temp = $row;
				unset($temp[$key]);

				$this->GenerateTree($temp, $colparent, $colid, $collabel, $return, $value[$colid], $i, $level,$spacea, $max_level);

				$row = $temp;
			}
		}

		if($row && $level==1)
			$return = array_merge($return, $row);
	}

	protected function _beforeDetail($id_rab_niaga=null, $id=null){
		$this->data['id_rab_niaga'] = $id_rab_niaga;
		$this->data['rowheader3'] = $this->rabrab->GetByPk($id_rab_niaga);
		$this->data['id_rab'] = $id_rab = $this->data['rowheader3']['id_rab_parent'];
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
		$this->data['id_pekerjaan'] = $id_pekerjaan = $this->data['rowheader3']['id_pekerjaan'];
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_rab;
/*		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		if($this->data['last_versi']<>$this->data['rowheader2']['id_rab']){
			$this->access_role['add']=0;
			$this->access_role['edit']=0;
			$this->access_role['delete']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['upload_file']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['save']=0;
		}*/
	}

	protected function _afterUpdate($id){
		return $this->_afterInsert($id);
	}

	protected function _afterInsert($id){
		$ret = true;

		// $this->conn->debug=1;
		if($ret)
			$ret = $this->_hitungSubParent($this->data['id_rab_detail_parent'], $this->data['row']['sumber_nilai']=='5');

		/*if($ret)
			$ret = $this->_hitungTotalRabParent();*/
		// dpr($ret,1);

		if($ret)
			$ret = $this->_delsertFiles($id);

		if($ret)
			$ret = $this->_delsertJabatanProyek($id);

		return $ret;
	}

	private function _delsertFiles($id_rab_detail = null){
		$ret = true;

		if(is_array($this->post['files']) && count($this->post['files'])){
			foreach($this->post['files']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_rab_detail'=>$id_rab_detail), $v);
			}
		}
		return $ret;
	}

	private function _delsertJabatanProyek($id_rab_detail = null){
		$ret = $this->conn->Execute("delete from rab_rab_detail_jabatan_proyek where id_rab_detail = ".$this->conn->escape($id_rab_detail));

		if(is_array($this->post['id_jabatan_proyek']) && count($this->post['id_jabatan_proyek'])){
			foreach($this->post['id_jabatan_proyek'] as $k=>$v){
				if(!$ret)
					break;

				$record = array();
				$record['id_rab_detail'] = $id_rab_detail;
				$record['id_jabatan_proyek'] = $k;

				$this->conn->goInsert("rab_rab_detail_jabatan_proyek", $record);
			}
		}
		return $ret;
	}

	protected function Record($id=null){
		$return = array(
			'id_item'=>$this->post['id_item'],
			'uraian'=>$this->post['uraian'],
			'kode_biaya'=>$this->post['kode_biaya'],
			'id_pos_anggaran'=>$this->post['id_pos_anggaran'],
			'keterangan'=>$this->post['keterangan'],
			'vol'=>$this->post['vol'],
			'satuan'=>$this->post['satuan'],
			'sumber_satuan'=>$this->post['sumber_satuan'],
			'sumber_nilai'=>$this->post['sumber_nilai'],
			'is_ppn'=>(int)$this->post['is_ppn'],
			'id_sumber_pegawai'=>$this->post['id_sumber_pegawai'],
			'jasa_material'=>$this->post['jasa_material'],
			'jenis_mandays'=>$this->post['jenis_mandays'],
			'day'=>"{{null}}",
			'jenis'=>"1",
		);

		if($return['sumber_satuan']=='1'){
			$return['vol'] = $this->post['vol'];
			$return['satuan'] = $this->post['satuan'];
		}elseif($return['sumber_satuan']=='2'){
			// $this->conn->debug = 1;
			$return['vol'] = $this->_hitungMd($return, $this->post['id_jabatan_proyek']);
			$return['satuan'] = 'MD';
			if($return['jenis_mandays']==1){
				$return['day'] = $this->post['day'];
				$return['satuan'] = $this->post['satuan'];
			}

			// dpr($return,1);
		}else{
			$return['vol'] = "{{null}}";
			$return['satuan'] = "{{null}}";
		}

		if($return['sumber_nilai']=='1'){
			$return['nilai_satuan'] = $this->_hitungSub($this->data['row']['id_rab_detail']);

			if($return['is_ppn'])
				$return['nilai_satuan'] = $return['nilai_satuan']*1.1;
		}elseif($return['sumber_nilai']=='2'){
			$return['nilai_satuan'] = $this->post['nilai_satuan'];
			$return['is_ppn'] = "{{null}}";
		}elseif($return['sumber_nilai']=='3'){
			$return['nilai_satuan'] = $this->_hitungSow($return);

			if($return['is_ppn'])
				$return['nilai_satuan'] = (float)$return['nilai_satuan']*1.1;
		}elseif($return['sumber_nilai']=='5'){
			$id_rab_detail = $this->data['row']['id_rab_detail'];
			if(!$id_rab_detail)
				$id_rab_detail = $this->data['id_rab_detail_parent'];
/*
			$this->conn->debug = 1;*/
			$return['nilai_satuan'] = $this->_hitungTotalRab($id_rab_detail, $this->data['id_rab_detail_parent']);/*

			dpr($return,1);*/

			if($return['is_ppn'])
				$return['nilai_satuan'] = $return['nilai_satuan']*1.1;
		}else{
			$return['nilai_satuan'] = $this->post['nilai_satuan'];
		}

		return $return;
	}

	protected function Rules(){
		$return = array(
			"uraian"=>array(
				'field'=>'uraian', 
				'label'=>'Uraian', 
				'rules'=>"required|max_length[200]",
			),
			"nilai_satuan"=>array(
				'field'=>'nilai_satuan', 
				'label'=>'Nilai Satuan', 
				'rules'=>"numeric",
			),
			"kode_biaya"=>array(
				'field'=>'kode_biaya', 
				'label'=>'Kode Biaya', 
				'rules'=>"max_length[20]",
			),
			"id_pos_anggaran"=>array(
				'field'=>'id_pos_anggaran', 
				'label'=>'POS Anggaran', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['mtposanggaranarr']))."]",
			),
			"sumber_nilai"=>array(
				'field'=>'sumber_nilai', 
				'label'=>'Sumber Nilai', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['sumbernilaiarr']))."]",
			),
			"sumber_satuan"=>array(
				'field'=>'sumber_satuan', 
				'label'=>'Sumber Satuan', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['sumbersatuanarr']))."]",
			),
			"keterangan"=>array(
				'field'=>'keterangan', 
				'label'=>'Keterangan', 
				'rules'=>"max_length[4000]",
			),
			"is_ppn"=>array(
				'field'=>'is_ppn', 
				'label'=>'IS PPN', 
				'rules'=>"max_length[1]",
			),
		);

		if($this->data['row']['sumber_nilai']==2){
			$return['id_item'] = array(
				'field'=>'id_item', 
				'label'=>'Item', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['mtitemarr']))."]",
			);
		}elseif($this->data['row']['sumber_nilai']==3){
			$return['kode_biaya'] = array(
				'field'=>'kode_biaya', 
				'label'=>'Kode Biaya', 
				'rules'=>"required|max_length[20]",
			);
			$return['id_pos_anggaran'] = array(
				'field'=>'id_pos_anggaran', 
				'label'=>'POS Anggaran', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['mtposanggaranarr']))."]",
			);
			$return['jasa_material'] = array(
				'field'=>'jasa_material', 
				'label'=>'Jasa/Material', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['jasamaterialarr']))."]",
			);
		}

		/*elseif($this->data['row']['sumber_nilai']==4){

			$return['nilai_satuan'] = array(
				'field'=>'nilai_satuan', 
				'label'=>'Nilai Satuan', 
				'rules'=>"required|numeric",
			);
		}*/

		if($this->data['row']['sumber_satuan']==1){
			$return['vol'] = array(
				'field'=>'vol', 
				'label'=>'VOL', 
				'rules'=>"required|integer",
			);
			$return['satuan'] = array(
				'field'=>'satuan', 
				'label'=>'Satuan', 
				'rules'=>"required|max_length[20]",
			);
		}elseif($this->data['row']['sumber_satuan']==2){
			$return['id_sumber_pegawai'] = array(
				'field'=>'id_sumber_pegawai', 
				'label'=>'Sumber Pegawai', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['mtsumberpegawaiarr']))."]",
			);
			$return['jenis_mandays'] = array(
				'field'=>'jenis_mandays', 
				'label'=>'Jenis Mandays', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['jenismandaysarr']))."]",
			);
		}


		return $return;
	}

}