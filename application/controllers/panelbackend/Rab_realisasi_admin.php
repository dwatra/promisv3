<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_realisasi_admin extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_realisasiadminlist";
		$this->viewdetail = "panelbackend/rab_realisasiadmindetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";
		$this->data['width'] = "1200px";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah Realisasi';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit Realisasi';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail Realisasi';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Realisasi';
		}

		$this->load->model("Rab_realisasiModel","model");
		$this->load->model("Rab_realisasi_filesModel","modelfile");
		$this->load->model("Rab_rab_detailModel","rabrabdetail");
		$this->load->model("Rab_rabModel","rabrab");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		// $this->data['rabrabdetailarr'] = $this->rabrabdetail->GetCombo();


		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->data['mtposanggaranarr'] = $this->mtposanggaran->GetCombo();
		
		$this->load->model("Rab_jasa_materialModel","rabjasa_material");
		// $this->data['rabjasa_materialarr'] = $this->rabjasa_material->GetCombo();
		// 
		$this->data['configfile'] = $this->config->item('file_upload_config');
		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'select2','upload','datepicker'
		);
	}

	public function HeaderExport(){
		$this->model->pk = null;
		return $this->Header();
	}

	protected function Record($id=null){
		$this->post['nilai_satuan'] = Rupiah2Number($this->post['nilai_satuan']);
		unset($this->post['nilai']);
		return array(
			'id_jasa_material'=>$this->post['id_jasa_material'],
			'id_rab_detail'=>$this->post['id_rab_detail'],
			'id_jabatan_proyek'=>$this->post['id_jabatan_proyek'],
			'nilai'=>(float)$this->post['vol']*(float)$this->post['nilai_satuan'],
			'nilai_satuan'=>$this->post['nilai_satuan'],
			'vol'=>$this->post['vol'],
			'satuan'=>$this->post['satuan'],
			'tgl'=>$this->post['tgl'],
			'keterangan'=>$this->post['keterangan'],
			'nama'=>$this->post['nama'],
		);
	}

	protected function Rules(){
		return array(
			/*"id_jasa_material"=>array(
				'field'=>'id_jasa_material', 
				'label'=>'Scope', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['rabjasa_materialarr']))."]",
			),
			"id_rab_detail"=>array(
				'field'=>'id_rab_detail', 
				'label'=>'RAB Detail', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['rabrabdetailarr']))."]",
			),*/
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"required",
			),
			"tgl"=>array(
				'field'=>'tgl', 
				'label'=>'Tgl', 
				'rules'=>"required",
			),
			"nilai"=>array(
				'field'=>'nilai', 
				'label'=>'Nilai', 
				'rules'=>"required",
			),
			"nilai_satuan"=>array(
				'field'=>'nilai_satuan', 
				'label'=>'Nilai Satuan', 
				'rules'=>"required",
			),
			"vol"=>array(
				'field'=>'vol', 
				'label'=>'Vol', 
				'rules'=>"required|integer",
			),
			"satuan"=>array(
				'field'=>'satuan', 
				'label'=>'Satuan', 
				'rules'=>"required",
			),
		/*	"file"=>array(
				'field'=>'file[id]', 
				'label'=>'File', 
				'rules'=>"required",
			),*/
			"keterangan"=>array(
				'field'=>'keterangan', 
				'label'=>'Keterangan', 
				'rules'=>"max_length[4000]",
			),
		);
	}

	protected function _afterDetail($id){
		if($this->data['rowheader3']['id_rab_detail_parent'])
			$this->data['id_rab_detail_parent'] = $this->data['rowheader3']['id_rab_detail_parent'];

		$this->data['breadcrumb'] = $this->rabrabdetail->GetComboParent($this->data['id_rab_detail_parent']);

		if(!$this->data['row']['file']['id'] && $id){
			$rows = $this->conn->GetArray("select id_realisasi_files as id, client_name as name
				from rab_realisasi_files
				where id_realisasi = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['file']['id'][] = $r['id'];
				$this->data['row']['file']['name'][] = $r['name'];
			}
		}

		$this->data['rabjasa_materialarr'][$this->data['row']['id_jasa_material']] = $this->conn->GetOne("select nama from rab_jasa_material where id_jasa_material = ".$this->conn->escape($this->data['row']['id_jasa_material']));



		if($this->data['rowheader3']['sumber_satuan']==2){
			$this->data['jabatanarr'] = $this->conn->GetList("select a.id_jabatan_proyek as key,  c.nama as val
					from rab_rab_detail_jabatan_proyek a
					join mt_jabatan_proyek c on c.id_jabatan_proyek = c.id_jabatan_proyek
					where exists (select 1 from rab_rab_manpower b 
					where a.id_jabatan_proyek = b.id_jabatan_proyek 
					and b.id_rab = ".$this->conn->escape($this->data['id_rab']).")
					and id_rab_detail = ".$this->conn->escape($this->data['rowheader3']['id_rab_detail']));
		}
	}

	public function Index($id_rab=0){
		$this->access_role['add'] = false;
		if($this->post['act']=='set_value')
			$_SESSION[SESSION_APP]['id_pos_anggaran'] = $this->post['id_pos_anggaran'];

		$id_pos_anggaran = $_SESSION[SESSION_APP]['id_pos_anggaran'];

		if($id_pos_anggaran)
			$filter_pos_anggaran = " and id_pos_anggaran = $id_pos_anggaran";

		$this->_header($id_rab);
		$rows = $this->conn->GetArray("select a.*, nvl(nilai_satuan,0)*nvl(vol,1)*nvl(day,1) as nilai from rab_rab_detail a where id_rab = ".$this->conn->escape($id_rab)." $filter_pos_anggaran
			order by nvl(id_rab_detail_parent, id_rab_detail), kode_biaya");

		$this->data['rows'] = array();
		$i = 0;
		$this->GenerateTree($rows, "id_rab_detail_parent", "id_rab_detail", "uraian", $this->data['rows'], null, $i, 0, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

		$this->View($this->viewlist);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'id_jasa_material', 
				'label'=>'Scope', 
				'width'=>"auto",
			),
			array(
				'name'=>'id_rab_detail', 
				'label'=>'RAB Detail', 
				'width'=>"auto",
			),
			/*array(
				'name'=>'induk', 
				'label'=>'Induk', 
				'width'=>"auto",
			),*/
			array(
				'name'=>'uraian', 
				'label'=>'Uraian', 
				'width'=>"auto",
			),
			array(
				'name'=>'tanggal', 
				'label'=>'Tanggal', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'harga_satuan', 
				'label'=>'Harga Satuan', 
				'width'=>"auto",
			),
			array(
				'name'=>'vol', 
				'label'=>'Vol', 
				'width'=>"auto",
			),
			array(
				'name'=>'satuan', 
				'label'=>'Satuan', 
				'width'=>"auto",
			),
			array(
				'name'=>'total_harga', 
				'label'=>'Total Harga', 
				'width'=>"auto",
			),
			array(
				'name'=>'keterangan', 
				'label'=>'Keterangan', 
				'width'=>"auto",
			),
		);
	}

	public function import_list($id_rab=null, $id_rab_detail=null){

		$file_arr = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel','application/wps-office.xls','application/wps-office.xlsx');

		if(in_array($_FILES['import'.$id_rab_detail.'upload']['type'], $file_arr)){

			$this->_beforeDetail($id_rab_detail);

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("","");
		
			$this->load->library('Factory');
			$inputFileType = Factory::identify($_FILES['import'.$id_rab_detail.'upload']['tmp_name']);
			$objReader = Factory::createReader($inputFileType);
			$excel = $objReader->load($_FILES['import'.$id_rab_detail.'upload']['tmp_name']);
			$sheet = $excel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
            $this->model->conn->StartTrans();

			#header export
			$header=array(
				array(
					'name'=>$this->model->pk
				)
			);
			$header=$this->HeaderExport();

			for ($row = 2; $row <= $highestRow; $row++){ 

		    	$col = 'A';
		    	$rcrd = array();
		    	foreach($header as $r1){
	           		$rcrd[$r1['name']] = $sheet->getCell($col.$row)->getValue();
	           		$col++;
		    	}

		    	$record = array(
					'id_jasa_material'=>$rcrd['id_jasa_material'],
					'id_rab_detail'=>$rcrd['id_rab_detail'],
					'nama'=>$rcrd['uraian'],
					'nilai'=>($rcrd['harga_satuan']?(float)$rcrd['vol']*(float)$rcrd['harga_satuan']:(float)$rcrd['total_harga']),
					'nilai_satuan'=>($rcrd['harga_satuan']?$rcrd['harga_satuan']:((float)$rcrd['total_harga']/($rcrd['vol']?(float)$rcrd['vol']:1))),
					'vol'=>$rcrd['vol'],
					'satuan'=>$rcrd['satuan'],
					'tgl'=>date('d-m-Y',PHPExcel_Shared_Date::ExcelToPHP($rcrd['tanggal'])),
					'keterangan'=>$rcrd['keterangan'],
				);

		    	if($record['id_jasa_material']){
					$record[$this->pk] = $this->conn->GetOne("select id_realisasi from rab_realisasi where id_jasa_material = ".$this->conn->escape($record['id_jasa_material'])." and tgl = ".$this->conn->escape($record['tgl']));
		    	}
				elseif($record['id_rab_detail'])
					$record[$this->pk] = $this->conn->GetOne("select id_realisasi from rab_realisasi where id_rab_detail = ".$this->conn->escape($record['id_rab_detail'])." and tgl = ".$this->conn->escape($record['tgl']));
				else
					$record[$this->pk] = $this->conn->GetOne("select id_realisasi from rab_realisasi where trim(lower(nama)) = trim(lower(".$this->conn->escape($record['nama']).")) and to_char(created_date,'DD-MM-YYYY') = ".$this->conn->escape(date('d-m-Y')));

	    		if(!$record['id_rab_detail'])
	    			$record['id_rab_detail'] = $id_rab_detail;
	    		
		    	$this->data['row'] = $record;

            	$this->_setLogRecord($record,$record[$this->pk]);

		    	$error = $this->_isValidImport($record);
		    	if($error){
		    		$return['error'] = $error;
		    	}else{
			    	if($record[$this->pk]){
			    		$return = $this->model->Update($record, $this->pk."=".$record[$this->pk]);
			    		$id = $record[$this->pk];

				    	if($return['success']){
				    		$ret = $this->_afterUpdate($id);

				    		if(!$ret){
				    			$return['success'] = false;
				    			$return['error'] = "Gagal update";
				    		}
				    	}
			    	}else{

			    		$return = $this->model->Insert($record);
			    		$id = $return['data'][$this->pk];

				    	if($return['success']){
				    		$ret = $this->_afterInsert($id);

				    		if(!$ret){
				    			$return['success'] = false;
				    			$return['error'] = "Gagal insert";
				    		}
				    	}
			    	}
			    }

				if(!$return['success'])
					break;				
			}


			if (!$return['error'] && $return['success']) {
            	$this->model->conn->trans_commit();
				SetFlash('suc_msg', $return['success']);
			}else{
            	$this->model->conn->trans_rollback();
				$return['error'] = "Gagal import. ".$return['error'];
				$return['success'] = false;
			}
		}else{
			$return['error'] = "Format file tidak sesuai";
		}

		echo json_encode($return);
	}

	public function export_list($id_rab=null, $id_rab_detail=null){
		$this->load->library('PHPExcel');
		$this->load->library('Factory');
		$excel = new PHPExcel();
		$excel->setActiveSheetIndex(0);	
		$excelactive = $excel->getActiveSheet();


		#header export
		$header=$this->HeaderExport();

		$row = 1;

	    foreach($header as $r){
	    	if(!$col)
	    		$col = 'A';
	    	else
	        	$col++;    

	        $excelactive->setCellValue($col.$row,$r['name']);
	    }

		$excelactive->getStyle('A1:'.$col.$row)->getFont()->setBold(true);
        $excelactive
		    ->getStyle('A1:'.$col.$row)
		    ->getFill()
		    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB('6666ff');

	    #data
		$rs = $this->conn->GetArray("select a.*, nvl(nilai_satuan,0)*nvl(vol,1)*nvl(day,1) as nilai from rab_rab_detail a where id_rab = ".$this->conn->escape($id_rab)." 
			and id_rab_detail_parent = ".$this->conn->escape($id_rab_detail)."
			order by kode_biaya, id_rab_detail");

		if(!$rs){
			$rs = $this->conn->GetArray("select a.*, nvl(nilai_satuan,0)*nvl(vol,1)*nvl(day,1) as nilai from rab_rab_detail a where id_rab = ".$this->conn->escape($id_rab)." 
			and id_rab_detail = ".$this->conn->escape($id_rab_detail)."
			order by kode_biaya, id_rab_detail");
		}


		$rows = array();

		$i = 0;
		$this->GenerateTree($rs, "id_rab_detail_parent", "id_rab_detail", "uraian", $rows, $id_rab_detail, $i, 0, null);

		$rows1 = $this->conn->GetArray("select a.*, nvl(vol,1)*nvl(harga_satuan,0) as nilai from rab_jasa_material a where id_rab = ".$this->conn->escape($id_rab));

		$rowsjasamaterial = array();
		foreach($rows1 as $r)
			$rowsjasamaterial[$r['id_pos_anggaran']][$r['jasa_material']][$r['kode_biaya']][] = $r;

		$row = 2;
        foreach($rows as $r){
        	unset($r['keterangan']);
        	unset($r['vol']);
        	$r['uraian'] = htmlspecialchars_decode(str_replace(array("<b>","</b>"), "", $r['uraian']));

			if($r['sumber_nilai']==1 or !$r['nilai_satuan']){
	    		$induk = $r['uraian'];
				continue;
			}

			if($r['sumber_nilai']==3){
				if(!empty($rowsjasamaterial[$r['id_pos_anggaran']][$r['jasa_material']][$r['kode_biaya']])){
					$rows1 = $rowsjasamaterial[$r['id_pos_anggaran']][$r['jasa_material']][$r['kode_biaya']];
					foreach($rows1 as $r2){
						$r2['id_rab_detail'] = $r['id_rab_detail'];
						$r2['uraian'] = $r2['nama'];
        				unset($r2['keterangan']);
        				unset($r2['vol']);
        				unset($r2['harga_satuan']);
        				$r2['uraian'] = htmlspecialchars_decode(str_replace(array("<b>","</b>"), "", $r2['uraian']));

				    	$col = 'A';
				    	$r2['induk'] = $induk;
				    	foreach($header as $r1){
			           		$excelactive->setCellValue($col.$row,$r2[$r1['name']]);
			           		$col++;
				    	}
		            	$row++;
					}
				}
			}else{
		    	$col = 'A';
		    	$r['induk'] = $induk;
		    	foreach($header as $r1){
	           		$excelactive->setCellValue($col.$row,$r[$r1['name']]);
	           		$col++;
		    	}
            	$row++;
		    }
        }


	    $objWriter = Factory::createWriter($excel,'Excel2007');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->ctrl.date('Ymd').'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
	}

	function GenerateTree(&$row, $colparent, $colid, $collabel, &$return=array(), $valparent=null, &$i=0, $level=0, $spacea = "&nbsp;➥&nbsp;", $max_level=100){

		$level++;
		foreach ($row as $key => $value) {
			# code...
			if(trim($value[$colparent])==trim($valparent)){
			
				$space = '';
				for($k=1; $k<$level; $k++){
					$space .= $spacea;
				}

				$value[$collabel] = $space.$value[$collabel];
				$value['level'] = $level;

				if($level<=1 or (!$value['kode_biaya'] && $level<=2))
					$value[$collabel] = "<b>".$value[$collabel]."</b>";


				if($level<=$max_level){
					$return[$i]=$value;
				}

				$i++;

				$temp = $row;
				unset($temp[$key]);

				$this->GenerateTree($temp, $colparent, $colid, $collabel, $return, $value[$colid], $i, $level,$spacea, $max_level);

				$row = $temp;
			}
		}

		if($row && $level==1)
			$return = array_merge($return, $row);
	}

	public function Add($id_rab_detail=0, $id_jasa_material=null){
		$this->Edit($id_rab_detail,$id_jasa_material);
	}

	public function Realisasi($id_rab_detail=0, $id_jasa_material=null){
		$this->_beforeDetail($id_rab_detail, $id_jasa_material);

		$this->data['id_jasa_material'] = $id_jasa_material;

		if($id_jasa_material)
			$rows = $this->conn->GetArray("select * from rab_realisasi where id_jasa_material = ".$this->conn->escape($id_jasa_material)." and id_rab_detail = ".$this->conn->escape($id_rab_detail));
		else
			$rows = $this->conn->GetArray("select * from rab_realisasi where id_rab_detail = ".$this->conn->escape($id_rab_detail));

		$this->data['rowsrealisasi'] = $rows;

		$keystr = $this->conn->GetKeysStr($rows, 'id_realisasi');

		if($keystr){
			$rows = $this->conn->GetArray("select id_realisasi, id_realisasi_files as id, client_name as name
				from rab_realisasi_files
				where id_realisasi in ($keystr)");

			foreach($rows as $r){
				$this->data['filerealisasi'][$r['id_realisasi']][] = $r;
			}
		}

		$this->View("panelbackend/rab_realisasi_adminrealisasi");
	}

	public function Edit($id_rab_detail=0, $id_jasa_material=null, $id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_rab_detail,$id_jasa_material,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if($id_jasa_material)
			$this->data['row']['id_jasa_material'] = $id_jasa_material;

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_rab_detail'] = $id_rab_detail;
			$this->data['row']['id_jasa_material'] = $record['id_jasa_material'];

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {
					$this->data['row'] = $return['data'];

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				$this->ctrl = "rab_realisasi_admin";
				SetFlash('suc_msg', $return['success']);
				redirect("panelbackend/rab_realisasi_admin/realisasi/$id_rab_detail/$id_jasa_material");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_rab_detail=null, $id_jasa_material=null, $id=null){

		$this->_beforeDetail($id_rab_detail, $id_jasa_material, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Delete($id_rab_detail=null, $id_jasa_material=null, $id=null){

        $this->model->conn->StartTrans();

        $this->_beforeDetail($id_rab_detail, $id_jasa_material, $id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");
			$this->ctrl = "rab_realisasi_admin";
			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/realisasi/$id_rab_detail/$id_jasa_material/$id");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab_detail/$id_jasa_material/$id");
		}

	}

	protected function _header($id_rab=null, $id=null){

		$this->data['id_rab'] = $id_rab;
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
		$this->data['id_pekerjaan'] = $id_pekerjaan = $this->data['rowheader2']['id_pekerjaan'];
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';

		if($id)
			$this->data['add_param'] .= $this->data['id_rab_detail']."/".$this->data['id_jasa_material'];
		else
			$this->data['add_param'] .= $id_rab;
	}

	protected function _beforeDetail($id_rab_detail=null, $id_jasa_material=null, $id=null){
		$this->data['id_rab_detail'] = $id_rab_detail;
		$this->data['id_jasa_material'] = $id_jasa_material;
		$this->data['rowheader3'] = $this->rabrabdetail->GetByPk($id_rab_detail);
		$this->data['rowheader4'] = $this->rabjasa_material->GetByPk($id_jasa_material);
		$this->_header($this->data['rowheader3']['id_rab'], $id);
	}

	protected function _afterInsert($id=null){
		$ret = true;
		if($ret)
			$ret = $this->_afterUpdate($id);

		return $ret;
	}

	protected function _afterUpdate($id){
		$ret = true;
		
		if($ret)
			$ret = $this->_delsertFiles($id);

		if($ret && $this->data['row']['id_jasa_material']){
			$realisasi = (float) $this->conn->GetOne("select sum(nvl(nilai,0)) from rab_realisasi where id_jasa_material = ".$this->conn->escape($this->data['row']['id_jasa_material']));

			$ret = $this->conn->Execute("update rab_jasa_material set nilai_realisasi = $realisasi where id_jasa_material = ".$this->conn->escape($this->data['row']['id_jasa_material']));
		}

		if($ret){
			$realisasi = (float) $this->conn->GetOne("select sum(nvl(nilai,0)) from rab_realisasi where id_rab_detail = ".$this->conn->escape($this->data['id_rab_detail']));

			$ret = $this->conn->Execute("update rab_rab_detail set nilai_realisasi = $realisasi where id_rab_detail = ".$this->conn->escape($this->data['id_rab_detail']));
		}

		if($ret)
			$ret = $this->_hitungRealisasiParent($this->data['rowheader3']['id_rab_detail_parent']);
		
		return $ret;
	}

	private function _delsertFiles($id_realisasi = null){
		$ret = true;

		if(!empty($this->post['file'])){
			foreach($this->post['file']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_realisasi'=>$id_realisasi), $v);
			}
		}
		return $ret;
	}

}