<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Mt_item extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/mt_itemlist";
		$this->viewdetail = "panelbackend/mt_itemdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah Item';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit Item';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail Item';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Item';
		}

		$this->load->model("Mt_itemModel","model");
		
		$this->load->model("Mt_jabatan_proyekModel","mtjabatanproyek");
		$this->data['mtjabatanproyekarr'] = $this->mtjabatanproyek->GetCombo();
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'select2'
		);
		$this->data['width'] = "800px";
	}

	protected function Header(){
		return array(
			array(
				'name'=>'kode', 
				'label'=>'Kode', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'keterangan_spec', 
				'label'=>'Keterangan Spec', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'id_jabatan_proyek', 
				'label'=>'Jabatan Proyek', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['mtjabatanproyekarr'],
			),
		);
	}

	protected function Record($id=null){
		return array(
			'nama'=>$this->post['nama'],
			'kode'=>$this->post['kode'],
			'keterangan_spec'=>$this->keterangan(),
			'id_spec_item'=>$this->post['id_spec_item'],
			'id_jabatan_proyek'=>$this->post['id_jabatan_proyek'],
		);
	}

	protected function Rules(){
		$return = array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"required|max_length[200]",
			),
			"kode"=>array(
				'field'=>'kode', 
				'label'=>'Kode', 
				'rules'=>"max_length[20]",
			),
			"keterangan_spec"=>array(
				'field'=>'keterangan_spec', 
				'label'=>'Keterangan Spec', 
				'rules'=>"max_length[4000]",
			),
			"id_jabatan_proyek"=>array(
				'field'=>'id_jabatan_proyek', 
				'label'=>'Jabatan Proyek', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['mtjabatanproyekarr']))."]",
			),
		);

		if($this->data['row']['id_spec_item']){
			$return['attribute'] = array(
				'field'=>'attribute', 
				'label'=>'Sepesifikasi', 
				'rules'=>"callback_cekspek",
			);
		}

		return $return;
	}

	public function Detail($id=null){
		redirect("panelbackend/mt_item_detail/index/$id");
	}

    public function cekspek($str)
    {

		foreach ($this->data['row']['attribute'] as $k=>$v) {
			if(!$v){
				unset($this->data['row']['attribute'][$k]);
				continue;
			}
		}
    	if(empty($this->data['row']['attribute'])){
            $this->form_validation->set_message('cekspek', 'Attribute harus di isi');
            return FALSE;
    	}

    	return true;
    }

	protected function _afterDetail($id=null){
		if($this->data['row']['id_spec_item']){
			$this->data['attribute_default'] = $this->conn->GetArray("select * from mt_spec_attribute 
			where id_spec_item = ".$this->conn->escape($this->data['row']['id_spec_item'])." 
			order by is_main desc");

			$this->data['specitemarr'][$this->data['row']['id_spec_item']] = $this->conn->GetOne("select nama from mt_spec_item where id_spec_item = ".$this->conn->escape($this->data['row']['id_spec_item']));
		}

		if($this->data['row']['id_item']){
			$this->data['attribute'] = $this->conn->GetList("select id_spec_attribute as key, nilai as val from mt_item_spec_attribute
			where id_item = ".$this->conn->escape($this->data['row']['id_item']));
		}
	}

	protected function _afterInsert($id){
		$ret = true;

		if($ret)
			$ret = $this->_afterUpdate($id);

		return $ret;
	}

	protected function _afterUpdate($id){
		$ret = true;

		if($ret)
			$ret = $this->_delsertSpec($id);

		return $ret;
	}

	private function keterangan(){

		$keterangan_spec = "";
		if(count($this->post['attribute'])){
			$id_spec_attribute_arr = array();
			$id_spec_nilai_arr = array();

			foreach ($this->post['attribute'] as $k=>$v) {
				if(!$v){
					unset($this->post['attribute'][$k]);
					continue;
				}
				$id_spec_attribute_arr[] = $k;
				$id_spec_nilai_arr[$k] = $v;
			}

			if(count($this->post['attribute'])){
				$keterangan_spec_arr = array();
				$rowatt = $this->conn->GetArray("select id_spec_attribute, nama from mt_spec_attribute where id_spec_attribute in (".implode(",",$id_spec_attribute_arr).") and id_spec_item = ".$this->conn->escape($this->post['id_spec_item']));

				foreach($rowatt as $r){
					$keterangan_spec_arr[] = $r['nama']." ".$id_spec_nilai_arr[$r['id_spec_attribute']];
				}

				$keterangan_spec = $this->post['nama'].", ".implode(", ", $keterangan_spec_arr);
			}
		}

		return $keterangan_spec;
	}

	private function _delsertSpec($id_item = null){
		$ret = $this->conn->Execute("delete from mt_item_spec_attribute where id_item = ".$this->conn->escape($id_item));

		$MainSpecarr = array();

		if(count($this->post['attribute'])){
			foreach ($this->post['attribute'] as $key => $v) {
				if(!$ret)
					break;

				$record = array();
				$record['id_item'] = $id_item;
				$record['id_spec_attribute'] = $key;
				$record['nilai'] = $v;

				$ret = $this->conn->goInsert('mt_item_spec_attribute', $record);
			}
		}

		return $ret;
	}
}