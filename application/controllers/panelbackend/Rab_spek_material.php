<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_spek_material extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_spek_materiallist";
		$this->viewdetail = "panelbackend/mt_spek_materialdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		$this->data['no_header'] = false;

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['no_header'] = false;
		}

		unset($this->access_role['add']);
		unset($this->access_role['edit']);
		unset($this->access_role['delete']);

		$this->load->model("Mt_spek_materialModel","model");
		$this->load->model("Mt_spek_material_filesModel","modelfile");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		$this->data['configfile'] = $this->config->item('file_upload_config');
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'upload','select2'
		);
	}
	public function Index($id_proyek=0, $page=0){

		$this->_beforeDetail($id_proyek);

		$this->data['header']=$this->Header();
		$this->_setFilter("id_wh_unit = ".$this->conn->escape($this->data['id_wh_unit']));

		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_proyek"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;


		$this->View($this->viewlist);
	}

	protected function _beforeDetail($id_proyek=null, $id=null){
		$this->data['id_proyek'] = $id_proyek;
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['id_wh_unit'] = $this->data['rowheader']['id_wh_unit'];
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_proyek;
	}

	public function go_print($id_proyek=0, $page=0){

		$this->_beforeDetail($id_proyek);

		$this->_setFilter("a.id_wh_unit = ".$this->conn->escape($this->data['id_wh_unit']));

		$this->data['sub_page_title'] = $this->data['rowheader']['nama_proyek'];
		$this->data['sub_page_title'] .= "<br/>".$this->data['rowheader1']['nama_pekerjaan'];
		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout4";

		$this->data['header']=$this->Header();

		$this->data['list']=$this->_getListPrint();

		$this->View($this->viewprint);
	}


	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'spesifikasi', 
				'label'=>'Spesifikasi', 
				'type'=>"varchar2",
			),
		);
	}

}