<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Pr_hpe extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/pr_hpelist";
		$this->viewdetail = "panelbackend/pr_hpedetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";
			$this->data['width'] = "1200px";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah HPE';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit HPE';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail HPE';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar HPE';
		}

		$this->load->model("Pr_hpeModel","model");
		$this->load->model("Pr_hpe_detailModel","modeldetail");
		
		$this->load->model("Mt_status_prModel","mtstatuspr");
		$this->data['mtstatusprarr'] = $this->mtstatuspr->GetCombo();
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");

		$this->data['configfile'] = $this->config->item('file_upload_config');

		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;

		$this->plugin_arr = array(
			'datepicker','select2','upload'
		);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_dibutuhkan', 
				'label'=>'Tgl. Dibutuhkan', 
				'width'=>"auto",
				'type'=>"date",
			),
		);
	}

	protected function Record($id=null){
		$return = array(
			'nama'=>$this->post['nama'],
			'tgl_dibutuhkan'=>$this->post['tgl_dibutuhkan'],
			'id_status_hpe'=>1,
		);

		return $return;
	}

	protected function Rules(){
		return array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"required|max_length[1000]",
			),
			"tgl_dibutuhkan"=>array(
				'field'=>'tgl_dibutuhkan', 
				'label'=>'Tgl. Dibutuhkan', 
				'rules'=>"required",
			),
		);
	}
	public function Index($id_rab=0, $page=0){

		unset($_SESSION[SESSION_APP][$this->page_ctrl]['list_sort']);
		$this->_beforeDetail($id_rab);
		$this->data['header']=$this->Header();
		$this->_setFilter("id_rab = ".$this->conn->escape($id_rab));

		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_rab"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;


		$this->View($this->viewlist);
	}

	public function Add($id_rab=0){
		$this->Edit($id_rab);
	}

	private function _ajukanHPE(){
		$this->conn->StartTrans();

		$data = array();
		$data['header']['tgl'] = date("d-m-Y");
		$data['header']['nama'] = $this->data['row']['nama'];
		$data['header']['id_source'] = $this->data['row']['id_hpe'];
		$data['header']['tgl_dibutuhkan'] = $this->data['row']['tgl_dibutuhkan'];
		$data['header']['source'] = 'resplan';

		$this->_afterDetail($this->data['row']['id_hpe']);

		$data['detail'] = array();

		$lampirankosongarr = array();

		foreach($this->data['rows'] as $i=>$r){
			$data['detail'][$i]['vol'] = $r['vol'];
			$data['detail'][$i]['satuan'] = $r['satuan'];
			$data['detail'][$i]['nama'] = $r['uraian'];
			$data['detail'][$i]['keterangan'] = $r['keterangan'];
			$data['detail'][$i]['id_source_detail'] = $r['id_hpe_detail'];

			$lampiranarr = array();

			if($r['id_jasa_material']){
				$rf = $this->conn->GetArray("select * from rab_jasa_material_files where id_jasa_material = ".$this->conn->escape($r['id_jasa_material']));
				foreach($rf as $r3){
					$lampiranarr[] = $r3;
				}

				if(empty($lampiranarr) && $r['jasa_material']=='1'){
					$lampirankosongarr[] = $r['uraian'];
				}
			}

			if($r['id_rab_detail']){
				$rf = $this->conn->GetArray("select * from rab_rab_detail_files where id_rab_detail = ".$this->conn->escape($r['id_rab_detail']));
				foreach($rf as $r3){
					$r3 = array(
						'file_name'=>$r3['file_name'],
						'client_name'=>$r3['client_name'],
						'file_type'=>$r3['file_type'],
					);
					$lampiranarr[] = $r3;
				}
			}

			$data['detail'][$i]['lampiran']=json_encode($lampiranarr);
		}

		if(!empty($lampirankosongarr)){
			$this->conn->trans_rollback();
			SetFlash("err_msg", "<b>TOR wajib dilampirkan berikut data yang lampiranya kosong : </b><br/>".implode("<br/>", $lampirankosongarr));
		}else{

			$pos = array();
			$pos['data'] = $data;

			$return = $this->reqscm("insert_hpe", $pos);

			if($return['success'])
				$return = $this->model->Update(array('id_status_hpe'=>2), "id_hpe=".$this->conn->escape($this->data['row']['id_hpe']));

			if($return['success']){
				$this->conn->trans_commit();
				SetFlash("suc_msg","Berhasil");
			}
			else{
				$this->conn->trans_rollback();
				SetFlash("err_msg","Gagal");
			}
		}

		redirect("$this->page_ctrl/detail/".$this->data['id_rab']."/".$this->data['row']['id_hpe']);
	}

	private function _getHPE(){
		$this->conn->StartTrans();

		$data = array();
		$data['id_source'] = $this->data['row']['id_hpe'];
		$data['source'] = "resplan";

		$pos = array();
		$pos['data'] = $data;

		$return = $this->reqscm("get_hpe", $pos);

		$ret = $return['data'];

		if($ret['detail'])
			foreach($ret['detail'] as $r){
				if(!$return['success'])
					break;

				$record = array();
				$record['harga_satuan'] = $r['harga_satuan'];
				$id_hpe_detail = $r['id_source_detail'];
				$return = $this->modeldetail->Update($record, "id_hpe_detail = ".$this->conn->escape($id_hpe_detail));

				if($return['success']){
					$rw = $this->modeldetail->GetByPk($id_hpe_detail);
					if($rw['id_jasa_material'])
						$return['success'] = $this->conn->goUpdate("rab_jasa_material",array("harga_satuan"=>$record['harga_satuan']),"id_jasa_material = ".$this->conn->escape($rw['id_jasa_material']));
					elseif($rw['id_rab_detail'])
						$return['success'] = $this->conn->goUpdate("rab_rab_detail",array("nilai_satuan"=>$record['harga_satuan']),"id_rab_detail = ".$this->conn->escape($rw['id_rab_detail']));

				}
		}

		if($return['success'])
			$return['success'] = $this->_hitungTotalRabParent();

		if($return['success']){
			$record = array();
			$record['id_status_hpe'] = $ret['header']['id_status_hpe'];

			if($ret['header']['riwayat'] && $ret['header']['id_status_hpe']==3)
				$record['riwayat'] = $ret['header']['riwayat'];

			$return = $this->model->Update($record, "id_hpe=".$this->conn->escape($this->data['row']['id_hpe']));
		}

		if($return['success']){
			$this->conn->trans_commit();
			SetFlash("suc_msg","Berhasil");
		}
		else{
			$this->conn->trans_rollback();
			SetFlash("err_msg","Gagal");
		}

		redirect("$this->page_ctrl/detail/".$this->data['id_rab']."/".$this->data['row']['id_hpe']);
	}

	public function Edit($id_rab=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_rab,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_rab'] = $id_rab;

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_rab/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _onDetail($id=null){

		if($this->post['act']=='ajukan_hpe' && $id && !($this->data['row']['id_status_hpe']==2 or $this->data['row']['id_status_hpe']==4)){
			$this->_ajukanHPE();
		}

		if($this->post['act']=='cek_hpe' && $id && ($this->data['row']['id_status_hpe']==2 or $this->data['row']['id_status_hpe']==4)){
			$this->_getHPE();
		}

		if($this->data['row']['id_status_hpe']==2 or $this->data['row']['id_status_hpe']==4){
			$this->data['edited'] = false;
			$this->access_role['edit'] = false;
			$this->access_role['delete'] = false;
		}
	}

	public function Detail($id_rab=null, $id=null){

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function go_print($id_rab=null, $id=null){

		$this->data['page_title'] = 'Daftar Permintaan Perhitungan HPE';

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout4";

		$this->View("panelbackend/pr_hpeprint");
	}

	public function Delete($id_rab=null, $id=null){

        $this->model->conn->StartTrans();

        $this->_beforeDetail($id_rab,$id);

		$this->data['row'] = $this->model->GetByPk($id);

		if($this->data['row']['id_status_hpe']==2 or $this->data['row']['id_status_hpe']==4){
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab/$id");
		}

		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_rab");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab/$id");
		}

	}

	protected function _beforeDetail($id_rab=null, $id=null){
		$this->data['id_rab'] = $id_rab;
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
		$this->data['id_pekerjaan'] = $id_pekerjaan = $this->data['rowheader2']['id_pekerjaan'];
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_rab;
		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
	}

	protected function _afterDetail($id=null){
		if($this->data['row']['id_rab']){
			$this->data['rows'] = $this->conn->GetArray("select
				a.*, nvl(b.nama, c.uraian) as uraian, nvl(b.satuan, c.satuan) as satuan, nvl(b.vol, c.vol) as vol, nvl(b.keterangan, c.keterangan) as keterangan, b.jasa_material
				from pr_hpe_detail a
				left join rab_jasa_material b on a.id_jasa_material = b.id_jasa_material
				left join rab_rab_detail c on a.id_rab_detail = c.id_rab_detail
				where a.id_hpe = ".$this->conn->escape($id)."
				order by uraian");
		}
		if($this->data['row']['id_status_hpe']==2 or $this->data['row']['id_status_hpe']==4){
			$this->data['edited'] = false;
			$this->access_role['edit'] = false;
			$this->access_role['delete'] = false;
		}
	}

	function GenerateTree(&$row, $colparent, $colid, $collabel, &$return=array(), $valparent=null, &$i=0, $level=0, $spacea = "&nbsp;➥&nbsp;", $max_level=100){

		$level++;
		foreach ($row as $key => $value) {
			# code...
			if(trim($value[$colparent])==trim($valparent)){
			
				$space = '';
				for($k=1; $k<$level; $k++){
					$space .= $spacea;
				}

				$value[$collabel] = $space.$value[$collabel];
				$value['level'] = $level;

				if($level<=1 or (!$value['kode_biaya'] && $level<=2))
					$value[$collabel] = "<b>".$value[$collabel]."</b>";


				if($level<=$max_level){
					$return[$i]=$value;
				}

				$i++;

				$temp = $row;
				unset($temp[$key]);

				$this->GenerateTree($temp, $colparent, $colid, $collabel, $return, $value[$colid], $i, $level,$spacea, $max_level);

				$row = $temp;
			}
		}

		if($row && $level==1)
			$return = array_merge($return, $row);
	}

	protected function _isValidImport($record){
	}

	public function HeaderExport(){
		return array(
			array(
				'name'=>'id_jasa_material', 
				'label'=>'id_jasa_material', 
			),
			array(
				'name'=>'id_rab_detail', 
				'label'=>'id_rab_detail', 
			),
			array(
				'name'=>'uraian', 
				'label'=>'Jasa/Material', 
			),
			array(
				'name'=>'vol', 
				'label'=>'vol', 
			),
			array(
				'name'=>'satuan', 
				'label'=>'satuan', 
			),
		);
	}

	protected function _beforeDelete($id=null){
		$ret = true;
		$rows = $this->conn->GetArray("select * from pr_hpe_detail where id_hpe = ".$this->conn->escape($id));

		foreach($rows as $r){
			if(!$ret)
				break;

			$ret = $this->conn->Execute("delete from pr_hpe_detail where id_hpe_detail = ".$this->conn->escape($r['id_hpe_detail']));

			if($ret){
				if($r['id_jasa_material']){
					$ret = $this->conn->Execute("update rab_jasa_material set harga_satuan = null where id_jasa_material = ".$this->conn->escape($r['id_jasa_material']));
				}elseif($r['id_rab_detail']){
					$ret = $this->conn->Execute("update rab_rab_detail set nilai_satuan = null where id_rab_detail = ".$this->conn->escape($r['id_rab_detail']));
				}
			}
		}

		return $ret;
	}

	public function import_list($id_rab=null, $id_hpe=null){

		$file_arr = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel','application/wps-office.xls','application/wps-office.xlsx');

		if(in_array($_FILES['importupload']['type'], $file_arr)){

			$this->_beforeDetail($id_rab, $id_hpe);

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("","");

			$this->load->library('Factory');
			$inputFileType = Factory::identify($_FILES['importupload']['tmp_name']);
			$objReader = Factory::createReader($inputFileType);
			$excel = $objReader->load($_FILES['importupload']['tmp_name']);
			$sheet = $excel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
            $this->modeldetail->conn->StartTrans();

			#header export
			$header=array(
				array(
					'name'=>$this->modeldetail->pk
				)
			);
			$header=array_merge($header,$this->HeaderExport());

			$id_hpe_detail = array();

			for ($row = 2; $row <= $highestRow; $row++){ 

		    	$col = 'A';
		    	$record = array();
		    	$record['id_hpe'] = $id_hpe;
		    	foreach($header as $r1){
		    		if($r1['type']=='list')
		           		$record[$r1['name']] = (string)$sheet->getCell($col.$row)->getValue();
		           	else
		           		$record[$r1['name']] = $sheet->getCell($col.$row)->getValue();
		           	
	           		$col++;
		    	}

		    	if(!$record['id_jasa_material'] && !$record['id_rab_detail'])
		    		continue;

		    	$this->data['row'] = $record;

		    	$error = $this->_isValidImport($record);
		    	if($error){
		    		$return['error'] = $error;
		    	}else{
			    	if($record[$this->modeldetail->pk]){
			    		$return = $this->modeldetail->Update($record, $this->modeldetail->pk."=".$record[$this->modeldetail->pk]);
			    		$id_hpe_detail[] = $id = $record[$this->modeldetail->pk];
			    	}else{
			    		$return = $this->modeldetail->Insert($record);
			    		$id_hpe_detail[] = $id = $return['data'][$this->modeldetail->pk];
			    	}
			    }
			}


			if (!$return['error'] && $return['success'] && count($id_hpe_detail)) {
				$return['success'] = $this->conn->Execute("delete 
					from pr_hpe_detail 
					where id_hpe = ".$this->conn->escape($id_hpe)." 
					and id_hpe_detail not in (".implode(", ", $id_hpe_detail).")");
			}

			if (!$return['error'] && $return['success']) {
            	$this->modeldetail->conn->trans_commit();
				SetFlash('suc_msg', "Import Berhasil");
			}else{
            	$this->modeldetail->conn->trans_rollback();
				$return['error'] = "Gagal import. ".$return['error'];
				$return['success'] = false;
			}
		}else{
			$return['error'] = "Format file tidak sesuai";
		}

		echo json_encode($return);
	}

	public function export_list($id_rab=null, $id=null){
		$this->load->library('PHPExcel');
		$this->load->library('Factory');
		$excel = new PHPExcel();
		$excel->setActiveSheetIndex(0);	
		$excelactive = $excel->getActiveSheet();


		#header export
		$header=array(
			array(
				'name'=>$this->modeldetail->pk
			)
		);
		$header=array_merge($header,$this->HeaderExport());

		$row = 1;

	    foreach($header as $r){
	    	if(!$col)
	    		$col = 'A';
	    	else
	        	$col++;    

	        $excelactive->setCellValue($col.$row,$r['name']);
	    }

		$excelactive->getStyle('A1:'.$col.$row)->getFont()->setBold(true);
        $excelactive
		    ->getStyle('A1:'.$col.$row)
		    ->getFill()
		    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB('6666ff');

	    #data
	    $rows = $this->conn->GetArray("select a.* from (
	    	select
	    	nvl(c.nama, e.uraian||' '||a.uraian) as uraian, 
	    	nvl(c.satuan, a.satuan) as satuan,
	    	nvl(d.id_hpe_detail, b.id_hpe_detail) as id_hpe_detail, 
	    	nvl(c.vol, a.vol) as vol,
	    	c.id_jasa_material, a.id_rab_detail
	    	from rab_rab_detail a
	    	left join rab_jasa_material c 
	    	on a.id_pos_anggaran = c.id_pos_anggaran
	    	and a.jasa_material = c.jasa_material
	    	and trim(lower(a.kode_biaya))=trim(lower(c.kode_biaya))
	    	and c.id_rab = ".$this->conn->escape($id_rab)."
	    	left join pr_hpe_detail b on a.id_rab_detail = b.id_rab_detail and b.id_hpe=".$this->conn->escape($id)." and b.id_jasa_material is null
	    	left join pr_hpe_detail d on c.id_jasa_material = d.id_jasa_material and d.id_hpe=".$this->conn->escape($id)."
	    	left join rab_rab_detail e on a.id_rab_detail_parent = e.id_rab_detail
	    	where a.id_rab = ".$this->conn->escape($id_rab)." and a.sumber_nilai in (1,3) and a.id_rab_detail_parent is not null) a 
	    	where vol > 0 order by uraian ");

		$row = 2;
        foreach($rows as $r){
	    	$col = 'A';
	    	foreach($header as $r1){
           		$excelactive->setCellValue($col.$row,$r[$r1['name']]);
           		$col++;
	    	}
            $row++;
        }


	    $objWriter = Factory::createWriter($excel,'Excel2007');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->ctrl.date('Ymd').'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
	}
}