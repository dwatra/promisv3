<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Wbs_realisasi extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/wbs_realisasilist";
		$this->viewdetail = "panelbackend/wbs_realisasidetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah WBS Realisasi';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit WBS Realisasi';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail WBS Realisasi';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar WBS Realisasi';
		}

		$this->data['width'] = "1000px";

		$this->load->model("Wbs_realisasiModel","model");
		$this->load->model("Wbs_plan_detailModel","wbsplandetail");
		$this->data['wbsplandetailarr'] = $this->wbsplandetail->GetCombo();

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			''
		);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'id_plan', 
				'label'=>'Plan', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['wbsplandetailarr'],
			),
			array(
				'name'=>'urutan', 
				'label'=>'Plan Detail', 
				'width'=>"auto",
				'type'=>"number",
			),
			array(
				'name'=>'prosentase', 
				'label'=>'Prosentase', 
				'width'=>"auto",
				'type'=>"number",
			),
			array(
				'name'=>'keterangan', 
				'label'=>'Keterangan', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl', 
				'label'=>'Tgl.', 
				'width'=>"auto",
				'type'=>"number",
			),
		);
	}

	protected function Record($id=null){
		return array(
			'id_plan'=>Rupiah2Number($this->post['id_plan']),
			'urutan'=>Rupiah2Number($this->post['urutan']),
			'prosentase'=>Rupiah2Number($this->post['prosentase']),
			'keterangan'=>$this->post['keterangan'],
			'tgl'=>Rupiah2Number($this->post['tgl']),
		);
	}

	protected function Rules(){
		return array(
			"id_plan"=>array(
				'field'=>'id_plan', 
				'label'=>'Plan', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['wbsplandetailarr']))."]|max_length[10]",
			),
			"urutan"=>array(
				'field'=>'urutan', 
				'label'=>'Plan Detail', 
				'rules'=>"required|numeric|max_length[10]",
			),
			"prosentase"=>array(
				'field'=>'prosentase', 
				'label'=>'Prosentase', 
				'rules'=>"required|numeric|max_length[10]",
			),
			"keterangan"=>array(
				'field'=>'keterangan', 
				'label'=>'Keterangan', 
				'rules'=>"max_length[4000]",
			),
			"tgl"=>array(
				'field'=>'tgl', 
				'label'=>'Tgl.', 
				'rules'=>"required|numeric|max_length[11]",
			),
		);
	}

}