<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_scope extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_scopelist";
		$this->viewdetail = "panelbackend/rab_scopedetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah Scope';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit Scope';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail Scope';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Scope';
		}

		$this->load->model("Rab_scopeModel","model");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("Rab_pekerjaan_filesModel","modelfile");
		$this->data['configfile'] = $this->config->item('file_upload_config');
		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'upload'
		);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'equipment', 
				'label'=>'Equipment', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'id_scope_parent', 
				'label'=>'Scope Parent', 
				'width'=>"auto",
				'type'=>"list",
			),
			array(
				'name'=>'detail_sow', 
				'label'=>'Detail SOW', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'pic', 
				'label'=>'PIC', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'term_of_condition', 
				'label'=>'Term OF Condition', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
		);
	}

	protected function Record($id=null){
		return array(
			'equipment'=>$this->post['equipment'],
			'id_scope_parent'=>$this->post['id_scope_parent'],
			'detail_sow'=>$this->post['detail_sow'],
			'pic'=>$this->post['pic'],
			'term_of_condition'=>$this->post['term_of_condition'],
		);
	}

	protected function Rules(){
		return array(
			"equipment"=>array(
				'field'=>'equipment', 
				'label'=>'Equipment', 
				'rules'=>"max_length[200]",
			),
			"detail_sow"=>array(
				'field'=>'detail_sow', 
				'label'=>'Detail SOW', 
				'rules'=>"max_length[4000]",
			),
			"pic"=>array(
				'field'=>'pic', 
				'label'=>'PIC', 
				'rules'=>"max_length[20]",
			),
			"term_of_condition"=>array(
				'field'=>'term_of_condition', 
				'label'=>'Term OF Condition', 
				'rules'=>"max_length[4000]",
			),
		);
	}
/*
	protected function _limit(){
		return -1;
	}*/

	public function Index($id_rab=0, $page=0){

		$this->_beforeDetail($id_rab);

		$this->_setFilter("a.id_rab = ".$this->conn->escape($id_rab));

		$this->data['list']=$this->_getList($page);

		$id_pekerjaan = $this->data['id_pekerjaan'];
		$rows = $this->conn->GetArray("select id_pekerjaan_files as id, client_name as name, jenis_file
			from rab_pekerjaan_files
			where jenis_file = 'scope__$id_pekerjaan' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		foreach($rows as $r){
			$this->data['row'][$r['jenis_file']]['id'][] = $r['id'];
			$this->data['row'][$r['jenis_file']]['name'][] = $r['name'];
		}

		$id_scope_str = $this->conn->GetKeysStr($this->data['list']['rows'], "id_scope");
		if($id_scope_str){
			$rows = $this->conn->GetArray("select a.*, b.nama 
				from rab_scope_jasa_material a 
				join rab_jasa_material b on a.id_jasa_material = b.id_jasa_material
				where id_scope in ($id_scope_str)");

			$this->data['rowsjasamaterial'] = array();
			foreach($rows as $r){
				$this->data['rowsjasamaterial'][$r['id_scope']][] = $r['nama'];
			}
		}

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_rab"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;

		$this->View($this->viewlist);
	}

	protected function _uploadFiles($jenis_file=null, $id=null){
		list($jenis_file1, $id_pekerjaan) = explode("__",$jenis_file);
		$id_pekerjaan = str_replace("upload", "", $id_pekerjaan);

		$name = $_FILES[$jenis_file]['name'];

		$this->data['configfile']['file_name'] = $jenis_file.time().$name;

		$this->load->library('upload', $this->data['configfile']);

        if ( ! $this->upload->do_upload($jenis_file))
        {
            $return = array('error' => "File $name gagal upload, ".strtolower(str_replace(array("<p>","</p>"),"",$this->upload->display_errors())));
        }
        else
        {
    		$upload_data = $this->upload->data();

			$record = array();
			$record['client_name'] = $upload_data['client_name'];
			$record['file_name'] = $upload_data['file_name'];
			$record['file_type'] = $upload_data['file_type'];
			$record['file_size'] = $upload_data['file_size'];
			$record['id_pekerjaan'] = $id_pekerjaan;
			$record['jenis_file'] = str_replace("upload","",$jenis_file);
			$ret = $this->modelfile->Insert($record);
			if($ret['success'])
			{
				$return = array('file'=>array("id"=>$ret['data'][$this->modelfile->pk],"name"=>$upload_data['client_name']));
			}else{
				unlink($upload_data['full_path']);
				$return = array('errors'=>"File $name gagal upload");
			}

        }

        return $return;

	}

	public function Add($id_rab=0){
		$this->Edit($id_rab);
	}

	public function Edit($id_rab=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_rab,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_rab'] = $id_rab;

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_rab/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_rab=null, $id=null){

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Delete($id_rab=null, $id=null){

        $this->model->conn->StartTrans();

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_rab");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab/$id");
		}

	}

	public function Delete_all($id_rab=null){

		$return = true;

        $this->model->conn->StartTrans();

        $rows = $this->model->GArray("*", " where id_rab = ".$this->conn->escape($id_rab));

        foreach($rows as $r){
        	if(!$return)
        		break;

        	$id = $r[$this->pk];

	        $this->_beforeDetail($id_rab, $id);

			$this->data['row'] = $r;
			
			$this->_onDetail($id);

			if (!$this->data['row'])
				$this->NoData();

			$return = $this->_beforeDelete($id);

			if($return){
				$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
			}

			if($return){
				$return1 = $this->_afterDelete($id);
				if(!$return1)
					$return = false;
			}

			if($return)
				$this->log("menghapus $id");
        }

        $this->model->conn->CompleteTrans();

		if ($return) {
			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_rab");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/index/$id_rab");
		}
	}

	protected function _beforeDetail($id_rab=null, $id=null){
		$this->data['id_rab'] = $id_rab;
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
		$this->data['id_pekerjaan'] = $id_pekerjaan = $this->data['rowheader2']['id_pekerjaan'];
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_rab;
		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		if($this->data['last_versi']<>$this->data['rowheader2']['id_rab']){
			$this->access_role['add']=0;
			$this->access_role['edit']=0;
			$this->access_role['delete']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['upload_file']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['save']=0;
		}
	}

	protected function _afterDelete($id){
		return $this->_afterInsert($id);
	}

	protected function _afterUpdate($id){
		return $this->_afterInsert($id);
	}

	protected function _afterInsert($id){
		$ret = true;
		return $ret;
	}

	public function import_list($id_rab=null){

		$file_arr = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel','application/wps-office.xls','application/wps-office.xlsx');

		if(in_array($_FILES['importupload']['type'], $file_arr)){

			$this->_beforeDetail($id_rab);

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("","");

			$this->load->library('Factory');
			$inputFileType = Factory::identify($_FILES['importupload']['tmp_name']);
			$objReader = Factory::createReader($inputFileType);
			$excel = $objReader->load($_FILES['importupload']['tmp_name']);
			$sheet = $excel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
            $this->model->conn->StartTrans();

			#header export
			$header=array(
				array(
					'name'=>$this->model->pk
				)
			);
			$header=array_merge($header,$this->HeaderExport());

			$id_parrent = array();
			for ($row = 2; $row <= $highestRow; $row++){ 

		    	$col = 'A';
		    	$record = array();
		    	$record['id_rab'] = $id_rab;
		    	foreach($header as $r1){
		    		if($r1['type']=='list')
		           		$record[$r1['name']] = (string)$sheet->getCell($col.$row)->getValue();
		           	else
		           		$record[$r1['name']] = $sheet->getCell($col.$row)->getValue();

		           	if($record['sub_equipment']){
		           		$equipment = strtolower(trim($record['equipment']));

		           		if(!$equipment)
		           			$id_scope_parent = $id;
		           		else
	           				$id_scope_parent = $id_parrent[$equipment];

		           		if(!$id_scope_parent)
		           			$id_scope_parent = $this->conn->GetOne("select id_scope from rab_scope where lower(trim(equipment)) = '$equipment' and id_rab = ".$this->conn->escape($id_rab));

		           		if(!$id_scope_parent){
		           			$rec_parent = $record;
		           			unset($rec_parent['sub_equipment']);
		           			$return = $this->model->Insert($rec_parent);
		           			$id_scope_parent = $return['data'][$this->model->pk];
		           		}

		           		$record['id_scope_parent'] = $id_scope_parent;
		           		$record['equipment'] = $record['sub_equipment'];
		           		unset($record['sub_equipment']);
		           	}
		           	
	           		$col++;
		    	}

		    	$this->data['row'] = $record;

		    	$error = $this->_isValidImport($record);
		    	if($error){
		    		$return['error'] = $error;
		    	}else{
			    	if($record[$this->model->pk]){
			    		$return = $this->model->Update($record, $this->model->pk."=".$record[$this->model->pk]);
			    		$id = $record[$this->model->pk];

				    	if($return['success']){
				    		$ret = $this->_afterUpdate($id);

				    		if(!$ret){
				    			$return['success'] = false;
				    			$return['error'] = "Gagal update";
				    		}
				    	}
			    	}else{
			    		$return = $this->model->Insert($record);
			    		$id = $return['data'][$this->model->pk];

				    	if($return['success']){
				    		$ret = $this->_afterInsert($id);

				    		if(!$ret){
				    			$return['success'] = false;
				    			$return['error'] = "Gagal insert";
				    		}
				    	}
			    	}

			    	$id_parrent[trim(strtolower($record['equipment']))] = $id;
			    }

				if(!$return['success'])
					break;				
			}


			if (!$return['error'] && $return['success']) {
            	$this->model->conn->trans_commit();
				SetFlash('suc_msg', $return['success']);
			}else{
            	$this->model->conn->trans_rollback();
				$return['error'] = "Gagal import. ".$return['error'];
				$return['success'] = false;
			}
		}else{
			$return['error'] = "Format file tidak sesuai";
		}

		echo json_encode($return);
	}

	public function HeaderExport(){
		return array(
			array(
				'name'=>'equipment', 
				'label'=>'Equipment', 
			),
			array(
				'name'=>'sub_equipment', 
				'label'=>'Sub Equipment', 
			),
			array(
				'name'=>'detail_sow', 
				'label'=>'Detail Sow', 
			),
			array(
				'name'=>'pic', 
				'label'=>'PIC', 
			),
			array(
				'name'=>'term_of_condition', 
				'label'=>'Term of Condition', 
			),
		);
	}

	public function export_list($id_rab=null){
		$this->load->library('PHPExcel');
		$this->load->library('Factory');
		$excel = new PHPExcel();
		$excel->setActiveSheetIndex(0);	
		$excelactive = $excel->getActiveSheet();


		#header export
		$header=array(
			array(
				'name'=>$this->model->pk
			)
		);
		$header=array_merge($header,$this->HeaderExport());

		$row = 1;

	    foreach($header as $r){
	    	if(!$col)
	    		$col = 'A';
	    	else
	        	$col++;    

	        $excelactive->setCellValue($col.$row,$r['name']);
	    }

		$excelactive->getStyle('A1:'.$col.$row)->getFont()->setBold(true);
        $excelactive
		    ->getStyle('A1:'.$col.$row)
		    ->getFill()
		    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB('6666ff');

	    #data
	    $this->_setFilter("a.id_rab = ".$this->conn->escape($id_rab));
		$respon = $this->model->SelectGrid(
			array(
			'limit' => -1,
			'order' => $this->_order(),
			'filter' => $this->_getFilter()
			)
		);
		$rows = $respon['rows'];

		$row = 2;
        foreach($rows as $r){
	    	$col = 'A';
	    	foreach($header as $r1){
           		$excelactive->setCellValue($col.$row,$r[$r1['name']]);
           		$col++;
	    	}
            $row++;
        }


	    $objWriter = Factory::createWriter($excel,'Excel2007');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->ctrl.date('Ymd').'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
	}

	public function go_print($id_rab=0, $page=0){

		$this->_beforeDetail($id_rab);

		$this->_setFilter("a.id_rab = ".$this->conn->escape($id_rab));

		$this->data['sub_page_title'] = $this->data['rowheader']['nama_proyek'];
		$this->data['sub_page_title'] .= "<br/>".$this->data['rowheader1']['nama_pekerjaan'];
		$this->viewprint = "panelbackend/rab_scopeprint";
		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout4";

		$this->data['header']=$this->Header();

		$this->data['list']=$this->_getListPrint();

		$this->View($this->viewprint);
	}
}