<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Ws extends _adminController{

	public function __construct(){
		parent::__construct();
		$this->helper("a");
		$this->helper("s");
	}

	protected function init(){
		parent::init();
	}

	private function auth($username, $password){
		$this->load->model("AuthModel","auth");
		$this->auth->LoginWs($username,$password);
		return $this->is_auth();
	}

	private function is_auth($token=null){
		return $_SESSION[SESSION_APP]['login'];
	}

	private function ipos($data){
		return $data;
	}

	public function get_lampiran(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];
		if($this->auth($data_auth['username'], $data_auth['password'])){
			$this->data['configfile'] = $this->config->item('file_upload_config');
			$full_path = $this->data['configfile']['upload_path'].$data['file_name'];
			$data_respons = base64_encode(file_get_contents($full_path));
			$this->success($data_respons);
		}

		$this->error("Tidak boleh mengakses");
	}

	public function update_hpe(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$this->load->model("Pr_hpeModel","model");
			$this->load->model("Pr_hpe_detailModel","modeldetail");

			$id = $data['header']['id_source'];
			$this->data['row'] = $this->model->GetByPk($id);
			$this->id_rab = $this->data['id_rab'] = $this->data['row']['id_rab'];

			$this->conn->StartTrans();

			$return['success'] = true;

			if($data['detail'])
				foreach($data['detail'] as $r){
					if(!$return['success'])
						break;

					$record = array();
					$record['harga_satuan'] = $r['harga_satuan'];
					$id_hpe_detail = $r['id_source_detail'];
					$return = $this->modeldetail->Update($record, "id_hpe_detail = ".$this->conn->escape($id_hpe_detail));

					if($return['success']){
						$rw = $this->modeldetail->GetByPk($id_hpe_detail);
						if($rw['id_jasa_material'])
							$return['success'] = $this->conn->goUpdate("rab_jasa_material",array("harga_satuan"=>$record['harga_satuan']),"id_jasa_material = ".$this->conn->escape($rw['id_jasa_material']));
						elseif($rw['id_rab_detail'])
							$return['success'] = $this->conn->goUpdate("rab_rab_detail",array("nilai_satuan"=>$record['harga_satuan']),"id_rab_detail = ".$this->conn->escape($rw['id_rab_detail']));

					}
			}

			if($return['success'])
				$return['success'] = $this->_hitungTotalRabParent();

			if($return['success']){
				$record = array();
				$record['id_status_hpe'] = $data['header']['id_status_hpe'];

				if($data['header']['riwayat'] && $data['header']['id_status_hpe']==3)
					$record['riwayat'] = $data['header']['riwayat'];

				$return = $this->model->Update($record, "id_hpe=".$this->conn->escape($this->data['row']['id_hpe']));
			}

			if($return['success']){
				$this->conn->trans_commit();
				$this->success();
			}
			else{
				$this->conn->trans_rollback();
				$this->error("Data gagal di input");
			}
		}

		$this->error("Tidak boleh mengakses");
	}

	public function update_status_pr(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$this->conn->StartTrans();

			$return['success'] = true;

			foreach($data as $no_pr_ellipse=>$r){
				foreach($r as $no_item_pr=>$r1){
					if(!$return['success'])
						break;

					$return['success'] = $this->conn->goUpdate('pr_pr_detail',$r1,"no_pr_ellipse = ".$this->conn->escape($no_pr_ellipse)." and no_item_pr = ".$this->conn->escape($no_item_pr));
				}
			}

			if($return['success']){
				$this->conn->trans_commit();
				$this->success();
			}
			else{
				$this->conn->trans_rollback();
				$this->error("Data gagal di input");
			}
		}

		$this->error("Tidak boleh mengakses");
	}

	public function insert_po(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$this->load->model("Pr_poModel","po");
			$this->conn->StartTrans();

			$record = $data['header'];
			$detail = $data['detail'];

			$id_po = $this->conn->GetOne("select id_po from pr_po where no_spk = ".$this->conn->escape($record['no_spk']));

			$is_insert = true;
			if($id_po)
				$return = $this->po->Update($record, "id_po = ".$this->conn->escape($id_po));
			else{
				$is_insert = true;
				$return = $this->po->Insert($record);
				$id_po = $return['data']['id_po'];
			}

			if($return['success']){
				$this->conn->Execute("update pr_pr_detail set id_po = null where id_po = ".$this->conn->escape($id_po));

				foreach($detail as $no_pr_ellipse=>$r){
					foreach($r as $no_item_pr=>$r1){
						if(!$return['success'])
							break;

						$return['success'] = $this->conn->goUpdate('pr_pr_detail',array('id_po'=>$id_po),"no_pr_ellipse = ".$this->conn->escape($no_pr_ellipse)." and no_item_pr = ".$this->conn->escape($no_item_pr));
					}
				}
			}

			#tarik spk
			if($return['success'] && $is_insert){
				$this->load->model("Pr_po_filesModel","pofile");
				foreach($record['lampiran'] as $r){
					$upload_data = $r;
					$record = array();
					$record['client_name'] = $upload_data['client_name'];
					$record['file_name'] = $upload_data['file_name'];
					$record['file_type'] = $upload_data['file_type'];
					$record['file_size'] = $upload_data['file_size'];
					$record['jenis_file'] = 'spk';
					$record['id_po'] = $id_po;
					$this->_setLogRecord($record);
					
					$return = $this->pofile->Insert($record);

					#downloadfile
					if($return['success']){
						$configfile = $this->config->item('file_upload_config');
						$path = $configfile['upload_path'];

						$pos = array();
						$pos['data'] = $upload_data;
						$return = $this->reqscm("get_lampiran", $pos);
						$strfile = rawurldecode(base64_decode($return['data']));

						file_put_contents($path.$record['file_name'], $strfile);
					}
				}
			}

			if($return['success']){
				$this->conn->trans_commit();
				$this->success();
			}
			else{
				$this->conn->trans_rollback();
				$this->error("Data gagal di input");
			}
		}

		$this->error("Tidak boleh mengakses");
	}

	public function check_hpe(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$no_pr_ellipse = $data['no_pr_ellipse'];

			$data = array();
			$data['hpe'] = $this->conn->GetArray("select 
				pe.id_hpe_detail as id_source_detail, 
				pd.no_item_pr
				from pr_pr pr
				join pr_pr_detail pd on pr.id_pr = pd.id_pr
				join pr_hpe_detail pe on pd.id_rab_detail = pe.id_rab_detail and pd.id_jasa_material = pe.id_jasa_material
				join pr_hpe ph on pe.id_hpe = ph.id_hpe
				where ph.id_status_hpe = 4 and pr.no_pr_ellipse = ".$this->conn->escape($no_pr_ellipse));

			if($data['hpe']){
				$this->success($data);
			}
			else{
				$this->error("Tidak ditemukan");
			}
		}

		$this->error("Tidak boleh mengakses");
	}

	public function get_proyek(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$params = $data['params'];

			$where =  null;

			if($params){
				foreach($params as $k=>$v){
					$where .= " and a.".$this->conn->escape_str($k)." = ".$this->conn->escape($v);
				}
			}

			$rows = $this->conn->GetArray("
			select a.* from (
			select 
			p.id_proyek,
			p.id_customer,
			c.nama as nama_customer,
			p.nama_proyek,
			p.tgl_rencana_mulai,
			p.tgl_rencana_selesai,
			p.id_pic,
			p.nama_pic,
			p.jabatan_pic,
			p.tgl_realisasi_mulai,
			p.tgl_realisasi_selesai,
			p.id_tipe_proyek,
			tp.nama as nama_tipe_proyek,
			p.id_status_proyek,
			sp.nama as nama_status_proyek,
			p.is_pln,
			p.pic_customer,
			p.pic_pln,
			p.id_rendal_proyek,
			p.nama_rendal_proyek,
			p.jabatan_rendal_proyek,
			p.jabatan_pic_customer,
			p.jabatan_pic_pln,
			p.id_warehouse,
			w.table_desc nama_warehouse
			from rab_proyek p
			left join mt_customer c on p.id_customer = c.id_customer
			left join mt_tipe_proyek tp on p.id_tipe_proyek = tp.id_tipe_proyek
			left join mt_status_proyek sp on p.id_status_proyek = sp.id_status_proyek
			left join mt_warehouse w on p.ID_WAREHOUSE = w.table_code
			where p.is_deleted <> 1
			) a
			where tgl_rencana_mulai is not null $where
			order by tgl_rencana_mulai
			");

			foreach($rows as &$r){
				$r['jumlah_tenaga_kerja'] = $this->conn->GetOne("select sum(jumlah) from rab_manpower a where id_rab in (select id_rab from rab_rab b join rab_pekerjaan c on b.id_pekerjaan = c.id_pekerjaan where c.id_proyek = ".$this->conn->escape($r['id_proyek']).")");
			}
			$this->success($rows);
		}

		$this->error("Tidak boleh mengakses");
	}

	public function get_proyek_full(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$tahun = $data['tahun'];

			if(!$tahun)
				$tahun = date('Y');

			$rows = $this->conn->GetArray("
			select a.* from rab_proyek a
			where is_deleted <> '1' 
			and to_char(nvl(tgl_rencana_mulai,nvl(tgl_realisasi_mulai,created_date)), 'YYYY')=".$this->conn->escape($tahun)
			);
			
			$this->success($rows);
		}

		$this->error("Tidak boleh mengakses");
	}

	public function get_pekerjaan(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$params = $data['params'];

			$where =  null;

			if($params){
				foreach($params as $k=>$v){
					$where .= " and a.".$this->conn->escape_str($k)." = ".$this->conn->escape($v);
				}
			}

			$rows = $this->conn->GetArray("
			select a.* from (
			select 
			pk.id_pekerjaan,
			pk.nama_pekerjaan,
			pk.no_pekerjaan,
			pk.tgl_pekerjaan,
			pk.no_kontrak,
			pk.tgl_kontrak,
			pk.nilai_hpp,
			pk.no_prk,
			pk.tgl_mulai_pelaksanaan,
			pk.tgl_selesai_pelaksanaan,
			pk.hmin,
			pk.h,
			pk.hplus,
			pk.id_tipe_pekerjaan,
			tp.nama as nama_tipe_pekerjaan,
			pk.id_proyek,
			p.nama_proyek,
			pk.nilai_rab,
			pk.nilai_realisasi,
			pk.id_pic,
			pk.nama_pic,
			pk.jabatan_pic,
			pk.tgl_prk,
			pk.tgl_hpp,
			pk.progress,
			pk.tgl_selesai_pelaksanaan_real,
			pk.tgl_mulai_pelaksanaan_real
			from rab_pekerjaan pk
			left join mt_tipe_pekerjaan tp on pk.id_tipe_pekerjaan = tp.id_tipe_pekerjaan
			left join rab_proyek p on pk.id_proyek = p.id_proyek
			where pk.is_deleted <> '1'
			) a
			where 1=1 $where
			");

			if($rows)
				foreach($rows as &$r){
				$id_rab = $this->conn->GetOne("select max(id_rab) from rab_rab where id_pekerjaan = ".$this->conn->escape($r['id_pekerjaan']));

				$r['jumlah_pekerja'] = $this->conn->GetOne("select sum(jumlah) from rab_manpower where id_rab = ".$this->conn->escape($id_rab));
			}

			$this->success($rows);
		}

		$this->error("Tidak boleh mengakses");
	}

	public function get_pekerjaan_full(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$params = $data['params'];

			$where =  null;

			if($params){
				foreach($params as $k=>$v){
					$where .= " and a.".$this->conn->escape_str($k)." = ".$this->conn->escape($v);
				}
			}

			$rows = $this->conn->GetArray("
			select a.* from rab_pekerjaan a
			where is_deleted <> '1' $where
			");

			$this->success($rows);
		}

		$this->error("Tidak boleh mengakses");
	}

	public function get_rab(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$id_pekerjaan = $data['id_pekerjaan'];

			$id_rab = $this->conn->GetOne("select max(id_rab) from rab_rab where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

			$retarr = array();

			$retarr['rab'] = $this->conn->GetRow("select * from rab_rab where id_rab = ".$this->conn->escape($id_rab));

			$retarr['jasa_material'] = $this->conn->GetArray("select * from rab_jasa_material where id_rab = ".$this->conn->escape($id_rab));

			$retarr['manpower'] = $this->conn->GetArray("select * from rab_manpower where id_rab = ".$this->conn->escape($id_rab));

			foreach($retarr['manpower'] as &$r){
				$r['mandays'] = $this->conn->GetArray("select * from rab_mandays where id_manpower = ".$this->conn->escape($r['id_manpower']));
			}

			$rows = $this->conn->GetArray("
			select a.* from rab_rab_detail a
			where id_rab = ".$this->conn->escape($id_rab)." order by id_rab_detail_parent nulls first, id_rab_detail");

			foreach($rows as &$r){
				$r['jabatan_proyek'] = $this->conn->GetArray("select * from rab_rab_detail_jabatan_proyek where id_rab_detail = ".$this->conn->escape($r['id_rab_detail']));
				$r['sumber_pegawai'] = $this->conn->GetArray("select * from rab_rab_detail_sumber_pegawai where id_rab_detail = ".$this->conn->escape($r['id_rab_detail']));
			}

			$i=0;
			$retarr['rab_detail'] = array();
			$this->GenerateSort($rows, "id_rab_detail_parent", "id_rab_detail", $retarr['rab_detail'], null, $i);
			
			$this->success($retarr);
		}

		$this->error("Tidak boleh mengakses");
	}


	function GenerateSort(&$row, $colparent, $colid, &$return=array(), $valparent=null, &$i=0, $level=0, $is_space = false){
		$level++;
		foreach ($row as $key => $value) {
			# code...
			if(trim($value[$colparent])==trim($valparent) && ($value[$colparent] or $valparent===null)){
				unset($row[$key]);
				$return[$i]=$value;
				$i++;
				$this->GenerateSort($row, $colparent, $colid, $return, $value[$colid], $i, $level, $is_space);
			}
		}

		if($row && $level==1)
			$return = array_merge($return, $row);
	}

	public function get_pekerjaan_detail(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$params = $data['params'];

			$where =  null;

			if($params){
				foreach($params as $k=>$v){
					$where .= " and a.".$this->conn->escape_str($k)." = ".$this->conn->escape($v);
				}
			}

			$rows = $this->conn->GetArray("
			select a.* from (
			select 
			p.id_pekerjaan,
			pk.nama_pekerjaan,
			pd.urutan,
			pd.nama,
			pd.mulai,
			pd.selesai,
			pd.rencana,
			pd.realisasi,
			pd.urutan_parent,
			pd.is_leaf,
			pd.progress,
			pd.keterangan,
			pd.last_update_realisasi,
			pd.id_plan_detail,
			pd.is_print,
			pd.durasi,
			pd.bobot_custom
			from wbs_plan p
			join wbs_plan_detail pd on p.id_plan = pd.id_plan
			join rab_pekerjaan pk on p.id_pekerjaan = pk.id_pekerjaan
			) a
			where 1=1 $where
			");
			
			$this->success($rows);
		}

		$this->error("Tidak boleh mengakses");
	}

	public function get_kpi(){
		$data_auth = $this->post['data_auth'];
		$data = $this->post['data'];

		if($this->auth($data_auth['username'], $data_auth['password'])){
			$id_proyek = $data['id_proyek'];

			$cek = $this->conn->GetOne("select 1 from rab_proyek where id_status_proyek = 1 and id_proyek = ".$this->conn->escape($id_proyek));

			$ret = array();

			if($cek){
				$sql = "select 
					count(1) jumlah, 
					sum(nvl(a.nilai_rab,0)) nilai_rab, 
					sum(nvl(a.nilai_realisasi,0)) nilai_realisasi,
					sum(to_date(to_char(a.tgl_selesai_pelaksanaan,'yyyymmdd'),'yyyymmdd') - to_date(to_char(a.tgl_mulai_pelaksanaan,'yyyymmdd'),'yyyymmdd')) as rencana,
					sum(to_date(to_char(a.tgl_selesai_pelaksanaan_real,'yyyymmdd'),'yyyymmdd') - to_date(to_char(a.tgl_mulai_pelaksanaan_real,'yyyymmdd'),'yyyymmdd')) as realisasi
					from rab_pekerjaan a
					where is_deleted <> '1' and a.id_proyek = ".$this->conn->escape($id_proyek);

				$row = $this->conn->GetRow($sql);

				if($row['rencana'] && $row['realisasi'])
					$ret['on_time'] = (2-($row['realisasi']/$row['rencana']))*100;

				if($row['nilai_rab'] && $row['nilai_realisasi'])
					$ret['on_cost'] = (2-($row['nilai_realisasi']/$row['nilai_rab']))*100;

				$ret['on_spec'] = (($row['jumlah']>1)?0:100);
			}
			
			$this->success($ret);
		}

		$this->error("Tidak boleh mengakses");
	}

	private function success($data=array()){
		$data = array("success"=>true, "data"=>$data);
		echo json_encode($data);
		exit();
	}

	private function error($msg=null, $data=array()){
		$data = array("success"=>false, "message"=>$msg, "data"=>$data);
		echo json_encode($data);
		exit();
	}
}