<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_task extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_tasklist";
		$this->viewdetail = "panelbackend/mt_team_proyekdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->data['width'] = "1200px";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Task';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Task';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Task';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Task';
		}

		$this->load->model("Rab_rabModel","model");
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			''
		);
	}

	protected function _getList($page=0){
		$this->_resetList();

		$this->arrNoquote = $this->model->arrNoquote;

		$param=array(
			'page' => $page,
			'limit' => $this->_limit(),
			'order' => $this->_order(),
			'filter' => $this->_getFilter()
		);

		if($this->post['act']){
			
			if($this->data['add_param']){
				$add_param = '/'.$this->data['add_param'];
			}
			redirect(str_replace(strstr(current_url(),"/index$add_param/$page"), "/index{$add_param}", current_url()));
		}

		$respon = $this->model->SelectGridTask(
			$param
		);

		return $respon;
	}

	protected function Header(){
		return array(
			array(
				'name'=>'nama_proyek', 
				'label'=>'Nama Proyek', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nama_pekerjaan', 
				'label'=>'Nama Pekerjaan', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nama_status', 
				'label'=>'Nama Status', 
				'width'=>"100px",
				'type'=>"varchar2",
			),
		);
	}

	public function Detail($id=null){
		redirect("panelbackend/rab_rab_detail/index/".$id);
	}
}