<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Wbs extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/wbs_planlist";
		$this->viewdetail = "panelbackend/wbs_plandetail";
		$this->viewcetak = "panelbackend/wbs_cetak";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		if ($this->mode == 'add') {
			// $this->data['width'] = "1200px";
			$this->data['page_title'] = 'Tambah Rencana';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			// $this->data['width'] = "1200px";
			$this->data['page_title'] = 'Edit Rencana';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			// $this->data['width'] = "1200px";
			$this->data['page_title'] = 'Detail Rencana';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Rencana';
		}

		unset($this->access_role['lst']);
		unset($this->access_role['index']);
		unset($this->access_role['list']);


		// $this->data['no_header'] = true;

		$this->load->model("Wbs_planModel","model");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'datepicker','select2','upload'
		);
	}

	function Index($id_pekerjaan=0, $page=0){
		$this->_beforeDetail($id_pekerjaan);

		$this->data['header']=$this->Header();

		//$this->_setFilter("id_plan = ".$this->conn->escape($id_plan));
		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_pekerjaan"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;

		$this->View($this->viewlist);
	}

	function readMPP($file) {
		require_once($this->config->item("url_java"));

		if(!file_exists($file))
			return false;

		$dest = FCPATH."uploads/".time().".mpp";
		move_uploaded_file ($file , $dest);
		chmod($dest, 0777);

		$project = new java('javaapplication1.JavaApplication1');
		$ret = java_values($project->importMPP($dest));

		$arr = json_decode($ret, true);
		ksort($arr);
		unlink($dest);
/*
		dpr($project);
		// dpr($ret);
		dpr($arr,1);*/

		return $arr;

	}


	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama Rencana', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			// array(
			// 	'name'=>'no_prk', 
			// 	'label'=>'No. PRK', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'id_tipe_pekerjaan', 
			// 	'label'=>'Tipe Pekerjaan', 
			// 	'width'=>"auto",
			// 	'type'=>"list",
			// 	'value'=>$this->data['mttipepekerjaanarr'],
			// ),
			// array(
			// 	'name'=>'no_pekerjaan', 
			// 	'label'=>'No. SP3', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'tgl_pekerjaan', 
			// 	'label'=>'Tgl. SP3', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'no_kontrak', 
			// 	'label'=>'No. Kontrak', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'tgl_kontrak', 
			// 	'label'=>'Tgl. Kontrak', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'nilai_hpp', 
			// 	'label'=>'Nilai HPP', 
			// 	'width'=>"auto",
			// 	'type'=>"number",
			// ),
			// array(
			// 	'name'=>'tgl_mulai_rab', 
			// 	'label'=>'Pengerjaan RAB', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'durasi', 
			// 	'label'=>'Durasi', 
			// 	'width'=>"auto",
			// 	'type'=>"polos",
			// ),
		);
	}

	protected function Record($id=null){
		$return = array(
			'nama'=>$this->post['nama'],
		);

		// $pic = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_pic'])));

		// $return['nama_pic'] = $pic['nama'];
		// $return['jabatan_pic'] = $pic['jabatan'];

		return $return;
	}

	protected function Rules(){
		return array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama Rencana', 
				'rules'=>"required",
			),
		);
	}

	public function Add($id_pekerjaan=0){
		$this->Edit($id_pekerjaan);
	}

	public function Edit($id_pekerjaan=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_pekerjaan,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_pekerjaan'] = $id_pekerjaan;

			$this->_isValid($record,true);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

			if ($return['success']) {
				$this->conn->trans_commit();

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);

				redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");

			} else {
				$this->conn->trans_rollback();

				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
				$this->data['err_msg'] .= $this->data['err_msg1'];
			}
		}

		$this->View($this->viewdetail);
	}

	protected function _afterEditSucceed($id = null){
		$this->_updateProgressPekerjaan($id);
	}

	public function Delete($id_pekerjaan=null, $id=null){
        $this->model->conn->StartTrans();

        $this->_beforeDetail($id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_pekerjaan");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");
		}

	}

	protected function _beforeDetail($id_pekerjaan=null, &$id=null, $tgl=null){
		$id = $this->conn->GetOne("select max(id_plan) from wbs_plan where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$this->data['row'] = $this->model->GetByPk($id);

		$this->data['id_pekerjaan'] = $id_pekerjaan;

		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan, $tgl);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_pekerjaan;

		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['id_rab'] = $id_rab = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);

		if (!$this->data['rowheader2']) $this->NoData('Tidak ada master RAB');

		# TODO: sementara
	}

	protected function _afterInsert($id=null){
		return $this->_afterUpdate($id);
	}

	function parentid($arr){
		$ret = array();
		if($arr)
		foreach($arr as $r){
			$ret[$r['urutan']] = $r['urutan_parent'];
		}

		return $ret;
	}

	function parentlabel($arr){
		$ret = array();
		if($arr)
		foreach($arr as $r){
			$ret[$r['urutan']] = strtolower(trim($r['nama']));
		}

		return $ret;
	}

	function parentTingkat($arrlabel, $arrid, $id){
		$ret = $arrlabel[$id];

		if($arrid[$id])
			$ret .= $this->parentTingkat($arrlabel, $arrid, $arrid[$id]);

		return $ret;
	}

	function parentTingkatLabel($arr){
		$arrid = $this->parentid($arr);
		$arrlabel = $this->parentlabel($arr);

		$ret = array();
		if($arr)
		foreach($arr as $r){
			$label = $this->parentTingkat($arrlabel, $arrid, $r['urutan']);
			$ret[$label] = $r['id_plan_detail'];
		}

		return $ret;
	}

	protected function _afterUpdate($id){
		$ret = true;

		if($this->post['working_time']){
			$ret = $this->conn->Execute("delete from wbs_working_time where id_plan = ".$this->conn->escape($id));

			foreach($this->post['working_time'] as $k=>$v){
				if(!$ret)
					break;

				$record = $v;
				$record['id_plan'] = $id;

				$ret = $this->conn->goInsert("wbs_working_time",$record);
			}
		}

		if($ret)
			$ret = $this->_execMpp($id);
		
		return $ret;
	}

	private function _execMpp($id=null){
		$ret = true;
		if ($_FILES) {
			$id = (int) $id;
			$file = $_FILES['file1']['tmp_name'];
			// $file = "/media/solikul/Data/a.mpp";
			$details = $this->readMPP($file);

			if($details){

				$this->getPlan($id);
				$rows = $this->plan;
				$arrlabelid = $this->parentTingkatLabel($rows);

				$idarr = $this->plan_id;

				$temparr = array();
				foreach($details as $key => $value) {

					if(!$key)
						$value[0]['name'][0] = $value[0]['name'][0].' (root)';

					$temparr[] = array(
						'urutan' => (int)$key,
						'urutan_parent' => (int)$value[0]['parent'][0],
						'nama' => $value[0]['name'][0]
					);
				}
				$arrid = $this->parentid($temparr);
				$arrlabel = $this->parentlabel($temparr);

				foreach ($details as $key => $value) {
					if(!$ret)
						break;

					$id_detail = (int) $key;
					$urutan_parent = (int) $value[0]['parent'][0];
					$nama = $value[0]['name'][0];
					$start = date('d-m-Y H:i:s', strtotime1($value[0]['start'][0]));
					$finish = date('d-m-Y H:i:s', strtotime1($value[0]['finish'][0]));
					$is_leaf = $value[0]['isleaf'][0];
					$record = array(
						'id_plan'=> $id,
						'urutan'=>$id_detail,
						'nama'=>$nama,
						'mulai'=>$start,
						'selesai'=>$finish,
						'rencana'=>"{{null}}",
						'durasi'=>"{{null}}",
						'urutan_parent'=>$urutan_parent,
						'is_leaf'=>$is_leaf,
					);

					$label = $this->parentTingkat($arrlabel, $arrid, $id_detail);

					if($templabelarr[$label]){
						$this->data['err_msg1'] .= "<br/>".strtoupper($arrlabel[$id_detail])." doubel, penamaan dalam tingkat dan induk yang sama harus unik.";

						// return false;
					}

					if($id_detail==0)
						$id_plan_detail = $this->conn->GetOne("select id_plan_detail from wbs_plan_detail where id_plan = $id and urutan = 0");
					else
						$id_plan_detail = $arrlabelid[$label];

					$this->_setLogRecord($record,$id_plan_detail);

					if($id_plan_detail)
			        	$sql = $this->conn->UpdateSQL('wbs_plan_detail', $record, "id_plan_detail=$id_plan_detail");
			        else{
			        	$sql = $this->conn->InsertSQL('wbs_plan_detail', $record);
			        }

			        if($sql){
					    $ret = $this->conn->Execute($sql);
					    if(!$id_plan_detail){
			        		$id_plan_detail = $this->conn->GetOne("select id_plan_detail
			        		from wbs_plan_detail 
			        		where id_plan = $id and urutan = '$id_detail' and trim(nama) = ".$this->conn->escape($nama));
					    }
					    $templabelarr[$label] = $id_plan_detail;
					    unset($idarr[$id_plan_detail]);
					}else{
						$ret = false;
					}
				}

				if($this->data['err_msg1'])
					return false;

				if($idarr){
					foreach($idarr as $idp=>$i){
						if(!$ret)
							break;

						$ret = $this->conn->Execute("delete from wbs_plan_detail where id_plan = $id and id_plan_detail = ".$this->conn->escape($idp));
					}
				}

				unset($temparr);
				unset($idarr);
				unset($arrid);
				unset($details);
				unset($arrlabel);
				unset($arrlabelid);
				unset($rows);
				unset($templabelarr);

				$this->_hitungDetail($id);
				$this->_hitungRencanaParent($id);
				$this->_hitungRealisasiParentAll($id, true);
			}else{
				$ret = false;
			}
		}

		return $ret;
	}
/*
	private function _delsertFiles($id_pekerjaan = null){
		$ret = true;

		if(!empty($this->post['sp3'])){
			foreach($this->post['sp3']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}

		if(!empty($this->post['kontrak'])){
			foreach($this->post['kontrak']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}

		if(!empty($this->post['file'])){
			foreach($this->post['file']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}
		return $ret;
	}

	private function _delsertTTD($id_pekerjaan = null){
		$ret = $this->conn->Execute("delete from rab_pekerjaan_ttd where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$MainSpecarr = array();

		if(!empty($this->post['ttd'])){
			foreach ($this->post['ttd'] as $key => $v) {
				if(!$v['nid'])
					continue;

				if(!$ret)
					break;

				$record = array();
				$record['id_pekerjaan'] = $id_pekerjaan;
				$record['nid'] = $v['nid'];
				$row_pegawai = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape($v['nid']));
				$record['nama'] = $row_pegawai['nama'];
				$record['jabatan'] = $row_pegawai['jabatan'];

				$ret = $this->conn->goInsert('rab_pekerjaan_ttd', $record);
			}
		}

		return $ret;
	}
*/	


	private function getTotalDetik($id, $urutan=0){
		if($this->total_detik1[$urutan])
			return $this->total_detik1[$urutan];

		$this->getPlan($id);
		$total_detik = 0;
		$rs = $this->plan_child[$urutan];

		if($rs)
			foreach($rs as $r){
				if($r['is_leaf']){
					$total_detik += hitung_detik($id, $r['mulai'], $r['selesai']);
				}else{
					$total_detik += $this->getTotalDetik($id, $r['urutan']);
				}
		}else{
			$r = $this->plan[$urutan];
			if($r['is_leaf'])
				$total_detik = hitung_detik($id, $r['mulai'], $r['selesai']);
		}

		$this->total_detik1[$urutan] = 0;

		unset($rs);

		return $total_detik;
	}

	private function getUrutanBobotCustom($id, $urutan=0){
		$u = null;

		if($this->bobot_parent[$urutan])
			return $this->bobot_parent[$urutan];

		$this->getPlan($id);
		$rows = $this->plan;
		$r = $rows[$urutan];

		if($r){
			if($r['bobot_custom'])
				$u = $r['urutan'];
			else
				$u = $this->getUrutanBobotCustom($id, $r['urutan_parent']);
		}
		unset($rows);

		$this->bobot_parent[$urutan] = $u;

		return $u;
	}

	private function getTotalBobotCustom($id, $urutan=0, $urutan_self=null){
		if($urutan==$urutan_self)
			return 0;

		if($this->total_bobot_custom[$urutan])
			return $this->total_bobot_custom[$urutan];

		$this->getPlan($id);
		$rows = $this->plan_child_leaf[0][$urutan];

		$bobot=0;

		if($rows)
			foreach($rows as $r){
			if($r['bobot_custom'])
				$bobot += $r['bobot_custom'];
			else{
				$bobot += $this->getTotalBobotCustom($id, $r['urutan']);
			}
		}
		unset($rows);

		$this->total_bobot_custom[$urutan] = $bobot;

		return $bobot;
	}

	private function getTotalBobot($id, $urutan=0, $label='rencana'){

		if($this->total_bobot[$label][$urutan])
			return $this->total_bobot[$label][$urutan];

		$this->getPlan($id);
		$rows = $this->plan_child[$urutan];

		$bobot=0;

		if($rows)
			foreach($rows as $r){
			if($r['is_leaf'])
				$bobot += $r[$label];
			else{
				$bobot += $this->getTotalBobot($id, $r['urutan'], $label);
			}
		}else
			$bobot = $this->plan[$urutan][$label];

		unset($rows);

		$this->total_bobot[$label][$urutan] = $bobot;

		return $bobot;
	}

	private function getTotalDetikBobotCustom($id, $urutan=0, $urutan_self=null){
		if($urutan==$urutan_self)
			return 0;

		if($this->total_detik_custom[$urutan])
			return $this->total_detik_custom[$urutan];

		$this->getPlan($id);
		$rows = $this->plan_child_leaf[0][$urutan];

		$detik=0;

		if($rows)
			foreach($rows as $r){
			if($r['bobot_custom'])
				$detik += $this->getTotalDetik($id, $r['urutan']);
			else
				$detik += $this->getTotalDetikBobotCustom($id, $r['urutan']);
		}

		unset($rows);

		$this->total_detik_custom[$urutan] = $detik;

		return $detik;
	}

	private function getRencana($id, $urutan){
		$this->getPlan($id);
		$r = $this->plan[$urutan];
		$detik = $this->getTotalDetik($id, $urutan);
		$urutan_custom = $this->getUrutanBobotCustom($id, $urutan);

		if($urutan_custom){
			$total_detik = $this->getTotalDetik($id, $urutan_custom)-$this->getTotalDetikBobotCustom($id, $urutan_custom);
			$total_persen = $this->plan[$urutan_custom]['bobot_custom']-$this->getTotalBobotCustom($id, $urutan_custom);
		}else{
			$total_detik = ($this->getTotalDetik($id)-$this->getTotalDetikBobotCustom($id, 0, $urutan));
			$total_persen = (100-$this->getTotalBobotCustom($id, 0, $urutan));

		}

		if(!$total_persen or !$total_detik)
			$rencana = 0;
		else
			$rencana = $detik/$total_detik*$total_persen;

		return $rencana;
	}

	function _hitungDetail($id) {
		$this->conn->Execute("update wbs_plan_detail set rencana = null where id_plan=$id");
		$this->getPlan($id, true);
		$rows = $this->plan_leaf[1];		
		$rows1 = $this->plan;		

		foreach($rows as $r){
			$detik = hitung_detik($id, $r['mulai'], $r['selesai']);

			$bobot_rencana = $this->getRencana($id, $r['urutan']);
			$bobot_realisasi = $bobot_rencana*(float)$r['progress']/100;

			$sql = "update wbs_plan_detail set durasi = $detik, rencana=$bobot_rencana, realisasi = $bobot_realisasi where id_plan=$id and urutan=$r[urutan]";
			$ret = $this->conn->Execute($sql);
		}

		$cek = $this->conn->GetOne("select 1 from wbs_plan_detail where id_plan = $id and is_print = '1' and urutan <> '0'");

		foreach($rows1 as $row){
			if(!$cek && $row['tingkat']<=2){
				$sql = "update wbs_plan_detail set is_print = '1' where id_plan=$id and urutan=$row[urutan]";
				$ret = $this->conn->Execute($sql);
			}
		}

		if($ret){
			$rows = $this->conn->GetArray(" SELECT
		            min(id_plan_detail) as id
		        FROM
		            wbs_plan_detail
		        WHERE
		            id_plan = $id
		    group by urutan having count(urutan)>1
		    order by urutan desc");

		    foreach($rows as $r){
		    	if(!$ret)
		    		break;

		    	$ret = $this->conn->Execute("delete from wbs_plan_detail where id_plan_detail = ".$this->conn->escape($r['id']));
		    }
		}

		unset($this->total_detik_custom);
		unset($this->total_bobot_custom);
		unset($this->bobot_parent);
		unset($this->total_detik1);
		unset($this->strtotime);
		unset($this->detikarr);
		unset($this->total_detik);
		$this->getPlan($id, true);

		return $ret;
	}

	function _hitungRencanaParent($id_plan) {
		$this->getPlan($id_plan);

		$rows = $this->plan_leaf[0];

		foreach ($rows as $r) {
			$rencana = $this->getTotalBobot($id_plan, $r['urutan'], 'rencana');

			$detik = hitung_detik($id_plan, $r['mulai'], $r['selesai']);

			$sql = "update wbs_plan_detail set durasi = $detik, rencana=$rencana where id_plan=$id_plan and urutan=$r[urutan] and is_leaf=0";
			$this->conn->Execute($sql);
		}

		unset($rows);
	}

	function _hitungRealisasiParentAll($id_plan, $is_upload=false) {
		$this->getPlan($id_plan);

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		$rows = $this->plan_leaf[0];

		if(!$is_upload)
			$update_realisasi = ", last_update_realisasi=sysdate, modified_by_desc = '$user_name', modified_by = $user_id";

		foreach ($rows as $r) {
			$rencana = $this->getTotalBobot($id_plan, $r['urutan'], 'rencana');
			$realisasi = $this->getTotalBobot($id_plan, $r['urutan'], 'realisasi');

			$detik = hitung_detik($id_plan, $r['mulai'], $r['selesai']);

			$sql = "update wbs_plan_detail set realisasi=$realisasi, progress=case when rencana is null or rencana = 0 then 0 else ($realisasi/rencana*100) end $update_realisasi where id_plan=$id_plan and urutan=$r[urutan] and is_leaf=0";
			$this->conn->Execute($sql);
		}

		unset($rows);
	}

	function _hitungRealisasiParent($id_plan, $urutan_parent=0) {
		$this->getPlan($id_plan);

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		$r = $this->plan[$urutan_parent];
		if(!$r)
			return;

		$rencana = $this->getTotalBobot($id_plan, $r['urutan'], 'rencana');
		$realisasi = $this->getTotalBobot($id_plan, $r['urutan'], 'realisasi');

		$detik = hitung_detik($id_plan, $r['mulai'], $r['selesai']);

		$sql = "update wbs_plan_detail set 
			realisasi=$realisasi, 
			progress=($realisasi/rencana*100), 
			last_update_realisasi=(select max(last_update_realisasi) from wbs_plan_detail where id_plan=$id_plan and urutan_parent=$r[urutan] ), 
			first_update_realisasi=(select min(first_update_realisasi) from wbs_plan_detail where id_plan=$id_plan and urutan_parent=$r[urutan] ), 
			modified_by_desc = '$user_name',
			last_update_realisasi_real=sysdate,
			modified_by = $user_id 
			where id_plan=$id_plan and urutan=$r[urutan] 
			and is_leaf=0";
		$this->conn->Execute($sql);

		$this->_hitungRealisasiParent($id_plan, $r['urutan_parent']);
	}

	function setdatetimemanual() {
		$id = (int) $_POST['id'];
		$urutan = (int) $_POST['id_detail'];

		if (!strtotime1($_POST['val']) && !strtotime1($_POST['mulai'])) die('-1');

		$datetimemulai =  date('d-m-Y H:i:s', strtotime1($_POST['mulai']));
		$datetime =  date('d-m-Y H:i:s', strtotime1($_POST['val']));

		// die($datetime);

		$record = array(
			'last_update_realisasi_real'=>"{{sysdate}}",
		);

		if($_POST['mulai'])
			$record['first_update_realisasi'] = $datetimemulai;

		if($_POST['val'])
			$record['last_update_realisasi'] = $datetime;


        $sql = $this->conn->UpdateSQL('wbs_plan_detail', $record, "id_plan=$id and urutan=$urutan");
        if($sql){
		    $ret = $this->conn->Execute($sql);
		}

		if ($ret) {
			$sql = "select urutan_parent, to_char(last_update_realisasi, 'dd-mm-yyyy hh24:mi') as last_update_realisasi, to_char(first_update_realisasi, 'dd-mm-yyyy hh24:mi') as first_update_realisasi from wbs_plan_detail where id_plan=$id and urutan=$urutan";
			$row = $this->conn->GetRow($sql);
			$update_terakhir = Eng2Ind($row['last_update_realisasi']);
			$update_terawal = Eng2Ind($row['first_update_realisasi']);
			$urutan_parent = $row['urutan_parent'];

			$this->_hitungRealisasiParent($id, $urutan_parent);

			$ret = array('update_terakhir'=>$update_terakhir,'update_terawal'=>$update_terawal);
			echo json_encode($ret);
			$this->_updateProgressPekerjaan($id);

		}
		else {
			echo '-1';
		}
		die();
	}

	function updaterealisasi() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);
		$val = (int) $_POST['val'];
		list($tgl, $bln, $thn) = explode("-",$this->post['tgl']);
		$tgl = $thn.$bln.$tgl;

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		if ($val > 100) $val = 100;

		$row = $this->conn->GetRow("select * from wbs_plan_detail where id_plan = $id and urutan = $urutan");

		$id_plan_detail = $row['id_plan_detail'];
		$rencana_target = $row['rencana'];

		$cek = $this->conn->GetOne("select 1 from wbs_plan_detail_realisasi where id_plan_detail = $id_plan_detail and tgl = ".$this->conn->escape($tgl));

		$rec = array();
		$rec['tgl'] = $tgl;
		$rec['id_plan_detail'] = $id_plan_detail;
		$rec['progress'] = $val;

		$this->conn->goUpdate("wbs_plan_detail", 
		array("last_update_realisasi"=>$this->post['tgl']." ".date("H-i-s")), 
		"id_plan_detail = $id_plan_detail");

		if($cek){
			$progressb = $this->conn->GetOne("select max(progress) from wbs_plan_detail_realisasi where id_plan_detail = $id_plan_detail and tgl < ".$this->conn->escape($tgl));
			if($val<$progressb)
				$val = $progressb;

			$progressb = $this->conn->GetOne("select min(progress) from wbs_plan_detail_realisasi where id_plan_detail = $id_plan_detail and tgl > ".$this->conn->escape($tgl));

			if($val>$progressb && $progressb)
				$val = $progressb;

			$rec['progress'] = $val;

			$ret = $this->conn->goUpdate("wbs_plan_detail_realisasi", $rec, "id_plan_detail = $id_plan_detail and tgl = ".$this->conn->escape($tgl));
		}
		else
			$ret = $this->conn->goInsert("wbs_plan_detail_realisasi", $rec);

		if ($ret) {
			$realisasi = round($rencana_target*$val/100,2);

			/*$urutan_parent = $row['urutan_parent'];
			$this->_hitungRealisasiParent($id, $urutan_parent);*/

			$progress_pr = setProgressBar($rec['progress']);

			$ret = array('progress'=>$progress, 'progress_pr'=>$progress_pr, 'realisasi'=>$realisasi);
			echo json_encode($ret);

			$this->_updateProgressPekerjaan($id);

		}
		else {
			echo '-1';
		}
		die();
	}

	function updateketerangan() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('k', '', $_POST['id_detail']);
		$ket = $_POST['ket'];
		list($tgl, $bln, $thn) = explode("-",$this->post['tgl']);
		$tgl = $thn.$bln.$tgl;

		$row = $this->conn->GetRow("select id_plan_detail from wbs_plan_detail where id_plan = $id and urutan = $urutan");

		$id_plan_detail = $row['id_plan_detail'];

		$cek = $this->conn->GetOne("select 1 from wbs_plan_detail_realisasi where id_plan_detail = $id_plan_detail and tgl = ".$this->conn->escape($tgl));

		$rec = array();
		$rec['tgl'] = $tgl;
		$rec['id_plan_detail'] = $id_plan_detail;
		$rec['keterangan'] = $ket;

		if($cek)
			$ret = $this->conn->goUpdate("wbs_plan_detail_realisasi", $rec, "id_plan_detail = $id_plan_detail and tgl = ".$this->conn->escape($tgl));
		else
			$ret = $this->conn->goInsert("wbs_plan_detail_realisasi", $rec);

		if ($ret) {
			echo '1';
		}
		else {
			echo '-1';
		}
		die();
	}

	function updatebobot() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);
		$val = (float) $_POST['val'];

		$bobot_parent = $this->conn->GetOne("select a.rencana from wbs_plan_detail a 
			join wbs_plan_detail b on a.urutan = b.urutan_parent where b.id_plan = $id and a.id_plan = $id and b.urutan = $urutan");

		$bobot_child = $this->conn->GetOne("select sum(bobot_custom) from wbs_plan_detail where id_plan = $id and urutan_parent = $urutan");

		$bobot_total = $this->getTotalBobotCustom($id);

		if($val > $bobot_parent && $bobot_parent)
			$val = $bobot_parent;

		if($val < $bobot_child)
			$val = $bobot_child;

		$bobot_sisa = (100-(float)$bobot_total);

		if($val > $bobot_sisa)
			$val = $bobot_sisa;

		if ($val > 100) 
			$val = 100;

		if($val<0)
			$val = 0;

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		$sql = "update wbs_plan_detail set rencana = $val, bobot_custom = $val, modified_by = '$user_id', modified_by_desc = '$user_name' where id_plan=$id and urutan=$urutan";

		$ret = $this->conn->query($sql);
		if ($ret) {

			/*$this->_hitungDetail($id);
			$this->_hitungRencanaParent($id);
			$this->_hitungRealisasiParentAll($id);*/

			$ret = array('bobot'=>round($val,2));
			echo json_encode($ret);

		}
		else {
			echo '-1';
		}

		// TODO log
		die();
	}

	private function updatecheckparent($is_print, $id, $urutan){
		$urutan_parent = $this->conn->GetOne("select urutan_parent from wbs_plan_detail where id_plan = $id and urutan = $urutan");

		$ret = $this->conn->Execute("update wbs_plan_detail set is_print = $is_print where id_plan = $id and urutan = $urutan");

		if($ret && $urutan_parent && $is_print)
			$ret = $this->updatecheckparent($is_print, $id, $urutan_parent);

		return $ret;
	}

	private function updatecheckchild($id, $urutan_parent){
		$rows = $this->conn->GetArray("select urutan from wbs_plan_detail where id_plan = $id and urutan_parent = $urutan_parent");

		$ret = true;

		if($rows)
		foreach($rows as $r){
			if($ret)
				$ret = $this->conn->Execute("update wbs_plan_detail set is_print = 0 where id_plan = $id and urutan = $r[urutan]");

			if($ret)
				$ret = $this->updatecheckchild($id, $r['urutan']);
		}

		return $ret;
	}

	public function updatecheckbox() {

		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);
		$is_print = ($_POST['is_print']=='true'?1:0);

		$user_id = (int)$_SESSION[SESSION_APP]['user_id'];
		$user_name = $_SESSION[SESSION_APP]['name'];

		$ret = true;

		if(!$is_print)
			$ret = $this->updatecheckchild($id, $urutan);

		if($ret)
			$ret = $this->updatecheckparent($is_print, $id, $urutan);

		if ($ret) {
			$ret = $this->conn->GetList("select urutan val from wbs_plan_detail where id_plan = $id and is_print = '1'");

			echo json_encode($ret);
		}
		else {
			echo '-1';
		}

		// TODO log
		die();
	}

	protected function _afterDetail($id){
		$id = (int) $id;

		list($tgl, $bln, $thn) = explode("-",explode(" ",$this->data['rowheader1']['sysdate1'])[0]);
		$this->data['tglfilter'] = $thn.$bln.$tgl;
		$this->data['details'] = $this->getDetails($id);

		$this->data['row']['working_time'] = $this->conn->GetArray("select * from wbs_working_time where id_plan = ".$this->conn->escape($id));

		if(!$this->data['row']['working_time']){
			$id_plan_max = $this->conn->GetOne("select max(id_plan) from wbs_working_time");
			$this->data['row']['working_time'] = $this->conn->GetArray("select * from wbs_working_time where id_plan = ".$this->conn->escape($id_plan_max));
		}

		list_detik($id);

		if($this->post['act']=='hitung_ulang'){
			$this->_hitungDetail($id);
			$this->_hitungRencanaParent($id);
			$this->_hitungRealisasiParentAll($id, true);
			redirect(current_url());
			exit();
		}
	}

	private function getPlan($id=null, $is_reset=false){
		if($is_reset or !$this->plan){
			$rows = $this->conn->GetArray("select * from wbs_plan_detail where urutan<>0 and id_plan = $id order by urutan");

			foreach($rows as &$r){
				if (!$r['urutan_parent']) {
					$r['tingkat'] = $parents[$r['urutan']] = 0;
				}
				else {
					$r['tingkat'] = $parents[$r['urutan']] = $parents[$r['urutan_parent']] + 1;
				}

				$r['rencana'] = (float)$r['rencana'];
				$r['realisasi'] = (float)$r['realisasi'];

				$this->plan[$r['urutan']] = $r;
				$this->plan_id[$r['id_plan_detail']] = 1;
				$this->plan_child[$r['urutan_parent']][$r['urutan']] = $r;
				$this->plan_child_leaf[$r['is_leaf']][$r['urutan_parent']][$r['urutan']] = $r;
				$this->plan_leaf[$r['is_leaf']][$r['urutan']] = $r;
			}
		}

	}

	function cetak($id_pekerjaan=null, $id=null, $tgl=null) {
		$this->kurva($id_pekerjaan, $id, $tgl, true);
		$this->_afterDetail($id);

		$i=0;
		foreach ($this->data['details'] as $detail) {
			if (!$detail['is_print'] or $detail['tingkat']==0) continue;
			$i++;
		}

		$this->data['height_kurva'] = $i*21;
		$this->data['num_item_show'] = $i;

		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout3";
		$this->data['width'] = "900px";
		$this->data['no_header'] = true;
		$this->data['excel'] = true;

		$this->View($this->viewcetak);;
	}

	function kurva($id_pekerjaan, $id, $tgl=null, $inline=false, $no_layout=false) {
		$this->_beforeDetail($id_pekerjaan, $id, $tgl);

		$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai,
		to_char(min(nvl(first_update_realisasi,last_update_realisasi)), 'yyyy-mm-dd') as realisasi_mulai, to_char(max(last_update_realisasi), 'yyyy-mm-dd') as realisasi_akhir
		from wbs_plan_detail where id_plan=$id and urutan > 0";
		$times = $this->conn->GetRow($sql);

		$mulai = strtotime1($times['mulai']);
		$realisasi_mulai = strtotime1($times['realisasi_mulai']);
		$selesai = strtotime1($times['selesai']);
		$realisasi_akhir = strtotime1($times['realisasi_akhir']);

		if($mulai>$realisasi_mulai && $times['realisasi_mulai'])
			$mulai = $realisasi_mulai;
		
		if($selesai<$realisasi_akhir && $times['realisasi_akhir'])
			$selesai = $realisasi_akhir;

		if(!$mulai)
			$mulai = strtotime1($this->data['rowheader1']['sysdate1']);

		if(!$mulai)
			$selesai = strtotime1($this->data['rowheader1']['sysdate1']);

		$sysdate = strtotime1($this->data['rowheader1']['sysdate1']);

		$total_hari = (($selesai-$mulai)/(3600*24));

		$plan_per_day = array();
		$realisasi_per_day = array();

		$tgl_plans = array();
		$total_hari++;
		$mulai_int = $mulai-(3600*24); 
		for ($i=0;$i<=$total_hari;$i++) {
			$tanggal = strtotime("+$i day", $mulai_int);
			$tgl_plans[date('Y-m-d', $tanggal)] = array();
		}

		list($tg, $jm) = explode(" ", $this->data['rowheader1']['sysdate1']);

		$is_realisasi = true;
		foreach ($tgl_plans as $key => $value) {
			$key1 = $key.' '.$jm;
			$sql = "select sum(rencana) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and selesai <= to_date('$key1', 'yyyy-mm-dd hh24:mi:ss') and is_leaf='1')";
			$rencana = (float)$this->conn->GetOne($sql);

			$rs = $this->conn->GetArray("select id_plan, mulai, rencana, durasi, selesai
				from wbs_plan_detail a
				where a.selesai > to_date('$key1', 'yyyy-mm-dd hh24:mi:ss')
				and a.mulai <= to_date('$key1', 'yyyy-mm-dd hh24:mi:ss')
				and is_leaf = 1 and id_plan = $id");

			foreach($rs as $r){
				$detik0 = (int)$r['durasi'];
				
				if(!$detik0)
					$detik0 = hitung_detik($r['id_plan'], $r['mulai'], $r['selesai']);

				$detik1 = hitung_detik($id, $r['mulai'], $key1);

				if($r['rencana'])
					$run = ($detik1/$detik0*$r['rencana']);
				else
					$run = 0;

				$rencana += $run;
			}

			$plan_per_day[$key] = $rencana;
		}
		ksort($plan_per_day);

		foreach ($tgl_plans as $key => $value) {
			$tgl = str_replace("-","",$key);
			$ti = strtotime1($key);

			if($ti<=$sysdate){
				$sql = "select 
				sum(nvl(realisasi,0)) as progress
				from (
					select 
					a.id_plan,
					max(b.progress)/100*a.rencana as realisasi
					from wbs_plan_detail a 
					left join wbs_plan_detail_realisasi b on a.id_plan_detail = b.id_plan_detail
					where is_leaf = '1' and a.id_plan = ".$this->conn->escape($id)."
					and b.tgl <= '$tgl'
					and b.progress > 0 and b.progress is not null
					group by a.id_plan, a.id_plan_detail, a.rencana
				) a group by id_plan";

				$realisasi = (float)$this->conn->GetOne($sql);
				$realisasi_per_day[$key] = $realisasi;
			}
		}
		ksort($realisasi_per_day);

		$times['total_hari'] = $total_hari;
		$this->data['times'] = $times;
		$this->data['plan_per_day'] = $plan_per_day;
		$this->data['realisasi_per_day'] = $realisasi_per_day;
		if(!$this->data['rowheader']['nama_rendal_proyek'])
			$this->data['rowheader']['nama_rendal_proyek'] = $this->conn->GetOne("select modified_by_desc 
			from wbs_plan_detail where id_plan = $id and modified_by_desc is not null and last_update_realisasi is not null order by last_update_realisasi desc");

		if (!$inline) {
			$this->View('panelbackend/wbs_kurva');
		}

		if ($no_layout){
			$this->data['no_layout'] = $no_layout;
			echo $this->PartialView('panelbackend/wbs_kurva', true);
		}
	}

	private function update_parent($rows, &$ret=array(), $parent=0){
		unset($rows[0][0]);
		$total_rencana = 0;
		$total_realisasi = 0;
		$tgl_realisasi_max = null;
		if($rows[$parent])
			foreach($rows[$parent] as $r){

			$rencana = 0;
			$realisasi = 0;
			$progress = 0;
			$tgl_realisasi = null;
			if($rows[$r['urutan']])
				list($rencana, $realisasi, $tgl_realisasi) = $this->update_parent($rows, $ret, $r['urutan']);
			else{
				if(strtotime1($this->data['rowheader1']['sysdate1'])>=strtotime1($r['selesai'])){
					$rencana = $r['rencana'];
				}

				if($r['is_leaf']){
					$rw = $this->conn->GetRow("select keterangan, progress, tgl
						from wbs_plan_detail_realisasi 
						where id_plan_detail = $r[id_plan_detail]
						and tgl = ".$this->conn->escape($this->data['tglfilter']));

					$r['keterangan'] = $rw['keterangan'];
					$progress = $rw['progress'];
					$tgl_realisasi = $rw['tgl'];

					if(!$progress){
						$rw1 = $this->conn->GetRow("select 
							max(progress) progress, 
							max(tgl) as tgl
						from wbs_plan_detail_realisasi 
						where id_plan_detail = $r[id_plan_detail]
						and tgl <= ".$this->conn->escape($this->data['tglfilter']));

						$progress = $rw1['progress'];
						$tgl_realisasi = $rw1['tgl'];
					}

					if($progress)
						$realisasi = $r['rencana']*$progress/100;
				}

				if(strtotime1($this->data['rowheader1']['sysdate1'])>=strtotime1($r['mulai']) and strtotime1($this->data['rowheader1']['sysdate1'])<strtotime1($r['selesai'])){

					$detik0 = $r['durasi'];
					if(!$detik0)
						$detik0 = hitung_detik($r['id_plan'], $r['mulai'], $r['selesai']);

					$detik1 = hitung_detik($r['id_plan'], $r['mulai'], $this->data['rowheader1']['sysdate1']);
					$run = ($detik1/$detik0*$r['rencana']);
					$rencana += $run;
				}
			}

			$total_rencana += $rencana;
			$total_realisasi += $realisasi;
			if($tgl_realisasi_max<$tgl_realisasi)
				$tgl_realisasi_max = $tgl_realisasi;

			$r['rencana_target'] = $r['rencana'];
			$r['rencana'] = $rencana;
			$r['tgl_realisasi'] = $tgl_realisasi;
			$r['last_update_realisasi_str'] = substr($tgl_realisasi,-2)."-".substr($tgl_realisasi,4,2)."-".substr($tgl_realisasi,0,4);
			$r['realisasi'] = $realisasi;

			if($realisasi && $r['rencana_target'])
				$r['progress'] = $realisasi/$r['rencana_target']*100;
			else
				$r['progress'] = $progress;

			$ret[$r['urutan']] = $r;
		}

		return array($total_rencana,$total_realisasi,$tgl_realisasi_max);
	}

	public function Detail($id_pekerjaan=null, $id=null){
		if($this->post['tglfilter'])
			$_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id] = $this->post['tglfilter'];

		$tgl = $_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id];
		
		$this->_beforeDetail($id_pekerjaan, $id, $tgl);

		if(!$_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id]){
			$_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id] = $this->data['rowheader1']['sysdate1'];
			redirect(current_url());
		}

		if($this->post['act']=="set_harike"){
			$t = strtotime($this->data['rowheader1']['tgl_mulai_pelaksanaan'])+(($this->post['harike']-1)*86400);

			if($t>time())
				$t = time();

			$_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id] = date("d-m-Y", $t);
			redirect(current_url());
		}

		unset($this->access_role['add']);
		unset($this->access_role['delete']);

		$this->data['harike'] = round((strtotime($tgl)-strtotime($this->data['rowheader1']['tgl_mulai_pelaksanaan']))/86400)+1;

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	private function getDetails($id){
		$this->getPlan($id);
		$ret = $this->plan;
		$this->update_parent($this->plan_child, $ret);
		return $ret;
	}

	function getparentrealisasi() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);

		list($tgl,$bln,$thn) = explode("-",$_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id]);
		$this->data['tglfilter'] = $thn.$bln.$tgl;
		$this->data['rowheader1']['sysdate1'] = $_SESSION[SESSION_APP][$this->page_ctrl]['tgl'][$id].' 23:59:59';

		$details = $this->getDetails($id);

		$details1 = $this->_getParentRealisasi($details, $details[$urutan]['urutan_parent']);
		
		$list_realisasi = array();
		foreach($details1 as $r){
			$list_realisasi["rparent{$r['urutan']}"] = round($r['realisasi'],2);
			$list_realisasi["pr{$r['urutan']}"] = setProgressBar($r['progress']);
		}

		echo json_encode($list_realisasi);
	}

	function _getParentRealisasi($details, $urutan_parent) {
		$list_realisasi = array();

		$r = $details[$urutan_parent];
		$list_realisasi[$urutan_parent] = $r;

		if($r['urutan_parent']){
			$arr = $this->_getParentRealisasi($details, $r['urutan_parent']);
			$list_realisasi = array_merge($list_realisasi, $arr);
		}

		return $list_realisasi;
	}

	public function download_pdf($id_pekerjaan=null, $id_plan=null, $tgl=null){
		$this->_beforeDetail($id_pekerjaan);
		$this->load->library("PHPPdf");

		$pdf = new PHPPdf('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetMargins(3, 3, 3);
		$pdf->SetHeaderMargin(0);
		$pdf->SetFooterMargin(0);
		// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->SetFontSize(7);
		$pdf->SetAutoPageBreak(false, 0);

		$pdf->AddPage();
		$html = $this->curlwait(site_url("panelbackend/wbs/cetak/$id_pekerjaan/$id_plan/$tgl"),array(date('Ymd')=>base64_encode(base64_encode(json_encode($_SESSION[SESSION_APP]['menu'])).md5(date('Ymdhi')).base64_encode(json_encode($_SESSION[SESSION_APP]['group_id'])))));
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->lastPage();

		$pdf->Output('wbs '.$this->data['rowheader']['nama_proyek'].' '.$this->data['rowheader1']['nama_pekerjaan'].date('Ymdhis').'.pdf', 'I');
	}

}
