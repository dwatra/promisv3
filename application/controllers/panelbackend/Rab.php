<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab extends _adminController{
	public $limit = 5;
	public $limit_arr = array('5','10','30','50','100');

	public function __construct(){
		parent::__construct();
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->plugin_arr = array(
			'select2'
		);
	}

	protected function init(){
		parent::init();
	}

	function Index($page=0){
		redirect("panelbackend/sp3");
	}
	function Add(){
		$this->Edit(0);
	}

	function Edit($id=null){
		$this->data['page_title'] = 'SP3';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/rabdetail");
	}

	function Add1(){
		$this->data['page_title'] = 'Curva Omega';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/rabdetail1");
	}

	function Add2(){
		$this->data['page_title'] = 'RAB';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/rabdetail2");
	}

	function Detail($id=null){
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'RAB';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/rabdetail");
	}

	function Detail1($id=null){
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'RAB';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/rabdetail1");
	}

	function Detail2($id=null){
		$this->data['width'] = "1200px";
		$this->data['page_title'] = 'RAB';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/rabdetail2");
	}
}
