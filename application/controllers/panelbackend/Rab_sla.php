<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_sla extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_slalist";
		$this->viewdetail = "panelbackend/rab_sladetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah SLA';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit SLA';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail SLA';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar SLA';
		}
			$this->data['width'] = "1200px";

		$this->load->model("Rab_slaModel","model");
		$this->load->model("Rab_rabModel","rabrab");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("Mt_kriteria_slaModel","kriteriasla");

		$this->data['kriteriaarr'] = $this->kriteriasla->GArray();
		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			''
		);
	}
	public function Index($id_pekerjaan=0, $page=0){

		redirect("panelbackend/rab_sla/detail/$id_pekerjaan");
	}

	public function Add($id_pekerjaan=0){
		$this->Edit($id_pekerjaan);
	}

	public function Edit($id_pekerjaan=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$id = $id_pekerjaan;

		$this->_beforeDetail($id_pekerjaan,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id_pekerjaan);

		if (!$this->data['rowheader'])
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->post;

			$this->data['row'] = array_merge($this->data['row'],$record);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_pekerjaan'] = $id_pekerjaan;

			$this->_isValid($record,true);

			$return['success'] = true;

			$records = $record['nilai'];

            $this->model->conn->StartTrans();

			foreach($records as $id_kriteria_sla=>$rws){

				if(!$return['success'])
					break;

				foreach($rws as $jenis=>$nilai){

					if(!$return['success'])
						break;

					$record = array();
					$record['id_pekerjaan'] = $id_pekerjaan;
					$record['jenis'] = $jenis;
					$record['nilai'] = Rupiah2Number($nilai);
					$record['id_kriteria_sla'] = $id_kriteria_sla;

					$id_sla = $this->conn->GetOne("select id_sla from rab_sla where id_pekerjaan = ".$this->conn->escape($id_pekerjaan)." and jenis = ".$this->conn->escape($jenis)." and id_kriteria_sla = ".$this->conn->escape($id_kriteria_sla));

					if ($id_sla) {
						$return = $this->model->Update($record, "id_sla = ".$this->conn->qstr($id_sla));
					}else {
						$return = $this->model->Insert($record);
					}
				}
			}
			if ($return['success']) {

				$this->conn->trans_commit();

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");

			} else {

				$this->conn->trans_rollback();

				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function Rules(){
		return array(
			"nilai[]"=>array(
				'field'=>'nilai[]', 
				'label'=>'Nilai', 
				'rules'=>"required",
			),
		);
	}

	public function Detail($id_pekerjaan=null, $id=null){

		$id = $id_pekerjaan;

		unset($this->access_role['index']);

		$this->_beforeDetail($id_pekerjaan, $id);

		$this->data['row'] = $this->model->GetByPk($id_pekerjaan);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _beforeDetail($id_pekerjaan=null, &$id=null){
		$id = $this->conn->GetOne("select max(id_plan) from wbs_plan where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$this->data['id_pekerjaan'] = $id_pekerjaan;

		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_pekerjaan;

		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['id_rab'] = $id_rab = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);

		if (!$this->data['rowheader2']) $this->NoData('Tidak ada master RAB');
	}

	protected function _onDetail($id=null){
		if($this->data['row'])
			$this->access_role['add']=0;

		$this->data['row'][$this->pk] = $this->data['id_pekerjaan'];
	}

}