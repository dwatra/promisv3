<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Kurva extends _adminController{
	public $limit = 5;
	public $limit_arr = array('5','10','30','50','100');

	public function __construct(){
		parent::__construct();
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->plugin_arr = array(
			'select2'
		);
	}

	protected function init(){
		parent::init();
	}

	function Index($page=0){
		$this->data['page_title'] = 'Kurva Omega';
		$this->View("panelbackend/kurvalist");
	}

	function Edit($id=null){

		$this->data['page_title'] = 'Kurva Omega';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/kurvadetail");
	}

	function Add(){
		$this->Edit(0);
	}

	function Detail($id=null){
		$this->data['page_title'] = 'Kurva Omega';
		$this->layout = "panelbackend/layout_rab";
		$this->View("panelbackend/kurvadetail");
	}
}
