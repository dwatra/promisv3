<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Home extends _adminController{
	public $limit = 5;
	public $limit_arr = array('5','10','30','50','100');

	public function __construct(){
		parent::__construct();
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout";



	}
	protected function init(){
		parent::init();
	}

	function Index($page=0){
		// redirect('panelbackend/rab_proyek');

		redirect( base_url().$_SESSION[SESSION_APP]['redirect'] );
		//$this->View("panelbackend/home");
	}

	function Loginasback(){
		if(!($_SESSION[SESSION_APP]['loginas']))
			redirect('panelbackend');

		$loginas = $_SESSION[SESSION_APP]['loginas'];
		unset($_SESSION[SESSION_APP]);
		$_SESSION[SESSION_APP] = $loginas;

		redirect('panelbackend');
	}

	function Profile(){
		$this->access_mode[]='save';
		$this->access_mode[]='batal';
		$this->data['page_title'] = 'Profile';

		$this->load->model("Public_sys_userModel","model");

		$id=$_SESSION[SESSION_APP]['user_id'];
		$this->data['edited'] = true;

		$this->data['row'] = $this->model->GetByPk($id);
		if (!$this->data['row'] && $id)
			$this->NoData();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$valid = $this->_isValidProfile();
			if(!$valid){
				$this->View('panelbackend/profile');
				return;
			}

			$record = array();
			$record['name'] = $this->post['name'];

			if(!empty($this->post['password']))
			{
				$record['password']=sha1(md5($this->post['password']));
			}

            $this->_setLogRecord($record,$id);

			if ($id) {
				$return = $this->model->Update($record, "user_id = $id");
				if ($return) {
					SetFlash('suc_msg', $return['success']);
					redirect("panelbackend/home/profile");
				}
				else {
					$this->data['row'] = $record;
					$this->data['err_msg'] = "Data gagal diubah";
				}
			}
		}

		$this->View('panelbackend/profile');
	}

	function _isValidProfile(){

		$rules = array(
		   array(
				 'field'   => 'name',
				 'label'   => 'Nama',
				 'rules'   => 'required'
			  )
		);
		if($isnew=="true"){
			$rules[]=array(
				'field'   => 'password',
				'label'   => 'Password',
				'rules'   => 'required'
			);
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules($rules);

		$error_msg = '';
		if ($this->form_validation->run() == FALSE)
		{
			$error_msg .= validation_errors();
		}

		if($this->post['password']<>$this->post['confirmpassword']){
			if($error_msg) $error_msg.="<br/>";

			$error_msg .= "Konfirmasi password salah";
		}

		if($error_msg){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$this->data['row'] = array_merge($this->data['row'],$this->post);
			$this->data['err_msg'] = $error_msg;
			return false;
		}

		return true;
	}

	function ug(){
		if($_SESSION[SESSION_APP]['group_id']==1)
			$full_path = FCPATH."assets/doc/1.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==68)
			$full_path = FCPATH."assets/doc/68.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==48)
			$full_path = FCPATH."assets/doc/48.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==49)
			$full_path = FCPATH."assets/doc/49.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==50)
			$full_path = FCPATH."assets/doc/50.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==51)
			$full_path = FCPATH."assets/doc/51.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==52)
			$full_path = FCPATH."assets/doc/52.pdf";
		elseif($_SESSION[SESSION_APP]['group_id']==88)
			$full_path = FCPATH."assets/doc/88.pdf";

		header("Content-Type: application/pdf");
		header("Content-Disposition: inline; filename='ug.pdf'");
		echo file_get_contents($full_path);
		die();
	}
}
