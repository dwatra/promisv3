<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Mt_spek_material extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/mt_spek_materiallist";
		$this->viewdetail = "panelbackend/mt_spek_materialdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		$this->data['no_header'] = false;

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Spesifikasi Material';
			$this->data['no_header'] = false;
		}

		$this->load->model("Mt_spek_materialModel","model");
		$this->load->model("Mt_spek_material_filesModel","modelfile");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		$this->data['configfile'] = $this->config->item('file_upload_config');
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'upload','select2'
		);
	}

	protected function _beforeDelete($id=null){

		$rows = $this->conn->GetArray("select * from rab_spek_material_files where id_spek_material = ".$this->conn->escape($id));

		$ret = true;

		foreach($rows as $r){
			if(!$ret)
				break;
			
			@unlink($this->data['configfile']['upload_path'].$r['file_name']);
		}

		if($ret)
			$ret = $this->conn->Execute("delete from rab_spek_material_files where id_spek_material = ".$this->conn->escape($id));

		return $ret;
	}

	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'spesifikasi', 
				'label'=>'Spesifikasi', 
				'type'=>"varchar2",
			),
		);
	}

	public function Delete_all($id_wh_unit=null){

		$return = array('success'=>"Delete berhasil");

        $this->model->conn->StartTrans();

        $rows = $this->model->GArray("*", " where id_wh_unit = ".$this->conn->escape($id_wh_unit));

        if($rows)
        foreach($rows as $r){
        	if(!$return)
        		break;

        	$id = $r[$this->pk];

	        $this->_beforeDetail($id_wh_unit, $id);

			$this->data['row'] = $r;
			
			$this->_onDetail($id);

			if (!$this->data['row'])
				$this->NoData();

			$return = $this->_beforeDelete($id);

			if($return){
				$return = $this->model->Delete("$this->pk = ".$this->conn->qstr($id));
			}

			if($return){
				$return1 = $this->_afterDelete($id);
				if(!$return1)
					$return = false;
			}

			if($return)
				$this->log("menghapus $id");
        }else
	        $this->_beforeDetail($id_wh_unit);

        $this->model->conn->CompleteTrans();

		if ($return['success']) {
			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_wh_unit");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/index/$id_wh_unit");
		}
	}

	protected function Record($id=null){
		$return = array(
			'nama'=>$this->post['nama'],
			'spesifikasi'=>$this->post['spesifikasi'],
		);

		return $return;
	}

	public function HeaderExport(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
			),
			array(
				'name'=>'spesifikasi', 
				'label'=>'Spesifikasi', 
				'type'=>"varchar2",
			),
			/*array(
				'name'=>'durasi', 
				'label'=>'Durasi', 
				'width'=>"auto",
				'type'=>"number",
			),*/
		);
	}

	protected function Rules(){
		$return = array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"required|max_length[1000]",
			),
			"spesifikasi"=>array(
				'field'=>'spesifikasi', 
				'label'=>'Spesifikasi', 
				'rules'=>"max_length[4000]",
			),
		);


		return $return;
	}
	public function Index($id_wh_unit=0, $page=0){

		$this->_beforeDetail($id_wh_unit);

		$this->data['header']=$this->Header();
		$this->_setFilter("id_wh_unit = ".$this->conn->escape($id_wh_unit));

		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_wh_unit"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;


		$this->View($this->viewlist);
	}

	public function Add($id_wh_unit=0){
		$this->Edit($id_wh_unit);
	}

	public function Edit($id_wh_unit=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_wh_unit,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_wh_unit'] = $id_wh_unit;

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_wh_unit/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_wh_unit=null, $id=null){

		$this->_beforeDetail($id_wh_unit, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Delete($id_wh_unit=null, $id=null){

        $this->model->conn->StartTrans();

		$this->_beforeDetail($id_wh_unit, $id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return['success']) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_wh_unit");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_wh_unit/$id");
		}

	}

	protected function _beforeDetail($id_wh_unit=null, $id=null){
		$this->data['id_wh_unit'] = $id_wh_unit;
		$this->data['rowheader'] = $this->proyek->GetByPk($id_wh_unit);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_wh_unit;
	}

	protected function _afterDetail($id){

		if(!$this->data['row']['file']['id'] && $id){
			$rows = $this->conn->GetArray("select id_spek_material_files as id, client_name as name
				from rab_spek_material_files
				where jenis_file = 'file' and id_spek_material = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['file']['id'][] = $r['id'];
				$this->data['row']['file']['name'][] = $r['name'];
			}
		}
	}

	protected function _afterDelete($id){
		
		$ret = true;
		
		if($ret)
			$ret = $this->_delsertFiles($id);

		return $ret;
	}

	protected function _afterUpdate($id){
		return $this->_afterDelete($id);
	}

	protected function _afterInsert($id){
		return $this->_afterDelete($id);
	}

	private function _delsertFiles($id_spek_material = null){
		$ret = true;

		if(!empty($this->post['file'])){
			foreach($this->post['file']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_spek_material'=>$id_spek_material), $v);
			}
		}

		return $ret;
	}

	protected function _deleteFiles($id){
		
		$row = $this->modelfile->GetByPk($id);

		if(!$row)
			$this->Error404();

		$file_name = $row['file_name'];

		$return = $this->modelfile->Delete($this->modelfile->pk." = ".$this->conn->escape($id));

		if ($return) {
			$full_path = $this->data['configfile']['upload_path'].$file_name;
			@unlink($full_path);

			$ret = array("success"=>true);
		}else{
			$ret = array("error"=>"File ".$row['client_name']." gagal dihapus");
		}

		return $ret;
	}

	public function import_list($id_wh_unit=null){

		$file_arr = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel','application/wps-office.xls','application/wps-office.xlsx');

		if(in_array($_FILES['importupload']['type'], $file_arr)){

			$this->_beforeDetail($id_wh_unit);

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("","");

			$this->load->library('Factory');
			$inputFileType = Factory::identify($_FILES['importupload']['tmp_name']);
			$objReader = Factory::createReader($inputFileType);
			$excel = $objReader->load($_FILES['importupload']['tmp_name']);
			$sheet = $excel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
            $this->model->conn->StartTrans();

			#header export
			$header=array(
				array(
					'name'=>$this->model->pk
				)
			);
			$header=array_merge($header,$this->HeaderExport());

			for ($row = 2; $row <= $highestRow; $row++){ 

		    	$col = 'A';
		    	$record = array();
		    	$record['id_wh_unit'] = $id_wh_unit;
		    	foreach($header as $r1){
		    		if($r1['type']=='list')
		           		$record[$r1['name']] = (string)$sheet->getCell($col.$row)->getValue();
		           	else
		           		$record[$r1['name']] = $sheet->getCell($col.$row)->getValue();
		           	
	           		$col++;
		    	}

		    	$this->data['row'] = $record;

		    	$error = $this->_isValidImport($record);
		    	if($error){
		    		$return['error'] = $error;
		    	}else{
			    	if($record[$this->model->pk]){
			    		$return = $this->model->Update($record, $this->model->pk."=".$record[$this->model->pk]);
			    		$id = $record[$this->model->pk];

				    	if($return['success']){
				    		$ret = $this->_afterUpdate($id);

				    		if(!$ret){
				    			$return['success'] = false;
				    			$return['error'] = "Gagal update";
				    		}
				    	}
			    	}else{
			    		$return = $this->model->Insert($record);
			    		$id = $return['data'][$this->model->pk];

				    	if($return['success']){
				    		$ret = $this->_afterInsert($id);

				    		if(!$ret){
				    			$return['success'] = false;
				    			$return['error'] = "Gagal insert";
				    		}
				    	}
			    	}
			    }

				if(!$return['success'])
					break;				
			}


			if (!$return['error'] && $return['success']) {
            	$this->model->conn->trans_commit();
				SetFlash('suc_msg', $return['success']);
			}else{
            	$this->model->conn->trans_rollback();
				$return['error'] = "Gagal import. ".$return['error'];
				$return['success'] = false;
			}
		}else{
			$return['error'] = "Format file tidak sesuai";
		}

		echo json_encode($return);
	}

	public function export_list($id_wh_unit=null){
		$this->load->library('PHPExcel');
		$this->load->library('Factory');
		$excel = new PHPExcel();
		$excel->setActiveSheetIndex(0);	
		$excelactive = $excel->getActiveSheet();


		#header export
		$header=array(
			array(
				'name'=>$this->model->pk
			)
		);
		$header=array_merge($header,$this->HeaderExport());

		$row = 1;

	    foreach($header as $r){
	    	if(!$col)
	    		$col = 'A';
	    	else
	        	$col++;    

	        $excelactive->setCellValue($col.$row,$r['name']);
	    }

		$excelactive->getStyle('A1:'.$col.$row)->getFont()->setBold(true);
        $excelactive
		    ->getStyle('A1:'.$col.$row)
		    ->getFill()
		    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB('6666ff');

	    #data
	    $this->_setFilter("id_wh_unit = ".$this->conn->escape($id_wh_unit));
		$respon = $this->model->SelectGrid(
			array(
			'limit' => -1,
			'order' => $this->_order(),
			'filter' => $this->_getFilter()
			)
		);
		$rows = $respon['rows'];

		$row = 2;
        foreach($rows as $r){
	    	$col = 'A';
	    	foreach($header as $r1){
           		$excelactive->setCellValue($col.$row,$r[$r1['name']]);
           		$col++;
	    	}
            $row++;
        }


	    $objWriter = Factory::createWriter($excel,'Excel2007');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->ctrl.date('Ymd').'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
	}

	public function go_print($id_wh_unit=0, $page=0){

		$this->_beforeDetail($id_wh_unit);

		$this->_setFilter("a.id_wh_unit = ".$this->conn->escape($id_wh_unit));

		$this->data['sub_page_title'] = $this->data['rowheader']['nama_proyek'];
		$this->data['sub_page_title'] .= "<br/>".$this->data['rowheader1']['nama_pekerjaan'];
		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout4";

		$this->data['header']=$this->Header();

		$this->data['list']=$this->_getListPrint();

		$this->View($this->viewprint);
	}


}