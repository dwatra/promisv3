<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Pr_pr extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/pr_prlist";
		$this->viewdetail = "panelbackend/pr_prdetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";
			$this->data['width'] = "1200px";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah PR';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit PR';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail PR';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar PR';
		}

		$this->load->model("Pr_prModel","model");
		$this->load->model("Pr_pr_detailModel","modeldetail");
		
		$this->load->model("Mt_status_prModel","mtstatuspr");
		$this->data['mtstatusprarr'] = $this->mtstatuspr->GetCombo();
		$this->load->model("Mt_priority_prModel","mtprioritypr");
		$this->data['mtpriorityprarr'] = $this->mtprioritypr->GetCombo();
		$this->load->model("Mt_jenis_no_prModel","mtjenisnopr");
		$this->data['jenisnopr'] = $this->mtjenisnopr->GetCombo();
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("Pr_pr_tor_filesModel","modelfile");
		$this->load->model("Pr_pr_torModel","tor");

		$this->data['configfile'] = $this->config->item('file_upload_config');

		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;

		$this->plugin_arr = array(
			'datepicker','select2','upload'
		);
	}

	protected function Header(){
		return array(
/*			array(
				'name'=>'no', 
				'label'=>'NO', 
				'width'=>"auto",
				'type'=>"varchar2",
			),*/
			array(
				'name'=>'no_desc', 
				'label'=>'NO', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_permintaan', 
				'label'=>'Tgl. Permintaan', 
				'width'=>"auto",
				'type'=>"date",
			),
	/*		array(
				'name'=>'no_prk', 
				'label'=>'NO PRK', 
				'width'=>"auto",
				'type'=>"varchar2",
			),*/
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_dibutuhkan', 
				'label'=>'Tgl. Dibutuhkan', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'id_status_pr', 
				'label'=>'Status PR', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['mtstatusprarr'],
			),/*
			array(
				'name'=>'id_penyusun_tor', 
				'label'=>'Penyusun TOR', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'id_menyetujui_tor', 
				'label'=>'Menyetujui TOR', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_mulai_pengerjaan', 
				'label'=>'Tgl. Mulai Pengerjaan', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'tgl_selesai_pengerjaan', 
				'label'=>'Tgl. Selesai Pengerjaan', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'id_pemohon', 
				'label'=>'Pemohon', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'id_menyetujui', 
				'label'=>'Menyetujui', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['mtpegawaiarr'],
			),*/
		);
	}

	private function upsertNo($tahun, $id_jenis_no_pr, $no){
		$id_no_pr = $this->conn->GetOne("select id_no_pr from mt_no_pr where tahun = ".$this->conn->escape($tahun)." and id_jenis_no_pr = ".$this->conn->escape($id_jenis_no_pr));

		if($id_no_pr){
			$ret = $this->conn->goUpdate("mt_no_pr",array("no"=>$no),"id_no_pr = ".$this->conn->escape($id_no_pr));
		}else{
			$ret = $this->conn->goInsert("mt_no_pr",array("tahun"=>$tahun, "id_jenis_no_pr"=>$id_jenis_no_pr, "no"=>$no));
		}

		return $ret;
	}

	protected function Record($id=null){
		$return = array(
			'no_desc'=>$this->post['no_desc'],
			'id_jenis_no_pr'=>$this->post['id_jenis_no_pr'],
			'tgl_permintaan'=>$this->post['tgl_permintaan'],
			'no_prk'=>$this->post['no_prk'],
			'id_priority_pr'=>$this->post['id_priority_pr'],
			'nama'=>$this->post['nama'],
			'keterangan'=>$this->post['keterangan'],
			'id_rab_detail'=>$this->post['id_rab_detail'],
			'tgl_dibutuhkan'=>$this->post['tgl_dibutuhkan'],
			'id_status_pr'=>$this->post['id_status_pr'],
			'id_penyusun_tor'=>$this->post['id_penyusun_tor'],
			'id_menyetujui_tor'=>$this->post['id_menyetujui_tor'],
		/*	'tgl_mulai_pengerjaan'=>$this->post['tgl_mulai_pengerjaan'],
			'tgl_selesai_pengerjaan'=>$this->post['tgl_selesai_pengerjaan'],*/
			'id_pemohon'=>$this->post['id_pemohon'],
			'id_menyetujui'=>$this->post['id_menyetujui'],
			'id_mengetahui'=>$this->post['id_mengetahui'],
		);

		if(!$id && $this->post['id_jenis_no_pr']){
			$tahun = date('Y',strtotime($this->post['tgl_permintaan']));
			$return['no'] = $this->conn->GetOne("select no from mt_no_pr where id_jenis_no_pr = ".$this->conn->escape($this->post['id_jenis_no_pr'])." and tahun = ".$tahun)+1;
			#no.pr/bidang/pjbs/tahun
			unset($this->post['no_desc']);
			$return['no_desc'] = str_pad($return['no'],3,'0', STR_PAD_LEFT).'.PR/'.$this->data['jenisnopr'][$return['id_jenis_no_pr']].'/PJBS/'.$tahun;
		}



		$r = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_penyusun_tor'])));
		$return['nama_penyusun_tor'] = $r['nama'];
		$return['jabatan_penyusun_tor'] = $r['jabatan'];

		$r = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_menyetujui_tor'])));
		$return['nama_menyetujui_tor'] = $r['nama'];
		$return['jabatan_menyetujui_tor'] = $r['jabatan'];

		$r = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_pemohon'])));
		$return['nama_pemohon'] = $r['nama'];
		$return['jabatan_pemohon'] = $r['jabatan'];

		$r = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_menyetujui'])));
		$return['nama_menyetujui'] = $r['nama'];
		$return['jabatan_menyetujui'] = $r['jabatan'];

		$r = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_mengetahui'])));
		$return['nama_mengetahui'] = $r['nama'];
		$return['jabatan_mengetahui'] = $r['jabatan'];

		return $return;
	}

	function checkprk($str)
    {
    	$ret = $this->conn->GetOne("select 1 from mt_prk where prk = ".$this->conn->escape($str));
        if (!$ret)
        {
                $this->form_validation->set_message('checkprk', 'PRK tidak terdaftar');
                return FALSE;
        }
        else
        {
                return TRUE;
        }
    }

	protected function Rules(){
		return array(
			"no"=>array(
				'field'=>'no', 
				'label'=>'NO', 
				'rules'=>"max_length[20]",
			),
			"no_desc"=>array(
				'field'=>'no_desc', 
				'label'=>'NO Desc', 
				'rules'=>"required|max_length[200]",
			),
			"id_priority_pr"=>array(
				'field'=>'id_priority_pr', 
				'label'=>'Prioritas', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['mtpriorityprarr']))."]",
			),
			"no_prk"=>array(
				'field'=>'no_prk', 
				'label'=>'NO PRK', 
				'rules'=>"required|max_length[200]|callback_checkprk",
			),
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"required|max_length[1000]",
			),
			"tgl_dibutuhkan"=>array(
				'field'=>'tgl_dibutuhkan', 
				'label'=>'Tgl. Dibutuhkan', 
				'rules'=>"required",
			),
			"tgl_permintaan"=>array(
				'field'=>'tgl_permintaan', 
				'label'=>'Tgl. Permintaan', 
				'rules'=>"required",
			),
			"id_status_pr"=>array(
				'field'=>'id_status_pr', 
				'label'=>'Status PR', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['mtstatusprarr']))."]",
			),
			"id_jenis_no_pr"=>array(
				'field'=>'id_jenis_no_pr', 
				'label'=>'Jenis PR', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['jenisnopr']))."]",
			),
		);
	}

	protected function _getListMonitoring($page=0){
		$this->data['page_title'] = "Monitoring Pengadaan";
		$this->access_role['add']=false;
		$this->access_role['edit']=false;
		$this->access_role['delete']=false;
		unset($this->data['width']);
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";
		$this->_resetList();

		$this->arrNoquote = $this->model->arrNoquote;

		$param=array(
			'page' => $page,
			'limit' => $this->_limit(),
			'order' => $this->_order(),
			'filter' => $this->_getFilter()
		);

		if($this->post['act']){
			
			if($this->data['add_param']){
				$add_param = '/'.$this->data['add_param'];
			}
			redirect(str_replace(strstr(current_url(),"/index$add_param/$page"), "/index{$add_param}", current_url()));
		}

		$respon = $this->model->SelectGridMonitoring(
			$param
		);

		return $respon;
	}

	protected function HeaderMonitoring(){
		return array(
			array(
				'name'=>'nama_proyek', 
				'label'=>'Proyek', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'no_desc', 
				'label'=>'No. PR', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nama_item', 
				'label'=>'Item', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'status', 
				'label'=>'Status', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_permintaan', 
				'label'=>'Tgl. Permintaan', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'tgl_dibutuhkan', 
				'label'=>'Tgl. Dibutuhkan', 
				'width'=>"auto",
				'type'=>"date",
			),
		);
	}

	public function monitoring($page=0){
		$this->data['header']=$this->HeaderMonitoring();
		$this->data['list']=$this->_getListMonitoring($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/monitoring"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;

		$this->View($this->viewlist);
	}

	public function Index($id_rab=0, $page=0){

			unset($_SESSION[SESSION_APP][$this->page_ctrl]['list_sort']);
		$this->_beforeDetail($id_rab);
		$this->data['header']=$this->Header();
		$this->_setFilter("id_rab = ".$this->conn->escape($id_rab));

		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_rab"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;


		$this->View($this->viewlist);
	}

	public function Add($id_rab=0){
		$this->Edit($id_rab);
	}

	public function Edit($id_rab=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_rab,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_rab'] = $id_rab;

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_rab/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _onDetail($id=null){


		if($this->post['act']=='push_pr' && $id && $this->data['row']['id_status_pr']<>5){
			$this->_pushPr();
		}

		if($this->data['row']['id_status_pr']==5){
			$this->data['edited'] = false;
			$this->access_role['edit'] = false;
			$this->access_role['delete'] = false;
		}
	}

	public function Detail($id_rab=null, $id=null){

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		if(($this->post['act']=='delete_tgl' or $this->post['act']=='save') && $this->access_role['edit']){
			if($this->post['act']=='delete_tgl')
				$return = $this->tor->delete("id_pr_tor = ".$this->conn->escape($this->post['key']));
			elseif($this->post['act'] == 'save'){
				$record = array();
				$record['tgl'] = $this->post['tgl'];
				$record['jenis'] = $this->post['jenis'];
				$record['id_pr'] = $id;
				if($this->post['key']){
					$id_pr_tor = $this->post['key'];
					$return = $this->tor->Update($record,"id_pr_tor = ".$this->conn->escape($this->post['key']));
				}else{
					$return = $this->tor->Insert($record);
					$id_pr_tor = $return['data']['id_pr_tor'];
				}

				if($return['success']){
					if(count($this->post['tor'])){
						$this->_updateFiles(array('id_pr_tor'=>$id_pr_tor), $this->post['tor']['id']);
					}
					if(count($this->post['dmr'])){
						$this->_updateFiles(array('id_pr_tor'=>$id_pr_tor), $this->post['dmr']['id']);
					}
				}
			}


			if($return['success']){

				SetFlash("suc_msg","Berhasil");
			}else{
				SetFlash("err_msg","Gagal");
			}

			redirect(current_url());
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	private function _pushSCM(){
		$this->conn->StartTrans();

		$data = array();
		$data['header'] = $this->model->GetByPk($this->data['row']['id_pr']);
		$data['header']['jenis_pr'] = $this->data['jenisnopr'][$data['header']['id_jenis_no_pr']];
		$data['header']['prioritas_pr'] = $this->data['mtpriorityprarr'][$data['header']['id_priority_pr']];
		$data['header']['id_unit'] = $this->data['id_unit'];
		$data['header']['id_bidang'] = $_SESSION[SESSION_APP]['subdit'];
		$data['header']['lampiran'] = json_encode($this->conn->GetRow("select * from pr_pr_tor_files where id_pr_tor_files = ".$this->conn->escape($this->data['rowtor']['id'])));
		$pos = array();
		$pos['data'] = $data;

		$return = $this->reqscm("insert_pr", $pos);

		return $return['success'];
	}

	private function _pushPr(){
		$id_pr = $this->data['row']['id_pr'];

		$this->_afterDetail($id_pr);

		$cek = false;

		$rowtor = array();

		foreach($this->data['rowstgl'] as $r){
			if($r['jenis']==2 && $r['tor']){
				$cek = true;
				$this->data['rowtor'] = $r['tor'];
				break;
			}
		}

		if(!$cek){
			SetFlash("err_msg","Tor Final wajib dilampirkan");
			redirect(current_url());
		}

		$rows = $this->conn->GetArray("select 
			b.id_pr_detail,
			f.id_warehouse,
			a.id_pemohon,
			a.nama_pemohon,
			a.jabatan_pemohon,
			p.kdjabatan as j_pemohon,
			a.id_menyetujui,
			a.nama_menyetujui,
			a.jabatan_menyetujui,
			p1.kdjabatan as j_menyetujui,
			a.id_priority_pr,
			d.jasa_material,
			a.no_prk,
			g.account_code,
			nvl(d.satuan, c.satuan) as satuan,
			d.id_jasa_material, 
			c.id_rab_detail, 
			c.kode_biaya, 
			a.no_desc, 
			a.tgl_permintaan, 
			nvl(d.nama,c.uraian) as uraian, 
			a.tgl_dibutuhkan, 
			a.nama,
			nvl(b.harga_satuan,0) * 1.1 as nilai,
			nvl(d.vol, c.vol) as vol, 
			to_char(a.tgl_permintaan, 'YYYYMMDD') as requirebydate,
			to_char(a.tgl_dibutuhkan, 'YYYYMMDD') as issuedate
			from pr_pr a 
			join pr_pr_detail b on a.id_pr = b.id_pr
			join rab_rab_detail c on b.id_rab_detail = c.id_rab_detail
			left join rab_jasa_material d on b.id_jasa_material = d.id_jasa_material
			left join rab_rab h on c.id_rab = h.id_rab
			left join rab_pekerjaan e on h.id_pekerjaan = e.id_pekerjaan and e.is_deleted = '0'
			left join rab_proyek f on e.id_proyek = f.id_proyek and f.is_deleted = '0'
			left join mt_prk g on upper(trim(a.no_prk)) = upper(trim(g.prk))
			left join mt_pegawai p on a.id_pemohon = p.nid
			left join mt_pegawai p1 on a.id_menyetujui = p1.nid
			where a.id_pr = ".$this->conn->escape($id_pr));

		$data = array();

		foreach($rows as $r){
			$row = array();

			$row['DISTRICTCODE'] = 'PJBS';
			$row['IREQTYPE'] = 'PR';
			$row['ID_PROM'] = '1';
			$row['STATUS_TRANSFER'] = "";
			$row['ORIGWHOUSEID'] = str_replace("PJBS","",$r['id_warehouse']);
			$row['MXREQUESTEDBY'] = $r['id_pemohon'];
			$row['MXREQUESTEDNAMA'] = $r['nama_pemohon'];
			$row['MXREQUESTEDPOSISI'] = $r['j_pemohon'];
			$row['AUTHSDBY'] = $r['id_menyetujui'];
			$row['AUTHSDBYNAME'] = $r['nama_menyetujui'];
			$row['AUTHSDPOSITION'] = $r['j_menyetujui'];
			$row['PRIORITYCODE'] = $r['id_priority_pr'];
			$row['ITEMTYPE'] = (($r['jasa_material']=='2' or $r['jasa_material']=='3')?'D':'V');
			$row['ALLOCPCA'] = "100";
			$row['COSTCENTREA'] = $r['account_code'];
			$row['PROJECTA'] = $r['no_prk'];
			$row['UNITOFMEASURE'] = strtoupper($r['satuan']);
			$row['REQUIREDBYDATE'] = $r['requirebydate'];
			$row['ISSUEDATE'] = $r['issuedate'];
			$row['UNITCOST'] = $r['nilai'];
			$row['QUANTITY'] = $r['vol'];
			$row['DESCRIPTION2'] = $r['uraian'];
			$row['QTYREQUIRED'] = $r['vol'];
			$row['CREATIONDATE'] = date('Ymd');
			$row['DESCRIPTION'] = $r['nama'];

			$this->data['id_unit'] = $r['id_warehouse'];
			// dpr($row,1);
			/*echo "<pre>";
			print_r($row);
			die();*/

			$data[] = $row;
		}

		$url_ellipse = $this->config->item('url_ellipse');

		$ret = $this->curlx($url_ellipse, array("Datas"=>array("Data"=>$data)));

		$return = (strstr($ret,'sukses')!==false);

		if($return){
			$no_pr_ellipse = str_replace("sukses ","",$ret);
			$i=1;
			foreach($rows as $r){
				if(!$return)
					break;

				$return = $this->conn->goUpdate("pr_pr_detail",
					array(
						'no_pr_ellipse'=>$no_pr_ellipse,
						'no_item_pr'=>str_pad($i, 3, "0", STR_PAD_LEFT)
					),
					"id_pr_detail=".$this->conn->escape($r['id_pr_detail'])
				);

				$i++;
			}

			if($return){
				$ret = $this->model->Update(
					array(
						'id_status_pr'=>5,
						'no_pr_ellipse'=>$no_pr_ellipse,
					), 
					"id_pr=".$this->conn->escape($id_pr)
				);

				$return = $ret['success'];
			}
		}else{
			list($ex, $ret) = explode("Message:",$ret);
			list($ret, $ex) = explode("FieldName:",$ret);
			$err = '"'.trim($ret).'"';
		}

		if($return)
			$return = $this->_pushSCM();

		if($return){
			SetFlash("suc_msg","Berhasil");
			redirect("$this->page_ctrl/detail/".$this->data['id_rab']."/".$this->data['row']['id_pr']);
		}
		else{
			SetFlash("err_msg","Gagal ".$err);
			redirect(current_url());
		}
	}

	public function go_print($id_rab=null, $id=null){

		$this->_beforeDetail($id_rab, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->template = "panelbackend/main3";
		$this->layout = "panelbackend/layout4";

		$this->data['rowsdetail'] = $this->conn->GetArray("select
				a.*, nvl(b.nama, c.uraian) as uraian, nvl(b.satuan, c.satuan) as satuan
				from pr_pr_detail a
				left join rab_jasa_material b on a.id_jasa_material = b.id_jasa_material
				left join rab_rab_detail c on a.id_rab_detail = c.id_rab_detail
				where a.id_pr = ".$this->conn->escape($id));
		$total = 0;
	    foreach($this->data['rowsdetail'] as $r){ 
        	$r['harga_satuan'] = ((float)$r['harga_satuan'])*1.1;
            $total+=(float)$r['harga_satuan']*(float)$r['vol'];
	    }

		$this->data['page_title'] = 'DAFTAR PERMINTAAN PENGADAAN BARANG DAN JASA ( Rp 300 Juta s.d 2 Milyar )';

		if($total<300000000)
			$this->data['page_title'] = 'DAFTAR PERMINTAAN PENGADAAN BARANG DAN JASA';

		if($total>2000000000)
			$this->data['page_title'] = 'DAFTAR PERMINTAAN PENGADAAN BARANG DAN JASA ( > Rp 2 Milyar )';

		$this->View("panelbackend/pr_prprint");
	}

	public function Delete($id_rab=null, $id=null){

        $this->model->conn->StartTrans();

        $this->_beforeDetail($id_rab,$id);

		$this->data['row'] = $this->model->GetByPk($id);

		if($this->data['row']['id_status_pr']==5){
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab/$id");
		}

		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_rab");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab/$id");
		}

	}

	protected function _beforeDetail($id_rab=null, $id=null){
		$this->data['id_rab'] = $id_rab;
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
		$this->data['id_pekerjaan'] = $id_pekerjaan = $this->data['rowheader2']['id_pekerjaan'];
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_rab;
		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		/*if($this->data['last_versi']<>$this->data['rowheader2']['id_rab']){
			$this->access_role['add']=0;
			$this->access_role['edit']=0;
			$this->access_role['delete']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['upload_file']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['save']=0;
		}*/
	}

	protected function _afterDetail($id=null){
		$this->data['rowstgl'] = $this->tor->GArray("*","where id_pr = ".$this->conn->escape($id)." order by jenis asc, id_pr_tor asc");

		$rows = $this->conn->GetArray("select a.id_pr_tor_files as id, client_name as name, jenis_file, a.id_pr_tor from pr_pr_tor_files a join pr_pr_tor b on a.id_pr_tor = b.id_pr_tor
			where b.id_pr = ".$this->conn->escape($id));

		$temp = array();

		foreach($rows as $r){
			$temp[$r['id_pr_tor']][$r['jenis_file']] = $r;
		}

		$temp1 = $this->data['rowstgl'];
		foreach($temp1 as $k=>$r){
			$this->data['rowstgl'][$k]['dmr'] = $temp[$r['id_pr_tor']]['dmr'];
			$this->data['rowstgl'][$k]['tor'] = $temp[$r['id_pr_tor']]['tor'];
		}

		if($this->data['row']['id_rab']){
			$this->data['rows'] = $this->conn->GetArray("select
				a.*, nvl(b.nama, c.uraian) as uraian, nvl(b.satuan, c.satuan) as satuan
				from pr_pr_detail a
				left join rab_jasa_material b on a.id_jasa_material = b.id_jasa_material
				left join rab_rab_detail c on a.id_rab_detail = c.id_rab_detail
				where a.id_pr = ".$this->conn->escape($id)."
				order by uraian");
			/*$rows = $this->conn->GetArray("select a.*, nvl(nilai_satuan,0)*nvl(vol,1)*nvl(day,1) as nilai from rab_rab_detail a where id_rab = ".$this->conn->escape($this->data['row']['id_rab'])." 
				and id_item is null 
				and id_rab_detail not in (
					select id_rab_detail 
					from pr_pr_detail a
					join pr_pr b on a.id_pr=b.id_pr
					where id_jasa_material is null and b.id_rab = ".$this->conn->escape($this->data['row']['id_rab'])."
					and a.id_pr <> ".$this->conn->escape($id)."
				) 
				order by kode_biaya, id_rab_detail");

			$this->data['rows'] = array();
			$i = 0;
			$this->GenerateTree($rows, "id_rab_detail_parent", "id_rab_detail", "uraian", $this->data['rows'], null, $i, 0, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

			$rows = $this->conn->GetArray("select a.*, nvl(vol,1)*nvl(harga_satuan,0) as nilai from rab_jasa_material a where id_rab = ".$this->conn->escape($this->data['row']['id_rab'])."
				and id_jasa_material not in (
					select id_jasa_material 
					from pr_pr_detail a
					join pr_pr b on a.id_pr=b.id_pr
					where id_jasa_material is not null and b.id_rab = ".$this->conn->escape($this->data['row']['id_rab'])."
					and a.id_pr <> ".$this->conn->escape($id)."
				) ");

			$this->data['jasa_materialarr'] = array();
			foreach($rows as $r){
				$this->data['jasa_materialarr'][$r['id_pos_anggaran']][$r['jasa_material']][$r['kode_biaya']][] = $r;
			}

			$rows = $this->conn->GetArray("select id_rab_detail, id_jasa_material, no_item_pr, nilai from pr_pr_detail where id_pr = ".$this->conn->escape($id));

			$this->data['id_rab_detail'] = array();
			foreach($rows as $r){
				if($r['id_jasa_material']){
					$this->data['id_rab_detail'][$r['id_rab_detail']][$r['id_jasa_material']]=1;
					$this->data['no_item_pr'][$r['id_rab_detail']][$r['id_jasa_material']]=$r['no_item_pr'];
					$this->data['nilai'][$r['id_rab_detail']][$r['id_jasa_material']]=$r['nilai'];
				}
				else{
					$this->data['id_rab_detail'][$r['id_rab_detail']]=1;
					$this->data['nilai'][$r['id_rab_detail']]=$r['nilai'];
				}
			}*/
		}

		$this->data['penyusuntorarr'][$this->data['row']['id_penyusun_tor']] = $this->data['row']['nama_penyusun_tor'];
		$this->data['menyetujuitorarr'][$this->data['row']['id_menyetujui_tor']] = $this->data['row']['nama_menyetujui_tor'];
		$this->data['pemohonrarr'][$this->data['row']['id_pemohon']] = $this->data['row']['nama_pemohon'];
		$this->data['menyetujuirarr'][$this->data['row']['id_menyetujui']] = $this->data['row']['nama_menyetujui'];
		$this->data['mengetahuirarr'][$this->data['row']['id_mengetahui']] = $this->data['row']['nama_mengetahui'];

		if($this->data['row']['id_status_pr']==5){
		/*	$this->data['edited'] = false;
			$this->access_role['edit'] = false;*/
			$this->access_role['delete'] = false;
		}
	}

	function GenerateTree(&$row, $colparent, $colid, $collabel, &$return=array(), $valparent=null, &$i=0, $level=0, $spacea = "&nbsp;➥&nbsp;", $max_level=100){

		$level++;
		foreach ($row as $key => $value) {
			# code...
			if(trim($value[$colparent])==trim($valparent)){
			
				$space = '';
				for($k=1; $k<$level; $k++){
					$space .= $spacea;
				}

				$value[$collabel] = $space.$value[$collabel];
				$value['level'] = $level;

				if($level<=1 or (!$value['kode_biaya'] && $level<=2))
					$value[$collabel] = "<b>".$value[$collabel]."</b>";


				if($level<=$max_level){
					$return[$i]=$value;
				}

				$i++;

				$temp = $row;
				unset($temp[$key]);

				$this->GenerateTree($temp, $colparent, $colid, $collabel, $return, $value[$colid], $i, $level,$spacea, $max_level);

				$row = $temp;
			}
		}

		if($row && $level==1)
			$return = array_merge($return, $row);
	}

	protected function _afterUpdate($id){
		return $this->_afterInsert($id);
	}

	protected function _afterInsert($id){
		$ret = true;

	/*	if($ret)
			$ret = $this->_delsertRab($id);*/

		if($ret)
			$ret = $this->upsertNo(date("Y",strtotime($this->post['tgl_permintaan'])), $this->post['id_jenis_no_pr'], $this->data['row']['no']);

		return $ret;
	}
/*
	private function _delsertRab($id_pr = null){
		$ret = $this->conn->Execute("delete from pr_pr_detail where id_pr = ".$this->conn->escape($id_pr));

		if(is_array($this->post['id_rab_detail']) && count($this->post['id_rab_detail'])){
			foreach($this->post['id_rab_detail'] as $k=>$v){
				if(!$ret)
					break;

				if(is_array($v)){
					foreach($v as $k1=>$v1){

						$record = array();
						$record['id_pr'] = $id_pr;
						$record['id_rab_detail'] = $k;
						$record['id_jasa_material'] = $k1;
						$record['no_item_pr'] = $this->post['no_item_pr'][$k][$k1];
						$record['nilai'] = $this->post['nilai'][$k][$k1];

						$ret = $this->conn->goInsert("pr_pr_detail", $record);
					}
				}else{

					$record = array();
					$record['id_pr'] = $id_pr;
					$record['id_rab_detail'] = $k;
					$record['no_item_pr'] = $this->post['no_item_pr'][$k];
					$record['nilai'] = $this->post['nilai'][$k];

					$ret = $this->conn->goInsert("pr_pr_detail", $record);
				}
			}
		}
		return $ret;
	}*/


	protected function _isValidImport($record){
		/*$this->data['rules'] = $this->Rules();

		$rules = array_values($this->data['rules']);

		if($record){
			$this->form_validation->set_data($record);
		}

		$this->form_validation->set_rules($rules);

		if (count($rules) && $this->form_validation->run() == FALSE)
		{
			return validation_errors();
		}*/
	}

	public function HeaderExport(){
		return array(
			array(
				'name'=>'uraian', 
				'label'=>'Jasa/Material', 
			),
			array(
				'name'=>'id_jasa_material', 
				'label'=>'id_jasa_material', 
			),
			array(
				'name'=>'id_rab_detail', 
				'label'=>'id_rab_detail', 
			),
			array(
				'name'=>'vol', 
				'label'=>'vol', 
			),
			array(
				'name'=>'satuan', 
				'label'=>'satuan', 
			),
			array(
				'name'=>'harga_satuan', 
				'label'=>'harga_satuan', 
			),
			array(
				'name'=>'no_item_pr', 
				'label'=>'no_item_pr', 
			),
		);
	}

	protected function _beforeDelete($id=null){
		$ret = true;
		$rows = $this->conn->GetArray("select * from pr_pr_detail where id_pr = ".$this->conn->escape($id));

		foreach($rows as $r){
			if(!$ret)
				break;

			$ret = $this->conn->Execute("delete from pr_pr_detail where id_pr_detail = ".$this->conn->escape($r['id_pr_detail']));

			if($ret){
				if($r['id_jasa_material']){
					$ret = $this->conn->Execute("update rab_jasa_material set vol_pengadaan = (select sum(nvl(vol,0)) from pr_pr_detail where id_jasa_material = ".$this->conn->escape($r['id_jasa_material']).") where id_jasa_material = ".$this->conn->escape($r['id_jasa_material']));
				}elseif($r['id_rab_detail']){
					$ret = $this->conn->Execute("update rab_rab_detail set vol_pengadaan = (select sum(nvl(vol,0)) from pr_pr_detail where id_rab_detail = ".$this->conn->escape($r['id_rab_detail']).") where id_rab_detail = ".$this->conn->escape($r['id_rab_detail']));
				}
			}
		}

		return $ret;
	}

	public function import_list($id_rab=null, $id_pr=null){

		$file_arr = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel','application/wps-office.xls','application/wps-office.xlsx');

		if(in_array($_FILES['importupload']['type'], $file_arr)){

			$this->_beforeDetail($id_rab, $id_pr);

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("","");

			$this->load->library('Factory');
			$inputFileType = Factory::identify($_FILES['importupload']['tmp_name']);
			$objReader = Factory::createReader($inputFileType);
			$excel = $objReader->load($_FILES['importupload']['tmp_name']);
			$sheet = $excel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
            $this->modeldetail->conn->StartTrans();

			#header export
			$header=array(
				array(
					'name'=>$this->modeldetail->pk
				)
			);
			$header=array_merge($header,$this->HeaderExport());

			$id_pr_detail = array();

			for ($row = 2; $row <= $highestRow; $row++){ 

		    	$col = 'A';
		    	$record = array();
		    	$record['id_pr'] = $id_pr;
		    	foreach($header as $r1){
		    		if($r1['type']=='list')
		           		$record[$r1['name']] = (string)$sheet->getCell($col.$row)->getValue();
		           	else
		           		$record[$r1['name']] = $sheet->getCell($col.$row)->getValue();
		           	
	           		$col++;
		    	}

		    	if(!$record['id_jasa_material'] && !$record['id_rab_detail'])
		    		continue;

		    	$this->data['row'] = $record;

		    	$error = $this->_isValidImport($record);
		    	if($error){
		    		$return['error'] = $error;
		    	}else{
			    	if($record[$this->modeldetail->pk]){
			    		$return = $this->modeldetail->Update($record, $this->modeldetail->pk."=".$record[$this->modeldetail->pk]);
			    		$id_pr_detail[] = $id = $record[$this->modeldetail->pk];
			    	}else{
			    		$return = $this->modeldetail->Insert($record);
			    		$id_pr_detail[] = $id = $return['data'][$this->modeldetail->pk];
			    	}
			    }

				if(!$return['success'])
					break;				
				else{
					if($record['id_jasa_material']){
						$return['success'] = $this->conn->Execute("update rab_jasa_material set vol_pengadaan = (select sum(nvl(vol,0)) from pr_pr_detail where id_jasa_material = ".$this->conn->escape($record['id_jasa_material']).") where id_jasa_material = ".$this->conn->escape($record['id_jasa_material']));
					}elseif($record['id_rab_detail']){
						$return['success'] = $this->conn->Execute("update rab_rab_detail set vol_pengadaan = (select sum(nvl(vol,0)) from pr_pr_detail where id_rab_detail = ".$this->conn->escape($record['id_rab_detail']).") where id_rab_detail = ".$this->conn->escape($record['id_rab_detail']));
					}
				}
			}


			if (!$return['error'] && $return['success'] && count($id_pr_detail)) {
				$return['success'] = $this->conn->Execute("delete 
					from pr_pr_detail 
					where id_pr = ".$this->conn->escape($id_pr)." 
					and id_pr_detail not in (".implode(", ", $id_pr_detail).")");
			}

			if (!$return['error'] && $return['success']) {
            	$this->modeldetail->conn->trans_commit();
				SetFlash('suc_msg', "Import Berhasil");
			}else{
            	$this->modeldetail->conn->trans_rollback();
				$return['error'] = "Gagal import. ".$return['error'];
				$return['success'] = false;
			}
		}else{
			$return['error'] = "Format file tidak sesuai";
		}

		echo json_encode($return);
	}

	public function export_list($id_rab=null, $id=null){
		$this->load->library('PHPExcel');
		$this->load->library('Factory');
		$excel = new PHPExcel();
		$excel->setActiveSheetIndex(0);	
		$excelactive = $excel->getActiveSheet();

		$rowheader = $this->model->GetByPk($id);

		#header export
		$header=array(
			array(
				'name'=>$this->modeldetail->pk
			)
		);
		$header=array_merge($header,$this->HeaderExport());

		$row = 1;

	    foreach($header as $r){
	    	if(!$col)
	    		$col = 'A';
	    	else
	        	$col++;    

	        $excelactive->setCellValue($col.$row,$r['name']);
	    }

		$excelactive->getStyle('A1:'.$col.$row)->getFont()->setBold(true);
        $excelactive
		    ->getStyle('A1:'.$col.$row)
		    ->getFill()
		    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB('6666ff');

		$add_filter = "";

		if($rowheader['id_status_pr']==5)
			$add_filter = " and id_pr_detail is not null ";

	    #data
	    $rows = $this->conn->GetArray("select a.* from (
	    	select
	    	nvl(c.nama, e.uraian||' '||a.uraian) as uraian, 
	    	nvl(c.satuan, a.satuan) as satuan,
	    	nvl(d.id_pr_detail, b.id_pr_detail) as id_pr_detail, 
	    	nvl(d.no_item_pr, b.no_item_pr) as no_item_pr, 
	    	nvl(nvl(d.harga_satuan,c.harga_satuan), nvl(b.harga_satuan,a.nilai_satuan)) as harga_satuan,
	    	nvl(nvl(d.vol,(c.vol-nvl(c.vol_pengadaan,0))), nvl(b.vol,(a.vol-nvl(a.vol_pengadaan,0)))) as vol,
	    	c.id_jasa_material, a.id_rab_detail
	    	from rab_rab_detail a
	    	left join rab_jasa_material c 
	    	on a.id_pos_anggaran = c.id_pos_anggaran
	    	and a.jasa_material = c.jasa_material
	    	and trim(lower(a.kode_biaya))=trim(lower(c.kode_biaya))
	    	and c.id_rab = ".$this->conn->escape($id_rab)."
	    	left join pr_pr_detail b on a.id_rab_detail = b.id_rab_detail and b.id_pr=".$this->conn->escape($id)." and b.id_jasa_material is null
	    	left join pr_pr_detail d on c.id_jasa_material = d.id_jasa_material and d.id_pr=".$this->conn->escape($id)."
	    	left join rab_rab_detail e on a.id_rab_detail_parent = e.id_rab_detail
	    	where a.id_rab = ".$this->conn->escape($id_rab)." and a.sumber_nilai in (1,3) and a.id_rab_detail_parent is not null) a 
	    	where vol > 0 $add_filter order by uraian ");

		$row = 2;
        foreach($rows as $r){
	    	$col = 'A';
	    	foreach($header as $r1){
           		$excelactive->setCellValue($col.$row,$r[$r1['name']]);
           		$col++;
	    	}
            $row++;
        }


	    $objWriter = Factory::createWriter($excel,'Excel2007');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->ctrl.date('Ymd').'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
	}
}