<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_realisasi extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_realisasilist";
		$this->viewdetail = "panelbackend/rab_realisasidetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";
		$this->data['width'] = "1200px";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah Realisasi';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit Realisasi';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail Realisasi';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Realisasi';
		}

		$this->load->model("Rab_realisasiModel","model");
		$this->load->model("Rab_realisasi_filesModel","modelfile");
		$this->load->model("Rab_rab_detailModel","rabrabdetail");
		$this->load->model("Rab_rabModel","rabrab");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");
		// $this->data['rabrabdetailarr'] = $this->rabrabdetail->GetCombo();


		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->data['mtposanggaranarr'] = $this->mtposanggaran->GetCombo();
		
		$this->load->model("Rab_jasa_materialModel","rabjasa_material");
		// $this->data['rabjasa_materialarr'] = $this->rabjasa_material->GetCombo();
		// 
		$this->data['configfile'] = $this->config->item('file_upload_config');
		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'select2','upload','datepicker'
		);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'id_jasa_material', 
				'label'=>'Scope', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['rabjasa_materialarr'],
			),
			array(
				'name'=>'id_rab_detail', 
				'label'=>'RAB Detail', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['rabrabdetailarr'],
			),
			array(
				'name'=>'nilai', 
				'label'=>'Nilai', 
				'width'=>"auto",
				'type'=>"number",
			),
			array(
				'name'=>'keterangan', 
				'label'=>'Keterangan', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
		);
	}

	protected function Record($id=null){
		$this->post['nilai_satuan'] = Rupiah2Number($this->post['nilai_satuan']);
		unset($this->post['nilai']);
		return array(
			'id_jasa_material'=>$this->post['id_jasa_material'],
			'id_rab_detail'=>$this->post['id_rab_detail'],
			'id_jabatan_proyek'=>$this->post['id_jabatan_proyek'],
			'nilai'=>(float)$this->post['vol']*(float)$this->post['nilai_satuan'],
			'nilai_satuan'=>$this->post['nilai_satuan'],
			'vol'=>$this->post['vol'],
			'satuan'=>$this->post['satuan'],
			'tgl'=>$this->post['tgl'],
			'keterangan'=>$this->post['keterangan'],
			'nama'=>$this->post['nama'],
		);
	}

	protected function Rules(){
		return array(
			/*"id_jasa_material"=>array(
				'field'=>'id_jasa_material', 
				'label'=>'Scope', 
				'rules'=>"in_list[".implode(",", array_keys($this->data['rabjasa_materialarr']))."]",
			),
			"id_rab_detail"=>array(
				'field'=>'id_rab_detail', 
				'label'=>'RAB Detail', 
				'rules'=>"required|in_list[".implode(",", array_keys($this->data['rabrabdetailarr']))."]",
			),*/
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"required",
			),
			"tgl"=>array(
				'field'=>'tgl', 
				'label'=>'Tgl', 
				'rules'=>"required",
			),
			"nilai"=>array(
				'field'=>'nilai', 
				'label'=>'Nilai', 
				'rules'=>"required",
			),
			"nilai_satuan"=>array(
				'field'=>'nilai_satuan', 
				'label'=>'Nilai Satuan', 
				'rules'=>"required",
			),
			"vol"=>array(
				'field'=>'vol', 
				'label'=>'Vol', 
				'rules'=>"required|integer",
			),
			"satuan"=>array(
				'field'=>'satuan', 
				'label'=>'Satuan', 
				'rules'=>"required",
			),
		/*	"file"=>array(
				'field'=>'file[id]', 
				'label'=>'File', 
				'rules'=>"required",
			),*/
			"keterangan"=>array(
				'field'=>'keterangan', 
				'label'=>'Keterangan', 
				'rules'=>"max_length[4000]",
			),
		);
	}

	protected function _afterDetail($id){
		if($this->data['rowheader3']['id_rab_detail_parent'])
			$this->data['id_rab_detail_parent'] = $this->data['rowheader3']['id_rab_detail_parent'];

		$this->data['breadcrumb'] = $this->rabrabdetail->GetComboParent($this->data['id_rab_detail_parent']);

		if(!$this->data['row']['file']['id'] && $id){
			$rows = $this->conn->GetArray("select id_realisasi_files as id, client_name as name
				from rab_realisasi_files
				where id_realisasi = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['file']['id'][] = $r['id'];
				$this->data['row']['file']['name'][] = $r['name'];
			}
		}

		$this->data['rabjasa_materialarr'][$this->data['row']['id_jasa_material']] = $this->conn->GetOne("select nama from rab_jasa_material where id_jasa_material = ".$this->conn->escape($this->data['row']['id_jasa_material']));



		if($this->data['rowheader3']['sumber_satuan']==2){
			$this->data['jabatanarr'] = $this->conn->GetList("select a.id_jabatan_proyek as key,  c.nama as val
					from rab_rab_detail_jabatan_proyek a
					join mt_jabatan_proyek c on c.id_jabatan_proyek = c.id_jabatan_proyek
					where exists (select 1 from rab_manpower b 
					where a.id_jabatan_proyek = b.id_jabatan_proyek 
					and b.id_rab = ".$this->conn->escape($this->data['id_rab']).")
					and id_rab_detail = ".$this->conn->escape($this->data['rowheader3']['id_rab_detail']));
		}
	}

	public function Index($id_rab_detail=0){

		$this->_beforeDetail($id_rab_detail);

		redirect("panelbackend/rab_rab_detail/detail/{$this->data['id_rab']}/$id_rab_detail");
	}

	public function Add($id_rab_detail=0, $id_jasa_material=null){
		$this->Edit($id_rab_detail,null,$id_jasa_material);
	}

	public function Edit($id_rab_detail=0,$id=null, $id_jasa_material=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_rab_detail,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if($id_jasa_material)
			$this->data['row']['id_jasa_material'] = $id_jasa_material;

		if (!$this->data['rowheader1'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_rab_detail'] = $id_rab_detail;
			$this->data['row']['id_jasa_material'] = $record['id_jasa_material'];

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {
					$this->data['row'] = $return['data'];

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				$this->ctrl = "rab_rab_detail";
				SetFlash('suc_msg', $return['success']);
				redirect("panelbackend/rab_rab_detail/detail/{$this->data['id_rab']}/$id_rab_detail");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_rab_detail=null, $id=null){

		$this->_beforeDetail($id_rab_detail, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader1'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Delete($id_rab_detail=null, $id=null){

        $this->model->conn->StartTrans();

        $this->_beforeDetail($id_rab_detail, $id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");
			$this->ctrl = "rab_rab_detail";
			SetFlash('suc_msg', $return['success']);
			redirect("panelbackend/rab_rab_detail/detail/{$this->data['id_rab']}/$id_rab_detail");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_rab_detail/$id");
		}

	}


	protected function _beforeDetail($id_rab_detail=null, $id=null){
		$this->data['id_rab_detail'] = $id_rab_detail;
		$this->data['rowheader3'] = $this->rabrabdetail->GetByPk($id_rab_detail);
		$this->data['id_rab'] = $id_rab = $this->data['rowheader3']['id_rab'];
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
		$this->data['id_pekerjaan'] = $id_pekerjaan = $this->data['rowheader2']['id_pekerjaan'];
		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_rab_detail;
		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

	/*	if($this->data['last_versi']<>$this->data['rowheader2']['id_rab_detail']){
			$this->access_role['add']=0;
			$this->access_role['edit']=0;
			$this->access_role['delete']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['upload_file']=0;
			$this->access_role['delete_file']=0;
			$this->access_role['save']=0;
		}*/
	}

	protected function _afterInsert($id=null){
		$ret = true;
		if($ret)
			$ret = $this->_afterUpdate($id);

		return $ret;
	}

	protected function _afterDelete($id=null){
		$ret = true;
		if($ret)
			$ret = $this->_afterUpdate($id);

		return $ret;
	}

	protected function _afterUpdate($id){
		$ret = true;
		
		if($ret)
			$ret = $this->_delsertFiles($id);

		if($ret && $this->data['row']['id_jasa_material']){
			$realisasi = (float) $this->conn->GetOne("select sum(nvl(nilai,0)) from rab_realisasi where id_jasa_material = ".$this->conn->escape($this->data['row']['id_jasa_material']));

			$ret = $this->conn->Execute("update rab_jasa_material set nilai_realisasi = $realisasi where id_jasa_material = ".$this->conn->escape($this->data['row']['id_jasa_material']));
		}

		if($ret){
			$realisasi = (float) $this->conn->GetOne("select sum(nvl(nilai,0)) from rab_realisasi where id_rab_detail = ".$this->conn->escape($this->data['id_rab_detail']));

			$ret = $this->conn->Execute("update rab_rab_detail set nilai_realisasi = $realisasi where id_rab_detail = ".$this->conn->escape($this->data['id_rab_detail']));
		}

		if($ret)
			$ret = $this->_hitungRealisasiParent($this->data['rowheader3']['id_rab_detail_parent']);
		
		return $ret;
	}

	private function _delsertFiles($id_realisasi = null){
		$ret = true;

		if(count($this->post['file'])){
			foreach($this->post['file']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_realisasi'=>$id_realisasi), $v);
			}
		}
		return $ret;
	}

}