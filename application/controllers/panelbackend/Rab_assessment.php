<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Rab_assessment extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/rab_assessmentlist";
		$this->viewdetail = "panelbackend/rab_assessmentdetail";
		$this->template = "panelbackend/main";

		if ($this->mode == 'add') {
			$this->layout = "panelbackend/layout_rab";
			$this->data['width'] = "1200px";
			$this->data['page_title'] = 'Tambah Pekerjaan';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->layout = "panelbackend/layout_rab";
			$this->data['width'] = "1200px";
			$this->data['page_title'] = 'Edit Pekerjaan';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->layout = "panelbackend/layout_rab";
			$this->data['width'] = "1200px";
			$this->data['page_title'] = 'Detail Pekerjaan';
			$this->data['edited'] = false;	
		}else{
			$this->layout = "panelbackend/layout_rab";
			$this->data['page_title'] = 'Daftar Pekerjaan';
		}

		// $this->data['no_header'] = true;

		$this->load->model("Rab_pekerjaanModel","model");
		$this->load->model("Rab_proyekModel","proyek");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Rab_pekerjaan_filesModel","modelfile");
		$this->load->model("Rab_proyek_filesModel","modelfile1");
		
		$this->load->model("Mt_customerModel","mtcustomer");
		$this->data['mtcustomerarr'] = $this->mtcustomer->GetCombo();
		$this->data['configfile'] = $this->config->item('file_upload_config');

		
		$this->load->model("Mt_tipe_pekerjaanModel","mttipepekerjaan");
		$this->data['mttipepekerjaanarr'] = $this->mttipepekerjaan->GetCombo();
		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'datepicker','select2','upload'
		);
	}
	public function Index($id_proyek=null, $id_pekerjaan=null){
		redirect("panelbackend/rab_assessment/detail/$id_proyek/$id_pekerjaan");
	}

	public function Add($id_proyek=0){
		$this->Edit($id_proyek);
	}

	public function Edit($id_proyek=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_proyek,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_proyek'] = $id_proyek;

			$this->_isValid($record,true);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_proyek/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_proyek=null, $id=null){

		$this->_beforeDetail($id_proyek, $id);

		unset($this->access_role['index']);
		unset($this->access_role['list']);
		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}


	protected function _beforeDetail($id_proyek=null, $id=null){
		$this->data['id_proyek'] = $id_proyek;
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		if($id)
			$this->data['add_param'] .= $id_proyek;
	}

	protected function _afterDetail($id){
		if($id){
			$rows = $this->conn->GetArray("select jenis_file, id_pekerjaan_files as id, client_name as name, created_date, created_by_desc
				from rab_pekerjaan_files
				where id_pekerjaan = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek'].'___'.$id]['id'][] = $r['id'];
				$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek'].'___'.$id]['name'][] = $r['name'];
				$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek']]['created_date'][] = $r['created_date'];
				$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek']]['created_by_desc'][] = $r['created_by_desc'];
			}
		}


		$rows = $this->conn->GetArray("select jenis_file, id_proyek_files as id, client_name as name, created_date, created_by_desc
			from rab_proyek_files
			where id_proyek = ".$this->conn->escape($this->data['id_proyek']));

		foreach($rows as $r){
			$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek']]['id'][] = $r['id'];
			$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek']]['name'][] = $r['name'];
			$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek']]['created_date'][] = $r['created_date'];
			$this->data['row'][$r['jenis_file'].'___'.$this->data['id_proyek']]['created_by_desc'][] = $r['created_by_desc'];
		}

		if($id){

			$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id));
			$this->data['id_rab'] = $id_rab = $this->data['last_versi'] = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id));
			$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);
			$this->data['rowheader1'] = $this->data['row'];
		}
	}

	protected function _uploadFiles($jenis_file1=null, $id=null){

		$name = $_FILES[$jenis_file1]['name'];

		list($jenis_file, $id_proyek, $id_pekerjaan) = explode("___",str_replace("upload","",$jenis_file1));

		if(!$id_pekerjaan)
			$this->modelfile = $this->modelfile1;

		$this->data['configfile']['file_name'] = $jenis_file.time().$name;

		$this->load->library('upload', $this->data['configfile']);

        if ( ! $this->upload->do_upload($jenis_file1))
        {
            $return = array('error' => "File $name gagal upload, ".strtolower(str_replace(array("<p>","</p>"),"",$this->upload->display_errors())));
        }
        else
        {
    		$upload_data = $this->upload->data();

			$record = array();
			$record['client_name'] = $upload_data['client_name'];
			$record['file_name'] = $upload_data['file_name'];
			$record['file_type'] = $upload_data['file_type'];
			$record['file_size'] = str_replace('.', ',', $upload_data['file_size']);
			$record['id_pekerjaan'] = $id_pekerjaan;
			$record['id_proyek'] = $id_proyek;
			$record['jenis_file'] = str_replace("upload","",$jenis_file);
			$ret = $this->modelfile->Insert($record);
			if($ret['success'])
			{
				$return = array('file'=>array("id"=>$ret['data'][$this->modelfile->pk],"name"=>$upload_data['client_name']));
			}else{
				unlink($upload_data['full_path']);
				$return = array('errors'=>"File $name gagal upload (gagal input)");
			}

        }

        return $return;

	}

	function open_file($id=null, $id1=null){
		$this->_openFiles($id, $id1);
	}

	protected function _openFiles($id=null, $id1=null){
		if($id1)
			$row = $this->modelfile->GetByPk($id1);
		else
			$row = $this->modelfile1->GetByPk($id);

		if($row ){
			$full_path = $this->data['configfile']['upload_path'].$row['file_name'];
			header("Content-Type: {$row['file_type']}");
			header("Content-Disposition: inline; filename='".str_replace(" ","_",basename($row['client_name']))."'");
			echo file_get_contents($full_path);
			die();
		}else{
			$this->Error404();
		}
	}

	function delete_file($id=null){
		$ret = $this->_deleteFiles($this->post['id'], $id);
		
		echo json_encode($ret);
	}

	protected function _deleteFiles($id=null, $id1=null){
		
		if($id1)
			$row = $this->modelfile->GetByPk($id);
		else
			$row = $this->modelfile1->GetByPk($id);

		if(!$row)
			$this->Error404();

		$file_name = $row['file_name'];

		$return = $this->modelfile->Delete($this->modelfile->pk." = ".$this->conn->escape($id));

		if ($return) {
			$full_path = $this->data['configfile']['upload_path'].$file_name;
			@unlink($full_path);

			return array("success"=>true);
		}else{
			return array("error"=>"File ".$row['client_name']." gagal dihapus");
		}
	}
}