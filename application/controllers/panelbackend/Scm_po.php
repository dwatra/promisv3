<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Scm_po extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/scm_polist";
		$this->viewdetail = "panelbackend/scm_podetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah PO';
			$this->data['edited'] = true;
			$this->layout = "panelbackend/layout_po";
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit PO';
			$this->data['edited'] = true;	
			$this->layout = "panelbackend/layout_po";
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail PO';
			$this->data['edited'] = false;	
			$this->layout = "panelbackend/layout_po";
		}else{
			$this->data['page_title'] = 'Daftar PO';
		}

		$this->data['width'] = "2000px";

		$this->load->model("Scm_poModel","model");
		$this->load->model("Scm_po_filesModel","modelfile");
		$this->data['configfile'] = $this->config->item('file_upload_config');
		//1:PO, 2:BAST, 3: Penilaian oleh user, 4: Disetujui pimpinan User 5:Penilaian oleh pengadaan, 6:Sudah dinilai
		$this->data['statusarr'] = array(
			''=>"",
			'1'=>"PO",
			'2'=>"BAST",
			'3'=>"Penilaian oleh user",
			'4'=>"Disetujui pimpinan User",
			'5'=>"Penilaian oleh pengadaan",
			'6'=>"Done"
		);
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'datepicker','upload','select2'
		);
	}
	public function Index($page=0){
		$this->data['header']=$this->Header();

		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;
		
		$this->data['limit_arr']=$this->limit_arr;



		$id_scm_poarr = array();
		foreach ($this->data['list']['rows'] as $r) {
			$id_scm_poarr[] = $r['id_scm_po'];
		}
		$id_scm_po_str = "'".implode("','", $id_scm_poarr)."'";

		$picpoarr = array();
		$rows = $this->conn->GetArray("select h.id_scm_po, p.nama 
			from scm_po_pic h 
			join mt_pic p on h.id_pic = p.nid 
			where h.id_scm_po in ($id_scm_po_str)");

		foreach ($rows as $r) {
			$picpoarr[$r['id_scm_po']][] = $r['nama'];
		}

		$this->data['picpo'] = array();
		foreach ($picpoarr as $k=>$v) {
			$this->data['picpo'][$k] = implode(",", $v);
		}


		$this->View($this->viewlist);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nomor', 
				'label'=>'Nomor', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_kontrak', 
				'label'=>'Tgl. Kontrak', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'picpo', 
				'label'=>'PIC', 
				'type'=>"implodelist",
			),
			array(
				'name'=>'nilai', 
				'label'=>'Nilai Kontrak', 
				'width'=>"auto",
				'type'=>"numeric",
			),
			array(
				'name'=>'id_status', 
				'label'=>'Status', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['statusarr']
			),
		);
	}

	protected function Record($id=null){
		list($id_suplier, $id_dpt) = explode("_",$this->post['id_suplier']);
		return array(
			'id_pic'=>$this->post['id_pic'],
			'tgl_kontrak'=>$this->post['tgl_kontrak'],
			'tgl_aktual_minta_no_kontrak'=>$this->post['tgl_aktual_minta_no_kontrak'],
			'tgl_aktual_kontrak_ttd'=>$this->post['tgl_aktual_kontrak_ttd'],
			'nilai'=>Rupiah2Number($this->post['nilai']),
			'levering_jumlah_hari'=>$this->post['levering_jumlah_hari'],
			'levering_tgl_terakhir'=>$this->post['levering_tgl_terakhir'],
			'nomor'=>$this->post['nomor'],
			'levering_tgl_mulai'=>$this->post['levering_tgl_mulai'],
			'nama'=>$this->post['nama'],
			'no_po'=>$this->post['no_po'],
			'id_suplier'=>$id_suplier,
			'id_dpt'=>$id_dpt,
		);
	}

	public function Edit($id=null){
		if($this->post['act']=='save_file'){
			$this->_uploadFile($id);
		}

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$this->_isValid($record,false);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_setGo($id);

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/edit/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _onDetail($id=null){
		$id_suplierarr = array();
		$id_dptarr = array();

		$suplierarr = $this->conn->GetArray("select distinct a.* 
			from scm_rks_peserta a
			left join scm_pr_detail b on a.id_scm_rks = b.id_scm_rks
			where b.id_scm_po = '$id'");

		if(!empty($suplierarr))
			foreach ($suplierarr as $key => $value) {
				if($value['id_dpt'])
					$id_dptarr[]=$value['id_dpt'];
				else
					$id_suplierarr[]=$value['id_suplier'];
		}

		if($this->data['row']['id_dpt'])
			$id_dptarr[] = $this->data['row']['id_dpt'];
		elseif($this->data['row']['id_suplier'])
			$id_suplierarr[] = $this->data['row']['id_suplier'];



		if(!empty($id_dptarr)){
			$id_dptarr = array_unique($id_dptarr);
			$id_dptstr = implode(",", $id_dptarr);
			$dptarr = $this->conn->GetArray("
				select to_char(s.id_suplier)||'_'||to_char(d.id_dpt) as id, 
				s.nama||' (dpt) '||d.nama as text
				from mt_suplier s 
				left join mt_suplier_dpt d on s.id_suplier = d.id_suplier 
				where d.id_dpt in ($id_dptstr)");

			if(!empty($dptarr))
				foreach ($dptarr as $r) {
				$this->data['suplierarr'][$r['id']] = $r['text'];
			}
		}

		if(!empty($id_suplierarr)){
			$id_suplierarr = array_unique($id_suplierarr);
			$id_suplierstr = implode(",", $id_suplierarr);
			$suplierarr = $this->conn->GetArray("
				select s.id_suplier as id, 
				s.nama as text
				from mt_suplier s 
				left join mt_suplier_dpt d on s.id_suplier = d.id_suplier 
				where s.id_suplier in ($id_suplierstr)");

			if(!empty($suplierarr))
				foreach ($suplierarr as $r) {
				$this->data['suplierarr'][$r['id']] = $r['text'];
			}
		}



		if($this->data['row']['id_dpt'])
			$this->data['row']['id_suplier'] .= '_'.$this->data['row']['id_dpt'];
	}

	protected function Rules(){
		return array(
			"nilai"=>array(
				'field'=>'nilai', 
				'label'=>'Nilai', 
				'rules'=>"numeric|max_length[22]",
			),
			"levering_jumlah_hari"=>array(
				'field'=>'levering_jumlah_hari', 
				'label'=>'Levering Jumlah Hari', 
				'rules'=>"integer|max_length[10]",
			),
			"nomor"=>array(
				'field'=>'nomor', 
				'label'=>'Nomor', 
				'rules'=>"is_unique[SCM_PO.NOMOR]|max_length[45]",
			),
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama', 
				'rules'=>"max_length[2000]",
			),
		);
	}
	
	protected function _afterDetail($id=null){
		$this->data['rowheader'] = $this->data['row'];

		if($this->modelfile){
			if(!$this->data['row']['files']['id'] && $id){
				$rows = $this->modelfile->GArray("*"," where ".$this->pk."=".$this->conn->escape($id));

				foreach($rows as $r){
					$this->data['row']['files']['id'][] = $r[$this->modelfile->pk];
					$this->data['row']['files']['name'][] = $r['client_name'];
				}
			}
		}

		if(empty($this->data['row']['id_pic'])){
			$id_picarr = array();

			$picarr = $this->conn->GetArray("select id_pic from scm_po_pic where id_scm_po = '$id'");

			foreach ($picarr as $key => $value) {
				$id_picarr[]=$value['id_pic'];
			}

			$this->data['row']['id_pic'] = $id_picarr;

			$id_picstr = implode(",", $id_picarr);

		}

		if(!empty($this->data['row']['id_pic'])){

			$id_picarr = $this->data['row']['id_pic'];
			$id_picstr = "'".implode("','", $id_picarr)."'";

			$picarr = $this->conn->GetArray("select * from mt_pic where nid in ($id_picstr)");
			foreach ($picarr as $r) {
				$this->data['picarr'][$r['nid']] = $r['nama'];
			}
		}
	}

	protected function _beforeDelete($id){

		$return = $this->model->Execute("delete from scm_po_pic where id_scm_po = '$id'");

		return $return;
	}

	protected function _beforeDetail($id=null){
		if($this->post['act']=='pullellipse'){
			$this->data['is_pull_ellipse'] = true;
			$this->post['act']='save';
		}
	}

	protected function _afterInsert($id=null){
		return $this->_afterUpdate($id);
	}

	private function _updateNama($id_scm_po=null){
		$nama = $this->conn->GetListStr("select pr.nama as val from scm_pr pr 
			join scm_pr_detail prd on pr.id_scm_pr = prd.id_scm_pr
			where prd.id_scm_po = ".$this->conn->escape($id_scm_po));

		if($nama)
			return $this->conn->goUpdate("scm_po",array("nama"=>$nama),"id_scm_po = ".$this->conn->escape($id_scm_po));

		return true;
	}

	protected function _afterUpdate($id=null){
		$ret = true;
		if($this->post['no_po'] && $this->data['is_pull_ellipse']){
			$ret = $this->conn->Execute("merge into scm_pr_detail t1 
				using view_pr_po@ellipse t2 on (t1.no_pr = t2.preq_no and t1.no_pr_item = t2.preq_item_no)
				when matched then update set t1.no_po = t2.po_no, t1.nilai_satuan_po = t2.nilai_po/nvl(t1.vol,1), t1.id_scm_po = ".$this->conn->escape($id)."
				where t2.po_no = ".$this->conn->escape($this->post['no_po']));

			if($ret){
				$row = $this->conn->GetRow("select order_date, due_date, supplier_name, po_no, sum(nilai_po) as nilai
					from view_pr_po@ellipse 
					where po_no = ".$this->conn->escape($this->post['no_po'])." 
					group by order_date, due_date, supplier_name, po_no");
				$record = array();
				$record['tgl_kontrak'] = $row['order_date'];
				$record['levering_tgl_mulai'] = $row['order_date'];
				$record['levering_tgl_terakhir'] = $row['due_date'];
				$record['nilai'] = $row['nilai'];
				$record['no_po'] = $row['no_po'];
				$record['levering_jumlah_hari'] = daydiff($row['due_date'], $row['order_date']);

				$id_suplierarr = array_keys($this->data['suplierarr']);
				$id_suplierstr = implode(",", $id_suplierarr);
				$id_suplier = $this->conn->GetOne("select id_suplier 
					from mt_suplier 
					where lower(nama) like '%".strtolower($row['supplier_name'])."%'");
				$record['id_suplier'] =  $id_suplier;	

				$ret = $this->conn->goUpdate("scm_po", $record, "id_scm_po = ".$this->conn->escape($id));		
			}
		}

		if($ret)
			$ret = $this->_updateNama($id);

		if($ret)
			$ret = $this->_delSertPic($id);


		$this->update_po_promis($id);
				
		return $ret;
	}

	private function _delSertPic($id){
		$return = $this->conn->Execute("delete from scm_po_pic where id_scm_po = '$id'");

		if(is_array($this->post['id_pic'])){
			foreach ($this->post['id_pic'] as $key => $value) {
				if($return){
					if(!$value)
						continue;

					$record = array();
					$record['id_scm_po'] = $id;
					$record['id_pic'] = $value;
			        $sql = $this->conn->InsertSQL("scm_po_pic", $record);
			        
			        if($sql){
					    $return = $this->conn->Execute($sql);
					}
				}
			}
		}

		return $return;
	}

	protected function _afterEditSucceed($id=null){
		$this->reset_count_down("pd.id_scm_po = ".$this->conn->escape($id));
	}
}