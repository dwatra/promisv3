<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Pr_po extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/pr_polist";
		$this->viewdetail = "panelbackend/pr_podetail";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout1";

		if ($this->mode == 'add') {
			$this->data['page_title'] = 'Tambah PO';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['page_title'] = 'Edit Pengadaan';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['page_title'] = 'Detail PO';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Monitoring PR';
		}

		$this->load->model("Pr_poModel","model");
		$this->load->model("Pr_prModel","pr");
		$this->load->model("Pr_po_filesModel","modelfile");
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'datepicker','upload'
		);
		$this->data['configfile'] = $this->config->item('file_upload_config');
		$this->data['width'] = "1800px";
		$this->data['statusarr'] = array(
        	""=>"",
        	"1"=>"Review Perencana Pengadaan",
        	"2"=>"Perhitungan HPE",
        	"8"=>"Perhitungan HPE Selesai",
        	"3"=>"Proses Pengadaan",
        	"4"=>"PO",
        	"5"=>"Proyek Berjalan",
        	"6"=>"BAST",
        	"7"=>"Receive",
        	"98"=>"Kembali ke user",
        	"99"=>"Cancel",
        );

		unset($this->access_role['add']);
	}

	protected function Header(){
		return array(
			array(
				'name'=>'tgl_permintaan', 
				'label'=>'Tgl. Permintaan', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'no_desc', 
				'label'=>'Nomor PR', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'no_prk', 
				'label'=>'No. WO/PRK', 
				'width'=>"auto",
				'type'=>"varchar2",
				'field'=>'c.no_prk'
			),
			array(
				'name'=>'nama', 
				'label'=>'Nama Jasa/Barang', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nama_proyek', 
				'label'=>'Nama Anggaran', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'tgl_dibutuhkan', 
				'label'=>'Tgl. Dibutuhkan', 
				'width'=>"auto",
				'type'=>"date",
			),
			array(
				'name'=>'no_spk', 
				'label'=>'NO SPK', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'no_spmk', 
				'label'=>'NO SPMK', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			array(
				'name'=>'nilai_anggaran', 
				'label'=>'Nilai Anggaran', 
				'width'=>"auto",
				'type'=>"number",
			),
			array(
				'name'=>'id_status', 
				'label'=>'Status', 
				'width'=>"auto",
				'type'=>"list",
				'value'=>$this->data['statusarr']
			),
		);
	}

	protected function Record($id=null){
		return array(
			'no_spk'=>$this->post['no_spk'],
			'nama_suplier'=>$this->post['nama_suplier'],
			'tgl_spk'=>$this->post['tgl_spk'],
			'tgl_spmk'=>$this->post['tgl_spmk'],
			'no_spmk'=>$this->post['no_spmk'],
			'no_bapp'=>$this->post['no_bapp'],
			'no_bapb'=>$this->post['no_bapb'],
			'tgl_mulai'=>$this->post['tgl_mulai'],
			'tgl_selesai'=>$this->post['tgl_selesai'],
			'tgl_real_selesai'=>$this->post['tgl_real_selesai'],
		);
	}

	protected function Rules(){
		return array(
			"no_spk"=>array(
				'field'=>'no_spk', 
				'label'=>'NO SPK', 
				'rules'=>"required|max_length[20]",
			),
			"no_spmk"=>array(
				'field'=>'no_spmk', 
				'label'=>'NO Spmk', 
				'rules'=>"required|max_length[20]",
			),
			"tgl_mulai"=>array(
				'field'=>'tgl_mulai', 
				'label'=>'Tgl. Mulai', 
				'rules'=>"required",
			),
			"tgl_spmk"=>array(
				'field'=>'tgl_spmk', 
				'label'=>'Tgl. SPMK', 
				'rules'=>"required",
			),
			"tgl_selesai"=>array(
				'field'=>'tgl_selesai', 
				'label'=>'Tgl. Selesai', 
				'rules'=>"required",
			),
		);
	}

	public function Detail($id_pr=null, $id=null){

		$this->data['id_pr'] = $id_pr;

		$this->_beforeDetail($id_pr, $id);

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['row'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _beforeDetail($id_pr=null, $id=null){
		$this->data['rowheader'] = $this->pr->GetByPk($id_pr);
		$this->load->model("Rab_rabModel","mrab");
		$this->data['rab'] = $this->mrab->GetByPk($this->data['rowheader']['id_rab']);

		$this->load->model("Rab_pekerjaanModel","mpekerjaan");
		$this->data['pekerjaan'] = $this->mpekerjaan->GetByPk($this->data['rab']['id_pekerjaan']);

		$this->load->model("Rab_proyekModel","mproyek");
		$this->data['proyek'] = $this->mproyek->GetByPk($this->data['pekerjaan']['id_proyek']);
		$this->data['add_param'] .= $id_pr;

		unset($this->access_role['index']);
		unset($this->access_role['lst']);
		unset($this->access_role['list']);
	}

	public function Edit($id_pr=null, $id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->data['id_pr'] = $id_pr;

		$this->_beforeDetail($id_pr, $id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if($this->post && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {
			$this->_isValid($record,true);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->model->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_pr/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _afterDetail($id=null){
		$add_filter = "";
		$this->data['rows'] = $this->conn->GetArray("select
			a.*, nvl(b.nama, c.uraian) as uraian, nvl(b.satuan, c.satuan) as satuan, d.no_desc, d.nama as nama_anggaran
			from pr_pr_detail a
			left join rab_jasa_material b on a.id_jasa_material = b.id_jasa_material
			left join rab_rab_detail c on a.id_rab_detail = c.id_rab_detail
			left join pr_pr d on a.id_pr = d.id_pr
			where (a.id_pr = ".$this->conn->escape($this->data['id_pr'])." or a.id_po = ".$this->conn->escape($id).") and (a.id_po is null or a.id_po = ".$this->conn->escape($id).")
			order by uraian");

		if(!$this->data['row']['spk']['id'] && $id){
			$rows = $this->conn->GetArray("select id_po_files as id, client_name as name
				from pr_po_files
				where jenis_file = 'spk' and id_po = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['spk']['id'][] = $r['id'];
				$this->data['row']['spk']['name'][] = $r['name'];
			}
		}

		if(!$this->data['row']['spmk']['id'] && $id){
			$rows = $this->conn->GetArray("select id_po_files as id, client_name as name
				from pr_po_files
				where jenis_file = 'spmk' and id_po = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['spmk']['id'][] = $r['id'];
				$this->data['row']['spmk']['name'][] = $r['name'];
			}
		}

		if(!$this->data['row']['bapp']['id'] && $id){
			$rows = $this->conn->GetArray("select id_po_files as id, client_name as name
				from pr_po_files
				where jenis_file = 'bapp' and id_po = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['bapp']['id'][] = $r['id'];
				$this->data['row']['bapp']['name'][] = $r['name'];
			}
		}

		if(!$this->data['row']['bapb']['id'] && $id){
			$rows = $this->conn->GetArray("select id_po_files as id, client_name as name
				from pr_po_files
				where jenis_file = 'bapb' and id_po = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['bapb']['id'][] = $r['id'];
				$this->data['row']['bapb']['name'][] = $r['name'];
			}
		}

		if($this->post['act']=='spmk')
			$this->spmk();
	}

	private function spmk(){
		$rab = $this->data['rab'];
		$pekerjaan = $this->data['pekerjaan'];
		$proyek = $this->data['proyek'];

		$this->data['configfile'] = $this->config->item('file_upload_config');
		$this->load->library("word");
		$word = $this->word;

		$row = $this->conn->GetRow("select file_name from mt_template_doc_files where id_template_doc = 4");
		$temp = $this->data['configfile']['upload_path'].$row['file_name'];
		
		if(!file_exists($temp) or !$row)
			$this->Error404();

		$word->template($temp);
		$template = $word->templateProcessor;
		$phpword = $word->phpword();
		$writer = \PhpOffice\PhpWord\IOFactory::createWriter($phpword, 'Word2007');
		$section = $phpword->addSection();

		$template->setValue("no_spmk", $this->data['row']['no_spmk']);
		$template->setValue("tgl_spmk", Eng2Ind($this->data['row']['tgl_spmk']));
		$template->setValue("nama_suplier", $this->data['row']['nama_suplier']);
		$template->setValue("nama_proyek", $proyek['nama_proyek']);
		$template->setValue("nama_pm", $proyek['nama_pic']);
		$template->setValue("no_hp_pm", $proyek['no_hp_pm']);
		$template->setValue("nama_rendal_proyek", $proyek['nama_rendal_proyek']);
		$template->setValue("no_hp_rendal_proyek", $proyek['no_hp_rendal_proyek']);


		$table = 'pekerjaan';
		$cellHCentered = array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER,'spaceAfter'=>0);
		$tablestyle = array('cellMarginTop' => 50, 'cellMarginBottom' => 50, 'cellMarginLeft' => 50, 'cellMarginRight' => 50);
		$cellBCentered = array('spaceAfter'=>0);
		$header = array('borderSize' => 6, 'borderColor' => '000000');
		$textheader = array('bold'=>true,'size'=>9, 'name'=>'Arial');
		$textbody = array('size'=>9, 'name'=>'Arial');
		$cneter = array('valign' => 'center');
		$table = $section->addTable($tablestyle);

		$row = $table->addRow();
		$cell = $row->addCell(2700, $header+$cneter)->addText("NAMA PEKERJAAN",$textheader,$cellHCentered);
		$cell = $row->addCell(2100, $header+$cneter)->addText("NOMOR SPK/PENUNJUKAN",$textheader,$cellHCentered);
		$cell = $row->addCell(2100, $header+$cneter)->addText("TANGGAL SPK/PENUNJUKAN",$textheader,$cellHCentered);
		$cell = $row->addCell(2550, $header+$cneter)->addText("TANGGAL MULAI KERJA",$textheader,$cellHCentered);

		foreach($this->data['rows'] as $i=>$r){
			if(!$r['id_po'])
				continue;

			$row = $table->addRow();
			$row->addCell(2700, $header+$cneter)->addText($r['uraian'],$textbody,$cellHCentered);
			if($i==0){
				$row->addCell(2100, $header+$cneter+array('vMerge' => 'restart'))->addText($this->data['row']['no_spk'],$textbody,$cellHCentered);
				$row->addCell(2100, $header+$cneter+array('vMerge' => 'restart'))->addText(Eng2Ind($this->data['row']['tgl_spk']),$textbody,$cellHCentered);
			}else{
				$row->addCell(2100, $header+array('vMerge' => 'continue'));
				$row->addCell(2100, $header+array('vMerge' => 'continue'));
			}
			$row->addCell(2550, $header+$cneter)->addText(Eng2Ind($r['tgl_mulai_kerja']),$textbody,$cellHCentered);
		}

		$tabletext = $writer->getWriterPart('document')->getTableAsText($table);
		$template->setElement('table_pekerjaan', $tabletext);

		$template->setValue("nama_pekerjaan", $pekerjaan['nama_pekerjaan']);
		$template->setValue("no_spk", $this->data['row']['no_spk']);
		$template->setValue("tgl_spk", $this->data['row']['tgl_spk']);
		$template->setValue("tgl_mulai", Eng2Ind($this->data['row']['tgl_mulai']));

		$word->download('spmk '.$this->data['row']['no_spmk'].'.docx');
		exit();
	}

	protected function _afterUpdate($id){
		return $this->_afterInsert($id);
	}

	protected function _afterInsert($id){
		$ret = true;

		if($ret)
			$ret = $this->upsertPr($id);
		
		if($ret)
			$ret = $this->_delsertFiles($id);

		return $ret;
	}

	private function upsertPr($id){
		$this->conn->debug = 1;
		$ret = $this->conn->Execute("update pr_pr_detail set id_po = null where id_po = ".$this->conn->escape($id));

		if($ret && $this->post['id_pr_detail']){
			foreach($this->post['id_pr_detail'] as $k=>$v){
				if(!$ret)
					break;

				$tgl_mulai_kerja = $this->post['tgl_mulai_kerja'][$k];

				if($tgl_mulai_kerja)
					$rec = ", tgl_mulai_kerja = ".$this->conn->escape($tgl_mulai_kerja);

				$ret = $this->conn->Execute("update pr_pr_detail set id_po = ".$this->conn->escape($id)." $rec where id_pr_detail = ".$this->conn->escape($v));
			}
		}

		return $ret;
	}

	private function _delsertFiles($id_po = null){
		$ret = true;

		if(count($this->post['spk'])){
			foreach($this->post['spk']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_po'=>$id_po), $v);
			}
		}

		if(count($this->post['spmk'])){
			foreach($this->post['spmk']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_po'=>$id_po), $v);
			}
		}

		if(count($this->post['bapb'])){
			foreach($this->post['bapb']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_po'=>$id_po), $v);
			}
		}

		if(count($this->post['bapp'])){
			foreach($this->post['bapp']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_po'=>$id_po), $v);
			}
		}
		return $ret;
	}

}