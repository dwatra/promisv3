<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH."core/_adminController.php";
class Wbs extends _adminController{

	public function __construct(){
		parent::__construct();
	}
	protected function init(){
		parent::init();
		$this->viewlist = "panelbackend/wbs_planlist";
		$this->viewdetail = "panelbackend/wbs_plandetail";
		$this->viewcetak = "panelbackend/wbs_cetak";
		$this->template = "panelbackend/main";
		$this->layout = "panelbackend/layout_rab";

		if ($this->mode == 'add') {
			$this->data['width'] = "1200px";
			$this->data['page_title'] = 'Tambah Rencana';
			$this->data['edited'] = true;
		}
		elseif ($this->mode == 'edit') {
			$this->data['width'] = "1200px";
			$this->data['page_title'] = 'Edit Rencana';
			$this->data['edited'] = true;	
		}
		elseif ($this->mode == 'detail'){
			$this->data['width'] = "1200px";
			$this->data['page_title'] = 'Detail Rencana';
			$this->data['edited'] = false;	
		}else{
			$this->data['page_title'] = 'Daftar Rencana';
		}

		// $this->data['no_header'] = true;

		$this->load->model("Wbs_planModel","model");
		$this->load->model("Rab_rabModel","rabrab");		
		$this->load->model("Mt_pos_anggaranModel","mtposanggaran");
		$this->load->model("Rab_pekerjaanModel","rabpekerjaan");
		$this->load->model("Rab_proyekModel","proyek");

		
		$this->pk = $this->model->pk;
		$this->data['pk'] = $this->pk;
		$this->plugin_arr = array(
			'datepicker','select2','upload'
		);

		$this->access_role = array('add'=>1, 'edit'=>1, 'delete'=>1, 'detail'=>1, 'list'=>1, 'reset'=>1, 'save'=>1, 'batal'=>1);
	}

	function Index($id_pekerjaan=0, $page=0){
		$this->_beforeDetail($id_pekerjaan);
		$this->data['header']=$this->Header();
		//$this->_setFilter("id_plan = ".$this->conn->escape($id_plan));
		$this->data['list']=$this->_getList($page);

		$this->data['page']=$page;

		$param_paging = array(
			'base_url'=>base_url("{$this->page_ctrl}/index/$id_pekerjaan"),
			'cur_page'=>$page,
			'total_rows'=>$this->data['list']['total'],
			'per_page'=>$this->limit,
			'first_tag_open'=>'<li>',
			'first_tag_close'=>'</li>',
			'last_tag_open'=>'<li>',
			'last_tag_close'=>'</li>',
			'cur_tag_open'=>'<li class="active"><a href="#">',
			'cur_tag_close'=>'</a></li>',
			'next_tag_open'=>'<li>',
			'next_tag_close'=>'</li>',
			'prev_tag_open'=>'<li>',
			'prev_tag_close'=>'</li>',
			'num_tag_open'=>'<li>',
			'num_tag_close'=>'</li>',
			'anchor_class'=>'pagination__page',

		);
		$this->load->library('pagination');

		$paging = $this->pagination;

		$paging->initialize($param_paging);

		$this->data['paging']=$paging->create_links();

		$this->data['limit']=$this->limit;

		$this->data['limit_arr']=$this->limit_arr;

		$this->View($this->viewlist);
	}

	function readMPP($file) {
		require_once("http://localhost:8081/javabridge/java/Java.inc");

		$project = new java('javaapplication1.JavaApplication1');
		$ret = java_values($project->importMPP($file));

		$arr = json_decode($ret, true);
		ksort($arr);

		return $arr;

	}


	protected function Header(){
		return array(
			array(
				'name'=>'nama', 
				'label'=>'Nama Rencana', 
				'width'=>"auto",
				'type'=>"varchar2",
			),
			// array(
			// 	'name'=>'no_prk', 
			// 	'label'=>'No. PRK', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'id_tipe_pekerjaan', 
			// 	'label'=>'Tipe Pekerjaan', 
			// 	'width'=>"auto",
			// 	'type'=>"list",
			// 	'value'=>$this->data['mttipepekerjaanarr'],
			// ),
			// array(
			// 	'name'=>'no_pekerjaan', 
			// 	'label'=>'No. SP3', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'tgl_pekerjaan', 
			// 	'label'=>'Tgl. SP3', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'no_kontrak', 
			// 	'label'=>'No. Kontrak', 
			// 	'width'=>"auto",
			// 	'type'=>"varchar2",
			// ),
			// array(
			// 	'name'=>'tgl_kontrak', 
			// 	'label'=>'Tgl. Kontrak', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'nilai_hpp', 
			// 	'label'=>'Nilai HPP', 
			// 	'width'=>"auto",
			// 	'type'=>"number",
			// ),
			// array(
			// 	'name'=>'tgl_mulai_rab', 
			// 	'label'=>'Pengerjaan RAB', 
			// 	'width'=>"auto",
			// 	'type'=>"date",
			// ),
			// array(
			// 	'name'=>'durasi', 
			// 	'label'=>'Durasi', 
			// 	'width'=>"auto",
			// 	'type'=>"polos",
			// ),
		);
	}

	protected function Record($id=null){
		$return = array(
			'nama'=>$this->post['nama'],
		);

		// $pic = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape(trim($return['id_pic'])));

		// $return['nama_pic'] = $pic['nama'];
		// $return['jabatan_pic'] = $pic['jabatan'];

		return $return;
	}

	protected function Rules(){
		return array(
			"nama"=>array(
				'field'=>'nama', 
				'label'=>'Nama Rencana', 
				'rules'=>"required",
			),
		);
	}

	public function Add($id_pekerjaan=0){
		$this->Edit($id_pekerjaan);
	}

	public function Edit($id_pekerjaan=0,$id=null){

		if($this->post['act']=='reset'){
			redirect(current_url());
		}

		$this->_beforeDetail($id_pekerjaan,$id);

		$this->data['idpk'] = $id;

		$this->data['row'] = $this->model->GetByPk($id);

		if (!$this->data['rowheader'] && !$this->data['row'] && $id)
			$this->NoData();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("","");

		if(count($this->post) && $this->post['act']<>'change'){
			if(!$this->data['row'])
				$this->data['row'] = array();

			$record = $this->Record($id);

			$this->data['row'] = array_merge($this->data['row'],$record);
			$this->data['row'] = array_merge($this->data['row'],$this->post);
		}

		$this->_onDetail($id);

		$this->data['rules'] = $this->Rules();

		## EDIT HERE ##
		if ($this->post['act'] === 'save') {

			$record['id_pekerjaan'] = $id_pekerjaan;

			$this->_isValid($record,true);

            $this->_beforeEdit($record,$id);

            $this->_setLogRecord($record,$id);

            $this->model->conn->StartTrans();
			if (trim($this->data['row'][$this->pk])==trim($id) && trim($id)) {

				$return = $this->_beforeUpdate($record, $id);

				if($return){
					$return = $this->model->Update($record, "$this->pk = ".$this->conn->qstr($id));
				}

				if ($return['success']) {

					$this->log("mengubah ".$record['nama']);

					$return1 = $this->_afterUpdate($id);

					if(!$return1){
						$return = false;
					}
				}
			}else {

				$return = $this->_beforeInsert($record);

				if($return){
					$return = $this->model->Insert($record);
					$id = $return['data'][$this->pk];
				}

				if ($return['success']) {

					$this->log("menambah ".$record['nama']);

					$return1 = $this->_afterInsert($id);

					if(!$return1){
						$return = false;
					}
				}
			}

            $this->conn->CompleteTrans();

			if ($return['success']) {

				$this->_afterEditSucceed($id);

				SetFlash('suc_msg', $return['success']);
				redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");

			} else {
				$this->data['row'] = array_merge($this->data['row'],$record);
				$this->data['row'] = array_merge($this->data['row'],$this->post);

				$this->_afterEditFailed($id);

				$this->data['err_msg'] = "Data gagal disimpan";
			}
		}

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	public function Detail($id_pekerjaan=null, $id=null){
		$this->_beforeDetail($id_pekerjaan, $id);
		unset($this->access_role['add']);
		unset($this->access_role['delete']);

		$this->data['row'] = $this->model->GetByPk($id);

		$this->_onDetail($id);

		if (!$this->data['row'] && !$this->data['rowheader'])
			$this->NoData();

		$this->_afterDetail($id);

		$this->View($this->viewdetail);
	}

	protected function _beforeDelete($id=null){
		return false;

		$rows = $this->conn->GetArray("select * from rab_pekerjaan_files where id_pekerjaan = ".$this->conn->escape($id));

		$ret = true;

		foreach($rows as $r){
			if(!$ret)
				break;
			
			@unlink($this->data['configfile']['upload_path'].$r['file_name']);
		}

		if($ret)
			$ret = $this->conn->Execute("delete from rab_pekerjaan_files where id_pekerjaan = ".$this->conn->escape($id));

		if($ret)
			$ret = $this->conn->Execute("delete from rab_rab where id_pekerjaan = ".$this->conn->escape($id));

		if($ret)
			$ret = $this->conn->Execute("delete from rab_pekerjaan_ttd where id_pekerjaan = ".$this->conn->escape($id));

		return $ret;
	}

	public function Delete($id_pekerjaan=null, $id=null){
        $this->model->conn->StartTrans();

        $this->_beforeDetail($id);

		$this->data['row'] = $this->model->GetByPk($id);
		
		$this->_onDetail($id);

		if (!$this->data['row'])
			$this->NoData();

		$return = $this->_beforeDelete($id);

		if($return){
			$return = $this->model->delete("$this->pk = ".$this->conn->qstr($id));
		}

		if($return){
			$return1 = $this->_afterDelete($id);
			if(!$return1)
				$return = false;
		}

        $this->model->conn->CompleteTrans();

		if ($return) {

			$this->log("menghapus $id");

			SetFlash('suc_msg', $return['success']);
			redirect("$this->page_ctrl/index/$id_pekerjaan");
		}
		else {
			SetFlash('err_msg',"Data gagal didelete");
			redirect("$this->page_ctrl/detail/$id_pekerjaan/$id");
		}

	}

	protected function _beforeDetail($id_pekerjaan=null, &$id=null){
		$id = $this->conn->GetOne("select max(id_plan) from wbs_plan where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$this->data['id_pekerjaan'] = $id_pekerjaan;

		$this->data['rowheader1'] = $this->rabpekerjaan->GetByPk($id_pekerjaan);
		$this->data['id_proyek'] = $id_proyek = $this->data['rowheader1']['id_proyek'];
		$this->data['rowheader'] = $this->proyek->GetByPk($id_proyek);
		$this->data['editedheader'] = false;
		$this->data['modeheader'] = 'detail';
		$this->data['add_param'] .= $id_pekerjaan;

		$this->data['versiarr'] = $this->conn->GetArray("select * from rab_rab where jenis='1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['id_rab'] = $id_rab = $this->conn->GetOne("select max(id_rab) from rab_rab where jenis = '1' and id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
		$this->data['rowheader2'] = $this->rabrab->GetByPk($id_rab);

		if (!$this->data['rowheader2']) $this->NoData('Tidak ada master RAB');

		# TODO: sementara
		$this->access_role = array('add'=>1, 'edit'=>1, 'delete'=>1, 'detail'=>1, 'list'=>1, 'reset'=>1, 'save'=>1, 'batal'=>1);
	}

	protected function _afterDetail($id){
		$id = (int) $id;
/*
		if(!$this->data['row']['ttd'] && $id){
			$this->data['row']['ttd'] = $this->conn->GetArray("select a.*
				from rab_pekerjaan_ttd a
				where id_pekerjaan = ".$this->conn->escape($id)." 
				order by id_pekerjaan_ttd");
		}

		if(!$this->data['row']['ttd']){
			$max_id = $this->conn->GetOne("select max(id_pekerjaan) from rab_pekerjaan_ttd a");
			$this->data['row']['ttd'] = $this->conn->GetArray("select a.*
				from rab_pekerjaan_ttd a
				where id_pekerjaan = ".$this->conn->escape($max_id)." 
				order by id_pekerjaan_ttd");
		}

		if(count($this->data['row']['ttd'])){
			$this->data['ttdarr'] = $this->conn->GetList("select nid as key, nama as val from mt_pegawai where nid in (".$this->conn->GetKeysStr($this->data['row']['ttd'],'nid').")");
		}

		if(!$this->data['row']['sp3']['id'] && $id){
			$rows = $this->conn->GetArray("select id_pekerjaan_files as id, client_name as name
				from rab_pekerjaan_files
				where jenis_file = 'sp3' and id_pekerjaan = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['sp3']['id'][] = $r['id'];
				$this->data['row']['sp3']['name'][] = $r['name'];
			}
		}

		if(!$this->data['row']['kontrak']['id'] && $id){
			$rows = $this->conn->GetArray("select id_pekerjaan_files as id, client_name as name
				from rab_pekerjaan_files
				where jenis_file = 'kontrak' and id_pekerjaan = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['kontrak']['id'][] = $r['id'];
				$this->data['row']['kontrak']['name'][] = $r['name'];
			}
		}

		if(!$this->data['row']['file']['id'] && $id){
			$rows = $this->conn->GetArray("select id_pekerjaan_files as id, client_name as name
				from rab_pekerjaan_files
				where jenis_file = 'file' and id_pekerjaan = ".$this->conn->escape($id));

			foreach($rows as $r){
				$this->data['row']['file']['id'][] = $r['id'];
				$this->data['row']['file']['name'][] = $r['name'];
			}
		}

		$this->data['mtpegawaiarr'][$this->data['row']['id_pic']] = $this->data['row']['nama_pic'];
*/

//		$id_pekerjaan = $this->data['id_pekerjaan'] = $this->conn->GetOne("select max(id_pekerjaan) from rab_rab where id_pekerjaan = ".$this->conn->escape($id)." and jenis = '1'");

		// $this->data['rowheader2'] = $this->rabrab->GetByPk($id_pekerjaan);

		$id_plan = $this->data['id_plan'];

		$cek = $this->conn->GetOne("select 1 from wbs_plan where id_plan = ".$this->conn->escape($id_plan));
/*
		if(!$cek && !$this->data['row']['tgl_mulai_pelaksanaan'] && !$this->data['row']['tgl_selesai_pelaksanaan']){
			$this->data['row']['tgl_mulai_pelaksanaan'] = $this->data['rowheader']['tgl_rencana_mulai'];
			$this->data['row']['tgl_selesai_pelaksanaan'] = $this->data['rowheader']['tgl_rencana_selesai'];
		}

		if(!$cek && !$this->data['row']['id_pic']){
			$this->data['row']['id_pic'] = $_SESSION[SESSION_APP]['nid'];
			$this->data['row']['nama_pic'] = $_SESSION[SESSION_APP]['name'];
		}

		$this->data['mtpegawaiarr'][$this->data['row']['id_pic']] = $this->data['row']['nama_pic'];
*/
		if ($id) {
			$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai, max(selesai)-min(mulai) as total_hari from wbs_plan_detail where id_plan=$id ";
			$times = $this->conn->GetRow($sql);
			$mulai = $times['mulai'];
			$selesai = $times['selesai'];
			$total_hari = $times['total_hari'];

			$sql = "select t.*, extract(day from t.selesai-t.mulai) as hari, to_char(t.mulai, 'dd-mm-yyyy hh24:mi') as mulai_str, to_char(t.selesai, 'dd-mm-yyyy hh24:mi') as selesai_str from wbs_plan_detail t where t.id_plan=$id order by urutan";
			$details = $this->conn->GetArray($sql);
			$parents = array();
			foreach ($details as &$detail) {
				# menghitung tingkat
				if (!$detail['urutan_parent']) {
					$detail['tingkat'] = $parents[$detail['urutan']] = 0;
				}
				else {
					$detail['tingkat'] = $parents[$detail['urutan']] = $parents[$detail['urutan_parent']] + 1;
				}
			}


			$this->data['details'] = $details;

		}
	}

	protected function _afterInsert($id=null){

		############################

		$ret = true;

		if ($_FILES) {
			$file = $_FILES['file1']['tmp_name'];
			$file = "/media/solikul/Data/a.mpp";
			$details = $this->readMPP($file);

			foreach ($details as $key => $value) {
				$id_detail = (int) $key;
				$urutan_parent = (int) $value[0]['parent'][0];
				$nama = $value[0]['name'][0];
				$start = date('Y-m-d H:i:s', strtotime($value[0]['start'][0]));
				$finish = date('Y-m-d H:i:s', strtotime($value[0]['finish'][0]));
				$is_leaf = $value[0]['isleaf'][0];
				$record = array(
					'id_plan'=>(int) $id,
					'urutan'=>$id_detail,
					'nama'=>$nama,
					'mulai'=>$start,
					'selesai'=>$finish,
					'rencana'=>$rencana,
					'realisasi'=>0,
					'urutan_parent'=>$urutan_parent,
					'is_leaf'=>$is_leaf,
				);
		        $sql = $this->conn->InsertSQL('wbs_plan_detail', $record);
		        if($sql){
				    $ret = $this->conn->Execute($sql);
				}
			}

			$this->_hitungDetail($id);
			$this->_hitungRencanaParent($id);
		}

		###########################


		// if($ret)
		// 	$ret = $this->newRab($id);

		return $ret;
	}

	protected function _afterUpdate($id){
		if ($_FILES) {
			$id = (int) $id;
			$file = $_FILES['file1']['tmp_name'];
			$file = "/media/solikul/Data/a.mpp";
			$details = $this->readMPP($file);

			foreach ($details as $key => $value) {
				$id_detail = (int) $key;
				$urutan_parent = (int) $value[0]['parent'][0];
				$nama = $value[0]['name'][0];
				$start = date('Y-m-d H:i:s', strtotime($value[0]['start'][0]));
				$finish = date('Y-m-d H:i:s', strtotime($value[0]['finish'][0]));
				$is_leaf = $value[0]['isleaf'][0];
				$record = array(
					'id_plan'=> $id,
					'urutan'=>$id_detail,
					'nama'=>$nama,
					'mulai'=>$start,
					'selesai'=>$finish,
					'urutan_parent'=>$urutan_parent,
					'is_leaf'=>$is_leaf,
				);
		        $sql = $this->conn->UpdateSQL('wbs_plan_detail', $record, "id_plan=$id and urutan=$id_detail");
		        if($sql){
				    $ret = $this->conn->Execute($sql);
				}
			}

			$this->_hitungDetail($id);
			$this->_hitungRencanaParent($id);
		}

		$ret = true;
		
		// if($ret)
		// 	$ret = $this->_delsertFiles($id);
		
		// if($ret)
		// 	$ret = $this->_delsertTTD($id);
		
		return $ret;
	}
/*
	private function _delsertFiles($id_pekerjaan = null){
		$ret = true;

		if(!empty($this->post['sp3'])){
			foreach($this->post['sp3']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}

		if(!empty($this->post['kontrak'])){
			foreach($this->post['kontrak']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}

		if(!empty($this->post['file'])){
			foreach($this->post['file']['id'] as $k=>$v){
				if(!$ret)
					break;

				$this->_updateFiles(array('id_pekerjaan'=>$id_pekerjaan), $v);
			}
		}
		return $ret;
	}

	private function _delsertTTD($id_pekerjaan = null){
		$ret = $this->conn->Execute("delete from rab_pekerjaan_ttd where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));

		$MainSpecarr = array();

		if(!empty($this->post['ttd'])){
			foreach ($this->post['ttd'] as $key => $v) {
				if(!$v['nid'])
					continue;

				if(!$ret)
					break;

				$record = array();
				$record['id_pekerjaan'] = $id_pekerjaan;
				$record['nid'] = $v['nid'];
				$row_pegawai = $this->conn->GetRow("select nama, jabatan from mt_pegawai where trim(nid) = ".$this->conn->escape($v['nid']));
				$record['nama'] = $row_pegawai['nama'];
				$record['jabatan'] = $row_pegawai['jabatan'];

				$ret = $this->conn->goInsert('rab_pekerjaan_ttd', $record);
			}
		}

		return $ret;
	}
*/	

	function _hitungDetail($id) {
		$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai, max(selesai)-min(mulai) as total_hari from wbs_plan_detail where id_plan=$id ";
		$times = $this->conn->GetRow($sql);
		$mulai = $times['mulai'];
		$selesai = $times['selesai'];
		$total_hari = $times['total_hari'];

		$sql = "select t.*, extract(day from t.selesai-t.mulai) as hari from wbs_plan_detail t where t.id_plan=$id order by urutan";
		$details = $this->conn->GetArray($sql);
		$parents = array();
		foreach ($details as &$detail) {
			# menghitung tingkat
			if (!$detail['urutan_parent']) {
				$detail['tingkat'] = $parents[$detail['urutan']] = 0;
			}
			else {
				$detail['tingkat'] = $parents[$detail['urutan']] = $parents[$detail['urutan_parent']] + 1;
			}
		}

		$tgl_plans = array();
		$mulai_int = strtotime($mulai);
		for ($i=0;$i<$total_hari;$i++) {
			# menghitung bobot rencana
			$tanggal = strtotime("+$i day", $mulai_int);
			$tgl_plans[date('Y-m-d', $tanggal)] = array();
		}

		foreach ($tgl_plans as $key => $value) {
			$sql = "select urutan from wbs_plan_detail where id_plan=$id and (to_char(mulai, 'yyyy-mm-dd') <= '$key' and to_char(selesai, 'yyyy-mm-dd') >= '$key')  and is_leaf='1' ";
			$rows = $this->conn->GetArray($sql);
			foreach ($rows as $row) {
				$tgl_plans[$key][] = $row['urutan'];
			}
		}

		$bobots = array();
		foreach ($details as &$detail) {
			$urutan = $detail['urutan'];
			foreach ($tgl_plans as $key => $tgl_plan) {
				if (in_array($urutan, $tgl_plan)) {
					$bobots[$urutan][] = 1/count($tgl_plan)/$total_hari*100;
				}
				
			}
		}

		$bobotrencana_total = array();
		foreach ($bobots as $key=>$bobot) {
			foreach ($bobot as $value) {
				$bobotrencana_total[$key] += $value;
			}	
		}

		$total = 0;
		foreach ($bobotrencana_total as $urutan=>$bobot_rencana) {
			$bobot_rencana = number_format($bobot_rencana, 5);
			$sql = "update wbs_plan_detail set rencana=$bobot_rencana where id_plan=$id and urutan=$urutan";
			$ret = $this->conn->Execute($sql);
		}

	}

	function _hitungRencanaParent($id_plan, $id_parent=false) {
		if ($id_parent === false) 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and is_leaf=1 order by urutan";
		else 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and urutan_parent=$id_parent order by urutan";

		$rows = $this->conn->GetArray($sql);
		$list_rencana = array();
		$list_parent = array();
		foreach ($rows as $row) {
			if ($row['urutan_parent'] == 0) return false;

			$list_rencana[$row['urutan_parent']] += $row['rencana'];
			$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan={$row['urutan_parent']}";
			$new_urutan_parent = $this->conn->GetOne($sql);
			if (!in_array($new_urutan_parent, $list_parent)) {
				$list_parent[] = $new_urutan_parent;
			}
		}

		foreach ($list_rencana as $urutan_parent=>$rencana) {
			$sql = "update wbs_plan_detail set rencana=$rencana where id_plan=$id_plan and urutan=$urutan_parent and is_leaf=0";
			$this->conn->Execute($sql);
		}

		foreach ($list_parent as $urutan_parent) {
			$this->_hitungRencanaParent($id_plan, $urutan_parent);
		}

	}

	function _hitungRealisasiParent($id_plan, $id_parent=false) {
		if ($id_parent === false) 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and is_leaf=1 order by urutan";
		else 
			$sql = "select * from wbs_plan_detail where id_plan=$id_plan and urutan_parent=$id_parent order by urutan";

		$rows = $this->conn->GetArray($sql);
		$list_realisasi = array();
		$list_parent = array();
		foreach ($rows as $row) {
			if ($row['urutan_parent'] == 0) return false;

			$list_realisasi[$row['urutan_parent']] += $row['realisasi'];
			$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan={$row['urutan_parent']}";
			$new_urutan_parent = $this->conn->GetOne($sql);
			if (!in_array($new_urutan_parent, $list_parent)) {
				$list_parent[] = $new_urutan_parent;
			}
		}

		foreach ($list_realisasi as $urutan_parent=>$realisasi) {
			$sql = "update wbs_plan_detail set realisasi=$realisasi where id_plan=$id_plan and urutan=$urutan_parent and is_leaf=0";
			$this->conn->Execute($sql);
		}

		foreach ($list_parent as $urutan_parent) {
			$this->_hitungRealisasiParent($id_plan, $urutan_parent);
		}

	}	

	function kurva($id_pekerjaan, $id) {
		$this->_beforeDetail($id_pekerjaan);

		$sql = "select to_char(min(mulai), 'yyyy-mm-dd') as mulai, to_char(max(selesai), 'yyyy-mm-dd') as selesai, max(selesai)-min(mulai) as total_hari from wbs_plan_detail where id_plan=$id ";
		$times = $this->conn->GetRow($sql);
		$mulai = $times['mulai'];
		$selesai = $times['selesai'];
		$total_hari = $times['total_hari'];

		$sql = "select t.*, extract(day from t.selesai-t.mulai) as hari from wbs_plan_detail t where t.id_plan=$id order by urutan";
		$details = $this->conn->GetArray($sql);
		$parents = array();
		foreach ($details as &$detail) {
			# menghitung tingkat
			if (!$detail['urutan_parent']) {
				$detail['tingkat'] = $parents[$detail['urutan']] = 0;
			}
			else {
				$detail['tingkat'] = $parents[$detail['urutan']] = $parents[$detail['urutan_parent']] + 1;
			}
		}



/*
		$bobots = array();
		$bobots_per_day = array(0);

		# bobot dihitung detail perhari, hasilnya kurva linear

		$tgl_plans = array();
		$mulai_int = strtotime($mulai);
		for ($i=0;$i<$total_hari;$i++) {
			# menghitung bobot
			$tanggal = strtotime("+$i day", $mulai_int);
			$tgl_plans[date('Y-m-d', $tanggal)] = array();
		}

		foreach ($tgl_plans as $key => $value) {
			$sql = "select urutan from wbs_plan_detail where id_plan=$id and (to_char(mulai, 'yyyy-mm-dd') <= '$key' and to_char(selesai, 'yyyy-mm-dd') >= '$key')  and is_leaf='1' ";
			$rows = $this->conn->GetArray($sql);
			foreach ($rows as $row) {
				$tgl_plans[$key][] = $row['urutan'];
			}
		}

		foreach ($details as &$detail) {
			$urutan = $detail['urutan'];
			foreach ($tgl_plans as $key => $tgl_plan) {
				$bobot_temp = 0;
				if (in_array($urutan, $tgl_plan)) {
					$bobots[$urutan][] = 1/count($tgl_plan)/$total_hari*100;
					$bobot_temp += 1/count($tgl_plan)/$total_hari*100;
				}
				$bobots_per_day[$key] += $bobot_temp;
			}
		}
*/
		$plan_per_day = array();
		$realisasi_per_day = array();

		# bobot rencana dihitung setelah selesai
		$tgl_plans = array();
		$mulai_int = strtotime($mulai);
		for ($i=0;$i<$total_hari;$i++) {
			# menghitung bobot rencana
			$tanggal = strtotime("+$i day", $mulai_int);
			$tgl_plans[date('Y-m-d', $tanggal)] = array();
		}

		$is_realisasi = true;
		foreach ($tgl_plans as $key => $value) {
			$sql = "select sum(rencana) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and to_char(selesai, 'yyyy-mm-dd') <= '$key'  and is_leaf='1')";
			$plan_per_day[$key] = $this->conn->GetOne($sql);

			if ($is_realisasi) {
				$sql = "select sum(nvl(realisasi,0)) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and (to_char(mulai, 'yyyy-mm-dd') <= '$key' and to_char(selesai, 'yyyy-mm-dd') >= '$key')  and is_leaf='1')";
				if ($this->conn->GetOne($sql)) {
					$sql = "select sum(nvl(realisasi,0)) from wbs_plan_detail where id_plan=$id and urutan in (select urutan from wbs_plan_detail where id_plan=$id and to_char(selesai, 'yyyy-mm-dd') <= '$key'  and is_leaf='1')";
					$realisasi_per_day[$key] = $this->conn->GetOne($sql);
				}
				else {
					$is_realisasi = false;
				}
			}

		}

		// echo '<pre>';var_dump($plan_per_day);var_dump($realisasi_per_day);die();
		ksort($plan_per_day);
		ksort($realisasi_per_day);

		$this->data['details'] = $details;
		$this->data['times'] = $times;
		$this->data['plan_per_day'] = $plan_per_day;
		$this->data['realisasi_per_day'] = $realisasi_per_day;
		$this->View('panelbackend/wbs_kurva');
	}

	function updaterealisasi() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);
		$val = (int) $_POST['val'];

		if ($val > 100) $val = 100;

		$sql = "update wbs_plan_detail set progress=$val, realisasi=rencana*$val/100, last_update_realisasi=sysdate where id_plan=$id and urutan=$urutan";
		$ret = $this->conn->query($sql);
		if ($ret) {
			$sql = "select urutan_parent, progress, realisasi from wbs_plan_detail where id_plan=$id and urutan=$urutan";
			$row = $this->conn->GetRow($sql);
			$progress = $row['progress'];
			$realisasi = number_format($row['realisasi'], 2);
			$urutan_parent = $row['urutan_parent'];

			$this->_hitungRealisasiParent($id, $urutan_parent);

			$progress_pr = '<div class="progress-bar ' . ($progress>100?'progress-bar-red':(($progress==100)?'progress-bar-blue':'progress-bar-green')) . '" ';
			$progress_pr .= 'style="width: ' . round($progress,2) .'%;">' .round($progress,2) .'%</div>';

			$ret = array('progress'=>$progress, 'progress_pr'=>$progress_pr, 'realisasi'=>$realisasi);
			echo json_encode($ret);

			// $this->conn->debug = 1;
			$this->_updateProgressPekerjaan($id, $urutan);

		}
		else {
			echo '-1';
		}

		// TODO log
		die();
	}

	function getparentrealisasi() {
		$id = (int) $_POST['id'];
		$urutan = (int) str_replace('r', '', $_POST['id_detail']);

		$sql = "select urutan_parent from wbs_plan_detail where id_plan=$id and urutan=$urutan";
		$urutan_parent = $this->conn->GetOne($sql);

		$list_realisasi = $this->_getParentRealisasi($id, $urutan_parent);
		$str = '';
		foreach ($list_realisasi as $key => $value) {
			$str .= $key . '.' . $value . '__';
		}

		$ret = array('list_realisasi'=>$str);
		echo json_encode($list_realisasi);
	}


	function _getParentRealisasi($id_plan, $urutan_parent) {
		$list_realisasi = array();

		$sql = "select realisasi, urutan, urutan_parent from wbs_plan_detail where id_plan=$id_plan and urutan=$urutan_parent";
		$row = $this->conn->GetRow($sql);
		if ($row['urutan_parent'] == 0) return array('rparent'.$row['urutan']=>@number_format($row['realisasi'],2));

		$list_realisasi["rparent{$row['urutan']}"] = @number_format($row['realisasi'],2);
		$arr = $this->_getParentRealisasi($id_plan, $row['urutan_parent']);
		$list_realisasi = array_merge($list_realisasi, $arr);

		return $list_realisasi;
	}

	function cetak($id_pekerjaan=null, $id=null) {
		$this->_afterDetail($id);
		// $this->data['content'] = $this->load->view($view, $this->data, TRUE);
		echo $this->load->view($this->viewcetak, $this->data, true);
	}

}
