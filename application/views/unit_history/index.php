<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
    <h1 class="text-white">Master Unit History</h1>
</section>
<style type="text/css">
    .table {
        margin-bottom: 10px
    }
</style>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="row" style="margin-bottom:  15px; margin-top: 5px">
                <div class="col-md-5">
                    <div class="input-group">
                        <input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="button" class='btn btn-default btn-sm' title="Filter" onclick="load_table()"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn waves-effect btn-sm btn-default" onclick="tbl_reset()"><span class="glyphicon glyphicon-refresh"></span></button>
                        </span>
                    </div>

                </div>
                <div class="col-md-1">
                    <button type="button" onclick="do_add()" href="javascript:void(0)" class=" btn btn-warning">
                        <span class="glyphicon glyphicon glyphicon-plus"></span> Unit Baru
                    </button>
                </div>
            </div>
            <table class="table table-striped table-hover" style="width: 100%;" id="tblunit">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nama Unit</th>
                        <th class="text-center">Jenis Pekerjaan</th>
                        <th class="text-center">Plan Start</th>
                        <th class="text-center">Act Start</th>
                        <th class="text-center">Plan End</th>
                        <th class="text-center">Act End</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="text-center">

                </tbody>
            </table>
        </div>
    </div>
</section>
<div class="modal" tabindex="-1" role="dialog" id="modal-add">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Unit History <label>Input</label></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-group" id="inputan" enctype="multipart/form-data">
                    <input type="hidden" name="" id="id">
                    <div class="form-group">
                        <label for=""> Nama Unit </label>
                        <input type="hidden" class="form-control" name="nama_unit" id="nama_unit" placeholder="Nama Unit"> <br>
                        <select data-live-search="true" tabindex="2" name="tdb_id" id="tdb_id" class="form-control2 w-100"></select>
                    </div>
                    <div class="form-group">
                        <label for=""> Jenis Pekerjaan </label>
                        <input type="text" class="form-control" name="jenis_pekerjaan" id="jenis_pekerjaan" placeholder="Jenis Pekerjaan">
                    </div>
                    <div class="form-group">
                        <label for=""> Plan Start </label>
                        <input type="text" class="form-control date" name="plan_start" id="plan_start">
                    </div>
                    <div class="form-group">
                        <label for=""> Act Start </label>
                        <input type="text" class="form-control date" name="act_start" id="act_start">
                    </div>
                    <div class="form-group">
                        <label for=""> Plan End </label>
                        <input type="text" class="form-control date" name="plan_end" id="plan_end">
                    </div>
                    <div class="form-group">
                        <label for=""> Act End </label>
                        <input type="text" class="form-control date" name="act_end" id="act_end">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="do_process()" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        load_tdb();
        load_table();

        $('.date').datetimepicker({
            format: "DD-MM-YYYY",
            useCurrent: false
        });
    });

    function tbl_reset() {

        $("#filter").val('');
        load_table();

    }

    function load_tdb() {
        $.ajax({
            type: "GET",
            url: "<?= base_url() ?>new/Unit_History/load_tdb",
            // data: "data",
            dataType: "json",
            success: function(response) {
                if (response.status == 200) {
                    var count = response.data.length;
                    var html = '';
                    html += '<option value="">Pilih Nama Unit</option>';
                    for (let index = 0; index < count; index++) {

                        html += '<option value="' + response.data[index].id + '">' + response.data[index].nama_unit + '</option>';

                    }
                    $("#tdb_id").html(html);
                    $("#tdb_id").selectpicker('refresh');
                    $("#tdb_id").trigger('change');
                    $("#tdb_id").change(function() {
                        $("#nama_unit").val($("#tdb_id option:selected").text());
                        console.log($("#tdb_id option:selected").text());
                    });
                }
            }
        });
    }

    function load_table() {


        var datatable = $('#tblunit').dataTable({
            destroy: true,
            "processing": true,
            "serverSide": true,
            ajax: {
                url: "<?php echo base_url('/new/unit_history/load_data/') ?>"
            },
            "order": [
                [0, 'desc']
            ],
            "language": {
                "lengthMenu": "Perhalaman _MENU_",
                "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
            },
            "columns": [{
                    "name": "ID"
                },
                {
                    "name": "NAMA_UNIT"
                },
                {
                    "name": "JENIS_PEKERJAAN"
                },
                {
                    "name": "PLAN_START"
                },
                {
                    "name": "ACT_START"
                },
                {
                    "name": "PLAN_END"
                },
                {
                    "name": "ACT_END"
                },
                // { "name": "ACT" },
                {
                    className: "td-right",
                    "orderable": false,
                    "targets": [10]
                },
            ],
            "iDisplayLength": 10,
            "scrollX": false,
        });

        var filter = $("#filter").val();
        if (filter != '') {
            datatable.search(filter).draw();
        }

    }

    function do_add() {

        $("#modal-add").modal('show');
        $('#inputan').trigger('reset');
        $('.date').val('');
        $(".date").datetimepicker("update");
        $("#tdb_id").val('');
        $("#tdb_id").selectpicker('refresh');
    };

    function do_process() {

        var tdb_id = $("#id").val();
        // var form = $("#inputan").serialize();
        var formData = new FormData($('#inputan')[0]);
        // alert(formData);
        // alert(form);
        $.ajax({
            url: '<?= base_url() ?>new/unit_history/process/' + tdb_id,
            type: 'POST',
            dataType: 'JSON',
            data: formData,
            contentType: false,
            processData: false,
            success: function(resp) {
                if (resp.status == 200) {

                    alert('success update data');
                } else if (resp.status == 201) {

                    alert('success save data');
                } else {

                    alert('error : ' + resp.msg);
                }
                $('#inputan').trigger('reset');
                $("#plan_start").datetimepicker("update");
                $("#act_start").datetimepicker("update");
                $("#plan_end").datetimepicker("update");
                $("#act_end").datetimepicker("update");
                $("#modal-add").modal('hide');
                load_table();
            }
        });

    }

    function edit_unit(id) {
        load_data_simple(id);
        $("#modal-add").modal('show');
    }

    function load_data_simple(unit_id) {

        $.ajax({
            url: '<?= base_url() ?>new/unit_history/load_detail/' + unit_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(resp) {
                if (resp.status == 200) {
                    console.log(resp);
                    $("#id").val(resp.data.id);
                    $("#nama_unit").val(resp.data.nama_unit);
                    $("#tdb_id").val(resp.data.tdb_id);
                    $("#tdb_id").selectpicker('refresh');
                    $("#jenis_pekerjaan").val(resp.data.jenis_pekerjaan);
                    $("#plan_start").val(resp.data.plan_start);
                    $("#plan_start").datetimepicker("update");
                    $("#act_start").val(resp.data.act_start);
                    $("#act_start").datetimepicker("update");
                    $("#plan_end").val(resp.data.plan_end);
                    $("#plan_end").datetimepicker("update");
                    $("#act_end").val(resp.data.act_end);
                    $("#act_end").datetimepicker("update");
                }
            }
        });

    }

    function delete_data(id) {


        if (confirm('Hapus data history ini ?')) {
            $.ajax({
                url: '<?= base_url() ?>new/unit_history/delete_data/' + id,
                type: 'POST',
                dataType: 'JSON',
                success: function(resp) {
                    if (resp.status == 200) {
                        alert('success deleted');
                        load_table();
                    }
                }
            });
        }


    }
</script>