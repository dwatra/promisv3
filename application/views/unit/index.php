<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Master Unit</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<div class="row" style="margin-bottom:  15px; margin-top: 5px">
				<div class="col-md-5">
					<div class="input-group">
						<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="button" class='btn btn-default btn-sm' title="Filter" onclick="load_table()" ><span class="glyphicon glyphicon-search"></span></button>
							<button type="button" class="btn waves-effect btn-sm btn-default" onclick="tbl_reset()"><span class="glyphicon glyphicon-refresh"></span></button>
						</span>
					</div>
					
				</div>
				<div class="col-md-1">
					<button type="button" onclick="do_add()" href="javascript:void(0)" class=" btn btn-warning">
						<span class="glyphicon glyphicon glyphicon-plus"></span> Unit Baru
					</button>	
				</div>
			</div>
			<table class="table table-striped table-hover" id="tblunit">
				<thead>
					<tr>
						<th class="text-center">ID</th>
						<th class="text-center">ID Distrik</th>
                        <th class="text-center">Kode Distrik</th>
						<th class="text-center">Nama Distrik</th>
                        <th class="text-center">Alamat</th>
						<th class="text-center">Keterangan</th>
						<th></th>
					</tr>
				</thead>
				<tbody class="text-center">
					
				</tbody>
			</table>
		</div>
    </div>
</section>
<div class="modal" tabindex="-1" role="dialog" id="modal-add">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Unit <label>Input</label></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-group" id="inputan">
          <input type="hidden" name="" id="id">
          <div class="form-group">
            <label for="id_distrik"> Id Distrik </label>
            <input type="text" class="form-control" name="id_distrik" id="id_distrik" placeholder="Id Distrik">
          </div>
          <div class="form-group">
            <label for="kd_distrik"> Kode Distrik </label>
            <input type="text" class="form-control" name="kd_distrik" id="kd_distrik" placeholder="Kode Distrik">
          </div>
          <div class="form-group">
            <label for="nm_distrik"> Nama Distrik </label>
            <input type="text" class="form-control" name="nm_distrik" id="nm_distrik" placeholder="Nama Distrik">
          </div>
          <div class="form-group">
            <label for="alamat_distrik"> Alamat </label>
            <input type="text" class="form-control" name="alamat_distrik" id="alamat_distrik" placeholder="Alamat Distrik">
          </div>
          <div class="form-group">
            <label for="keterangan_distrik"> Keterangan </label>
                <select class="form-control" style="width:100%" name="ket" id="ket">
                    <option value="PJBS">PJBS</option>
                    <option value="NON PJBS">NON PJBS</option>
                </select>    
            
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="do_process()" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        load_table();
    });
    function tbl_reset(){

        $("#filter").val('');
        load_table();
    }

    function load_table(){


    	var datatable = $('#tblunit').DataTable({
            destroy: true,
            "processing": true,
            "serverSide": true,
            ajax: {
              url: "<?php echo base_url('/new/unit/load_data/') ?>"
            },
            "order": [
              [0, 'desc']
            ],
    		"language": {
                "lengthMenu": "Perhalaman _MENU_",
                "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
            },
            "columns": [
            	{ "name": "ID"},
            	{ "name": "ID_DISTRIK"},
                { "name": "KD_DISTRIK"},
            	{ "name": "NAMA_DISTRIK"},
                { "name": "ALAMAT" },
                { "name": "KETERANGAN" },
             	{ className: "td-right","orderable": false, "targets": [ 10 ] },
             ],
            "iDisplayLength": 10,
            "scrollX" : false,
        });

        var filter =$("#filter").val();
        if (filter != '') {
            datatable.search(filter).draw();
        }

    }
    function do_add(){

        $("#modal-add").modal('show');
    }

    function do_process(){

        var unit_id = $("#id").val();
        var form = $("#inputan").serialize();
        // alert(form);
        $.ajax({
            url: '<?= base_url() ?>new/unit/process/'+unit_id,
            type: 'POST',
            dataType: 'JSON',
            data:  form,
            success:function(resp){
                if (resp.status == 200  ) {

                    alert('success save data');
                } else if(resp.status == 201){

                    alert('success update data');
                } else {

                    alert('error : '.resp.msg );
                }
                $("#modal-add").modal('hide');
                load_table();
            }
        });
        
    }
    function edit_unit (id){
        load_data_simple(id);
        $("#modal-add").modal('show');
    }
    function load_data_simple(unit_id){

        $.ajax({
            url: '<?= base_url() ?>new/unit/load_detail/'+unit_id,
            type: 'POST',
            dataType: 'JSON',
            success:function(resp){
                if (resp.status == 200) {
                    console.log(resp);
                    $("#id").val(resp.data.id);
                    $("#id_distrik").val(resp.data.id_distrik);
                    $("#kd_distrik").val(resp.data.kd_distrik);
                    $("#nm_distrik").val(resp.data.nama_distrik);
                    $("#alamat_distrik").val(resp.data.alamat);
                    $("#ket").val(resp.data.keterangan);
                }
            }
        });
        
    }

    function delete_data(id){


        if (confirm('Hapus data unit ini ?')) {
            $.ajax({
                url: '<?= base_url() ?>new/unit/delete_data/'+id,
                type: 'POST',
                dataType: 'JSON',
                success:function(resp){
                    if (resp.status == 200) {
                        alert('success deleted');
                        load_table();
                    }
                }
            });
        }


    }

</script>