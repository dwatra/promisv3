<section class="content-header">
	<h1 class="text-white">Master Tool</h1>
</section>
<section class="content">
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active">
					<div class="box-header with-border">
				      	<div class="pull-left">
				      		UBAH DATA TOOLS
						</div>
				      	<div class="pull-right">

						</div>
				    </div>
				    <div class="box-body">
				    	<form class="form-horizontal" enctype="multipart/form-data" method="POST">
				    		<input autocomplete="off" type="hidden" name="tool_id" id="tool_id" class="form-control w-100" value="<?php echo ucfirst($tool->tool_id) ?>" hidden>
				    		<input autocomplete="off" type="hidden" name="id" id="id" class="form-control w-100" value="<?php echo ucfirst($tool->id) ?>" >
		    		    	<div class="col-sm-6">
		    					<div class="form-group">
		    						<label for="invt" class="col-sm-4 control-label">
		    							No Inventarisasi
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="invt" id="invt" class="form-control w-100" value="<?php echo $tool->no_inventarisasi ?>" readonly>
		    						</div>
		    					</div>
		    		    		<div class="form-group">
		    						<label for="jenis" class="col-sm-4 control-label">
		    							Jenis&nbsp;<span style="color:#dd4b39">*</span>
		    						</label>
		    						<div class="col-sm-7">
		    							<select  data-placeholder="Pilih..." tabindex="2" name="jenis" id="jenis" class="form-control w-100" onchange="jenisChange()" required>
		    								<option value="general">General</option>
		    								<option value="specific">Specific</option>
		    								<option value="special">Special</option>
		    							</select>
		    						</div>
		    					</div>
		    		    		<div class="form-group">
		    						<label for="kategori" class="col-sm-4 control-label">
		    							Kategori&nbsp;<span style="color:#dd4b39">*</span>
		    						</label>
		    						<div class="col-sm-7">
		    							<!-- <input autocomplete="off" type="text" name="" id="" class="form-control w-100" > -->
		    							<select  data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100">
		    								<option value="mekanik">Mekanik</option>
		    								<option value="listrik">Listrik</option>
		    								<option value="instrument">Instrument</option>
		    								<option value="predictive">Predictive</option>
		    							</select>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="name" class="col-sm-4 control-label">
		    							Nama Tools&nbsp;<span style="color:#dd4b39">*</span>
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="name" id="name" class="form-control w-100" value="<?php echo ucfirst($tool->name) ?>">
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="merk" class="col-sm-4 control-label">
		    							Merk
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="merk" id="merk" class="form-control w-100" value="<?php echo ucfirst($tool->merk) ?>">
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="spesifikasi" class="col-sm-4 control-label">
		    							Spesifikasi
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="spesifikasi" id="spesifikasi" class="form-control w-100" value="<?php echo ucfirst($tool->spek) ?>">
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label class="col-sm-4 control-label">
		    							Foto
		    						</label>
		    						<div class="col-sm-8">
		    							<div id="fileprogress" class="progress" style="display:none">
											<div class="progress-bar progress-bar-success"></div>
		    							</div>
		    							<div id="filefiles" class="files read_detail"></div>
		    							<div id="fileerrors" style="color:red"></div>
		    							<span class="label label-upload">Ext : jpg,jpeg,png</span> &nbsp;&nbsp;&nbsp;<span class="label label-upload">Max : 512 Mb</span><br/>
		    							<span class="btn btn-upload fileinput-button">
    								        <i class="glyphicon glyphicon-upload"></i>
    								        <span>Select files...</span>
    								        <input id="fileupload" name="fileupload" type="file" multiple>
    								    </span>
		    						</div>
		    		    		</div>
		    		    		<div class="form-group">
		    						<label for="tgl" class="col-sm-4 control-label">
		    							Tanggal Perolehan
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="tgl" id="tgl" class="form-control w-100 datepicker" value="<?php echo $tool->tgl_perolehan ?>">
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="harga" class="col-sm-4 control-label">
		    							Harga
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="number" name="harga" id="harga" class="form-control w-100" value="<?php echo ucfirst($tool->harga) ?>">
		    						</div>
		    					</div>
		    		    	</div>
		    		    	<div class="col-sm-6">
		    		    		<div class="form-group">
		    						<label for="kondisi" class="col-sm-4 control-label">
		    							Kondisi&nbsp;<span style="color:#dd4b39">*</span>
		    						</label>
		    						<div class="col-sm-7">
		    							<select  data-placeholder="Pilih..." tabindex="2" name="kondisi" id="kondisi" class="form-control w-100">
		    								<option value="1">Baik</option>
		    								<option value="2">Rusak</option>
		    							</select>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="lokasi" class="col-sm-4 control-label">
		    							Lokasi
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="lokasi" id="lokasi" class="form-control w-100" value="<?php echo ucfirst($tool->lokasi) ?>">
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="tempat" class="col-sm-4 control-label">
		    							Tempat Penyimpanan
		    						</label>
		    						<div class="col-sm-8">
		    							<input autocomplete="off" type="text" name="tempat" id="tempat" class="form-control w-100" value="<?php echo ucfirst($tool->penyimpanan) ?>">
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="ket" class="col-sm-4 control-label">
		    							Keterangan
		    						</label>
		    						<div class="col-sm-8">
		    							<textarea class="form-control w-100 h-100 " rows="10" name="ket" id="ket"><?php echo ucfirst($tool->keterangan) ?></textarea>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<div class="col-sm-4"></div>
		    						<div class="col-sm-4">
		    							<button type="submit" class="btn-save btn btn-sm btn-success"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
		    						</div>
		    					</div>	
		    		    	</div>
				    	</form>
				    </div>
				</div>
			</div>
		</div>		
	</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$('#fileupload').fileupload({
	        url: "http://localhost:8080/pjbs-api/api/tool/file",
	        dataType: 'json',
	        done: function (e, data) {
	        	var file = data.result;
	        	console.log(file);
	            if(file == "error"){
	            	var error = data.result.errors;
	                $('<p onclick="$(this).remove()">'+"Terjadi Kesalahan"+'</p>').appendTo('#fileerrors');
		        }else{
	                $('#filefiles').html('<p class="pfile"><a target="_BLANK" href="http://localhost:8080/pjbs-api/assets/upload/tool/'+file.file_name+'">'+file.file_name+'</a></p>');
	                $('<input type="hidden" name="filename" value="'+file.file_name+'">').appendTo('#filefiles');
		        }
	            $('#fileprogress').hide();
	        },
	        progressall: function (e, data) {
	            $('#fileprogress').show();
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#fileprogress .progress-bar').css(
	                'width',
	                progress + '%'
	            );
	        },
	        fail: function(a, data){
            	$('<p onclick="$(this).remove()">'+data.errorThrown+'</p>').appendTo('#fileerrors');
	            $('#fileprogress').hide();
	        }
	    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	    //select2 and file upload default value
	    $('<p class="pfile"><a target="_BLANK" href="http://localhost:8080/pjbs-api/assets/upload/tool/<?php echo $tool->IMAGE ?>"><?php echo $tool->IMAGE ?></a></p>').appendTo('#filefiles');
	    $('<input type="hidden" name="filename" value="<?php echo $tool->IMAGE ?>">').appendTo('#filefiles');

	    $("select[name='kondisi'").val("<?php echo $tool->kondisi ?>")
	    $("select[name='kategori'").val("<?php echo $tool->kategori ?>")
	    $("select[name='jenis'").val("<?php echo $tool->jenis ?>")
	    $('select').select2().trigger('change')
	    var data = "<?php echo base_url()."new/tool/autoName" ?>";
 		$("#name").autocomplete({
 		  source: data
 		});

	});

	function jenisChange(){
		$("#invt").val('<?php echo $tool->no_inventarisasi ?>');
		if($("#jenis").val() == 'special'){
			$("#invt").prop('readonly', false);
		}else{
			$("#invt").prop('readonly', true);
		}
	}
</script>
