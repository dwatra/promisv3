<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>berita acara</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }

        .data {
            font-size: 14px;
        }

        .data tr th {
            padding: 5px
        }

        .data tr td {
            padding: 5px
        }

        .data tr:nth-child(even) {
            background-color: #b1cde2;
        }

        .data2 {
            font-size: 14px;
        }

        .data2 tr th {
            padding: 5px
        }

        .data2 tr td {
            padding: 5px
        }

        .data2 tr:nth-child(even) {
            background-color: #b1cde2;
        }
    </style>
</head>
<?php
$pic_general = $pegawai_general->pegawai_general;
$pic_special = $pegawai_special->pegawai_special;
 ?>
<body>
    <table style="width: 100%;">
        <tr>
            <td style="width: 5%;"><img src="https://pinpku.umsida.ac.id/wp-content/uploads/2017/07/pjb-services-pembangkitan-jawa-bali.jpg" alt="" width="75px"></td>
            <td>
                <center>
                    BERITA ACARA <br> *) KERUSAKAN/ KEHILANGAN BARANG
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;padding: 5px;">Pada hari ini tanggal <?= tgl_indo(date('Y-m-d')) ?>, saya selaku peminjam peralatan adapun Kendala/Kerusakan/Hilang tools berupa:</td>
        </tr>
    </table>
    <table class="data" style="width: 100%; border-collapse: collapse;" border="1">
        <thead>
            <tr style="background: #406988; color: white;">
                <th style="width: 3%;">NO</th>
                <th>NAMA PERALATAN</th>
                <th>NO. INVENTARIS</th>
                <th>SPESIFIKASI</th>
                <th>MERK</th>
                <th>JUMLAH</th>
                <th>KETERANGAN</th>
                <th>PENYEBAB</th>
                <th>Pic</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($item) > 0){ ?>
            <?php $no=1 ?>
            <?php foreach($item as $data){ ?>
            <?php if ($data->penyebab != '' || $data->penyebab != null){ ?>

            <tr>
                <td style="text-align: center;"><?= $no++ ?></td>
                <td><?= $data->tool_name ?></td>
                <td style="text-align: center;"><?= $data->tool_inventaris ?></td>
                <td style="text-align: center;"><?= $data->tool_spec ?></td>
                <td style="text-align: center;"><?= $data->merk ?></td>
                <td style="text-align: center;"><?= $data->tool_qty ?></td>
                <td style="text-align: center;"><?= $data->keterangan_kembali ?></td>
                <td style="text-align: center;"><?= $data->penyebab ?></td>
                <td style="text-align: center;">

                </td>
            </tr>
            <?php } ?>
            <?php } ?>
            <?php } ?>

        </tbody>
    </table>
    <table class="data2" style="width: 100%; border-collapse: collapse;" border="1">
        <tbody>
            <tr>
                <td colspan="2">Kronologis Kejadian :</td>
            </tr>
            <tr>
                <td style="text-align: center;width:3%">1.</td>
                <td><?= $paket->kronologis ?></td>
            </tr>
        </tbody>
    </table>
    <table class="data2" style="width: 100%; border-collapse: collapse;" border="1">
        <tbody>
            <tr>
                <td colspan="2">Usulan :</td>
            </tr>
            <tr>
                <td style="text-align: center;width:3%">1.</td>
                <td><?= $paket->usulan   ?></td>
            </tr>
        </tbody>
    </table><br>
    <table>
        <tr>
            <td>NID</td>
            <td>:</td>
            <td><?= $peminjaman->pic_general ?></td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td>:</td>
            <td><?= $pegawai->nama ?></td>
        </tr>
        <tr>
            <td>JABATAN</td>
            <td>:</td>
            <td><?= $pegawai->jabatan ?></td>
        </tr>
        <tr>
            <td>PROYEK/NAMA PEKERJAAN</td>
            <td>:</td>
            <td><?= $proyek->nama_proyek ?></td>
        </tr>
    </table>
    <p>Demikian Berita Acara ini dibuat dengan sebenarnya agar dapat dipergunakan sebagaimana mestinya</p>
    <!-- <p style="text-align: right;">Surabaya, 25-06-2012</p> -->
    <table style="width: 100%;">
        <tr>
            <td style="width: 33.3%;">
                <center>
                    <table style="text-align: center;">
                        <tr>
                            <td>User,</td>
                        </tr>
                        <tr>
                            <td><br><br><br></td>
                        </tr>
                        <tr>
                            <td><b><u>(<?= $pegawai->nama ?>)</u></b></td>
                        </tr>
                    </table> <br>
                    <table style="text-align: center;">
                        <tr>
                            <td><br><br><br></td>
                        </tr>
                        <tr>
                            <td><b><u>(<?= $pegawai->nama ?>)</u></b></td>
                        </tr>
                    </table>
                </center>
            </td>
            <td style="text-align: center;width: 33.3%;">
                <br><br><br><br><br><br><br><br><br> Menyetujui, <br><br><br><br><br>
                <center>
                    <table style="text-align: center;">
                        <tr>
                            <td>Mengetahui,</td>
                        </tr>
                        <tr>
                            <td><br><br><br></td>
                        </tr>
                        <tr>
                            <td><b><u>(Eastoni)</u></b></td>
                        </tr>
                        <tr>
                            <td>Manajer Penunjang Proyek</td>
                        </tr>
                    </table>
                </center>
            </td>
            <td style="width: 33.3%;">
                <center>
                    <table style="text-align: center;">
                        <tr>
                            <td>
                                Surabaya, 20-12-2012 <br><br> PIC Manajemen Tools,
                            </td>
                        </tr>
                        <tr>
                            <td><br><br><br></td>
                        </tr>
                        <tr>
                            <td><b><u>(Eastoni)</u></b></td>
                        </tr>
                    </table> <br>
                    <table style="text-align: center;">
                        <tr>
                            <td><br><br><br></td>
                        </tr>
                        <tr>
                            <td><b><u>(Eastoni)</u></b></td>
                        </tr>
                        <tr>
                            <td>Proyek Manajer</td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
</body>

</html>
<script>
    $(document).ready(function() {

        $(window).on('resize', function() {
            $('.data2').width($('.data').height());
        }).trigger('resize');

    });
</script>
<?php

    function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }



 ?>
