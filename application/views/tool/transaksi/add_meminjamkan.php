<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<style type="text/css">
	.bootstrap-select > .dropdown-toggle{
		width:380px;
	}
</style>
<section class="content">
	<div class="col-sm-12 no-padding">
	</div>
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-header with-border">
						<div class="pull-left" style="max-width: -90px">
							<h4 style="margin: 10px 0px 0px 0px !important; display: inline;">Peminjaman Internal Eksternal (Keluar)</h4>
						</div>
						<div class="pull-right">
						</div>
					</div>
					<div class="box-body">
						<div class="col-md-12">
							<form class="form-horizontal" method="POST" id="post" action="<?php echo base_url('/new/tool_keluar/create') ?>">
								
								<div class="form-group">
									<label class="col-sm-1 control-label">Vendor</label>
									<div class="col-sm-9">
										<!-- <input id="jml" type="text" name="vendor" class="form-control w-100"> -->
										<select class="form-control w-100" id="tags" name="vendor_name">

										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
										Vendor PIC&nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_pic" class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
										Jabatan Vendor PIC
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_pic_level" class="form-control w-100" >
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
										NID / No KTP PIC &nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_ktp" class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
										Divisi
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_div" class="form-control w-100" >
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
										Tgl Mulai Pinjam &nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text" name="start" id="start" class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
										Lampiran
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="file" name='berkas' class="form-control w-100" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">Keperluan Peminjaman</label>
									<div class="col-sm-9">
										<!-- <input id="jml" type="text" name="vendor" class="form-control w-100"> -->
										<textarea name="keperluan" class="form-control" rows="5" cols="80"></textarea>
									</div>
								</div>
							</form>
						</div>
						<div class="col-md-12">
							<center><h5>Daftar Tool</h5></center>
							<br>
							<table class="table table-striped table-hover  text-center m-3" id="tbl_detail">
								<thead>
									<th width="10">ID.</th>
									<th>Nama Tool</th>
									<th>Spec</th>
									<th width="10">No Inventaris</th>
									<th>Jenis Tool </th>
									<th></th>
								</thead>
								<tbody>

								</tbody>
							</table>
							<br>
							<div class="row">
								<div class="col-md-6">
									<button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
										<span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
									</button>
								</div>
								<div class="col-md-6">
									<button type="submit" form="post" class="btn btn-success pull-right">
										<span class="glyphicon glyphicon glyphicon-ok"></span> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div style="clear: both;"></div>
</section>
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
      </div>
      <div class="modal-body">
		<form method="POST" class="form-horizontal" action="<?php echo base_url('new/peminjaman/save') ?>" id="form-paket">
			<div class="form-group w-100">
				<label for="invt" class="col-sm-3 control-label">
					Kategori&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<!-- <select  data-placeholder="Pilih..." tabindex="2" name="kategori" onchange="select_items()" id="kategori" class="form-control w-100" required style="width: 100%"> -->
					<select data-live-search="true" placeholder="Pilih..." tabindex="2" onchange="select_items()" id="kategori" class="form-control2">
						<option value="mekanik">Mekanik</option>
						<option value="listrik">Listrik</option>
						<option value="instrument">Instrument</option>
						<option value="predictive">Predictive</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="tool" class="col-sm-3 control-label">
					Tool&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<select data-live-search="true" tabindex="2" name="tool" id="tool" class="form-control2 w-100" required>

					</select>
				</div>
			</div>
<!-- 			<div class="form-group">
				<label class="col-sm-3 control-label">Jumlah Stock </label>
				<div class="col-sm-9">
					<input id="jml_stock" type="number" class="form-control w-100" readonly>
				</div>
			</div> -->
			<!-- <div class="form-group">
				<label class="col-sm-3 control-label">Jumlah</label>
				<div class="col-sm-9">
					<input id="jml" type="number" name="jml" class="form-control w-100">
				</div>
			</div> -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="saved_item();" class="btn btn-primary" >Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	// $("#modal_add").on('hide.bs.modal', function(){
	//     $('#name').val('');
	// 	$('#spek').val('');
	// 	$('#jml').val('');
	// });

	$(document).ready(function() {
		load_select_vendor();
		load_tbl();
		select_items();
		$("#kategori").selectpicker();
	});


	function select_items(){

		$.ajax({
	  		url: '<?php echo base_url("/new/tool/get_tool_by_kategori_transaksi_ie") ?>',
			dataType: 'json',
			data: {
				kategori : $("#kategori").val()
			},
	  		success: function (response) {
	  			var html = '';


				if (response.length > 0) {
					html += '<option value="0">- Pilih List Tool -</option>';
					for (i = 0; i < response.length; i++) {

						value = response[i].id;
						html += '<option value='+value+'>'+response[i].name+' - '+response[i].spek+' - '+response[i].no_inventarisasi+'</option>'
					}
				} else {
					html += '<option value="0">- Tidak ada Tools di Kategori Ini -</option>'
				}

				$("#tool").html(html).selectpicker('refresh');
	  		}
	    });
	}

	function load_select_vendor(){
		$("#tags").select2({
		  tags: true,
		  ajax: {
		  		url:'<?= base_url() ?>new/tools/transaksi/load_vendors/',
		  		type : 'GET',
		  		dataType:'json',
		  		delay:250,
		  		data:function(params){
		  			return {
		  				searchTerm: params.term,
		  			}
		  		},
		  		processResults: function(response){
		  			return {
		  				results:response
		  			}
		  		},
		  		cache: false,
		  },
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  },
		  templateResult: function (data) {
		    var $result = $("<span></span>");

		    $result.text(data.text);

		    if (data.newOption) {
		     	$result.append(" <em>(new)</em>");
		    }

		    return $result;
		  }
		});
	}

	function delete_this(id){

		if ( confirm('Apakah yakin menghapus ?')) {

			$.ajax({
				url: '<?= base_url() ?>new/tool_keluar/delete/'+id,
				dataType: 'json',
				success:function(resp){
					if (resp.status == 200) {
						load_tbl();
					}
				}
			});
		}


	}

	function saved_item(){
		var tool = $("#tool").val();
		if (tool == '0') {
			alert('Pilih Tool nya dahulu');
		} else {
			$.ajax({
				url: '<?php echo base_url('new/tool_keluar/save') ?>',
				method: 'POST',
				dataType: 'JSON',
				data: {
					kategori:$("#kategori").val(),
					tool:  tool	 ,
				},
				success:function(resp){
					if (resp.status == 200) {
						$("input[name='jml']").val('');
						$("input[name='kategori']").val('mekanik');
						select_items();
						load_tbl();
					}
				}
			});
		}
	}

	function load_tbl(){
		$('#tbl_detail').DataTable({
	        destroy: true,
	        "processing": true,
	        "serverSide": true,
	        ajax: {
	          url: "<?php echo base_url('/new/tool_keluar/load_data_meminjamnkan_detail/0') ?>"
	        },
	        "order": [
	          [0, 'asc']
	        ],
			"language": {
	            "lengthMenu": "Perhalaman _MENU_",
	            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
	        },
	        "columns": [
	        	{ "name": "ID", "searchable":false},
	        	{ "name": "TOOL_NAME" },
	        	{ "name": "TOOL_SPEC" },
	        	{ "name": "TOOL_INVENTARIS" },
	        	{ "name": "TOOL_JENIS" },
	         	{ className: "td-right","orderable": false, "targets": [ 10 ] }
	         ],
	        "iDisplayLength": 10,
	        "scrollX" : false,
	    });

	}



	// function sel_go(){
	// 	var  x = $("#tool").val();
	// 	alert(x);
	// }
</script>
