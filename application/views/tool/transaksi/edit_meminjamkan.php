<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<style type="text/css">
	.bootstrap-select > .dropdown-toggle{
		width:380px;
	}
</style>
<section class="content">
	<div class="col-sm-12 no-padding">
	</div>
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-header with-border">
						<div class="pull-left" style="max-width: -90px">
							<?php if($data->direction == 'masuk'){ ?>
								<h4 style="margin: 10px 0px 0px 0px !important; display: inline;">Peminjaman Internal Eksternal (Masuk)</h4>
							<?php }else{ ?>
								<h4 style="margin: 10px 0px 0px 0px !important; display: inline;">Peminjaman Internal Eksternal (Keluar)</h4>
							<?php } ?>
						</div>
						<div class="pull-right">
						</div>
					</div>
				<form class="form-horizontal" method="POST" >
					<div class="box-body">
						<div class="col-md-12">
							<?php if($data->direction == 'masuk'){ ?>
								<div class="form-group">
									<label class="col-sm-1">Kebutuhan Proyek</label>
									<div class="col-sm-9">
										<?= $data->pr_name ?>
									</div>
								</div>
							<?php } ?>
								<div class="form-group">
									<label class="col-sm-1">Vendor</label>
									<div class="col-sm-9">
										<?= $data->vendor ?>
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1">
										Vendor PIC
									</label>
									<div class="col-sm-3">
										<?= $data->pic_vendor ?>
									</div>
									<label for="endi" class="col-sm-2">
										Jabatan Vendor PIC
									</label>
									<div class="col-sm-3">
										<?= $data->pic_vendor_jabatan ?>
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1">
										NID / No KTP PIC
									</label>
									<div class="col-sm-3">
										<?= $data->no_ktp ?>
									</div>
									<label class="col-sm-2">
										Divisi
									</label>
									<div class="col-sm-3">
										<?= $data->divisi ?>
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1">
										Periode&nbsp;<span style="color:#dd4b39"></span>
									</label>
									<div class="col-sm-3">
										<?= $data->start_date ?>
									</div>
									<div class="col-sm-2" >
										<label for="endi">
											Sampai dengan&nbsp;<span style="color:#dd4b39">*</span>
										</label>
									</div>
									<div class="col-sm-2">
										<input autocomplete="off" type="text" name="end_date" id="end" class="form-control w-100" value="<?= $data->end_date ?>" placeholder="DD-MM-YYYY">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1">Keperluan Peminjaman</label>
									<div class="col-sm-9">
										<?= $data->keperluan ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1">Lampiran</label>
									<?php if($data->lampiran != ''){ ?>
									<div class="col-sm-9">
										<?= '<a href="'.base_url().'uploads/peminjaman_ie_file/'.$data->lampiran.'" download> Download File </a>' ?>
									</div>
									<?php } ?>
								</div>
						</div>
						<div class="col-md-12">
							<center><h5><b>Daftar Tool</b></h5></center>
							<!-- <br> -->
							<input type="checkbox" name="select-all" id="select-all" /> Check All
							<table class="table table-striped table-hover  text-center" id="tbl_detail">
							<?php if($data->direction == 'masuk'){ ?>
								<thead>
									<th width="10"></th>
									<th>Nama Tool</th>
									<th>Spec</th>
									<th>Jumlah</th>
									<th>File (Gambar / PDF)</th>
									<th>Kondisi</th>
								</thead>
							<?php }else{ ?>
								<thead>
									<th width="10">K</th>
									<th>Nama Tool</th>
									<th>Spec</th>
									<th>Kategori Tool</th>
									<th>Kondisi</th>
								</thead>
							<?php } ?>
								<tbody>
									
								</tbody>
							</table>
							<br>
							<div class="row">
								<div class="col-md-6">
									<!-- <button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
										<span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
									</button> -->
								</div>
								<div class="col-md-6">
									<button type="submit" class="btn btn-info pull-right">
										<span class="glyphicon glyphicon glyphicon-file"></span> Simpan & Print Berita Acara
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	<div style="clear: both;"></div>
</section>
<script type="text/javascript">
	$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	// $("#modal_add").on('hide.bs.modal', function(){
	//     $('#name').val('');
	// 	$('#spek').val('');
	// 	$('#jml').val('');
	// });

	$(document).ready(function() {
		load_select_vendor();
		load_tbl();
		select_items();
		$("#kategori").selectpicker();
	});


	function select_items(){

		$.ajax({
	  		url: '<?php echo base_url("/new/tool/get_tool_by_kategori_transaksi_ie") ?>',
			dataType: 'json',
			data: {
				kategori : $("#kategori").val()
			},
	  		success: function (response) {
	  			var html = '';


				if (response.length > 0) {
					html += '<option value="0">- Pilih List Tool -</option>';
					for (i = 0; i < response.length; i++) {
						
						value = response[i].id;
						html += '<option value='+value+'>'+response[i].name+' - '+response[i].spek+' - '+response[i].no_inventarisasi+'</option>'
					}
				} else {
					html += '<option value="0">- Tidak ada Tools di Kategori Ini -</option>'
				}

				$("#tool").html(html).selectpicker('refresh');
	  		}
	    });
	}

	function load_select_vendor(){
		$("#tags").select2({
		  tags: true,
		  ajax: {
		  		url:'<?= base_url() ?>new/tools/transaksi/load_vendors/',
		  		type : 'GET',
		  		dataType:'json',
		  		delay:250,
		  		data:function(params){
		  			return {
		  				searchTerm: params.term,
		  			}
		  		},
		  		processResults: function(response){
		  			return {
		  				results:response
		  			}
		  		},
		  		cache: false,		
		  },
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  },
		  templateResult: function (data) {
		    var $result = $("<span></span>");

		    $result.text(data.text);

		    if (data.newOption) {
		     	$result.append(" <em>(new)</em>");
		    }

		    return $result;
		  }
		});
	}

	function delete_this(id){

		if ( confirm('Apakah yakin menghapus ?')) {

			$.ajax({
				url: '<?= base_url() ?>new/tool_keluar/delete/'+id,
				dataType: 'json',
				success:function(resp){
					if (resp.status == 200) {
						load_tbl();
					}
				}
			});
		}


	}

	function saved_item(){
		var tool = $("#tool").val();
		if (tool == '0') {
			alert('Pilih Tool nya dahulu');
		} else {
			$.ajax({
				url: '<?php echo base_url('new/tool_keluar/save') ?>',
				method: 'POST',
				dataType: 'JSON',
				data: {
					kategori:$("#kategori").val(),
					tool:  tool	 ,
				},
				success:function(resp){
					if (resp.status == 200) {
						$("input[name='jml']").val('');
						$("input[name='kategori']").val('mekanik');
						select_items();
						load_tbl();
					}
				}
			}); 
		}
	}

	function load_tbl(){
		<?php if($data->direction == 'masuk'){ ?>
			var table = $('#tbl_detail').DataTable({
		        destroy: true,
		        "processing": true,
		        "serverSide": true,
		        ajax: {
		          url: "<?php echo base_url('/new/tool_masuk/load_detail_tool_masuk/').$data->id ?>"
		        },
		        "order": [
		          [0, 'asc']
		        ],
				"language": {
		            "lengthMenu": "Perhalaman _MENU_",
		            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
		        },
		        "columns": [
		         	{ className: "td-right","orderable": false, "targets": [ 10 ]},
		        	// { "name": "ID", "searchable":false},
		        	{ "name": "TOOL_NAME" },
		        	{ "name": "TOOL_SPEC" },
		        	{ "name": "QTY" },
		        	{ "name": "ITEM_FILES" },
		         	{ className: "td-right","orderable": false, "targets": [ 10 ]},
		         	// { className: "td-right","orderable": false, "targets": [ 10 ] }
		         ],
		        "iDisplayLength": 10,
		        "scrollX" : false,
		    });
		<?php }else{ ?>
			var table = $('#tbl_detail').DataTable({
		        destroy: true,
		        "processing": true,
		        "serverSide": true,
		        ajax: {
		          url: "<?php echo base_url('/new/tool_keluar/load_detail_tool_keluar/').$data->id ?>"
		        },
		        "order": [
		          [0, 'asc']
		        ],
				"language": {
		            "lengthMenu": "Perhalaman _MENU_",
		            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
		        },
		        "columns": [
		         	{ className: "td-right","orderable": false, "targets": [ 10 ]},
		        	// { "name": "ID", "searchable":false},
		        	{ "name": "TOOL_NAME" },
		        	{ "name": "TOOL_SPEC" },
		        	{ "name": "TOOL_JENIS" },
		         	{ className: "td-right","orderable": false, "targets": [ 10 ]},
		         	// { className: "td-right","orderable": false, "targets": [ 10 ] }
		         ],
		        "iDisplayLength": 10,
		        "scrollX" : false,
		    });
		<?php } ?>

		$('#select-all').on('click', function(){
	        var rows = table.rows({ 'search': 'applied' }).nodes();
	        $('input[type="checkbox"]', rows).prop('checked', this.checked);
	    });
	}

	function save_rusak(id){
		var x = $('#kondisi_'+id).val();
		alert(id+' '+x);
	}

    $('#tbl_detail tbody').on('change', 'input[type="checkbox"]', function(){
  
        if(!this.checked){
            var el = $('#select-all').get(0);           
            if(el && el.checked && ('indeterminate' in el)){
                el.indeterminate = true;
            }
        }
    });



	// function sel_go(){
	// 	var  x = $("#tool").val();
	// 	alert(x);
	// }
</script>