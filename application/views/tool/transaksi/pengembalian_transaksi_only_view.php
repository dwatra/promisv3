<section class="content">
	<div class="col-sm-12 no-padding">
		<br>
	</div>
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-header with-border">
						<div class="pull-left" style="max-width: -90px">
							<h4 style="margin: 10px 0px 0px 0px !important; display: inline;">

								<?php echo $trx->name ?></h4>


							<br/>
							<small>PENGECEKAN & PENGEMBALIAN ke Admin Pusat </small>
						</div>
						<div class="pull-right">
						</div>
					</div>
					<div class="box-body">
						<div class="row form-group">
							<div class="col-md-3">
							<label> Nama Proyek </label>
							</div>
							<div class="col-md-9">
								<input type="text" class="form-control" name="" value="<?= $trx->proyek?>" readonly>

							</div>
<!-- 							<div class="col-md-3">
							</div> -->





						<?php if ($pic_general): ?>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
							<label> Pic General </label>
							</div>
							<div class="col-md-9">
								<input type="text" class="form-control" name="" value="<?= $pic_general->row()->nama ?>" id="pic_general" readonly>

							</div>
<!-- 							<div class="col-md-3">
							</div> -->
						</div>
						<?php endif ?>
						<?php if ($pic_specific): ?>
						<div class="row form-group">
							<div class="col-md-3">
							<label> Pic Specific </label>
							</div>
							<div class="col-md-9">
								<input type="text" class="form-control" name="" value="<?= $pic_specific->row()->nama ?>" id="pic_specific" readonly>

							</div>
<!-- 							<div class="col-md-3">
							</div> -->
						</div>
						<?php endif ?>

						<?php if ($pic_special): ?>
						<div class="row form-group">
							<div class="col-md-3">
							<label> Pic Special </label>
							</div>
							<div class="col-md-9">
								<input type="text" class="form-control" name="" value="<?= $pic_special->row()->nama ?>" id="pic_special" readonly>

							</div>
<!-- 							<div class="col-md-3">
							</div> -->
						</div>
						<?php endif ?>
						<div class="row form-group">
							<div class="col-md-3">
							<label> STATUS PROYEK </label>
							</div>
							<div class="col-md-9">
								<?= $status ?>
							</div>
						</div>



							<br>

							<input type="text" class="" name="" id="filter">
							<button class="btn btn-default" onclick="load_data_detail()" type="button">
								<i class="glyphicon glyphicon-search"></i>
							</button>
							<!-- <input type="checkbox" onchange="value_all()" name="" id="cek_all"> <label for="cek_all">Aman Semua</label> 	 -->
							<table style="width:100%" class="table table-striped table-hover  text-center m-3" id="tblx">
								<thead>
									<th width="10">No.</th>
									<th>Nama Tool</th>
									<th>Spec</th>
									<th>Jenis</th>
									<th>Kategori</th>
									<th>Kembali </th>  <!--tidak ada == beli / roll-->
									<th>Inventaris </th>
									<th>Status Items</th> <!--ada / tidak ada-->
									<!-- <th></th> -->
								</thead>
								<tbody>

								</tbody>
							</table>
							<br>
						<div class="col-md-6">
							<!-- <button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
								<span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
							</button> -->
							<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#modal_proyek">
								<span class="glyphicon glyphicon-copy"></span> Salin Template
							</button> -->
						</div>
						<div class="col-md-6">


							<!-- <button type="button" class="btn btn-info pull-right" onclick="generate_ba()">
								<span class="glyphicon glyphicon glyphicon-file"></span> Simpan & Print Berita Acara
							</button> -->

							<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#modal_proyek">
								<span class="glyphicon glyphicon-copy"></span> Salin Template
							</button> -->
						</div>
					</div>

				</div>
			</div>
		</div>

	<div style="clear: both;"></div>

</section>
<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Paket</h4>
      </div>
      <div class="modal-body">
		<form method="POST">
			<label>Pilih Template Tool</label>
			<select  data-placeholder="Pilih..." tabindex="2" name="paket" id="paket" class="form-control" style="width: 100% !important">
				<?php foreach ($pakets as $p): ?>
					<option value="<?php echo $p->id ?>"><?php echo strtoupper($p->name) ?></option>
				<?php endforeach ?>
			</select>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" >Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>
 -->
<div class="modal fade" id="modal_add" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
      </div>
      <div class="modal-body">
		<form method="POST" class="form-horizontal" id="form-paket">
			<input type="hidden" id="paket_detail_id" value="0" name="">
			<div class="form-group w-100" id="select_tipe">
				<label for="invt" class="col-sm-3 control-label">
					Kategori&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<select  data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100" required style="width: 100%">
						<option value="mekanik">Mekanik</option>
						<option value="listrik">Listrik</option>
						<option value="instrument">Instrument</option>
						<option value="predictive">Predictive</option>
					</select>
				</div>
			</div>
			<div id="select_tool" class="form-group w-100">
				<label for="tool" class="col-sm-3 control-label">
					Tool&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<select  data-placeholder="Pilih..." tabindex="2" name="tool" id="tool" class="form-control w-100" required style="width: 100%">

					</select>
				</div>
			</div>
			<input type="hidden" id="current_stok" name="">
			<div class="form-group">
				<label class="col-sm-3 control-label">Jumlah</label>
				<div class="col-sm-9">
					<input id="jml" type="number" name="jml" class="form-control w-100">
				</div>
			</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn_save" onclick="add_new_item()" class="btn btn-primary" >Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_ba" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Simpan & Print Ba</h4>
      </div>
      <div class="modal-body">
			<table class="table">
				<thead>
					<tr>
						<th colspan="7" class="text-center"> Table Barang Hilang</th>
					</tr>
					<tr>
						<th>Nama Barang</th>
						<th>No Inv</th>
						<th>Spec</th>
						<th>Merk</th>
						<th>Jumlah</th>
						<th>Keterangan</th>
						<th>Penyebab</th>
					</tr>
				</thead>
				<tbody id="tbl_load_ilang">

				</tbody>
				<tfoot id="krono" style="display:none;">
					<tr>
						<td colspan="3"> Kronologis </td>
						<td colspan="4"><textarea id="txtkronologis" readonly class="form-control"><?= $trx->kronologis ?></textarea></td>
					</tr>
				</tfoot>
				<tfoot id="usulan" style="display:none;">
						<td colspan="3"> Usulan </td>
						<td colspan="4"><textarea id="txtusulan" readonly class="form-control"><?= $trx->usulan ?></textarea></td>
				</tfoot>
			</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn_save" onclick="process_ba()" class="btn btn-primary" >Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">

	$(document).ready(function() {
		load_data_detail();
		$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
		$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	});

	function generate_ba(){


		$("#modal_ba").modal('show');
		load_tbl_ilang();
	}

	function process_ba(){

		window.open('<?= base_url() ?>new/transaksi_kembali/berita_acara/<?= $trx->pr_id ?>', '_blank');

	}

	function load_tbl_ilang(){
		$.ajax({
			url: '<?= base_url() ?>new/transaksi_kembali/load_barang_hilang/'+<?= $trx->pr_id ?>+'/view',
			type: 'POST',
			dataType: 'html',
			success:function(resp){
				$("#tbl_load_ilang").html(resp);
				if ($(".itungx").length) {
					$("#krono").show();
					$("#usulan").show();
				} else {
					$("#krono").hide();
					$("#usulan").hide();
				}
			}
		});
	}
	function load_data_detail(){
		var filter = $("#filter").val();
  	var datatable = $('#tblx').DataTable({
	    destroy: true,
	        "processing": true,
	        "serverSide": true,
	        ajax: {
	          url: "<?php echo base_url('/new/transaksi_kembali/load_data_detail/') ?>"+<?= $trx->pr_id ?>+'/view',
	        },
	        "order": [
	          [0, 'desc']
	        ],
	        "dom": "<'row'<'col-sm-12'tr>>" +
					/**/"<'row'<'col-sm-2'l><'col-sm-4'i><'col-sm-6'p>>",
			"language": {
	            "lengthMenu": "Perhalaman _MENU_",
	            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
	        },
	        "columns": [
	        	{ "name": "A.ID" },
	        	{ "name": "A.TOOL_NAME" },
	        	{ "name": "A.TOOL_SPEC" },
	        	{ "name": "B.JENIS" },
	        	{ "name": "B.KATEGORI" },
	        	{ "name": "A.TOOL_INVENTARIS" },
	        	{ "name": "A.TOOL_KEMBALI" },
	        	{ "name": "A.KETERANGAN_KEMBALI" },
	        	// { "orderable": false, "searchable":false},
	         	// { className: "countdable td-right","orderable": false }
	         ],
	        "iDisplayLength": 10,
	        "scrollX" : false,
	    });

	    if(filter != ''){
	    	datatable.search(filter).draw()
	    }
	}


</script>
