<section class="content">
	<div class="col-sm-12 no-padding">
	</div>
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-header with-border">
						<div class="pull-left" style="max-width: -90px">
							<h4 style="margin: 10px 0px 0px 0px !important; display: inline;">Peminjaman Internal Eksternal (Masuk)</h4>
						</div>
						<div class="pull-right">
						</div>
					</div>
					<div class="box-body">
						<div class="col-md-12">
							<form class="form-horizontal" method="POST" enctype="multipart/form-data" id="post" action="<?= base_url() ?>new/tool_masuk/progress_peminjaman/">
								<div class="form-group">
									<label class="col-sm-1 control-label">Kebutuhan Proyek</label>
									<div class="col-sm-9">
										<!-- <input id="jml" type="text" name="vendor" class="form-control w-100"> -->
										<select class="form-control w-100" id="proyeks" name="proyek_id">

											<?php foreach ($proyek as $k): ?>
												<option value="<?= $k->ID_PROYEK ?>"><?= $k->NAMA_PROYEK ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">Vendor</label>
									<div class="col-sm-9">
										<!-- <input id="jml" type="text" name="vendor" class="form-control w-100"> -->
										<select class="form-control w-100" id="tags" name="vendor_name">

										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
										Vendor PIC&nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_pic" class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
										Jabatan Vendor PIC
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_pic_level" class="form-control w-100" >
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
										NID / No KTP PIC &nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_ktp" class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
										Divisi
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text"  name="vendor_div" class="form-control w-100" >
									</div>
								</div>
								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
										Tgl Mulai Pinjam &nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text" name="start" id="start"  class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
										Lampiran
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="file" name='berkas' class="form-control w-100" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">Keperluan Peminjaman</label>
									<div class="col-sm-9">
										<!-- <input id="jml" type="text" name="vendor" class="form-control w-100"> -->
										<textarea name="keperluan" class="form-control" rows="5" cols="80"></textarea>
									</div>
								</div>

							</form>
						</div>
						<div class="col-md-12">
							<hr>
							<center><h5>Daftar Tool</h5></center>
							<br>
							<table class="table table-striped table-hover  text-center m-3" id="tbl_detail">
								<thead>
									<th width="10">No.</th>
									<th>Nama Tool</th>
									<th>Spec</th>
									<th width="10">Jumlah</th>
									<th	>File (Gambar / PDF) </th>
									<th></th>
								</thead>
								<tbody>

								</tbody>
							</table>
							<br>
							<div class="row">
								<div class="col-md-6">
									<button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
										<span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
									</button>
								</div>
								<div class="col-md-6">
									<button type="submit" form="post" class="btn btn-success pull-right">
										<span class="glyphicon glyphicon glyphicon-ok"></span> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div style="clear: both;"></div>
</section>
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
      </div>
      <div class="modal-body">
		<form method="POST" class="form-horizontal" id="form-paket" id="form_paket">
			<div class="form-group w-100">
				<label for="tool" class="col-sm-3 control-label">
					Tool&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<input id="tool" type="text" name="tool" class="form-control w-100">
				</div>
			</div>
			<div class="form-group w-100">
				<label for="spek" class="col-sm-3 control-label">
					Spek&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<input id="spek" type="text" name="spek" class="form-control w-100">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Jumlah&nbsp;<span style="color:#dd4b39">*</span></label>
				<div class="col-sm-9">
					<input id="jml" type="number" name="jml" class="form-control w-100">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Foto &nbsp;<span style="color:#dd4b39">*</span></label>
				<div class="col-sm-9">
					<input type="file" class="form-control" id="file" name="images" value="">
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="do_add_item()" class="btn btn-primary" >Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	// $('table').DataTable()
	$("#modal_add").on('hide.bs.modal', function(){
	    $('#name').val('');
		$('#spek').val('');
		$('#jml').val('');
	});
	function fsubmit(){
		$('#form').submit();
	}
	function load_tbl(){
		$('#tbl_detail').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": true,
        ajax: {
          url: "<?php echo base_url('/new/tool_masuk/load_temporary_data_peminjaman/0') ?>"
        },
        "order": [
          [0, 'asc']
        ],
		"language": {
            "lengthMenu": "Perhalaman _MENU_",
            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
        },
        "columns": [
        	{ "name": "ID", "searchable":false},
        	{ "name": "TOOL_NAME" },
        	{ "name": "TOOL_SPEC" },
        	{ "name": "ITEM_FILES" },
        	{ "name": "QTY" },
         	{ className: "td-right","orderable": false, "targets": [ 10 ] }
         ],
        "iDisplayLength": 10,
        "scrollX" : false,
    });

	}

	$(document).ready(function() {
		load_tbl();
		$("#tags").select2({
		  tags: true,
		  ajax: {
		  		url:'<?= base_url() ?>new/tools/transaksi/load_vendors/',
		  		type : 'GET',
		  		dataType:'json',
		  		delay:250,
		  		data:function(params){
		  			return {
		  				searchTerm: params.term,
		  			}
		  		},
		  		processResults: function(response){
		  			return {
		  				results:response
		  			}
		  		},
		  		cache: false,
		  },
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  },
		   templateResult: function (data) {
		    var $result = $("<span></span>");

		    $result.text(data.text);

		    if (data.newOption) {
		      $result.append(" <em>(new)</em>");
		    }

		    return $result;
		  }
		});

	});

	function do_add_item(){
		 var formdata = new FormData();
		 formdata.append('spek', $("#spek").val() );
		 formdata.append('tool', $("#tool").val() );
     formdata.append('jml', $("#jml").val() );
     formdata.append('category_image', $('#file')[0].files[0] );
		 $.ajax({
				url: '<?= base_url() ?>new/tool_masuk/progress_add_item/',
				type: 'POST',
				dataType: 'json',
				data: formdata,
				processData: false,
	      contentType: false,
				success:function(resp){
					if (resp.status==200) {
						load_tbl();
						$("#form_paket").trigger('reset');
						$("#modal_add").modal('hide');
						$("input[name='tool']").val('');
					}
				}
		});
	}

	function delete_this(id){

		$.ajax({
			url: '<?= base_url() ?>new/tool_masuk/delete_item_peminjaman/',
			type: 'POST',
			dataType: 'json',
			data: {
				id : id
			},
			success:function(resp){
				load_tbl();
			}
		});


	}
</script>
