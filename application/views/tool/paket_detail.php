<section class="content">
   <div class="col-sm-12 no-padding">
      <br>
   </div>
   <div class="col-sm-12 no-padding">
      <div class="box box-default">
         <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
               <div class="box-header with-border">
                  <div class="pull-left" style="max-width: -90px">
                     <h4 style="margin: 10px 0px 0px 0px !important; display: inline;"><?php echo $detail->name ?></h4>
                     <br/>

                  </div>
                  <div class="pull-right">
                  </div>
               </div>
               <div class="box-body">


                  <?php if($detail->pr_id == 0){ ?>
                  <div class="row form-group">
                     <div class="col-md-2">
                        <label> Pilih Template Paket </label>
                     </div>
                     <div class="col-md-6">
                        <select class="select2 form-control w-80" id="paket_id">
													 <?php if (!empty($paket)){ ?>
													 <?php foreach($paket as $data_paket){ ?>
													 <option value="<?= $data_paket->id ?>"> <?= $data_paket->name ?> </option>
													 <?php } ?>
												   <?php } else { ?>
													 <option>Belum ada Paket yang lain</option>
													 <?php } ?>
                        </select>
                     </div>
                     <div class="col-md-3">
                        <button onclick="copy_paket()" type="button" class="btn btn-success btn-sm">
                        <i class="glyphicon-file glyphicon"></i>&nbsp;
                        Copy Paket
                        </button>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="row form-group">
                     <div class="col-md-2">
                        <label> Beri nama Paket ini </label>
                     </div>
                     <div class="col-md-6">
                        <input type="text" id="paket_name" class="form-control " value="<?php echo $detail->name ?>" name="">
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-md-2">
                        <label> Keterangan </label>
                     </div>
                     <div class="col-md-6">
                        <textarea id="txt_area" class="form-control"><?php echo $detail->keterangan ?></textarea>
                     </div>
                  </div>
                  <center>
                     <h5></h5>
                  </center>
                  <br>
                  <table class="table table-striped table-hover  text-center m-3" id="table_detail">
                     <thead>
                        <th width="10">No.</th>
                        <th>Nama Tool</th>
                        <th>Spec</th>
                        <th>Jenis Tool</th>
                        <th>Kategori Tool</th>
                        <th>Quantity</th>
                        <th>PIC Name</th>
                        <th></th>
                     </thead>
                     <tbody>
                     </tbody>
                  </table>
                  <br>
                  <div class="row">
                    <?php if($detail->pr_id == 0){ ?>
                     <div class="col-md-6">
                        <button class="btn btn-warning" onclick="do_add_item()">
                        <span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
                        </button>
                     </div>
                     <div class="col-md-6">
											  <button class="btn btn-info pull-right" onclick="save_paket_edit()">
                        <span class="glyphicon glyphicon glyphicon-ok"></span> Simpan Perubahan
                        </button>
                     </div>
                   <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<div class="modal fade" id="modal_add" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
         </div>
         <div class="modal-body">
            <form method="POST" class="form-horizontal" action="<?php echo base_url('new/paket/additem') ?>" id="form-paket">
               <input type="hidden" name="paket" value="<?php echo $paket->id ?>">
               <div class="form-group w-100" id="select_tipe">
                  <label for="invt" class="col-sm-3 control-label">
                  Kategori&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <input type="hidden" id="paket_detail_id" value="0" name="">
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="jenis" id="jenis" class="form-control w-100" required style="width: 100%">
                        <option value="mekanik">Mekanik</option>
                        <option value="listrik">Listrik</option>
                        <option value="instrument">Instrument</option>
                        <option value="predictive">Predictive</option>
                     </select>
                  </div>
               </div>
               <div class="form-group w-100" id="select_tipe">
                  <label for="invt" class="col-sm-3 control-label">
                  Jenis&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select onchange="search_pic(this.value)" data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100" required style="width: 100%">
                        <option value="general">General</option>
                        <option value="special">Special</option>
                        <option value="specific">Specific</option>
                     </select>
                  </div>
               </div>
               <div class="form-group w-100" id="select_person" style="display:none;">
                  <label for="invt" class="col-sm-3 control-label">
                  Pic Tool
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="karyawan" id="karyawan" class="form-control w-100" required style="width: 100%">
                        <?php foreach ($kp2 as $person): ?>
                        <option value="<?= $person->nid ?>xXx<?= $person->nama ?>"><?= $person->nama ?></option>
                        <?php endforeach ?>
                     </select>
                  </div>
               </div>
               <div id="select_tool" class="form-group w-100">
                  <label for="tool" class="col-sm-3 control-label">
                  Tool&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="tool" id="tool" class="form-control w-100" required style="width: 100%">
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 control-label">Jumlah</label>
                  <div class="col-sm-9">
                     <input id="jml" type="number" name="jml" class="form-control w-100">
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button type="button" id="btn_save" onclick="add_new_item()" class="btn btn-primary" >Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>
<script type="text/javascript">
var paket_id = <?= $detail->id ?>;
$(document).ready(function() {
  load_detail_data();
  $.ajax({
   url: '<?php echo base_url("/new/tool/get_tool_by_kategori?jenis=mekanik") ?>',
   type: 'GET',
   dataType: 'json',
   success : function(data){
     var html = '';
     for (var i = 0; i < data.length; i++) {
       html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
     }
     $('#tool').html(html);
     $('#tool').trigger('change');
   }
 });
});

function load_detail_data(){

  var datatable= $('#table_detail').DataTable({
      destroy: true,
       "processing": true,
       "serverSide": true,
       ajax: {
         url: "<?php echo base_url('/new/paket/load_data_paket_detai/') ?>"+paket_id+'/edit',
       },
       "order": [
         [0, 'desc']
       ],
       "language": {
         "lengthMenu": "Perhalaman _MENU_",
         "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
       },
       /* <th width="10">No.</th>
       <th>Nama Tool</th>
       <th>Spec</th>
       <th>Jenis Tool</th>
       <th>Kategori Tool</th>
       <th width="10">Jumlah</th>
       <th >Status</th>
       <th >PIC Name</th> */
       "columns": [
        { "name": "A.ID" },
        { "name": "A.TOOL_NAME" },
        { "name": "A.TOOL_SPEC" },
        { "name": "B.JENIS" },
        { "name": "B.KATEGORI" },
        { "name": "A.QTY" },
        { "name": "A.PIC_NAME" },
        { className: "countdable td-right","orderable": false, "targets": [ 10 ] }
        ],
       "iDisplayLength": 10,
       "scrollX" : false,
   });
}

function copy_paket(){
  $.ajax({
    url: '<?= base_url() ?>new/paket/copy_paket/',
    type: 'POST',
    dataType: 'json',
    data: {
      to_paket_id : paket_id,
      from_paket_id : $("#paket_id").val()
    },
    success:function(res){
      if (res.status == 200) {
         alert(res.msg);
      } else {
         alert(res.msg);
      }
      load_detail_data();
    }
  });
}

function do_add_item(){
  $("#modal_add").modal('show');
}

function search_pic(val){
 if (val == 'general') {
   $("#select_person").hide();
 } else {
   $("#select_person").show();
 }
}

function load_tool_based_on_kategori(id = null){
 $.ajax({
   url: '<?php echo base_url("/new/tool/get_tool_by_kategori") ?>',
   type: 'GET',
   dataType: 'json',
   data : {
     kategori : $('#kategori').val(),
     jenis : $("#jenis").val(),
   },
   success : function(data){
     var html = '';
     if (data.length > 0) {
       html = '<option value="0" disabled> Ada Tools </option>';
       for (var i = 0; i < data.length; i++) {
         html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
       }
       $('#tool').html(html);
       if (id != null) {
         $("#tool").val(id);
       }
       $('#tool').trigger('change');
     } else {
       html = `<option value="0"> Tidak ada Tool </option>`;
       $('#tool').html(html);
     }
     $('#tool').trigger('change');
   }
 })
}

function add_new_item(){

 var jenis = $("#jenis").val();
 if (jenis == 'general' || jenis == 'special') {
   var nid = null;
 } else {
   var nid = $("#karyawan").val();
 }
 var kat = $("#kategori").val();
 $.ajax({
   url: '<?= base_url() ?>new/paket/additem/'+paket_id,
   type: 'POST',
   dataType: 'json',
   data: {
     tool  : $("#tool").val(),
     jml   : $("#jml").val(),
     idx   : $("#paket_detail_id").val(),
     nid   : nid,
     jenis : jenis,
     kat   : kat,
   },
   success:function(resp){
     if (resp.status == 200) {
       load_detail_data();
       $("#modal_add").modal('hide');
     }
   }
 });
}

function save_paket_edit(){

  $.ajax({
    url: '<?=  base_url() ?>new/paket/add_paket/'+paket_id,
    method: 'POST',
    dataType: 'json',
    data: {
      name : $("#paket_name").val(),
      // project : $("#txt_area").val(),
      keterangan : $("#txt_area").val()
    },
    success:function(resp){
       if (resp.status==200) {
         alert(resp.msg);
         window,location.href="<?= base_url() .'new/paket/' ?>";
       }
    }
  });

}

$("#modal_add").on('hide.bs.modal', function(){
     $("#paket_detail_id").val('0');
     $('#tool').val('');
     $('#jml').val('');
     $('#kategori').val('');
     $("#myModalLabel").html('Tambah Item Paket');
});
$('#kategori').on('change', function(event) {
 load_tool_based_on_kategori();
});

$('#jenis').on('change', function(event) {
 load_tool_based_on_kategori();
});

function edit2(id){
 $.ajax({
   url: '<?= base_url("new/tool/load_detail_item_paket/") ?>'+id,
   dataType: 'json',
   success:function(resp){
     // console.log(resp);
     $("#modal_add").modal('show');
     search_pic(resp.jenis);
     $("#paket_detail_id").val(resp.id);
     $('#kategori').val(resp.kategori);
     $('#kategori').trigger('change');
     $('#jenis').val(resp.jenis);
     $('#jenis').trigger('change');
     load_tool_based_on_kategori(resp.tool_id);
     $('#jml').val(resp.qty);
     $("#myModalLabel").html('Update Item Paket');
   }
 });
}

</script>
