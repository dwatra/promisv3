<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.select2-selection--multiple{
		border-radius: 10px !important;
		padding-left: 5px;
		min-height: 30px;
	}
</style>
<section class="content" style="padding-bottom: 50px">

	<!-- end -->
	<div class="box box-default">
		<div class="box-body">
			<div class="row">
				<div class="col-lg-12">
					<h3 align="center">Daftar Proyek Yamg Lagi Jalan di <?php echo date('Y') ?></h3>

					<table class="table table-striped table-hover">
						<thead>
							<th>No.</th>
							<th>Nama Proyek</th>
							<th>Rencana Mulai</th>
							<th>Rencana Selesai</th>
							<!-- <th>Status</th> -->
						</thead>
						<tbody>
							<?php foreach ($proyek as $p => $pr): ?>
								<tr>
									<td><?php echo $p+1 ?></td>
									<td>
										<a href="<?= base_url() ?>transaction_site_detail/<?= $pr->id_proyek ?>" target="_blank" ><?php echo $pr->nama_proyek ?></a>		
									</td>
									<td><?php echo $pr->tgl_rencana_mulai ?></td>
									<td><?php echo $pr->tgl_rencana_selesai ?></td>

								
									
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- panel barchart -->
	
</section>

<script type="text/javascript">
	$('table').DataTable()
	<?php //if(!empty($notif['proyek'] or !empty($notif['kalibrasi']))): ?>
		$('#notifmodal').modal('show')
	<?php //endif ?>
</script>
