<section class="content-header">
	<h1 class="text-white">Transaksi</h1>
</section>
<style type="text/css">
	.td-right{
		text-align: right;
	}
</style>


<section class="content">
	<div class="box box-default">
			<div class="box-header">
				<div class="pull-right"></div>
				<div class="pull-left"></div>
			</div>
		    <div class="box-body">
				<div class="px-4 py-4">
					<div class="row pd-4">
						<div class="col-md-12 text-center">
							<h5><strong>Daftar Transaksi</strong></h5>
						</div>
					</div>
					<table class="table table-striped table-hover" id="tbl_trx">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Nama Proyek</th>
								<th class="text-center">Rencana Mulai</th>
								<th class="text-center">Rencana Selesai</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<?php foreach ($trx as  $i=> $t): ?>
								<tr>
									<td><?php echo $i+1 ?></td>
									<td class="text-left">

										<?php if ($t->status == 'disetujui'){ ?>
										<a href="<?php echo base_url('new/Transaksi_kembali/pengembalian_transaksi/'.$t->id_proyek) ?>"><?php echo $t->nama_proyek ?></a>
										<?php } else {
											 echo $t->nama_proyek;
											}
										?>
									</td>
									<td><?php echo $t->tgl_rencana_mulai ?></td>
									<td><?php echo $t->tgl_rencana_selesai ?></td>
									<td>
										<?php
											if ($t->status == 'menunggu persetujuan') {
										?>
										<span class="label label-warning">
											<i class="glyphicon glyphicon-refresh"></i>
											Menunggu Persetujuan
										</span>
										<?php } else if($t->status == 'ditolak') { ?>
											<span class="label label-danger">
												<i class="glyphicon glyphicon-remove"></i>
												Ditolak
											</span>
										<?php } else if ($t->status == 'disetujui'){ ?>
											<span class="label label-success">
												<i class="glyphicon glyphicon-check"></i>
												Diterima
											</span>
										<?php } else { ?>
											<span class="label label-success">
												<i class="glyphicon glyphicon-question-sign"></i>
												Unknown
											</span>
										<?php } ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
		    </div>
		</div>
	<div class="box box-default">
			<div class="box-header">
				<div class="pull-right"></div>
				<div class="pull-left"></div>
			</div>
		    <div class="box-body">
				<div class="px-4 py-4">
					<div class="row pd-4">
						<div class="col-md-4">
							<div class="input-group">
								<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
								<span class="input-group-btn">
									<button type="submit" class='btn btn-default btn-sm' title="Filter"><span class="glyphicon glyphicon-search"></span></button>
									<button type="button" class="btn waves-effect btn-sm btn-default" title="Reset"><span class="glyphicon glyphicon-refresh"></span></button>
								</span>
							</div>

						</div>
						<div class="col-md-7"></div>
						<div class="col-md-1">
							<div class="dropdown" style="display:inline">
								<a  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="display:inline-block;" class=" btn btn-warning float-right dropdown-toggle">
									<span class="glyphicon glyphicon glyphicon-plus"></span> Baru
								</a>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuNew" style="min-width: 10px; margin-top:-20px">
									<li><a href="<?php echo base_url('new/tool_masuk') ?>" class="waves-effect " ><span class="glyphicon glyphicon-plus"></span> Masuk</a></li>
									<li><a href="<?php echo base_url('new/tool_keluar') ?>" class="waves-effect " ><span class="glyphicon glyphicon-plus"></span> Keluar</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-12 text-center">
							<h5><strong>Peminjaman Internal Eksternal</strong></h5>
						</div>
					</div>
					<table class="table table-striped table-hover" id="ie">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Vendor</th>
								<th class="text-center">Nama Proyek</th>
								<th class="text-center">Vendor PIC </th>
								<th class="text-center">Vendor ID / NID / KTP  </th>
								<th class="text-center">Tgl Mulai</th>
								<th class="text-center">Tgl Selesai</th>
							</tr>
						</thead>
						<tbody class="text-center">

						</tbody>
					</table>
				</div>
		    </div>
		</div>
</section>
<script type="text/javascript">
    var ie = $('table#ie').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": true,
        ajax: {
          url: "<?php echo base_url('/new/tool/load_io') ?>"
        },
        "order": [
          [0, 'asc']
        ],
        "dom": "<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-2'l><'col-sm-4'i><'col-sm-6'p>>",
		"language": {
            "lengthMenu": "Perhalaman _MENU_",
            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
        },
        "iDisplayLength": 10,
        "scrollX" : false,
    });
    $('.dataTables_filter').css('display', 'none')
    $('#filter').keyup(function(){
         ie.search($(this).val()).draw();
    })
    $('#tbl_trx').DataTable();
	$('input[name="dates"]').daterangepicker();

</script>
