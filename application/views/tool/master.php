<section class="content-header">
	<h1 class="text-white">Master Tool</h1>
</section>
<style type="text/css">
	.td-right{
		text-align: right;
	}
</style>
<section class="content">
	<div class="box box-default">
		    <div class="box-body">
				<div class="row pd-4">
					<div class="col-md-2">
						<div class="input-group">
							<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button type="submit" class='btn btn-default btn-sm' title="Filter"><span class="glyphicon glyphicon-search"></span></button>
								<button type="button" class="btn waves-effect btn-sm btn-default" title="Reset"><span class="glyphicon glyphicon-refresh"></span></button>
							</span>
						</div>
						
					</div>
					<div class="col-md-1">
						<a href="<?php echo base_url('/new/tool/new') ?>" class=" btn btn-warning">
							<span class="glyphicon glyphicon-plus"></span> Tool Baru
						</a>	
					</div>
					
					<div class="col-md-1">
						<a class="btn btn-primary" download="template_tool" href="<?php echo base_url('assets/excel/template_paket_pjbs.xlsx') ?>" >
							<span class="glyphicon glyphicon-download"></span> Download
						</a>
					</div>
					<form id="form-upload" enctype="multipart/form-data" method="POST">
						<div class="col-md-1" style="margin-top: -5px;">
							<span class="btn btn-upload fileinput-button">
						        <i class="glyphicon glyphicon-upload"></i>
						        <span>import</span>
						        <input id="fileimport" name="fileimport" type="file" accept=".xls, .xlsx, .csv" onchange="upload()">
						    </span>
						</div>
					</form>
					<div class="col-md-3">
						<div class="form-group row" style="width: 100%">
							<!-- <label for="kategori" class="col-sm-2 control-label" style="margin-top: 3px;">
								Kategori
							</label> -->
							<div class="col-sm">
								<select  data-placeholder="Pilih..." tabindex="2" id="jenis" class="form-control w-100">
								
									<option value="0"> No Filter </option>
									<option value="special">Special</option>
									<option value="specific">Specific</option>
									<option value="general">General</option>
									<option value="consumable">Consumable</option>
								</select>
							</div>							
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group row" style="width: 100%">
							<!-- <label for="kategori" class="col-sm-2 control-label" style="margin-top: 3px;">
								Jenis
							</label> -->
							<div class="col-sm">
								<select  data-placeholder="Pilih..." tabindex="2" id="kategori" class="form-control w-100">
									<option value="0"> No Filter </option>
									<option value="listrik">Listrik</option>
									<option value="mekanik">Mekanik</option>
									<option value="instrument">Instrument</option>
									<option value="predictive">Predictive</option>
									
								</select>
							</div>							
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="text-center">No</th>
							<th class="text-center">Nama Tool</th>
							<th class="text-center">Merk</th>
							<th class="text-center">Spesifikasi</th>
							<th class="text-center">No Inventaris</th>
							<th class="text-center">Jenis</th>
							<th class="text-center">Kategori</th>
							<th class="text-center">Tgl Perolehan</th>
							<th class="text-center">Kondisi</th>
							<th class="text-center">Utilitas</th>
							<th class="text-center"></th>
						</tr>
					</thead>
					<tbody class="text-center">
						
					</tbody>
				</table>
		    </div>
		</div>
</section>
<script type="text/javascript">

	$(document).ready(function() {
		load_datatable();	
	});

	function upload() {
        var dataForm = new FormData($('#form-upload')[0]);
        var url = '<?= base_url() . "new/tool/upload_file" ?>';
        $('#fileimport').prop('disabled', true);
        $.ajax({
            url: url,
            type: 'POST',
            data: dataForm,
            enctype: 'multipart/form-data',
            processData: false, // Important!
            contentType: false,
            cache: false,
            success: function (response) {
                if (response) {
                    // $.growl.notice({message: 'File telah di upload.'});
                    alert('File telah di upload.');
                    location.reload(); 
                } else {
                    // $.growl.error({message: 'Error, Silakan dicoba lagi.'});
                    alert('File gagal di upload, silahkan coba lagi.');
                }
            }
        }).done(function () {
            $('#fileimport').prop('disabled', false);
        });
    }

	
    $('.dataTables_filter').css('display', 'none')
    $('#filter').on('change', function(){
    	load_datatable();
    })
    $('#kategori').on('change', function(){
    	load_datatable();
    })
    $('#jenis').on('change', function(){
    	load_datatable();
    })

    function load_datatable(){
		var jenis  = $("#kategori").val();
		var kat    = $("#jenis").val();
		var search = $("#filter").val();

    	var datatable= $('table').DataTable({
	        destroy: true,
	        "processing": true,
	        "serverSide": true,
	        ajax: {
	          url: "<?php echo base_url('/new/tool/load_data/') ?>"+jenis+'/'+kat
	        },
	        "order": [
	          [0, 'desc']
	        ],
	        "dom": "<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-2'l><'col-sm-4'i><'col-sm-6'p>>",
			"language": {
	            "lengthMenu": "Perhalaman _MENU_",
	            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
	        },
	        "columns": [
	        	// { "orderable": false, "searchable":false},
	        	{ "name": "A.ID" },
	        	{ "name": "A.NAME" },
	        	{ "name": "A.MERK" },
	        	{ "name": "A.SPEK" },
	        	{ "name": "B.NO_INVENTARISASI" },
	        	{ "name": "A.JENIS" },
	        	{ "name": "A.KATEGORI" },
	        	{ "name": "B.TGL_PEROLEHAN" },
	        	{ "name": "B.KONDISI" },
	        	{ "name": "B.UTILITAS" },
	         	{ className: "td-right","orderable": false, "targets": [ 10 ] }
	         ],
	        "iDisplayLength": 10,
	        "scrollX" : false,
	    });

    	if (search != null || search != '') {
    		datatable.search(search).draw();
    	}

    }

</script>
