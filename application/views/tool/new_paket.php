<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Master Paket</h1>
</section>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.select2-selection--multiple{
		border-radius: 10px !important;
		padding-left: 5px;
		min-height: 30px;
	}
	.my-navbar {
        background: #27ae60;
        position:absolute;
        top:0;
        width: 100%;
        height: 50px;
    }
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?= base_url() ?>new/paket/add_paket/">
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">
						Nama Paket&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="name" id="name" class="form-control w-100" >
					</div>
				</div>
				<div class="form-group" style="display:none;">
					<label for="project" class="col-sm-3 control-label">
						Proyek
					</label>
					<div class="col-sm-8">
						<!-- <input autocomplete="off" type="text" name="" id="" class="form-control w-100" > -->
<!-- 						<select data-placeholder="Pilih..." tabindex="2" name="project" id="project" class="form-control w-100">
							<option value="0"> Untuk Seluruh Proyek </option>
						</select> -->

						<input type="hidden" name="project" id="project" value="0">
					</div>
				</div>
				<div class="form-group">
					<label for="project" class="col-sm-3 control-label">
						Keterangan
					</label>
					<div class="col-sm-8">
						<!-- <input autocomplete="off" type="text" name="" id="" class="form-control w-100" > -->
						<textarea class="form-control" name="keterangan"></textarea>
					</div>
				</div>
				<input type="hidden" id="numberx">
				<div class="form-group add_new_tool">
					<label for="tool" class="col-sm-3 control-label">
						Tool
					</label>
					<div class="col-sm-6">
						<select data-placeholder="Pilih..." tabindex="2" name="tool[]" id="tool" class="form-control w-100">
							<?php foreach ($tools as $tool): ?>
								<option value="<?php 
								echo $tool->ID.'xXx'.$tool->NAME.'xXx'.$tool->SPEK ?>"><?php echo strtoupper($tool->NAME);
								echo ($tool->SPEK != '') ? ' - '.strtoupper($tool->SPEK) : ''; 
							?>
							</option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-2">
						<button class="btn btn-success" type="button" onclick="add_new_tool()">
							<i class="glyphicon glyphicon-plus"></i>
						</button>
						<button class="btn btn-danger" type="button" onclick="delete_new_tool()">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</section>
<script type="text/javascript">
	function add_new_tool(){

		var html  = $(".add_new_tool:last");
		$.ajax({
			url: '<?= base_url() ?>'+'new/Paket/add_new_item_in_paket_new/',
			type: 'GET',
			dataType: 'html',
			success:function(response){

				var current_nth = $("#numberx").val($(".add_new_tool").length);
				var str = '<div class="form-group add_new_tool">'+
								'<label for="tool" class="col-sm-3 control-label">'+
									' '+
								'</label>'+
								'<div class="col-sm-6">'+
									'<select data-placeholder="Pilih..." tabindex="2" name="tool[]" class="form-control w-100 selectpicker_x" data-live-search="true">'+
									response+
									'</select>'+
								'</div>'+
								'<div class="col-sm-2">'+
								'</div>'+
					       '</div>';
				html.after(str);
				$(".selectpicker_x").selectpicker();
			}
		});
	}

	function delete_new_tool(){

		var number = $("#numberx").val()
		var length = $(".add_new_tool").length;
		if (length == 1) {
			alert('Cannot delete anymore!');
		} else {
			$(".add_new_tool:last").remove();
			$("#numberx").val(length);
		}

	}

</script>