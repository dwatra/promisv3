

<section class="content">
   <div class="col-sm-12 no-padding">
      <?php
         echo $this->auth->GetTabRab($id_proyek, null);
         ?>
      <?php $key = 0; ?>
   </div>
   <div class="col-sm-12 no-padding">
   <div class="box box-default">
      <div class="tab-content">
         <div class="tab-pane active" id="tab_1">
            <div class="box-header with-border">
               <div class="pull-left" style="max-width: -90px">
                  <h4 style="margin: 10px 0px 0px 0px !important; display: inline;"><?php echo $project['nama_proyek'] ?></h4>
                  <br/>
                  <small>PEMBERI PEKERJAAN : <?php echo $project['pemberi_pekerjaan'] ?> </small>
               </div>
               <div class="pull-right">
               </div>
            </div>
            <div class="box-body">
               <?php /* if (empty($paket)): */?>
               <!-- <center><h5>Belum ada paket yang ditambahkan</h5> <br> <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambahkan Sekarang</button></center> -->
               <?php /*else: */ ?>
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Pilih Template Paket </label>
                  </div>
                  <div class="col-md-6">
                     <select class="select2 form-control w-80" id="paket_id">
                        <?php if (count($paket) > 0){ ?>
                        <?php foreach ($paket as $data){ ?>
                        <option value="<?= $data->id ?>"> <?= $data->name ?> </option>
                        <?php } ?>
                        <?php } else { ?>
                        <option>Belum ada Paket  </option>
                        <?php } ?>
                     </select>
                  </div>
                  <div class="col-md-3">
                     <button  onclick="do_select_paket()" type="button" class="btn btn-success btn-sm">
                     <i class="glyphicon-ok glyphicon "></i>&nbsp;
                     Pilih </button>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Beri nama Paket ini </label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" class="form-control " value="Paket Tools <?php echo $project['nama_proyek'] ?>" name="">
                  </div>
               </div>
               <center>
                  <h5></h5>
               </center>
               <br>
               <table class="table table-striped table-hover  text-center m-3">
                  <thead>
                     <th width="10">No.</th>
                     <th>Nama Tool</th>
                     <th>Spec</th>
                     <th width="10">Jumlah</th>
                     <th width="10">Tersedia</th>
                     <th>Status</th>
                     <th></th>
                     <th></th>
                  </thead>
                  <tbody>
                     <?php $enough = 0 ?>
                     <?php foreach ($paket_detail['detail'] as $key => $item): ?>
                     <tr>
                        <td><?php echo $key+1 ?></td>
                        <td><?php echo $item->tool_name ?></td>
                        <td><?php echo $item->tool_spec ?></td>
                        <td><?php echo $item->qty ?></td>
                        <td><?php echo $item->stok ?></td>
                        <?php if ($item->qty <= $item->stok) {
                           $enough += 1;
                           echo "<td>Tersedia</td>";
                           echo "<td></td>";
                           }else{
                           $enough += 0;
                           echo "<td>Tidak Tersedia</td>";
                           $content = '<select  data-placeholder="Pilih..." tabindex="2" name="paket" id="paket" class="form-control" style="width: 100% !important">
                           				<option>Pembelian</option>
                           				<option>Rolling</option>
                           			</select> ';
                           echo "<td>".$content."</td>";
                           } ?>
                        <td>
                           <?php if (empty($trx)): ?>
                           <button onclick="edit2(<?php echo $item->id ?>, '<?php echo $item->tool_name ?>', '<?php echo $item->tool_spec ?>', <?php echo $item->qty ?> , <?php echo $item->stok ?> )" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon glyphicon-plus"></span></button>
                           <a href="<?php echo base_url('new/paket/deleteitem/').$item->id ?>" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon glyphicon-trash"></span></a>
                           <?php endif ?>
                        </td>
                     </tr>
                     <?php endforeach ?>
                  </tbody>
               </table>
               <br>
               <div class="row">
                  <div class="col-md-6">
                     <button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
                     <span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
                     </button>
                     <button class="btn btn-primary" data-toggle="modal" data-target="#modal_proyek">
                     <span class="glyphicon glyphicon-copy"></span> Salin Template
                     </button>
                  </div>
                  <div class="col-md-6">
                     <!-- <a href="<?php echo base_url('/new/paket/realisasi') ?>" class="btn btn-success">
                        <span class="glyphicon glyphicon glyphicon-ok"></span> Realisasi
                        </a> -->
                     <?php if (empty($trx)) : ?>
                     <a href="<?php echo base_url('/new/tools/transaksi/realisasi/').$project['id_proyek'] ?>" <?php if ($enough != $key+1): ?>
                        disabled="disabled"
                        <?php endif ?> class="btn btn-success pull-right"><span class="glyphicon glyphicon glyphicon-ok"></span>  </a>
                     <?php elseif($trx->status == "menunggu persetujuan") : ?>
                     <div class="pull-right">
                        <a href="<?php echo base_url('/new/tools/transaksi/reject/').$trx->id ?>" class="btn btn-danger"><span class="glyphicon glyphicon glyphicon-remove"></span></a>
                        <!-- <a href="<?php echo base_url('/new/tools/transaksi/accept/').$trx->id ?>" class="btn btn-success"><span class="glyphicon glyphicon glyphicon-ok"></span></a> -->
                        <button class="btn btn-success" id="btn-approve" data-toggle="modal" data-target="#modal-approve"><span class="glyphicon glyphicon glyphicon-ok"></span></button>
                     </div>
                     <?php endif ?>
                  </div>
               </div>
               <?php //endif ?>
            </div>
         </div>
      </div>
   </div>
   <div style="clear: both;"></div>
</section>
<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel">Pilih Paket</h4>
       </div>
       <div class="modal-body">
   <form method="POST">
   	<label>Pilih Template Tool</label>
   	<select  data-placeholder="Pilih..." tabindex="2" name="paket" id="paket" class="form-control" style="width: 100% !important">
   		<?php foreach ($pakets as $p): ?>
   			<option value="<?php echo $p->id ?>"><?php echo strtoupper($p->name) ?></option>
   		<?php endforeach ?>
   	</select>

       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary" >Kirim</button>
       </div>
       </form>
     </div>
   </div>
   </div>
   -->
<div class="modal fade" id="modal_add" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
         </div>
         <div class="modal-body">
            <form method="POST" class="form-horizontal" action="<?php echo base_url('new/paket/additem') ?>" id="form-paket">
               <input type="hidden" name="paket" value="<?php echo $paket->id ?>">
               <div class="form-group w-100" id="select_tipe">
                  <label for="invt" class="col-sm-3 control-label">
                  Kategori&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100" required style="width: 100%">
                        <option value="mekanik">Mekanik</option>
                        <option value="listrik">Listrik</option>
                        <option value="instrument">Instrument</option>
                        <option value="predictive">Predictive</option>
                     </select>
                  </div>
               </div>
               <div id="select_tool" class="form-group w-100">
                  <label for="tool" class="col-sm-3 control-label">
                  Tool&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="tool" id="tool" class="form-control w-100" required style="width: 100%">
                     </select>
                  </div>
               </div>
               <input type="hidden" id="current_stok" name="">
               <div class="form-group">
                  <label class="col-sm-3 control-label">Jumlah</label>
                  <div class="col-sm-9">
                     <input id="jml" type="number" name="jml" class="form-control w-100">
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button type="button" id="btn_save" onclick="before_submit('edit')" class="btn btn-primary" >Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="modal_proyek" tabindex="-1" role="dialog" aria-labelledby="proyekLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="proyekLabel">Salin Template</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form method="POST" action="<?php echo base_url('new/tool/copy_paket/').$project['id_proyek'] ?>">
               <label>Pilih Proyek</label>
               <select  data-placeholder="Pilih..." tabindex="2" name="id" id="proyek" class="form-control" style="width: 100% !important">
                  <?php foreach ($projects as $p): ?>
                  <option value="<?php echo $p->id_proyek ?>"><?php echo strtoupper($p->nama_proyek) ?></option>
                  <?php endforeach ?>
               </select>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Save changes</button>
         </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function($) {
   	load_data_detail();
   });
   $.ajax({
   	url: '<?php echo base_url("/new/tool/get_tool_by_kategori?kategori=mekanik") ?>',
   	type: 'GET',
   	dataType: 'json',
   	success : function(data){
   		var html = '';
   		for (var i = 0; i < data.length; i++) {
   			html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
   		}
   		$('#tool').html(html);
   		$('#tool').trigger('change');
   	}
   })
   $('.select2').each(function() {
       $(this).select2({ dropdownParent: $(this).parent()});
   })
   function add_new_item(){
   	$.ajax({
   		url: '<?= base_url() ?>new/paket/additem',
   		type: 'POST',
   		dataType: 'json',
   		data: {
   			tool : $("#tool").val(),
   			jml : $("#jml").val(),
   			idx : $("#paket_detail_id").val(),
   		},
   		success:function(resp){
   			if (resp.status == 200) {
   				load_data_detail();
   				$("#modal_add").modal('hide');
   			}
   		}
   	});
   }
   function load_data_detail(){

      	var datatable= $('#tblx').DataTable({
           destroy: true,
           "processing": true,
           "serverSide": true,
           ajax: {
             url: "<?php echo base_url('/new/tool/load_data_paket_detail/') ?>"
           },
           "order": [
             [0, 'desc']
           ],
           "dom": "<'row'<'col-sm-12'tr>>" +
   				"<'row'<'col-sm-2'l><'col-sm-4'i><'col-sm-6'p>>",
   		"language": {
               "lengthMenu": "Perhalaman _MENU_",
               "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
           },
           "columns": [
           	{ "name": "A.ID" },
           	{ "name": "A.TOOL_NAME" },
           	{ "name": "A.TOOL_SPEC" },
           	{ "name": "B.JENIS" },
           	{ "name": "B.KATEGORI" },
           	{ "name": "A.QTY" },
           	{ "orderable": false, "searchable":false},
           	{ "orderable": false, "searchable":false},
           	{ "orderable": false, "searchable":false},
            	{ className: "countdable td-right","orderable": false, "targets": [ 10 ] }
            ],
           "iDisplayLength": 10,
           "scrollX" : false,
       });
   }
   function do_select_paket(){

		if (confirm('Anda yakin menghapus semua data dan menyalin dari paket ini ?')) {
			var paket_id   = $("#paket_id").val();
	   	var project_id = "<?= $this->uri->segment(4) ?>";
	   	$.ajax({
	   		url: '<?= base_url('new/tool/generate_paket_detail/') ?>',
	   		type: 'POST',
	   		dataType: 'json',
	   		data: {
	   			paket_id :paket_id,
	   			project_id : project_id,
	   		},
	   		success:function(resp){
	   			if (resp.status==200) {
	   				window.location.reload();
	   			}
	   		}
	   	});
		}


   }
   function do_realisasi(proyek_id){

   	if ( $('tbody tr .countdable').length  > 0) {

   		$.ajax({
   			url: '<?= base_url() ?>new/paket/realisasi/',
   			type: 'POST',
   			dataType: 'JSON',
   			data: {
   				proyek_id: proyek_id,
   				paket_name : $("#paket_name").val()
   			},
   			success:function(resp){
   				if (resp.status == 200) {
   					alert(' Berhasil, Realisasi telah dikirim');
   					// window.location.reload();
   					console.log(resp);
   				} else {
   					console.log(resp);
   				}
   			}
   		});
   	} else {
   		alert('Anda Harus Memilih Item terlebih dahulu');
   	}
   }
   function edit() {
   	$('#name').val(nama);
   	$('#spek').val(spec);
   	$('#jml').val(jumlah);
   	$('#form-paket').attr('action', "<?php echo base_url('new/paket/edititem/') ?>"+id);
   	$('#modal_add').modal('show');
   }
   function edit2(id){

   	$.ajax({
   		url: '<?= base_url("new/tool/load_detail_item_paket/") ?>'+id,
   		dataType: 'json',
   		success:function(resp){
   			$("#modal_add").modal('show');
   			$("#paket_detail_id").val(resp.id);
   			$('#kategori').val(resp.kategori);
   			$('#kategori').trigger('change');
   			load_tool_based_on_kategori(resp.tool_id);
   			// $('#tool').val(resp.tool_id);
   			// $("#tool").trigger('refresh');
   			$('#jml').val(resp.qty);
   			$("#myModalLabel").html('Update Item Paket');
   		}
   	});

   }
   function delete2(id){
   	$.ajax({
   		url: '<?= base_url("new/tool/delete_detail_item_paket/") ?>'+id,
   		dataType: 'json',
   		success:function(resp){
   			load_data_detail();
   		}
   	});
   }
   function update_tidak_cukup(id , value){
   	$.ajax({
   		url: '<?= base_url("new/tool/update_keterangan_paket_detail") ?>',
   		method: 'POST',
   		data : {
   			idx : id,
   			val : value,
   		},
   		dataType: 'json',
   		success:function(resp){
   			if (resp.status == 201) {
   				load_data_detail();
   			}
   		}
   	});

   }
   $("#modal_add").on('hide.bs.modal', function(){
       $("#paket_detail_id").val('0');
   	$('#tool').val('');
   	$('#jml').val('');
   	$('#kategori').val('');
   	$("#myModalLabel").html('Tambah Item Paket');
   });
   $('#kategori').on('change', function(event) {
   	load_tool_based_on_kategori();
   });
   function load_tool_based_on_kategori(id){
   	$.ajax({
   		url: '<?php echo base_url("/new/tool/get_tool_by_kategori?kategori=") ?>'+$('#kategori').val(),
   		type: 'GET',
   		dataType: 'json',
   		success : function(data){
   			var html = '';
   			if (data.length > 0) {
   				for (var i = 0; i < data.length; i++) {
   					html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
   				}
   				$('#tool').html(html);
   				if (id != null) {
   					$("#tool").val(id);
   				}
   				$('#tool').trigger('change');
   			} else {
   				html = `<option value="0"> Tidak ada Tool </option>`;
   				$('#tool').html(html);
   				$('#tool').trigger('change');
   			}
   		}
   	})
   }
</script>
