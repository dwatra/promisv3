<section class="content-header">
	<h1 class="text-white">Master Tool</h1>
</section>
<section class="content">
<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active">
					<div class="box-header with-border">
				      	<div class="pull-left">
				      		DETAIL DATA TOOLS
						</div>
				      	<div class="pull-right">

						</div>
				    </div>
				    <div class="box-body">
				    	<form class="form-horizontal" enctype="multipart/form-data" method="POST">
		    		    	<div class="col-sm-6">
		    					<div class="form-group">
		    						<label for="name" class="col-sm-4" style="text-align: right;">
		    							No Inventarisasi
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool_first->no_inventarisasi) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="name" class="col-sm-4" style="text-align: right;">
											Jenis
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->jenis) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="name" class="col-sm-4" style="text-align: right;">
											Kategori
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->kategori) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="name" class="col-sm-4" style="text-align: right;">
		    							Nama Tools
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->name) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="merk" class="col-sm-4" style="text-align: right;">
		    							Merk
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->merk) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="spesifikasi" class="col-sm-4" style="text-align: right;">
		    							Spesifikasi
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->spek) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label class="col-sm-4" style="text-align: right;">
		    							Foto
		    						</label>
		    						<div class="col-sm-8">
		    							<img style="max-height: 150px" src="http://localhost:8080/pjbs-api/assets/upload/tool/<?php echo $tool->image ?>" >
		    						</div>
		    		    		</div>
		    		    		<div class="form-group">
		    						<label for="tgl" class="col-sm-4" style="text-align: right;">
		    							Tanggal Perolehan
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->created_at) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="harga" class="col-sm-4" style="text-align: right;">
		    							Harga
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->harga) ?>
		    						</div>
		    					</div>
		    		    	</div>
		    		    	<div class="col-sm-6">
		    		    		<!-- <div class="form-group">
		    						<label for="kondisi" class="col-sm-4" style="text-align: right;">
		    							Kondisi
		    						</label>
		    						<div class="col-sm-7">
		    							<?php echo $tool->kondisi == 1 ? 'Baik' : 'Rusak' ?>
		    						</div>
		    					</div> -->
		    					<div class="form-group">
		    						<label for="lokasi" class="col-sm-4" style="text-align: right;">
		    							Lokasi
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->lokasi) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="tempat" class="col-sm-4" style="text-align: right;">
		    							Tempat Penyimpanan
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->penyimpanan) ?>
		    						</div>
		    					</div>
		    					<div class="form-group">
		    						<label for="ket" class="col-sm-4" style="text-align: right;">
		    							Keterangan
		    						</label>
		    						<div class="col-sm-8">
		    							<?php echo ucfirst($tool->keterangan) ?>
		    						</div>
		    					</div>
									<?php if($tool->kategori == 'specific') ?>
									<div class="form-group">
		    						<label for="btn_upload" class="col-sm-4" style="text-align: right;">
											<button type="button" class="btn btn-success btn-sm" id="btn_upload" onclick="modal_files()">
												<i class="glyphicon glyphicon-upload "></i>
											</button>
		    							Upload PDF
		    						</label>
		    						<div class="col-sm-8">
											<ul>
												<?php if ($tool_file): ?>
													<?php foreach ($tool_file as $x): ?>
														<li> <a href='<?= base_url() ?>uploads/spec_tool/<?= $x->file_path ?>' download> <?= $x->file_path ?></a> </li>
													<?php endforeach; ?>
												<?php endif; ?>
												<!--
												<li> <a href='asdasdasdasdasd'> File 1.pdf</a></li>
												<li> <a href='asdasdasdasdasd'> File 1.pdf</a></li> -->
											</ul>
		    						</div>
		    					</div>
								</div>
				    	</form>
				    </div>
				</div>
			</div>
		</div>
	</div>
		</div>
	</div>
</div>
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 align="center">Detail Tool & Nomor Inventaris</h4><br/>

								<button class="btn btn-success btn-sm" type="button" name="button" onclick="add_details()">
									<i class="glyphicon glyphicon-plus"></i>
									Tambah Tool Baru
								</button>

									<div style="overflow: auto;">
			        <table class="table table-bordered">
			        <thead>
			            <tr>
			                <th>No</th>
			                <th>No Inventaris</th>
			                <th>Kondisi</th>
			                <th>Jam Utilitas</th>
			                <!-- <th>Action</th> -->
			            </tr>
			        </thead>
			        <tbody align="center">

			        	<?php if ($tool_detail){ ?>
			        	<?php $no = 1; ?>
			        	<?php foreach ($tool_detail as $var): ?>
			        			<tr>
			        				<td><?= $no++ ?></td>
											<?php if($tool->kategori != 'general'){ ?>
												<td><?php echo $var->no_inventarisasi ?></td>
											<?php } else { ?>
													<td></td>
											<?php } ?>

			        				<td>
			        					<?php
			        						switch ($var->kondisi) {
			        							case '1':
			        								echo 'Tersedia';
			        								break;

			        							case '2':
			        								echo 'Rusak';
			        								break;

			        							case '3':
			        								echo 'Terpakai';
			        								break;

			        							default:
			        								echo 'Unknown';
			        								break;
			        						}
			        					 ?>
			        				</td>
			        				<td>
			        					<?php echo $var->utilitas ?>
			        				</td>
			        			</tr>
			        	<?php endforeach ?>
			        	<?php } else { ?>
			        		<tr>
			        			<td colspan="4">
			        				No Data
			        			</td>
			        		</tr>
			        	<?php } ?>

			        </tbody>
			        </table>
			    </div>
            </div>
        </div>
    </div>
</div>
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 align="center">History Tools</h4><br/>
				<div style="overflow: auto;">
			        <table class="table table-bordered">
			        <thead>
			            <tr>
			                <th>No</th>
			                <th>Nama Proyek</th>
			                <th>Tgl Mulai</th>
			                <th>Tgl Selesai</th>
			                <th>Tool Inventaris</th>
			                <th>Utilitas</th>
			            </tr>
			        </thead>
			        <tbody align="center">
			            <!-- <tr align="center"><td colspan="4">Tidak ada data</td></tr> -->

			            <?php foreach ($tool_history as $h ) { ?>
			                <tr>
			                	<td><?php echo $i++; ?></td>
			                	<td><?php echo $h->nama_proyek ?></td>
			                	<td><?php echo $h->start_date ?></td>
			                	<td><?php echo $h->end_date ?></td>
												<?php if($tool->kategori != 'general'){ ?>
													<td><?php echo $h->tool_inv ?></td>
												<?php } else { ?>
`													<td></td>
												<?php } ?>
			                	<td><?php echo $h->utilitas ?></td>
			                </tr>
			            <?php } ?>
			        </tbody>
			        </table>
			    </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" id="modal_add_item_details">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

			<form class="form-horizontal" action="<?= base_url('new/tool/update_detail/') ?>" method="post">

				<input type="hidden" name="tool_id" value="<?= $tool->id ?>">

					<?php if($tool->kategori == 'general' || $tool->kategori == 'consumable' ){   ?>

					<div class="fom-group row">
						<div class="col-md-12">
							<label> Quantity  </label>
							<input class="form-control" type="text" name="quantity" value="">
						</div>
					</div>
					<input type="hidden" name="no_inventaris" value="0">

				<?php } else {  ?>

					<div class="fom-group row">
						<div class="col-md-12">
							<label> No Inventaris  </label>
							<input class="form-control" type="text" name="no_inventaris" value="">
						</div>
					</div>
					<input type="hidden" name="quantity" value="0">

				<?php } ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
		</form>
    </div>
  </div>
</div>
<div class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" id="modal_files">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

			<form class="form-horizontal" action="<?= base_url('new/tool/insert_files/') ?>" method="post" enctype="multipart/form-data">

				<input type="hidden" name="tool_id" value="<?= $tool->id ?>">
				<div class="fom-group row">
					<div class="col-md-12">
						<label> Select Files (Bisa lebih dari 1 file)  </label>
						<input class="form-control" type="file" name="files[]" multiple value="">
					</div>
				</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
		</form>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$('table').DataTable(	)
	});

	$("#btn_add_modals").onclick(function(){

	});

	function add_details(){

		// alert('test')
		$("#modal_add_item_details").modal('show')
	}
	function modal_files(){

		// alert('test')
		$("#modal_files").modal('show')
	}
</script>
