<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<style type="text/css">
	.bootstrap-select > .dropdown-toggle{
		width:380px;
	}
</style>
<section class="content">
	<div class="col-sm-12 no-padding">
	</div>
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-header with-border">
						<div class="pull-left" style="max-width: -90px">
							<h4 style="margin: 10px 0px 0px 0px !important; display: inline;"> - LAPORAN TOOL - </h4>
						</div>
						<div class="pull-right">
						</div>
					</div>
					<div class="box-body">
						<div class="col-md-12">
							<form class="form-horizontal" method="POST" id="post" action="<?php echo base_url('/new/tool_keluar/create') ?>">


								<div class="form-group">
									<label for="start" class="col-sm-1 control-label">
									  	Tgl Proyek start &nbsp;<span style="color:#dd4b39">*</span>
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text" name="start" id="start" class="form-control w-100" >
									</div>
									<label for="endi" class="col-sm-2 control-label">
											Tgl Proyek End
									</label>
									<div class="col-sm-3">
										<input autocomplete="off" type="text" name="end" id="end" class="form-control w-100" >
									</div>
								</div>

							</form>
						</div>
						<div class="col-md-12">
							<center><h5>Daftar Proyek</h5></center>
							<br>
							<table class="table table-striped table-hover  text-center m-3" id="tbl_report">
								<thead>
									<th width="10">ID.</th>
									<th> NAMA PROYEK </th>
									<th> TGL RENCANA MULAI </th>
									<th> TGL RENCANA SELESAI  </th>
									<th width="10%"> PAKET TOOL DIBAWA </th>
									<th> STATUS PROYEK </th>
									<th> JUMLAH TOOL DIBAWA</th>
									<th> JUMLAH TOOL KEMBALI</th>
									<!-- <th> </th> -->
								</thead>
								<tbody>

								</tbody>
							</table>
							<br>
							<div class="row" style="display:none;">
								<div class="col-md-6">
									<button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
										<span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
									</button>
								</div>
								<div class="col-md-6">
									<button type="submit" form="post" class="btn btn-success pull-right">
										<span class="glyphicon glyphicon glyphicon-ok"></span> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div style="clear: both;"></div>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
		$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})

		load_table();
	});

	function load_table(){
		 var datatable = $('#tbl_report').DataTable({
					destroy: true,
					"processing": true,
					"serverSide": true,
					ajax: {
						url: "<?php echo base_url('/new/report_tool/load_data/') ?>",
					},
					"order": [
						[5, 'desc']
					],
				  "language": {
								 "lengthMenu": "Perhalaman _MENU_",
								 "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
					},
					/*<th width="10">ID.</th>
					<th> Nama Proyek  </th>
					<th> TGL RENCANA MULAI </th>
					<th> TGL RENCANA SELESAI  </th>
					<th width="10%"> Paket Tools Yang di bawa </th>
					<th> Total Tool dibawa </th>
					<th> Total Jumlah Spec Tool dibawa </th>
					<th> Action </th>*/
					"columns": [
					 { "name": "A.ID_PROYEK" },
					 { "name": "A.NAMA_PROYEK" },
					 { "name": "A.TGL_RENCANA_MULAI" },
					 { "name": "A.TGL_RENCANA_SELESAI" },
					 { "name": "C.NAME" },
					 { "name": "B.STATUS" },
					 { "orderable": true , "searchable":false},
					 { "orderable": true , "searchable":false},
					 // { "orderable": true , "searchable":false},
					 // { "orderable": false, "searchable":false},
					 ],
					"iDisplayLength": 10,
					"scrollX" : false,
			});
	}


</script>
