<section class="content">
   <div class="col-sm-12 no-padding">
      <?php

         if ($trx) {
         $read = $trx->status == 'menunggu persetujuan' ? 'readonly' : '';
         $cant_update= $trx->status == 'disetujui' ? '1' : '0';

         if ($trx->status == 'disetujui' || $trx->status == 'ditolak'  ) {
         	$cant_update = '1';
         } else {

         	$cant_update = '0';
         }

         } else {
         $read = '';
         $cant_update = '0';
         }
         ?>
      <br>
   </div>

   <div class="col-sm-12 no-padding">
   <div class="box box-default">
      <div class="tab-content">
         <div class="tab-pane active" id="tab_1">
            <div class="box-header with-border">
               <div class="pull-left" style="max-width: -90px">
                  <h4 style="margin: 10px 0px 0px 0px !important; display: inline;"><?php echo $project['nama_proyek'] ?></h4>
                  <br/>
                  <small>PEMBERI PEKERJAAN : <?php echo $project['pemberi_pekerjaan'] ?> </small>
               </div>
               <div class="pull-right">
               </div>
            </div>
            <div class="box-body">
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Pilih Template Paket </label>
                  </div>
                  <div class="col-md-6">
                     <select class="select2 form-control w-80" id="paket_id">
                        <?php if (count($paket) > 0){ ?>
                        <?php foreach ($paket as $data){ ?>
                        <option value="<?= $data->id ?>"> <?= $data->name ?> </option>
                        <?php } ?>
                        <?php } else { ?>
                        <option>Belum ada Paket  </option>
                        <?php } ?>
                     </select>
                  </div>
                  <div class="col-md-3">
                     <?php if ($cant_update != '1'): ?>
                     <button  onclick="do_select_paket()" type="button" class="btn btn-success btn-sm">
                     <i class="glyphicon-ok glyphicon "></i>&nbsp;
                     Pilih </button>
                     <?php endif ?>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Beri nama Paket ini </label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" id="paket_name" class="form-control " value="Paket Tools <?php echo $project['nama_proyek'] ?>" name="">
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Keterangan </label>
                  </div>
                  <div class="col-md-6">
                     <textarea class="form-control" id="paket_keterangan"><?= $project['keterangan'] ?></textarea>
                  </div>
               </div>
               <?php if ($trx->status == 'ditolak'){ ?>
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Ditolak </label>
                  </div>
                  <div class="col-md-6">
                     <textarea readonly class="form-control"><?= $trx->ket_ditolak ?></textarea>
                  </div>
               </div>
               <?php } else if($trx->status == 'disetujui') { ?>
               <?php

                $detail_account = $this->query->get_data_simple('NEW_TL_USER' , ['PEMINJAMAN_ID' => $trx->id])->row();

               ?>

               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Username Apps Android </label>
                  </div>
                  <div class="col-md-6">
                    <?php echo $detail_account->user_name ?>
                  </div>
               </div>
               <div class="row form-group">
                  <div class="col-md-2">
                     <label> Password Apps Android </label>
                  </div>
                  <div class="col-md-6">
                    <?php echo $detail_account->peminjaman_id.$detail_account->proyek_id.$detail_account->paket_id ?>
                  </div>
               </div>
               <?php } ?>
               <center>
                 <h5></h5>
               </center>
               <br>
               <table class="table table-striped table-hover  text-center m-3" id="tblx">
                  <thead>
                     <th width="10">No.</th>
                     <th>Nama Tool</th>
                     <th>Spec</th>
                     <th>Jenis Tool</th>
                     <th>Kategori Tool</th>
                     <th width="10">Jumlah</th>
                     <th width="10">Tersedia</th>
                     <th >Status</th>
                     <th >Pic Name</th>
                     <!-- <th >Act</th> -->
                     <th></th>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
               <br>
               <div class="row">
                  <div class="col-md-6">
                     <?php if ($cant_update != '1'): ?>
                     <button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
                     <span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
                     </button>
                     <?php endif ?>
                     <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#modal_proyek">
                        <span class="glyphicon glyphicon-copy"></span> Salin Template
                        </button> -->
                  </div>
                  <div class="col-md-6">
                     <?php if ($trx->status == 'menunggu persetujuan'){ ?>
                     <div class="pull-right">
                        <?php /*echo base_url('/new/tools/transaksi/reject/').$trx->id*/ ?>
                        <button type="button" onclick="response_transaksi(<?= $trx->id ?>,'tolak')" class="btn btn-danger"><span class="glyphicon glyphicon glyphicon-remove"></span>
                        Tolak
                        </button>
                        <button type="button" onclick="response_transaksi(<?= $trx->id ?>,'accept')" class="btn btn-success" id="btn-approve" data-toggle="modal"><span class="glyphicon glyphicon glyphicon-ok"></span>
                        Terima Realisasi
                        </button>
                     </div>
                     <?php } else if($trx->status == 'ditolak') { ?>
                     <label class="btn btn-warning pull-right"> Sudah Ditolak , Menunggu Update dari Admin </label>
                     <?php } else if($trx->status == 'disetujui'){ ?>
                     <label class="btn btn-success pull-right"> Sudah Disetujui </label>
                     <button type="button" onclick="print_surat_jalan(<?= $trx->pr_id ?>)" class="pull-right btn btn-primary"><span class="glyphicon glyphicon glyphicon-file"></span>
                     Print Surat Jalan
                     </button>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div style="clear: both;"></div>
</section>
<div class="modal fade" id="modal_add" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
         </div>
         <div class="modal-body">
            <form method="POST" class="form-horizontal" id="form-paket">
               <input type="hidden" id="paket_detail_id" value="0" name="">
               <div class="form-group w-100" id="select_tipe">
                  <label for="invt" class="col-sm-3 control-label">
                  Jenis&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="jenis" id="jenis" class="form-control w-100" required style="width: 100%">
                        <option value="mekanik">Mekanik</option>
                        <option value="listrik">Listrik</option>
                        <option value="instrument">Instrument</option>
                        <option value="predictive">Predictive</option>
                     </select>
                  </div>
               </div>
               <div class="form-group w-100" id="select_tipe">
                  <label for="invt" class="col-sm-3 control-label">
                  Kategori&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select onchange="search_pic(this.value)" data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100" required style="width: 100%">
                        <option value="general">General</option>
                        <option value="special">Special</option>
                        <option value="specific">Specific</option>
                     </select>
                  </div>
               </div>
               <div class="form-group w-100" id="select_person" style="display:none;">
                  <label for="invt" class="col-sm-3 control-label">
                  Pic Tool
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="karyawan" id="karyawan" class="form-control w-100" required style="width: 100%">
                        <?php foreach ($kp2 as $person): ?>
                        <option value="<?= $person->nid ?>xXx<?= $person->nama ?>"><?= $person->nama ?></option>
                        <?php endforeach ?>
                     </select>
                  </div>
               </div>
               <div id="select_tool" class="form-group w-100">
                  <label for="tool" class="col-sm-3 control-label">
                  Tool&nbsp;<span style="color:#dd4b39">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select  data-placeholder="Pilih..." tabindex="2" name="tool" id="tool" class="form-control w-100" required style="width: 100%">
                     </select>
                  </div>
               </div>
               <input type="hidden" id="current_stok" name="">
               <div class="form-group">
                  <label class="col-sm-3 control-label">Jumlah</label>
                  <div class="col-sm-9">
                     <input id="jml" type="number" name="jml" class="form-control w-100">
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button type="button" id="btn_save" onclick="add_new_item()" class="btn btn-primary" >Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="modal_proyek" tabindex="-1" role="dialog" aria-labelledby="proyekLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="proyekLabel">Salin Template</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form method="POST" action="<?php echo base_url('new/tool/copy_paket/').$project['id_proyek'] ?>">
               <label>Pilih Proyek</label>
               <select  data-placeholder="Pilih..." tabindex="2" name="id" id="proyek" class="form-control" style="width: 100% !important">
                  <?php foreach ($projects as $p): ?>
                  <option value="<?php echo $p->id_proyek ?>"><?php echo strtoupper($p->nama_proyek) ?></option>
                  <?php endforeach ?>
               </select>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="button" onclick="add_new_item()" class="btn btn-primary">Save</button>
         </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="modal-approve" tabindex="-1" role="dialog" aria-labelledby="proyekLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="proyekLabel">Pilih Pic</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form-group">
               <label class="col-sm-4">PIC Tool General</label>
               <select  data-placeholder="Pilih..." tabindex="2"  id="picgnr" class="form-control col-sm-7" style="width: 100% !important">
                  <?php foreach ($kp2 as $kp): ?>
                  <option value="<?php echo $kp->nid ?>"><?php echo $kp->nama ?></option>
                  <?php endforeach ?>
               </select>
            </div>
            <div class="form-group">
               <label class="col-sm-4">PIC Tool Special</label>
               <select  data-placeholder="Pilih..." tabindex="2"  id="picspec" class="form-control col-sm-7" style="width: 100% !important">
                  <?php foreach ($kp2 as $kp): ?>
                  <option value="<?php echo $kp->nid ?>"><?php echo $kp->nama ?></option>
                  <?php endforeach ?>
               </select>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" onclick="do_acc()" class="btn btn-primary">Save changes</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="modal-tolak" tabindex="-1" role="dialog" aria-labelledby="proyekLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="proyekLabel">Beri Alasan untuk menolak Tools</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="form-tolak">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-sm-4">Keterangan Tolak</label>
                  <textarea id="txt_area" class="form-control" name="keterangan"></textarea>
               </div>
            </div>
         </form>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" onclick="do_tolak()" class="btn btn-primary">Save changes</button>
            </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function($) {
   	load_data_detail();
   });
   function response_transaksi(trx_id , mode){
   	if (mode == 'accept') {
   		$("#modal-approve").modal('show');
   	} else {
   		$("#modal-tolak").modal('show');
   	}
   }

   function search_pic(val){
   	if (val == 'general' || val == 'special') {
   		$("#select_person").hide();
   	} else {
   		$("#select_person").show();
   	}
   }
   function do_tolak(){
   	var text = $("#txt_area").val();
   	if (text != '') {
   		if (confirm('Lanjutkan Tolak ke Admin ? ') ) {
   			$.ajax({
   				url: '<?= base_url() ?>new/asman/reject/'+<?= $trx->id ?>,
   				type: 'POST',
   				dataType: 'json',
   				data: { text : text},
   				success:function(resp){
   					if (resp.status == 200) {
   						window.location.reload();
   					}
   				}
   			});

   		}
   	} else {
   		alert('Mohon isi keterangan tolak');
   	}
   }
   function do_acc(){
   	var trx_id = <?= $trx->id ?>;
   	if (confirm(' Setuju dengan ini ? ') ) {
   		$.ajax({
   			url: '<?= base_url() ?>new/asman/accept/'+trx_id,
   			type: 'POST',
   			dataType: 'json',
   			data:{
   				picgnr : $("#picgnr").val(),
   				// picspe : $("#picspe").val(),
   				picspc : $("#picspc").val(),
   			},
   			success:function(resp){
   				if (resp.status == 200) {
   					if (confirm(resp.msg)) {
   						print_surat_jalan(trx_id)
   						// window.location.reload();
   					} else {
   						// window.location.reload();
   					}
   				}
   			}
   		});
   	}
   }

   function print_surat_jalan(trx_id){
   	var pic_gnr = $("#picgnr").val();
   	// var pic_spe = $("#picspe").val();
   	// var pic_spc = $("#picspc").val();
   	var urls = trx_id ;
   	window.open('<?= base_url() ?>new/asman/generate_surat_jalan/'+urls,'_blank')
   }


   $.ajax({
   	url: '<?php echo base_url("/new/tool/get_tool_by_kategori?jenis=mekanik") ?>',
   	type: 'GET',
   	dataType: 'json',
   	success : function(data){
   		var html = '';
   		for (var i = 0; i < data.length; i++) {
   			html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
   		}
   		$('#tool').html(html);
   		$('#tool').trigger('change');
   	}
   })
   $('.select2').each(function() {
       $(this).select2({ dropdownParent: $(this).parent()});
   })

   function add_new_item(){
   	var paket_id = '<?= $trx->paket_id ?>';
   	var kategori = $("#kategori").val();
   	if (kategori == 'general' || kategori == 'special') {
   		var nid = null;
   	} else {
   		var nid = $("#karyawan").val();
   	}
    var jenis = $("#jenis").val();
    var x = $("#tool").val();
    var y = $("#jml").val();
    var z = $("#paket_detail_id").val();

    // alert(x+'|'+y+'|'+z);

   	$.ajax({
   		url: '<?= base_url() ?>new/paket/additem/'+paket_id,
   		type: 'POST',
   		dataType: 'json',
   		data: {
   			tool  :x,
   			jml   :y,
   			idx   :z,
   			nid   :nid,
   			jenis :jenis,
   			kat   :kategori,
   		},
   		success:function(resp){
   			if (resp.status == 200) {
   				load_data_detail();
   				$("#modal_add").modal('hide');
   			}
   		}
   	});
   }
   function load_data_detail(){

   	var paket_id = '<?= $trx->paket_id ?>';

   	var datatable= $('#tblx').DataTable({
           destroy: true,
           "processing": true,
           "serverSide": true,
           ajax: {
             url: "<?php echo base_url('/new/tool/load_data_paket_detail/') ?>"+paket_id+'/'+'<?= $cant_update ?>'
           },
           "order": [
             [0, 'desc']
           ],
           "dom": "<'row'<'col-sm-12'tr>>" +
   				"<'row'<'col-sm-2'l><'col-sm-4'i><'col-sm-6'p>>",
   		"language": {
               "lengthMenu": "Perhalaman _MENU_",
               "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
           },
           "columns": [
           	{ "name": "A.ID" },
           	{ "name": "A.TOOL_NAME" },
           	{ "name": "A.TOOL_SPEC" },
           	{ "name": "B.JENIS" },
           	{ "name": "B.KATEGORI" },
           	{ "name": "A.QTY" },
           	{ "orderable": false, "searchable":false},
           	{ "orderable": false, "searchable":false},
           	{ "name": "A.PIC_NAME" },
           	// { "orderable": false, "searchable":false},
            	{ className: "countdable td-right","orderable": false, "targets": [ 10 ] }
            ],
           "iDisplayLength": 10,
           "scrollX" : false,
       });
   }
   function do_select_paket(){
   	var paket_id   = $("#paket_id").val();
   	var current_paket_id = <?= $trx->paket_id ?>;

    if(confirm('Yakin menghapus semua data tool ini dan menyalin data tool dari paket tersbut ?')){
      $.ajax({
        url: '<?= base_url('new/tool/generate_paket_detail/') ?>',
        type: 'POST',
        dataType: 'json',
        data: {
          paket_id :paket_id,
          current_paket_id : current_paket_id,
        },
        success:function(resp){
          if (resp.status==200) {
            load_data_detail();
            console.log(resp);
          }else{
            alert(resp.msg);
            load_data_detail();
          }
        }
      });
    }

   }
   function do_realisasi(proyek_id){

   	if ( $('tbody tr .countdable').length  > 0) {

   		$.ajax({
   			url: '<?= base_url() ?>new/paket/realisasi/',
   			type: 'POST',
   			dataType: 'JSON',
   			data: {
   				proyek_id: proyek_id,
   				paket_name : $("#paket_name").val()
   			},
   			success:function(resp){
   				if (resp.status == 200) {
   					alert(' Berhasil, Realisasi telah dikirim');
   					// window.location.reload();
   					console.log(resp);
   				} else {
   					console.log(resp);
   				}
   			}
   		});
   	} else {
   		alert('Anda Harus Memilih Item terlebih dahulu');
   	}
   }
   function edit() {
   	$('#name').val(nama);
   	$('#spek').val(spec);
   	$('#jml').val(jumlah);
   	$('#form-paket').attr('action', "<?php echo base_url('new/paket/edititem/') ?>"+id);
   	$('#modal_add').modal('show');
   }
   function edit2(id){

   	$.ajax({
   		url: '<?= base_url("new/tool/load_detail_item_paket/") ?>'+id,
   		dataType: 'json',
   		success:function(resp){

        $("#karyawan").val(resp.nid_pic+'xXx'+resp.pic_name);
        $("#karyawan").trigger('change');

   			$("#modal_add").modal('show');
   			$("#paket_detail_id").val(resp.id);
   			$('#kategori').val(resp.kategori);
   			$('#kategori').trigger('change');
   			$('#jenis').val(resp.jenis);
   			$('#jenis').trigger('change');
   			load_tool_based_on_kategori(resp.tool_id);

   			// // alert(resp.tool_id);
   			// $('#tool').val(resp.tool_id);
   			// $("#tool").trigger('change');
   			$('#jml').val(resp.qty);
   			$("#myModalLabel").html('Update Item Paket');
   			console.log(resp);
   		}
   	});

   }
   function delete2(id){
   	$.ajax({
   		url: '<?= base_url("new/tool/delete_detail_item_paket/") ?>'+id,
   		dataType: 'json',
   		success:function(resp){
   			load_data_detail();
   		}
   	});
   }
   function update_tidak_cukup(id , value){
   	$.ajax({
   		url: '<?= base_url("new/tool/update_keterangan_paket_detail") ?>',
   		method: 'POST',
   		data : {
   			idx : id,
   			val : value,
   		},
   		dataType: 'json',
   		success:function(resp){
   			if (resp.status == 201) {
   				load_data_detail();
   			}
   		}
   	});

   }
   $("#modal_add").on('hide.bs.modal', function(){
       $("#paket_detail_id").val('0');
   	$('#tool').val('');
   	$('#jml').val('');
   	$('#kategori').val('');
   	$("#myModalLabel").html('Tambah Item Paket');
   });
   $('#kategori').on('change', function(event) {
   	load_tool_based_on_kategori();
   });
   $('#jenis').on('change', function(event) {
   	load_tool_based_on_kategori();
   });
   function load_tool_based_on_kategori(id = null){
   	$.ajax({
   		url: '<?php echo base_url("/new/tool/get_tool_by_kategori") ?>',
   		type: 'GET',
   		dataType: 'json',
   		data : {
   			kategori : $('#kategori').val(),
   			jenis : $("#jenis").val(),
   		},
   		success : function(data){
   			var html = '';
   			if (data.length > 0) {
   				html = '<option value="0" disabled> Ada Tools </option>';
   				for (var i = 0; i < data.length; i++) {
   					html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
   				}
   				$('#tool').html(html);
   				if (id != null) {
   					$("#tool").val(id);
   				}
   				$('#tool').trigger('change');
   			} else {
   				html = `<option value="0"> Tidak ada Tool </option>`;
   				$('#tool').html(html);
   			}
   			$('#tool').trigger('change');
   		}
   	})
   }
</script>
