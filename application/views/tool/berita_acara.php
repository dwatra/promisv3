<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>berita acara</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
        
        .data {
            font-size: 14px;
        }
        
        .data tr th {
            padding: 5px
        }
        
        .data tr td {
            padding: 5px
        }
        
        .data tr:nth-child(even) {
            background-color: #b1cde2;
        }
        
        .data2 {
            font-size: 14px;
        }
        
        .data2 tr th {
            padding: 5px
        }
        
        .data2 tr td {
            padding: 5px
        }
        
        .data2 tr:nth-child(even) {
            background-color: #b1cde2;
        }
    </style>
</head>

<body>
    <div class="box box-default">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            <div class="col-md-12">
                                <!-- <label class="col-sm-1">Berita Acara Kerusakan</label> -->
                                <div class="col-sm-9">
                                    <!-- <?= $data->vendor ?> -->
                                    <?php if($count->jumlah > 0) { ?>
                                    <button class="btn btn-warning" onclick="window.open('<?php echo base_url("/new/pengembalian/berita_acara/").$data->id ?>')"><span class="glyphicon glyphicon glyphicon-file"></span> Print Berita Acara Kerusakan</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 5%;"><img src="https://pinpku.umsida.ac.id/wp-content/uploads/2017/07/pjb-services-pembangkitan-jawa-bali.jpg" alt="" width="75px"></td>
                                <td>
                                    <center>
                                        BERITA ACARA <br> *) PENGEMBALIAN BARANG
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;padding: 5px;">Pada hari ini tanggal <?= $data->tgl ?> bulan <?= $data->bulan ?> tahun <?= $data->tahun ?>, saya selaku peminjam peralatan adapun Pengembalian tools berupa:</td>
                            </tr>
                        </table>
                        <br>
                        <table class="data" style="width: 100%; border-collapse: collapse;" border="1">
                            <thead>
                                <tr style="background: #406988; color: white;">
                                    <th style="width: 3%;">NO</th>
                                    <th>NAMA PERALATAN</th>
                                    <th>NO. INVENTARIS</th>
                                    <th>SPESIFIKASI</th>
                                    <!-- <th>MERK</th> -->
                                    <!-- <th>JUMLAH</th>
                                    <th>KETERANGAN</th>
                                    <th>PENYEBAB</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0; ?>
                                <?php foreach ($detail as $dtl) { ?>
                                    <tr align="center">
                                        <td><?= ++$no ?></td>
                                        <td><?= $dtl->tool_name ?></td>
                                        <td><?= $dtl->tool_inventaris ?></td>
                                        <td><?= $dtl->tool_spec ?></td>
                                        <!-- <td>1</td>
                                        <td>-</td>
                                        <td>-</td> -->
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- <table class="data2" style="width: 100%; border-collapse: collapse;" border="1">
                            <tbody>
                                <tr>
                                    <td colspan="2">Kronologis Kejadian :</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;width:3%">1.</td>
                                    <td>adsdas</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="data2" style="width: 100%; border-collapse: collapse;" border="1">
                            <tbody>
                                <tr>
                                    <td colspan="2">Usulan :</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;width:3%">1.</td>
                                    <td>adsdas</td>
                                </tr>
                            </tbody>
                        </table> -->
                        <br>
                        <br>
                        <!-- <table>
                            <tr>
                                <td>NID</td>
                                <td>:</td>
                                <td>1987237912KP</td>
                            </tr>
                            <tr>
                                <td>NAMA</td>
                                <td>:</td>
                                <td>Muhammad Eastoni Maulana</td>
                            </tr>
                            <tr>
                                <td>JABATAN</td>
                                <td>:</td>
                                <td>kuwik kuwik</td>
                            </tr>
                            <tr>
                                <td>PROYEK/NAMA PEKERJAAN</td>
                                <td>:</td>
                                <td>kuwik kuwik</td>
                            </tr>
                        </table> -->
                        <br/>
                        <p>Demikian Berita Acara ini dibuat dengan sebenarnya agar dapat dipergunakan sebagaimana mestinya</p>
                        <br>
                        <p style="text-align: right;">Surabaya, <?= $data->tgl_ba ?></p>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 33.3%;">
                                    <center>
                                        <table style="text-align: center;">
                                            <tr>
                                                <td>User,</td>
                                            </tr>
                                            <tr>
                                                <td><br><br><br></td>
                                            </tr>
                                            <tr>
                                                <td><b><u><!-- (Eastoni) --></u></b></td>
                                            </tr>
                                        </table> <br>
                                        <table style="text-align: center;">
                                            <tr>
                                                <td><br><br><br></td>
                                            </tr>
                                            <tr>
                                                <td><b><u><!-- (Eastoni) --></u></b></td>
                                            </tr>
                                        </table>
                                    </center>
                                </td>
                                <td style="text-align: center;width: 33.3%;">Menyetujui,</td>
                                <td style="width: 33.3%;">
                                    <center>
                                        <table style="text-align: center;">
                                            <tr>
                                                <td>PIC Manajemen Tools,</td>
                                            </tr>
                                            <tr>
                                                <td><br><br><br></td>
                                            </tr>
                                            <tr>
                                                <td><b><u><!-- (Eastoni) --></u></b></td>
                                            </tr>
                                        </table> <br>
                                        <table style="text-align: center;">
                                            <tr>
                                                <td><br><br><br></td>
                                            </tr>
                                            <tr>
                                                <td><b><u><!-- (Eastoni) --></u></b></td>
                                            </tr>
                                        </table>
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<script>
    $(document).ready(function() {

        $(window).on('resize', function() {
            $('.data2').width($('.data').height());
        }).trigger('resize');

    });
</script>