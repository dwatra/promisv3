
	<div class="box box-default">
		<div class="box-body">
			<div class="row">
				<div class="col-lg-12">
					<h3 align="center"> Storage Item List</h3>

					<table class="table table-striped table-hover">
						<thead>
							<th>No.</th>
							<th>TOOL NAME </th>
							<th>TOOL SPEC</th>
							<th>TOOL INVENTARIS</th>
							<th>TOOL JENIS</th>
							<th>TOOL KATEGORI</th>
							<th>TERSEDIA?</th>
							<!-- <th>Status</th> -->
						</thead>
						<tbody>
							<?php foreach ($data_tool_storage as $p){ ?>
								<tr>
									<td><?php echo $p->id ?></td>
									<td><?php echo $p->tool_name ?></td>
									<td><?php echo $p->tool_spec ?></td>
									<td><?php echo $p->tool_inventaris ?></td>
									<td><?php echo $p->tool_jenis ?></td>
									<td><?php echo $p->tool_kategori ?></td>
									<td>
										<?php

											echo ($p->instorage == 1) ? '<span style="color:green"> AVAILABLE </span>' : '<span style="color:red">NOT AVAILABLE</span>';

										?>

									</td>
									

								
									
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="box box-default">
		<div class="box-body">
			<div class="row">
				<div class="col-lg-12">
					<h3 align="center"> History Keluar Masuk Tools </h3>

					<table class="table table-striped table-hover">
						<thead>
							<th>NO.</th>
							<th>PIC</th>
							<th>KODE NOTA</th>
							<th>TAKER</th>
							<th>AREA PEKERJAAN</th>
							<th>TGL PEMINJAMAN MULAI</th>
							<th>TGL PEMINJAMAN SELESAI</th>
							<th>STATUS</th>
							<!-- <th>Status</th> -->
						</thead>
						<tbody>
							<?php foreach ($data_nota as $nota): ?>
								<tr>
									<td><?php echo $nota->id ?></td>
									<td><?php echo $nota->pic ?> (<?= $nota->pic_nid ?>)</td>
									<td><?php echo $nota->kode_nota ?></td>
									<td><?php echo $nota->taker_name ?></td>
									<td><?php echo $nota->area_pekerjaan ?></td>
									<td><?php echo $nota->tgl_peminjaman_mulai ?></td>
									<td><?php echo $nota->tgl_peminjaman_end ?></td>
									<td>
										<?php

											
											if ($p->status_kembali == 0) {
												echo '<span style="color:red"> Belum Dikembalikan </span>';
											} else if ($p->status_kembali == 1){
												echo '<span style="color:red"> Sudah Dikembalikan </span>';
											} else {
												echo '<span style="color:yellow"> Dikembalikan Sebagian </span>';
											}
										?>
									</td>
									
									

								
									
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- panel barchart -->
	
</section>

<script type="text/javascript">
	$('table').DataTable()
	<?php //if(!empty($notif['proyek'] or !empty($notif['kalibrasi']))): ?>
		$('#notifmodal').modal('show')
	<?php //endif ?>
</script>
 -->