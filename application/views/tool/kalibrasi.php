<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Jadwal Kalibrasi</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default" style="/*min-height: 500px*/">
		<div class="box-body">
			<div class="row" style="margin-bottom:  15px; margin-top: 5px">
				<div class="col-md-5">
					<div class="input-group">
						<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" class='btn btn-default btn-sm' title="Filter"><span class="glyphicon glyphicon-search"></span></button>
							<button type="button" class="btn waves-effect btn-sm btn-default" title="Reset"><span class="glyphicon glyphicon-refresh"></span></button>
						</span>
					</div>
					
				</div>
				<div class="col-md-1">
					<a href="<?php echo base_url('/new/kalibrasi/add') ?>" class=" btn btn-warning">
						<span class="glyphicon glyphicon glyphicon-plus"></span> Jadwal
					</a>	
				</div>
			</div>
			<h5>Daftar Kalibrasi Tool</h5>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">No Inventarisasi</th>
						<th class="text-center">Periode</th>
						<th class="text-center">Status</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody class="text-center">
					
				</tbody>
			</table>
		</div>
    </div>
</section>
<script type="text/javascript">
	var datatable = $('table').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": true,
        ajax: {
          url: "<?php echo base_url('/new/kalibrasi/load_data') ?>"
        },
        "dom": "<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'i><'col-sm-6'p>>",
		"language": {
            "lengthMenu": "Perhalaman _MENU_",
            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
        },
        "columnDef": [
         	{ className: "td-right","orderable": false, "targets": [ 4 ] },
         ],
        "iDisplayLength": 10,
        "scrollX" : false,
    })
    $('.dataTables_filter').css('display', 'none');
    $('#filter').keyup(function(){
          datatable.search($(this).val()).draw();
    })
</script>