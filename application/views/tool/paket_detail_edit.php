<section class="content">
	<div class="col-sm-12 no-padding">
	</div>
	<div class="col-sm-12 no-padding">
		<div class="box box-default">
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-header with-border">
						<div class="pull-left" style="max-width: -90px">
							<h4 style="margin: 10px 0px 0px 0px !important; display: inline;"><?php echo $paket->name ?></h4>
						</div>
						<div class="pull-right">
						</div>
					</div>
					<div class="box-body">
							<center><h5>Daftar Tool</h5></center>
							<br>
							<table class="table table-striped table-hover  text-center m-3" id="tbl2">
								<thead>
									<th width="10">No.</th>
									<th>Nama Tool</th>
									<th>Spec</th>
									<th width="10">Jumlah</th>
									<th></th>
								</thead>
								<tbody>
								</tbody>
							</table>
							<br>
							<div class="row">
								<div class="col-md-6">
									<button class="btn btn-warning" data-toggle="modal" data-target="#modal_add">
										<span class="glyphicon glyphicon glyphicon-plus"></span> Tambah tool
									</button>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	<div style="clear: both;"></div>
</section>
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah item</h4>
      </div>
      <div class="modal-body">
		<form method="POST" class="form-horizontal" action="<?php echo base_url('new/paket/additem') ?>" id="form-paket">
			<input type="hidden" name="paket" value="<?php echo $paket->id ?>">
			<div class="form-group w-100">
				<label for="invt" class="col-sm-3 control-label">
					Kategori&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<select  data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100" required style="width: 100%">
						<option value="mekanik">Mekanik</option>
						<option value="listrik">Listrik</option>
						<option value="instrument">Instrument</option>
						<option value="predictive">Predictive</option>
					</select>
				</div>
			</div>
			<div class="form-group w-100">
				<label for="tool" class="col-sm-3 control-label">
					Tool&nbsp;<span style="color:#dd4b39">*</span>
				</label>
				<div class="col-sm-8">
					<select  data-placeholder="Pilih..." tabindex="2" name="tool" id="tool" class="form-control w-100" required style="width: 100%">

					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Jumlah</label>
				<div class="col-sm-9">
					<input id="jml" type="number" name="jml" class="form-control w-100">
				</div>
			</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" >Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$("#modal_add").on('hide.bs.modal', function(){
	    $('#name').val('');
		  $('#spek').val('');
		  $('#jml').val('');
	});
	$('#kategori').on('change', function(event) {
		$.ajax({
			url: '<?php echo base_url("/new/tool/get_tool_by_kategori?kategori=") ?>'+$('#kategori').val(),
			type: 'GET',
			dataType: 'json',
			success : function(data){
				var html = '';
				for (var i = 0; i < data.length; i++) {
					html += `<option value="${data[i].id}">${data[i].name+' - '+data[i].spek}</option>`
				}
				$('#tool').html(html);
				$('#tool').trigger('change');
			}
		})
	});
  $('#tbl2').DataTable({
    destroy: true,
    "processing": true,
    "serverSide": true,
    ajax: {
      url: "<?php echo base_url('/new/Paket/load_data_detail/') ?>"+'<?= $this->uri->segment(4) ?>'
    },
    "order": [
      [0, 'asc']
    ],
    "language": {
        "lengthMenu": "Perhalaman _MENU_",
				"info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
    },
    "columns": [
    	{ "name": "ID", "searchable":false},
    	{ "name": "TOOL_NAME" },
    	{ "name": "TOOL_SPEC" },
    	{ "name": "TOOL_INVENTARIS" },
    	{ "name": "TOOL_JENIS" },
     	{ className: "td-right","orderable": false, "targets": [ 10 ] }
     ],
    "iDisplayLength": 10,
    "scrollX" : false,
  });

</script>
