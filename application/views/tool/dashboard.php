<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>
<section class="content-header" style="max-width:1000px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Dashboard Tool</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.select2-selection--multiple{
		border-radius: 10px !important;
		padding-left: 5px;
		min-height: 30px;
	}
</style>
<section class="content" style="max-width: 1000px; padding-bottom: 50px">
	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Mobilisasi Tool</h3><br/>
	                <div id="indoMaps" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getMapChart(){
	                        var data = [
	                            ['id-ac', 1], //Aceh
	                            ['id-jt', 2], //Jawa Tengah
	                            ['id-be', 3], //Bengkulu
	                            ['id-bt', 4], //Banten
	                            ['id-kb', 5], //Kalimantan Barat
	                            ['id-bb', 6], //Bangka-Belitung
	                            ['id-ba', 7], //Bali
	                            ['id-ji', 8], //Jawa Timur
	                            ['id-ks', 9], //Kalimantan Selatan
	                            ['id-nt', 10], //Nusa Tenggara Timur
	                            ['id-se', 11], //Sulawesi Selatan
	                            ['id-kr', 12], //Kepulauan Riau
	                            ['id-ib', 13], //Irian Jaya Barat
	                            ['id-su', 14], //Sumatera Utara
	                            ['id-ri', 15], //Riau
	                            ['id-sw', 16], //Sulawesi Utara
	                            ['id-ku', 17], //Kalimantan Utara
	                            ['id-la', 18], //Maluku Utara
	                            ['id-sb', 19], //Sumatera Barat
	                            ['id-ma', 20], //Maluku
	                            ['id-nb', 21], //Nusa Tenggara Barat
	                            ['id-sg', 22], //Sulawesi Tenggara
	                            ['id-st', 23], //Sulawesi Tengah
	                            ['id-pa', 24], //Papua
	                            ['id-jr', 25], //Jawa Barat
	                            ['id-ki', 26], //Kalimantan Timur
	                            ['id-1024', 27], //Lampung
	                            ['id-jk', 28], //Jakarta Raya
	                            ['id-go', 29], //Gorontalo
	                            ['id-yo', 30], //Yogyakarta
	                            ['id-sl', 31], //Sumatera Selatan
	                            ['id-sr', 32], //Sulawesi Barat
	                            ['id-ja', 33], //Jambi
	                            ['id-kt', 34] //Kalimantan Tengah
	                        ];
	                        Highcharts.mapChart('indoMaps', {
	                            chart: {
	                                map: 'countries/id/id-all'
	                            },
	                            title: {
	                                text: ''
	                            },
	                            // untuk show label nama kota
	                            /*
	                            mapNavigation: {
	                                enabled: true,
	                                buttonOptions: {
	                                    verticalAlign: 'bottom'
	                                }
	                            },
	                            */
	                            colorAxis: {
	                                min: 0,
	                            },
	                            series: [{
	                                data: data,
	                                name: 'Tool',
	                                states: {
	                                    hover: {
	                                        color: '#db1e61'
	                                    }
	                                },
	                                dataLabels: {
	                                    enabled: false,
	                                    format: '{point.name}'
	                                }
	                            }]
	                        });
	                    }
	                    getMapChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end -->
	<div class="box box-default">
		<div class="box-body">
			<div class="row">
				<div class="col-sm-12">
					<h3 align="center">Daftar Proyek <?php echo date('Y') ?></h3>

					<table class="table table-striped table-hover">
						<thead>
							<th>No.</th>
							<th>Nama Proyek</th>
							<th>Rencana Mulai</th>
							<th>Rencana Selesai</th>
							<th>Status</th>
							<th></th>
						</thead>
						<tbody>

							<?php foreach ($proyek as $p => $pr): ?>
								<tr>
									<td><?php echo $p+1 ?></td>
									<td><?php echo $pr->nama_proyek ?></td>
									<td><?php echo $pr->tgl_rencana_mulai ?></td>
									<td><?php echo $pr->tgl_rencana_selesai ?></td>

									<td>
										<?php if ($pr->id){
											echo ucwords($pr->status);
										}else {
											echo "Belum ada Tools";
										}
										?>
									</td>
									<td>
										<a href="<?php echo base_url('new/').$url.$pr->id_proyek ?>" class="btn btn-sm btn-primary">
											<span class="glyphicon glyphicon-pencil">
										</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- panel barchart -->
	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Gap Tool</h3><br/>
	                <div id="barchart_tool" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getBarChart(){
	                        var dataBarchart = {
	                          series: [{
	                          name: 'Kebutuhan',
	                          data: [
	                          <?php foreach ($gap_tool['plan'] as $p) {
	                             echo "$p,";
	                          } ?>
	                          ] //value data
	                        }, {
	                          name: 'Ketersediaan',
	                          data: [<?php foreach ($gap_tool['stok'] as $s) {
	                             echo "$s,";
	                          } ?>] //value data
	                        }],
	                          chart: {
	                          type: 'bar',
	                          height: 350
	                        },
	                        plotOptions: {
	                          bar: {
	                            horizontal: false,
	                            columnWidth: '55%',
	                          },
	                        },
	                        dataLabels: {
	                          enabled: false,
	                        },
	                        stroke: {
	                          show: true,
	                          width: 2,
	                          colors: ['transparent']
	                        },
	                        xaxis: {
	                          categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'] //value label
	                        },
	                        yaxis: {
	                          title: {
	                            // text: 'Jumlah Man Power'
	                          }
	                        },
	                        fill: {
	                          opacity: 1,
	                        },
	                        tooltip: {
	                          y: {
	                            formatter: function (val) {
	                              return val + " Item"
	                            }
	                          }
	                        },
	                        colors:['#0879ce', '#db1e61']
	                        };

	                        var barchart_tool = new ApexCharts(document.querySelector("#barchart_tool"), dataBarchart);
	                        barchart_tool.render();
	                    }
	                    getBarChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div>
</section>
<div class="modal fade in" id="notifmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">PEMBERITAHUAN</h4>
          </div>
          <div class="modal-body">
                <?php if (!empty($notif['proyek'])): ?>
                	<small>Proyek yang Harus Segera Disiapkan Toolnya</small>
                	<?php foreach ($notif['proyek'] as $k): ?>
                		<hr style="margin:5px 0px;"><a href="<?php echo base_url('new/tool/paket/'.$k->id_proyek) ?>"><?php echo $k->nama_proyek ?> </a>
                	<?php endforeach ?>
                <?php endif ?>
                <?php if (!empty($notif)): ?>
                	<small>Tool yang akan Jatuh Tempo Kalibrasi</small>

                	<ul>

                		<li><b>Listrik</b></li>
                		<?php if(!empty($notif['kalibrasi_listrik'])){ ?>
	                	<?php foreach ($notif['kalibrasi_listrik'] as $k) : ?>
	                		<hr style="margin:5px 0px;"><a href="<?php echo base_url('new/tool/detail/'.$k->id) ?>"><?php echo $k->name.' - '.$k->no_inventarisasi ?></a> Kurang <?php echo intval(abs(strtotime(date('d-m-Y')) - strtotime($k->start_date)) / 86400); ?> Hari</p>
	                	<?php endforeach ?>
	                	<?php } else { ?>
	                		-
	                	<?php } ?>

	                	<li><b>Instrument</b></li>
                		<?php if(!empty($notif['kalibrasi_instrument'])){ ?>
	                	<?php foreach ($notif['kalibrasi_instrument'] as $k) : ?>
	                		<hr style="margin:5px 0px;"><a href="<?php echo base_url('new/tool/detail/'.$k->id) ?>"><?php echo $k->name.' - '.$k->no_inventarisasi ?></a> Kurang <?php echo intval(abs(strtotime(date('d-m-Y')) - strtotime($k->start_date)) / 86400); ?> Hari</p>
	                	<?php endforeach ?>
	                	<?php } else { ?>
	                		-
	                	<?php } ?>

	                	<li><b>Mekanik</b></li>
                		<?php if(!empty($notif['kalibrasi_mekanik'])){ ?>
	                	<?php foreach ($notif['kalibrasi_mekanik'] as $k) : ?>
	                		<hr style="margin:5px 0px;"><a href="<?php echo base_url('new/tool/detail/'.$k->id) ?>"><?php echo $k->name.' - '.$k->no_inventarisasi ?></a> Kurang <?php echo intval(abs(strtotime(date('d-m-Y')) - strtotime($k->start_date)) / 86400); ?> Hari</p>
	                	<?php endforeach ?>
	                	<?php } else { ?>
	                		-
	                	<?php } ?>

	                	<li><b>Predictive</b></li>
                		<?php if(!empty($notif['kalibrasi_predictive'])){ ?>
	                	<?php foreach ($notif['kalibrasi_predictive'] as $k) : ?>
	                		<hr style="margin:5px 0px;"><a href="<?php echo base_url('new/tool/detail/'.$k->id) ?>"><?php echo $k->name.' - '.$k->no_inventarisasi ?></a> Kurang <?php echo intval(abs(strtotime(date('d-m-Y')) - strtotime($k->start_date)) / 86400); ?> Hari</p>
	                	<?php endforeach ?>
	                	<?php } else { ?>
	                		-
	                	<?php } ?>
                	</ul>
                <?php endif ?>
           </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
	$('table').DataTable()
	<?php //if(!empty($notif['proyek'] or !empty($notif['kalibrasi']))): ?>
		$('#notifmodal').modal('show')
	<?php //endif ?>
</script>
