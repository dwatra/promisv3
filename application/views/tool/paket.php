<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Master Paket</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<div class="row" style="margin-bottom:  15px; margin-top: 5px">
				<div class="col-md-5">
					<div class="input-group">
						<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" class='btn btn-default btn-sm' title="Filter"><span class="glyphicon glyphicon-search"></span></button>
							<button type="button" class="btn waves-effect btn-sm btn-default" title="Reset"><span class="glyphicon glyphicon-refresh"></span></button>
						</span>
					</div>
					
				</div>
				<div class="col-md-1">
					<a href="<?php echo base_url('/new/paket/new') ?>" class=" btn btn-warning">
						<span class="glyphicon glyphicon glyphicon-plus"></span> Paket Baru
					</a>	
				</div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama Paket</th>
						<th class="text-center">Nama Proyek</th>
						<!-- <th class="text-center">Status</th> -->
						<th class="text-center">Keterangan</th>
						<th></th>
					</tr>
				</thead>
				<tbody class="text-center">
					
				</tbody>
			</table>
		</div>
    </div>
</section>
<script type="text/javascript">
	var datatable = $('table').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": true,
        ajax: {
          url: "<?php echo base_url('/new/paket/load_data') ?>"
        },
        "order": [
          [0, 'desc']
        ],
		"language": {
            "lengthMenu": "Perhalaman _MENU_",
            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
        },
        "columns": [
        	{ "name": "NEW_TL_PAKET.ID", "searchable":false},
        	{ "name": "NEW_TL_PAKET.NAME" },
        	{ "name": "RAB_PROYEK.NAMA_PROYEK" },
        	// { "name": "NEW_TL_PAKET.TYPE" },
        	{ "name": "NEW_TL_PAKET.KETERANGAN" },
         	{ className: "td-right","orderable": false, "targets": [ 10 ] }
         ],
        "iDisplayLength": 10,
        "scrollX" : false,
    });
    $('.dataTables_filter').css('display', 'none')
    $('#filter').keyup(function(){
          datatable.search($(this).val()).draw();
    });
    $(document).ready(function() {
    	$("input[name='DataTables_Table_0_length']").attr('disabled' , 'true');
    });
    function delete_data_this(id){
    	$.ajax({
    		url: '<?= base_url() ?>new/paket/delete/'+id,
    		type: 'POST',
    		dataType: 'json',
    		success:function(resp){
    			if (resp.status == 200) {
					datatable.draw();
    			}
    		}
    	});
    	
    }
</script>