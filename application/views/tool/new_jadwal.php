<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Kalibrasi</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label for="invt" class="col-sm-3 control-label">
						Kategori&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<select  data-placeholder="Pilih..." tabindex="2" name="kategori" id="kategori" class="form-control w-100" required>
							<option value="mekanik">Mekanik</option>
							<option value="listrik">Listrik</option>
							<option value="instrument">Instrument</option>
							<option value="predictive">Predictive</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="tool" class="col-sm-3 control-label">
						Tool&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<select  data-placeholder="Pilih..." tabindex="2" name="tool" id="tool" class="form-control w-100" required>
							
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="invt" class="col-sm-3 control-label">
						No Inventarisasi&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="invt" id="invt" class="form-control w-100" value="<?php echo @$_GET['invt'] ?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="start" class="col-sm-3 control-label">
						Periode&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-3">
						<input autocomplete="off" type="text" name="start" id="start" class="form-control w-100" >
					</div>
					<label for="endi" class="col-sm-2 control-label">
						Sampai dengan
					</label>
					<div class="col-sm-3">
						<input autocomplete="off" type="text" name="end" id="end" class="form-control w-100" >
					</div>
				</div>
				<div class="form-group">
					<label for="ket" class="col-sm-3 control-label">
						Keterangan
					</label>
					<div class="col-sm-8">
						<textarea class="form-control w-100 h-100 " rows="7" name="ket" id="ket"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
			<table class="table table-striped table-hover" style="display: none">
				<thead>
					<th>No.</th>
					<th>Kalibrasi Terakhir</th>
					<th>Keterangan</th>
				</thead>
				<tbody id="tbody">

				</tbody>
			</table>
		</div>
    </div>
</section>
<script type="text/javascript">
	$(document).on('ready', function(){
		$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
		$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
		$('#kategori').on('change', function(event) {
			$.ajax({
				url: '<?php echo base_url("/new/tool/get_tool_by_kategori?kategori=") ?>'+$('#kategori').val(),
				type: 'GET',
				dataType: 'json',
				success : function(data){
					var html = '';
					for (var i = 0; i < data.length; i++) {
						html += `<option value="${data[i].no_inventarisasi}">${data[i].no_inventarisasi+' - '+data[i].name}</option>`
					}
					$('#tool').html(html);
					$('#tool').trigger('change');
				}
			})
		});
		$('#tool').on('change', function(event){
			$('#invt').val($('#tool').val());
			$.ajax({
				url: '<?php echo base_url("/new/tool/get_old_jwl?invt=") ?>'+$('#tool').val(),
				type: 'GET',
				dataType: 'json',
				success : function(data){
					var html = '';
					for (var i = 0; i < data.length; i++) {
						html += `<tr>
									<td>${i+1}</td>
									<td>${data[i].start_date+' - '+data[i].end_date}</td>
									<td>${data[i].Keterangan}</td>
								</tr>`
					}
					$('#tbody').html(html);
					$('.table').show();
				}
			})
		});
	})
</script>