<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Rencanakan Tool</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.select2-selection--multiple{
		border-radius: 10px !important;
		padding-left: 5px;
		min-height: 30px;
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label for="proyek" class="col-sm-3 control-label">
						Proyek&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<!-- <input autocomplete="off" type="text" name="" id="" class="form-control w-100" > -->
						<select  data-placeholder="Pilih..." tabindex="2" id="proyek" name="proyek" class="form-control w-100">
							<?php foreach ($projects as $project): ?>
								<option value="<?php echo $project->ID_PR ?>"><?php echo strtoupper($project->NAMA) ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="paket" class="col-sm-3 control-label">
						Paket&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<!-- <input autocomplete="off" type="text" name="" id="" class="form-control w-100" > -->
						<select  data-placeholder="Pilih..." tabindex="2" name="paket" id="paket" class="form-control w-100">
							<?php foreach ($pakets as $paket): ?>
								<option value="<?php echo $paket->ID ?>"><?php echo strtoupper($paket->NAME) ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="user" class="col-sm-3 control-label">
						Admin Proyek&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<!-- <input autocomplete="off" type="text" name="" id="" class="form-control w-100" > -->
						<select  data-placeholder="Pilih..." tabindex="2" name="user" id="user" class="form-control w-100">
							<?php foreach ($users as $user): ?>
								<option value="<?php echo $user->ID ?>"><?php echo strtoupper($user->NAME) ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						Berkas
					</label>
					<div class="col-sm-7">
						<div id="fileprogress" class="progress" style="display:none">
							<div class="progress-bar progress-bar-success"></div>
						</div>
						<div id="filefiles" class="files read_detail"></div>
						<div id="fileerrors" style="color:red"></div>
						<span class="label label-upload">Ext : PDF,DOC,DOCX,XLS,XLSX,JPG</span> &nbsp;&nbsp;&nbsp;<span class="label label-upload">Max : 200 kb</span><br/>
						<span class="btn btn-upload fileinput-button">
					        <i class="glyphicon glyphicon-upload"></i>
					        <span>Select files...</span>
					        <input id="fileupload" name="fileupload" type="file" multiple>
					    </span>
					</div>
	    		</div>
				<div class="form-group">
					<label for="ket" class="col-sm-3 control-label">
						Keterangan
					</label>
					<div class="col-sm-8">
						<textarea class="form-control w-100 h-100 " rows="7" name="ket" id="ket"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</section>
<script type="text/javascript">
	$('#fileupload').fileupload({
	        url: "http://localhost:8080/pjbs-api/api/transaksi/file",
	        dataType: 'json',
	        done: function (e, data) {
	        	var file = data.result;
	        	console.log(file);
	            if(file == "error"){
	            	var error = data.result.errors;
	                $('<p onclick="$(this).remove()">'+"Terjadi Kesalahan"+'</p>').appendTo('#fileerrors');
		        }else{
	                $('<p class="pfile"><a target="_BLANK" href="http://localhost:8080/pjbs-api/assets/upload/tool/transaksi/doc/'+file.file_name+'">'+file.file_name+'</a></p>').appendTo('#filefiles');
	                $('<input type="hidden" name="filename[]" value="'+file.file_name+'">').appendTo('#filefiles');
		        }
	            $('#fileprogress').hide();
	        },
	        progressall: function (e, data) {
	            $('#fileprogress').show();
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#fileprogress .progress-bar').css(
	                'width',
	                progress + '%'
	            );
	        },
	        fail: function(a, data){
            	$('<p onclick="$(this).remove()">'+data.errorThrown+'</p>').appendTo('#fileerrors');
	            $('#fileprogress').hide();
	        }
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');
</script>