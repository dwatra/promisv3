<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Master Paket</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label for="invt" class="col-sm-3 control-label">
						No Inventarisasi&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="invt" id="invt" class="form-control w-100" >
					</div>
				</div>
				<div class="form-group">
					<label for="start" class="col-sm-3 control-label">
						Periode&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-3">
						<input autocomplete="off" type="text" name="start" id="start" class="form-control w-100" value="">
					</div>
					<label for="endi" class="col-sm-2 control-label">
						Sampai dengan
					</label>
					<div class="col-sm-3">
						<input autocomplete="off" type="text" name="end" id="end" class="form-control w-100" >
					</div>
				</div>
				<div class="form-group">
					<label for="ket" class="col-sm-3 control-label">
						Keterangan
					</label>
					<div class="col-sm-8">
						<textarea class="form-control w-100 h-100 " rows="7" name="ket" id="ket"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</section>
<script type="text/javascript">
	$(document).on('ready', function(){
		$('#start').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
		$('#end').datetimepicker({format: "DD-MM-YYYY",useCurrent:false})
	})
</script>