<?php if(!$edited){ ?>
<div class="row">
	<div class="col-sm-12">
		<center>
		<?php if($rowheader1['is_selesai']){ ?>
			<h4>Selesai dalam durasi <b><?=round($rowheader1['durasi'])?> Hari</b></h4>
			<?php }else{ ?>
			<h4>
				Progress <b><?=round($rowheader1['durasi'])?> Hari </b> dari <b><?=$rowheader1['rencanadurasi']?></b>
			</h4>
			<?php }	?>
		</center>
		<div class="row">
			<div class="col-sm-6">
				<b>FISIK :</b>
					<div class="progress" title="<?=round($rowheader1['progress'],2)?> %">
                        <div class="progress-bar progress-bar-green" style="width: <?=round($rowheader1['progress'],2)?>%;"><?=round($rowheader1['progress'],2)?>%</div>
                      </div>
                      <div style="text-align: right; color: #999"><small><i>Terakhir update realisasi : <?=Eng2Ind($rowheader2['last_update_realisasi_wbs'])?></i></small></div>
			</div>
			<div class="col-sm-6">
			<b>BIAYA :</b>
					<?php
						$prosen=0;
						if($rowheader1['nilai_rab'])
							$prosen = (float)$rowheader1['nilai_realisasi']/(float)$rowheader1['nilai_rab']*100;
					?>
					<div class="progress" title="<?=round($prosen,2)?> %">
                        <div class="progress-bar <?=($prosen>100?"progress-bar-red":(($prosen==100)?"progress-bar-blue":"progress-bar-green"))?>" style="width: <?=round($prosen,2)?>%;"><?=round($prosen,2)?>%</div>
                      </div>
                      <div style="text-align: right; color: #999"><small><i>Terakhir update realisasi : <?=Eng2Ind($rowheader2['last_update_realisasi_rab'])?></i></small></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<br/>
	<hr/>
	<br/>
</div>

<script src="<?=site_url("assets/template/backend/plugins/chartjs/Chart.bundle.js")?>"></script>
<script src="<?=site_url("assets/template/backend/plugins/chartjs/utils.js")?>"></script>


<script type="text/javascript">
    
    var COLORS = [
        '#4dc9f6',
        '#f67019',
        '#f53794',
        '#537bc4',
        '#acc236',
        '#166a8f',
        '#00a950',
        '#58595b',
        '#8549ba'
    ];

    window.onload = function() {
        var ctx = document.getElementById('omega').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: {
                satuan:'',
                labels: <?php echo json_encode(array_keys($realisasi)); ?>,
                datasets: [{
                    data:<?php echo json_encode(array_values($realisasi)); ?>,
                    backgroundColor: COLORS[0],
                    borderColor: COLORS[0]
                }],
            },
            options: {
                legend: {
                    display: false
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:false
                        },
                        gridLines: {
                            display:false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function(value, index, values) {
                            	var label = value.toString();
	                            var tahun = label.substring(0,4);
	                            var bulan = label.substring(4,6);
	                            var tgl = label.substring(6,8);
	                            label = tgl+'-'+bulan+'-'+tahun;
                                return label;
                            }
                        },
                        gridLines: {
                            display:false
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var label = data.datasets[0].data[tooltipItem.index];
                           
                            return label;
                        }
                    }
                }
            }
        });
    }
</script>
<?php } ?>


<div class="col-sm-6">

<?php 

$from = UI::createTextArea('nama_pekerjaan',$row['nama_pekerjaan'],'2','',$edited,$class='form-control ');
echo UI::createFormGroup($from, $rules["nama_pekerjaan"], "nama_pekerjaan", "Deskripsi Pekerjaan");
?>

<?php 
if($row['id_pekerjaan']){
echo UI::createFormGroup("<a href='javascript:void(0)' onclick='goSubmit(\"pernyataan_emergency\")'><span class='glyphicon glyphicon-download'></span> Template Pernyataan Emergency</a>");
echo UI::createFormGroup("<a href='javascript:void(0)' onclick='goSubmit(\"pengajuan_prk\")'><span class='glyphicon glyphicon-download'></span> Template Pengajuan PRK</a>");
}

$from = "<div class='col-sm-6 no-padding no-margin'>".
UI::createTextBox('no_prk',$row['no_prk'],'200','20',$edited,$class='form-control ',"style='width:200px'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Tgl.</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_prk',$row['tgl_prk'],'10','10',$edited,$class='form-control datepicker',"style='width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["no_prk"], "no_prk", "No. PRK");
?>

<?php 
$from = UI::createSelect('id_tipe_pekerjaan',$mttipepekerjaanarr,$row['id_tipe_pekerjaan'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_tipe_pekerjaan"], "id_tipe_pekerjaan", "Tipe Pekerjaan");
?>

<?php 
$from = "<div class='col-sm-6 no-padding no-margin'>".
UI::createTextBox('no_pekerjaan',$row['no_pekerjaan'],'200','20',$edited,$class='form-control ',"style='width:100%'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Tgl.</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_pekerjaan',$row['tgl_pekerjaan'],'10','10',$edited,$class='form-control datepicker',"style='width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["no_pekerjaan"], "no_pekerjaan", "No. SP3");
?>

<?php 
$from = UI::createUploadMultiple("sp3", $row['sp3'], $page_ctrl, $edited, "sp3");
echo UI::createFormGroup($from, $rules["sp3"], "sp3", "Lampiran SP3");
?>

<?php 
$from = "<div class='col-sm-6 no-padding no-margin'>".
UI::createTextBox('no_kontrak',$row['no_kontrak'],'200','20',$edited,$class='form-control ',"style='width:100%'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Tgl.</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_kontrak',$row['tgl_kontrak'],'10','10',$edited,$class='form-control datepicker',"style='width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["no_kontrak"], "no_kontrak", "No. Kontrak");
?>

<?php 
$from = UI::createUploadMultiple("kontrak", $row['kontrak'], $page_ctrl, $edited, "kontrak");
echo UI::createFormGroup($from, $rules["kontrak"], "kontrak", "Lampiran Kontrak");
?>

<?php 
$from = UI::createUploadMultiple("file", $row['file'], $page_ctrl, $edited, "kontrak");
echo UI::createFormGroup($from, $rules["file"], "file", "Berkas Pekerjaan");
?>
				

</div>
<div class="col-sm-6">

<?php 
$from = "<div class='col-sm-6 no-padding no-margin'>".
UI::createTextBox('nilai_hpp',$row['nilai_hpp'],'','',$edited,$class='form-control rupiah',"style='text-align:right; width:200px'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Tgl.</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_hpp',$row['tgl_hpp'],'10','10',$edited,$class='form-control datepicker',"style='width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["nilai_hpp"], "nilai_hpp", "Nilai HPP");
?>

<?php 
$from = "<div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_mulai_pelaksanaan',$row['tgl_mulai_pelaksanaan'],'10','10',$edited,$class='form-control datepicker',"style='width:100%'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>sd</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_selesai_pelaksanaan',$row['tgl_selesai_pelaksanaan'],'10','10',$edited,$class='form-control datepicker',"style='width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["tgl_mulai_pelaksanaan"], "tgl_mulai_pelaksanaan", "Tgl. Pekerjaan");
?>

<?php 
$from = "<label class='col-sm-1 control-label' style='padding-left: 0px; '>H-</label><div class='col-sm-2 no-padding no-margin'>".
UI::createTextNumber('hmin',$row['hmin'],'','',$edited,$class='form-control ',"style='text-align:right; width:100%'")
."</div><label class='col-sm-1 control-label' style='padding-left: 2px; '>H</label><div class='col-sm-2 no-padding no-margin'>".
UI::createTextNumber('h',$row['h'],'','',$edited,$class='form-control ',"style='text-align:right; width:100%'")
."</div><label class='col-sm-1 control-label' style='padding-left: 2px; '>H+</label><div class='col-sm-2 no-padding no-margin'>".
UI::createTextNumber('hplus',$row['hplus'],'','',$edited,$class='form-control ',"style='text-align:right; width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["hmin"], "hmin", "Jumlah Hari");
?>

<?php 
$from = UI::createSelect('ttd[0][nid]',$ttdarr,$row['ttd'][0]['nid'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"")."<br/>";
$from .= UI::createSelect('ttd[1][nid]',$ttdarr,$row['ttd'][1]['nid'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"")."<br/>";
$from .= UI::createSelect('ttd[2][nid]',$ttdarr,$row['ttd'][2]['nid'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"")."<br/>";
echo UI::createFormGroup($from, $rules["ttd"], "ttd", "TTD");
?>

<?php 
if($row['tgl_mulai_rab']===null)
	$row['tgl_mulai_rab'] = date('d-m-Y');

if($is_selesai){
	$from = "<div class='col-sm-5 no-padding no-margin'>".
	UI::createTextBox('tgl_mulai_rab',$row['tgl_mulai_rab'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'")
	."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>sd</label><div class='col-sm-5 no-padding no-margin'>".
	UI::createTextBox('tgl_selesai_rab',$row['tgl_selesai_rab'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'")
	."</div>";
}else{
	$from = UI::createTextBox('tgl_mulai_rab',$row['tgl_mulai_rab'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
}
echo UI::createFormGroup($from, $rules["tgl_mulai_rab"], "tgl_mulai_rab", "Tgl. RAB");
?>

<?php 
$from = UI::createSelect('id_pic',$mtpegawaiarr,$row['id_pic'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_pic"], "id_pic", "PIC RAB");
?>	

<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>

<?php if($edited){ ?>
	<script type="text/javascript">
		$('#tgl_mulai_pelaksanaan').on('dp.change', function(e){
			datediffpelaksanaan();
		});

		$('#tgl_selesai_pelaksanaan').on('dp.change', function(e){
			datediffpelaksanaan();
		});

		$(function(){
			datediffpelaksanaan();
		})

		function datediffpelaksanaan(){
			var start = new Date(Eng2Ind($("#tgl_mulai_pelaksanaan").val())),
		    end   = new Date(Eng2Ind($("#tgl_selesai_pelaksanaan").val())),
		    diff  = new Date(end - start),
		    days  = diff/1000/60/60/24;
		    $("#h").val(days+1);
		}
	</script>
<?php } ?>