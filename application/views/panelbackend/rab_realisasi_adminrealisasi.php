
<div class="col-sm-6">

<?php 
$from = UI::createSelect('id_pos_anggaran',$mtposanggaranarr,$rowheader3['id_pos_anggaran'],false,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_pos_anggaran"], "id_pos_anggaran", "Pos Anggaran");

$row['id_rab_detail'] = $id_rab_detail;
$rabdetailarr[$row['id_rab_detail']] = $rowheader3['uraian'];
$from = UI::createSelect('id_rab_detail',$rabdetailarr,$row['id_rab_detail'],false,$class='form-control ',"style='width:auto; max-width:100%;'");
$from .= UI::createTextHidden('id_rab_detail',$row['id_rab_detail'],$edited);
echo UI::createFormGroup($from, $rules["id_rab_detail"], "id_rab_detail", "Uraian");
if($id_jasa_material){
$row['id_jasa_material'] = $id_jasa_material;
$rabjasa_materialarr[$row['id_jasa_material']] = $rowheader4['nama'];
$from = UI::createSelect('id_jasa_material',$rabjasa_materialarr,$row['id_jasa_material'],false,$class='form-control ',"style='width:auto; max-width:100%;'");
$from .= UI::createTextHidden('id_jasa_material',$row['id_jasa_material'],$edited);
echo UI::createFormGroup($from, $rules["id_jasa_material"], "id_jasa_material", "Jasa/Material");
}
?>
</div>
<div class="col-sm-6">
	
</div>
<?php
if(count($rowsrealisasi)){ ?>
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th width="10px">No.</th>
			<th>NAMA</th>
			<th width="130px">TGL</th>
			<th>NILAI</th>
			<th>KETERANGAN</th>
			<th>LAMPIRAN</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no=1;
		if(count($rowsrealisasi)){
			foreach($rowsrealisasi as $r){ 
				if(!$r['id_jasa_material'])
					$r['id_jasa_material'] = 0;

				$total+=$r['nilai'];
				?>
		<tr>
			<td align="center"><?=$no++?></td>
			<td><a href="<?=site_url("panelbackend/rab_realisasi_admin/detail/$r[id_rab_detail]/$r[id_jasa_material]/$r[id_realisasi]")?>"><?=$r['nama']?></a></td>
			<td><?=Eng2Ind($r['tgl'])?></td>
			<td style="text-align: right;"><a href="<?=site_url("panelbackend/rab_realisasi_admin/detail/$r[id_rab_detail]/$r[id_jasa_material]/$r[id_realisasi]")?>"><?=rupiah($r['nilai'],2)?></a></td>
			<td><?=$r['keterangan']?></td>
			<td>
				<?php if($filerealisasi[$r['id_realisasi']]){
					$arr=array();
					foreach($filerealisasi[$r['id_realisasi']] as $r1){
						$arr[]="<a href='".site_url("panelbackend/rab_realisasi/open_file/$r1[id]")."' target='_BLANK'>$r1[name]</a>";
					}

					echo implode("<br/>", $arr);
				}?>
			</td>

		</tr>

	<?php }?>
		<tr>
			<td align="center" colspan="3">Total</td>
			<td style="text-align: right;"><?=rupiah($total,2)?></td>
			<td></td>
			<td>
			</td>

		</tr>
	<?php }else{ ?>
		<tr><td colspan="6"><i>Belum ada data</i></td></tr>
	<?php } ?>
	</tbody>
</table>
<?php 
}

	echo '<div style="text-align:right"><br/><a href="'.site_url("panelbackend/rab_realisasi_admin/add/$rowheader3[id_rab_detail]/$id_jasa_material").'" class="btn btn-sm btn-info waves-effect "><span class="glyphicon glyphicon-check"></span> Tambah Realisasi</a></div>';
?>