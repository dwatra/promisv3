<!-- library of chart -->
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>
<!-- end -->
<section class="content-header" <?=($width?"style='max-width:$width; margin-right:auto; margin-left:auto;'":"")?>>

        <h1 style="float: left">
        <?=$page_title?>
        <?=$layout_header?>
        </h1>
      <div class="pull-right">
          <?php echo UI::getButton('add',null, $add, 'btn-warning', "Add New Project", false, $access_role, $page_escape)?>
          <?php echo UI::getButton('print')?>
      </div>
      <div style="clear: both;"></div>
</section>
<section class="content" <?=($width?"style='max-width:$width'":"")?>>
  <div class="box box-default">
    <div class="box-body">

      <?php  if(is_array($_SESSION[SESSION_APP]['loginas']) && count($_SESSION[SESSION_APP]['loginas'])){ ?>
      <div class="alert alert-warning">
          Anda sedang mengakses user lain. <a href="<?=base_url("panelbackend/home/loginasback")?>" class="alert-link">Kembali</a>.
      </div>
      <?php }?>

      <?=FlashMsg()?>

<?php
$bulanlist = ListBulan();
$warnarr = array();
foreach($status_proyek as $r){
    $warnarr[$r['id_status_proyek']] = $r['warna'];
}
?>
    <div class="row">
        <div class="col-sm-12">
            <h3 align="center">PROYEK <?=$tahun?></h3><br/>
            <div class="table-responsive">
            <table width="100%" height="250px">
                <tr>
                    <?php 
                    $max_rencana = $total_rencana_proyek['maksimal'];
                    $total_rencana = $total_rencana_proyek['total_data'];
                    $rows = $total_rencana_proyek['rows'];

                    ?>
                    <td style="vertical-align: bottom; width: 10px; border-bottom: 1px solid #eee;border-right: 1px solid #eee;">
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*1)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*2)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*3)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*4)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*5)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*6)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*7)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*8)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*9)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*10)),2)?>M</span></div>
                    </td>
                    <?php 
                    $totalp = array();
                    foreach($bulanlist as $k=>$v){ 
                        if(is_array($rows[$k]) && count($rows[$k])){
                            $width = count($rows[$k])/$total_rencana*100;
                            $rs = $rows[$k];
                            $width1 = (100-(count($rs)+3))/count($rs);
                        ?>
                    <td style="vertical-align: bottom; padding: 0px !important; position: relative; border-bottom: 1px solid #eee; width: <?=$width?>%;border-left: 1px solid #eee;">

                        <?php 
                        $left=3;
                        foreach($rs as $i=>$r){ 

                        if(!$totalp[$r['id_status_proyek']])
                            $totalp[$r['id_status_proyek']] = 1;
                        else
                            $totalp[$r['id_status_proyek']]++;

                        if(!$max_rencana)
                            $height = 0;
                        else
                            $height = ($r['nilai_rab']/1000000000)/$max_rencana*100;
                        
                        if($i==0){ ?>
                        <div style="background: <?=$r['warna']?>;width: <?=$width1?>%;height:<?=$height?>%;position: absolute;bottom: 0px;left: <?=$left?>%;" title="<?=round(($r['nilai_rab']/1000000000),2)?>M, <?=$r['nama_proyek']?>" data-toggle="tooltip" data-placement="top" onclick="window.location='<?=site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")?>'"></div>
                        <?php }else{ ?>
                        <div style="background: <?=$r['warna']?>;width: <?=$width1?>%;height:<?=$height?>%;position: absolute;bottom: 0px;left: <?=$left=($left+1+$width1)?>%;" title="<?=round(($r['nilai_rab']/1000000000),2)?>M, <?=$r['nama_proyek']?>" data-toggle="tooltip" data-placement="top" onclick="window.location='<?=site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")?>'"></div>
                        <?php } } ?>

                    </td>
                    <?php } } ?>
                </tr>
                <tr>
                    <td style="height: 10px; position: relative;"></td>
                    <?php $no=1; foreach($bulanlist as $k=>$v){ 
                        if(is_array($rows[$k]) && count($rows[$k])){
                            $rs = $rows[$k];
                            $width1 = (100-(count($rs)+3))/count($rs);
                        ?>
                        <td style="vertical-align: bottom; padding: 0px !important; position: relative; border-right: 1px solid #fff; text-align: center; height: 12px;">

                        <?php 
                        $left=3;
                        foreach($rs as $i=>$r){ 
                            if($i==0){ ?>
                            <div style="font-size:9px; width: <?=$width1?>%;position: absolute;bottom: 0px;left: <?=$left?>%;"><?=$no++?></div>
                            <?php }else{ ?>
                            <div style="font-size:9px; width: <?=$width1?>%;position: absolute;bottom: 0px;left: <?=$left=($left+1+$width1)?>%;"><?=$no++?></div>
                            <?php } 
                        } ?>                            
                        </td>
                    <?php } } ?>
                </tr>
                <tr>
                    <td style="height: 10px; position: relative;"></td>
                    <?php $no=1; foreach($bulanlist as $k=>$v){ 
                        if(is_array($rows[$k]) && count($rows[$k])){
                        ?>
                        <td style="text-align: center; font-size: 11px;">
                        <?=$v?>                      
                        </td>
                    <?php } } ?>
                </tr>
            </table>
            </div>
        </div>
        <br/>
        <div class="col-sm-12">
            <br/>
            <table  align="center">
                <tr>
                    <?php foreach($status_proyek as $r){ ?>
                    <td style="background:<?=$r['warna']?>; width:15px;"></td>
                    <td style="padding: 0px 10px; padding-right: 20px; font-size: 11px;">
                        <a href="javascript:void(0)" onclick="setFilterStatus('<?=$r['id_status_proyek']?>')">
                        <?php if($totalp[$r['id_status_proyek']]){
                            echo "<b>".$totalp[$r['id_status_proyek']]."</b>";
                        }   
                        ?>
                        <?=$r['nama']?>
                    </a>
                    </td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function setFilterStatus(id_status_proyek){
            sessionStorage.scrollTop = 100000;
            $("#list_search_filter\\[id_status_proyek\\]").val(id_status_proyek);
            goSearch();

        }
    </script>
    </div>
</div>
  <div class="box box-default">
    <div class="box-body">
<div class="row">
        <div class="col-sm-12">
            <h3 style="text-align: center">PENYERAPAN ANGGARAN <?=$tahun?></h3><br/>
            <div class="table-responsive">
            <table width="100%" height="250px">
                <tr>
                    <?php 
                    $max_rencana = $total_realisasi_proyek['maksimal'];
                    $total_rencana = $total_realisasi_proyek['total_data'];
                    $rows = $total_realisasi_proyek['rows'];

                    ?>
                    <td style="vertical-align: bottom; width: 10px; border-bottom: 1px solid #eee;border-right: 1px solid #eee;">
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*1)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*2)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*3)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*4)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*5)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*6)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*7)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*8)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*9)),2)?>M</span></div>
                        <div style="height: 10%; display: flex; text-align: right; border-top: 1px solid #eee;"><span style="align-self: flex-end; font-size: 10px;width:100%;"><?=round(($max_rencana-(($max_rencana/10)*10)),2)?>M</span></div>
                    </td>
                    <?php foreach($bulanlist as $k=>$v){ 
                        if(is_array($rows[$k]) && count($rows[$k])){
                            $width = count($rows[$k])/$total_rencana*100;
                            $rs = $rows[$k];
                            $width1 = (100-(count($rs)+3))/count($rs);
                        ?>
                    <td style="vertical-align: bottom; padding: 0px !important; position: relative; border-bottom: 1px solid #eee; width: <?=$width?>%;border-left: 1px solid #eee;">

                        <?php 
                        $left=3;
                        foreach($rs as $i=>$r){ 
                        if($max_rencana){
                            $heightrab = ($r['nilai_rab']/1000000000)/$max_rencana*100;
                            $heightrea = ($r['nilai_realisasi']/1000000000)/$max_rencana*100;
                        }else{
                            $heightrab = 0;
                            $heightraa = 0;
                        }
                        $width2 = $width1/2;
                        if($i==0){ ?>
                        <div style="background: #145ca2;width: <?=$width2?>%;height:<?=$heightrab?>%;position: absolute;bottom: 0px;left: <?=$left?>%;" title="<?=round(($r['nilai_rab']/1000000000),2)?>M, <?=$r['nama_proyek']?>" data-toggle="tooltip" data-placement="top" onclick="window.location='<?=site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")?>'"></div>
                        <div style="background: <?=($r['nilai_realisasi']>$r['nilai_realisasi'])?"#de3b3b":"#14a23c"?>;width: <?=$width2?>%;height:<?=$heightrea?>%;position: absolute;bottom: 0px;left: <?=$left=$left+($width2)?>%;" title="<?=round(($r['nilai_realisasi']/1000000000),2)?>M, <?=$r['nama_proyek']?>" data-toggle="tooltip" data-placement="top" onclick="window.location='<?=site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")?>'"></div>
                        <?php }else{ ?>
                        <div style="background: #145ca2;width: <?=$width2?>%;height:<?=$heightrab?>%;position: absolute;bottom: 0px;left: <?=$left=($left+1+$width2)?>%;" title="<?=round(($r['nilai_rab']/1000000000),2)?>M, <?=$r['nama_proyek']?>" data-toggle="tooltip" data-placement="top" onclick="window.location='<?=site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")?>'"></div>
                        <div style="background: <?=($r['nilai_realisasi']>$r['nilai_realisasi'])?"#de3b3b":"#14a23c"?>;width: <?=$width2?>%;height:<?=$heightrea?>%;position: absolute;bottom: 0px;left: <?=$left=($left+$width2)?>%;" title="<?=round(($r['nilai_realisasi']/1000000000),2)?>M, <?=$r['nama_proyek']?>" data-toggle="tooltip" data-placement="top" onclick="window.location='<?=site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")?>'"></div>
                        <?php } } ?>

                    </td>
                    <?php } } ?>
                </tr>
                <tr>
                    <td style="height: 10px; position: relative;"></td>
                    <?php $no=1; foreach($bulanlist as $k=>$v){ 
                        if(is_array($rows[$k]) && count($rows[$k])){
                            $rs = $rows[$k];
                            $width1 = (100-(count($rs)+3))/count($rs);
                        ?>
                        <td style="vertical-align: bottom; padding: 0px !important; position: relative; border-right: 1px solid #fff; text-align: center; height: 12px;">

                        <?php 
                        $left=3;
                        foreach($rs as $i=>$r){ 
                            if($i==0){ ?>
                            <div style="font-size:9px; width: <?=$width1?>%;position: absolute;bottom: 0px;left: <?=$left?>%;"><?=$no++?></div>
                            <?php }else{ ?>
                            <div style="font-size:9px; width: <?=$width1?>%;position: absolute;bottom: 0px;left: <?=$left=($left+1+$width1)?>%;"><?=$no++?></div>
                            <?php } 
                        } ?>                            
                        </td>
                    <?php } } ?>
                </tr>
                <tr>
                    <td style="height: 10px; "></td>
                    <?php foreach($bulanlist as $k=>$v){ 
                        if(is_array($rows[$k]) && count($rows[$k])){?>
                        <td style="text-align: center; font-size: 11px;"><?=$v?></td>
                    <?php } } ?>
                </tr>
            </table>
            </div>
        </div>
        <br/>
        <div class="col-sm-12">
            <br/>
            <table align="center">
                <tr>
                    <td style="background:#145ca2; width:15px;"></td>
                    <td style="padding: 0px 10px; padding-right: 20px; font-size: 11px;">RAB</td>
                    <td style="background:#14a23c; width:15px;"></td>
                    <td style="padding: 0px 10px; padding-right: 20px; font-size: 11px;">Realisasi</td>
                    <td style="background:#de3b3b; width:15px;"></td>
                    <td style="padding: 0px 10px; padding-right: 20px; font-size: 11px;">Realisasi melebihi RAB</td>
                </tr>
            </table>
        </div>
</div>
    </div>
</div>
  <div class="box box-default">
    <div class="box-body">
            <h3 style="text-align: center">DAFTAR PROYEK TAHUN <?=$tahun?></h3><br/>
  <table class="table table-striped table-hover dataTable">
    <thead>
    <?=UI::showHeader($header, $filter_arr, $list_sort, $list_order)?>
    </thead>
    <tbody>
    <?php
    $i = $page;
    foreach($list['rows'] as $rows){
        $i++;
        echo "<tr style='background:".trim($warnarr[$rows['id_status_proyek']])."35;'>";
        echo "<td>$i</td>";
        foreach($header as $rows1){
            $val = $rows[$rows1['name']];
            if($rows1['name']=='nama_proyek'){
                echo "<td><a href='".($url=base_url($page_ctrl."/detail/$rows[$pk]"))."'>$val</a></td>";   
            }elseif($rows1['name']=='isi'){
                echo "<td>".ReadMore($val,$url)."</td>";
            }else{
                switch ($rows1['type']) {
                    case 'list':
                        echo "<td>".$rows1["value"][$val]."</td>";
                        break;
                    case 'number':
                        echo "<td style='text-align:right'>$val</td>";
                    break;
                    case 'date':
                        echo "<td>".($val)."</td>";
                        break;
                    case 'datetime':
                        echo "<td>".Eng2Ind($val)."</td>";
                        break;
                    default :
                        echo "<td>$val</td>";
                        break;
                }
            }
        }
        echo "<td style='text-align:right'>
        ".UI::showMenuMode('inlist', $rows[$pk])."
        </td>";
        echo "</tr>";
    }
    if(!count($list['rows'])){
        echo "<tr><td colspan='".(count($header)+2)."'>Data kosong</td></tr>";
    }
    ?>
    </tbody>
  </table>
  <?=UI::showPaging($paging,$page, $limit_arr,$limit,$list)?>

<?php if(!empty($pengumuman)){ ?>
<div class="modal fade" id="pengumumanmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">PEMBERITAHUAN</h4>
          </div>
          <div class="modal-body">
                <small>Proyek yang Harus Segera Dibuatkan RAB<br/>(3 Bulan Sebelum Pelaksanaan)</small>
                    <?php foreach($pengumuman as $r){ 
                        echo "<hr style='margin:5px 0px;'/>";
                        echo "<a href='".site_url("panelbackend/rab_proyek/detail/$r[id_proyek]")."'>".$r['nama_proyek'].' '.$r['nama_pekerjaan']."</a>";
                    } ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
    $(function(){
        $("#pengumumanmodal").modal('show');
    });
</script>
<?php } ?>

      <div style="clear: both;"></div>

    </div>
    <?php if($layout_footer){ ?>
    <div class="box-footer with-border">
        <?=$layout_footer?>
    </div>
    <?php } ?>
  </div><!-- /.box -->

<!-- panel table -->
<!-- <div class="row">
    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-body">
                <h3 align="center">Reminder Kalibrasi Tools</h3><br/>
                <div style="overflow: auto;">
                    <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>No Inventaris</th>
                            <th>Tools</th>
                            <th>Periode</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($kalibrasis as $kalibrasi) { ?>
                            <tr align="center">
                                <td><?php echo $kalibrasi->NO_INVENTARISASI ?></td>
                                <td><?php echo strtoupper($kalibrasi->NAME) ?></td>
                                <td><?php echo $kalibrasi->START_DATE ? date_format(date_create($kalibrasi->START_DATE), 'd M Y')." - ".date_format(date_create($kalibrasi->END_DATE), 'd M Y') : "< ".$kalibrasi->SISA_UTILITAS." jam" ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    </table>
                </div>
                <br/>
                <div style="float: right;">
                    <a href="<?php echo base_url('/new/kalibrasi') ?>" class="btn btn-warning">Detail</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel panel-inverse">
            <div class="panel-body">
                <h3 align="center">Reminder Tools Rusak</h3><br/>
                <div style="overflow: auto;">
                    <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No Inventaris</th>
                            <th>Tools</th>
                            <th>Kondsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($rusaks as $rusak) { ?>
                            <tr align="center">
                                <td><?php echo $kalibrasi->NO_INVENTARISASI ?></td>
                                <td><?php echo strtoupper($kalibrasi->NAME) ?></td>
                                <td>Rusak</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    </table>
                </div>
                <br/>
                <div style="float: right;">
                    <a href="<?php echo base_url('new/tool') ?>" class="btn btn-warning">Detail</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">
            <div class="panel-body">
                <h3 align="center">Reminder Tools Belum Kembali</h3><br/>
                <div style="overflow: auto;">
                    <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>No Inventaris</th>
                            <th>Paket Tool</th>
                            <th>Proyek</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($peminjamans as $peminjaman) { ?>
                            <tr align="center">
                                <td><?php echo $peminjaman->NO_INVENTARISASI ?></td>
                                <td><?php echo $peminjaman->NAME ?></td>
                                <td><?php echo $peminjaman->NAMA ?></td>
                                <td><?php echo $trx->START_DATE.' - '.$trx->END_DATE ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    </table>
                </div>
                <br/>
                <div style="float: right;">
                    <a href="<?php echo base_url('new/tools/transaksi') ?>" class="btn btn-warning">Detail</a>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- end -->
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Mobilisasi Tool</h3><br/>
                <div id="indoMaps" class="height-sm"></div>
                <script type="text/javascript">
                    function getMapChart(){
                        var data = [
                            ['id-ac', 1], //Aceh
                            ['id-jt', 2], //Jawa Tengah
                            ['id-be', 3], //Bengkulu
                            ['id-bt', 4], //Banten
                            ['id-kb', 5], //Kalimantan Barat
                            ['id-bb', 6], //Bangka-Belitung
                            ['id-ba', 7], //Bali
                            ['id-ji', 8], //Jawa Timur
                            ['id-ks', 9], //Kalimantan Selatan
                            ['id-nt', 10], //Nusa Tenggara Timur
                            ['id-se', 11], //Sulawesi Selatan
                            ['id-kr', 12], //Kepulauan Riau
                            ['id-ib', 13], //Irian Jaya Barat
                            ['id-su', 14], //Sumatera Utara
                            ['id-ri', 15], //Riau
                            ['id-sw', 16], //Sulawesi Utara
                            ['id-ku', 17], //Kalimantan Utara
                            ['id-la', 18], //Maluku Utara
                            ['id-sb', 19], //Sumatera Barat
                            ['id-ma', 20], //Maluku
                            ['id-nb', 21], //Nusa Tenggara Barat
                            ['id-sg', 22], //Sulawesi Tenggara
                            ['id-st', 23], //Sulawesi Tengah
                            ['id-pa', 24], //Papua
                            ['id-jr', 25], //Jawa Barat
                            ['id-ki', 26], //Kalimantan Timur
                            ['id-1024', 27], //Lampung
                            ['id-jk', 28], //Jakarta Raya
                            ['id-go', 29], //Gorontalo
                            ['id-yo', 30], //Yogyakarta
                            ['id-sl', 31], //Sumatera Selatan
                            ['id-sr', 32], //Sulawesi Barat
                            ['id-ja', 33], //Jambi
                            ['id-kt', 34] //Kalimantan Tengah
                        ];
                        Highcharts.mapChart('indoMaps', {
                            chart: {
                                map: 'countries/id/id-all'
                            },
                            title: {
                                text: ''
                            },
                            // untuk show label nama kota
                            /*
                            mapNavigation: {
                                enabled: true,
                                buttonOptions: {
                                    verticalAlign: 'bottom'
                                }
                            },
                            */
                            colorAxis: {
                                min: 0,
                            },
                            series: [{
                                data: data,
                                name: 'Tool',
                                states: {
                                    hover: {
                                        color: '#db1e61'
                                    }
                                },
                                dataLabels: {
                                    enabled: false,
                                    format: '{point.name}'
                                }
                            }]
                        });
                    }
                    getMapChart();
                </script>
            </div>
        </div>
    </div>
</div>
<!-- end -->

<!-- panel barchart -->
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Gap Tool</h3><br/>
                <div id="barchart_tool" class="height-sm"></div>
                <script type="text/javascript">
                    function getBarChart(){
                        var dataBarchart = {
                          series: [{
                          name: 'Kebutuhan',
                          data: [
                          <?php foreach ($gap_tool['plan'] as $p) {
                             echo "$p,";
                          } ?>
                          ] //value data
                        }, {
                          name: 'Ketersediaan',
                          data: [<?php foreach ($gap_tool['stok'] as $s) {
                             echo "$s,";
                          } ?>] //value data
                        }],
                          chart: {
                          type: 'bar',
                          height: 350
                        },
                        plotOptions: {
                          bar: {
                            horizontal: false,
                            columnWidth: '55%',
                          },
                        },
                        dataLabels: {
                          enabled: false,
                        },
                        stroke: {
                          show: true,
                          width: 2,
                          colors: ['transparent']
                        },
                        xaxis: {
                          categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'] //value label
                        },
                        yaxis: {
                          title: {
                            // text: 'Jumlah Man Power'
                          }
                        },
                        fill: {
                          opacity: 1,
                        },
                        tooltip: {
                          y: {
                            formatter: function (val) {
                              return val + " Item"
                            }
                          }
                        },
                        colors:['#0879ce', '#db1e61']
                        };

                        var barchart_tool = new ApexCharts(document.querySelector("#barchart_tool"), dataBarchart);
                        barchart_tool.render();
                    }
                    getBarChart();
                </script>
            </div>
        </div>
    </div>
</div>
<!-- panel map chart -->
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Mobilisasi Man Power</h3><br/>
                <div id="indoMaps2" class="height-sm"></div>
                <script type="text/javascript">
                    function getMapChart(){
                        var data = [
                            ['id-ac', 1], //Aceh
                            ['id-jt', 2], //Jawa Tengah
                            ['id-be', 3], //Bengkulu
                            ['id-bt', 4], //Banten
                            ['id-kb', 5], //Kalimantan Barat
                            ['id-bb', 6], //Bangka-Belitung
                            ['id-ba', 7], //Bali
                            ['id-ji', 8], //Jawa Timur
                            ['id-ks', 9], //Kalimantan Selatan
                            ['id-nt', 10], //Nusa Tenggara Timur
                            ['id-se', 11], //Sulawesi Selatan
                            ['id-kr', 12], //Kepulauan Riau
                            ['id-ib', 13], //Irian Jaya Barat
                            ['id-su', 14], //Sumatera Utara
                            ['id-ri', 15], //Riau
                            ['id-sw', 16], //Sulawesi Utara
                            ['id-ku', 17], //Kalimantan Utara
                            ['id-la', 18], //Maluku Utara
                            ['id-sb', 19], //Sumatera Barat
                            ['id-ma', 20], //Maluku
                            ['id-nb', 21], //Nusa Tenggara Barat
                            ['id-sg', 22], //Sulawesi Tenggara
                            ['id-st', 23], //Sulawesi Tengah
                            ['id-pa', 24], //Papua
                            ['id-jr', 25], //Jawa Barat
                            ['id-ki', 26], //Kalimantan Timur
                            ['id-1024', 27], //Lampung
                            ['id-jk', 28], //Jakarta Raya
                            ['id-go', 29], //Gorontalo
                            ['id-yo', 30], //Yogyakarta
                            ['id-sl', 31], //Sumatera Selatan
                            ['id-sr', 32], //Sulawesi Barat
                            ['id-ja', 33], //Jambi
                            ['id-kt', 34] //Kalimantan Tengah
                        ];
                        Highcharts.mapChart('indoMaps2', {
                            chart: {
                                map: 'countries/id/id-all'
                            },
                            title: {
                                text: ''
                            },
                            // untuk show label nama kota
                            /*
                            mapNavigation: {
                                enabled: true,
                                buttonOptions: {
                                    verticalAlign: 'bottom'
                                }
                            },
                            */
                            colorAxis: {
                                min: 0,
                            },
                            series: [{
                                data: data,
                                name: 'Man Power',
                                states: {
                                    hover: {
                                        color: '#db1e61'
                                    }
                                },
                                dataLabels: {
                                    enabled: false,
                                    format: '{point.name}'
                                }
                            }]
                        });
                    }
                    getMapChart();
                </script>
            </div>
        </div>
    </div>
</div>
<!-- end -->
<br>
<!-- panel barchart -->
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Statistik Man Power</h3><br/>
                <div id="barchart" class="height-sm"></div>
                <script type="text/javascript">
                    function getBarChart(){
                        var dataBarchart = {
                          series: [{
                          name: 'Kebutuhan',
                          data: [250, 1000, 150, 900, 900, 550, 400, 150, 250, 250] //value data
                        }, {
                          name: 'Realisasi',
                          data: [400, 800, 650, 700, 500, 250, 750, 650, 100, 400] //value data
                        }],
                          chart: {
                          type: 'bar',
                          height: 350
                        },
                        plotOptions: {
                          bar: {
                            horizontal: false,
                            columnWidth: '55%',
                            // endingShape: 'rounded'
                          },
                        },
                        dataLabels: {
                          enabled: false,
                        },
                        stroke: {
                          show: true,
                          width: 2,
                          colors: ['transparent']
                        },
                        xaxis: {
                          categories: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] //value label
                        },
                        yaxis: {
                          title: {
                            // text: 'Jumlah Man Power'
                          }
                        },
                        fill: {
                          opacity: 1,
                        },
                        tooltip: {
                          y: {
                            formatter: function (val) {
                              return val + " Orang"
                            }
                          }
                        },
                        colors:['#0879ce', '#db1e61']
                        };

                        var barchart = new ApexCharts(document.querySelector("#barchart"), dataBarchart);
                        barchart.render();
                    }
                    getBarChart();
                </script>
                <div style="overflow: auto;">
                    <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Proyek</th>
                            <th>Lokasi</th>
                            <th>Tgl Mulai</th>
                            <th>Tgl Selesai</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr align="center"><td colspan="9">Tidak ada data</td></tr>
                        <?php //foreach ($variable as $key => $value) { ?>
                            
                        <?php //} ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end -->

</section>

<style type="text/css">
    table.dataTable {
    clear: both;
    margin-bottom: 6px !important;
    max-width: none !important;
}

</style>
<script type="text/javascript">
    $('.highcharts-credits').css('display', 'none');
</script>