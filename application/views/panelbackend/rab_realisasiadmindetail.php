
<div class="col-sm-6">

<?php 
$from = UI::createSelect('id_pos_anggaran',$mtposanggaranarr,$rowheader3['id_pos_anggaran'],false,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_pos_anggaran"], "id_pos_anggaran", "Pos Anggaran");
$row['id_rab_detail'] = $id_rab_detail;
$rabdetailarr[$row['id_rab_detail']] = $rowheader3['uraian'];
$from = UI::createSelect('id_rab_detail',$rabdetailarr,$row['id_rab_detail'],false,$class='form-control ',"style='width:auto; max-width:100%;'");
$from .= UI::createTextHidden('id_rab_detail',$row['id_rab_detail'],$edited);
echo UI::createFormGroup($from, $rules["id_rab_detail"], "id_rab_detail", "Uraian RAB");

if(is_array($manpowerarr) && count($manpowerarr)){
	$from = UI::createSelect('id_manpower',$manpowerarr,$row['id_manpower'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
	echo UI::createFormGroup($from, $rules["id_manpower"], "id_manpower", "Man Power");

	$row['nama'] = $manpowerarr[$row['id_manpower']];
}
if($row['id_jasa_material']){
$from = UI::createSelect('id_jasa_material',$rabjasa_materialarr,$row['id_jasa_material'],false,$class='form-control ',"style='width:auto; max-width:100%;'");
$from .= UI::createTextHidden('id_jasa_material',$row['id_jasa_material'],$edited);
echo UI::createFormGroup($from, $rules["id_jasa_material"], "id_jasa_material", "Jasa/Material");

if(!$row['nama'])
	$row['nama'] = $rabjasa_materialarr[$row['id_jasa_material']];
}

if(!$row['nama'] && $rowheader3['sumber_nilai']<>3)
	$row['nama'] = $rabdetailarr[$row['id_rab_detail']];
?>
<?php 
$from = UI::createTextArea('nama',$row['nama'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama");
?>
<?php 
$from = UI::createTextBox('tgl',($row['tgl']===null?date('d-m-Y'):$row['tgl']),'','',$edited,'form-control datepicker',"style='width:100%'");
echo UI::createFormGroup($from, $rules["tgl"], "tgl", "Tgl");
?>
<?php 
$from = UI::createTextBox('nilai_satuan',$row['nilai_satuan'],'','',$edited,$class='form-control rupiah',"style='text-align:right; width:100%' step='any' onchange='goSubmit(\"set_value\")'");
echo UI::createFormGroup($from, $rules["nilai_satuan"], "nilai_satuan", "Nilai");
?>
<?php 
$from = "<div class='col-sm-5 no-padding no-margin'>".
UI::createTextNumber('vol',$row['vol'],'','',$edited,$class='form-control ',"style='text-align:right; width:100%' step='any' onchange='goSubmit(\"set_value\")'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Satuan</label><div class='col-sm-5 no-padding no-margin'>".
UI::createTextBox('satuan',$row['satuan'],'20','20',$edited,$class='form-control ',"style='width:100%'")
."</div>";
echo UI::createFormGroup($from, $rules["satuan"], "satuan", "Vol");
?>
<?php 
$from = UI::createTextBox('nilai',$row['nilai'],'','',$edited,$class='form-control rupiah',"style='text-align:right; width:100%' step='any' readonly");
echo UI::createFormGroup($from, $rules["nilai"], "nilai", "Total Nilai");
?>
<?php 
$from = UI::createTextArea('keterangan',$row['keterangan'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["keterangan"], "keterangan", "Keterangan");
?>

</div>
<div class="col-sm-6">
				



<?php 
$from = UI::createUploadMultiple("file", $row['file'], $page_ctrl, $edited, "file");
echo UI::createFormGroup($from, $rules["file"], "file", "Lampiran");
?>


<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>