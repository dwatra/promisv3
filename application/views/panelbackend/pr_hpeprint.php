<br/>
<table width="100%" border="1">
    <tr>
        <th width="150px" style="border-right: 0px !important;"><b>Proyek</b></th>
        <th width="10px" style="border-right: 0px !important;border-left: 0px !important;">:</th>
        <th style="border-left: 0px !important;"><?=$rowheader['nama_proyek']?></th>
    </tr>
    <tr>
        <th style="border-right: 0px !important;"><b>Nama</b></th>
        <th style="border-right: 0px !important;border-left: 0px !important;">:</th>
        <th style="border-left: 0px !important;"><?=$row['nama']?></th>
    </tr>
    <tr>
        <th style="border-right: 0px !important;"><b>Tgl. Dibutuhkan</b></th>
        <th style="border-right: 0px !important;border-left: 0px !important;">:</th>
        <th style="border-left: 0px !important;"><?=Eng2Ind($row['tgl_dibutuhkan'])?></th>
    </tr>
</table>
<table width="100%" border="1">
<thead>
    <tr>
        <th width="10px">No</th>
        <th style="width: 300px">Nama Pekerjaan/Barang</th>
        <th style="text-align: center;width: 50px;">Jumlah</th>
        <th style="text-align: center;width: 50px;">Satuan</th>
        <th style="text-align: center">Keterangan</th>
    </tr>
    <tr>
        <th class="bluetua">1</th>
        <th class="bluetua">2</th>
        <th class="bluetua">3</th>
        <th class="bluetua">4</th>
        <th class="bluetua">5</th>
    </tr>
</thead>
<tbody>

    <tr>
        <td width="10px"></td>
        <td ><?=$row['nama']?></td>
        <td style="text-align: center"></td>
        <td style="text-align: center"></td>
        <td style="text-align: center"><?=$row['keterangan']?></td>
    </tr>
    <?php
    $no=1;
    $total = 0;
    if(!empty($rows))
    foreach($rows as $r){ 
        ?>
        <tr>
            <td width="10px" align="center"><?=$no++?></td>
            <td>
                <?=$r['uraian']?>
                
                <?php if($r['keterangan']){ ?>
                    <br/>
                    <small><b><?=nl2br($r['keterangan'])?></b></small>
                <?php } ?>
            </td>
            <td style="text-align: center"><?=$r['vol']?></td>
            <td style="text-align: center"><?=$r['satuan']?></td>
            <td style="text-align: center"></td>
        </tr>
    <?php } ?>
</tbody>
</table>

<style type="text/css">
    td{
        padding: 3px !important;
        vertical-align: top !important;
    }
    .bluemuda, th{
        padding: 5px !important;
        background: #d9e1f2 !important;
    }
    .bluetua {
        padding: 5px !important;
        background: #337ab7 !important;
        color: #fff !important;
        text-align: center;
    }
    thead th{
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<br/>