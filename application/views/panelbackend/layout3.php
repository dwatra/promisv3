<?php
if($width)
	$width_page = $width;
?>
<div id="container" class="container">
<?php if(!$this->post[date('Ymd')]){ ?>
<center>
	<div class="notshow">
	<?php if($excel!==false){ ?>
	<a download="<?=$exp=str_replace(array('"',"'"), "", $page_title)?>.xls" class="btn btn-sm btn-primary" href="#" onclick="return ExcellentExport.excel(this, 'datatable', '<?=$exp?>');">
	<span class="glyphicon glyphicon-th"></span> Excel</a>
    &nbsp;
    <?php }?>
	<button type="button" class="btn btn-sm btn-primary" onclick="goprint()">
	<span class="glyphicon glyphicon-print"></span>
	Print
	</button>
	<br/>
	<br/>
	</div>
</center>
<script type="text/javascript">
	function goprint(){
		window.print();
	}
</script>
<?php } ?>

<div id="datatable">
	<?php if(!$no_header){ ?>
	<div class="header">
	<table border="0" style="padding: 0px; margin: 0px;" width="100%">
	<tr>
		<td width="0px" style="padding: 0px !important; margin: 0px !important;"></td>
		<td width="100px" align="left">
		<img src="<?=base_url()?>assets/img/logo.jpg" width="70px">
		</td>
		<td>
		<b>
		<b>
		<h4 style="font-weight: bold;">
		<?=$this->config->item("company_name")?>
		</h4>
		</b>
		<small>
		<?=$this->config->item("company_address")?>
		</small>
		</b>
		</td>
	</tr>
	<tr>
		<td width="0px" style="padding: 0px !important; margin: 0px !important;"></td>
		<td colspan="2">
			<table border="0" width="100%">
				<tr style="border-top:1px solid #555;border-bottom:1px solid #555;">
					<td width="30%" align="left"><small>Telepon : <?=$this->config->item("company_telp")?></small></td>
					<td width="30%" align="center"><small>Faksimile : <?=$this->config->item("company_fax")?></small></td>
					<td width="30%" align="right"><small>Email : <?=$this->config->item("company_email")?></small></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	</div>
	<?php if($page_title){ ?>
	<ins><h5 style="padding:10px;text-align: center;">
	<b><?=$page_title?></b>
		<?php if($sub_page_title){ echo "<br/>".$sub_page_title; } ?>
	</h5></ins>
	<?php } } ?>
<?php echo $content1;?>
</div>
</div>
<?php if(!$this->post[date('Ymd')]){ ?>
<script src="<?php echo base_url()?>assets/js/excellentexport.min.js"></script>
<style>
		.notshow{
			margin-top: 10px;
		}
	@media print{
		.notshow{
			display: none;
		}
		body{
			margin: 0px;
			padding: 10px 5px;
		}
		html{
			margin:0px;
			padding:0px;
		}
	}
#container{
	width: 100%;
    font-size:14px;
    font-family:Arial, Helvetica, sans-serif;
}
td,th{
    padding: 3px;
    font-size: 12px;
    vertical-align:text-center;
}
.h4, .h5, .h6, h4, h5, h6, hr {
    margin-top: 5px;
    margin-bottom: 5px;
}
.table-label th{
	background: #b5ddff;
	text-align: center;
}
.tableku {
    margin-top: 20px;
    width:100%;
    border:1px solid #555;
}
.tableku td{border: 1px solid #555;
    padding: 0px 3px;
	vertical-align: top;
}
.tableku thead th{
	border:1px solid #555;
	border-bottom:2px  solid #555;
	padding:0px 3px;
}
.tableku th{
	border:1px solid #555;
	padding:0px 3px;
}
.tableku thead, .tableku1 thead{
	border:1px solid #555;
	page-break-before: always;
}
hr{
	border-color:#555;
}
.tableku1 {
    margin-top: 10px;
    width:100%;
    border:1px solid #555;
}
.tableku1 td{
    border:1px solid #555;
	padding:3px 5px;   
	vertical-align: top;
}
.tableku1 thead th{
	border:1px solid #555;
	border-bottom:2px  solid #555;
	padding:3px 5px;    
	text-align: center;
}
.tableku1 th{
	border:1px solid #555;
	padding:0px 3px;
	text-align: center;
}

</style>
<?php }else{ ?>
<style type="text/css">
	td{
        padding: 5px !important;
        font-size: 8px !important;
        vertical-align: top !important;
    }
    th{
        padding: 5px !important;
        font-size: 8px !important;
    }
    thead th{
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<?php } ?>