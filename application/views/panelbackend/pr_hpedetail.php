<div class="row">
<div class="col-sm-6">
<?php 
$from = UI::createTextArea('nama',$row['nama'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama Permintaan");
?>
<?php 
$from = UI::createTextBox('tgl_dibutuhkan',$row['tgl_dibutuhkan'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_dibutuhkan"], "tgl_dibutuhkan", "Tgl. Dibutuhkan");
?>
<?php 
$from = "<div class='read_detail'>";

if($row['id_status_hpe']==2)
	$from .= "<span class='label label-info'>Diajukan</span>";
elseif($row['id_status_hpe']==3)
	$from .= "<span class='label label-danger'>Ditolak</span>";
elseif($row['id_status_hpe']==4)
	$from .= "<span class='label label-success'>Selesai</span>";
else
	$from .= "<span class='label label-warning'>Draft</span>";

$from .= "</div>";

echo UI::createFormGroup($from, $rules["id_status_hpe"], "id_status_hpe", "Status");
?>
<?php 
if($row['riwayat']){
	$from = "<div class='read_detail'>";
	$from .= $row['riwayat'];
	$from .= "</div>";
	echo UI::createFormGroup($from, $rules["riwayat"], "riwayat", "Riwayat Pengajuan");
}
?>

</div>
<div class="col-sm-6">
	<?php
if(!$edited && !empty($rows)){
$from = "<a href='".site_url("panelbackend/pr_hpe/go_print/$id_rab/$row[id_hpe]")."' target='_blank' class='btn btn-info'>Print</a> ";

if(!($row['id_status_hpe']==2 or $row['id_status_hpe']==4)){
	$from .= " <a href='javascript::void(0)' onclick='if(confirm(\"Apakah Anda akan mengajukan ke tim HPE ?\")){goSubmit(\"ajukan_hpe\")}' class='btn btn-success'>Ajukan</a>";
}else{
	$from .= " <a href='javascript::void(0)' onclick='goSubmit(\"cek_hpe\")' class='btn btn-success'>Get HPE</a>";
}


echo UI::createFormGroup($from);
}
?>
	</div>
</div>
<hr/>
<?php
if(!empty($rows)){ 
echo "<table class='table table-hover'>";
echo "<thead><tr><th>Uraian</th><th>Jumlah</th><th>Satuan</th><th>Harga Satuan</th><th>Total Harga</th></tr></thead>";
$total = 0;
foreach($rows as $r){ 
	$r['nilai'] = (float)$r['harga_satuan']*(float)$r['vol'];
	$total+=$r['nilai'];
	echo "<tr><td>";
	echo $r['uraian'];
	echo "</td><td style='text-align:right'>";
	echo $r['vol'];
	echo "</td><td>";
	echo $r['satuan'];
	echo "</td><td style='text-align:right'>";
	echo rupiah($r['harga_satuan'],2);
	echo "</td><td style='text-align:right'>";
	echo rupiah($r['nilai'],2);
	echo "</td></tr>";
}

echo "<tr><td>";
echo "</td><td>";
echo "</td><td>";
echo "</td><td style='text-align:right'>Total</td><td style='text-align:right'>";
echo rupiah($total,2);
echo "</td></tr>";

echo "</table>";
}

if($this->access_role['edit'] && !$edited){
	echo UI::createExportImport();
}
?>
<div style="clear: both;"></div>
<div style="text-align: right;">
<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>