
<?php if ($edited) { ?>
<div class="col-sm-6">

<?php 
if(!$row['nama'])
	$row['nama'] = $rowheader1['nama_pekerjaan'];

$from = UI::createTextArea('nama',$row['nama'],'2','',$edited,$class='form-control ');
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama Rencana");
?>


<?php 
if ($edited) {
	echo '<div class="form-group ">
	<label for="nama" class="col-sm-4 control-label">File MPP</label>
	<div class="col-sm-8">
	<input type="file" name="file1">
	</div>
	</div>';

}


$from = "<table width='300px'><thead><tr><th>Mulai</th><th>Selesai</th><th><th></tr></thead>";
$from .= "<tbody id='working_time'>";
foreach($row['working_time'] as $i=>$r){
	$from .= "<tr id='key$i'><td>";
	$from .= UI::createTextBox("working_time[$i][mulai]",$r['mulai'],'2','',$edited,'form-control timepicker');
	$from .= "</td><td>";
	$from .= UI::createTextBox("working_time[$i][selesai]",$r['selesai'],'2','',$edited,'form-control timepicker');
	$from .= "</td><td>";
	$from .= "<button type='button' class='btn btn-xs btn-danger' onclick='$(\"#key$i\").remove()'>x</button>";
	$from .= "</td></tr>";
}
$lasti = $i;
$from .= "</tbody>";
$from .= "</table>";
if($edited){
	$from .= "<button type='button' class='btn btn-xs btn-success' onclick='addWt()'>Add</button>";
}
echo UI::createFormGroup($from, $rules["working_time"], "working_time", "Working Time");

?>

<script type="text/javascript">
	 $(function () {
        timepicker();
    });

	 var i = <?=(int)$lasti?>;
	 function addWt(){
		i = i+1;
		$("#working_time").append(
			'<tr id="key'+i+'">'
				+"<td>"+<?php $frm = UI::createTextBox('working_time[$i][mulai]',null,'2','',true,'form-control timepicker')?> <?=str_replace('$i',"\"+i+\"",json_encode($frm))?> +"</td>"
				+"<td>"+<?php $frm = UI::createTextBox('working_time[$i][selesai]',null,'2','',true,'form-control timepicker')?> <?=str_replace('$i',"\"+i+\"",json_encode($frm))?> +"</td>"
				+'<td>'
					+'<button type="button" class="btn btn-danger btn-xs" onclick="$(\'#key'+i+'\').remove();">x</button>'
				+'</td>'
			+'</tr>'
		);

		timepicker();
	 }

	 function timepicker(){
	 	$('.timepicker').datetimepicker({
            format: 'HH:mm'
        });
	 }
</script>
				

<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>

</div>
<div class="col-sm-6">
	&nbsp;
</div>
<div style="clear: both">
</div>
<br/>

<?php }else { ?>
<div class="row">
	<div class="col-sm-6">
		<?php if($rowheader1['is_selesai']){ ?>
			<h4>Selesai dalam durasi <b><?=round($rowheader1['durasi'])?> Hari</b></h4>
			<?php }else{ ?>
			<h4>
				Progress <b><?=($rowheader1['durasi'])?> </b> dari <b><?=$rowheader1['rencanadurasi']?></b>
			</h4>
			<?php }	?>
			<b>RENCANA</b><br/>
			<div class="progress" title="<?=round($rowheader1['progress_rencana'],2)?> %">
                <div class="progress-bar progress-bar-green" style="width: <?=round($rowheader1['progress_rencana'],2)?>%;"><?=round($rowheader1['progress_rencana'],2)?>%</div>
              </div>
              <br/>
			<b>REALISASI 
             <?php
             if($rowheader1['progress']<$rowheader1['progress_rencana'])
             	echo "<span class='label label-warning'>LAGGING</span>";
             elseif($rowheader1['progress']>$rowheader1['progress_rencana'])
             	echo "<span class='label label-success'>LEADING</span>";
             else
             	echo "<span class='label label-info'>ON SCHEDULE</span>";
             ?></b> <br/>
			<div class="progress" title="<?=round($rowheader1['progress'],2)?> %">
                <div class="progress-bar progress-bar-green" style="width: <?=round($rowheader1['progress'],2)?>%;"><?=round($rowheader1['progress'],2)?>%</div>
              </div>
              <div style="text-align: right; color: #999"><small><i>Terakhir update realisasi : <?=Eng2Ind($rowheader2['last_update_realisasi_wbs'])?></i></small></div>
	</div>
	<div class="col-sm-6" style="text-align: right; padding-top: 35px;">
		<a class="btn btn-success" href="<?php echo site_url("panelbackend/wbs/kurva/{$row['id_pekerjaan']}/{$row['id_plan']}") ?>" target="_blank">
			<i class="glyphicon glyphicon-signal icon-white"></i> Kurva
		</a>
		<a class="btn btn-success" href="<?php echo site_url("panelbackend/wbs/cetak/{$row['id_pekerjaan']}/{$row['id_plan']}") ?>" target="_blank"><i class="glyphicon glyphicon-print icon-white"></i> Print</a></div>
	</div>
<?php } ?>

<div style="clear: both">
<?php 
if ($details) {
	echo '<table class="table table-condensed"  id="fixedcolumns">';
	echo '<tr><th rowspan="2">Item Pekerjaan</th><th rowspan="2" style="width:120px">Mulai</th><th rowspan="2" style="width:120px">Selesai</th><th rowspan="2" style="width:70px">Durasi</th><th colspan="2">Bobot % </th><th rowspan="2" style="width:50px">Progress %</th><th rowspan="2" style="width:120px">Progress terakhir</th><th rowspan="2" style="width:200px">Keterangan</th></tr>';
	echo '<tr><th>Rencana</th><th>Realisasi</th></tr><tbody>';
	$urutan_min = null;
	$urutan_max = null;
	foreach ($details as $detail) {
		if($detail['is_leaf']){
			if(!$urutan_min)
				$urutan_min = $detail['urutan'];

			if($detail['urutan']<$urutan_min)
				$urutan_min = $detail['urutan'];

			if(!$urutan_max)
				$urutan_max = $detail['urutan'];

			if($detail['urutan']>$urutan_max)
				$urutan_max = $detail['urutan'];
		}
	}

	$parents = array();
	$total_bobot_rencana = $total_bobot_realisasi = 0;
	foreach ($details as $detail) {
		if ($detail['urutan'] == 0) continue;
		if ($detail['urutan'] == 1) {
			$total_bobot_rencana = $detail['rencana'];
			$total_bobot_realisasi = $detail['realisasi'];
		}

		$parents[$detail['urutan']] = $detail['urutan_parent'];

		$class_childs = array();
		$j=0;
		$id_temp_parent = $detail['urutan_parent'];
		while ($id_temp_parent) {
			$class_childs[] = $id_temp_parent;
			$id_temp_parent = $parents[$id_temp_parent];
			if (!$id_temp_parent)
				break;

			$j++;
			if ($j>10)
				break;
		}

		$class_childs_temp = array();
		foreach ($class_childs as $id_parent) {
			$class_childs_temp[] = 'child' . $id_parent;
		}

		$class_childs_str = implode(' ', $class_childs_temp); 

		$btn_datetimepicker = '';
		if ($detail['urutan'] == $urutan_min || $detail['urutan'] == $urutan_max) {
			$btn_datetimepicker = '<span class="datetime" id="datetime'. $detail['urutan'] .'" style="float:right"><i class="glyphicon glyphicon-calendar"></i></span><input type="text" id="textdatetime' . $detail['urutan'] .'" style="width: 15px;height: 15px; position: absolute; z-index: 1; top: 2px; right:1px; background: transparent; border: transparent; color: transparent;cursor: pointer;">';
		}

		$durasi = durasi_plan($detail['durasi']);

		$progress_attr = ' style="cursor:not-allowed"';
		if ($detail['is_leaf']) 
			$progress_attr = 'onclick="showProgress('.$detail['urutan'].')" style="cursor:pointer"';

		// if($this->access_role['updaterealisasi']) $progress_attr = '';

		$progress = '<div class="progress lg no-margin" '.$progress_attr.' id="pr'. $detail['urutan'].'" >' . setProgressBar($detail['progress']) . '</div>';


		if($this->access_role['updateketerangan'])
			$keterangan_btn = ' <span class="keterangan btn btn-sm" id="k'. $detail['urutan'].'" style="height:20px;width:30px;float:right">K</span>';

		$keterangan = '<div onclick="showKeterangan('. $detail['urutan'] .')" class="keterangan" id="keterangan' .$detail['urutan']. '" style="background-color:#efefef;width:100%;height:20px;padding:2px"> '. $detail['keterangan'] . '&nbsp;</div>';

		$update_terakhir = '<div style="float:left" id="update_terakhir'.$detail['urutan'].'">'.Eng2Ind($detail['last_update_realisasi_str']) .'</div>';

		
		if ($detail['is_leaf'] == '0') {
			$progress_btn = '';
			$rencana = '';
			if (is_numeric($detail['rencana'])) {
				$rencana_str = @number_format($detail['rencana'], 2);
				/*if ($rencana_str >= '100.00')
					$rencana_str = '100';*/
				$rencana = '<div style="border-bottom:1px dotted #ccc;width:100%">' . $rencana_str . '</div>';
			}

			$realisasi = '';
			if (is_numeric($detail['realisasi'])) {
				$realisasi_str =  @number_format($detail['realisasi'], 2);
				/*if ($realisasi_str >= '100.00') {
					$realisasi_str = '100';
				}*/
				$realisasi = '<div id="rparent'.$detail['urutan'].'" style="border-bottom:1px dotted #ccc;width:100%">' . $realisasi_str . '</div>';
			}
			$bold = ' style="font-weight:bold"';
		}
		else {
			$rencana = @number_format($detail['rencana'], 2);
			$realisasi = @number_format($detail['realisasi'],2);

			$bold = '';
		}
		 
		if ($detail['urutan'] == 0 and $detail['urutan_parent'] == 0) {
			$realisasi = $rencana = '';
		}

		$expand_collapse = '';
		if (!$detail['is_leaf']) {
			$expand_collapse = '<span style="cursor:pointer" onclick="itemClick('.$detail['urutan'].')"><i id="minus'.$detail['urutan'].'" class="glyphicon glyphicon-minus-sign minus-item '.$class_childs_str.'"></i><i id="plus'.$detail['urutan'].'" class="glyphicon glyphicon-plus-sign plus-item '.$class_childs_str.'" style="display:none"></i></span>';
		}


		$tab = $detail['tingkat'] * 30;
		$textprogress = '<input class="textprogress" id="textprogress'.$detail['urutan'].'" type="text" value="'.$detail['progress'].'" size="2" maxlength="3" style="display:none">';

		$textketerangan = '<input class="textketerangan" id="textketerangan'.$detail['urutan'].'" type="text" value="'.$detail['keterangan'].'" maxlength="200" style="display:none;width:100%">';

		list($selesai, $time) = explode(" ",$detail['selesai_str']);
		list($mulai, $time) = explode(" ",$detail['mulai_str']);
		$selesai = strtotime($selesai);
		$mulai = strtotime($mulai);
		$sekarang1 = strtotime($rowheader1['sysdate1']);

		if($detail['progress']<100 && $sekarang1>=$selesai)
			$class_childs_str .= " text-red ";
		elseif($detail['progress']<100 && $sekarang1>=$mulai)
			$class_childs_str .= " text-yellow ";

		$checkbox = '';

		if($this->access_role['updatecheckbox'])
			$checkbox = UI::createCheckBox('is_print['.$detail['urutan'].']',1,$detail['is_print'], null,true,"is_print is_print".$detail['urutan'],"onclick='updatecheckbox(".$detail['urutan'].", this)'");

		echo '<tr' . $bold  . ' class="'. $class_childs_str .'">';
		echo '<td><div style="margin-left:'.$tab.'px">'.$checkbox.' '. $expand_collapse .' '. $detail['nama'] . '</div></td><td align="center">'. Eng2Ind($detail['mulai_str']) .'</td>';
		echo '<td  align="center">'. Eng2Ind($detail['selesai_str']) .'</td><td align="center">'.  $durasi .'</td><td align="center">' .  $rencana .'</td>';
		echo '<td align="center"><span id="r'.$detail['urutan'].'">' .  $realisasi .'</span></td>';
		echo '<td align="center">'.         $progress . $textprogress .'</td>';
		echo '<td style="position:relative">' . $update_terakhir . $btn_datetimepicker . '</td>';
		echo '<td>' . $keterangan . $textketerangan . '</td>';
		echo '</tr>' . "\r\n";
	}
		echo '<tr><td><h4>TOTAL</h4></td><td></td><td></td><td align="center"></td><td align="center"><h4>'.  @number_format($total_bobot_rencana) .'%</h4></td><td align="center"><h4>'. @number_format($total_bobot_realisasi, 2) .'%</h4></td></tr>' . "\r\n";

	echo '</tbody></table>';
}
	
?>
<!--
<link href="<?=base_url()?>assets/css/fixedColumns.dataTables.min.css" rel="stylesheet">
<script src="<?=site_url("assets/js/jquery.dataTables.min.js")?>"></script>
<script src="<?=site_url("assets/js/dataTables.fixedColumns.min.js")?>"></script>



<script type="text/javascript">
/*$(document).ready(function() {
var table = $('#fixedcolumns').DataTable( {
    scrollY:        false,
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    ordering: false,
    searching:false,
    info:false,
    fixedColumns:   {
        leftColumns: 3,
    }
} );
} );*/
</script>
-->

<style type="text/css">
.table td, .table th {
    font-size: 11px;
    padding: 3px !important;
}
table.dataTable {
    clear: both;
    margin-bottom: 0px !important;
    max-width: none !important;
}

</style>

<?php if($edited){ ?>
	<script type="text/javascript">
		$('#tgl_mulai_pelaksanaan').on('dp.change', function(e){
			datediffpelaksanaan();
		});

		$('#tgl_selesai_pelaksanaan').on('dp.change', function(e){
			datediffpelaksanaan();
		});

		$(function(){
			datediffpelaksanaan();
		})

		function datediffpelaksanaan(){
			var start = new Date(Eng2Ind($("#tgl_mulai_pelaksanaan").val())),
		    end   = new Date(Eng2Ind($("#tgl_selesai_pelaksanaan").val())),
		    diff  = new Date(end - start),
		    days  = diff/1000/60/60/24;
		    $("#h").val(days+1);
		}
	</script>
<?php }  ?>

	<script type="text/javascript">
		var urutan = 0;

		function itemClick(id) {
			if ($('#plus' + id).is(":hidden")) {
				// ketika expand
				$('#plus'+id).show();
				$('#minus'+id).hide();
				$("tr.child" + id).hide();	
			}
			else {
				$('#plus'+id).hide();
				$('#minus'+id).show();
				$("tr.child" + id).show();	

				$('i.plus-item.child'+id).hide();
				$('i.minus-item.child'+id).show();
			}
			
		}

		function showProgress(id) {
			setProgressNormalView();

			$("#textprogress" + id).show();
			$("#textprogress" + id).focus();
			$("#pr"+id).hide();
		}

		function showKeterangan(id) {
			setKeteranganNormalView();

			$("#textketerangan" + id).show();
			$("#textketerangan" + id).focus();
			$("#keterangan"+id).hide();
		}

		function setProgressNormalView() {
			$('.textprogress:visible').each(function() {
				$(this).hide();
			});
			$('.progress:hidden').each(function() {
				$(this).show();
			});			
		}

		function setKeteranganNormalView() {
			$('.textketerangan:visible').each(function() {
				$(this).hide();
			});
			$('.keterangan:hidden').each(function() {
				$(this).show();
			});			
		}

		function saveRealisasi(urutan, progress) {
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/updaterealisasi') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan, val: progress },
				success: function(result){
					if (result != '-1') {
						var ret = JSON.parse(result);
						
						$("#r" + urutan).html(ret.realisasi);
						$("#pr" + urutan).html(ret.progress_pr);
						$("#phidden" + urutan).html(ret.progress);
						$("#update_terakhir" + urutan).html(ret.update_terakhir);

						getparentrealisasi();
						getparentprogress();
						getparentlastupdate();
					}
			    	setProgressNormalView();
			  	}
			});
  		}

  		function updatecheckbox(urutan, checkbox){
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/updatecheckbox') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan, is_print: $(checkbox).is(":checked") },
				success: function(result){
					if (result != '-1') {
						$('.is_print').removeAttr('checked');

						var ret = JSON.parse(result);

						$.each(ret, function( index, value ) {
						  $('.is_print'+value).prop('checked', true);

						});
					}
			    	// setProgressNormalView();
			  	}
			});
  		}

  		function setDateTimeManual(urutan, datetime) {
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/setdatetimemanual') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan, val: datetime },
				success: function(result){
					if (result != '-1') {
						var ret = JSON.parse(result);
						$("#update_terakhir" + urutan).html(ret.update_terakhir);
					}
			    	// setProgressNormalView();
			  	}
			});
  		}

		function saveKeterangan(urutan, obj) {
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/updateketerangan') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan, ket: $(obj).val() },
				success: function(result){
					if (result != '-1') {
						// var ret = JSON.parse(result);
						$("#keterangan" + urutan).html($(obj).val());
					}
					setKeteranganNormalView();
			  	}
			});
  		}

  		function getparentrealisasi() {
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/getparentrealisasi') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan },
				success: function(result){
					if (result != '-1') {
						var ret = JSON.parse(result);

						$.each(ret, function (index, value) {
					        $("#" + index).html(value);
					    });
					}
			  	}
			});
  		}


  		function getparentprogress() {
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/getparentprogress') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan },
				success: function(result){
					if (result != '-1') {
						var ret = JSON.parse(result);

						$.each(ret, function (index, value) {
					        $("#" + index).html(value);
					    });
					}
			  	}
			});
  		}

  		function getparentlastupdate() {
			$.ajax({
				method: "POST",
				url: "<?php echo site_url('panelbackend/wbs/getparentlastupdate') ?>", 
				data: { id: "<?php echo $row['id_plan'] ?>", id_detail: urutan },
				success: function(result){
					if (result != '-1') {
						var ret = JSON.parse(result);

						$.each(ret, function (index, value) {
					        $("#" + index).html(value);
					    });
					}
			  	}
			});
  		}

  		<?php if(!$edited){ ?>
		$(document).keyup(function(e) {
			$('form').each(function(){
			    $(this).submit(function(e){
			        e.preventDefault();
			        return  false;
			    })
			}) 

		    if (e.key === "Escape") {
		        setProgressNormalView();
		    }
		    else if (e.key === "Enter") {
				$('.textprogress:visible').each(function() {
					urutan = $(this).attr('id').replace('textprogress', '');
					progress = $(this).val();
					saveRealisasi(urutan, progress);
				});

				$('.textketerangan:visible').each(function() {
					urutan = $(this).attr('id').replace('textketerangan', '');
					saveKeterangan(urutan, $(this));
				});

		    }
		});
	<?php } ?>

	$(function(){
		$('#textdatetime<?= $urutan_min ?>').datetimepicker({format: "DD-MM-YYYY HH:mm:ss",useCurrent:false});
		$('#textdatetime<?= $urutan_max ?>').datetimepicker({format: "DD-MM-YYYY HH:mm:ss",useCurrent:false});

		$('#datetime<?= $urutan_min ?>').click(function(){
			$('#textdatetime<?= $urutan_min ?>').datetimepicker('show');
			$('#textdatetime<?= $urutan_min ?>').trigger('click');
		});

		$('#datetime<?= $urutan_max ?>').click(function(){
			$('#textdatetime<?= $urutan_max ?>').datetimepicker('show');
		});

		$('#textdatetime<?= $urutan_min ?>').on("dp.change", function(selectedDate) {
		        setDateTimeManual('2', $('#textdatetime<?= $urutan_min ?>').val());
	    	}
		);

		$('#textdatetime<?= $urutan_max ?>').on("dp.change", function(selectedDate) {
		        setDateTimeManual('<?= $urutan_max ?>', $('#textdatetime<?= $urutan_max ?>').val());
	    	}		
		);
	})

	</script>	