<div style="position: relative;">
<a style="position: absolute; right: 0px;" href="<?=site_url('panelbackend/rab_manpower/index/'.$id_rab)?>" class="btn btn-sm"><span class="
glyphicon glyphicon-chevron-left"></span> BACK</a>
</div>
<br/>
<br/>
<div class="table-responsive"><canvas id="omega" style="height:25vw; width:100%"></canvas>
</div>


<?php
$labels = array();
for($i=$rowheader1['hmin']; $i>0; $i--){
    $labels[] = 'H-'.$i;
}
?>

<?php
for($i=1; $i<=$rowheader1['h']; $i++){
    $labels[] = $i;
}
?>

<?php
for($i=1; $i<=$rowheader1['hplus']; $i++){
    $labels[] = 'H+'.$i;
}
?>


<?php 
$jj=array();
$jjr=array();
$tot_max = array();
$tot_jum = array();
$jabatanarr = array();
foreach($rows as $row){ 
    $jum = 0;
    $max = 0;
    for($i=$rowheader1['hmin']; $i>0; $i--){
        $j = $rowdays[$row['id_manpower']]['hmin'.$i];
        $jj['hmin'.$i]+=(int)$j;

        $j = $rowdaysrealisasi[$row['id_manpower']]['hmin'.$i];
        $jjr['hmin'.$i]+=(int)$j;
    }
    for($i=1; $i<=$rowheader1['h']; $i++){
        $j = $rowdays[$row['id_manpower']]['h'.$i];
        $jj['h'.$i]+=(int)$j;

        $j = $rowdaysrealisasi[$row['id_manpower']]['h'.$i];
        $jjr['h'.$i]+=(int)$j;
    }
    for($i=1; $i<=$rowheader1['hplus']; $i++){
        $j = $rowdays[$row['id_manpower']]['hplus'.$i];
        $jj['hplus'.$i]+=(int)$j;

        $j = $rowdaysrealisasi[$row['id_manpower']]['hplus'.$i];
        $jjr['hplus'.$i]+=(int)$j;
    }
} ?>

<?php
$jum = 0;
$max = 0;
$datas = array();
$datasr = array();
for($i=$rowheader1['hmin']; $i>0; $i--){
    $j = $jj['hmin'.$i];
    $datas[] = $j;

    $j = $jjr['hmin'.$i];
    $datasr[] = $j;
}
?>

<?php
for($i=1; $i<=$rowheader1['h']; $i++){
    $j = $jj['h'.$i];
    $datas[] = $j;

    $j = $jjr['h'.$i];
    $datasr[] = $j;
}
?>

<?php
for($i=1; $i<=$rowheader1['hplus']; $i++){
    $j = $jj['hplus'.$i];
    $datas[] = $j;

    $j = $jjr['hplus'.$i];
    $datasr[] = $j;
}
?>

<script src="<?=site_url("assets/template/backend/plugins/chartjs/Chart.bundle.js")?>"></script>
<script src="<?=site_url("assets/template/backend/plugins/chartjs/utils.js")?>"></script>
<script type="text/javascript">
	
 window.chartColors = {
      red: 'rgb(165, 19, 1)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(5, 25, 110)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };

    window.onload = function() {
        var ctx = document.getElementById('omega').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: {
                satuan:'',
                labels: <?php echo json_encode($labels); ?>,
                datasets: [
                {
                    label: 'Rencana',
                    lineTension: 0,
                    fill: false,
                    data:<?php echo json_encode($datas); ?>,
                    borderColor: window.chartColors.blue,
                },
                {
                    label: 'Realisasi',
                    lineTension: 0,
                    fill: false,
                    data:<?php echo json_encode($datasr); ?>,
                    borderColor: window.chartColors.red,
                }
                ],
            },
            options: {
                bezierCurve : false,
                legend: {
                    display: true
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return value.toString();
                            }
                        },
                        gridLines: {
                            display:false
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]+data.satuan;
                            return label;
                        }
                    }
                }
            }
        });
    }
</script>