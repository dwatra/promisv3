<style>
body {
    margin:10px;
    padding:0;
    line-height: 1.4em;
    word-spacing:1px;
    letter-spacing:0.2px;
    font: 10pt Arial, Helvetica,"Lucida Grande",serif;
    color: #000 !important;
}

h1, h2, h3 {
	margin:0px
}

table.data_grid {
	background: white;
	border-collapse: collapse;
}

table.data_grid th, table.data_grid td {
	font-size: 11pt;
	border: 1px #000 solid;
    padding: 0.2em 0.3em;
}

td,th{
    padding: 3px;
    font-size: 11pt;
    vertical-align:text-center;
}


table.data_grid th {
	text-align: center;vertical-align:middle;
}

table.data_grid td {
	text-align:left;
	vertical-align:top;
}

@media screen {
    .pagebreak  { height:10px; background:url(img/page-break.gif) 0 center repeat-x; border-top:1px dotted #999; margin-bottom:13px; }
}
@media print {
    .pagebreak { height:0; page-break-before:always; margin:0; border-top:none; }
}

canvas{
  height:<?php echo $height_kurva ?>px !important;
}

</style>
<?php

$total_rencana = $total_realisasi = 0;
foreach($details as $detail){
	if ($detail['tingkat'] == 0) {
		$total_rencana += $detail['rencana'];
		$total_realisasi += $detail['realisasi'];
	}
}
?>
<table class="data_grid" style="margin:auto; width: 100%; width: 100%">
	<tr>
		<td rowspan="4" style="width:150px;text-align:center;vertical-align:middle"><img src="<?=base_url()?>assets/img/logo.jpg" style="height:72px"></td>
		<td style="text-align: center">PT PEMBANGKITAN JAWA BALI SERVICES</td>
		<td rowspan="4" style="width:150px;text-align:center;vertical-align:middle"><img src="<?=site_url("panelbackend/rab_proyek/logo_customer/{$rowheader['id_customer']}") ?>" style="max-width: 150px;max-height: 72px;"></td>
	</tr>
	<tr><td style="text-align: center"><?=strtoupper($row['nama'])?></td></tr>
	<tr><td style="text-align: center"><?=strtoupper($rowheader['pemberi_pekerjaan'])?></td></tr>
	<tr><td style="text-align: center">
		<?php
		list($tgl,$bln,$thn) = explode("-",$rowheader1['tgl_pekerjaan']); 

		if(!$thn)
			list($tgl,$bln,$thn) = explode("-",$rowheader1['tgl_mulai_pelaksanaan']); 

		list($thn, $time) = explode(" ", $thn);
		echo 'TAHUN '.$thn;
		?>
	</td></tr>
</table>
<br>
<table class="data_grid" style="margin:auto; width: 100%">
	<tr>
		<td colspan="3" style="text-align:center;background-color:#ccc !important">LAPORAN PROGRES PEKERJAAN</td>
	</tr>
	<tr><td style="width:150px">TANGGAL</td><td><?=Eng2Ind($rowheader1['sysdate1'],false)?></td></tr>
	<tr>
		<td style="width:150px">
			<?php if($rowheader1['is_selesai'] && round($total_realisasi,2)>99.9){ ?>
				SELESAI </td><td>DALAM DURASI <b><?=round($rowheader1['durasi'])?> Hari</b>
			<?php }else{ ?>
				HARI KE</td><td><?=$rowheader1['durasi']?> dari <?=$rowheader1['rencanadurasi']?>
			<?php } ?>
		</td>
	</tr>
</table>
<table class="data_grid" style="margin:auto; width: 100%">
	<tr>
		<td rowspan="2" style="min-width:300px;text-align: center;vertical-align:middle;vertical-align: middle;background-color:yellow !important;border-top:0">ITEM PEKERJAAN</td>
		<td colspan="2" style="text-align: center;vertical-align:middle;background-color:yellow !important;border-top:0">BOBOT</td>
		<td colspan="2" style="text-align: center;vertical-align:middle;background-color:yellow !important;border-top:0">KETERANGAN</td>
		<td rowspan="2" style="text-align: center;vertical-align:middle;vertical-align: middle;background-color:yellow !important;border-top:0;width: 589px">KURVA S</td>
	</tr>
	<tr><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:60px;">RENCANA</td><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:60px;">REALISASI</td><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:90px;">STATUS</td><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:130px;">DATE</td></tr>
<?php 

$total_rencana = $total_realisasi = 0;


$i=0;
foreach ($details as $detail) { 
	if (!$detail['is_print'] or $detail['tingkat']==0) continue;
	$margin = '';
	$detail['tingkat']--;
	$mrg = ($detail['tingkat']*20);
	if($mrg)
		$margin = 'margin-left:'.$mrg.'px';

	$status = 'Belum Mulai';
	$tgl_realisasi = '';
	if (round($detail['realisasi'],2) > 0) {
		$status = '<span style="color:#ff9600 !important">Progress</span>';
		if (round($detail['rencana'],2) == round($detail['realisasi'],2)) {
			$status = '<span style="color:#0073b7 !important">Closed</span>';
			
		}
		$tgl_realisasi = Eng2Ind($detail['last_update_realisasi_str']);
	}

	if ($detail['tingkat'] == 0) {
		$total_rencana += $detail['rencana'];
		$total_realisasi += $detail['realisasi'];
	}
?>
	<tr>
		<td><div style="<?php echo $margin ?>"><?php echo $detail['nama'] ?></div></td>
		<td style="text-align: center;vertical-align:middle;"><?php echo @number_format($detail['rencana'],2) ?>%</td>
		<td style="text-align: center;vertical-align:middle;"><?php echo @number_format($detail['realisasi'],2) ?>%</td>
		<td style="text-align: center;vertical-align:middle;font-size:9pt"><?php echo $status ?></td>
		<td style="text-align: center;vertical-align:middle;font-size:9pt"><?php echo $tgl_realisasi ?></td>
		<?php if (!$i) { ?>
		<td rowspan="<?php echo $num_item_show ?>">

	<script src="<?=site_url("assets/template/backend/plugins/chartjs/Chart.bundle.js")?>"></script>
	<script src="<?=site_url("assets/template/backend/plugins/chartjs/utils.js")?>"></script>

	<div>
		<canvas id="canvas" style="width:100%;height:100%"></canvas>
	</div>
	<!-- <img src="<?=site_url($image)?>">
	<?php //unlink($image)?> -->
	<script>
		var timeFormat = 'MM/DD/YYYY HH:mm';

 window.chartColors = {
      red: 'rgb(165, 19, 1)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(5, 25, 110)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };

		var color = Chart.helpers.color;
		var config = {
			type: 'line',
			data: {
				labels: [
					<?php for ($i=0;$i<=$times['total_hari']; $i++) { ?>
						'<?php echo $i ?>',
					<?php } ?>
				],
				datasets: [
				{
					label: 'Rencana',
					backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
					borderColor: window.chartColors.blue,
					fill: false,
					lineTension: 0,
					data: [
					<?php 
					foreach ($plan_per_day as $progress) { 
						echo round($progress,2) . ',' ;
					} ?>
					],
				}, 
				{
					label: 'Realisasi',
					backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
					borderColor: window.chartColors.red,
					fill: false,
					lineTension: 0,
					data: [
					<?php 
					foreach ($realisasi_per_day as $progress) { 
						echo round($progress,2) . ',' ;
					} ?>
					],
				}, 
				]
			},
			options: {
				legend: {
                    display: true
                },
				responsive: true,
				title: {
					display: true,
					text: 'KURVA S'
				},
				scales: {
					xAxes: [{
						display: true,
						barThickness: 1,
						gridLines: { lineWidth: 1 },
						scaleLabel: {
							display: true,
							labelString: 'Hari Ke'
						},
						ticks: {
							major: {
								fontColor: '#FF0000'
							}
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Progress %'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		
	</script>
		</td>
		<?php } ?>
	</tr>
<?php 
	$i++;
	} 
?>
	<tr><td style="text-align: center">TOTAL</td><td style="text-align: center"><?php echo number_format($total_rencana, 2) ?>%</td><td style="text-align: center"><?php echo number_format($total_realisasi, 2) ?>%</td><td colspan="3">&nbsp;</td></tr>

</table>
<br>
<table style="margin:auto; width: 100%">
	<?php $persen_width = 25;
	if($rowheader['is_pln'])
		$persen_width = 20;
	?>
	<tr>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center">
			<table style="100%" class="data_grid">
				<tr>
					<td colspan="2" style="background-color:yellow !important">INFORMASI PROGRESS</td>
				</tr>
				<tr><td style="width:60%">RENCANA</td><td style="width:40%;text-align:center;"><?= number_format($total_rencana, 2) ?></td></tr>
				<tr><td>REALISASI</td><td style="text-align:center"><?= number_format($total_realisasi, 2) ?></td></tr>
				<?php $total_status = $total_realisasi - $total_rencana; ?>
				<tr><td><?=($total_status>0?'<span style="color:#2b982b !important">LEADING</span>':($total_realisasi==$total_rencana?'<span style="color:#0073b7 !important">ON&nbsp;SCHEDULE</span>':'<span style="color:#d33724 !important">LAGGING</span>'));?></td><td style="text-align:center"><?=number_format(abs($total_status),2)?></td></tr>
			</table>
			<br><br>
		</td>
		<?php if($rowheader['is_pln']){ ?>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center">
			MENGETAHUI<br>
			<?=strtoupper($rowheader['jabatan_pic_pln'])?><br>
			PLN PERSERO			
		</td>
		<?php } ?>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center">
			MENGETAHUI<br>
			<?=strtoupper($rowheader['jabatan_pic_customer'])?><br>
			<?=strtoupper($rowheader['pemberi_pekerjaan'])?>			
		</td>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center"><br>
			MANAJER PROYEK<br>
			PT PJB SERVICES			
		</td>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center"><br>
			RENDAL PROYEK<br>
			PT PJB SERVICES			
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<?php if($rowheader['is_pln']){ ?>
		<td style="text-align:center;">(<?=strtoupper($rowheader['pic_pln'])?>)</td>
		<?php } ?>
		<td style="text-align:center;">(<?=strtoupper($rowheader['pic_customer'])?>)</td>
		<td style="text-align:center;">(<?=strtoupper($rowheader['nama_pic'])?>)</td>
		<td style="text-align:center;">(<?=strtoupper($rowheader['nama_rendal_proyek'])?>)</td>
	</tr>
</table>

<br>
<div class="pagebreak"> </div>
<br/>
<table class="data_grid" style="margin:auto; width: 100%">
	<tr>
		<td rowspan="4" style="width:150px;text-align:center;vertical-align:middle"><img src="<?=base_url()?>assets/img/logo.jpg" style="height:72px"></td>
		<td style="text-align: center">PT PEMBANGKITAN JAWA BALI SERVICES</td>
		<td rowspan="4" style="width:150px;text-align:center;vertical-align:middle"><img src="<?=site_url("panelbackend/rab_proyek/logo_customer/{$rowheader['id_customer']}") ?>" style="max-width: 150px;max-height: 72px;"></td>
	</tr>
	<tr><td style="text-align: center"><?=strtoupper($row['nama'])?></td></tr>
	<tr><td style="text-align: center"><?=strtoupper($rowheader['pemberi_pekerjaan'])?></td></tr>
	<tr><td style="text-align: center">
		<?php
		list($tgl,$bln,$thn) = explode("-",$rowheader1['tgl_pekerjaan']); 

		if(!$thn)
			list($tgl,$bln,$thn) = explode("-",$rowheader1['tgl_mulai_pelaksanaan']); 

		list($thn, $time) = explode(" ", $thn);

		echo 'TAHUN '.$thn;
		?>
	</td></tr>
</table><br>
<table class="data_grid" style="margin:auto; width: 100%">
	<tr>
		<td colspan="3" style="text-align:center;background-color:#ccc !important">LAPORAN PROGRES PEKERJAAN</td>
	</tr>
	<tr><td style="width:150px">TANGGAL</td><td><?=Eng2Ind($rowheader1['sysdate1'],false)?></td></tr>
	<tr>
		<td style="width:150px">

			<?php if($rowheader1['is_selesai'] && round($total_realisasi,2)>99.9){ ?>
				SELESAI </td><td> DALAM DURASI <b><?=round($rowheader1['durasi'])?> Hari</b>
			<?php }else{ ?>
				HARI KE</td><td><?=$rowheader1['durasi']?> dari <?=$rowheader1['rencanadurasi']?>
			<?php } ?>

		</td>
	</tr>
</table>
<table class="data_grid" style="margin:auto; width: 100%">
	<tr>
		<td rowspan="2" style="width:300px;text-align: center;vertical-align:middle;vertical-align: middle;background-color:yellow !important;border-top:0">ITEM PEKERJAAN</td>
		<td colspan="2" style="text-align: center;vertical-align:middle;background-color:yellow !important;border-top:0">PROGRESS</td>
		<td colspan="2" style="text-align: center;vertical-align:middle;background-color:yellow !important;border-top:0">KETERANGAN</td>
		<td rowspan="2" style="width:350px;text-align: center;vertical-align:middle;vertical-align: middle;background-color:yellow;border-top:0">CATATAN LEADING DAN LAGGING PEKERJAAN</td>
	</tr>
	<tr><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:40px;">RENCANA</td><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:40px;">REALISASI</td><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:40px;">STATUS</td><td style="text-align: center;vertical-align:middle;background-color:yellow !important;width:40px;">GAP</td></tr>
<?php 
foreach ($details as $detail) { 
	if (!$detail['is_print'] or $detail['tingkat']==0) continue;
	$margin = '';
	$detail['tingkat']--;
	$mrg = ($detail['tingkat']*20);
	if($mrg)
		$margin = 'margin-left:'.$mrg.'px';

	if($detail['rencana_target']){
		$detail['realisasi'] = $detail['realisasi']/$detail['rencana_target']*100;
		$detail['rencana'] = $detail['rencana']/$detail['rencana_target']*100;
	}else{
		$detail['realisasi'] = 0;
		$detail['rencana'] = 0;
	}

	$detail['realisasi'] = round($detail['realisasi'],2);
	$detail['rencana'] = round($detail['rencana'],2);

	if($detail['realisasi']>100)
		$detail['realisasi'] = 100;

	if($detail['rencana']>100)
		$detail['rencana'] = 100;

	$gap = $detail['realisasi'] - $detail['rencana'];
	$status = '<span style="color:#0073b7 !important">On Schedule</span>';

	if($gap<0)
		$status = '<span style="color:#d33724 !important">Lagging</span>';
	elseif($gap>0)
		$status = '<span style="color:#2b982b !important">Leading</span>';

?>
	<tr>
		<td><div style="<?php echo $margin ?>"><?php echo $detail['nama'] ?></div></td>
		<td style="text-align: center;vertical-align:middle;"><?php echo @number_format($detail['rencana'],2) ?>%</td>
		<td style="text-align: center;vertical-align:middle;"><?php echo @number_format($detail['realisasi'],2) ?>%</td>
		<td style="text-align: center;vertical-align:middle;font-size: 9pt"><?php echo $status ?></td>
		<td style="text-align: center;vertical-align:middle;"><?php echo number_format($gap, 2) ?>%</td>
		<td><?php echo nl2br($detail['keterangan'])?></td>
	</tr>
<?php } ?>
</table>
<br>
<table style="margin:auto; width: 100%">
	<?php $persen_width = 25;
	if($rowheader['is_pln'])
		$persen_width = 20;
	?>
	<tr>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center">
			&nbsp;<br><br><br><br><br><br><br><br>
		</td>
		<?php if($rowheader['is_pln']){ ?>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center">
			MENGETAHUI<br>
			<?=strtoupper($rowheader['jabatan_pic_pln'])?><br>
			PLN PERSERO			
		</td>
		<?php } ?>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center">
			MENGETAHUI<br>
			<?=strtoupper($rowheader['jabatan_pic_customer'])?><br>
			<?=strtoupper($rowheader['pemberi_pekerjaan'])?>			
		</td>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center"><br>
			MANAJER PROYEK<br>
			PT PJB SERVICES			
		</td>
		<td style="width:<?=$persen_width?>%;vertical-align:top;text-align:center"><br>
			RENDAL PROYEK<br>
			PT PJB SERVICES			
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<?php if($rowheader['is_pln']){ ?>
		<td style="text-align:center;">(<?=strtoupper($rowheader['pic_pln'])?>)</td>
		<?php } ?>
		<td style="text-align:center;">(<?=strtoupper($rowheader['pic_customer'])?>)</td>
		<td style="text-align:center;">(<?=strtoupper($rowheader['nama_pic'])?>)</td>
		<td style="text-align:center;">(<?=strtoupper($rowheader['nama_rendal_proyek'])?>)</td>
	</tr>
</table>