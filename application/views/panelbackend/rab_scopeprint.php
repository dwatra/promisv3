
<table class="tableku">
    <thead>
        <tr>
            <th>Equipment</th>
            <th>Sub Equipment</th>
            <th>Detail SOW</th>
            <th>Jasa/Material</th>
            <th>PIC</th>
            <th>Term Of Condition</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($list['rows'] as $row){ ?>
        <tr>
            <td><?=$row['equipment'];?></td>
            <td><?=$row['sub_equipment']?></td>
            <td><?=$row['detail_sow']?></td>
            <td>
                <?php 
                $temp = array();
                if($rowsjasamaterial[$row['id_scope']]){
                    foreach($rowsjasamaterial[$row['id_scope']] as $k=>$v){
                        $temp[] = $v;
                    }

                    echo implode("<br/>", $temp);
                }
                ?>
            </td>
            <td><?=$row['pic']?></td>
            <td><?=$row['term_of_condition']?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>