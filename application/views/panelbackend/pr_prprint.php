<br/>
<table width="100%" border="1">
    <tr>
        <th width="150px" style="border-right: 0px !important;"><b>NO PRK</b></th>
        <th width="10px" style="border-right: 0px !important;border-left: 0px !important;">:</th>
        <th style="border-left: 0px !important;"><?=$row['no_prk']?></th>
    </tr>
    <tr>
        <th style="border-right: 0px !important;"><b>NO PR</b></th>
        <th style="border-right: 0px !important;border-left: 0px !important;">:</th>
        <th style="border-left: 0px !important;"><?=$row['no_desc']?></th>
    </tr>
    <tr>
        <th style="border-right: 0px !important;"><b>PROJECT OWNER</b></th>
        <th style="border-right: 0px !important;border-left: 0px !important;">:</th>
        <th style="border-left: 0px !important;"><?=$rowheader['nama_pic']?></th>
    </tr>
</table>
<table width="100%" border="1">
<thead>
    <tr>
        <th width="10px">No</th>
        <th style="width: 300px;">Nama Pekerjaan/Barang</th>
        <th style="text-align: center">Jumlah</th>
        <th style="text-align: center">Satuan</th>
        <th style="text-align: center">Harga Satuan</th>
        <th style="text-align: center">Harga Total (Anggaran)</th>
        <th style="text-align: center">Tgl. Dibutuhkan</th>
        <th style="text-align: center">Nama Anggaran</th>
        <th style="text-align: center">Keterangan</th>
    </tr>
    <tr>
        <th class="bluetua">1</th>
        <th class="bluetua">2</th>
        <th class="bluetua">3</th>
        <th class="bluetua">4</th>
        <th class="bluetua">5</th>
        <th class="bluetua">6</th>
        <th class="bluetua">7</th>
        <th class="bluetua">8</th>
        <th class="bluetua">9</th>
    </tr>
</thead>
<tbody>

    <tr>
        <td width="10px"></td>
        <td style="width: 300px;"><?=$row['nama']?></td>
        <td style="text-align: center"></td>
        <td style="text-align: center"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
        <td style="text-align: center"><?=Eng2Ind($row['tgl_dibutuhkan'])?></td>
        <td style="text-align: center"><?=$rowheader['nama_proyek']?></td>
        <td style="text-align: center"><?=$row['keterangan']?></td>
    </tr>
    <?php
    $no=1;
    $total = 0;
    if(!empty($rowsdetail))
    foreach($rowsdetail as $r){ 
        $r['harga_satuan'] = ((float)$r['harga_satuan'])*1.1;
        ?>
        <tr>
            <td width="10px" align="center"><?=$no++?></td>
            <td style="width: 300px;"><?=$r['uraian']?></td>
            <td style="text-align: center"><?=$r['vol']?></td>
            <td style="text-align: center"><?=$r['satuan']?></td>
            <td style="text-align: right"><?=rupiah($r['harga_satuan'])?></td>
            <td style="text-align: right">
            <?php 
            echo rupiah((float)$r['harga_satuan']*(float)$r['vol']); 
            $total+=(float)$r['harga_satuan']*(float)$r['vol'];
            ?>
            </td>
            <td style="text-align: center"></td>
            <td style="text-align: center"></td>
            <td style="text-align: center"></td>
        </tr>
    <?php } ?>
    <tr>
        <td width="10px"></td>
        <td style="width: 300px; text-align: center;"><b>Total</b></td>
        <td style="text-align: center"></td>
        <td style="text-align: center"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right">
        <?php 
        echo rupiah($total); 
        ?>
        </td>
        <td style="text-align: center"></td>
        <td style="text-align: center"></td>
        <td style="text-align: center"></td>
    </tr>
    <tr>
        <td colspan="9">
            <table width="100%">
                <tr>
                    <td width="300px" align="center" valign="top">
                        <?php if($row['nama_mengetahui'] && $total>1000000000){ ?><br/>
                        Mengetahui,<br/>
                        <?=$row['jabatan_mengetahui']?><br/><br/><br/><br/><br/><br/>
                        <?=$row['nama_mengetahui']?>
                    <?php } ?>
                    </td> 
                    <td width="300px" align="center" valign="top"><br/>
                        Menyetujui,<br/>
                        <?=$row['jabatan_menyetujui']?><br/><br/><br/><br/><br/><br/>
                        <?=$row['nama_menyetujui']?>
                    </td> 
                    <td  valign="top" align="center">
                        Sidoarjo,<?=Eng2Ind($row['tgl_permintaan'])?><br/>
                        Pemohon,<br/>
                        <?=$row['jabatan_pemohon']?><br/><br/><br/><br/><br/><br/>
                        <?=$row['nama_pemohon']?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="9" class="bluemuda">DISPOSISI : </td>
    </tr>
</tbody>
</table>

<style type="text/css">
    td{
        padding: 3px !important;
        vertical-align: top !important;
    }
    .bluemuda, th{
        padding: 5px !important;
        background: #d9e1f2 !important;
    }
    .bluetua {
        padding: 5px !important;
        background: #337ab7 !important;
        color: #fff !important;
        text-align: center;
    }
    thead th{
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<br/>