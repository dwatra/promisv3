<section class="content" <?=($width?"style='max-width:$width'":"")?>>
    <div class="col-sm-12 no-padding">
      <?=$this->auth->GetTabRab($rowheader['id_proyek'], $rowheader1['id_pekerjaan'], $id_rab, $rowheader2['is_final']);?>
    </div>
    <div class="col-sm-12 no-padding">
  <div class="box box-default">
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="box-header with-border">
            <?php  if(($_SESSION[SESSION_APP]['loginas'])){ ?>
            <div class="alert alert-warning">
                Anda sedang mengakses user lain. <a href="<?=base_url("panelbackend/home/loginasback")?>" class="alert-link">Kembali</a>.
            </div>
            <?php }?>

            <?php FlashMsg()?>
            <?php if(!$no_header){ ?>
            <div class="pull-left" style="max-width: <?=$width-90?>px">
              <h4 style="margin: 10px 0px 0px 0px !important; display: inline;">
                <?=$rowheader['nama_proyek']?>
              </h4>
                <?php 
                
                if(is_array($rowheader2) && count($rowheader2) && $this->ctrl=='rab_rab_detail'){
                ?>
                <?php if(!$rowheader2['status']){ ?>
                  <label class="label label-default">draft</label>
                <?php } ?>
                <?php if($rowheader2['status']==1){ ?>
                  <label class="label label-warning">
                    diajukan 
                    <?php if($rowheader2['asman_ok']){
                      echo "ke manager";
                    }else{
                      echo "ke asman";
                    }
                    ?>
                  </label>
                <?php } ?>
                <?php if($rowheader2['status']==2){ ?>
                  <label class="label label-success">disetujui</label>
                <?php } ?>
                <?php if($rowheader2['status']==3){ ?>
                  <label class="label label-danger">dikembalikan</label>
                <?php } 
                }
                ?>
                <br/> 
                <small>
                  <?php if($rowheader1['nama_pekerjaan']) {
                    echo $rowheader1['nama_pekerjaan']."<br/>";
                  } ?>
                  PEMBERI PEKERJAAN : <?=$rowheader['pemberi_pekerjaan']?>
                </small>

                <?php
                if(is_array($versiarr) && count($versiarr) && $last_versi){ ?>
                  <br/><br/>
                <?php 
                  foreach($versiarr as $r){
                    echo '<a href="'.site_url("panelbackend/rab_rab_detail/index/".$r['id_rab']).'">';
                    if($r['id_rab']==$id_rab){
                      echo '<h4 style="display: inline;margin: 0px 0px 0px 4px;">';
                    }
                    /*if($r['is_final']){
                      echo '<span class="label label-success"><span class="glyphicon glyphicon-ok"></span> Final</span>';
                    }else*/if($r['id_rab']==$last_versi){
                      echo '<span class="label label-info">V.'.$r['versi'].'</span>';
                      if($ctrl=='rab_rab_detail' && $mode=='index')
                        $is_enable_revisi = true;
                    }else{
                      echo '<span class="label label-default">V.'.$r['versi'].'</span>';
                    }
                    if($r['id_rab']==$id_rab){
                      echo '</h4>';
                    }
                    echo '</a>';
                  }
                }
                ?>

            </div>
            <?php }else{ ?>
            <div class="pull-left">
              <?php echo UI::showBack($mode)?>
            </div>
            <?php } ?>
            <div class="pull-right">
                <?php 
                if($is_enable_revisi && $last_versi == $rowheader2['id_rab']){ ?>
                  <?php if(!$rowheader2['status'] && $this->access_role['ajukan']){ ?>
                    <button type="button" class="btn waves-effect btn-sm btn-warning" onclick="goSubmitConfirm('set_ajukan')"> Ajukan <span class="glyphicon glyphicon-arrow-right"></span></button>
                  <?php } ?>

                  <?php if($rowheader2['status']==1 && !$rowheader2['asman_ok'] && $this->access_role['ajukan_manager']){ ?>
                    <button type="button" class="btn waves-effect btn-sm btn-warning" onclick="goSubmitConfirm('set_kembalikan')"><span class="glyphicon glyphicon-arrow-left"></span> Kembalikan</button>
                    <button type="button" class="btn waves-effect btn-sm btn-success" onclick="goSubmitConfirm('set_ajukan_manager')"> Teruskan ke manager <span class="glyphicon glyphicon-arrow-right"></span></button>
                  <?php } ?>

                  <?php if($rowheader2['status']==1 && !$rowheader2['manager_ok'] && $rowheader2['asman_ok'] && $this->access_role['setujui']){ ?>
                    <button type="button" class="btn waves-effect btn-sm btn-warning" onclick="goSubmitConfirm('set_kembalikan')"><span class="glyphicon glyphicon-arrow-left"></span> Kembalikan</button>
                    <button type="button" class="btn waves-effect btn-sm btn-success" onclick="goSubmitConfirm('set_setujui')"><span class="glyphicon glyphicon-ok"></span> Setujui </button>
                  <?php } ?>

                  <?php if(($rowheader2['status']==3 or $rowheader2['is_final']) && $this->access_role['add']){ ?>
                    <button type="button" class="btn waves-effect btn-sm btn-danger" onclick="goPopUp('set_revisi')"><span class="glyphicon glyphicon-edit"></span> Revisi</button>
                  <?php } ?>
<script type="text/javascript">
  function goPopUp(act){
    $('#act').val(act);
    $('#myModal').modal('show');
  }
</script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Keterangan</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" style="width: 100%" name='keterangan' id='keterangan'></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="$('#main_form').submit();">Kirim</button>
      </div>
    </div>
  </div>
</div>

                <?php } 

                if(!$no_menu){
                  if($row['id_rab_detail'] && !is_array($row['id_rab_detail'])){
                    if($row['sumber_nilai']==1){
                      $add = array('<li><a href="'.site_url("panelbackend/rab_rab_detail/add/$id_rab/$row[$pk]").'" class="waves-effect "><span class="glyphicon glyphicon-share"></span> Add Sub</a> </li>');
                    }
                    echo UI::showMenuMode($mode, ($row[$pk]?$row[$pk]:$rowheader['id_proyek']),false,'','',null,null,$add);
                  }else{
                    if(!$no_header && $this->access_role['index']){
                      echo UI::showBack($mode);
                    }
                    echo UI::showMenuMode($mode, ($row[$pk]?$row[$pk]:$rowheader['id_proyek']));
                  }
                }
                ?>
            </div>
          </div>
          <div class="box-body">


              <?php echo $content1;?>
              <div style="clear: both;"></div>
          </div>
        </div>
      </div>
<!--       <div class="tab-content">
          <div class="box-header with-border">
            BOQ > EVALUASI > PENETAPAN
          </div>
  </div> -->
    </div>
  </div><!-- /.box -->
    <div style="clear: both;"></div>
</section>

<style type="text/css">
    table.dataTable {
    clear: both;
    margin-bottom: 6px;
    max-width: none !important;
}
table.table-label{
  font-size: 11px;
  color: #666;
}
</style>