<table width="100%">
<tbody>
<tr>
<td>&nbsp;</td>
<td style="background: #082d69 !important; border: 1px solid #fff; font-style: italic; color: #fff; text-align: center; font-size: 20px; font-weight: bold; width: 90%;">Maksimalisasi Nilai Shareholder</td>
</tr>
<tr>
<td style="text-align: center; width: 10%; background-color: #034485; border: 1px solid #fff; color: #fff;">FINANCIAL</td>
<td style="border: 1px solid #fff;">
<table style="border-spacing: 7px; border-collapse: separate;" width="100%">
<tbody>
<tr>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #034485; border-radius: 5px; padding: 10px;" width="33%"><a id="PSF1" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> F1 : Peningkatan Pendapatan </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #034485; border-radius: 5px; padding: 10px;" width="33%"><a id="PSF2" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> F2 : Penguatan Kesehatan Finansial </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #034485; border-radius: 5px; padding: 10px;" width="33%"><a id="PSF3" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> F3 : Efisiensi Biaya </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>&nbsp;</td>
<td style="background: #082d69 !important; border: 1px solid #fff; font-style: italic; color: #fff; text-align: center; font-size: 20px; font-weight: bold; width: 45%;">Menarik Pelanggan &amp; Pasar Baru</td>
<td style="background: #082d69 !important; border: 1px solid #fff; font-style: italic; color: #fff; text-align: center; font-size: 20px; font-weight: bold; width: 45%;">Optimalisasi Biaya dan Keunggulan Operasional</td>
</tr>
<tr>
<td style="text-align: center; width: 10%; background-color: #ffa400; border: 1px solid #fff; color: #fff;">STAKE HOLDER / CUSTOMER</td>
<td style="border: 1px solid #fff;" colspan="2">
<table style="border-spacing: 7px; border-collapse: separate;" width="100%">
<tbody>
<tr>
<td style="text-align: center; font-size: 16px; border: 2px solid #ffffff; background-color: #ffa400; border-radius: 5px; padding: 10px; width: 25%;"><a id="PSS1" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> S1 : Menarik Pelanggan Baru </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #ffffff; background-color: #ffa400; border-radius: 5px; padding: 10px; width: 25%;"><a id="PSS2" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> S2 : Perusahaan Jasa O&amp;M dengan Reputasi bagus </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #ffffff; background-color: #ffa400; border-radius: 5px; padding: 10px; width: 17%;"><a id="PSS3" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> S3 : Menjaga long-relationship pelanggan dengan memberikan covinient customer experience </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #ffffff; background-color: #ffa400; border-radius: 5px; padding: 10px; width: 16%;"><a id="PSS4" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> S4 : Jasa O&amp;M yang handa dan efisien </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #ffffff; background-color: #ffa400; border-radius: 5px; padding: 10px; width: 16%;"><a id="PSS5" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> S5 : Jasa Proyek yang berkualitas dan kompetitif </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="text-align: center; width: 10%; background-color: #d21111; border: 1px solid #fff; color: #fff;">PROCESS EXCELLENCE</td>
<td style="border: 1px solid #fff;" colspan="2">
<table style="border-spacing: 7px; border-collapse: separate;" width="100%">
<tbody>
<tr>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="50%"><a id="PSP1" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P1 : Pengembangan strategi pemasaran yang agresif </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="25.5%"><a id="PSP5" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P5 : Standarisasi Asset Management Practice </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="25.5%"><a id="PSP6" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P6 : Optimalisasi Efisiensi Kualitas dan Percepatan Jasa Proyek </a></td>
</tr>
</tbody>
</table>
<table style="border-spacing: 7px; border-collapse: separate;" width="100%">
<tbody>
<tr>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="30%"><a id="PSP2" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P2 : Pengembangan Customized Product, bisnis baru &amp; clean energy </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="30%"><a id="PSP3" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P3 : Pengembangan <em>Digitalization Services</em> </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="20%"><a id="PSP4" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P4 : Optimalisasi Supply Chain Management </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #d21111; border-radius: 5px; padding: 10px;" width="20%"><a id="PSP7" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis">P7 : Peningkatan Manajemen Risiko dan LK3 </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table width="100%">
<tbody>
<tr>
<td>&nbsp;</td>
<td style="background: #082d69 !important; border: 1px solid #fff; font-style: italic; color: #fff; text-align: center; font-size: 20px; font-weight: bold;">Organisasi&nbsp;&amp; Tenaga Kerja yang Kompeten &amp; Agile</td>
</tr>
<tr>
<td style="text-align: center; width: 10%; background-color: #4396de; border: 1px solid #fff; color: #fff;">PEOPLE &amp; LEARNING</td>
<td style="border: 1px solid #fff;">
<table style="border-spacing: 7px; border-collapse: separate;" width="100%">
<tbody>
<tr>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #4396de; border-radius: 5px; padding: 10px;" width="25%"><a id="PSL1" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> L1 :&nbsp;Pengembangan kapabilitas bisnis, marketing &amp; teknologi baru </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #4396de; border-radius: 5px; padding: 10px;" width="25%"><a id="PSL2" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> L2 :&nbsp;Memperkuat budaya inovatif dan berwawasan bisnis </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #4396de; border-radius: 5px; padding: 10px;" width="25%"><a id="PSL3" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> L3 :&nbsp;Akselerasi pengembangan technical expertise </a></td>
<td style="text-align: center; font-size: 16px; border: 2px solid #fff; background-color: #4396de; border-radius: 5px; padding: 10px;" width="25%"><a id="PSL4" style="color: #fff;" href="#" data-toggle="modal" data-target="#risikostrategis"> L4 :&nbsp;Peningkatan Enggament dan Produktivitas organisasi </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>