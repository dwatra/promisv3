<div class="col-sm-6">

<?php 
$from = UI::createTextBox('kode',$row['kode'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["kode"], "kode", "Kode");
?>

<?php 
$from = UI::createSelect('id_unit',$mtunitarr,$row['id_unit'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_unit"], "id_unit", "Unit");
?>

</div>
<div class="col-sm-6">
				

<?php 
$from = UI::createTextNumber('nilai',$row['nilai'],'','',$edited,$class='form-control ',"style='text-align:right; width:100%' min='0' max='99999999999999' step='any'");
echo UI::createFormGroup($from, $rules["nilai"], "nilai", "Nilai");
?>

<?php 
if(!$edited){
	$row['url'] = "<a href='".$row['url']."' target='_BLANK'>".$row['url']."</a>";
}
$from = UI::createTextArea('url',$row['url'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["url"], "url", "URL");
?>

<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>