<table class="table table-striped table-bordered table-hover dataTable">
    <thead>
		<tr>
	        <th style="width:1px">#</th>
	        <th style='max-width:auto; cursor:pointer;'>Tgl. Permintaan</th>
	        <th style='max-width:auto; cursor:pointer;'>Hari Permintaan</th>
	        <th style='max-width:auto; cursor:pointer;' colspan="2">No. PR</th>
	        <th style='max-width:auto; cursor:pointer;'>No. WO / PRK</th>
	        <th style='max-width:auto; cursor:pointer;'>Nama Barang / Jasa</th>
	        <th style='max-width:auto; cursor:pointer;'>Nama Anggaran</th>
	        <th style='max-width:auto; cursor:pointer;'>Waktu Dibutuhkan</th>
	        <th style='max-width:auto; cursor:pointer;'>Nilai Anggaran</th>
	        <th style='max-width:auto; cursor:pointer;'>Status</th>
	        <th style='max-width:auto; cursor:pointer;'>Tgl. Pengerjaan PR</th>
	        <th style='max-width:auto; cursor:pointer;'>Durasi</th>
	        <th style="width:10px"></th>
		</tr>
    </thead>
    <tbody>
    	<tr>
    		<td style="text-align: center;">1</td>
    		<td>1-Jan-18</td>
    		<td>Senin</td>
    		<td>1</td>
    		<td>.PR/PROYEK/PJBS/2018</td>
    		<td>-</td>
    		<td>
    			<a href="<?=site_url("panelbackend/pr/detail/1")?>">
    			Jasa Fabrikasi Seal Strip Oil Deflector Upper Lower Bearing
    			</a>
    		</td>
    		<td>Variant Order Mean Inspection PLTU Pacitan Unit 2 Tahun 2017</td>
    		<td>Januari 2018</td>
    		<td>Rp 580.800.000 </td>
    		<td>Cancel</td>
    		<td>03 Januari 2018 - 09 Januari 2018</td>
    		<td style="background: blue; color: #fff;">6 Hari</td>
    		<td style='text-align:left'>
    			<div class="dropdown">
					<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Menu
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2" style="min-width: 10px;">
						<li>
							<a style="padding: 3px 20px; font-size: 12px" href="<?=site_url("panelbackend/pr/edit/1")?>">
							<span class="glyphicon glyphicon-print"></span> Template PR
							</a>
						</li>
						<li>
							<a style="padding: 3px 20px; font-size: 12px" href="<?=site_url("panelbackend/pr/edit/1")?>">
							<span class="glyphicon glyphicon-print"></span> Template TOR
							</a>
						</li>
						<li>
							<a style="padding: 3px 20px; font-size: 12px" href="<?=site_url("panelbackend/pr/edit/1")?>">
							<span class="glyphicon glyphicon-edit"></span> Edit
							</a>
						</li>
						<li>
							<a style="padding: 3px 20px; font-size: 12px" href="#">
							<span class="glyphicon glyphicon-remove"></span> Delete
							</a>
						</li>
					</ul>
				</div>
    		</td>
    	</tr>
    </tbody>
</table>

<style type="text/css">
.table-bordered > thead > tr > th {
    text-align: center !important;
    padding: 0px 5px !important;
}
</style>