<?php if(($realisasipusat or $realisasiproyek) && $last_versi==$id_rab) { ?>
<div>
	<center>
		<?php if($rowheader1['is_selesai']){ ?>
		<h4>Selesai dalam durasi <b><?=round($rowheader1['durasi'])?> Hari</b></h4>
		<?php }else{ ?>
		<h4>
			Progress realisasi biaya Hari <b><?=round($rowheader1['durasi'])?>  </b> dari <b><?=$rowheader1['rencanadurasi']?></b>
		</h4>
		<?php }	?>
	</center>
	<div class="row">
		<div class="col-sm-6" style="border-right: 1px solid #e5e5e5;">
			<h4><center>PUSAT</center></h4>
			<canvas id="omegapusat" style="height:200px; width:100%"></canvas>
			<table width="100%" class="table table-hover">
				<tr>
					<td style="font-size: 16px">Total RAB</td>
					<td style="font-size: 16px">:</td>
					<td style="text-align: right;font-size: 16px"><?=rupiah($rowheader1['rab_pusat'])?></td>
				</tr>
				<tr>
					<td style="font-size: 16px">Penyerapan</td>
					<td style="font-size: 16px">:</td>
					<td style="text-align: right;font-size: 16px"><?=rupiah($rowheader1['realisasi_pusat'])?></td>
				</tr>
				<tr>
					<td style="font-size: 16px">Sisa</td>
					<td style="font-size: 16px">:</td>
					<td style="text-align: right;font-size: 16px"><?=rupiah($rowheader1['rab_pusat']-$rowheader1['realisasi_pusat'])?></td>
				</tr>
			</table>

			<?php
				$prosen=0;
				if($rowheader1['rab_pusat'])
					$prosen = (float)$rowheader1['realisasi_pusat']/(float)$rowheader1['rab_pusat']*100;
			?>
			<div class="progress" title="<?=round($prosen,2)?> %">
                <div class="progress-bar <?=($prosen>100?"progress-bar-red":(($prosen==100)?"progress-bar-blue":"progress-bar-green"))?>" style="width: <?=round($prosen,2)?>%;"><?=round($prosen,2)?>%</div>
              </div>
              <div style="text-align: right; color: #999"><small><i>Terakhir update realisasi : <?=Eng2Ind($rowheader2['last_update_realisasi_rab'])?></i></small></div>
		</div>
		<div class="col-sm-6">
			<h4><center>PROYEK</center></h4>
			<canvas id="omegaproyek" style="height:200px; width:100%"></canvas>
			<table width="100%" class="table table-hover">
				<tr>
					<td style="font-size: 16px">Total RAB</td>
					<td style="font-size: 16px">:</td>
					<td style="text-align: right;font-size: 16px"><?=rupiah($rowheader1['rab_proyek'])?></td>
				</tr>
				<tr>
					<td style="font-size: 16px">Penyerapan</td>
					<td style="font-size: 16px">:</td>
					<td style="text-align: right;font-size: 16px"><?=rupiah($rowheader1['realisasi_proyek'])?></td>
				</tr>
				<tr>
					<td style="font-size: 16px">Sisa</td>
					<td style="font-size: 16px">:</td>
					<td style="text-align: right;font-size: 16px"><?=rupiah($rowheader1['rab_proyek']-$rowheader1['realisasi_proyek'])?></td>
				</tr>
			</table>

			<?php
				$prosen=0;
				if($rowheader1['rab_proyek'])
					$prosen = (float)$rowheader1['realisasi_proyek']/(float)$rowheader1['rab_proyek']*100;
			?>
			<div class="progress" title="<?=round($prosen,2)?> %">
                <div class="progress-bar <?=($prosen>100?"progress-bar-red":(($prosen==100)?"progress-bar-blue":"progress-bar-green"))?>" style="width: <?=round($prosen,2)?>%;"><?=round($prosen,2)?>%</div>
              </div>
              <div style="text-align: right; color: #999"><small><i>Terakhir update realisasi : <?=Eng2Ind($rowheader2['last_update_realisasi_rab'])?></i></small></div>
		</div>
	</div>
</div>
<script src="<?=site_url("assets/template/backend/plugins/chartjs/Chart.bundle.js")?>"></script>
<script src="<?=site_url("assets/template/backend/plugins/chartjs/utils.js")?>"></script>


<script type="text/javascript">
    
    var COLORS = [
        '#4dc9f6',
        '#f67019',
        '#f53794',
        '#537bc4',
        '#acc236',
        '#166a8f',
        '#00a950',
        '#58595b',
        '#8549ba'
    ];

    window.onload = function() {
    	<?php if($realisasipusat){ ?>
	        var ctx = document.getElementById('omegapusat').getContext('2d');
	        window.myBar = new Chart(ctx, {
	            type: 'line',
	            data: {
	                satuan:'',
	                labels: <?php echo json_encode(array_keys($realisasipusat)); ?>,
	                datasets: [{
						lineTension: 0,
	                    data:<?php echo json_encode(array_values($realisasipusat)); ?>,
	                    backgroundColor: COLORS[0],
	                    borderColor: COLORS[0]
	                }],
	            },
	            options: {
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                scales: {
	                    yAxes: [{
	                        ticks: {
	                            beginAtZero:false
	                        },
	                        gridLines: {
	                            display:false
	                        }
	                    }],
	                    xAxes: [{
	                        ticks: {
	                            callback: function(value, index, values) {
	                            	var label = value.toString();
		                            var tahun = label.substring(0,4);
		                            var bulan = label.substring(4,6);
		                            var tgl = label.substring(6,8);
		                            label = tgl+'-'+bulan+'-'+tahun;
		                            if(tahun=='1970')
		                            	return null;
		                            
	                                return label;
	                            }
	                        },
	                        gridLines: {
	                            display:false
	                        }
	                    }]
	                },
	                tooltips: {
	                    callbacks: {
	                        label: function(tooltipItem, data) {
	                            var label = data.datasets[0].data[tooltipItem.index];
	                           
	                            return label;
	                        }
	                    }
	                }
	            }
	        });
	    <?php } ?>

    	<?php if($realisasiproyek){ ?>
	        var ctx = document.getElementById('omegaproyek').getContext('2d');
	        window.myBar = new Chart(ctx, {
	            type: 'line',
	            data: {
	                satuan:'',
	                labels: <?php echo json_encode(array_keys($realisasiproyek)); ?>,
	                datasets: [{
						lineTension: 0,
	                    data:<?php echo json_encode(array_values($realisasiproyek)); ?>,
	                    backgroundColor: COLORS[0],
	                    borderColor: COLORS[0]
	                }],
	            },
	            options: {
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                scales: {
	                    yAxes: [{
	                        ticks: {
	                            beginAtZero:false
	                        },
	                        gridLines: {
	                            display:false
	                        }
	                    }],
	                    xAxes: [{
	                        ticks: {
	                            callback: function(value, index, values) {
	                            	var label = value.toString();
		                            var tahun = label.substring(0,4);
		                            var bulan = label.substring(4,6);
		                            var tgl = label.substring(6,8);
		                            label = tgl+'-'+bulan+'-'+tahun;
		                            if(tahun=='1970')
		                            	return null;
		                            
	                                return label;
	                            }
	                        },
	                        gridLines: {
	                            display:false
	                        }
	                    }]
	                },
	                tooltips: {
	                    callbacks: {
	                        label: function(tooltipItem, data) {
	                            var label = data.datasets[0].data[tooltipItem.index];
	                           
	                            return label;
	                        }
	                    }
	                }
	            }
	        });
	    <?php } ?>
    }
</script>
<?php } ?>
<br/>
<table class="table table-hover tree-table">
	<thead>
		<tr>
			<th width="50px" style="text-align: center;">No.</th>
			<th width="70px">KODE</th>
			<th style="text-align: left !important;">URAIAN BIAYA</th>
			<th width="120px" style="text-align: right !important;">ANGGARAN</th>
			<th width="120px" style="text-align: right !important;">REALISASI</th>
			<th>PROGRESS</th>
			<th>POS</th>
			<th>KETERANGAN</th>
			<th width="1px"></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$mtposanggaranarr[''] = '';
		$total = 0;
		$no=1;
		if(count($rows)){
			foreach($rows as $r){ 
				if($r['level']>2 && !$r['id_pos_anggaran'])
					continue;
			?>
		<tr data-tt-id='<?=$r['id_rab_detail']?>' data-tt-parent-id='<?=$r['id_rab_detail_parent']?>'>
			<td align="center">
				<b><?php if($r['level']<=1){echo $no++;} ?></b>
			</td>
			<td>
				<?php if($r['id_pos_anggaran']){
					echo $r['kode_biaya'];
				}else{ ?>
					<?=$r['kode_biaya']?>
				<?php } ?>
			</td>
			<td><a href='<?=site_url("panelbackend/rab_rab_detail/detail/$id_rab/$r[id_rab_detail]")?>'>
					<?=$r['uraian']?>
			</a></td>
			<td style="text-align: right;">
				<?php if($r['id_pos_anggaran']){ 
					$total+=(float)$r['nilai_satuan'];
					?>
					<?=rupiah($jumlah=$r['nilai_satuan'],2)?>
				<?php } ?>
					</td>
			<td style="text-align: right;">
				<?php if($r['id_pos_anggaran']){ 
					$totalrealisasi+=(float)$r['nilai_realisasi'];
					?>
					<?=rupiah($r['nilai_realisasi'],2)?>
				<?php } ?></td>
			<td>
				<?php if($r['id_pos_anggaran'] && $jumlah){ ?>
					<?php $prosen=($jumlah?((float)$r['nilai_realisasi']/(float)$jumlah*100):0); ?>
					<div class="progress" title="<?=round($prosen,2)?> %">
                        <div class="progress-bar <?=($prosen>100?"progress-bar-red":(($prosen==100)?"progress-bar-blue":"progress-bar-green"))?>" style="width: <?=round(($prosen>100?100:$prosen),2)?>%;"><?=round($prosen,2)?>%</div>
                      </div>
				<?php } ?></td>
			<td style="text-align: center;">

					<?=$mtposanggaranarr[$r['id_pos_anggaran']]?>
					</td>
			<td><?=$r['keterangan']?></td>
			<td><?php
			$add = array();
			if($r['sumber_nilai']==1){
				$add = array('<li><a href="'.site_url("panelbackend/rab_rab_detail/add/$id_rab/$r[$pk]").'" class="waves-effect "><span class="glyphicon glyphicon-share"></span> Add Sub</a> </li>');
			}
			echo UI::showMenuMode('inlist', $r[$pk],false,'','',null,null,$add);
			?></td>
		</tr>
	<?php } ?>
	<tr><td colspan="3" style="text-align: right;"><b>JUMLAH</b></td><td style="text-align: right;"><?=rupiah($total)?></td><td style="text-align: right;"><?=rupiah($totalrealisasi)?></td><td></td><td></td><td></td></tr>
<?php }else{ ?>
		<tr><td colspan="6"><i>Belum ada data</i></td></tr>
	<?php } ?>
	</tbody>
</table>
<br/>
<?=UI::getButton("add",null,null,'btn-sm')?>
<div style="float: right;">
	<?php if($this->access_role['download_pdf']){ ?>
	<a target='_BLANK' href="<?=site_url("panelbackend/rab_rab_detail/download_pdf/$id_rab")?>"><span class="glyphicon glyphicon-print"></span> Print ALL</a>
	<?php } ?> &nbsp;  &nbsp; 
	<a target='_BLANK' href="<?=site_url("panelbackend/rab_rab_detail/go_print/$id_rab")?>"><span class="glyphicon glyphicon-print"></span> Print</a>
</div>
				<?php 
                if($this->access_role['edit']){ ?>
                  <button type="button" class="btn waves-effect btn-sm btn-warning" onclick="goSubmit('set_recal')"><span class="glyphicon glyphicon-refresh"></span> Refresh</button>
                <?php } ?>
                <?php if($last_versi==$rowheader2['id_rab'] && $this->access_role['add']){ ?>
                  <button type="button" class="btn waves-effect btn-sm btn-danger" onclick="if(confirm('Apakah Anda yakin akan menjadikan pekerjaan baru ?')){goSubmit('set_pekerjaan_baru')}"><span class="glyphicon glyphicon-add"></span> Jadikan Pekerjaan Baru</button>
                  <button type="button" class="btn waves-effect btn-sm btn-danger" onclick="if(confirm('Apakah Anda yakin akan menjadikan proyek baru ?')){goSubmit('set_salin_proyek')}"><span class="glyphicon glyphicon-add"></span> Salin Proyek</button>
                <?php } ?>
<style type="text/css">
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		    padding: 5px 10px !important;
		}
</style>

<link href="<?=base_url()?>assets/css/jquery.treetable.theme.default.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/jquery.treetable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/js/jquery.treetable.js"></script>