<div class="col-sm-12">

<?php 
$from = UI::createTextBox('kode',$row['kode'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["kode"], "kode", "Kode",false,3);
?>

<?php
if($row['id_spec_item'] or $this->post['act']=='spek' or $row['spek']){
$from = UI::createSelect("id_spec_item",$specitemarr,$row['id_spec_item'],$edited,$class='form-control select2',"style='width:auto; width:100%;' data-ajax--data-type=\"json\" data-ajax--url=\"".site_url('panelbackend/ajax/listspec')."\"");
$from .= "<script>$('#id_spec_item').change(function(e) { 
var selections = $('#id_spec_item').select2('data');
$('#nama').val(selections[0].text);
goSubmit('set_value');
});</script>";
if($edited){
	$from .= "<button type='button' class='btn btn-default btn-xs' onclick='$(\"#nama\").val(\"\"); $(\"#id_spec_item\").val(\"\"); $(\"#spek\").val(\"\"); goSubmit(\"manual\")'>Manual</button>";
	$from .= UI::createTextHidden('nama',$row['nama'],$edited);
	$from .= UI::createTextHidden('spek',1,$edited);
}
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama",false,3);

if($row['id_spec_item']){
	$from = "<table class='table table-no-padding'><tr><th>Attribute</th><th>Nilai Spec</th></tr>";

	foreach($attribute_default as $i=>$r){
		if($edited or $attribute[$r['id_spec_attribute']]){
			$from .= "<tr><td><b>".$r['nama']." </b> </td><td>".UI::createTextBox("attribute[".$r['id_spec_attribute']."]",$attribute[$r['id_spec_attribute']],'','',$edited,'form-control',"style='width:100%'")."</td></tr>";
		}
	}
	$from .= "</table>";
	echo UI::createFormGroup($from, $rules["attribute"], "attribute", "Spesifikasi", false, 3);
}

} else {
	$from = UI::createTextBox('nama',$row['nama'],'200','100',$edited,$class='form-control ',"style='width:100%'");
	if($edited){
		$from .= "<button type='button' class='btn btn-default btn-xs' onclick='$(\"#nama\").val(\"\"); goSubmit(\"spek\")'>Spesifikasi</button>";
	}
	echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama",false,3);
}
?>

<?php 
$from = UI::createSelect('id_jabatan_proyek',$mtjabatanproyekarr,$row['id_jabatan_proyek'],$edited,$class='form-control ',"style='width:100%;'");
echo UI::createFormGroup($from, $rules["id_jabatan_proyek"], "id_jabatan_proyek", "Jabatan Proyek", false,3);
?>

<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from, null, null, null, false, 3);
?>
</div>