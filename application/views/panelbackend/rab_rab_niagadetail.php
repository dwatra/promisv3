<ol class="breadcrumb">
  <li><a href="<?=site_url("panelbackend/rab_rab_niaga/index/$id_rab_niaga")?>">RAB</a></li>
  <?php
  foreach($breadcrumb as $k=>$v){
  	echo '<li><a href="'.site_url("panelbackend/rab_rab_niaga/detail/$id_rab_niaga/$k").'">'.$v.'</a></li>';
  }
  ?>
  <li><?=$row['uraian']?></li>
</ol>

<div class="col-sm-12">
<?php
echo "<h4>$row[uraian]</h4>";
?>
<hr/>
</div>
<div class="col-sm-7">

<?php 

echo "<h5 class='no-margin'>";
$jumlahnilai = caljumlah($row, true);
echo rupiah($jumlahnilai)."</h5><br/>";

if(!$row['keterangan'])
	$row['keterangan'] = '<i>kosong</i>';

$from = UI::createTextArea('keterangan',$row['keterangan'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["keterangan"], "keterangan", "Keterangan",true,3);


if(is_array($row['files']) && count($row['files'])){
$from = UI::createUploadMultiple("files", $row['files'], $page_ctrl, $edited, "files");
echo UI::createFormGroup($from, $rules["files"], "files", "Lampiran", true, 3);
}
?>
</div>

<div style="clear: both;"></div>
<?php if(count($rows) && $row['sumber_nilai']==1){ 
$id_rab_niaga_detail_parent = $r['id_rab_detail_parent'];
$rowparent[$row['id_rab_detail']] = $row;
	?>
	<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th width="10px">No.</th>
			<?php if($is_kode_biaya){ ?>
				<th width="10px">KODE BIAYA</th>
			<?php } ?>
			<th>URAIAN BIAYA</th>
			<?php if($is_satuan){ ?>
				<th>VOLUME</th>
				<th>SATUAN</th>
				<th>HARGA SATUAN</th>
			<?php } ?>
			<th>JUMLAH</th>
			<th width="1px"></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$mtposanggaranarr[''] = '';
		if(count($rows)){
			$no=1;
			foreach($rows as $r){ 
				?>
		<tr>

			<?php if(($is_kode_biaya && $r['kode_biaya']) or !$is_kode_biaya){ ?>
			<td align="center"><?=$no++?></td>
			<?php } else { ?>
			<td align="center"></td>
			<?php } ?>
			<?php if($is_kode_biaya){ ?>
				<td><?=$r['kode_biaya']?></td>
			<?php } ?>
			<td><a href='<?=site_url("panelbackend/rab_rab_niaga/detail/$id_rab_niaga/$r[id_rab_detail]")?>'><?=$r['uraian']?></a></td>
			<?php if($is_satuan){ 
				if(!$r['day'])$r['day']=1;
				?>
				<td style="text-align: center;"><?=((float)$r['vol']*(float)$r['day'])?(float)$r['vol']*(float)$r['day']:''?></td>
				<td style="text-align: center;"><?=$r['satuan']?></td>
			<?php }  if($is_satuan){ ?>
			<td style="text-align: right;"><?=rupiah($r['nilai_satuan'],2)?></td>
			<?php } ?>
			<td style="text-align: right;"><?=rupiah($jumlah=caljumlah($r),2)?></td>
			<td><?php
			$add = array();
			if($r['sumber_nilai']==1){
				$add = array('<li><a href="'.site_url("panelbackend/rab_rab_niaga/add/$id_rab_niaga/$r[$pk]").'" class="waves-effect "><span class="glyphicon glyphicon-share"></span> Add Sub</a> </li>');
			}
			echo UI::showMenuMode('inlist', $r[$pk]."/".$r['id_rab_detail_parent'],false,'','',null,null,$add);
			?></td>
		</tr>

	<?php }?>
	<?php }else{ ?>
		<tr><td colspan="6"><i>Belum ada data</i></td></tr>
	<?php } ?>
	</tbody>
</table>

<?php }elseif ($row['sumber_nilai']=='3') { ?>

	<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th width="10px">No.</th>
			<th>SCOPE OF WORK</th>
			<th>VOLUME</th>
			<th>SATUAN</th>
			<th>HARGA SATUAN</th>
			<th>JUMLAH</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no=1;
		if(count($rowsjasa_material)){
			foreach($rowsjasa_material as $r){ 
				?>
		<tr>
			<td align="center"><?=$no++?></td>
			<td><?=$r['nama']?></td>
			<td style="text-align: right;"><?=$r['vol']?></td>
			<td><?=$r['satuan']?></td>
			<td style="text-align: right;"><?=rupiah($r['harga_satuan'],2)?></td>
			<td style="text-align: right;"><?=rupiah($jumlah=$r['total'],2)?></td>
		</tr>

	<?php }?>
	<?php }else{ ?>
		<tr><td colspan="7"><i>Belum ada data</i></td></tr>
	<?php } ?>
	</tbody>
</table>
<?php }
?>
<div style="clear: both;"></div>
<br/>
<?php if($row['sumber_nilai']==1){ ?>
<?=UI::getButton("add",null,null,'btn-sm', "Add Sub")?>

<script type="text/javascript">
	function goAdd(){
		        window.location = "<?=base_url("panelbackend/rab_rab_niaga/add/$id_rab_niaga/$row[id_rab_detail]")?>";
		    }
</script>
<?php } ?>

<style type="text/css">
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		    padding: 5px !important;
		}
	.pfile{
		display: inline !important;  
	}
</style>