<div class="col-sm-6">

<?php 
$from = UI::createTextBox('id_niaga_penawaran_parent',$row['id_niaga_penawaran_parent'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["id_niaga_penawaran_parent"], "id_niaga_penawaran_parent", "Niaga Penawaran Parent");
?>

<?php 
$from = UI::createTextBox('uraian',$row['uraian'],'200','100',$edited,'form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["uraian"], "uraian", "Uraian");
?>

<?php 
$from = UI::createTextBox('nilai_satuan',$row['nilai_satuan'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["nilai_satuan"], "nilai_satuan", "Nilai Satuan");
?>

<?php 
$from = UI::createTextBox('vol',$row['vol'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["vol"], "vol", "VOL");
?>

<?php 
$from = UI::createTextBox('satuan',$row['satuan'],'20','20',$edited,'form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["satuan"], "satuan", "Satuan");
?>

<?php 
$from = UI::createTextBox('sumber_nilai',$row['sumber_nilai'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["sumber_nilai"], "sumber_nilai", "Sumber Nilai");
?>

<?php 
$from = UI::createTextBox('sumber_satuan',$row['sumber_satuan'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["sumber_satuan"], "sumber_satuan", "Sumber Satuan");
?>

</div>
<div class="col-sm-6">
				

<?php 
$from = UI::createSelect('id_niaga_proyek',$niagaproyekarr,$row['id_niaga_proyek'],$edited,'form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_niaga_proyek"], "id_niaga_proyek", "Niaga Proyek");
?>

<?php 
$from = UI::createTextBox('created_by_desc',$row['created_by_desc'],'200','100',$edited,'form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["created_by_desc"], "created_by_desc", "Created BY Desc");
?>

<?php 
$from = UI::createTextBox('modified_by_desc',$row['modified_by_desc'],'200','100',$edited,'form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["modified_by_desc"], "modified_by_desc", "Modified BY Desc");
?>

<?php 
$from = UI::createTextBox('jenis_mandays',$row['jenis_mandays'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["jenis_mandays"], "jenis_mandays", "Jenis Mandays");
?>

<?php 
$from = UI::createTextBox('day',$row['day'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["day"], "day", "DAY");
?>

<?php 
$from = UI::createTextBox('pembagi',$row['pembagi'],'10','10',$edited,'form-control rupiah ',"style='text-align:right; width:190px' min='0' max='10000000000' step='1'");
echo UI::createFormGroup($from, $rules["pembagi"], "pembagi", "Pembagi");
?>

<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>