<link href="<?=base_url()?>assets/css/fixedColumns.dataTables.min.css" rel="stylesheet">
<script src="<?=site_url("assets/template/backend/plugins/chartjs/Chart.bundle.js")?>"></script>
<script src="<?=site_url("assets/template/backend/plugins/chartjs/utils.js")?>"></script>
<script src="<?=site_url("assets/js/jquery.dataTables.min.js")?>"></script>
<script src="<?=site_url("assets/js/dataTables.fixedColumns.min.js")?>"></script>

	<div style="width:100%; margin:0px auto; ">
		<canvas id="canvas"></canvas>
	</div>
	<script>
		var timeFormat = 'MM/DD/YYYY HH:mm';

 window.chartColors = {
      red: 'rgb(165, 19, 1)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(5, 25, 110)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };

		var color = Chart.helpers.color;
		var config = {
			type: 'line',
			data: {
				labels: [
					<?php for ($i=0;$i<=$times['total_hari']; $i++) { ?>
						'<?php echo $i ?>',
					<?php } ?>
				],
				datasets: [
				{
					label: 'Rencana',
					backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
					borderColor: window.chartColors.blue,
					fill: false,
					lineTension: 0,
					data: [
					<?php 
					foreach ($plan_per_day as $progress) { 
						echo round($progress,2) . ',' ;
					} ?>
					],
				}, 
				{
					label: 'Realisasi',
					backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
					borderColor: window.chartColors.red,
					fill: false,
					lineTension: 0,
					data: [
					<?php 
					foreach ($realisasi_per_day as $progress) { 
						echo round($progress,2) . ',' ;
					} ?>
					],
				}, 
				]
			},
			options: {
				legend: {
                    display: true
                },
				responsive: true,
				title: {
					display: true,
					text: 'KURVA S'
				},
				scales: {
					xAxes: [{
						display: true,
/*
						type: 'time',
						time: {
							parser: timeFormat,
							// round: 'day'
							tooltipFormat: 'll HH:mm'
						},
*/						
						scaleLabel: {
							display: true,
							labelString: 'Hari Ke'
						},
						ticks: {
							major: {
								fontStyle: 'bold',
								fontColor: '#FF0000'
							}
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Progress %'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		
	</script>