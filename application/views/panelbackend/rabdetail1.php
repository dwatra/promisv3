<ol class="breadcrumb">
  <li><a href="<?=site_url("panelbackend/rab/detail")?>">RAB</a></li>
  <li class="active">MATERIAL SPAREPART DAN PENDUKUNG PROYEK</li>
</ol>

<table class="table table-bordered">
	<thead>
		<tr>
			<th width="10px">No.</th>
			<th width="10px">KODE BIAYA</th>
			<th>URAIAN BIAYA</th>
			<th>JUMLAH ANGGARAN</th>
			<th>KETERANGAN</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td></td>
			<td><b>MATERIAL PUSAT</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E101</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL MEKANIK</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E102</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL LISTRIK</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E103</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL GENERAL/CONSUMABLE</a></td>
			<td align="right">1.488.113.220</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E104</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL KIMIA</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E105</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL I&C</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E106</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL LUBE & BBM</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E107</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>SPAREPART</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E108</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>TOOLS</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E109</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>LK3</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><b>MATERIAL PROYEK</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E101</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL MEKANIK</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E102</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL LISTRIK</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E103</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL GENERAL/CONSUMABLE</a></td>
			<td align="right">683.600.000</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E104</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL KIMIA</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E105</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL I&C</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E106</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>MATERIAL LUBE & BBM</a></td>
			<td align="right">150.942.857</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E107</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>SPAREPART</a></td>
			<td>-</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E108</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>TOOLS</a></td>
			<td align="right">330.000.000</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>E109</td>
			<td><a href='<?=site_url("panelbackend/rab/detail2")?>'>LK3</a></td>
			<td align="right">60.890.000</td>
			<td></td>
		</tr>
	</tbody>
</table>