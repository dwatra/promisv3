
<div class="col-sm-6">

<?php 
/*$from = UI::createTextBox('no',$row['no'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no"], "no", "NO");*/
?>

<?php 
if($row['tgl_permintaan']===null)
	$row['tgl_permintaan'] = date('d-m-Y');

$from = UI::createTextBox('tgl_permintaan',$row['tgl_permintaan'],'10','10',$edited,$class='form-control datepicker',"style='width:100px' onchange='goSubmit()'");
echo UI::createFormGroup($from, $rules["tgl_permintaan"], "tgl_permintaan", "Tgl. Permintaan");
?>
<?php 
$from = UI::createSelect('id_priority_pr',$mtpriorityprarr,$row['id_priority_pr'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_priority_pr"], "id_priority_pr", "Prioritas");
?>

<?php 
$from = UI::createSelect('id_jenis_no_pr',$jenisnopr,$row['id_jenis_no_pr'],$edited,$class='form-control ',"style='width:auto; max-width:100%;' onchange='goSubmit()'");
echo UI::createFormGroup($from, $rules["id_jenis_no_pr"], "id_jenis_no_pr", "Jenis PR");
?>

<?php 
$from = UI::createTextBox('no_desc',$row['no_desc'],'200','100',$edited,$class='form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["no_desc"], "no_desc", "No. Permintaan");
?>

<?php 
$from = UI::createTextBox('no_prk',$row['no_prk'],'200','100',$edited,$class='form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["no_prk"], "no_prk", "NO PRK");
?>

<?php 
$from = UI::createTextArea('nama',$row['nama'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama Permintaan");
?>


<?php 
if($row['keterangan']===null)
	$row['keterangan'] = "Sudah termasuk PPN 10%";

$from = UI::createTextArea('keterangan',$row['keterangan'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["keterangan"], "keterangan", "Keterangan");
?>		

</div>
<div class="col-sm-6">
		
<?php 
$from = UI::createTextBox('tgl_dibutuhkan',$row['tgl_dibutuhkan'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_dibutuhkan"], "tgl_dibutuhkan", "Tgl. Dibutuhkan");
?>

<?php 
$from = UI::createSelect('id_status_pr',$mtstatusprarr,$row['id_status_pr'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_status_pr"], "id_status_pr", "Status PR");
?>

<?php 
$from = UI::createSelect('id_penyusun_tor',$penyusuntorarr,$row['id_penyusun_tor'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_penyusun_tor"], "id_penyusun_tor", "Penyusun TOR");
?>

<?php 
$from = UI::createSelect('id_menyetujui_tor',$menyetujuitorarr,$row['id_menyetujui_tor'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_menyetujui_tor"], "id_menyetujui_tor", "Menyetujui TOR");
?>

<?php 
/*$from = UI::createTextBox('tgl_pengerjaan',$row['tgl_pengerjaan'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_pengerjaan"], "tgl_pengerjaan", "Tgl. Mulai Pengerjaan");
?>

<?php 
$from = UI::createTextBox('tgl_selesai_pengerjaan',$row['tgl_selesai_pengerjaan'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_selesai_pengerjaan"], "tgl_selesai_pengerjaan", "Tgl. Selesai Pengerjaan");*/
?>

<?php 
$from = UI::createSelect('id_pemohon',$pemohonrarr,$row['id_pemohon'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_pemohon"], "id_pemohon", "Pemohon");
?>

<?php 
$from = UI::createSelect('id_menyetujui',$menyetujuirarr,$row['id_menyetujui'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_menyetujui"], "id_menyetujui", "Menyetujui PR");

$from = UI::createSelect('id_mengetahui',$mengetahuirarr,$row['id_mengetahui'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_mengetahui"], "id_mengetahui", "Mengetahui PR");


if(!$edited && !empty($rows)){
$from = "<a href='".site_url("panelbackend/pr_pr/go_print/$id_rab/$row[id_pr]")."' target='_blank' class='btn btn-info'>Print</a> ";

if($row['id_status_pr']<>5 && !empty($rowstgl))
	$from .= " <a href='javascript::void(0)' onclick='if(confirm(\"PR akan diajukan ke SCM dan di-input di ellipse. Apakah semua isian sudah benar ?.\")){goSubmit(\"push_pr\")}' class='btn btn-success'>Send to Ellipse</a>";

echo UI::createFormGroup($from);
}
?>
</div>
<?php 
if(!$edited){ ?>
<div class="row">
<div class="col-sm-12">
<?php
$is_finish=false;
$from = "<table class='table'><tr><th style='width:150px'>Tgl.</th><th>TOR</th><th>DMR</th><th>Ket.</th><th><th></th></th>";
if(!empty($rowstgl))
foreach($rowstgl as $i=>$r){


	if($this->post['act']=='edit_tgl' && $this->post['key']==$r['id_pr_tor']){
		$from .= "<tr><td>".UI::createTextBox('tgl',$r['tgl'],'10','10',true,$class='form-control datepicker',"style='width:100%'");
		$from .= "</td><td>".UI::createUpload("tor", $r['tor'], $page_ctrl, true, "tor");
		$from .= "</td><td>".UI::createUpload("dmr", $r['dmr'], $page_ctrl, true, "dmr");
		$from .= "</td><td>".UI::createSelect('jenis',array('1'=>'Draft','2'=>'Final'),$r['jenis'],true,$class='form-control ',"style='width:auto; max-width:100%;'");
		$from .= "</td><td>".UI::showButtonMode("save", null, true);
		$from .= "</td></tr>";
	}else{
		$from .= "<tr><td>".Eng2Ind($r['tgl']);
		$from .= "</td><td>".UI::createUpload("tor", $r['tor'], $page_ctrl, false, "tor");
		$from .= "</td><td>".UI::createUpload("dmr", $r['dmr'], $page_ctrl, false, "dmr");
		$from .= "</td><td>";
		if($r['jenis']=='2'){
			$from .= "Final";
			$is_finish = true;
		}else{
			$from .= "V.".($i+1);
		}

		$from .= "</td><td>";

		if($this->access_role['edit']){
			$from .= "<a href='javascript:void(0)' class='btn btn-success btn-xs' onclick='goSubmitValue(\"edit_tgl\", $r[id_pr_tor])'><span class='glyphicon glyphicon-pencil'></span></a>";
			$from .= "<a href='javascript:void(0)' class='btn btn-danger btn-xs' onclick='goSubmitValue(\"delete_tgl\", $r[id_pr_tor])'><span class='glyphicon glyphicon-remove'></span></a>";
		}
		$from .= "</td></tr>";
	}
}

if($this->post['act']=='add_tgl'){
		$from .= "<tr><td>".UI::createTextBox('tgl',$r['tgl'],'10','10',true,$class='form-control datepicker',"style='width:100px'");
		$from .= "</td><td>".UI::createUpload("tor", null, $page_ctrl, true, "tor");
		$from .= "</td><td>".UI::createUpload("dmr", null, $page_ctrl, true, "dmr");
		$from .= "</td><td>".UI::createSelect('jenis',array('1'=>'Draft','2'=>'Final'),$r['jenis'],true,$class='form-control ',"style='width:auto; max-width:100%;'");
		$from .= "</td><td>".UI::showButtonMode("save", null, true);
		$from .= "</td></tr>";
}

$from .= "</table>";

if(!$is_finish && !$this->post['act']){
	$from .= "<a href='javascript:void(0)' class='btn btn-success btn-xs' onclick='goSubmit(\"add_tgl\")'><span class='glyphicon glyphicon-plus'></span></a>";
}
echo UI::createFormGroup($from, $rules["tor"], "tor", "TOR : ", true);
?>
</div>
</div>
<?php 
}
?>
<hr/>
<?php
if(!empty($rows)){ 
echo "<table class='table table-hover'>";
echo "<thead><tr><th>Uraian</th><th>Jumlah</th><th>Satuan</th><th>Harga Satuan</th><th>Total Harga</th><th>Status</th></tr></thead>";
$total = 0;
foreach($rows as $r){ 
	$r['nilai'] = (float)$r['harga_satuan']*(float)$r['vol'];
	$total+=$r['nilai'];
	echo "<tr><td>";
	echo $r['uraian'];
	echo "</td><td style='text-align:right'>";
	echo $r['vol'];
	echo "</td><td>";
	echo $r['satuan'];
	echo "</td><td style='text-align:right'>";
	echo rupiah($r['harga_satuan'],2);
	echo "</td><td style='text-align:right'>";
	echo rupiah($r['nilai'],2);
	echo "</td><td>";
/*	echo $r['no_item_pr'];
	echo "</td><td>";*/
	echo "</td></tr>";
}

echo "<tr><td>";
echo "</td><td>";
echo "</td><td>";
echo "</td><td style='text-align:right'>Total</td><td style='text-align:right'>";
echo rupiah($total,2);
echo "</td><td>";
echo "</td></tr>";

echo "</table>";
}

if($this->access_role['edit'] && !$edited){
	echo UI::createExportImport();
}
?>
<div style="clear: both;"></div>
<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>