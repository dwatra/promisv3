<div class="row">
    <div class="col-sm-6">
        <h4>REALISASI MANPOWER</h4>
    </div>
    <div class="col-sm-6" style="text-align: right;">
    <a href="<?=site_url('panelbackend/rab_manpower/index/'.$id_rab)?>" class="btn btn-sm"><span class="
glyphicon glyphicon-chevron-left"></span> Rencana Manpower</a>
    <a href="<?=site_url('panelbackend/rab_manpower/index/'.$id_rab.'/curva')?>" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-signal"></i> Kurva</a>
    </div>

</div>
<div class="row">
<table class="table table-bordered" id="fixedcolumns">
    <thead>
        <tr>
            <th rowspan="2" style="width:120px">TEAM PROYEK</th>
            <th rowspan="2" style="width:120px">JABATAN PROYEK</th>
            <th rowspan="2" style="width: 5px">QTY</th>
            <th colspan="<?=$rowheader1['hmin']+$rowheader1['h']+$rowheader1['hplus']?>">QTY</th>
            <th rowspan="2">TOTAL WORK</th>
            <th rowspan="2">MAX</th>
            <th rowspan="2" style="width:120px">SUMBER PEGAWAI</th>
        </tr>
        <tr>
            <?php
            for($i=$rowheader1['hmin']; $i>0; $i--){
                echo "<th>H-$i</th>";
            }
            ?>

            <?php
            for($i=1; $i<=$rowheader1['h']; $i++){
                echo "<th>$i</th>";
            }
            ?>

            <?php
            for($i=1; $i<=$rowheader1['hplus']; $i++){
                echo "<th>H+$i</th>";
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $jj=array();
        $tot_max = array();
        $tot_jum = array();
        $jabatanarr = array();
        $ir=0;
        foreach($rows as $row){ $ir++ ?>
        <tr>
            <td><?=$mtteamproyekarr[$row['id_team_proyek']]?></td>
            <td><a href="<?=site_url("panelbackend/rab_manpower/detail/$rowheader2[id_rab]/$row[id_manpower]")?>"><?=$mtjabatanproyekarr[$row['id_jabatan_proyek']]?></a></td>
            <td style='text-align:center'><?=$row['jumlah']?></td>

            <?php
            $jabatanarr[$row['id_jabatan_proyek']] = $mtjabatanproyekarr[$row['id_jabatan_proyek']];

            $jum = 0;
            $max = 0;
            $ic = 0;
            for($i=$rowheader1['hmin']; $i>0; $i--){
                $ic++;
                $j = $rowdaysrealisasi[$row['id_manpower']]['hmin'.$i];
                $jj['hmin'.$i]+=(int)$j;

                echo "<td style='text-align:center' data-day='".'hmin'.$i."' data-id='".$row['id_manpower']."' class='col c_".$ir.'_'.$ic."'>$j</td>";
                $jum+=(int)$j;
                if($j>$max)
                    $max = $j;
            }
            ?>

            <?php
            for($i=1; $i<=$rowheader1['h']; $i++){
                $ic++;
                $j = $rowdaysrealisasi[$row['id_manpower']]['h'.$i];
                $jj['h'.$i]+=(int)$j;
                
                echo "<td style='text-align:center' data-day='".'h'.$i."' data-id='".$row['id_manpower']."' class='col c_".$ir.'_'.$ic."'>$j</td>";
                $jum+=(int)$j;
                if($j>$max)
                    $max = $j;
            }
            ?>

            <?php
            for($i=1; $i<=$rowheader1['hplus']; $i++){
                $ic++;
                $j = $rowdaysrealisasi[$row['id_manpower']]['hplus'.$i];
                $jj['hplus'.$i]+=(int)$j;

                echo "<td style='text-align:center' data-day='".'hplus'.$i."' data-id='".$row['id_manpower']."' class='col c_".$ir.'_'.$ic."'>$j</td>";
                $jum+=(int)$j;
                if($j>$max)
                    $max = $j;
            }
            ?>

            <td style='text-align:center'><?=$jum?></td>
            <td style='text-align:center'><?=$max?></td>
            <?php
            $tot_jum[$row['id_sumber_pegawai']]+=$jum;
            $tot_max[$row['id_sumber_pegawai']]+=$max;
            $tot_jabatan[$row['id_sumber_pegawai']][$row['id_jabatan_proyek']]+=$jum;
            $tjum+=$jum;
            $tmax+=$max;
            ?>
            <td style='text-align:center'><?=$mtsumberpegawaiarr[$row['id_sumber_pegawai']]?></a></td></td>
        </tr>
        <?php } ?>

        <tr>
            <td></td>
            <td></td>
            <td></td>

            <?php
            $jum = 0;
            $max = 0;
            for($i=$rowheader1['hmin']; $i>0; $i--){
                $j = $jj['hmin'.$i];
                echo "<td style='text-align:center'>$j</td>";
            }
            ?>

            <?php
            for($i=1; $i<=$rowheader1['h']; $i++){
                $j = $jj['h'.$i];
                echo "<td style='text-align:center'>$j</td>";
            }
            ?>

            <?php
            for($i=1; $i<=$rowheader1['hplus']; $i++){
                $j = $jj['hplus'.$i];
                echo "<td style='text-align:center'>$j</td>";
            }
            ?>

            <td style='text-align:center'><?=$tjum?></td>
            <td style='text-align:center'><?=$tmax?></td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>
<link href="<?=base_url()?>assets/css/fixedColumns.dataTables.min.css" rel="stylesheet">
<script src="<?=site_url("assets/js/jquery.dataTables.min.js")?>"></script>
<script src="<?=site_url("assets/js/dataTables.fixedColumns.min.js")?>"></script>



<script type="text/javascript">
$(document).ready(function() {
var table = $('#fixedcolumns').DataTable( {
    scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    ordering: false,
    searching:false,
    info:false,
    fixedColumns:   {
        leftColumns: 3,
    }
} );

$(".col").click(function(){
    if($(this).children("input").attr("type")!='text'){
        $(this).addClass("td");
        $(this).attr("data-val",$(this).html());
        $(this).html("<input class='colinput' onblur='colinput($(this).parent(\".td\"), parseInt($(this).val()))' onkeydown='keycol(this, event)' type='text' style='width:100%; text-align:center' value='"+$(this).html()+"'/>");
        var searchInput = $(this).children("input");
        var strLength = searchInput.val().length * 2;

        searchInput.focus();
        searchInput[0].setSelectionRange(strLength, strLength);
        searchInput.inputFilter(function(value) {return /^\d*$/.test(value);});
    }
});

} );

(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

function klikcol(th, cls, val){
    var el = $("."+cls);
    if(el.length!=0){
        $(el).click();
        colinput($(th).parent(".td"), val);
    }
}

function keycol(th, e){
    var tp = $(th).parent(".td");
    var cl = $(tp).attr("class");
    var ex = cl.split(" ")[1].split("_");
    var col = parseInt(ex[1]);
    var row = parseInt(ex[2]);

    if (e.key === "Escape") {
        $(tp).removeClass("td");
        $(tp).html($(tp).attr("data-val"));
        $(tp).removeAttr("data-val");
    }else {
        if (e.key === "Enter") {
            savecol(e, th);
            colinput(tp, parseInt($(th).val()));
        }else if(e.key === 'ArrowDown') {
            savecol(e, th);
            col = col+1;
            var cls = 'c_'+col+'_'+row;
            klikcol(th, cls, parseInt($(th).val()));
        }else if(e.key === 'ArrowUp') {
            savecol(e, th);
            col = col-1;
            var cls = 'c_'+col+'_'+row;
            klikcol(th, cls, parseInt($(th).val()));
        }else if(e.key === 'ArrowLeft') {
            savecol(e, th);
            row = row-1;
            var cls = 'c_'+col+'_'+row;
            klikcol(th, cls, parseInt($(th).val()));
        }else if(e.key === 'ArrowRight') {
            savecol(e, th);
            row = row+1;
            var cls = 'c_'+col+'_'+row;
            klikcol(th, cls, parseInt($(th).val()));
        }
    }

    return false;
};

function savecol(e, th){
    var tp = $(th).parent(".td");
    var id_manpower = tp.attr("data-id");
    var day = tp.attr("data-day");
    var val = $(th).val();
    var valb = $(tp).attr("data-val");

    if(val!=valb){
        $.ajax({
            method: "POST",
            url: "<?php echo site_url('panelbackend/rab_manpower/index/'.$id_rab.'/updaterealisasi') ?>",
            data: { id_manpower:id_manpower, day:day, jumlah: val},
            success: function(respon){
                var result = JSON.parse(respon);
                if(result.success==true){
                    colinput($(th).parent(".td"), result.jumlah)
                }
            }
        });
    }
}

function colinput(tp, val){
    $(tp).removeClass("td");
    $(tp).html(val);
    $(tp).removeAttr("data-val");
}

</script>

<style type="text/css">
    .table td, .table th {
        font-size: 11px;
        padding: 3px !important;
    }

    .table .td{
        padding: 0px !important;
    }
        table.dataTable {
    clear: both;
    margin-bottom: 0px !important;
    max-width: none !important;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}   
</style>