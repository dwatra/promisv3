<div style="text-align: left; margin-bottom: 15px;">
<?php 
echo "<b>POS ANGGARAN :</b> ". UI::createSelect('id_pos_anggaran',$mtposanggaranarr,$_SESSION[SESSION_APP]['id_pos_anggaran'],true,'form-control ',"style='width:auto; max-width:100%;' onchange='goSubmit(\"set_value\")'");
?>
<div style="clear: both;"></div>
</div>
<?php
if(!empty($rows)){ 
echo "<table class='table table-hover  tree-table'>";
echo "<thead><tr><th>Uraian</th><th width='220px'></th></tr></thead>";
foreach($rows as $r){ 
	if($nilai[$r['id_rab_detail']])
		$r['nilai'] = $nilai[$r['id_rab_detail']];

	if($r['sumber_nilai']<>1 && $r['sumber_nilai']<>3){
		echo "<tr data-tt-id='$r[id_rab_detail]' data-tt-parent-id='$r[id_rab_detail_parent]'><td>";
		echo "<a href='".site_url("panelbackend/rab_realisasi_admin/realisasi/$r[id_rab_detail]")."'>".$r['uraian']."</a>";
		echo "</td><td align='right'>";
		echo "<a href='".site_url("panelbackend/rab_realisasi_admin/add/$r[id_rab_detail]")."' class='btn btn-info btn-xs'>Add Realisasi</a>";
		echo "</td></tr>";
	}
	elseif($r['sumber_nilai']==3){
		echo "<tr data-tt-id='$r[id_rab_detail]' data-tt-parent-id='$r[id_rab_detail_parent]'><td>";
		echo "<a href='".site_url("panelbackend/rab_realisasi_admin/realisasi/$r[id_rab_detail]")."'>".$r['uraian']."</a>";
		echo "</td><td align='right' class='import-small'>";
		echo UI::createExportImport("import".$r['id_rab_detail'], null, null, true, $r['id_rab_detail'],"<li>Abaikan kolom id_jasa_material dan id_rab_detail.</li><li>Apabila item tidak ada dalam daftar/tidak sesuai bisa dihapus dan ditambah sendiri.</li>");
		echo "</td></tr>";
	}else{
		if(!$_SESSION[SESSION_APP]['id_pos_anggaran']){
			echo "<tr data-tt-id='$r[id_rab_detail]' data-tt-parent-id='$r[id_rab_detail_parent]'><td colspan='2'>";
			echo "<b>".$r['uraian']."</b>";
			echo "</td></tr>";
		}
	}
} 
} ?>
<link href="<?=base_url()?>assets/css/jquery.treetable.theme.default.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/jquery.treetable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/js/jquery.treetable.js"></script>