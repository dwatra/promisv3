<div class="col-sm-6">
<?php 
$from = UI::createUploadMultiple("beforeoh___".$id_proyek, $row["beforeoh___".$id_proyek], $page_ctrl, $edited, "beforeoh___".$id_proyek,"$id_proyek/");
echo UI::createFormGroup($from, $rules["beforeoh___".$id_proyek], "beforeoh___".$id_proyek, "Assessment before OH");
?>
<?php 
$from = UI::createUploadMultiple("kajianrisiko___".$id_proyek, $row["kajianrisiko___".$id_proyek], $page_ctrl, $edited, "kajianrisiko___".$id_proyek,"$id_proyek/");
echo UI::createFormGroup($from, $rules["kajianrisiko___".$id_proyek], "kajianrisiko___".$id_proyek, "Kajian Risiko");
?>
<?php 
$from = UI::createUploadMultiple("rekomendasioh___".$id_proyek, $row["rekomendasioh___".$id_proyek], $page_ctrl, $edited, "rekomendasioh___".$id_proyek,"$id_proyek/");
echo UI::createFormGroup($from, $rules["rekomendasioh___".$id_proyek], "rekomendasioh___".$id_proyek, "Rekomendasi OH sebelumnya");
?>
</div>

<div class="col-sm-6">
</div>


<?php if(count($row) > 0){ ?>
<br/>
<div class="row">
<table align="right" class="table table-bordered">
    <thead>
    <tr>
        <th style="text-align:center">No</th>
        <th style="text-align:center">File</th>
        <th style="text-align:center">Tgl Upload</th>
        <th style="text-align:center">Oleh</th>
    </tr>
    </thead>
    <tbody style="">
    	<?php $no = 1; ?>
    	<?php foreach($row as $r){ ?>
    		<tr>
	            <td style="text-align:center"><?=$no++?></td>
	            <td style="text-align:center"><?=$r['name'][0]?></td>
	            <td style="text-align:center"><?=$r['created_date'][0]?></td>
	            <td style="text-align:center"><?=$r['created_by_desc'][0]?></td>
	        </tr>
    	<?php } ?>
    </tbody>
</table>
</div>
<?php } ?>