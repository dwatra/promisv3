<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th width="10px">No.</th>
			<th width="10px">KODE BIAYA</th>
			<th>URAIAN BIAYA</th>
			<th>JUMLAH ANGGARAN</th>
			<th>POS ANGGARAN</th>
			<th>KETERANGAN</th>
			<th width="1px"></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$mtposanggaranarr[''] = '';
		$total = 0;
		$no=1;
		if(count($rows)){
			foreach($rows as $r){ 
				if($r['level']>2 && !$r['id_pos_anggaran'])
					continue;
			?>
		<tr>
			<td align="center">
				<b><?php if($r['level']<=1){echo $no++;} ?></b>
			</td>
			<td>
				<?php if($r['id_pos_anggaran']){
					echo $r['kode_biaya'];
				}else{ ?>
					<?=$r['kode_biaya']?>
				<?php } ?>
			</td>
			<td><a href='<?=site_url("panelbackend/rab_rab_niaga/detail/$id_rab_niaga/$r[id_rab_detail]")?>'>
					<?=$r['uraian']?>
			</a></td>
			<td style="text-align: right;">
				<?php if($r['id_pos_anggaran']){ 
					$total+=(float)$r['nilai_satuan'];
					?>
					<?=rupiah($jumlah=$r['nilai_satuan'],2)?>
				<?php } ?>
					</td>
			<td style="text-align: center;">

					<?=$mtposanggaranarr[$r['id_pos_anggaran']]?>
					</td>
			<td><?=$r['keterangan']?></td>
			<td><?php
			$add = array();
			if($r['sumber_nilai']==1){
				$add = array('<li><a href="'.site_url("panelbackend/rab_rab_niaga/add/$id_rab_niaga/$r[$pk]").'" class="waves-effect "><span class="glyphicon glyphicon-share"></span> Add Sub</a> </li>');
			}
			echo UI::showMenuMode('inlist', $r[$pk],false,'','',null,null,$add);
			?></td>
		</tr>
	<?php } ?>
	<tr><td colspan="3" style="text-align: right;"><b>JUMLAH</b></td><td style="text-align: right;"><?=rupiah($total)?></td><td></td><td><?=rupiah($totalrealisasi)?></td><td></td></tr>
<?php }else{ ?>
		<tr><td colspan="4"><i>Belum ada data</i></td></tr>
	<?php } ?>
	</tbody>
</table>
<br/>
<?=UI::getButton("add",null,null,'btn-sm')?>

                <?php if($last_versi==$rowheader3['id_rab']){ ?>
                  <button type="button" class="btn waves-effect btn-sm btn-danger" onclick="goSubmit('set_pekerjaan_baru')"><span class="glyphicon glyphicon-add"></span> Jadikan Pekerjaan Baru</button>
                <?php } ?>
<style type="text/css">
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		    padding: 5px !important;
		}
</style>