<table class="table table-striped table-bordered table-hover dataTable">
    <thead>
		<tr>
	        <th style="width:1px">#</th>
	        <th style='max-width:auto; cursor:pointer;'>Pemberi Pekerjaan</th>
	        <th style='max-width:auto; cursor:pointer;'>Nama Proyek</th>
	        <th style='max-width:auto; cursor:pointer;'>No. SP3</th>
	        <th style='max-width:auto; cursor:pointer;'>Tgl. SP3</th>
	        <th style='max-width:auto; cursor:pointer;'>No. Kontrak</th>
	        <th style='max-width:auto; cursor:pointer;'>Tgl. Kontrak</th>
	        <th style='max-width:auto; cursor:pointer;'>Nilai HPP</th>
	        <th style='max-width:auto; cursor:pointer;'>No. PRK</th>
	        <th style='max-width:auto; cursor:pointer;'>Rencana Pelaksanaan</th>
	        <th style='max-width:auto; cursor:pointer;'>Manager Proyek</th>
	        <th style='max-width:auto; cursor:pointer;'>Tgl. Pengerjaan RAB</th>
	        <th style='max-width:auto; cursor:pointer;'>Durasi</th>
	        <th style="width:10px"></th>
		</tr>
    </thead>
    <tbody>
    	<tr>
    		<td style="text-align: center;">1</td>
    		<td>PT Pembangkit Jawa Bali</td>
    		<td>
    			<a href="<?=site_url("panelbackend/sp3/detail/1")?>">
				Serious Inspection PLTU Rembang Unit 10 Tahun 2018
				</a>
			</td>
    		<td>-</td>
    		<td>-</td>
    		<td>-</td>
    		<td>-</td>
    		<td>-</td>
    		<td style="background: green; color: #fff;">00018071</td>
    		<td>03 April 2018 - 29 Mei 2018</td>
    		<td>Igit W</td>
    		<td>03 Januari 2018 - 09 Januari 2018</td>
    		<td style="background: blue; color: #fff;">6 Hari</td>
    		<td style='text-align:left'>
    			<div class="dropdown">
					<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Menu
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2" style="min-width: 10px;">
						<li>
							<a style="padding: 3px 20px; font-size: 12px" href="<?=site_url("panelbackend/sp3/edit/1")?>">
							<span class="glyphicon glyphicon-edit"></span> Edit
							</a>
						</li>
						<li>
							<a style="padding: 3px 20px; font-size: 12px" href="#">
							<span class="glyphicon glyphicon-remove"></span> Delete
							</a>
						</li>
					</ul>
				</div>
    		</td>
    	</tr>
    </tbody>
</table>

<style type="text/css">
.table-bordered > thead > tr > th {
    text-align: center !important;
    padding: 0px 5px !important;
}
</style>