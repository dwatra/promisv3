
<div class="dropdown pull-right" style="margin-top: 7px; position: absolute; right: 15px; z-index: 1000">
<?php if($this->access_role["edit"]){ ?>
  <a class="dropdown-toggle" href="javascrit:void(0)" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
    <span class="glyphicon glyphicon-option-vertical"></span>
  </a>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=site_url("panelbackend/sp3/add")?>">Add</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=site_url("panelbackend/sp3/edit/".$rowheader1['id_formulir'])?>">Edit</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="deleteHps()">Delete</a></li>
  </ul>
  <script type="text/javascript">
    function deleteHps(){
      if(confirm('Apakah Anda akan menghapus HPS ini ?'))
        window.location = "<?=site_url("panelbackend/sp3/delete/".$rowheader1['id_formulir'])?>";
      else
        return false;
    }
  </script>
<?php } ?>
</div>

<div class="col-sm-6" style="margin-top: 7px;">

<?php 
$from = UI::createSelect('id_scm_pr',array('1'=>'PT Pembangkit Jawa Bali'),1,$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_scm_pr"], "id_scm_pr", "Pemberi Pekerjaan	");
?>

<?php 
$from = UI::createTextArea('nama','Serious Inspection PLTU Rembang Unit 10 Tahun 2018','4','',$edited,$class='form-control ',"style='width:100%'");
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama Proyek");
?>


<?php 
$from = "<div class='col-sm-6 no-padding no-margin'>".
UI::createTextBox('no_hps',$row['no_hps'],'20','20',$edited,$class='form-control ',"style='width:140px'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Tgl.</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_mulai_hps',$row['tgl_mulai_hps'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'")
."</div>";
echo UI::createFormGroup($from, $rules["no_hps"], "no_hps", "NO SP3");
?>

<?php 
$from = UI::createUploadMultiple("filescan", $row['filescan'], $page_ctrl, $edited, "filescan");
echo UI::createFormGroup($from, $rules["filescan"], "filescan", "Lampiran SP3");
?>


<?php 
$from = "<div class='col-sm-6 no-padding no-margin'>".
UI::createTextBox('no_hps',$row['no_hps'],'20','20',$edited,$class='form-control ',"style='width:140px'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>Tgl.</label><div class='col-sm-4 no-padding no-margin'>".
UI::createTextBox('tgl_mulai_hps',$row['tgl_mulai_hps'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'")
."</div>";
echo UI::createFormGroup($from, $rules["no_hps"], "no_hps", "NO Kontrak");
?>

<?php 
$from = UI::createUploadMultiple("filescan", $row['filescan'], $page_ctrl, $edited, "filescan");
echo UI::createFormGroup($from, $rules["filescan"], "filescan", "Lampiran Kontrak");
?>

</div>
<div class="col-sm-6" style="margin-top: 7px;">



<?php 
$from = UI::createTextBox('no_hps',$row['no_hps'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no_hps"], "no_hps", "Nilai HPP");
?>

<?php 
$from = UI::createTextBox('no_hps',$row['no_hps'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no_hps"], "no_hps", "No. PRK");
?>

<?php 
$from = "<div class='col-sm-5 no-padding no-margin'>".
UI::createTextBox('no_hps','03-04-2018','20','20',$edited,$class='form-control datepicker',"style='width:100px'")
."</div><label class='col-sm-2 control-label' style='padding-left: 0px;'>sd</label><div class='col-sm-5 no-padding no-margin'>".
UI::createTextBox('tgl_mulai_hps','29-04-2018','10','10',$edited,$class='form-control datepicker',"style='width:100px'")
."</div>";
echo UI::createFormGroup($from, $rules["no_hps"], "no_hps", "Rencana Pelaksanaan");
?>

<?php 
$from = UI::createSelect('id_scm_pr',array('1'=>'Igit W'),1,$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_scm_pr"], "id_scm_pr", "Manager Proyek");
?>

<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>

</div>