<div class="col-sm-6">

<?php
$from = UI::createSelect('id_customer',$mtcustomerarr,$row['id_customer'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_customer"], "id_customer", "Customer");
?>
<?php
$from = UI::createSelect('id_warehouse',$mtmtwarehousearr,$row['id_warehouse'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_warehouse"], "id_warehouse", "Warehouse");
?>

<?php //echo"<pre>";print_r($unit_tdb);exit; ?>

<?php
$from = UI::createSelect('id_tdb_unit',$unit_tdb ,$row['id_tdb_unit'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_tdb_unit"], "id_tdb_unit", "Tdb");
?>



<?php
$from = UI::createTextBox('pic_customer',$row['pic_customer'],'','',$edited, 'form-control',"style='width:100%'");
echo UI::createFormGroup($from, $rules["pic_customer"], "pic_customer", "PIC Customer");
?>


<?php
$from = UI::createTextBox('jabatan_pic_customer',$row['jabatan_pic_customer'],'','',$edited, 'form-control',"style='width:100%'");
echo UI::createFormGroup($from, $rules["jabatan_pic_customer"], "jabatan_pic_customer", "Jabatan PIC");
?>


<?php //createCheckBox($nameid,$valuecontrol='',$value='',$label='label',$edit=true,$class='',$add='')
$from = UI::createCheckBox('is_pln',1,$row['is_pln'], "PLN ?",$edited,null,"onclick='goSubmit(\"set_value\")'");
echo UI::createFormGroup($from, $rules["is_pln"], "is_pln", null);
?>

<?php
if($row['is_pln']){
	$from = UI::createTextBox('pic_pln',$row['pic_pln'],'','',$edited, 'form-control',"style='width:100%'");
	echo UI::createFormGroup($from, $rules["pic_pln"], "pic_pln", "PIC PLN");

	$from = UI::createTextBox('jabatan_pic_pln',$row['jabatan_pic_pln'],'','',$edited, 'form-control',"style='width:100%'");
	echo UI::createFormGroup($from, $rules["jabatan_pic_pln"], "jabatan_pic_pln", "Jabatan");
}
?>




<?php
$from = UI::createSelect('id_tipe_proyek',$mttipeproyekarr,$row['id_tipe_proyek'],$edited,$class='form-control ',"style='width:auto; max-width:100%;'");
echo UI::createFormGroup($from, $rules["id_tipe_proyek"], "id_tipe_proyek", "Tipe proyek");
?>

<?php
$from = UI::createTextArea('nama_proyek',$row['nama_proyek'],'','',$edited,$class='form-control',"");
echo UI::createFormGroup($from, $rules["nama_proyek"], "nama_proyek", "Nama Proyek");
?>

<?php

$from = UI::createSelect('id_pic',$mtpegawaiarr,$row['id_pic'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_pic"], "id_pic", "Manager Proyek");
if($row['id_proyek']){
echo UI::createFormGroup("<a href='javascript:void(0)' onclick='goSubmit(\"penunjukan_pm\")'><span class='glyphicon glyphicon-download'></span> Template Penunjukan PM</a>");
}

$from = UI::createSelect('id_rendal_proyek',$mtpegawaiarr,$row['id_rendal_proyek'],$edited,$class='form-control ',"style='width:100%;'  data-ajax--url=\"".site_url('panelbackend/ajax/listpegawai')."\" data-ajax--data-type=\"json\"");
echo UI::createFormGroup($from, $rules["id_rendal_proyek"], "id_rendal_proyek", "Rendal Proyek");
?>
</div>
<div class="col-sm-6">

<?php
$from = UI::createSelect('id_status_proyek',$mtstatusproyekarr,$row['id_status_proyek'],$edited,$class='form-control ',"style='width:auto; max-width:100%;' onchange='goSubmit(\"set_value\")'");
echo UI::createFormGroup($from, $rules["id_status_proyek"], "id_status_proyek", "Status");
?>

<?php
$from = UI::createUploadMultiple("file", $row['file'], $page_ctrl, $edited, "file");
echo UI::createFormGroup($from, $rules["file"], "kontrak", "Berkas Proyek");

if(!$edited){
$is_finish=false;
$from = "<table class='table'><tr><th>Tgl. Mulai</th><th>Tgl. Selesai</th><th>Ket.</th><th><th></th></th>";
foreach($rowstgl as $i=>$r){


	if($this->post['act']=='edit_tgl' && $this->post['key']==$r['id_proyek_tgl']){
		$from .= "<tr><td>".UI::createTextBox('tgl_mulai',$r['tgl_mulai'],'10','10',true,$class='form-control datepicker',"style='width:100px'");
		$from .= "</td><td>".UI::createTextBox('tgl_selesai',$r['tgl_selesai'],'10','10',true,$class='form-control datepicker',"style='width:100px'");
		$from .= "</td><td>".UI::createSelect('jenis',array('1'=>'Rencana','2'=>'Realisasi'),$r['jenis'],true,$class='form-control ',"style='width:auto; max-width:100%;'");
		$from .= "</td><td>".UI::showButtonMode("save", null, true);
		$from .= "</td></tr>";
	}else{
		$from .= "<tr><td>".Eng2Ind($r['tgl_mulai'])."</td><td>".Eng2Ind($r['tgl_selesai'])."</td><td>";
		if($r['jenis']=='2'){
			$from .= "Realisasi";
			$is_finish = true;
		}else{
			$from .= "V.".($i+1);
		}
		$from .= "</td><td>";
		if($this->access_role['edit']){
			$from .= "<a href='javascript:void(0)' class='btn btn-success btn-xs' onclick='goSubmitValue(\"edit_tgl\", $r[id_proyek_tgl])'><span class='glyphicon glyphicon-pencil'></span></a>";
			$from .= "<a href='javascript:void(0)' class='btn btn-danger btn-xs' onclick='goSubmitValue(\"delete_tgl\", $r[id_proyek_tgl])'><span class='glyphicon glyphicon-remove'></span></a>";
		}
		$from .= "</td></tr>";
	}
}

if($this->post['act']=='add_tgl'){
		$from .= "<tr><td>".UI::createTextBox('tgl_mulai',$r['tgl_mulai'],'10','10',true,$class='form-control datepicker',"style='width:100px'");
		$from .= "</td><td>".UI::createTextBox('tgl_selesai',$r['tgl_selesai'],'10','10',true,$class='form-control datepicker',"style='width:100px'");
		$from .= "</td><td>".UI::createSelect('jenis',array('1'=>'Rencana','2'=>'Realisasi'),$r['jenis'],true,$class='form-control ',"style='width:auto; max-width:100%;'");
		$from .= "</td><td>".UI::showButtonMode("save", null, true);
		$from .= "</td></tr>";
}

$from .= "</table>";

if(!$is_finish && !$this->post['act']){
	$from .= "<a href='javascript:void(0)' class='btn btn-success btn-xs' onclick='goSubmit(\"add_tgl\")'><span class='glyphicon glyphicon-plus'></span></a>";
}
echo UI::createFormGroup($from, $rules["pelaksanaan"], "pelaksanaan", "Tgl. Pelaksanaan");
}
?>





<?php
/*$from = UI::createTextBox('tgl_rencana_mulai',$row['tgl_rencana_mulai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_rencana_mulai"], "tgl_rencana_mulai", "Tgl. Rencana Mulai");
?>

<?php
$from = UI::createTextBox('tgl_rencana_selesai',$row['tgl_rencana_selesai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_rencana_selesai"], "tgl_rencana_selesai", "Tgl. Rencana Selesai");

if($row['id_status_proyek']=='1'){

$from = UI::createTextBox('tgl_realisasi_mulai',$row['tgl_realisasi_mulai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_realisasi_mulai"], "tgl_realisasi_mulai", "Tgl. Realisasi Mulai");
?>

<?php
$from = UI::createTextBox('tgl_realisasi_selesai',$row['tgl_realisasi_selesai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_realisasi_selesai"], "tgl_realisasi_selesai", "Tgl. Realisasi Selesai");
}*/
?>

<?php
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>
