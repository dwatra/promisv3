<div class="row">
<div class="col-sm-12">
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th width="10px">No</th>
			<th>Kriteria Kerja</th>
			<th>Satuan</th>
			<th width="100px">Bobot</th>
			<th>Target</th>
			<th>Realisasi</th>
		</tr>
	</thead>
	<?php 
	$total_bobot = 0;
	$no=1; foreach($kriteriaarr as $r){ 
	$total_bobot += (float)$row['nilai'][$r['id_kriteria_sla']][1];
	?>
	<tr>
		<td align="center"><?=$no++?></td>
		<td><?=$r['nama']?></td>
		<td><?=$r['satuan']?></td>
		<td style="padding:0px 7px !important; text-align: right;">
			<?=UI::createTextNumber("nilai[$r[id_kriteria_sla]][1]",$row['nilai'][$r['id_kriteria_sla']][1],'3','3',$edited,'form-control',"style='text-align:right; border:none;' placeholder='Bobot...'")?>
		</td>
		<td style="padding:0px 7px !important; text-align: right;">
			<?=UI::createTextBox("nilai[$r[id_kriteria_sla]][2]",$row['nilai'][$r['id_kriteria_sla']][2],'10','10',$edited,'form-control rupiah',"style='text-align:right; border:none;' placeholder='Target...'")?>
		</td>
		<td style="padding:0px 7px !important; text-align: right;">
			<?=UI::createTextBox("nilai[$r[id_kriteria_sla]][3]",$row['nilai'][$r['id_kriteria_sla']][3],'10','10',$edited,'form-control rupiah',"style='text-align:right; border:none;' placeholder='Realisasi...'")?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan="3" align="center"><b>Total</b></td>
		<td style="padding:0px 7px !important;  text-align: right;"><div class="read_detail"><?=rupiah($total_bobot)?></div></td>
		<td style="padding:0px 7px !important; text-align: right;"></td>
		<td style="padding:0px 7px !important; text-align: right;"></td>
	</tr>
</table>

</div>
</div>
<div style="clear: both;"></div>
<br/>
<div style="text-align: right;">
<?php 
$from = UI::showButtonMode("save", null, $edited);
echo UI::createFormGroup($from);
?>
</div>