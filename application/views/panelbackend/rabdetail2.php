<ol class="breadcrumb">
  <li><a href="<?=site_url("panelbackend/rab/detail")?>">RAB</a></li>
  <li><a href="<?=site_url("panelbackend/rab/detail1")?>">MATERIAL SPAREPART DAN PENDUKUNG PROYEK</a></li>
  <li class="active">MATERIAL GENERAL/CONSUMABLE</li>
</ol>

<table class="table table-bordered">
	<thead>
		<tr>
			<th width="10px">No.</th>
			<th>DESKRIPSI</th>
			<th>VOL</th>
			<th>SATUAN</th>
			<th>HARGA SATUAN</th>
			<th>JUMLAH</th>
			<th>KETERANGAN</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>ADHESIVE, 20GR, TRANSPARANT LOCTITE 495</td>
			<td align="right">45</td>
			<td align="center">TUBE</td>
			<td align="right">79.500</td>
			<td align="right">3.577.500</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>ADHESIVE, 250C, 100gr/Tube THREE BOND 1211</td>
			<td align="right">60</td>
			<td align="center">TUBE</td>
			<td align="right">345.500</td>
			<td align="right">20.730.000</td>
			<td></td>
		</tr>
		<tr>
			<td>3</td>
			<td>ADHESIVE, PLASTIC STEEL, DEXTONE DEXTONE</td>
			<td align="right">10</td>
			<td align="center">SET</td>
			<td align="right">13.800</td>
			<td align="right">138.000</td>
			<td></td>
		</tr>
		<tr>
			<td>4</td>
			<td>ADHESIVE, REDDISH BROWN, VISCOSITY 1000(PA-S)</td>
			<td align="right">45</td>
			<td align="center">TUBE</td>
			<td align="right">321.500</td>
			<td align="right">14.467.500</td>
			<td></td>
		</tr>
		<tr>
			<td>5</td>
			<td>BAG, PLASTIC 50KG</td>
			<td align="right">2000</td>
			<td align="center">EA</td>
			<td align="right">2.400</td>
			<td align="right">4.800.000</td>
			<td></td>
		</tr>
		<tr>
			<td>6</td>
			<td>BATTERY, NONRECHARGEABLE, AA, CYLINDER, 1.5V</td>
			<td align="right">200</td>
			<td align="center">EA</td>
			<td align="right">6.900</td>
			<td align="right">1.380.000</td>
			<td></td>
		</tr>
		<tr>
			<td>7</td>
			<td>BATTERY, NONRECHARGEABLE, AAA, CYLINDER, 1.5V</td>
			<td align="right">200</td>
			<td align="center">EA</td>
			<td align="right">7.300</td>
			<td align="right">1.460.000</td>
			<td></td>
		</tr>
		<tr>
			<td>8</td>
			<td>BATTERY, NONRECHARGEABLE, BIG SIZE, TYPE:R05/UN</td>
			<td align="right">200</td>
			<td align="center">EA</td>
			<td align="right">24.750</td>
			<td align="right">5.950.000</td>
			<td></td>
		</tr>
		<tr>
			<td>9</td>
			<td>BATTERY, NONRECHARGEABLE, SQUARE, 9 VDC</td>
			<td align="right">160</td>
			<td align="center">EA</td>
			<td align="right">29.500</td>
			<td align="right">4.720.000</td>
			<td></td>
		</tr>
		<tr>
			<td>10</td>
			<td>BATTERY, NONRECHARGEABLE, BIG SIZE, DRY CELL, 1.5V</td>
			<td align="right">700</td>
			<td align="center">EA</td>
			<td align="right">7.000</td>
			<td align="right">4.900.000</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="5" align="right"><b>Total</b></td>
			<td align="right"><b>1.352.830.200</b></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="5" align="right"><b>Total + PPN 10%</b></td>
			<td align="right"><b>1.488.113.220</b></td>
			<td></td>
		</tr>
	</tbody>
</table>