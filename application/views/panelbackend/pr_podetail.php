<div class="col-sm-6">
<?php 
$from = UI::createTextBox('nama_proyek',"<a href='".site_url("panelbackend/rab_proyek/detail/$proyek[id_proyek]")."' target='_BLANK'>".$proyek['nama_proyek']."</a>",'20','20',false);
echo UI::createFormGroup($from, $rules["nama_proyek"], "nama_proyek", "Proyek");
$from = UI::createTextBox('nama_pekerjaan',"<a href='".site_url("panelbackend/rab_pekerjaan/detail/$pekerjaan[id_proyek]/$pekerjaan[id_pekerjaan]")."' target='_BLANK'>".$pekerjaan['nama_pekerjaan']."</a>",'20','20',false);
echo UI::createFormGroup($from, $rules["nama_pekerjaan"], "nama_pekerjaan", "Pekerjaan");
$from = UI::createTextBox('no_desc',$rowheader['no_desc'],'20','20',false);
echo UI::createFormGroup($from, $rules["no_desc"], "no_desc", "No. PR");
$from = UI::createTextBox('nama',$rowheader['nama'],'20','20',false);
echo UI::createFormGroup($from, $rules["nama"], "nama", "Nama Jasa/Barang");

$from = UI::createTextBox('nama_suplier',$row['nama_suplier'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["nama_suplier"], "nama_suplier", "Nama Suplier");

$from = UI::createTextBox('no_spk',$row['no_spk'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no_spk"], "no_spk", "NO SPK");

$from = UI::createTextBox('tgl_spk',$row['tgl_spk'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_spk"], "tgl_spk", "Tgl. SPK");

$from = UI::createUploadMultiple("spk", $row['spk'], $page_ctrl, $edited, "spk");
echo UI::createFormGroup($from, $rules["spk"], "spk", "SPK");
?>

<?php 

$from = UI::createTextBox('tgl_spmk',$row['tgl_spmk'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_spmk"], "tgl_spmk", "Tgl. SPMK");

$from = UI::createTextBox('no_spmk',$row['no_spmk'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no_spmk"], "no_spmk", "NO SPMK");

if($row['id_po']){
echo UI::createFormGroup("<a href='javascript:void(0)' onclick='goSubmit(\"spmk\")'><span class='glyphicon glyphicon-download'></span> Template SPMK</a>");
}

$from = UI::createUploadMultiple("spmk", $row['spmk'], $page_ctrl, $edited, "spmk");
echo UI::createFormGroup($from, $rules["spmk"], "spmk", "SPMK");
?>


</div>
<div class="col-sm-6">
				
<?php 
$from = UI::createTextBox('no_bapb',$row['no_bapb'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no_bapb"], "no_bapb", "NO BAPB");

$from = UI::createUploadMultiple("bapb", $row['bapb'], $page_ctrl, $edited, "bapb");
echo UI::createFormGroup($from, $rules["bapb"], "bapb", "BAPB");
?>

<?php 
$from = UI::createTextBox('no_bapp',$row['no_bapp'],'20','20',$edited,$class='form-control ',"style='width:200px'");
echo UI::createFormGroup($from, $rules["no_bapp"], "no_bapp", "NO BAPP");

$from = UI::createUploadMultiple("bapp", $row['bapp'], $page_ctrl, $edited, "bapp");
echo UI::createFormGroup($from, $rules["bapp"], "bapp", "BAPP");
?>


<?php 
$from = UI::createTextBox('tgl_mulai',$row['tgl_mulai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_mulai"], "tgl_mulai", "Tgl. Mulai");
?>

<?php 
$from = UI::createTextBox('tgl_selesai',$row['tgl_selesai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_selesai"], "tgl_selesai", "Tgl. Selesai");
?>

<?php 
$from = UI::createTextBox('tgl_real_selesai',$row['tgl_real_selesai'],'10','10',$edited,$class='form-control datepicker',"style='width:100px'");
echo UI::createFormGroup($from, $rules["tgl_real_selesai"], "tgl_real_selesai", "Tgl. Real Selesai");
?>

</div>


<hr/>
<?php
if(!empty($rows)){ 
echo "<table class='table table-hover table-bordered'>";
echo "<thead><tr><th>Uraian</th><th>Tgl. Mulai Kerja</th><th>Jumlah</th><th>Satuan</th><th>Harga Satuan</th><th>Total Harga</th></tr></thead>";
$total = 0;
$no=1;
foreach($rows as $r){ 
	if(!$edited && !$r['id_po'])
		continue;

	$r['nilai'] = (float)$r['harga_satuan']*(float)$r['vol'];
	$total+=$r['nilai'];
	echo "<tr><td>";
    echo UI::createCheckBox("id_pr_detail[".$r['id_pr_detail']."]",$r['id_pr_detail'],($r['id_po'] or !$row['id_po']?$r['id_pr_detail']:null),$r['uraian'], $edited);
	echo "</td><td style='position:relative'>";
	echo UI::createTextBox("tgl_mulai_kerja[".$r['id_pr_detail']."]",$r['tgl_mulai_kerja'],'10','10',$edited,'form-control datepicker',"style='width:100%'");
	echo "</td><td style='text-align:right'>";
	echo $r['vol'];
	echo "</td><td>";
	echo $r['satuan'];
	echo "</td><td style='text-align:right'>";
	echo rupiah($r['harga_satuan'],2);
	echo "</td><td style='text-align:right'>";
	echo rupiah($r['nilai'],2);
	echo "</td></tr>";
}

echo "</table>";
}
echo "<br/>";
echo UI::showButtonMode("save", null, $edited);?>