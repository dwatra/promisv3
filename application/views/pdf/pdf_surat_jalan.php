<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .text-white {
            color: white;
        }
    </style>
</head>
<body>

    <?php if($person){ ?>
    <?php foreach ($person as $x): ?>
    <?php if ($x->pic_name != null): ?>


    <table style="width: 100%;" border="1">
        <tr>
            <td>
                <center>
                    <img width="50" src="<?php echo base_url('assets/img/akuarip.jpg') ?>" alt="">
                </center>
            </td>
            <td>
                <center>
                    <table style="text-align: center;border-collapse: collapse;width: 100%;border:none;font-weight: bold;" border="1">
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td>PT PEMBANGKITAN TENAGA LISTRIK JAWA-BALI SERVICES</td>
                        </tr>
                        <tr>
                            <td>MANAJEMEN TOOLS & MATERIAL</td>
                        </tr>
                        <tr>
                            <td>Jl. Raya Juanda no. 17</td>
                        </tr>
                        <tr>
                            <td>Sidoarjo, Jawa Timur</td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                    </table>
                </center>
            </td>
            <td>
                <center>
                    <img src="" alt="">
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center;background: #4A7182;" class="text-white">
                SURAT JALAN TRUCK TOOLS KELUAR
            </td>
        </tr>
            <div style="padding: 30px;font-family: arial;">
                <table>
                    <tr>
                        <td colspan="3">Diberikan kepada : <?php echo $x->pic_name ?></td>
                    </tr>
                    <tr>
                        <td colspan="3"><br></td>
                    </tr>
                    <tr>
                        <td>Nama (Driver)</td>
                        <td>:</td>
                        <td><b><?php echo strtoupper($general['pic']) ?></b></td>
                    </tr>
                    <tr>
                        <td>Perusahaan</td>
                        <td>:</td>
                        <td><b>PT PJB SERVICES</b></td>
                    </tr>
                    <tr>
                        <td>Tujuan</td>
                        <td>:</td>
                        <td><?php echo strtoupper($proyek['lokasi']) ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Peralatan</td>
                        <td>:</td>
                        <td>Tools Specifics PT PJB SERVICES</td>
                    </tr>
                    <tr>
                        <td>Nopol Kendaraan</td>
                        <td>:</td>
                        <td><b><?php echo strtoupper($nopol) ?></b></td>
                    </tr>
                    <tr>
                        <td>Keperluan</td>
                        <td>:</td>
                        <td><b><?php echo $proyek['nama_proyek'] ?></b></td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%;margin-top: 30px;" border="1">
                    <thead class="text-white">
                        <tr>
                            <th rowspan="2" style="background: #207494;">NO</th>
                            <th rowspan="2" style="background: #207494;">NAMA PERALATAN</th>
                            <th rowspan="2" style="background: #207494;">SPESIFIKASI</th>
                            <th colspan="2" style="background: #FFAA3D;">STANDART</th>
                            <th rowspan="2" style="background: #FFAA3D;">KETERANGAN</th>
                        </tr>
                        <tr>
                            <th style="background: #FFAA3D;">JUMLAH</th>
                            <th style="background: #FFAA3D;">SATUAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $q = 'SELECT * FROM NEW_TL_PAKET_DETAIL WHERE PIC_NAME = \''.$x->pic_name.'\' AND PAKET_ID = '.$paket_id;
                            $res = $this->db->query($q);


                        ?>
                        <?php if ($res ){ ?>

                          <?php $total = 0; ?>
                          <?php foreach ($res->result() as $item): ?>
                            <tr style="text-align: center;">
                                <td><?php echo $i+1 ?></td>
                                <td><?php echo $item->tool_name ?></td>
                                <td><?php echo $item->tool_spec ?></td>
                                <td><?php echo $item->qty ?></td>
                                <td> ea </td>
                                <?php $total =+ $qty; ?>
                                <td>BAIK</td>
                            </tr>
                          <?php endforeach; ?>
                      <?php } ?>
                    </tbody>
                    <tfoot style="background: #AEEEFD;">
                        <tr>
                            <th colspan="3">TOTAL ITEM</th>
                            <th>
                                <font style="color: red;"><?php echo $total ?></font>
                            </th>
                            <th>Ea</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%;">
                            <center>
                                Driver/Toolkeeper <br><br><br><br><br><br><br>
                                <u><b><?php echo strtoupper($general['pic']) ?></b></u>
                            </center>
                        </td>
                        <td style="width: 50%;">
                            <br>
                            <center>
                                Staff Manajemen Tools <br><br><br><br><br><br><br>
                                <u><b>INDRA HERMAWAN</b></u>
                            </center>
                        </td>
                    </tr>
                </table><br><br><br>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%;vertical-align: top;">
                            <center>
                                Mengetahui, <br><br><br><br><br><br><br>
                                <u><b>RENDRA WIDYA PERDANA</b></u>
                            </center>
                        </td>
                        <td style="width: 50%;">
                            <center>
                                Mengetahui, <br><br><br><br><br><br><br>
                                <u><b>PRADANGGAPASTI A.</b></u> <br>
                                <b>Manajer Penunjang Proyek PT. <br> PJB SERVICES</b>
                            </center>
                        </td>
                    </tr>
                </table>
         </div>
    </table>
    <?php endif ?>
    <?php endforeach ?>
    <?php } ?>

        <table style="width: 100%; page-break-before: always;" border="1">
            <tr>
                <td>
                    <center>
                        <img width="50" src="<?php echo base_url('assets/img/akuarip.jpg') ?>" alt="">
                    </center>
                </td>
                <td>
                    <center>
                        <table style="text-align: center;border-collapse: collapse;width: 100%;border:none;font-weight: bold;" border="1">
                            <tr>
                                <td><br></td>
                            </tr>
                            <tr>
                                <td>PT PEMBANGKITAN TENAGA LISTRIK JAWA-BALI SERVICES</td>
                            </tr>
                            <tr>
                                <td>MANAJEMEN TOOLS & MATERIAL</td>
                            </tr>
                            <tr>
                                <td>Jl. Raya Juanda no. 17</td>
                            </tr>
                            <tr>
                                <td>Sidoarjo, Jawa Timur</td>
                            </tr>
                            <tr>
                                <td><br></td>
                            </tr>
                        </table>
                    </center>
                </td>
                <td>
                    <center>
                        <img src="" alt="">
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;background: #4A7182;" class="text-white">
                    SURAT JALAN TRUCK TOOLS KELUAR
                </td>
            </tr>
            <!-- <tr>
                <td colspan="3"> -->
                    <div style="padding: 30px;font-family: arial;">
                        <table>
                            <tr>
                                <td colspan="3">Diberikan kepada : <?= $pegawai->nama; ?></td>
                            </tr>
                            <tr>
                                <td colspan="3"><br></td>
                            </tr>
                            <tr>
                                <td>Nama (Driver)</td>
                                <td>:</td>
                                <td><b></b></td>
                            </tr>
                            <tr>
                                <td>Perusahaan</td>
                                <td>:</td>
                                <td><b>PT PJB SERVICES</b></td>
                            </tr>
                            <tr>
                                <td>Tujuan</td>
                                <td>:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Jenis Peralatan</td>
                                <td>:</td>
                                <td>Tools General PT PJB SERVICES</td>
                            </tr>
                            <tr>
                                <td>Nopol Kendaraan</td>
                                <td>:</td>
                                <td><b><?php echo strtoupper($nopol) ?></b></td>
                            </tr>
                            <tr>
                                <td>Keperluan</td>
                                <td>:</td>
                                <td><b></b></td>
                            </tr>
                        </table>
                        <br>
                        <table style="width: 100%;margin-top: 30px;" border="1">
                            <thead class="text-white">
                                <tr>
                                    <th rowspan="2" style="background: #207494;">NO</th>
                                    <th rowspan="2" style="background: #207494;">NAMA PERALATAN</th>
                                    <th rowspan="2" style="background: #207494;">SPESIFIKASI</th>
                                    <th colspan="2" style="background: #FFAA3D;">STANDART</th>
                                    <th rowspan="2" style="background: #FFAA3D;">KETERANGAN</th>
                                </tr>
                                <tr>
                                    <th style="background: #FFAA3D;">JUMLAH</th>
                                    <th style="background: #FFAA3D;">SATUAN</th>
                                </tr>
                            </thead>
                            <?php
                            $q2 = 'SELECT * FROM NEW_TL_PAKET_DETAIL WHERE TOOL_JENIS  = \'general\' AND PAKET_ID = '.$paket_id;
                            $res2 = $this->db->query($q2);
                          
                             ?>
                            <tbody>
                                <?php $total = 0; ?>
                                <?php foreach ($res2->result() as $item): ?>
                                    <tr style="text-align: center;">
                                        <td><?php echo $i+1 ?></td>
                                        <td><?php echo $item->tool_name ?></td>
                                        <td><?php echo $item->tool_spec ?></td>
                                        <td><?php echo $item->qty ?></td>
                                        <td>ea</td>
                                        <td>BAIK</td>
                                    </tr>
                                    <?php $total += $item->qty ?>
                                <?php endforeach ?>
                            </tbody>
                            <tfoot style="background: #AEEEFD;">
                                <tr>
                                    <th colspan="3">TOTAL ITEM</th>
                                    <th>
                                        <font style="color: red;"><?php echo $total ?></font>
                                    </th>
                                    <th>Ea</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%;">
                                    <center>
                                        Driver/Toolkeeper <br><br><br><br><br><br><br>
                                        <u><b><?php echo strtoupper($specific['pic']) ?></b></u>
                                    </center>
                                </td>
                                <td style="width: 50%;">
                                    <br>
                                    <center>
                                        Staff Manajemen Tools <br><br><br><br><br><br><br>
                                        <u><b>INDRA HERMAWAN</b></u>
                                    </center>
                                </td>
                            </tr>
                        </table><br><br><br>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%;vertical-align: top;">
                                    <center>
                                        Mengetahui, <br><br><br><br><br><br><br>
                                        <u><b>RENDRA WIDYA PERDANA</b></u>
                                    </center>
                                </td>
                                <td style="width: 50%;">
                                    <center>
                                        Mengetahui, <br><br><br><br><br><br><br>
                                        <u><b>PRADANGGAPASTI A.</b></u> <br>
                                        <b>Manajer Penunjang Proyek PT. <br> PJB SERVICES</b>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </div>
                <!-- </td>
            </tr> -->
        </table>
</body>

</html>
