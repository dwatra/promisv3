<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Penilaian Man Power</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.ui-autocomplete {
	    position: absolute;
	    z-index: 1000;
	    cursor: default;
	    padding: 0;
	    margin-top: 2px;
	    list-style: none;
	    background-color: #ffffff;
	    border: 1px solid #ccc;
	    -webkit-border-radius: 5px;
	       -moz-border-radius: 5px;
	            border-radius: 5px;
	    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	       -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	}
	.ui-autocomplete > li {
	  padding: 3px 20px;
	}
	.ui-state-hover,
	.ui-state-active,
	.ui-state-focus {
	  text-decoration: none;
	  color: #262626;
	  background-color: #f5f5f5;
	  cursor: pointer;
	}
	.ui-helper-hidden-accessible {
	  display: none;
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Sumber Pegawai&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<?php echo $data->sumber_pegawai ?>
					</div>
				</div>
				<div class="form-group" style="display:none;">
					<label for="nid" class="col-sm-3 control-label">
						NID&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="nid" id="nid" class="form-control w-100" value="<?php echo $data->nid ?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						NID&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<?php echo $data->nid ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Nama&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<?php echo $data->nama ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Unit&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<?php echo $data->unit ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Jabatan&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<?php echo $data->jabatan ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Jabatan Proyek&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<?php echo $data->jabatan_proyek ?>
					</div>
				</div>
				<fieldset>
  				<legend>Kompetensi</legend>
					<div class="form-group">
						<label for="inti" class="col-sm-3 control-label">
							Inti&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<input autocomplete="off" type="text" name="inti" id="inti" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data_kompetensi->inti ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="tambahan" class="col-sm-3 control-label">
							Tambahan&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<input autocomplete="off" type="text" name="tambahan" id="tambahan" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data_kompetensi->tambahan ?>">
						</div>
					</div>
				</fieldset>
				<fieldset>
  				<legend>Penilaian Individu</legend>
					<div class="form-group">
						<label for="tahun_masuk" class="col-sm-3 control-label">
							Tahun Masuk&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<input autocomplete="off" type="text" name="tahun_masuk" id="tahun_masuk" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data_kompetensi->tahun_masuk ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="grade" class="col-sm-3 control-label">
							Grade&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<input autocomplete="off" type="text" name="grade" id="grade" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data_kompetensi->grade ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="skill" class="col-sm-3 control-label">
							Skill&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<input autocomplete="off" type="text" name="skill" id="skill" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data_kompetensi->skill ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="kompetensi" class="col-sm-3 control-label">
							Kompetensi&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<input autocomplete="off" type="text" name="kompetensi" id="kompetensi" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data_kompetensi->kompetensi ?>">
						</div>
					</div>
				</fieldset>
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</section>
<script type="text/javascript">
	$('select[name="unit"]').val('<?php echo $pegawai->unit ?>')
	$('select').select2().trigger('change')
	$('select[name="jabatan_proyek"]').val('<?php echo $data->id_jabatan_proyek ?>')
	$('select').select2().trigger('change')
	
	function namaChange(){ 
		if($("#nama").val() != '' && $("#nama").val().length > 2){
			$.ajax({
                url: '<?= base_url() . "new/man/getJsonName" ?>',
                type: 'get',
                data: {
                    name: $("#nama").val()
                },
                success: function (response) {
                	var nids = [];
                	var namas = [];
                	$.each(response.data, function (key, row) {
						nids[key] = row.nid;
						namas[key] = row.nama;
                    });
                    $( "#nama" ).autocomplete({
				      source: namas,
				      select: function( event, ui ) {
				            var val = ui.item['value'];
				            var index = namas.indexOf(val);
				            $("#nid").val(nids[index]);
				        }
				    });
                }
            });
		}
	}
</script>