<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Detail Persebaran Man Power</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
	    <div class="box-header with-border">
	      <div class="pull-left">
	        <?= '<button type="button" class="btn waves-effect btn-default" onclick="window.location=\''.base_url("new/man/report").'\'"><span class="glyphicon glyphicon-arrow-left"></span> Back</button>' ?>
	      </div>
	    </div>
		<div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h4 align="center"><?= $proyek->nama_proyek ?></h4><br/>
					<div style="overflow: auto;">
				        <table class="table table-bordered">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>NID</th>
				                <th>Nama</th>
				                <th>Jabatan Proyek</th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php if(count($detail)>0){ ?>
					            <?php $no = 0; ?>
					            <?php foreach ($detail as $his) { ?>
					            	<tr align="center">
						                <td><?= ++$no ?></td>
						                <td><?= $his->nid ?></td>
						                <td><?= $his->nama ?></td>
						                <td><?= $his->jabatan ?></td>
					            	</tr>
					            <?php } ?>
				            <?php }else{ ?>
				            	<tr align="center"><td colspan="4">Tidak ada data</td></tr>
					        <?php } ?>
				        </tbody>
				        </table>
				    </div>
	            </div>
	        </div>
		</div>
    </div>
</section>