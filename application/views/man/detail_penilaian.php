<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Detail Penilaian Man Power</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						NID&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<?php echo $data->nid ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Nama&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<?php echo $data->nama ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Unit&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<?php echo $data->unit ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Jabatan&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<?php echo $data->jabatan ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="text-align: right;">
						Jabatan Proyek&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<?php echo $data->jabatan_proyek ?>
					</div>
				</div>
				<fieldset>
  				<legend>Kompetensi</legend>
					<div class="form-group">
						<label for="inti" class="col-sm-3 control-label">
							Inti&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<?php echo $data_kompetensi->inti ?>
						</div>
					</div>
					<div class="form-group">
						<label for="tambahan" class="col-sm-3 control-label">
							Tambahan&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<?php echo $data_kompetensi->tambahan ?>
						</div>
					</div>
				</fieldset>
				<fieldset>
  				<legend>Penilaian Individu</legend>
					<div class="form-group">
						<label for="tahun_masuk" class="col-sm-3 control-label">
							Tahun Masuk&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<?php echo $data_kompetensi->tahun_masuk ?>
						</div>
					</div>
					<div class="form-group">
						<label for="grade" class="col-sm-3 control-label">
							Grade&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<?php echo $data_kompetensi->grade ?>
						</div>
					</div>
					<div class="form-group">
						<label for="skill" class="col-sm-3 control-label">
							Skill&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<?php echo $data_kompetensi->skill ?>
						</div>
					</div>
					<div class="form-group">
						<label for="kompetensi" class="col-sm-3 control-label">
							Kompetensi&nbsp;<span style="color:#dd4b39"></span>
						</label>
						<div class="col-sm-8">
							<?php echo $data_kompetensi->kompetensi ?>
						</div>
					</div>
				</fieldset>
			</form>

	       <!--  <div class="row">
	            <div class="col-sm-12">
	                <h4 align="center">History</h4><br/>
					<div style="overflow: auto;">
				        <table class="table table-bordered">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>Nama Proyek</th>
				                <th>Tgl Mulai</th>
				                <th>Tgl Selesai</th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php if(count($history)>0){ ?>
					            <?php $no = 0; ?>
					            <?php foreach ($history as $his) { ?>
					            	<tr align="center">
						                <td><?= ++$no ?></td>
						                <td><?= $his->nama_proyek ?></td>
						                <td><?= $his->tgl_mulai ?></td>
						                <td><?= $his->tgl_selesai ?></td>
					            	</tr>
					            <?php } ?>
				            <?php }else{ ?>
				            	<tr align="center"><td colspan="4">Tidak ada data</td></tr>
					        <?php } ?>
				        </tbody>
				        </table>
				    </div>
	            </div>
	        </div> -->
		</div>
    </div>
</section>