<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Man Power</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.ui-autocomplete {
	    position: absolute;
	    z-index: 1000;
	    cursor: default;
	    padding: 0;
	    margin-top: 2px;
	    list-style: none;
	    background-color: #ffffff;
	    border: 1px solid #ccc;
	    -webkit-border-radius: 5px;
	       -moz-border-radius: 5px;
	            border-radius: 5px;
	    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	       -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	}
	.ui-autocomplete > li {
	  padding: 3px 20px;
	}
	.ui-state-hover,
	.ui-state-active,
	.ui-state-focus {
	  text-decoration: none;
	  color: #262626;
	  background-color: #f5f5f5;
	  cursor: pointer;
	}
	.ui-helper-hidden-accessible {
	  display: none;
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
	    <div class="box-header with-border">
	      <div class="pull-left">
	        <?= '<button type="button" class="btn waves-effect btn-default" onclick="window.location=\''.base_url("new/man").'\'"><span class="glyphicon glyphicon-arrow-left"></span> Back</button>' ?>
	      </div>
	    </div>
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label for="sumber_pegawai" class="col-sm-3 control-label">
						Sumber Pegawai&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<select  data-placeholder="- Pilih Sumber Pegawai -" tabindex="2" name="sumber_pegawai" id="sumber_pegawai" class="form-control w-100" onchange="sumberChange()" required>
							<option value=""></option>
							<?php foreach ($sumbers as $sumber): ?>
								<option value="<?php echo $sumber->id_sumber_pegawai ?>"><?php echo $sumber->nama ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group" id="div_unit">
					<label for="unit" class="col-sm-3 control-label">
						Unit&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<select  data-placeholder="Pilih unit..." tabindex="2" name="unit" id="unit" class="form-control w-100">
							<?php foreach ($units as $unit): ?>
								<option value="<?php echo $unit->table_code ?>"><?php echo $unit->table_desc ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="nid" class="col-sm-3 control-label">
						NID&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="nid" id="nid" class="form-control w-100" onkeyup="nidChange()" value="<?php echo $data['nid'] ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="nama" class="col-sm-3 control-label">
						Nama&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="nama" id="nama" class="form-control w-100" style="text-transform: uppercase;" onkeyup="namaChange()" value="<?php echo $data['nama'] ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="jabatan" class="col-sm-3 control-label">
						Jabatan&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="jabatan" id="jabatan" class="form-control w-100" style="text-transform: uppercase;" value="<?php echo $data['jabatan'] ?>">
					</div>
				</div>
				<!-- <div class="form-group">
					<label for="kdjabtan" class="col-sm-3 control-label">
						Kode Jabatan&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="kdjabtan" id="kdjabtan" class="form-control w-100" style="text-transform: uppercase;">
					</div>
				</div> -->
				<div class="form-group">
					<label for="jabatan_proyek" class="col-sm-3 control-label">
						Jabatan Proyek&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<select  data-placeholder="- Pilih Jabatan -" tabindex="2" name="jabatan_proyek" id="jabatan_proyek" class="form-control w-100" required>
							<option value=""></option>
							<?php foreach ($jabatans as $jabatan): ?>
								<option value="<?php echo $jabatan->id_jabatan_proyek ?>"><?php echo $jabatan->nama ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<!-- <div class="form-group">
					<label for="kompetensi" class="col-sm-3 control-label">
						Kompetensi&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="kompetensi" id="kompetensi" class="form-control w-100" style="text-transform: uppercase;">
					</div>
				</div> -->
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</section>
<script type="text/javascript">
	$('select[name="unit"]').val('<?php echo $data['unit'] ?>')
	$('select').select2().trigger('change')
	$('select[name="jabatan_proyek"]').val('<?php echo $data['jabatan_proyek'] ?>')
	$('select[name="sumber_pegawai"]').val('<?php echo $data['sumber_pegawai'] ?>')
	$('select').select2().trigger('change')

	function nidChange(){ 
		if($("#nid").val() != '' && $("#nid").val().length > 2){
			$.ajax({
                url: '<?= base_url() . "new/man/getJsonNid" ?>',
                type: 'get',
                data: {
                    nid: $("#nid").val(),
                    unit: $("#unit").val()
                },
                success: function (response) {
                	var nidss = [];
                	var namass = [];
                	var namaviews = [];
                	$.each(response.data, function (key, row) {
						nidss[key] = row.nid;
						namass[key] = row.nama;
						namaviews[key] = row.nid+' - '+row.nama;
                    });
                    $( "#nid" ).autocomplete({
				      source: namaviews,
				      select: function( event, ui ) {
				            var val = ui.item['value'];
				            var index = namaviews.indexOf(val);
				            $("#nama").val(namass[index]);
				            setTimeout(function(){ $("#nid").val(nidss[index]); }, 5);
				        }
				    });
                }
            });
		}
	}
	function namaChange(){ 
		if($("#nama").val() != '' && $("#nama").val().length > 2){
			$.ajax({
                url: '<?= base_url() . "new/man/getJsonName" ?>',
                type: 'get',
                data: {
                    name: $("#nama").val(),
                    unit: $("#unit").val()
                },
                success: function (response) {
                	var nids = [];
                	var namas = [];
                	var namaview = [];
                	$.each(response.data, function (key, row) {
						nids[key] = row.nid;
						namas[key] = row.nama;
						namaview[key] = row.nama+' - '+row.nid;
                    });
                    $( "#nama" ).autocomplete({
				      source: namaview,
				      select: function( event, ui ) {
				            var val = ui.item['value'];
				            var index = namaview.indexOf(val);
				            $("#nid").val(nids[index]);
				            setTimeout(function(){ $("#nama").val(namas[index]); }, 5);
				        }
				    });
                }
            });
		}
	}
	function sumberChange(){
		if('<?php echo $data['unit'] ?>' == ''){
			$('#unit').select2('val','');
		}
		if($("#sumber_pegawai").val() != '' && $("#sumber_pegawai").val() == 3){
			$("#div_unit").slideDown();
		}else{
			$("#div_unit").slideUp();
		}
	}
</script>