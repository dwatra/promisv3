<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Tambah Man Power Managemen</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
		<div class="box-body">
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<label for="nama" class="col-sm-3 control-label">
						Nama&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-8">
						<select data-placeholder="- Pilih Nama -" tabindex="2" name="nama" id="nama" class="form-control w-100" onchange="namaChange()" required>
							<option value=""></option>
							<?php foreach ($namas as $nama): ?>
								<option value="<?php echo $nama->nid ?>"><?php echo $nama->nid .' - '. $nama->nama ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="nid" class="col-sm-3 control-label">
						NID&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-8">
						<input autocomplete="off" type="text" name="nid" id="nid" class="form-control w-100" readonly placeholder="Auto">
					</div>
				</div>
				<div class="form-group">
					<label for="unit" class="col-sm-3 control-label">
						Unit&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<input autocomplete="off" type="text" name="unit" id="unit" class="form-control w-100" readonly placeholder="Auto">
					</div>
				</div>
				<div class="form-group">
					<label for="unit" class="col-sm-3 control-label">
						Jabatan&nbsp;<span style="color:#dd4b39"></span>
					</label>
					<div class="col-sm-7">
						<input autocomplete="off" type="text" name="jabatan" id="jabatan" class="form-control w-100" readonly placeholder="Auto">
					</div>
				</div>
				<div class="form-group">
					<label for="jabatan_proyek" class="col-sm-3 control-label">
						Jabatan Proyek&nbsp;<span style="color:#dd4b39">*</span>
					</label>
					<div class="col-sm-7">
						<select  data-placeholder="- Pilih Jabatan -" tabindex="2" name="jabatan_proyek" id="jabatan_proyek" class="form-control w-100" required="">
							<option value=""></option>
							<?php foreach ($jabatans as $jabatan): ?>
								<option value="<?php echo $jabatan->id_jabatan_proyek ?>"><?php echo $jabatan->nama ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-11">
						<button type="submit" class="btn-save btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</section>
<script type="text/javascript">
	function namaChange(){ 
		if($("#nama").val() != ''){
			$.ajax({
                url: '<?= base_url() . "new/man/getJson" ?>',
                type: 'get',
                data: {
                    nid: $("#nama").val()
                },
                success: function (response) {
                	$.each(response.data, function (key, row) {
						$("#nid").val(row.nid);
						$("#unit").val(row.table_desc);
						$("#jabatan").val(row.jabatan);
                    });
                }
            });
		}else{
			$("#nid").val('');
			$("#unit").val('');
			$("#jabatan").val('');
		}
	}
</script>