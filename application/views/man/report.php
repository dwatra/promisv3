<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>
<section class="content-header" style="max-width:1000px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Man Power Managemen</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 1000px">
	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Mobilisasi Man Power</h3><br/>
	                <div id="indoMaps2" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getMapChart(){
	                        var data = [
	                            ['id-ac', 1], //Aceh
	                            ['id-jt', 2], //Jawa Tengah
	                            ['id-be', 3], //Bengkulu
	                            ['id-bt', 4], //Banten
	                            ['id-kb', 5], //Kalimantan Barat
	                            ['id-bb', 6], //Bangka-Belitung
	                            ['id-ba', 7], //Bali
	                            ['id-ji', 8], //Jawa Timur
	                            ['id-ks', 9], //Kalimantan Selatan
	                            ['id-nt', 10], //Nusa Tenggara Timur
	                            ['id-se', 11], //Sulawesi Selatan
	                            ['id-kr', 12], //Kepulauan Riau
	                            ['id-ib', 13], //Irian Jaya Barat
	                            ['id-su', 14], //Sumatera Utara
	                            ['id-ri', 15], //Riau
	                            ['id-sw', 16], //Sulawesi Utara
	                            ['id-ku', 17], //Kalimantan Utara
	                            ['id-la', 18], //Maluku Utara
	                            ['id-sb', 19], //Sumatera Barat
	                            ['id-ma', 20], //Maluku
	                            ['id-nb', 21], //Nusa Tenggara Barat
	                            ['id-sg', 22], //Sulawesi Tenggara
	                            ['id-st', 23], //Sulawesi Tengah
	                            ['id-pa', 24], //Papua
	                            ['id-jr', 25], //Jawa Barat
	                            ['id-ki', 26], //Kalimantan Timur
	                            ['id-1024', 27], //Lampung
	                            ['id-jk', 28], //Jakarta Raya
	                            ['id-go', 29], //Gorontalo
	                            ['id-yo', 30], //Yogyakarta
	                            ['id-sl', 31], //Sumatera Selatan
	                            ['id-sr', 32], //Sulawesi Barat
	                            ['id-ja', 33], //Jambi
	                            ['id-kt', 34] //Kalimantan Tengah
	                        ];
	                        Highcharts.mapChart('indoMaps2', {
	                            chart: {
	                                map: 'countries/id/id-all'
	                            },
	                            title: {
	                                text: ''
	                            },
	                            // untuk show label nama kota
	                            /*
	                            mapNavigation: {
	                                enabled: true,
	                                buttonOptions: {
	                                    verticalAlign: 'bottom'
	                                }
	                            },
	                            */
	                            colorAxis: {
	                                min: 0,
	                            },
	                            series: [{
	                                data: data,
	                                name: 'Man Power',
	                                states: {
	                                    hover: {
	                                        color: '#db1e61'
	                                    }
	                                },
	                                dataLabels: {
	                                    enabled: false,
	                                    format: '{point.name}'
	                                }
	                            }]
	                        });
	                    }
	                    getMapChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end -->

	<!-- panel barchart -->
	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Jumlah Gap Man Power</h3><br/>
	                <div id="barchart" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getBarChart(){
	                        var dataBarchart = {
	                          series: [{
	                          name: 'Kebutuhan',
	                          data: [
	                          <?php foreach ($dataChart['kebutuhan'] as $p) {
	                             echo "$p,";
	                          } ?>
	                          ] //value data
	                        }, {
	                          name: 'Realisasi',
	                          data: [
	                          <?php foreach ($dataChart['realisasi'] as $q) {
	                             echo "$q,";
	                          } ?>
	                          ] //value data
	                        }],
	                          chart: {
	                          type: 'bar',
	                          height: 350
	                        },
	                        plotOptions: {
	                          bar: {
	                            horizontal: false,
	                            columnWidth: '55%',
	                            // endingShape: 'rounded'
	                          },
	                        },
	                        dataLabels: {
	                          enabled: false,
	                        },
	                        stroke: {
	                          show: true,
	                          width: 2,
	                          colors: ['transparent']
	                        },
	                        xaxis: {
	                          categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
	                        },
	                        yaxis: {
	                          title: {
	                            // text: 'Jumlah Man Power'
	                          }
	                        },
	                        fill: {
	                          opacity: 1,
	                        },
	                        tooltip: {
	                          y: {
	                            formatter: function (val) {
	                              return val + " Orang"
	                            }
	                          }
	                        },
	                        colors:['#0879ce', '#db1e61']
	                        };

	                        var barchart = new ApexCharts(document.querySelector("#barchart"), dataBarchart);
	                        barchart.render();
	                    }
	                    getBarChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- panel barchart -->
	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Perbandingan Man Power Available/Sedang dalam Proyek</h3><br/>
	                <div id="barchart_av" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getBarChart(){
	                        var dataBarchart = {
	                          series: [{
	                          name: 'Available',
	                          data: [
	                          <?php //foreach ($dataChart['available'] as $p) {
	                             echo $dataChart['available'];
	                          //} ?>
	                          ] //value data
	                        }, {
	                          name: 'Dalam Proyek',
	                          data: [
	                          <?php //foreach ($dataChart['dalam_proyek'] as $q) {
	                             echo $dataChart['dalam_proyek'];
	                          //} ?>
	                          ] //value data
	                        }],
	                          chart: {
	                          type: 'bar',
	                          height: 350
	                        },
	                        plotOptions: {
	                          bar: {
	                            horizontal: false,
	                            columnWidth: '55%',
	                            // endingShape: 'rounded'
	                          },
	                        },
	                        dataLabels: {
	                          enabled: false,
	                        },
	                        stroke: {
	                          show: true,
	                          width: 2,
	                          colors: ['transparent']
	                        },
	                        xaxis: {
	                          categories: ['Man Power']
	                        },
	                        yaxis: {
	                          title: {
	                            // text: 'Jumlah Man Power'
	                          }
	                        },
	                        fill: {
	                          opacity: 1,
	                        },
	                        tooltip: {
	                          y: {
	                            formatter: function (val) {
	                              return val + " Orang"
	                            }
	                          }
	                        },
	                        colors:['#0879ce', '#db1e61']
	                        };

	                        var barchart = new ApexCharts(document.querySelector("#barchart_av"), dataBarchart);
	                        barchart.render();
	                    }
	                    getBarChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div>
	
	<div class="box box-default" style="/*min-height: 500px*/">
		<div class="box-body">
			<h5>Export Excel Report Man Power Managemen</h5>
			<br/>
				<form class="form-horizontal">
					<div class="form-group">
						<label for="periode" class="col-sm-1 control-label">
							Periode&nbsp;<span style="color:#dd4b39">*</span>
						</label>
						<div class="col-sm-2">
							<select  data-placeholder="- Pilih Periode -" tabindex="2" name="periode" id="periode" class="form-control w-100" required>
								<option value=""></option>
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-left: 0px;">
						<button class="btn btn-export fileinput-button" type="button" onclick="excel()">
					        <i class="glyphicon glyphicon-download"></i>
							Export
						</button>
					</div>
			</form>
		</div>
    </div>

	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Laporan Persebaran Man Power</h3><br/>
	                <div class="row" style="margin-bottom:  15px; margin-top: 5px">
					<div class="col-md-5">
						<div class="input-group">
							<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button type="button" class='btn btn-default btn-sm btn-filter' id="btn-filter-per" title="Filter"><span class="glyphicon glyphicon-search"></span></button>
								<button class="btn btn-export fileinput-button" type="button" onclick="excelPersebaran()">
							        <i class="glyphicon glyphicon-download"></i>
									Export
								</button>
							</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="col-md-6" align="right"><label style="margin-top: 3px;">Tgl Mulai</label></div>
						<span class="input-group col-md-6">
							<input type="text" name="tgl" id="tgl" class="form-control w-100 datepicker" >
						</span>
					</div>
					<div class="col-md-3">
						<div class="col-md-6" align="right"><label style="margin-top: 3px;">Tgl Selesai</label></div>
						<span class="input-group col-md-6">
							<input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control w-100 datepicker" >
						</span>
					</div>
						<button type="button" class='btn btn-default btn-sm btn-filter-tgl' id="btn-filter-tgl" title="Filter" onclick="filterTgl()"><span class="glyphicon glyphicon-search"></span></button>
					</div>
	                <div style="overflow: auto;">
	                    <table class="table table-bordered" id="tbl_persebaran">
	                    <thead>
	                        <tr>
	                            <th>No</th>
	                            <th>Nama Proyek</th>
	                            <th>Jumlah Man Power</th>
	                            <th>Tgl Mulai</th>
	                            <th>Tgl Selesai</th>
	                        </tr>
	                    </thead>
	                    <tbody class="text-center">
	                        <!-- <tr align="center"><td colspan="9">Tidak ada data</td></tr> -->
	                        <?php //foreach ($variable as $key => $value) { ?>
	                            
	                        <?php //} ?>
	                    </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    <script type="text/javascript">
    	$(document).ready(function() {
			load_datatable();
		});
		function filterTgl(){
			load_datatable();
		}
    	function load_datatable(){
    		var tgl  = $("#tgl").val();
    		var tgl_selesai  = $("#tgl_selesai").val();
			var datatable = $('#tbl_persebaran').DataTable({
		        destroy: true,
		        "processing": true,
		        "serverSide": true,
		        ajax: {
		          url: "<?php echo base_url('/new/man/load_data_persebaran/') ?>"+tgl+'/'+tgl_selesai
		        },
		        "order": [
		          [0, 'asc']
		        ],
		        "dom": "<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-3'l><'col-sm-3'i><'col-sm-6'p>>",
				"language": {
		            "lengthMenu": "Perhalaman _MENU_",
		            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
		        },
		        "columnDefs": [
		         	{ className: "td-right","orderable": false, "targets": [ 4 ] },
		         ],
		        "iDisplayLength": 10,
		        "scrollX" : false,
		    })
		    $('.dataTables_filter').css('display', 'none');
		    $('#btn-filter-per').click(function(){
		          datatable.search($('#filter').val()).draw();
		    })
		}
	</script>

	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Laporan Jam Kerja Man Power</h3><br/>
	                <div class="row" style="margin-bottom:  15px; margin-top: 5px">
					<div class="col-md-5">
						<div class="input-group">
							<input autocomplete="off" type="text" id="filter_jamkerja" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button type="button" class='btn btn-default btn-sm btn-filter' id="btn-filter-jamkerja" title="Filter"><span class="glyphicon glyphicon-search"></span></button>
								<button class="btn btn-export fileinput-button" type="button" onclick="excelJamKerja()">
							        <i class="glyphicon glyphicon-download"></i>
									Export
								</button>
							</span>
						</div>
					</div>
					</div>
	                <div style="overflow: auto;">
	                    <table class="table table-bordered" id="tbl_jamkerja">
	                    <thead>
	                        <tr>
	                            <th>No</th>
	                            <th>Nama</th>
	                            <th>NID</th>
	                            <th>Bidang</th>
	                            <th>Jabatan Proyek</th>
	                            <th>Total Utilitas</th>
	                        </tr>
	                    </thead>
	                    <tbody class="text-center">
	                        <tr align="center"><td colspan="9">Tidak ada data</td></tr>
	                        <?php //foreach ($variable as $key => $value) { ?>
	                            
	                        <?php //} ?>
	                    </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    <script type="text/javascript">
		var datatable_jam = $('#tbl_jamkerja').DataTable({
	        destroy: true,
	        "processing": true,
	        "serverSide": true,
	        ajax: {
	          url: "<?php echo base_url('/new/man/load_data_jamkerja') ?>"
	        },
	        "order": [
	          [0, 'asc']
	        ],
	        "dom": "<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-3'l><'col-sm-3'i><'col-sm-6'p>>",
			"language": {
	            "lengthMenu": "Perhalaman _MENU_",
	            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
	        },
	        "columnDefs": [
	         	{ className: "td-right","orderable": false, "targets": [ 5 ] },
	         ],
	        "iDisplayLength": 10,
	        "scrollX" : false,
	    })
	    $('.dataTables_filter').css('display', 'none');
	    $('#btn-filter-jamkerja').click(function(){
	          datatable_jam.search($('#filter_jamkerja').val()).draw();
	    })
	</script>

</section>
<script type="text/javascript">
	function excel() {
		if($('#periode').val() == ''){
			alert('Pilih periode terlebih dahulu.');
		}else{
        	document.location.href = '<?= base_url('/new/man/excel')  ?>/'+$('#periode').val();
    	}
    }
	function excelPersebaran() {
        document.location.href = '<?= base_url('/new/man/excelPersebaran')  ?>/'+$('#filter').val();
    }
	function excelJamKerja() {
        document.location.href = '<?= base_url('/new/man/excelJamKerja')  ?>/'+$('#filter_jamkerja').val();
    }
</script>