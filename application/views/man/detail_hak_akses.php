<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Manajemen Hak Akses Tools</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
</style>
<section class="content" style="max-width: 800px">
	<div class="box box-default">
	    <div class="box-header with-border">
	      <div class="pull-left">
	        <?= '<button type="button" class="btn waves-effect btn-default" onclick="window.location=\''.base_url("new/man/hak_akses").'\'"><span class="glyphicon glyphicon-arrow-left"></span> Back</button>' ?>
	      </div>
	    </div>
		<div class="box-body">
			<form class="form-horizontal" method="POST">
	        <div class="row">
	            <div class="col-sm-12">
	                <h4 align="center"><?= $proyek->nama_proyek ?></h4><br/>
					<div style="overflow: auto;">
				        <table class="table table-bordered">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>NID</th>
				                <th>Nama</th>
				                <th>Jabatan Proyek</th>
				                <th>Akses</th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php if(count($detail)>0){ ?>
					            <?php $no = 0; ?>
					            <?php foreach ($detail as $his) { ?>
					            <?php $checked = $his->checked == 1 ? 'checked' : ''; ?>
					            	<tr align="center">
						                <td><?= ++$no ?></td>
						                <td><?= $his->nid ?></td>
						                <td><?= $his->nama ?></td>
						                <td><?= $his->jabatan ?></td>
						                <td><?= '<input type="checkbox" name="cb_data['.$his->id.']" '.$checked.' >' ?></td>
					            	</tr>
					            <?php } ?>
				            <?php }else{ ?>
				            	<tr align="center"><td colspan="5">Tidak ada data</td></tr>
					        <?php } ?>
				        </tbody>
				        </table>
	            </div>
	            <?php if(count($detail)>0){ ?>
						<br>
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-info pull-right">
									<span class="glyphicon glyphicon glyphicon-floppy-save"></span> Simpan
								</button>
							</div>
						</div>
				<?php } ?>
				    </div>
				</div>
			</form>
	        </div>
		</div>
    </div>
</section>