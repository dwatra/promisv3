<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>
<section class="content-header" style="max-width:1000px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Dashboard Man Power</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.select2-selection--multiple{
		border-radius: 10px !important;
		padding-left: 5px;
		min-height: 30px;
	}
</style>
<section class="content" style="max-width: 1000px; padding-bottom: 50px">
	<div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Mobilisasi Man Power</h3><br/>
	                <div id="indoMaps2" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getMapChart(){
	                        var data = [
	                            ['id-ac', 1], //Aceh
	                            ['id-jt', 2], //Jawa Tengah
	                            ['id-be', 3], //Bengkulu
	                            ['id-bt', 4], //Banten
	                            ['id-kb', 5], //Kalimantan Barat
	                            ['id-bb', 6], //Bangka-Belitung
	                            ['id-ba', 7], //Bali
	                            ['id-ji', 8], //Jawa Timur
	                            ['id-ks', 9], //Kalimantan Selatan
	                            ['id-nt', 10], //Nusa Tenggara Timur
	                            ['id-se', 11], //Sulawesi Selatan
	                            ['id-kr', 12], //Kepulauan Riau
	                            ['id-ib', 13], //Irian Jaya Barat
	                            ['id-su', 14], //Sumatera Utara
	                            ['id-ri', 15], //Riau
	                            ['id-sw', 16], //Sulawesi Utara
	                            ['id-ku', 17], //Kalimantan Utara
	                            ['id-la', 18], //Maluku Utara
	                            ['id-sb', 19], //Sumatera Barat
	                            ['id-ma', 20], //Maluku
	                            ['id-nb', 21], //Nusa Tenggara Barat
	                            ['id-sg', 22], //Sulawesi Tenggara
	                            ['id-st', 23], //Sulawesi Tengah
	                            ['id-pa', 24], //Papua
	                            ['id-jr', 25], //Jawa Barat
	                            ['id-ki', 26], //Kalimantan Timur
	                            ['id-1024', 27], //Lampung
	                            ['id-jk', 28], //Jakarta Raya
	                            ['id-go', 29], //Gorontalo
	                            ['id-yo', 30], //Yogyakarta
	                            ['id-sl', 31], //Sumatera Selatan
	                            ['id-sr', 32], //Sulawesi Barat
	                            ['id-ja', 33], //Jambi
	                            ['id-kt', 34] //Kalimantan Tengah
	                        ];
	                        Highcharts.mapChart('indoMaps2', {
	                            chart: {
	                                map: 'countries/id/id-all'
	                            },
	                            title: {
	                                text: ''
	                            },
	                            // untuk show label nama kota
	                            /*
	                            mapNavigation: {
	                                enabled: true,
	                                buttonOptions: {
	                                    verticalAlign: 'bottom'
	                                }
	                            },
	                            */
	                            colorAxis: {
	                                min: 0,
	                            },
	                            series: [{
	                                data: data,
	                                name: 'Man Power',
	                                states: {
	                                    hover: {
	                                        color: '#db1e61'
	                                    }
	                                },
	                                dataLabels: {
	                                    enabled: false,
	                                    format: '{point.name}'
	                                }
	                            }]
	                        });
	                    }
	                    getMapChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end -->
	
	<div class="box box-default" style="/*min-height: 500px*/">
		<div class="box-body">
			<div class="row" style="margin-bottom:  15px; margin-top: 5px">
				<h3 align="center">Management Man Power</h3>
				<div class="col-md-5">
					<div class="input-group">
						<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="button" class='btn btn-default btn-sm btn-filter' title="Filter"><span class="glyphicon glyphicon-search"></span></button>
							<button type="button" class="btn waves-effect btn-sm btn-default" title="Reset"><span class="glyphicon glyphicon-refresh"></span></button>
						</span>
					</div>
					
				</div>
				<!-- <div class="col-md-1">
					<a href="<?php echo base_url('/new/man/assign') ?>" class=" btn btn-warning">
						<span class="glyphicon glyphicon glyphicon-plus"></span> Add
					</a>	
				</div> -->
			</div>
			<h5>Daftar Proyek</h5>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama Proyek</th>
						<th class="text-center">Kebutuhan Man Power</th>
						<th class="text-center">Rencana Mulai</th>
						<th class="text-center">Rencana Selesai</th>
						<th class="text-center">Status</th>
						<!-- <th class="text-center"></th> -->
					</tr>
				</thead>
				<tbody class="text-center">
					
				</tbody>
			</table>
		</div>
    </div>
    <script type="text/javascript">
		var datatable = $('table').DataTable({
	        destroy: true,
	        "processing": true,
	        "serverSide": true,
	        ajax: {
	          url: "<?php echo base_url('/new/man/load_data_bind') ?>"
	        },
	        "order": [
	          [0, 'asc']
	        ],
	        "dom": "<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-3'l><'col-sm-3'i><'col-sm-6'p>>",
			"language": {
	            "lengthMenu": "Perhalaman _MENU_",
	            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
	        },
	        "columnDefs": [
	         	{ className: "td-right","orderable": false, "targets": [ 5 ] },
	         ],
	        "iDisplayLength": 10,
	        "scrollX" : false,
	    })
	    $('.dataTables_filter').css('display', 'none');
	    $('.btn-filter').click(function(){
	          datatable.search($('#filter').val()).draw();
	    })
	</script>

	<!-- panel barchart -->
	<!-- <div class="box box-default">
	    <div class="box-body">
	        <div class="row">
	            <div class="col-sm-12">
	                <h3 align="center">Statistik Man Power</h3><br/>
	                <div id="barchart" class="height-sm"></div>
	                <script type="text/javascript">
	                    function getBarChart(){
	                        var dataBarchart = {
	                          series: [{
	                          name: 'Kebutuhan',
	                          data: [
	                          <?php foreach ($dataChart['kebutuhan'] as $p) {
	                             echo "$p,";
	                          } ?>
	                          ] //value data
	                        }, {
	                          name: 'Realisasi',
	                          data: [
	                          <?php foreach ($dataChart['realisasi'] as $q) {
	                             echo "$q,";
	                          } ?>
	                          ] //value data
	                        }],
	                          chart: {
	                          type: 'bar',
	                          height: 350
	                        },
	                        plotOptions: {
	                          bar: {
	                            horizontal: false,
	                            columnWidth: '55%',
	                            // endingShape: 'rounded'
	                          },
	                        },
	                        dataLabels: {
	                          enabled: false,
	                        },
	                        stroke: {
	                          show: true,
	                          width: 2,
	                          colors: ['transparent']
	                        },
	                        xaxis: {
	                          categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
	                        },
	                        yaxis: {
	                          title: {
	                            // text: 'Jumlah Man Power'
	                          }
	                        },
	                        fill: {
	                          opacity: 1,
	                        },
	                        tooltip: {
	                          y: {
	                            formatter: function (val) {
	                              return val + " Orang"
	                            }
	                          }
	                        },
	                        colors:['#0879ce', '#db1e61']
	                        };

	                        var barchart = new ApexCharts(document.querySelector("#barchart"), dataBarchart);
	                        barchart.render();
	                    }
	                    getBarChart();
	                </script>
	            </div>
	        </div>
	    </div>
	</div> -->
</section>
<script type="text/javascript">
</script>