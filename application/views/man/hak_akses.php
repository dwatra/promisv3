<section class="content-header" style="max-width:1000px; margin-right:auto; margin-left:auto;">
	<h1 class="text-white">Manajemen Hak Akses Tools</h1>
</section>
<style type="text/css">
	.table{
		margin-bottom: 10px
	}
	.select2-selection--multiple{
		border-radius: 10px !important;
		padding-left: 5px;
		min-height: 30px;
	}
</style>
<section class="content" style="max-width: 1000px; padding-bottom: 50px">
	
	<div class="box box-default" style="/*min-height: 500px*/">
		<div class="box-body">
			<div class="row" style="margin-bottom:  15px; margin-top: 5px">
				<!-- <h3 align="center">Management Man Power</h3> -->
				<div class="col-md-5">
					<div class="input-group">
						<input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="button" class='btn btn-default btn-sm btn-filter' title="Filter"><span class="glyphicon glyphicon-search"></span></button>
							<button type="button" class="btn waves-effect btn-sm btn-default" title="Reset"><span class="glyphicon glyphicon-refresh"></span></button>
						</span>
					</div>
					
				</div>
				<!-- <div class="col-md-1">
					<a href="<?php echo base_url('/new/man/assign') ?>" class=" btn btn-warning">
						<span class="glyphicon glyphicon glyphicon-plus"></span> Add
					</a>	
				</div> -->
			</div>
			<!-- <h5>Daftar Proyek</h5> -->
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama Proyek</th>
						<th class="text-center">Kebutuhan Man Power</th>
						<!-- <th class="text-center">Rencana Mulai</th>
						<th class="text-center">Rencana Selesai</th>
						<th class="text-center">Status</th> -->
						<!-- <th class="text-center"></th> -->
					</tr>
				</thead>
				<tbody class="text-center">
					
				</tbody>
			</table>
		</div>
    </div>
    <script type="text/javascript">
		var datatable = $('table').DataTable({
	        destroy: true,
	        "processing": true,
	        "serverSide": true,
	        ajax: {
	          url: "<?php echo base_url('/new/man/load_data_proyek') ?>"
	        },
	        "order": [
	          [0, 'asc']
	        ],
	        "dom": "<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-3'l><'col-sm-3'i><'col-sm-6'p>>",
			"language": {
	            "lengthMenu": "Perhalaman _MENU_",
	            "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
	        },
	        "columnDefs": [
	         	{ className: "td-right","orderable": false, "targets": [ 2 ] },
	         ],
	        "iDisplayLength": 10,
	        "scrollX" : false,
	    })
	    $('.dataTables_filter').css('display', 'none');
	    $('.btn-filter').click(function(){
	          datatable.search($('#filter').val()).draw();
	    })
	</script>
</section>
<script type="text/javascript">
</script>