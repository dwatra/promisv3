<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?php echo $page_title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?= base_url("assets/template/backend") ?>/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">
  <!-- <link href="<?= base_url() ?>assets/css/dataTables.bootstrap.css" rel="stylesheet"> -->
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css"> -->
  <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.js"></script>
    <![endif]-->

  <!-- jQuery 2.1.4 -->
  <script src="<?php echo base_url() ?>assets/template/backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
  <script src="<?php echo base_url() ?>assets/js/autoNumeric.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/tab-scrolling/jquery.scrolling-tabs.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="<?= site_url() ?>assets/template/backend/bootstrap/js/bootstrap.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/template/backend") ?>/plugins/slimScroll/jquery.slimscroll.js"></script>
  <script src="<?= base_url("assets/template/backend") ?>/plugins/sweetalert/sweetalert.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/template/backend") ?>/dist/js/app.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/template/backend") ?>/dist/js/demo.js"></script>
  <script src="<?= base_url("assets/js") ?>/custom.js"></script>
  <!-- kntl kode -->
  <script src="<?php echo base_url() ?>assets/js/datepicker/js/moment.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script>
    $(function() {
      $(".datepicker").datetimepicker({
        format: "DD-MM-YYYY",
        useCurrent: false
      });
    });
  </script>
  <script>
    $(function() {
      $(".datetimepicker").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        useCurrent: false
      });
    });
  </script>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/datepicker/css/bootstrap-datetimepicker.min.css" />
  <script src="<?php echo base_url() ?>assets/js/select2/select2.full.min.js"></script>
  <script>
    $(function() {
      $(".select2, select.form-control").select2({
        placeholder: '-pilih-',
        allowClear: true // Shows an X to allow the user to clear the value.
      });
    });
  </script>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/select2/select2.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/upload/css/jquery.fileupload.css" />
  <script src="<?php echo base_url() ?>assets/js/upload/js/vendor/jquery.ui.widget.js"></script>
  <script src="<?php echo base_url() ?>assets/js/upload/js/jquery.iframe-transport.js"></script>
  <script src="<?php echo base_url() ?>assets/js/upload/js/jquery.fileupload.js"></script>
  <script src="<?php echo base_url() ?>assets/js/jquery.mask.min.js"></script>
  <!-- kntl kode -->

  <link rel="stylesheet" href="<?= base_url("assets/template/backend") ?>/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?= base_url("assets/template/backend") ?>/dist/css/animate.css">
  <link rel="stylesheet" href="<?= base_url("assets/template/backend") ?>/dist/css/skins/_all-skins.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/tab-scrolling/jquery.scrolling-tabs.min.css">
  <!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/Datatables/css/dataTables.bootstrap.min.css">
  <script type="text/javascript" src="<?php echo base_url() ?>assets/Datatables/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/Datatables/js/dataTables.bootstrap.min.js"></script>
  <style type="text/css">
    .content {
      min-height: 100%;
      height: 100vh;
    }
  </style>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-blue layout-top-nav">
  <div class="page-loader-wrapper">
    <div class="loader">
      <div class="preloader">
        <div class="spinner-layer pl-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
      <p>Please wait...</p>
    </div>
  </div>
  <div class="wrapper">

    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="#" class="navbar-brand">
              <img src="<?= base_url("assets/template/backend") ?>/dist/img/logo-pjb-service-shear2.png" height="58px" style="display: inline; margin-top: -3px" />

              PROMIS
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <!-- <?= $this->auth->GetMenu(); ?> -->
            <?php echo $this->auth->GetMenu(); ?>
          </div><!-- /.navbar-collapse -->
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li>
                <a href="<?= site_url("panelbackend/rab_task") ?>">
                  <i class="glyphicon glyphicon-flag" style="font-size: 16px"></i><span class="label label-danger"><?= $count_task ?></span>
                </a>
              </li>
              <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                  <i class="glyphicon glyphicon-question-sign" style="font-size: 16px"></i>
                </a>
                <ul class="dropdown-menu" style="min-width: 150px !important;">
                  <!-- <li class="header" style="text-align: center;"><b>HELP</b><hr></li> -->
                  <li class="body">
                    <a target="_blank" href="<?= site_url('panelbackend/home/ug') ?>">
                      <div class="menu-info">
                        <i class="glyphicon glyphicon-book"></i>
                        USER GUIDE
                      </div>
                    </a>

                  </li>
                </ul>
              </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?= base_url() . "assets/img/akuarip.jpg" ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?= $_SESSION[SESSION_APP]['name'] ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?= base_url() . "assets/img/akuarip.jpg" ?>" class="img-circle" alt="User Image">
                    <p>
                      <?= $_SESSION[SESSION_APP]['name'] ?>
                    </p>
                  </li>
                  <?php if ($is_administrator) { ?>
                    <li class="user-body">
                      <div class="col-xs-12 text-center">
                        <a href="<?= base_url("panelbackend/loginas") ?>"><i class="fa fa-user fa-fw"></i> Login sebagai user lain</a>
                      </div>
                    </li>
                  <?php } ?>
                  <!-- Menu Footer-->
                  <?php if ($_SESSION[SESSION_APP]['login']) { ?>
                    <li class="user-footer">
                      <div class="pull-left">
                        <!--  <a href="<?= base_url("panelbackend/home/profile") ?>" class="btn btn-default btn-flat">Profile</a> -->
                      </div>
                      <div class="pull-right">
                        <a href="<?= base_url("panelbackend/Login/logout") ?>" class="btn btn-default btn-flat">Sign out</a>
                      </div>
                    </li>
                  <?php } else { ?>
                    <li class="user-body">
                      <div class="col-xs-12 text-center">
                        <a href="<?= base_url("panelbackend/login") ?>"><i class="fa fa-user fa-fw"></i> Login</a>
                      </div>
                    </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
        </div><!-- /.container-fluid -->
      </nav>
    </header>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <?php if ($_SESSION['success']) : ?>
          <div class="alert alert-success">
            <?php echo $_SESSION['success'];
            unset($_SESSION['success']) ?>
          </div>
        <?php endif ?>
        <?php if (isset($error)) : ?>
          <div class="alert alert-danger" role="alert">
            <?php if (is_array($error)) : ?>
              <?php foreach ($error as $e) : ?>
                <?php echo $e ?><br>
              <?php endforeach ?>
            <?php else : ?>
              <?php echo $error ?>
            <?php endif ?>
          </div>
        <?php endif ?>
        <form method="post" enctype="multipart/form-data" id="main_form" class="form-horizontal">
          <input type="hidden" name="act" id="act" />
          <input type="hidden" name="go" id="go" />
          <input type="hidden" name="key" id="key" value="<?= $this->post['key'] ?>" />
          <?php // echo $content;
          ?>


        </form>
        <!-- <section class="content contai"> -->
        <?php echo $content ?>
        <!-- </section> -->
      </div>
    </div>

  </div><!-- ./wrapper -->
  <script type="text/javascript">
    <?php if (isset($error)) : ?>
      <?php print_r($error) ?>
    <?php endif ?>
  </script>
</body>

</html>
