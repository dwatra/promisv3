<section class="content-header" style="max-width:800px; margin-right:auto; margin-left:auto;">
  <h1 class="text-white">Master TDB</h1>
</section>
<style type="text/css">
  .table {
    margin-bottom: 10px
  }
</style>
<section class="content">
  <div class="box box-default">
    <div class="box-body">
      <div class="row" style="margin-bottom:  15px; margin-top: 5px">
        <div class="col-md-5">
          <div class="input-group">
            <input autocomplete="off" type="text" id="filter" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="button" class='btn btn-default btn-sm' title="Filter" onclick="load_table()"><span class="glyphicon glyphicon-search"></span></button>
              <button type="button" class="btn waves-effect btn-sm btn-default" onclick="tbl_reset()"><span class="glyphicon glyphicon-refresh"></span></button>
            </span>
          </div>

        </div>
        <div class="col-md-1">
          <button type="button" onclick="do_add()" href="javascript:void(0)" class=" btn btn-warning">
            <span class="glyphicon glyphicon glyphicon-plus"></span> Unit Baru
          </button>
        </div>
      </div>
      <table class="table table-striped table-hover" style="width: 100%;" id="tbltdb">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">Nama Unit</th>
            <th class="text-center">Tipe Unit</th>
            <th class="text-center">Kapasitas</th>
            <th class="text-center">Tahun Operasi</th>
            <th class="text-center">Engine Manufacture</th>
            <th class="text-center">Boiler Manufacture</th>
            <th class="text-center">Turbine Manufacture</th>
            <th class="text-center">File 1</th>
            <th class="text-center">File 2</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="text-center">

        </tbody>
      </table>
    </div>
  </div>
</section>
<div class="modal" tabindex="-1" role="dialog" id="modal-add">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">TDB <label>Input</label></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-group" id="inputan" enctype="multipart/form-data">
          <input type="hidden" name="" id="id">
          <div class="form-group">
            <label for=""> Nama Unit </label>
            <input type="text" class="form-control" name="nama_unit" id="nama_unit" placeholder="Nama Unit">
          </div>
          <div class="form-group">
            <label for=""> Tipe Unit </label>
            <input type="text" class="form-control" name="tipe_unit" id="tipe_unit" placeholder="Tipe Unit">
          </div>
          <div class="form-group">
            <label for=""> Kapasitas </label>
            <input type="text" class="form-control" name="kapasitas" id="kapasitas" placeholder="Kapasitas">
          </div>
          <div class="form-group">
            <label for=""> Tahun Operasi </label>
            <input type="text" class="form-control" name="tahun_operasi" id="tahun_operasi" placeholder="Tahun Operasi">
          </div>
          <div class="form-group">
            <label for=""> Engine Manufacture </label>
            <input type="text" class="form-control" name="engine_manufacture" id="engine_manufacture" placeholder="Engine Manufacture">
          </div>
          <div class="form-group">
            <label for=""> Boiler Manufacture </label>
            <input type="text" class="form-control" name="boiler_manufacture" id="boiler_manufacture" placeholder="Boiler Manufacture">
          </div>
          <div class="form-group">
            <label for=""> Turbine Manufacture </label>
            <input type="text" class="form-control" name="turbine_manufacture" id="turbine_manufacture" placeholder="Turbine Manufacture">
          </div>
          <div class="form-group">
            <label for=""> File 1 </label>
            <input type="file" class="form-control" name="files[]" id="file_1">
            <p id="text_file_0"></p>
          </div>
          <div class="form-group">
            <label for=""> File 2 </label>
            <input type="file" class="form-control" name="files[]" id="file_2">
            <p id="text_file_1"></p>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="do_process()" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    load_table();
  });

  function tbl_reset() {

    $("#filter").val('');
    load_table();
  }

  function load_table() {


    var datatable = $('#tbltdb').dataTable({
      destroy: true,
      "processing": true,
      "serverSide": true,
      ajax: {
        url: "<?php echo base_url('/new/tdb/load_data/') ?>"
      },
      "order": [
        [0, 'desc']
      ],
      "language": {
        "lengthMenu": "Perhalaman _MENU_",
        "info": "Menampilkan _PAGE_ sampai _PAGES_ dari total _MAX_",
      },
      "columns": [{
          "name": "ID"
        },
        {
          "name": "NAMA_UNIT"
        },
        {
          "name": "TIPE_UNIT"
        },
        {
          "name": "KAPASITAS"
        },
        {
          "name": "TAHUN_OPERASI"
        },
        {
          "name": "ENGINE_MANUFACTURE"
        },
        {
          "name": "BOILER_MANUFACTURE"
        },
        {
          "name": "TURBINE_MANUFACTURE"
        },
        {
          "name": "FILE_0"
        },
        {
          "name": "FILE_1"
        },
        // { "name": "ACT" },
        {
          className: "td-right",
          "orderable": false,
          "targets": [10]
        },
      ],
      "iDisplayLength": 10,
      "scrollX": false,
    });

    var filter = $("#filter").val();
    if (filter != '') {
      datatable.search(filter).draw();
    }

  }

  function do_add() {

    $("#modal-add").modal('show');
    $('#inputan').trigger('reset');
    $("#text_file_0").text('');
    $("#text_file_1").text('');
  };

  function do_process() {

    var tdb_id = $("#id").val();
    // var form = $("#inputan").serialize();
    var formData = new FormData($('#inputan')[0]);
    // alert(formData);
    // alert(form);
    $.ajax({
      url: '<?= base_url() ?>new/tdb/process/' + tdb_id,
      type: 'POST',
      dataType: 'JSON',
      data: formData,
      contentType: false,
      processData: false,
      success: function(resp) {
        if (resp.status == 200) {

          alert('success update data');
        } else if (resp.status == 201) {

          alert('success save data');
        } else {

          alert('error : ' + resp.msg);
        }
        $('#inputan').trigger('reset');
        $("#text_file_0").text('');
        $("#text_file_1").text('');
        $("#modal-add").modal('hide');
        load_table();
      }
    });

  }

  function edit_unit(id) {
    load_data_simple(id);
    $("#modal-add").modal('show');
  }

  function load_data_simple(tdb_id) {

    $.ajax({
      url: '<?= base_url() ?>new/tdb/load_detail/' + tdb_id,
      type: 'POST',
      dataType: 'JSON',
      success: function(resp) {
        if (resp.status == 200) {
          console.log(resp);
          $("#id").val(resp.data.id);
          $("#nama_unit").val(resp.data.nama_unit);
          $("#tipe_unit").val(resp.data.tipe_unit);
          $("#kapasitas").val(resp.data.kapasitas);
          $("#tahun_operasi").val(resp.data.tahun_operasi);
          $("#engine_manufacture").val(resp.data.engine_manufacture);
          $("#boiler_manufacture").val(resp.data.boiler_manufacture);
          $("#turbine_manufacture").val(resp.data.turbine_manufacture);
          $("#text_file_0").text(resp.data.file_0);
          $("#text_file_1").text(resp.data.file_1);
        }
      }
    });

  }

  function delete_data(id) {


    if (confirm('Hapus data tdb ini ?')) {
      $.ajax({
        url: '<?= base_url() ?>new/tdb/delete_data/' + id,
        type: 'POST',
        dataType: 'JSON',
        success: function(resp) {
          if (resp.status == 200) {
            alert('success deleted');
            load_table();
          }
        }
      });
    }


  }
</script>