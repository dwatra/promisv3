<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tool extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('M_query', 'query');
		$this->load->model("Rab_proyekModel","proyek");
	}

	public function getDetail($id)
	{
		$ret = [];
		$paket = $this->query->get_data_simple('NEW_TL_PAKET' , ['PR_ID' => $id])->row()->id;
		$ret['detail'] = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL' , ['PAKET_ID' => $paket])->result();
		foreach ($ret['detail'] as $key => $value) {
			$value->stok = $this->_searchTool($value->tool_name, $value->tool_spec);
		}
		return $ret;
	}

	public function getCart(){
		$ret = [];
		$select = 'A.* , B.NAME , B.SPEK';

		$join['data'][] = [
			'table' =>'NEW_TL_TOOL B',
			'join'  => 'A.TOOL_ID = B.ID',
			'type'  => 'INNER',
		];

		$ret['detail'] = $this->query->get_data_complex($select,'NEW_TL_CART_IE A'  , NULL , NULL , NULL  , $join)->result();
		foreach ($ret['detail'] as $key => $value) {
			$data = $this->query->get_data_simple('NEW_TL_TOOL', ['ID' => $value->tool_id])->row();

			$value->stok = $this->_searchTool($data->name, $data->spek);
		}
		return $ret;
	}

	private function _searchTool($name, $spek){
		$this->db->select('sum(QTY) as stok');
		$this->db->from('NEW_TL_TOOL');
		$this->db->where('LOWER(NAME)', strtolower($name));
		$this->db->where('LOWER(SPEK)', strtolower($spek));
		$data = $this->db->get()->row();
		return $data->stok ? $data->stok : 0;
	}

	public function getSummedPlan($param=null){
		// ini_set('display_errors' , 0);
		$sum = [];
		for ($i=1; $i <= 12 ; $i++) {
			$where = [
				'to_char(CREATED_AT ,\'DD-MM-YYYY\' ) >=' => date("1-$i-Y"),
				'to_char(CREATED_AT ,\'DD-MM-YYYY\' ) <=' => date("28-$i-Y"),
			];
			$stok = $this->query->get_data_simple('NEW_TL_TOOL', $where);
			$sum['plan'][$i-1] = 0;
			$sum['stok'][$i-1] = 0;
			$paket_id = $this->_getPrByDate("'%".$i."-".date('Y')."'");
			foreach($paket_id as $id){

				$data = $this->query->get_data_simple('NEW_TL_PAKET_DETAIL', ['PAKET_ID' => $id->id])->result();
			 	foreach ($data as $d) {
					$sum['plan'][$i-1] += $d->qty;
				}

			}
			if ($stok) {
				foreach ($stok->result() as $s) {
					$sum['stok'][$i-1] += $s->qty;
				}
			}
		}
		return $sum;
	}
	private function _getPrByDate($param){
		// return $this->db->query("select id from RAB_PROYEK inner join NEW_TL_PAKET on RAB_PROYEK.ID_PROYEK=NEW_TL_PAKET.PR_ID where to_char(TGL_RENCANA_MULAI , 'DD-MM-YYYY') like $param or to_char(TGL_RENCANA_SELESAI , 'DD-MM-YYYY') like $param")->result();
	}
}

/* End of file Tool.php */
/* Location: ./application/models/Tool.php */
