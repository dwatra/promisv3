<?php
class FrontModel extends _Model{
	function __construct(){
		parent::__construct();
	}

	public function GetMenu($reset=false){
		/*$menu = $_SESSION[SESSION_APP]['menu_frontend'];

		if(!count($menu) or $reset){*/
			$menu = $this->GetMenuArr();
		/*	$_SESSION[SESSION_APP]['menu_frontend'] = $menu;
		}*/

		return $menu;
	}

	public function GetSideBar($is_home=false){
		$ret = array();

		$data = $this->GetArray("select * from contents_page_halaman where tipe in ('1','2') order by urutan");

		if($data){

			foreach ($data as $key => $value) {
				$url = str_replace(' ', '-', $value['nama']).'.html';

				$sub = array();
				# code...
				# 
				if($value['page_url'] && $is_home){
					$url = $value['page_url'];
				}else{
					switch($value['jenis']){
						case 1:
							$url = "#";
						break;
						case 2:
							$url = site_url("pagedetail/index/{$value['halaman']}/$url");
						break;
						case 3:
							$url = site_url("page/index/{$value['halaman']}");
						break;
						case 4:
							$url = site_url("$value[halaman]");
						break;
					}
				}

				$ret[] = array(
					"label"=>$value['nama'],
					"url"=>$url
				);
			}
		}
		return $ret;
	}

	private function GetMenuArr($parent=false){
		$ret = array();

		if(!$parent){
			$param = "where parent_halaman is null and tipe in ('0','2')";

		}else{
			$param = "where parent_halaman = '$parent' and tipe in ('0','2')";
		}
		$data = $this->GetArray("select * from contents_page_halaman $param order by urutan");

		if($data){

			foreach ($data as $key => $value) {
				$url = str_replace(' ', '-', $value['nama']).'.html';

				$sub = array();
				# code...
				switch($value['jenis']){
					case 1:
						$sub = $this->GetMenuArr($value['halaman']);
						$url = "#";
					break;
					case 2:
						$url = site_url("pagedetail/index/{$value['halaman']}/$url");
					break;
					case 3:
						$url = site_url("page/index/{$value['halaman']}");
					break;
					case 4:
						if($value['halaman']=='member/login' && $_SESSION[CUSTOMER]['customer_id']){
							$value['nama'] = 'Logout';
							$url = site_url('member/logout');
						}
						else{
							$url = site_url("$value[halaman]");
						}
					break;
				}

				$ret[] = array(
					"label"=>$value['nama'],
					"url"=>$url,
					"sub"=>$sub,
				);
			}
		}
		return $ret;
	}

	public function GetSponsor(){
		$sql = "select * from contents_sponsor where is_aktif = 1";
		$data = $this->conn->GetArray($sql);
		return $data;
	}

	private function _getDetailInfoFooter($data){
		$i=0;
		$data3 = array();
		foreach ($data as $key => $value) {
			$sql = "select * from contents_page a where halaman='".$value['halaman']."' and a.is_aktif = '1' order by modified_date desc limit 5";
			$data2 = $this->conn->GetArray($sql);
			$data3[$i] = $value;
			$data3[$i]['list'] = $data2;
			$i++;
		}

		return $data3;
	}

	public function _updateStatistik(){
		$tanggal = date('Y-m-d');

		$cek = $this->conn->GetOne("
			select 1 
			from contents_statistik_pengunjung 
			where tanggal = '$tanggal'"
		);

		if($cek){
			return $this->conn->Execute("
				update 
				contents_statistik_pengunjung set jumlah = jumlah+1
				where tanggal = '$tanggal'"
			);
		}else{
			return $this->conn->Execute("
				insert 
				into contents_statistik_pengunjung (tanggal, jumlah)
				values ('$tanggal',1)"
			);
		}
	}

	public function _updateStatistikPage($page_id=''){
		/*if(!$_SESSION['page'.$page_id]){
			$_SESSION['page'.$page_id] = $_SERVER["REMOTE_ADDR"];

			$tanggal = date('Y-m-d');

			$cek = $this->conn->GetOne("
				select 1 
				from contents_statistik_page 
				where tanggal = '$tanggal' and page_id = '$page_id'"
			);

			if($cek){
				return $this->conn->Execute("
					update 
					contents_statistik_page set jumlah = jumlah+1
					where tanggal = '$tanggal' and page_id = '$page_id'"
				);
			}else{
				return $this->conn->Execute("
					insert 
					into contents_statistik_page (page_id, tanggal, jumlah)
					values ('$page_id','$tanggal',1)"
				);
			}
		}*/
	}

	public function getStatistikPage($row=array()){
		$pagearr = array();
		foreach ($row as $r) {
			$pagearr[] = $r['page_id'];
		}

		$ret = array();
		/*if(count($pagearr)){
			$pagestr = implode(",", $pagearr);
			$sql = "select page_id, sum(jumlah) as jumlah from contents_statistik_page where page_id in ($pagestr) group by page_id";
			$data = $this->conn->GetArray($sql);
			foreach ($data as $r) {
				$ret[$r['page_id']] = $r['jumlah'];
			}
		}*/

		return $ret;
	}

	function statistik(){
		$sql = "select sum(jumlah) from contents_statistik_pengunjung where tanggal = curdate()";
		$data['hari'] = $this->conn->GetOne($sql);
		$sql = "select sum(jumlah) from contents_statistik_pengunjung where tanggal = subdate(current_date, 1)";
		$data['kemarin'] = $this->conn->GetOne($sql);
		$sql = "select sum(jumlah) from (select * from contents_statistik_pengunjung where DATE_FORMAT(tanggal,'%m-%Y') = DATE_FORMAT(curdate(),'%m-%Y') order by tanggal desc limit 6) a";
		$data['minggu'] = $this->conn->GetOne($sql);
		$sql = "select sum(jumlah) from contents_statistik_pengunjung where DATE_FORMAT(tanggal,'%m-%Y') = DATE_FORMAT(curdate(),'%m-%Y')";
		$data['bulan'] = $this->conn->GetOne($sql);
		$sql = "select sum(jumlah) from contents_statistik_pengunjung where DATE_FORMAT(tanggal,'%Y') = DATE_FORMAT(curdate(),'%Y')";
		$data['tahun'] = $this->conn->GetOne($sql);
		$sql = "select sum(jumlah) from contents_statistik_pengunjung";
		$data['total'] = $this->conn->GetOne($sql);
		return $data;
	}

	function GenerateMenu($data=null, $ul='<ul class="nav navbar-nav wow fadeInDown">', &$child_active='', $getchild = true, &$rowactive=array(), $level=false){

		if(!$data)
			$data = $this->GetMenu();

    	if($level===false)
    		$level = 1;
    	else
    		$level++;

        if($data)
        {
            $fulluri = current_url();
            $ret .= "$ul";
            foreach($data as $row){

                $url = $row['url'];

                $active = "";
                $str = str_replace(array('/index','/detail','/edit','/add'), '', $fulluri);
                $find = str_replace(array('/detail','/index'), '', $url);

                if(strpos($str, $find)!==false)
                    $child_active = $active = "active";

                $child_active1 = '';

                if(count($row['sub']) && $getchild){

                    $sub = $this->GenerateMenu($row['sub'],'<ul class="dropdown-menu">', $child_active1, true, $rowactive, $level);

                    if($child_active1)
                    	$child_active = $active = "active";

                	if($level>1){

                    $ret .=  "<li class=\"dropdown-submenu $child_active1\">\n";
                    $ret .= "<a href='".$url."'>\n".$row['label']."</a>\n";
                	}else{

                    $ret .=  "<li class=\"dropdown $child_active1\">\n";
                    $ret .= "<a href='".$url."' class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n".$row['label']." <i class=\"fa fa-angle-down\"></i>\n</a>\n";
                	}
                    $ret .= $sub;

                    $rowtemp = $row;
                    unset($rowtemp['sub']);
                    if($child_active1)
                        $rowactive[] = $rowtemp;

                }elseif(!count($row['sub'])){
                    $ret .=  "<li class=\"$active\">\n";
                    $ret .= "<a href='".$url."'>".$row['label']."</a>\n";

                    $rowtemp = $row;
                    unset($rowtemp['sub']);
                    if($active)
                        $rowactive[] = $rowtemp;
                }
                $ret .= "</li>\n";
            }

            $ret .= "</ul>";
        }
        return $ret;
    }


	public function GetTabSuplier($view="index", $step=1, $is_approved = false){

		$data = array(
			"biodata"=>array("url"=>site_url("suplier/biodata"),"label"=>"Biodata ","icon"=>"angle-right"),
			"siup"=>array("url"=>site_url("suplier/siup"),"label"=>"SIUP ","icon"=>"angle-right"),
			"pajak"=>array("url"=>site_url("suplier/pajak"),"label"=>"PAJAK & Keuangan ","icon"=>"angle-right"),
			"workshop"=>array("url"=>site_url("suplier/workshop"),"label"=>"Workshop ","icon"=>"angle-right"),
			"spesialisasi"=>array("url"=>site_url("suplier/spesialisasi"),"label"=>"Spesialisasi ","icon"=>"angle-right"),
		);

		if($is_approved){
			$data += array(
				"pemilikmodal"=>array("url"=>site_url("suplier/pemilikmodal"),"label"=>"Pemilik Modal ","icon"=>"angle-right"),
				"dewankomisaris"=>array("url"=>site_url("suplier/dewankomisaris"),"label"=>"Dewan Komisaris ","icon"=>"angle-right"),
				"pejabatstruktural"=>array("url"=>site_url("suplier/pejabatstruktural"),"label"=>"Pengurus Perusahaan","icon"=>"angle-right"),
				"kantorcabang"=>array("url"=>site_url("suplier/kantorcabang"),"label"=>"Kantor Cabang","icon"=>"angle-right"),
				"lainlain"=>array("url"=>site_url("suplier/lainlain"),"label"=>"Lain-lain","icon"=>"angle-right"),
			);
		}

		$ret.="<ul class='nav nav-tabs nav-stacked'>";

		$st = 0;
		foreach($data as $key=>$row){

			$url = trim($row['url'],"/");
			//$icon = $row['icon'];
			
			if($st<=$step){
				$active = "";
				if($key==$view)
					$active = "active";

				$ret.= "<li class=\"$active\">\n";
				$ret.="<a href='".$url."'> <i class='fa fa-{$icon} '></i> ".$row['label']." </a>\n";
				$ret.="</li>\n";
			}else{
				$ret.= "<li class=\"disabled\">\n";
				$ret.="<a > <i class='fa fa-{$icon} '></i> ".$row['label']." </a>\n";
				$ret.="</li>\n";
			}
			$st++;
		}

		$ret.="</ul>";

		return $ret;
	}
}