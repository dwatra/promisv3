<?php class Rab_scopeModel extends _Model{
	public $table = "rab_scope";
	public $pk = "id_scope";
	public $label = "equipment";
	public $order_default = "id_scope";
	function __construct(){
		parent::__construct();
	}

	public function SelectGrid($arr_param=array(), $str_field="a.id_scope, a.id_scope_parent, 
		b.equipment, a.equipment as sub_equipment, a.detail_sow, a.pic, a.term_of_condition")
	{
		$arr_return = array();
		$arr_params = array(
			'page' => 0,
			'limit' => 50,
			'order' => '',
			'filter' => ''
		);
		foreach($arr_param as $key=>$val){
			$arr_params[$key]=$val;
		}

		$arr_params['page'] = ($arr_params['page']/$arr_params['limit'])+1;

		$str_condition = " where 1=1 ";
		$str_order = "";
		if(!empty($arr_params['filter']))
		{
			$str_condition = "where ".$arr_params['filter'];
		}
		if(!empty($arr_params['order']))
		{
			$str_order = "order by ".$arr_params['order'];
		}elseif($this->order_default){
			$str_order = "order by ".$this->order_default;
		}

		$str_condition .= " and a.id_scope_parent is not null";
		if($arr_params['limit']===-1){
			$arr_return['rows'] = $this->conn->GetArray("
				select
				{$str_field}
				from
				".$this->table." a
				join ".$this->table." b on a.id_scope_parent = b.id_scope
				{$str_condition}
				{$str_order} ");
		}else{
			$arr_return['rows'] = $this->conn->PageArray("
				select
				{$str_field}
				from
				".$this->table." a
				join ".$this->table." b on a.id_scope_parent = b.id_scope
				{$str_condition}
				{$str_order} ",$arr_params['limit'],$arr_params['page']
			);
		}

		$arr_return['total'] = static::GetOne("
			select
			count(*) as total
			from
			".$this->table." a
			{$str_condition}
		");
/*
		foreach($arr_return['rows'] as $k=>$r){
			if($r['id_scope_parent']){
				$arr_return['rows'][$k]['sub_equipment'] = $r['equipment'];
				unset($arr_return['rows'][$k]['equipment']);
			}
		}

		$rows = $arr_return['rows'];
		$arr_return['rows'] = array();

		$this->GenerateSort($rows, "id_scope_parent", "id_scope", "equipment", $arr_return['rows']);*/

		return $arr_return;
	}

	public function SelectGridPrint($arr_param=array(), $str_field="a.id_scope, a.id_scope_parent, 
		b.equipment, a.equipment as sub_equipment, a.detail_sow, a.pic, a.term_of_condition")
	{
		$arr_return = array();
		$arr_params = array(
			'page' => 0,
			'limit' => 50,
			'order' => '',
			'filter' => ''
		);
		foreach($arr_param as $key=>$val){
			$arr_params[$key]=$val;
		}

		$arr_params['page'] = ($arr_params['page']/$arr_params['limit'])+1;

		$str_condition = " where 1=1 ";
		$str_order = "";
		if(!empty($arr_params['filter']))
		{
			$str_condition = "where ".$arr_params['filter'];
		}
		if(!empty($arr_params['order']))
		{
			$str_order = "order by ".$arr_params['order'];
		}elseif($this->order_default){
			$str_order = "order by ".$this->order_default;
		}

		$str_condition .= " and a.id_scope_parent is not null";
		
		$arr_return['rows'] = $this->conn->GetArray("
			select
			{$str_field}
			from
			".$this->table." a
			join ".$this->table." b on a.id_scope_parent = b.id_scope
			{$str_condition}
			{$str_order} ");

		$arr_return['total'] = static::GetOne("
			select
			count(*) as total
			from
			".$this->table." a
			{$str_condition}
		");
/*
		foreach($arr_return['rows'] as $k=>$r){
			if($r['id_scope_parent']){
				$arr_return['rows'][$k]['sub_equipment'] = $r['equipment'];
				unset($arr_return['rows'][$k]['equipment']);
			}
		}

		$rows = $arr_return['rows'];
		$arr_return['rows'] = array();

		$this->GenerateSort($rows, "id_scope_parent", "id_scope", "equipment", $arr_return['rows']);*/

		return $arr_return;
	}
}