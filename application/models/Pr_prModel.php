<?php class Pr_prModel extends _Model{
	public $table = "pr_pr";
	public $pk = "id_pr";
	public $label = "nama";
	function __construct(){
		parent::__construct();
	}
	public function SelectGridMonitoring($arr_param=array(), $str_field="*")
	{
		$arr_return = array();
		$arr_params = array(
			'page' => 0,
			'limit' => 50,
			'order' => '',
			'filter' => ''
		);
		foreach($arr_param as $key=>$val){
			$arr_params[$key]=$val;
		}

		$arr_params['page'] = ($arr_params['page']/$arr_params['limit'])+1;

		$str_condition = "";
		$str_order = "";
		if(!empty($arr_params['filter']))
		{
			$str_condition = "where ".$arr_params['filter'];
		}
		if(!empty($arr_params['order']))
		{
			$str_order = "order by ".$arr_params['order'];
		}elseif($this->order_default){
			$str_order = "order by ".$this->order_default;
		}

		$this->table = "(select 
		a.*, nvl(d.nama, c.uraian) as nama_item, case when b.status_msg is not null then b.status_msg else e.nama end as status, h.nama_proyek as nama_proyek
		from pr_pr a
		join pr_pr_detail b on a.id_pr = b.id_pr
		left join rab_rab_detail c on b.id_rab_detail = c.id_rab_detail
		left join rab_jasa_material d on d.id_jasa_material = b.id_jasa_material
		left join mt_status_pr e on a.id_status_pr = e.id_status_pr 
		left join rab_rab f on c.id_rab = f.id_rab
		left join rab_pekerjaan g on f.id_pekerjaan = g.id_pekerjaan and g.is_deleted = '0'
		left join rab_proyek h on g.id_proyek=h.id_proyek and h.is_deleted = '0'
		order by a.id_pr desc) a";

		$arr_return['rows'] = $this->conn->PageArray("
			select
			{$str_field}
			from
			".$this->table."
			{$str_condition}
			{$str_order} ",$arr_params['limit'],$arr_params['page']
		);

		$arr_return['total'] = static::GetOne("
			select
			count(*) as total
			from
			".$this->table."
			{$str_condition}
		");

		return $arr_return;
	}
}