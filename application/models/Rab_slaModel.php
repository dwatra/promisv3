<?php class Rab_slaModel extends _Model{
	public $table = "rab_sla";
	public $pk = "id_pekerjaan";
	public $label = "nama";
	function __construct(){
		parent::__construct();
	}

	public function GetByPk($id){
		if(!$id){
			return array();
		}
		$sql = "select * from ".$this->table." where {$this->pk} = ".$this->conn->qstr($id);
		$rows = $this->conn->GetArray($sql);

		$ret = array();

		foreach($rows as $r){
			$ret['nilai'][$r['id_kriteria_sla']][$r['jenis']] = $r['nilai'];
		}

		if(!$ret)
			$ret = array();

		return $ret;
	}
}