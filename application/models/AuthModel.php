<?php class AuthModel extends _Model{
	function __construct(){
		parent::__construct();
	}
	public function Login($usernamea="", $passworda="")
	{
		$username = $this->conn->qstr($usernamea);
		$password = $this->conn->qstr(sha1(md5($passworda)));
		$data = $this->GetRow("
		select * from public_sys_user
		where username= $username
		and is_active = '1'
		");
		// if($data['user_id']!=null){
		// 	$_SESSION['user']['name'] = $data['NAME'];
		// 	$_SESSION['user']['admin'] = TRUE;
		// 	$this->SetLogin($data);
		// }elseif($data)
		// {
		// 	$_SESSION['user']['name'] = $data['NAME'];
		// 	$_SESSION['user']['admin'] = FALSE;
		// 	return $this->autoLogin($usernamea, $passworda);
		// 	return array('success'=>'login berhasil');
		// }

		if($data['user_id']!=null){
			$_SESSION['user']['name'] = $data['NAME'];
			$_SESSION['user']['admin'] = TRUE;
			$this->SetLogin($data);
			return array('success'=>'login berhasil' , 'session' => $_SESSION[SESSION_APP] );
		}elseif($data) {
			$_SESSION['user']['name'] = $data['NAME'];
			$_SESSION['user']['admin'] = FALSE;
			return $this->autoLogin($usernamea, $passworda);
		}
		return array('error'=>'login gagal');
	}

	public function LoginWs($usernamea="", $passworda="")
	{
		$username = $this->conn->qstr($usernamea);
		$password = $this->conn->qstr($passworda);
		$data = $this->GetRow("
		select * from public_sys_user
		where username=$username and password=$password
		and is_active = '1'
		");
		if($data)
		{
			$this->SetLoginWs($data);
			return array('success'=>'login berhasil');
		}
		return array('error'=>'login gagal');
	}

	private function SetLoginWs($data=array(),$tokenarr=array()){
		$data = (array)$data;

		$data['login']=true;
		unset($data['password']);

		if($data['KODE_GROUP']){
			$data['group_id'] = $data['KODE_GROUP'];
		}

		if($data['PEGAWAI']){
			$data['name'] = $data['PEGAWAI'];
		}

		$data['nama_group'] = $this->conn->GetOne("select name from public_sys_group where group_id = ".$this->conn->escape($data['group_id']));

		$temp = $data;
		foreach($temp as $k=>$v){
			$k = strtolower($k);
			$data[$k] = $v;
			$_SESSION[SESSION_APP][$k]=$v;
		}

		$datenow = $this->conn->sysTimeStamp;
		$this->conn->Execute("
		update public_sys_user
		set last_ip = '{$_SERVER['REMOTE_ADDR']}', last_login = $datenow
		where username = '{$data['username']}'");
	}

    public function autoLogin($username=null, $password=null){
    	ini_set ('soap.wsdl_cache_enabled', 0);
        $wsdl = 'http://portal.pjbservices.com/index.php/portal_login?wsdl';

		$cl = new SoapClient($wsdl);

		$rv = $cl->loginAplikasi(32, $username, $password);

		$credential = $cl->getToken($username);

		if(!$credential)
			return array("error"=>"Login gagal pastikan username dan password sama dengan di portal.pjbservices.com");

		if($rv->RESPONSE == "1"){

        	$tokenarr = array(
        		'username'=>$username,
        		'credential'=>$credential,
        	);
        	$this->SetLogin($rv,$tokenarr);

			return array('success'=>'login berhasil');
		}
		elseif($rv->RESPONSE == "PAGE"){
			return array(
				'success'=>'login berhasil',
				'link'=>"http://portal.pjbservices.com/".$rv->RESPONSE_LINK."/?reqNID=".$rv->NID."&reqAplikasiId=".$rv->APLIKASI_ID
			);
		}
		else
			return array('success'=>'login berhasil');
    }

	private function SetLogin($data=array(),$tokenarr=array()){
		$data = (array)$data;

		$data['login']=true;
		unset($data['password']);

		if($data['KODE_GROUP']){
			$data['group_id'] = $data['KODE_GROUP'];
		}

		if($data['PEGAWAI']){
			$data['name'] = $data['PEGAWAI'];
		}

		$temp = $data;

		if ($temp['group_id'] == '100' || $temp['group_id'] == '101' || $temp['group_id'] == '102' ) {
			$temp['redirect'] = 'new/tool/dashboard';
		} else if ($temp['group_id'] == '110' || $temp['group_id'] == '111' || $temp['group_id'] == '112'){
			$temp['redirect'] = 'new/man/dashboard';
		} else {
			$temp['redirect'] = 'panelbackend/rab_proyek';
		}

		foreach($temp as $k=>$v){
			$k = strtolower($k);
			$data[$k] = $v;
			$_SESSION[SESSION_APP][$k]=$v;
		}

		foreach ($tokenarr as $k=>$v) {
			$_SESSION[SESSION_APP][$k]=$v;
		}

		$menuarr = $this->GetMenuArr();

		$_SESSION[SESSION_APP]['menu'] = $menuarr;

		$datenow = $this->conn->sysTimeStamp;
		$this->conn->Execute("
		update public_sys_user
		set last_ip = '{$_SERVER['REMOTE_ADDR']}', last_login = $datenow
		where username = '{$data['username']}'");
	}

	public function GetMenu($data=null, $ul="<ul class=\"nav navbar-nav\">", &$child_active='',$ischild=false, $level=1){

		$level++;


		if(!$data){
			$start = true;
			$data = $_SESSION[SESSION_APP]['menu'];
			// $data = $this->GetMenuArr();
		}

		// print_r($data);
		if($data)
		{
			$fulluri = current_url();
			$ret.="\n $ul \n ";
			foreach($data as $row){

				$url = $row['url'];
				if(!$ischild)
					$icon = $row['icon'];


				$active = "";
				$str = str_replace(array('/index','/detail','/edit','/add'), '', $fulluri);

				if(strpos($url, "rab_pekerjaan")!==false){

					$find = str_replace(array('/detail','/index'), '', $url);

					if(strpos($str, $find)!==false)
						$child_active = $active = "active";

					$find = str_replace(array('/detail','/index'), '', "panelbackend/rab");

					if(strpos($str, $find)!==false)
						$child_active = $active = "active";

					$find = str_replace(array('/detail','/index'), '', "panelbackend/rab_manpower");

					if(strpos($str, $find)!==false)
						$child_active = $active = "active";
				}else{

					$find = str_replace(array('/detail','/index'), '', $url);

					if(strpos($str, $find)!==false)
						$child_active = $active = "active";
				}


				$child_active1 = '';
				$sub = '';
				if(count($row['sub'])){
					$sub = $this->GetMenu($row['sub'],"<ul class=\"dropdown-menu\" role=\"menu\">", $child_active1,1, $level);
				}


				if($sub){
					if($level>2){
						if($child_active1)
							$child_active = $child_active1;

						$ret.= "<li class=\"$child_active1 dropdown-submenu\">\n";
					}else{
						$ret.= "<li class=\"$child_active1\">\n";
					}
					$ret.="<a href='".$url."' class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n<span class='fa fa-{$icon} '></span> ".$row['label']."<span class=\"caret\"></span>\n</a>\n";
					$ret.=$sub;
				}else{
					$ret.= "<li class=\"$active\">\n";
					$ret.="<a href='".$url."'><span class='fa fa-{$icon} '></span> ".$row['label']."</a>\n";
				}
				$ret.="</li>\n";
			}

			$ret.="</ul>";
		}
		return $ret;
	}

	public function GetTabRab($id=null, $id_pekerjaan, $id_rab=null, $is_approve=false){

		$data = array();

		if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_proyek']){
			$data = array(
				"rab_proyek/index"=>array("url"=>site_url("panelbackend/rab_proyek/index"),"label"=>"DASHBOARD","icon"=>"angle-right"),
			);
		}

		if($id){
			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_proyek']){
				$data += array(
					"rab_proyek"=>array("url"=>site_url("panelbackend/rab_proyek/detail/$id"),"label"=>"PROYEK","icon"=>"angle-right"),
				);
			}

			if(!$id_rab){
				if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_pekerjaan']){
					$data += array(
						"rab_pekerjaan"=>array("url"=>site_url("panelbackend/rab_pekerjaan/index/$id"),"label"=>"PEKERJAAN","icon"=>"angle-right"),
					);
				}
			}

			// $data += array(
			// 	"rab_tool"=>array("url"=>site_url("new/asman/atur_paket/$id"),"label"=>"TOOL","icon"=>"angle-right"),
			// );

			if(!$id_pekerjaan){
				if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_lampiran']){
					$data += array(
						"rab_lampiran"=>array("url"=>site_url("panelbackend/rab_lampiran/detail/$id"),"label"=>"LAMPIRAN","icon"=>"angle-right"),
					);
				}
			}

		}

		if($id_pekerjaan){
			if($id_rab){
				$data += array(
					"rab_pekerjaan"=>array("url"=>site_url("panelbackend/rab_pekerjaan/detail/$id/$id_pekerjaan"),"label"=>"PEKERJAAN","icon"=>"angle-right"),
				);
			}

			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_lampiran']){
				$data += array(
					"rab_lampiran"=>array("url"=>site_url("panelbackend/rab_lampiran/detail/$id/$id_pekerjaan"),"label"=>"LAMPIRAN","icon"=>"angle-right"),
				);
			}


			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/wbs']){
				$id_plan = $this->conn->GetOne("select max(id_plan) from wbs_plan where id_pekerjaan = ".$this->conn->escape($id_pekerjaan));
				if($id_plan){
					$data += array(
						"wbs"=>array("url"=>site_url("panelbackend/wbs/detail/$id_pekerjaan/$id_plan"),"label"=>"WBS","icon"=>"angle-right"),
					);
				}else{
					$data += array(
						"wbs"=>array("url"=>site_url("panelbackend/wbs/add/$id_pekerjaan"),"label"=>"WBS","icon"=>"angle-right"),
					);
				}
			}


			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_scope']){
				$data += array(
					"rab_scope"=>array("url"=>site_url("panelbackend/rab_scope/index/$id_rab"),"label"=>"SCOPE","icon"=>"angle-right"),
				);
			}


			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_jasa_material']){
				$data += array(
					"rab_jasa_material"=>array("url"=>site_url("panelbackend/rab_jasa_material/index/$id_rab"),"label"=>"JASA MATERIAL","icon"=>"angle-right"),
				);
			}

			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_manpower']){
				$data += array(
					"rab_manpower"=>array("url"=>site_url("panelbackend/rab_manpower/index/$id_rab"),"label"=>"TENAGA KERJA","icon"=>"angle-right"),
				);
			}

			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_rab_detail']){
				$data += array(
					"rab_rab_detail"=>array("url"=>site_url("panelbackend/rab_rab_detail/index/$id_rab"),"label"=>"RAB","icon"=>"angle-right"),
				);
			}

			if($is_approve){
				if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_realisasi_admin']){
					$data += array(
						"rab_realisasi_admin"=>array("url"=>site_url("panelbackend/rab_realisasi_admin/index/$id_rab"),"label"=>"REALISASI","icon"=>"angle-right"),
					);
				}
			}

			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_sla']){
				$data += array(
					"rab_sla"=>array("url"=>site_url("panelbackend/rab_sla/detail/$id_pekerjaan"),"label"=>"SLA/PRS","icon"=>"angle-right"),
				);
			}

			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/pr_pr']){
				$data += array(
					"pr_pr"=>array("url"=>site_url("panelbackend/pr_pr/index/$id_rab"),"label"=>"PR","icon"=>"angle-right"),
				);
			}

			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/pr_hpe']){
				$data += array(
					"pr_hpe"=>array("url"=>site_url("panelbackend/pr_hpe/index/$id_rab"),"label"=>"HPE","icon"=>"angle-right"),
				);
			}


			// $id_rab_niaga = $this->conn->GetOne("select max(id_rab) from rab_rab where id_rab_parent = ".$this->conn->escape($id_rab)." and jenis = '2'");

			// if($id_rab_niaga){
				/*if($_SESSION[SESSION_APP]['access_menu']['panelbackend/niaga_komersial']){
					$data += array(
						"niaga_komersial"=>array("url"=>site_url("panelbackend/niaga_komersial/index/$id_rab"),"label"=>"RAB KOM","icon"=>"angle-right"),
					);
				}*/

				/*if($_SESSION[SESSION_APP]['access_menu']['panelbackend/niaga_rab_evaluasi']){
					$data += array(
						"niaga_rab_evaluasi"=>array("url"=>site_url("panelbackend/niaga_rab_evaluasi/index/$id_rab"),"label"=>"RAB EVALUASI","icon"=>"angle-right"),
					);
				}

				if($_SESSION[SESSION_APP]['access_menu']['panelbackend/niaga_hpp']){
					$data += array(
						"niaga_hpp"=>array("url"=>site_url("panelbackend/niaga_hpp/index/$id_rab"),"label"=>"HPP","icon"=>"angle-right"),
					);
				}
				if($_SESSION[SESSION_APP]['access_menu']['panelbackend/niaga_penawaran']){
					$data += array(
						"niaga_penawaran"=>array("url"=>site_url("panelbackend/niaga_penawaran/index/$id_rab"),"label"=>"PRICING","icon"=>"angle-right"),
					);
				}*/
			// }
		}

		if($id){
			if($_SESSION[SESSION_APP]['access_menu']['panelbackend/rab_proyek']){
				/*$data += array(
					"rab_spek_material"=>array("url"=>site_url("panelbackend/rab_spek_material/index/$id"),"label"=>"SPEK MATERIAL","icon"=>"angle-right"),
				);*/
				$data += array(
					"rab_assessment"=>array("url"=>site_url("panelbackend/rab_assessment/detail/$id"),"label"=>"ASSESSMENT","icon"=>"angle-right"),
				);
			}
		}

		$curr_url = current_url();

		$ret.="<ul class='nav nav-tabs'>";


		foreach($data as $page=>$row){
			$url = $row['url'];

			$active = "";
			if($curr_url == $url)
				$active = "active";

			$ret.= "<li class=\"$active\">\n";
			$ret.="<a href='".$url."'> <i class='fa fa-{$icon} '></i> ".$row['label']." </a>\n";
			$ret.="</li>\n";
		}

		$ret.="</ul>";

		return $ret;
	}

	public function LoginAs($username="")
	{
		$username = $this->conn->qstr($username);
		$data = $this->GetRow("
		select * from public_sys_user
		where username=$username
		and is_active = '1'
		");
		if($data)
		{

			$loginas = $_SESSION[SESSION_APP];
			unset($_SESSION[SESSION_APP]);

			$data['login']=true;
			unset($data['password']);

			foreach($data as $k=>$v){
				$_SESSION[SESSION_APP][$k]=$v;
			}

			if($data['nid']){
				$datakaryawan = $this->GetRow("select * from mt_pegawai where nid='{$data['nid']}'");

				foreach($datakaryawan as $k=>$v){
					$_SESSION[SESSION_APP][$k]=$v;
				}
			}

			$_SESSION[SESSION_APP]['loginas'] = $loginas;

			$menuarr = $this->GetMenuArr();
			$_SESSION[SESSION_APP]['menu'] = $menuarr;

			$datenow = $this->conn->sysTimeStamp;
			$this->conn->Execute("
			update public_sys_user
			set last_ip = '{$_SERVER['REMOTE_ADDR']}', last_login = $datenow
			where username = '{$data['username']}'");
			return array('success'=>'login success');
		}
		return array('error'=>'login filed');
	}

	private function GetMenuArr($parent_id=null){
		$ret = '';
		$group_id = $_SESSION[SESSION_APP]['group_id'];
		$user_id = $_SESSION[SESSION_APP]['user_id'];
		$filter = ($parent_id==null)?'b.parent_id is null':'b.parent_id = '.$parent_id;
		if($user_id == 1)
		{
			$strSQL = " SELECT b.*
						FROM public_sys_menu b
						WHERE $filter
						ORDER BY b.sort";
		}else{
			$filter .= " and a.group_id =".$group_id;
			$strSQL = "	SELECT b.*
						FROM public_sys_group_menu a
						LEFT JOIN public_sys_menu b ON a.menu_id = b.menu_id
						WHERE $filter
						ORDER BY b.sort";
		}

		$data = $this->GetArray($strSQL);

		$ret = array();
		if($data)
		{
			foreach($data as $row){

				$_SESSION[SESSION_APP]['access_menu'][$row['url']]=1;

				//if($row['label']=='Setting')
				//	$ret=array_merge($ret,$this->GetMenuCmsArr());

				$url = '#';
				if($row['url']!=''){
					$url = base_url($row['url']);
				}

				$icon = 'folder';
				if($row['iconcls']){
					$icon = $row['iconcls'];
				}

				$sub = $this->GetMenuArr($row['menu_id']);

				if($row['visible']){
					$ret[] = array(
						"label"=>$row["label"],
						"icon"=>$icon,
						"url"=>$url,
						"sub"=>$sub,
					);
				}
			}
		}
		return $ret;
	}

	public function GetAction($url, $type){
		$group_id = $_SESSION[SESSION_APP]['group_id'];
		$user_id = $_SESSION[SESSION_APP]['user_id'];
		if($user_id == 1){
			$strSQL = "
				SELECT b.name
				from public_sys_action b
				LEFT JOIN public_sys_menu d ON b.menu_id=d.menu_id
				WHERE type = '$type' and b.visible = '1' AND d.url='$url'";
		}else{
			$strSQL = "
				SELECT b.name
				FROM public_sys_group_action a
				LEFT JOIN public_sys_action b ON a.action_id=b.action_id
				LEFT JOIN public_sys_group_menu c ON a.group_menu_id=c.group_menu_id
				LEFT JOIN public_sys_menu d ON c.menu_id=d.menu_id
				WHERE type = '$type'  and b.visible = '1' AND c.group_id = $group_id AND d.url='$url'";
		}

		$respons = $this->GetArray($strSQL);
		$respon = array();
		foreach($respons as $row)
		{
			$respon[]=$row['name'];
		}
		return $respon;
	}

	public function GetAccessRole($url=""){
		$group_id = $_SESSION[SESSION_APP]['group_id'];

		if($_SESSION[SESSION_APP]['user_id']==1){
			$sql = "
				SELECT
				    nvl(b.name,'index') as name
				FROM
				    public_sys_menu d
				    left join
				    public_sys_action b ON b.menu_id = d.menu_id
				WHERE d.url='$url'";
		}else{
			$sql = "
				SELECT
				    nvl(b.name,'index') as name
				FROM
				    public_sys_menu d
				        LEFT JOIN
				    public_sys_group_menu c ON c.menu_id = d.menu_id
				        left join
				    public_sys_group_action a ON a.group_menu_id = c.group_menu_id
				        LEFT JOIN
				    public_sys_action b ON a.action_id = b.action_id
				WHERE c.group_id = '$group_id' AND d.url='$url'";
		}

		$data = $this->GetArray($sql);
		$return = array();
		foreach ($data as $key => $value) {
			# code...
			$return[$value['name']]=1;
		}

		if($user_id == 1){
			$return['add'] = 1;
			$return['edit'] = 1;
			$return['delete'] = 1;
		}

		if(count($return)){

			$return['index']=1;
			$return['detail']=1;
			$return['lst']=1;
			$return['reset']=1;
			$return['preview_file']=1;
			$return['preview']=1;
			// $return['print']=1;
			$return['selesai']=1;
			$return['go_print']=1;
			$return['download_pdf']=1;
			$return['go_print_detail']=1;
			$return['open_file']=1;
			$return['logo_customer']=1;

			if($return['add'] or $return['edit']){
				$return['save']=1;
				$return['batal']=1;
				$return['import']=1;
				$return['download_template']=1;
				$return['upload_file']=1;
				$return['delete_file']=1;
				$return['logo_customer']=1;
				$return['import_list']=1;
				$return['eksport_list']=1;
				$return['export_list']=1;
			}

			if($return['delete']){
				$return['delete_file']=1;
				$return['delete_all']=1;
			}
		}

		return $return;
	}

	public function GetAccessRole1($url="",$action=""){
		$group_id = $_SESSION[SESSION_APP]['group_id'];
		$user_id = $_SESSION[SESSION_APP]['user_id'];
		if($user_id == 1){
			return true;
		}
		$return = false;
		$action = strtolower(str_replace("_action","",$action));
		if($action == 'index'){
			$filter_action = '';
		}else{
			$filter_action = " AND b.name='$action'";
		}
		if(preg_match("/index/",$action)) $filter_action = "";
		$sql = "
			SELECT 1
			FROM public_sys_group_action a
			LEFT JOIN public_sys_action b ON a.action_id=b.action_id
			LEFT JOIN public_sys_group_menu c ON a.group_menu_id=c.group_menu_id
			LEFT JOIN public_sys_menu d ON c.menu_id=d.menu_id
			WHERE c.group_id = '$group_id' AND d.url='$url' $filter_action";
		$return = $this->GetOne($sql);
		return (bool)$return;
	}

	public function statistikVisitor($limit=30){
		$sql = "select * from (select *
		from contents_statistik_pengunjung
		order by tanggal desc limit $limit) a order by tanggal asc";
		$rows = $this->conn->GetArray($sql);

		$data = array();
		$ticks = array();
		foreach ($rows as $key => $value) {
			# code...
			$data[]=array($key, $value['jumlah']);
			$ticks[]=array($key, Eng2Ind($value['tanggal']));
		}

		$ret['data'] = json_encode($data);
		$ret['ticks'] = json_encode($ticks);
		return $ret;
	}

	#active directory sso PJBS
	public function autoAuthenticate($username,$credential)
    {
        ini_set ('soap.wsdl_cache_enabled', 0);
        $wsdl = 'http://portal.pjbservices.com/index.php/portal_login?wsdl';
        $CI =& get_instance();

        $cl = new SoapClient($wsdl);
        $rv = $cl->loginToken(32, $username, $credential);
        if($rv->RESPONSE == "1")
        {
        	$tokenarr = array(
        		'username'=>$username,
        		'credential'=>$credential,
        	);
        	$this->SetLogin($rv,$tokenarr);
            return $rv;
        }
        else
            return $rv;

    }

    public function autoGroupAuthenticate($username,$credential, $groupId)
    {
        ini_set ('soap.wsdl_cache_enabled', 0);
        $wsdl = 'http://portal.pjbservices.com/index.php/portal_login?wsdl';
        $CI =& get_instance();

        $cl = new SoapClient($wsdl);
        $rv = $cl->loginGroup(32, $username, $credential, $groupId);
        if($rv->RESPONSE == "1")
        {
        	$tokenarr = array(
        		'username'=>$username,
        		'credential'=>$credential,
        	);
        	$this->SetLogin($rv,$tokenarr);
            return $rv;
        }
        else
            return $rv;

    }
}
