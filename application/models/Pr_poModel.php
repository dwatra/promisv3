<?php class Pr_poModel extends _Model{
	public $table = "pr_po";
	public $pk = "id_po";
	public $label = "nama";
	function __construct(){
		parent::__construct();
	}

	public function SelectGrid($arr_param=array(), $str_field="
		a.id_po, a.id_pr,
		tgl_permintaan, 
		no_desc,
		c.no_prk,
		nama,
		nama_proyek,
		tgl_dibutuhkan,
		no_spk,
		no_spmk,
		nilai_anggaran,
		status_msg,
		a.status,
		id_status
		")
	{
		$arr_return = array();
		$arr_params = array(
			'page' => 0,
			'limit' => 50,
			'order' => '',
			'filter' => ''
		);
		foreach($arr_param as $key=>$val){
			$arr_params[$key]=$val;
		}

		$arr_params['page'] = ($arr_params['page']/$arr_params['limit'])+1;

		$str_condition = "";
		$str_order = "";
		if(!empty($arr_params['filter']))
		{
			$str_condition = "where ".$arr_params['filter'];
		}
		if(!empty($arr_params['order']))
		{
			$str_order = "order by ".$arr_params['order'];
		}elseif($this->order_default){
			$str_order = "order by ".$this->order_default;
		}

		$str_order = str_replace("id_po", "a.id_po",$str_order);

		$tb = "(select status_msg, id_status, status, id_po, id_pr, sum(nvl(harga_satuan,0)*nvl(vol,1)) as nilai_anggaran from pr_pr_detail group by status_msg, status, id_po, id_pr, id_status)";

		if($arr_params['limit']===-1){
			$arr_return['rows'] = $this->conn->GetArray("
				select
				{$str_field}
				from
				$tb a
				left join pr_po b on a.id_po = b.id_po
				left join pr_pr c on a.id_pr = c.id_pr
				left join rab_rab d on c.id_rab = d.id_rab
				left join rab_pekerjaan e on d.id_pekerjaan = e.id_pekerjaan and e.is_deleted = '0'
				left join rab_proyek f on e.id_proyek = f.id_proyek and f.is_deleted = '0'
				{$str_condition}
				{$str_order} ");
		}else{
			$arr_return['rows'] = $this->conn->PageArray("
				select
				{$str_field}
				from
				$tb a
				left join pr_po b on a.id_po = b.id_po
				left join pr_pr c on a.id_pr = c.id_pr
				left join rab_rab d on c.id_rab = d.id_rab
				left join rab_pekerjaan e on d.id_pekerjaan = e.id_pekerjaan and e.is_deleted = '0'
				left join rab_proyek f on e.id_proyek = f.id_proyek and f.is_deleted = '0'
				{$str_condition}
				{$str_order} ",$arr_params['limit'],$arr_params['page']
			);
		}

		$arr_return['total'] = static::GetOne("
			select
			count(*) as total
			from
			$tb a
			left join pr_po b on a.id_po = b.id_po
			left join pr_pr c on a.id_pr = c.id_pr
			left join rab_rab d on c.id_rab = d.id_rab
			left join rab_pekerjaan e on d.id_pekerjaan = e.id_pekerjaan and e.is_deleted = '0'
			left join rab_proyek f on e.id_proyek = f.id_proyek and f.is_deleted = '0'
			{$str_condition}
		");

		return $arr_return;
	}

	public function SelectGridPrint($arr_param=array(), $str_field="
		a.id_po, a.id_pr,
		tgl_permintaan, 
		no_desc,
		c.no_prk,
		nama,
		nama_proyek,
		tgl_dibutuhkan,
		no_spk,
		no_spmk,
		nilai_anggaran,
		status_msg,
		a.status,
		id_status
		")
	{
		$arr_return = array();
		$arr_params = array(
			'order' => '',
			'filter' => ''
		);
		foreach($arr_param as $key=>$val){
			$arr_params[$key]=$val;
		}

		$str_condition = "";
		$str_order = "";
		if(!empty($arr_params['filter']))
		{
			$str_condition = "where ".$arr_params['filter'];
		}
		if(!empty($arr_params['order']))
		{
			$str_order = "order by ".$arr_params['order'];
		}elseif($this->order_default){
			$str_order = "order by ".$this->order_default;
		}

		$str_order = str_replace("id_po", "a.id_po",$str_order);

		$tb = "(select status_msg, status, id_status, id_po, id_pr, sum(nvl(harga_satuan,0)*nvl(vol,1)) as nilai_anggaran from pr_pr_detail group by status_msg, status, id_po, id_pr, id_status)";

		$arr_return['rows'] = $this->conn->GetArray("
			select
			{$str_field}
			from
			$tb a
			left join pr_po b on a.id_po = b.id_po
			left join pr_pr c on a.id_pr = c.id_pr
			left join rab_rab d on c.id_rab = d.id_rab
			left join rab_pekerjaan e on d.id_pekerjaan = e.id_pekerjaan and e.is_deleted = '0'
			left join rab_proyek f on e.id_proyek = f.id_proyek and f.is_deleted = '0'
			{$str_condition}
			{$str_order} ");

		$arr_return['total'] = static::GetOne("
			select
			count(*) as total
			from
			$tb a
			left join pr_po b on a.id_po = b.id_po
			left join pr_pr c on a.id_pr = c.id_pr
			left join rab_rab d on c.id_rab = d.id_rab
			left join rab_pekerjaan e on d.id_pekerjaan = e.id_pekerjaan and e.is_deleted = '0'
			left join rab_proyek f on e.id_proyek = f.id_proyek and f.is_deleted = '0'
			{$str_condition}
		");

		return $arr_return;
	}
}