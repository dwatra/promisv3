<?php class Mt_spek_materialModel extends _Model{
	public $table = "mt_spek_material";
	public $pk = "id_spek_material";
	public $label = "nama";
	public $order_default = "id_spek_material asc";
	function __construct(){
		parent::__construct();
	}

	public function SelectGridPrint($arr_param=array(), $str_field="*")
	{
		$arr_return = array();
		$arr_params = array(
			'page' => 0,
			'limit' => 50,
			'order' => '',
			'filter' => ''
		);
		foreach($arr_param as $key=>$val){
			$arr_params[$key]=$val;
		}

		$arr_params['page'] = ($arr_params['page']/$arr_params['limit'])+1;

		$str_condition = "";
		$str_order = "";
		if(!empty($arr_params['filter']))
		{
			$str_condition = "where ".$arr_params['filter'];
		}
		if(!empty($arr_params['order']))
		{
			$str_order = "order by ".$arr_params['order'];
		}elseif($this->order_default){
			$str_order = "order by ".$this->order_default;
		}

		$arr_return['rows'] = $this->conn->GetArray("
			select
			{$str_field}
			from
			".$this->table."
			{$str_condition}
			{$str_order} ");

		$arr_return['total'] = static::GetOne("
			select
			count(*) as total
			from
			".$this->table."
			{$str_condition}
		");

		return $arr_return;
	}
}