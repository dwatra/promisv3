<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Model {
    private $endpoint = 'localhost/git/promis-api/api/'; //local
    // private $endpoint = 'http://172.16.33.85/pjbs-api/api/'; //server
    private $key = '28d2c5fe4d1afdfe30d43bdb1722e39c';

	public function fetchapi($data, $method, $endpoint){
		$header = ['_token' => $this->key];
        $curl = curl_init();
        $data = http_build_query($data);
        $curl_config = [
                CURLOPT_HTTPHEADER     => [
                '_token'               => $this->key,
                ],
                CURLOPT_URL            => $this->endpoint.$endpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 60,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_POST           => 1,
                CURLOPT_POSTFIELDS     => $data,
                CURLOPT_CUSTOMREQUEST  => $method,
        ];
        curl_setopt_array($curl, $curl_config);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        // return $response;
        return json_decode($response);
    }

}

/* End of file Api.php */
/* Location: ./application/models/Api.php */
