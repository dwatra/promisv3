<?php class Rab_pekerjaanModel extends _Model{
	public $table = "rab_pekerjaan";
	public $pk = "id_pekerjaan";
	public $label = "nama";
	function __construct(){
		parent::__construct();
	}

	public function GetByPk($id, $tgl=null){
		if(!$id)
			return array();

		$sql = "select a.*, 
		case when tgl_selesai_pelaksanaan_real <= sysdate and tgl_selesai_pelaksanaan_real is not null
		then tgl_selesai_pelaksanaan_real 
		else sysdate end as sysdate1
		from ".$this->table."  a
		where {$this->pk} = ".$this->conn->qstr($id);
		$ret = $this->conn->GetRow($sql);

		$id_plan = $this->conn->GetOne("select id_plan from wbs_plan where id_pekerjaan = ".$this->conn->escape($id));

		$r = $this->conn->GetRow("select max(selesai) tgl, sum(realisasi) progress from wbs_plan_detail where is_leaf = '1' and id_plan = ".$this->conn->escape($id_plan));

		if(strtotime($ret['sysdate1'])<strtotime($r['tgl']) && $r['progress']>99)
			$ret['sysdate1'] = $r['tgl'];

		list($tg, $jm) = explode(" ", $r['tgl']);
		if($tgl)
			$ret['sysdate1'] = $tgl.' '.$jm;
		
		list($ret['sysdate1'], $time) = explode(" ", $ret['sysdate1']);
		$time = $this->conn->GetOne("select max(selesai) from wbs_working_time where id_plan = ".$this->conn->escape($id_plan));

		if(!$time)
			$time = $this->conn->GetOne("select max(selesai) from wbs_working_time where id_plan = (select max(id_plan) from wbs_working_time)");

		$ret['sysdate1'] .= ' '.$time;

		if($id_plan){

			/*$ret['progress'] = $this->conn->GetOne("select sum(realisasi) as progress
				from wbs_plan_detail a
				where a.last_update_realisasi <= to_date('".$ret['sysdate1']."','dd-mm-yyyy hh24:mi:ss') 
				and is_leaf = 1 and id_plan = ".$this->conn->escape($id_plan));*/

			$ret['progress_rencana'] = $this->conn->GetOne("select sum(rencana) as progress
				from wbs_plan_detail a
				where a.selesai <= to_date('".$ret['sysdate1']."','dd-mm-yyyy hh24:mi:ss') 
				and is_leaf = 1 and id_plan = ".$this->conn->escape($id_plan));

			$rows = $this->conn->GetArray("select mulai, rencana, durasi
				from wbs_plan_detail a
				where a.selesai > to_date('".$ret['sysdate1']."','dd-mm-yyyy hh24:mi:ss') 
				and a.mulai <= to_date('".$ret['sysdate1']."','dd-mm-yyyy hh24:mi:ss') 
				and is_leaf = 1 and id_plan = ".$this->conn->escape($id_plan));

			foreach($rows as $r){
				$detik0 = $r['durasi'];
				if(!$detik0)
					$detik0 = 1;
				
				$detik1 = hitung_detik($id_plan, $r['mulai'], $ret['sysdate1']);
				$run = ($detik1/$detik0*$r['rencana']);
				$ret['progress_rencana'] += $run;
			}

			$row = $this->conn->GetRow("select max(a.selesai) as selesai, min(a.mulai) as mulai
				from wbs_plan_detail a
				where id_plan = ".$this->conn->escape($id_plan)." and urutan <> 0");

			$ret['rencanadurasi'] = durasi_wbs($id_plan, $row['mulai'], $row['selesai']);

			$ret['durasi'] = durasi_wbs($id_plan, $ret['tgl_mulai_pelaksanaan'], $ret['sysdate1']);

			$ret['is_selesai'] = 0;

			if($ret['progress']>=99)
				$ret['is_selesai'] = 1;
		}

		if(!$ret)
			$ret = array();

		return $ret;
	}
}