<?php

class M_query extends CI_model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_query($query)
	{
		return $this->db->query($query);
	}

	public function insert_batch($tbl, $data)
	{
		$this->db->insert_batch($tbl, $data);
	}

	public function get_data($select = NULL, $from = NULL, $where = NULL, $join = NULL, $order = NULL, $or_where = NULL , $group_by = null)
	{

		if ($select) {
			$this->db->select($select);
		}
		if ($from) {
			$this->db->from($from);
		}
		if ($where) {
			$this->db->where($where);
		}
		if ($or_where) {
			for ($i = 0; $i < count($or_where); $i++) {
				$this->db->or_where($or_where[$i]['column'], $or_where[$i]['value']);
			}
		}

		if ($join) {
			$this->db->join($join['table'], $join['join'], $join['type']);
		}
		if ($order) {
			$this->db->order_by($order);
		}

		return $this->db->get();
	}

	function get_data_complex($select = NULL, $table = NULL, $limit = NULL, $like = NULL, $order = NULL, $join = NULL, $where = NULL, $where2 = NULL, $group_by = NULL, $orderby = null, $distinct = null, $having = null, $orderindex=null)
	{
		$this->db->select($select);
		$this->db->from($table);
		if ($join) {
			for ($i = 0; $i < sizeof($join['data']); $i++) {
				$this->db->join($join['data'][$i]['table'], $join['data'][$i]['join'], $join['data'][$i]['type']);
			}
		}
		if ($where) {
			for ($i = 0; $i < sizeof($where['data']); $i++) {
				$this->db->where($where['data'][$i]['column'], $where['data'][$i]['param']);
			}
		}
		if ($where2) {
			$this->db->group_start();
			for ($i = 0; $i < sizeof($where2['data']); $i++) {
				$this->db->or_where($where2['data'][$i]['column'], $where2['data'][$i]['param']);
			}
			$this->db->group_end();
		}
		if ($like) {
			$this->db->group_start();
			foreach (explode(',', $like['data'][0]['column']) as $i => $value) {
				if ($i == 0) $this->db->like($value, $like['data'][0]['param'], 'BOTH');
				else $this->db->or_like($value, $like['data'][0]['param'], 'BOTH');
			}
			$this->db->group_end();
		}
		if ($limit) {
			$this->db->limit($limit['finish'], $limit['start']);
		}
		if ($order) {
			for ($i = 0; $i < sizeof($order['data']); $i++) {
				$this->db->order_by($order['data'][$i]['column'], $order['data'][$i]['type']);
			}
		}
		if ($orderindex) {
			$this->db->order_by($orderindex);
		}
		if ($group_by) {
			$this->db->group_by($group_by);
		}

		if ($orderby) {
			$this->db->order_by($orderby);
		}

		if ($distinct) {
			$this->db->distinct();
		}

		if ($having) {
			$this->db->having($having);
		}

		$query = $this->db->get();
		return $query;

	}



	function get_data_simple($table, $where = null, $join = null)
	{
		if ($join) {
			$this->db->join($join[0], $join[1], $join[2]);
		}
		if ($where ) {
			$this->db->where($where);
		}

		$result 	=	$this->db->get($table);
		return $result;
	}

	function save_data($nama_table, $data)
	{
		$this->db->insert($nama_table, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function update_data($nama_table, $where,  $data_update = '')
	{

		$this->db->where($where);
		$this->db->update($nama_table, $data_update);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function delete_data($nama_table, $where)
	{
		$this->db->delete($nama_table, $where);
		return true;
	}


	function insert_for_id($table, $where, $data)
	{
		if ($where) {
			$this->db->set($data);
			$this->db->where($where);
			$this->db->update($table);
		} else {
			$this->db->insert($table, $data);
		}
		$error = $this->db->error();
		$result = new stdclass();
		if ($this->db->affected_rows() > 0 or $error['code'] == 0) {
			$result->status = true;
			$result->output = $this->db->insert_id();
		} else {
			$result->status = false;
			// if($error['code'] <> 0)
			$result->output = $error['code'] . ': ' . $error['message'];
		}

		return $result;
	}
}
